const express = require('express')
const app = express()
const port = process.env.PORT || 3004;

app.get('/', (req, res) => {
  res.send('Hello World!')
})
app.use(express.static('public'))
app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})