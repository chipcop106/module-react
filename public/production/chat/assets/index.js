var Ro=Object.defineProperty,Do=Object.defineProperties;var zo=Object.getOwnPropertyDescriptors;var xt=Object.getOwnPropertySymbols;var Xn=Object.prototype.hasOwnProperty,ea=Object.prototype.propertyIsEnumerable;var ta=(e,n,a)=>n in e?Ro(e,n,{enumerable:!0,configurable:!0,writable:!0,value:a}):e[n]=a,b=(e,n)=>{for(var a in n||(n={}))Xn.call(n,a)&&ta(e,a,n[a]);if(xt)for(var a of xt(n))ea.call(n,a)&&ta(e,a,n[a]);return e},v=(e,n)=>Do(e,zo(n));var $e=(e,n)=>{var a={};for(var r in e)Xn.call(e,r)&&n.indexOf(r)<0&&(a[r]=e[r]);if(e!=null&&xt)for(var r of xt(e))n.indexOf(r)<0&&ea.call(e,r)&&(a[r]=e[r]);return a};import{g as x,o as jo,A as Nt,O as Uo,I as Go,r as Bt,M as Ho,c as Vo,b as Wo,a as na,W as Qo,T as Yo,d as aa,s as Zo,e as Ko,f as Jo,S as Xo,h as c,j as t,i as er,k as h,F as bt,l as Lt,m as yt,u as k,n as Ct,B as P,p as oa,q as ra,t as Y,V as tr,v as ge,w as Rt,x as nr,y as sa,z as te,C as L,H as R,R as He,D as ia,E as ar,G as or,J as rr,K as la,L as ca,N as da,P as ua,Q as pa,U as Et,X as se,Y as sr,Z as ir,_ as we,$ as lr,a0 as Be,a1 as qe,a2 as ue,a3 as cr,a4 as ga,a5 as ha,a6 as ma,a7 as Dt,a8 as fa,a9 as Aa,aa as xa,ab as ot,ac as ba,ad as Te,ae as Ae,af as ya,ag as $t,ah as zt,ai as jt,aj as Ve,ak as Le,al as We,am as Re,an as Qe,ao as Ye,ap as rt,aq as st,ar as Ut,as as Pe,at as De,au as dr,av as ze,aw as ur,ax as Ca,ay as pr,az as Gt,aA as Ea,aB as gr,aC as $a,aD as St,aE as Ze,aF as Oe,aG as Sa,aH as hr,aI as Ht,aJ as Vt,aK as va,aL as vt,aM as Wt,aN as Ia,aO as mr,aP as fr,aQ as Ar,aR as xr,aS as ka,aT as Qt,aU as br,aV as yr,aW as Cr,aX as Er,aY as $r,aZ as Sr,a_ as Se,a$ as it,b0 as lt,b1 as vr,b2 as ct,b3 as Yt,b4 as Ir,b5 as kr,b6 as wr,b7 as Tr,b8 as Pr,b9 as _r,ba as Mr,bb as qr,bc as Or,bd as Fr,be as Nr,bf as Br,bg as Lr,bh as Rr,bi as Dr,bj as wa,bk as zr,bl as jr,bm as Ta,bn as Ur,bo as Gr,bp as Hr,bq as Vr,br as Wr,bs as Qr,bt as Yr,bu as Zr,bv as Kr,bw as Jr,bx as Xr,by as es,bz as Pa,bA as _a,bB as ts,bC as Zt,bD as ns,bE as as,bF as os,bG as rs,bH as ss,bI as is,bJ as ls,bK as cs,bL as ds,bM as Ma,bN as us,bO as ps,bP as gs,bQ as hs,bR as ms,bS as fs,bT as qa,bU as dt,bV as As,bW as xs,bX as le,bY as bs,bZ as ys,b_ as Cs,b$ as Kt,c0 as Jt,c1 as Oa,c2 as ut,c3 as Es,c4 as $s,c5 as Ss,c6 as vs,c7 as Is,c8 as ks,c9 as ws,ca as Ke,cb as Ce,cc as Xt,cd as en,ce as Fa,cf as Ts,cg as Na,ch as Ba,ci as Ps,cj as _s,ck as La,cl as Ms,cm as qs,cn as It,co as Os,cp as Fs,cq as Ns,cr as Bs,cs as Ls,ct as Rs,cu as Ra,cv as Ds,cw as zs,cx as js,cy as Us,cz as Gs,cA as Hs,cB as Vs,cC as Ws,cD as Qs,cE as Ys,cF as Zs,cG as Ks,cH as Js,cI as Xs,cJ as ei,cK as ti,cL as ni,cM as ai,cN as oi}from"./vendor.js";const ri=function(){const n=document.createElement("link").relList;if(n&&n.supports&&n.supports("modulepreload"))return;for(const s of document.querySelectorAll('link[rel="modulepreload"]'))r(s);new MutationObserver(s=>{for(const o of s)if(o.type==="childList")for(const l of o.addedNodes)l.tagName==="LINK"&&l.rel==="modulepreload"&&r(l)}).observe(document,{childList:!0,subtree:!0});function a(s){const o={};return s.integrity&&(o.integrity=s.integrity),s.referrerpolicy&&(o.referrerPolicy=s.referrerpolicy),s.crossorigin==="use-credentials"?o.credentials="include":s.crossorigin==="anonymous"?o.credentials="omit":o.credentials="same-origin",o}function r(s){if(s.ep)return;s.ep=!0;const o=a(s);fetch(s.href,o)}};ri();const si="modulepreload",Da={},ii="/",ce=function(n,a){return!a||a.length===0?n():Promise.all(a.map(r=>{if(r=`${ii}${r}`,r in Da)return;Da[r]=!0;const s=r.endsWith(".css"),o=s?'[rel="stylesheet"]':"";if(document.querySelector(`link[href="${r}"]${o}`))return;const l=document.createElement("link");if(l.rel=s?"stylesheet":si,s||(l.as="script",l.crossOrigin=""),l.href=r,document.head.appendChild(l),s)return new Promise((i,d)=>{l.addEventListener("load",i),l.addEventListener("error",d)})})).then(()=>n())};const pt=x`
  fragment Post on Post {
    id
    title
    slug
    excerpt
    content
    status
    numOfComments
    categories {
      id
      title
      slug
    }
    tags {
      id
      name
      slug
    }
    postGroups {
      id
      title
      slug
    }
    publishedAt
    featureMedias {
      id
      type
      path
      variants {
        id
        type
        width
        height
        path
      }
    }
    user {
      id
      role
      username
      fullName
      phone
      gender
      avatar
    }
    censor {
      id
      fullName
    }
    createdAt
    updatedAt
    numOfContextualLink
  }
`;x`
  query posts(
    $after: String
    $before: String
    $first: Int
    $last: Int
    $categorySlugs: [String]
    $relatedPost: ObjectID
  ) @api(name: "zim") {
    posts(
      after: $after
      before: $before
      first: $first
      last: $last
      categorySlugs: $categorySlugs
      relatedPost: $relatedPost
    ) {
      totalCount
      edges {
        cursor
        node {
          ...Post
        }
      }
      pageInfo {
        startCursor
        endCursor
        hasNextPage
        hasPreviousPage
      }
    }
  }
  ${pt}
`;x`
  query ($idOrSlug: String!) @api(name: "zim") {
    post(idOrSlug: $idOrSlug) {
      ...Post
      seo {
        title
        description
        ogDescription
        ogImage {
          id
          path
        }
        ogTitle
        publisher
        noIndex
        noFollow
        canonicalUrl
        customHeadHtml
      }
      rejectReason
    }
  }
  ${pt}
`;x`
  query postPagination(
    $searchString: String
    $page: Int
    $limit: Int
    $categoryIds: [ObjectID]
    $tagIds: [ObjectID]
    $postGroupIds: [ObjectID]
    $userIds: [Int]
    $statuses: [PostStatus]
    $censorUserIds: [Int]
    $orderBy: PostOrderBy
    $order: OrderDirection
  ) @api(name: "zim") {
    postPagination(
      searchString: $searchString
      page: $page
      limit: $limit
      categoryIds: $categoryIds
      tagIds: $tagIds
      postGroupIds: $postGroupIds
      userIds: $userIds
      statuses: $statuses
      censorUserIds: $censorUserIds
      orderBy: $orderBy
      order: $order
    ) {
      hasNextPage
      hasPrevPage
      limit
      nextPage
      prevPage
      totalDocs
      totalPages
      docs {
        ...Post
      }
    }
  }
  ${pt}
`;x`
  mutation updatePost($postId: ObjectID!, $input: UpdatePostData!)
  @api(name: "zim") {
    updatePost(input: $input, postId: $postId) {
      ...Post
    }
  }
  ${pt}
`;x`
  mutation createPost($input: CreatePostData!) @api(name: "zim") {
    createPost(input: $input) {
      ...Post
    }
  }
  ${pt}
`;x`
  mutation createTag($name: String!, $description: String) @api(name: "zim") {
    createTag(description: $description, name: $name) {
      id
      name
      slug
      description
    }
  }
`;x`
  mutation publishPost($postId: ObjectID!) @api(name: "zim") {
    publishPost(postId: $postId) {
      id
      title
      status
    }
  }
`;x`
  mutation unpublishPost($postId: ObjectID!) @api(name: "zim") {
    unpublishPost(postId: $postId) {
      id
      title
      status
    }
  }
`;x`
  mutation requestPostForApproval($postId: ObjectID!) @api(name: "zim") {
    requestPostForApproval(postId: $postId) {
      id
      title
      status
    }
  }
`;x`
  mutation approveOrRejectPost(
    $postId: ObjectID!
    $status: ApproveOrRejectPostStatus
    $rejectReason: String
    $updatedContent: String
  ) @api(name: "zim") {
    approveOrRejectPost(
      postId: $postId
      status: $status
      rejectReason: $rejectReason
      updatedContent: $updatedContent
    ) {
      id
      title
      status
      rejectReason
      content
    }
  }
`;x`
  mutation reassignPost($postId: ObjectID!, $userId: Int!) @api(name: "zim") {
    reassignPost(postId: $postId, userId: $userId) {
      id
      user {
        id
        birthday
        email
        fullName
        avatar
        roleId
        role
      }
    }
  }
`;x`
  mutation signInV2($username: String!, $password: String!) @api(name: "zim") {
    signInV2(username: $username, password: $password) {
      token
      expired_at
      refreshToken
    }
  }
`;const li=x`
  query @api(name: "zim") {
    me {
      id
      role
      roleId
      username
      fullName
      phone
      birthday
      gender
      avatar
      email
      canCensor
    }
  }
`,ci=x`
  mutation signOut @api(name: "zim") {
    signOut
  }
`;x`
  mutation createPostCategory($input: PostCategoryInput!) @api(name: "zim") {
    createPostCategory(input: $input) {
      id
      title
      slug
      description
    }
  }
`;const za=x`
  fragment CategoryFields on PostCategory {
    id
    title
    slug
    parentCategory {
      id
      title
      slug
    }
    description
  }
`;x`
  query postCategoriesTree @api(name: "zim") {
    postCategoriesTree {
      ...CategoryFields
      children {
        ...CategoryFields
        children {
          ...CategoryFields
          children {
            ...CategoryFields
          }
        }
      }
    }
  }
  ${za}
`;x`
  query postCategories($flatten: Boolean, $search: String, $parent: ObjectID)
  @api(name: "zim") {
    postCategories(search: $search, parent: $parent, flatten: $flatten) {
      ...CategoryFields
      children {
        ...CategoryFields
      }
    }
  }
  ${za}
`;x`
  mutation updatePostCategory($id: ObjectID!, $input: UpdatePostCategoryInput!)
  @api(name: "zim") {
    updatePostCategory(id: $id, input: $input) {
      id
      title
      slug
      description
    }
  }
`;x`
  mutation deletePostCategory($id: ObjectID!) @api(name: "zim") {
    deletePostCategory(id: $id)
  }
`;x`
  query comments(
    $type: CommentType!
    $after: String
    $before: String
    $first: Int
    $last: Int
    $refId: String!
    $parent: ObjectID
  ) @api(name: "zim") {
    comments(
      type: $type
      after: $after
      before: $before
      first: $first
      last: $last
      refId: $refId
      parent: $parent
    ) {
      totalCount
      edges {
        cursor
        node {
          ... on PostComment {
            id
            user {
              id
              role
              username
              fullName
              phone
              birthday
              gender
              avatar
            }
            content
            parent {
              id
              content
              user {
                id
                role
                username
                fullName
                phone
                birthday
                gender
                avatar
              }
            }
            liked
            numOfLikes
            createdAt
            updatedAt
          }
          __typename
        }
      }
      pageInfo {
        startCursor
        endCursor
        hasNextPage
        hasPreviousPage
      }
    }
  }
`;x`
  mutation postComment(
    $content: String!
    $type: CommentType
    $ref: ObjectID!
    $parent: ObjectID
  ) @api(name: "zim") {
    postComment(content: $content, type: $type, ref: $ref, parent: $parent) {
      ... on PostComment {
        id
        content
        user {
          id
          role
          username
          fullName
          phone
          birthday
          gender
          avatar
        }
        parent {
          id
          content
          user {
            id
            role
            username
            fullName
            phone
            birthday
            gender
            avatar
          }
          liked
          numOfLikes
          createdAt
          updatedAt
        }
        liked
        numOfLikes
        createdAt
        updatedAt
      }
      __typename
    }
  }
`;x`
  mutation likeOrUnlikeComment($commentId: ObjectID!, $liked: Boolean!)
  @api(name: "zim") {
    likeOrUnlikeComment(commentId: $commentId, liked: $liked) {
      ... on PostComment {
        id
        liked
        numOfLikes
      }
    }
  }
`;const ja=x`
  fragment Course on CoursePlanType {
    id
    slug
    start_date
    program {
      id
      name
      cat {
        id
        name
      }
      subject {
        id
        name
      }
      program_type {
        id
        name
      }
      level {
        subject_id
        id
        name
        graduation
        status
      }
      fee
      status
      sub_cats {
        id
        cat_id
        name
        status
        parent_id
      }
    }
    subject {
      id
      name
      status
    }
    level_out {
      subject_id
      id
      name
      graduation
    }
    type
    size {
      id
      name
      size
      status
    }
    school {
      id
      city {
        id
      }
      district {
        id
      }
      name
      address
      phone
      color_code
      lat
      lon
      status
    }
    curriculum {
      id
      name
      program {
        id
      }
      shift_minute
      total_lesson
      status
    }
    shifts {
      id
      start_time
      end_time
      status
      shift_minute
    }
    day_of_weeks {
      id
      name
    }
    config_fee {
      detail_name
      detail_id
      type
      fee
    }
    fee
    status
    ec_id
    teacher_lead_id
    description
    featured_image
  }
`,gu=x`
  query getListCoursePagination(
    $order_by: String
    $order: String
    $q: String
    $school_id: Int
    $city_id: Int
    $program_id: Int
    $program_type_id: Int
    $limit: Int
    $cat_id: Int
    $level_id: Int
    $subject_id: Int
    $status: [CourseStatusEnum]
    $type: EnumCourseTypeEnum
    $fromdate: String
    $todate: String
    $page: Int
    $bussiness_partner_id: Int
  ) @api(name: "appZim") {
    getListCoursePagination(
      q: $q
      status: $status
      subject_id: $subject_id
      level_id: $level_id
      cat_id: $cat_id
      school_id: $school_id
      city_id: $city_id
      program_id: $program_id
      program_type_id: $program_type_id
      type: $type
      fromdate: $fromdate
      todate: $todate
      order_by: $order_by
      order: $order
      page: $page
      limit: $limit
      bussiness_partner_id: $bussiness_partner_id
    ) {
      hasNextPage
      hasPrevPage
      limit
      page
      pageNext
      pagePrev
      pagingCounter
      totalPages
      totalDocs
      docs {
        id
        slug
        name
        start_date
        subcats {
          id
          cat_id
          name
          status
          parent_id
        }
        subject {
          id
          name
          status
        }
        school {
          id
          name
        }

        shifts {
          id
          start_time
          end_time
        }
        level_out {
          subject_id
          id
          name
          graduation
        }
        day_of_weeks {
          id
          name
        }
        size {
          id
          name
          size
          status
        }
        fee
        status
        total_register
        total_paid
        config_fee {
          type
          fee
          detail_id
          detail_name
        }
        end_date
        type
        ec {
          id
          full_name
        }
        teachers {
          id
          user_name
          full_name
        }
      }
    }
  }
`;x`
  query coursesPlan($offset: Int, $limit: Int, $school_id: Int, $city_id: Int)
  @api(name: "appZim") {
    coursesPlan(
      school_id: $school_id
      city_id: $city_id
      offset: $offset
      limit: $limit
    ) {
      totalCount
      edges {
        node {
          ...Course
        }
      }
      pageInfo {
        hasNextPage
        hasPreviousPage
      }
    }
  }
  ${ja}
`;x`
  query coursesPlan($limit: Int) @api(name: "appZim") {
    coursesPlan(status: incoming, limit: $limit) {
      edges {
        node {
          id
          slug
        }
      }
    }
  }
`;x`
  query coursePlan($idOrSlug: String!) @api(name: "appZim") {
    coursePlan(idOrSlug: $idOrSlug) {
      ...Course
    }
  }
  ${ja}
`;x`
  mutation createAdvisory(
    $name: String
    $phone: String!
    $email: String!
    $city_id: Int!
    $gclid: String
    $fbclid: String
    $link: String
    $note: String
  ) @api(name: "appZim") {
    createAdvisory(
      name: $name
      phone: $phone
      email: $email
      city_id: $city_id
      gclid: $gclid
      fbclid: $fbclid
      link: $link
      note: $note
    ) {
      success
      error
      message
    }
  }
`;x`
  mutation checkout($input: CheckoutInput!) @api(name: "zim") {
    checkout(input: $input) {
      paymentUrl
      order {
        id
        status
        paymentMethod
        paymentStatus
        shippingMethod
        orderId
        email
        shippingAddress {
          recipientName
          recipientPhone
          email
          street1
          street2
          city
          state
          zip
          country
        }
        total
      }
    }
  }
`;const Ua=x`
  fragment OrderBaseFields on Order {
    id
    status
    paymentMethod
    paymentStatus
    shippingMethod
    orderId
    status
    email
    shippingAddress {
      recipientName
      recipientPhone
      email
      street1
      street2
      city
      state
      zip
      country
    }
    total
    createdAt
    updatedAt
  }
`,di=x`
  fragment OrderDetailFields on Order {
    ...OrderBaseFields
    user {
      id
      fullName
    }
    billingAddress {
      recipientName
      recipientPhone
      email
      street1
      street2
      city
      state
      zip
      country
    }
    attachments {
      id
      filename
      filepath
    }
    total
    totalPreDiscount
    discount
    coupons
    items {
      quantity
      total
      preTaxTotal
      price
      salePrice
      tax
      type
      item {
        ... on SimpleProduct {
          id
          name
          slug
          images {
            id
            path
          }
          sku
          attributes {
            isVariantAttribute
            attribute {
              id
              name
            }
            value
          }
        }
        ... on Post {
          id
          excerpt
          featureMedias {
            id
            path
          }
          title
          slug
        }
        ... on MockTestItem {
          data
        }
      }
    }
  }
  ${Ua}
`;x`
  query order($idOrCode: String!) @api(name: "zim") {
    order(idOrCode: $idOrCode) {
      ...OrderBaseFields
      ...OrderDetailFields
    }
  }
  ${di}
`;x`
  query orderPagination(
    $searchString: String
    $statuses: [OrderStatus]
    $userIds: [Int]
    $orderBy: OrderOrderBy
    $order: OrderDirection
    $page: Int
    $limit: Int
    $itemType: [CheckoutProduct]
  ) @api(name: "zim") {
    orderPagination(
      searchString: $searchString
      statuses: $statuses
      userIds: $userIds
      order: $order
      orderBy: $orderBy
      page: $page
      limit: $limit
      itemType: $itemType
    ) {
      totalPages
      totalDocs
      docs {
        ...OrderBaseFields
      }
    }
  }
  ${Ua}
`;x`
  query tags(
    $after: String
    $before: String
    $first: Int
    $last: Int
    $search: String
  ) @api(name: "zim") {
    tags(
      after: $after
      before: $before
      first: $first
      last: $last
      search: $search
    ) {
      edges {
        node {
          id
          name
          description
          slug
        }
      }
      pageInfo {
        hasNextPage
        hasPreviousPage
        startCursor
        endCursor
      }
    }
  }
`;x`
  query postCategories($search: String, $parent: ObjectID) @api(name: "zim") {
    postCategories(search: $search, parent: $parent) {
      id
      title
      slug
      parentCategory {
        id
        title
        slug
      }
      children {
        id
        title
        slug
        parentCategory {
          id
          title
          slug
        }
      }
    }
  }
`;const je=x`
  fragment ProductBaseField on BaseProduct {
    ... on SimpleProduct {
      id
      sku
      name
      slug
      regularPrice
      salePrice
      salePriceFrom
      salePriceTo
      description
      shortDescription
      numOfContextualLink
      attributes {
        id
        attribute {
          id
          name
          type
          options
        }
        value
        isVariantAttribute
      }
      parentProduct {
        id
      }
      price
      categories {
        id
        title
        slug
      }
      seo {
        title
        description
        ogDescription
        ogImage {
          id
          path
        }
        ogTitle
        publisher
        noIndex
        noFollow
        canonicalUrl
        customHeadHtml
      }
      images {
        id
        path
      }
      status
      productType
      tags {
        id
        name
        description
        slug
      }
      downloadable
      attributes {
        attribute {
          id
          type
          name
          options
        }
        value
      }
      inStock
      inventories {
        id
        inStock
        manageStock
        reserved
        storeId
        inventory
        store {
          id
          name
        }
      }
      createdAt
      updatedAt
    }
    ... on VariableProduct {
      id
      sku
      name
      slug
      categories {
        id
        title
        slug
      }
      images {
        id
        filename
        path
      }
      description
      shortDescription
      status
      minPrice
      maxPrice
      minRegularPrice
      maxRegularPrice
      productType
      inStock
      variationAttributes {
        id
        name
        type
        options
      }
      createdAt
      updatedAt
      seo {
        title
        description
        ogDescription
        ogImage {
          id
          path
        }
        ogTitle
        publisher
        noIndex
        noFollow
        canonicalUrl
        customHeadHtml
      }
    }
  }
`,gt=x`
  fragment ProductDetailFields on BaseProduct {
    ...ProductBaseField
    ... on SimpleProduct {
      tags {
        id
        name
      }
      downloadable
      downloads {
        id
        filename
        path
      }

      attributes {
        id
        attribute {
          id
          name
          type
          options
        }
        value
        isVariantAttribute
        media {
          id
          type
          filename
          path
        }
      }
      parentProduct {
        id
        slug
        name
        status
        variationAttributes {
          id
          name
          type
          options
        }
      }
    }
    ... on VariableProduct {
      childCount
      childPublishedCount
      attributes {
        id
        attribute {
          id
          name
          type
          options
        }
        value
        media {
          id
          type
          filename
          path
        }
      }
      tags {
        id
        name
      }
    }
  }
  ${je}
`;x`
  query product($id: ObjectID!) @api(name: "zim") {
    productAdmin(id: $id) {
      ...ProductBaseField
      ...ProductDetailFields
    }
  }
  ${gt}
`;x`
  query productPagination(
    $searchString: String
    $statuses: [ProductStatus]
    $orderBy: ProductOrderBy
    $order: OrderDirection
    $page: Int
    $limit: Int
    $parentProduct: ObjectID
    $tagSlugs: [String]
    $flatten: Boolean
    $simpleOnly: Boolean
    $tagIds: [ObjectID]
  ) @api(name: "zim") {
    productPagination(
      searchString: $searchString
      statuses: $statuses
      parentProduct: $parentProduct
      order: $order
      orderBy: $orderBy
      page: $page
      limit: $limit
      tagSlugs: $tagSlugs
      flatten: $flatten
      simpleOnly: $simpleOnly
      tagIds: $tagIds
    ) {
      totalPages
      totalDocs
      docs {
        ...ProductBaseField
      }
    }
  }
  ${je}
`;x`
  query publishedProducts(
    $search: String
    $after: String
    $before: String
    $first: Int
    $last: Int
  ) @api(name: "zim") {
    publishedProducts(
      after: $after
      before: $before
      first: $first
      last: $last
      search: $search
    ) {
      totalCount
      pageInfo {
        hasNextPage
        hasPreviousPage
        startCursor
        endCursor
      }
      edges {
        cursor
        node {
          ...ProductBaseField
        }
      }
    }
  }
  ${je}
`;x`
  query productVariants($parentProductId: ObjectID!) @api(name: "zim") {
    productVariants(parentProductId: $parentProductId) {
      ...ProductBaseField
    }
  }
  ${je}
`;x`
  query (
    $searchString: String
    $excludeIds: [ObjectID]
    $page: Int
    $limit: Int
  ) @api(name: "zim") {
    productAttributePagination(
      page: $page
      limit: $limit
      excludeIds: $excludeIds
      searchString: $searchString
    ) {
      totalDocs
      docs {
        id
        name
        options
        type
      }
    }
  }
`;x`
  query ($id: ObjectID!) @api(name: "zim") {
    productAttribute(id: $id) {
      id
      name
      options
      type
    }
  }
`;x`
  query @api(name: "zim") {
    productCategoriesTree {
      id
      title
      slug
      parentCategory {
        id
        title
        slug
      }
      children {
        id
        title
        slug
        parentCategory {
          id
          title
          slug
        }
      }
    }
  }
`;x`
  query productCategories($search: String, $parent: ObjectID)
  @api(name: "zim") {
    productCategories(search: $search, parent: $parent) {
      id
      title
      slug
      parentCategory {
        id
        title
        slug
      }
      children {
        id
        title
        slug
        parentCategory {
          id
          title
          slug
        }
      }
    }
  }
`;x`
  query slugExist($slug: String!, $type: SlugType, $refId: ObjectID)
  @api(name: "zim") {
    slugExists(type: $type, slug: $slug, refId: $refId)
  }
`;x`
  query titleExists($title: String!, $type: SlugType, $refId: ObjectID)
  @api(name: "zim") {
    titleExists(type: $type, title: $title, refId: $refId)
  }
`;x`
  query postViewReport(
    $startTime: DateTime
    $endTime: DateTime
    $page: Int
    $limit: Int
    $userId: Int
  ) @api(name: "zim") {
    postViewReport(
      startTime: $startTime
      endTime: $endTime
      page: $page
      limit: $limit
      userId: $userId
    ) {
      totalDocs
      docs {
        post {
          id
          title
          slug
          user {
            id
            fullName
          }
        }
        viewCount
      }
    }
  }
`;x`
  query generalPostViewByDate(
    $range: FromToDateInput
    $dateViewMode: DateViewMode
  ) @api(name: "zim") {
    generalPostViewByDate(range: $range, dateViewMode: $dateViewMode) {
      label
      viewCount
      postCount
    }
  }
`;x`
  query postViewReportByUser(
    $startTime: DateTime
    $endTime: DateTime
    $page: Int
    $limit: Int
  ) @api(name: "zim") {
    postViewReportByUser(
      startTime: $startTime
      endTime: $endTime
      page: $page
      limit: $limit
    ) {
      totalDocs
      docs {
        user {
          id
          fullName
        }
        postCount
        viewCount
      }
    }
  }
`;x`
  query postReportBySpecificUser(
    $userId: Int!
    $startTime: DateTime
    $endTime: DateTime
  ) @api(name: "zim") {
    postReportBySpecificUser(
      startTime: $startTime
      endTime: $endTime
      userId: $userId
    ) {
      totalLifetimePosts
      totalLifetimePublishedPosts
      totalLifetimeViews
      totalPosts
      totalPublishedPosts
    }
  }
`;x`
  query postReportByStatus($userId: Int) @api(name: "zim") {
    postReportByStatus(userId: $userId) {
      status
      count
    }
  }
`;x`
  query postByUser(
    $page: Int
    $limit: Int
    $order: OrderDirection
    $orderBy: PostByUserSort
    $userFilter: UserWithPostUserFilter
  ) @api(name: "zim") {
    postByUser(
      page: $page
      limit: $limit
      order: $order
      orderBy: $orderBy
      userFilter: $userFilter
    ) {
      limit
      page
      totalPages
      totalDocs
      docs {
        user {
          id
          email
          fullName
          avatar
          role
        }
        reviewsAverage
        postCount
        viewCount
        employeeInfo {
          id
          showInFrontPage
          position
          priority
          userId
          user {
            id
            avatar
            role
            username
            fullName
          }
        }
      }
    }
  }
`;x`
  query commentsPagination(
    $type: CommentType!
    $refId: String!
    $parent: ObjectID
    $page: Int
  ) @api(name: "zim") {
    commentsPagination(
      type: $type
      refId: $refId
      parent: $parent
      page: $page
    ) {
      totalDocs
      totalPages
      limit
      page
      docs {
        ... on PostComment {
          id
          content
          numOfLikes
          createdAt
          numOfChildComments
          user {
            id
            email
            avatar
            fullName
          }
        }
      }
    }
  }
`;x`
  query comments(
    $first: Int
    $type: CommentType!
    $refId: String!
    $parent: ObjectID
    $after: String
  ) @api(name: "zim") {
    comments(
      type: $type
      refId: $refId
      parent: $parent
      first: $first
      after: $after
    ) {
      pageInfo {
        hasNextPage
        hasPreviousPage
        endCursor
        startCursor
      }
      totalCount
      edges {
        node {
          ... on PostComment {
            id
            content
            numOfLikes
            createdAt
            numOfChildComments
            user {
              id
              email
              avatar
              fullName
            }
          }
        }
        cursor
      }
    }
  }
`;x`
  query getUserList($roleIds: [Int], $search: String) @api(name: "zim") {
    users(role_ids: $roleIds, search: $search) {
      totalDocs
      docs {
        id
        fullName
        role
      }
    }
  }
`;x`
  query user($userId: Int!) @api(name: "zim") {
    user(id: $userId) {
      id
      fullName
      avatar
      role
      roleId
      username
      birthday
      gender
    }
  }
`;x`
  query user($userId: Int!) @api(name: "zim") {
    user(id: $userId) {
      id
      fullName
      avatar
      role
      roleId
      username
      birthday
      gender
      email
      phone
      moreInfo
    }
  }
`;const Ga=x`
  query getUserById($userId: Int!) @api(name: "appZim") {
    getUserById(id: $userId) {
      id
      full_name
      avatar
      role_name
      address
      email
      phone
      status_online
      supporter {
        id
        full_name
        avatar
        role_name
      }
      more_info {
        facebook
      }
      courseStudents {
        id
        course {
          status
          id
          slug
          name
        }
      }
    }
  }
`,ui=x`
  query getUserById($userId: Int!) @api(name: "appZim") {
    getUserById(id: $userId) {
      id
      supporter {
        id
      }
    }
  }
`,Ha=x`
  query getAccountChatPagination(
    $searchString: String
    $roles: [EnumRoleType]
    $page: Int
    $limit: Int
  ) @api(name: "appZim") {
    getAccountChatPagination(
      input: { q: $searchString, roles: $roles, page: $page, limit: $limit }
    ) {
      hasNextPage
      hasPrevPage
      limit
      page
      pageNext
      pagePrev
      pagingCounter
      totalPages
      totalDocs
      docs {
        user {
          id
          full_name
          role_id
          role_name
          status
          avatar
          status_online
        }
      }
    }
  }
`;x`
  query getListStudentsPagination($input: TicketGetListStudentsInputType)
  @api(name: "appZim") {
    getListStudentsPagination(input: $input) {
      hasNextPage
      hasPrevPage
      limit
      page
      pageNext
      pagePrev
      pagingCounter
      totalPages
      totalDocs
      docs {
        supportUId
        userId
        schoolAddress
        course {
          name
        }
        status
        user {
          id
          user_name
          full_name
          phone
          address
          email
          role_id
          role_name
          status
          avatar
        }
      }
    }
  }
`;const hu=x`
  query getListStudentsPagination($input: TicketGetListStudentsInputType)
  @api(name: "appZim") {
    getListStudentsPagination(input: $input) {
      hasNextPage
      hasPrevPage
      limit
      page
      pageNext
      pagePrev
      pagingCounter
      totalPages
      totalDocs
      docs {
        supportUId
        userId
        schoolAddress
        course {
          name
          code
          #          day_of_weeks {
          #            id
          #            name
          #          }
          #          shifts {
          #            id
          #            start_time
          #            end_time
          #          }
          ec {
            id
            full_name
          }
        }
        status
        user {
          id
          user_name
          full_name
          phone
          address
          email
          role_id
          role_name
        }
      }
    }
  }
`;x`
  query getListStudentByEc($input: AccountByEcInputType) @api(name: "appZim") {
    getListStudentByEc(input: $input) {
      page
      hasNextPage
      totalPages
      totalDocs
      docs {
        id
        full_name
        avatar
        phone
        address
        email
        status
        supporter
      }
    }
  }
`;const pi=(e=[])=>x`
  query getListStudentByEc($input: AccountByEcInputType)
  @api(name: "appZim") {
    getListStudentByEc(input: $input) {
      page
      hasNextPage
      docs {
        id
        ${e.join()}
      }
    }
  }
`;x`
  query getPersonalInfo($id: Int!) @api(name: "appZim") {
    getPersonalInfo(id: $id) {
      id
      userName
      sourceOfCustomer {
        id
        sourceOfCustomer
      }
      supporter {
        isRequestCancel
        id
        user_name
        full_name
        phone
        address
        email
      }
      city {
        id
        name
        status
      }
      district {
        id
        name
        status
      }
      ward {
        id
        wardName
      }
      street {
        id
        wardName
      }
      homeNumber
      fullName
      phone
      email
      birthday
      address
      identityCard
      placeOfIssue {
        id
        name
        status
      }
      identityCardDate
      job {
        id
        jobName
      }
      workPlace
      academicPurposes {
        id
        academicPurposesName
      }
      noteHome
      typeOfEducation
      scoreIn
      scoreOut
      status
      statusId
    }
  }
`;const mu=x`
  query searchPhone($phone: String!) @api(name: "appZim") {
    searchPhone(phone: $phone) {
      id
      user_name
      full_name
      phone
      address
      email
      role_id
      role_name
      status
      status_online
      avatar
      supporter
      student_more_info {
        identity_card_city_id
        note_home
        birthday
        city_id
        city_name
        district_id
        district_name
        ward_id
        ward_name
        street_id
        street_name
        home_number
        source_id
        source_name
        job_id
        job_name
        identity_card
        identity_card_city_name
        identity_card_date
        learning_status_id
        work_place
        learning_status_name
      }
    }
  }
`;x`
  query isUserExist($field: ExistFieldCheck!, $value: String!)
  @api(name: "zim") {
    isUserExist(field: $field, value: $value)
  }
`;const Va=x`
  fragment AdvertisementBaseFields on Advertisement {
    id
    type
    status
    title
    description
    image {
      id
      filename
      path
      width
      height
    }
    url
    clicks
    showIn
    startDate
    endDate
    createdAt
    updatedAt
  }
`,gi=x`
  fragment AdvertisementDetailFields on Advertisement {
    ...AdvertisementBaseFields
    postCategories {
      id
      title
    }
    productCategories {
      id
      title
    }
    posts {
      id
      title
    }
    tags {
      id
      name
    }
    products {
      ... on SimpleProduct {
        id
        name
      }
      ... on VariableProduct {
        id
        name
      }
    }
    excludeUrls
    position
    tagHtml
    tagPosition
  }
  ${Va}
`;x`
  query advertisementPagination(
    $page: Int
    $limit: Int
    $order: OrderDirection
    $orderBy: AdvertisementSortBy
    $statuses: [AdvertisementStatus]
  ) @api(name: "zim") {
    advertisementPagination(
      page: $page
      limit: $limit
      order: $order
      orderBy: $orderBy
      statuses: $statuses
    ) {
      totalDocs
      totalPages
      docs {
        ...AdvertisementBaseFields
      }
    }
  }
  ${Va}
`;x`
  query advertisement($id: ObjectID!) @api(name: "zim") {
    advertisement(id: $id) {
      ...AdvertisementDetailFields
    }
  }
  ${gi}
`;const Ee=x`
  query supportConversations(
    $status: ConversationStatus
    $search: String
    $offset: Int
    $limit: Int
  ) @api(name: "chat") {
    supportConversations(
      status: $status
      search: $search
      offset: $offset
      limit: $limit
    ) {
      hasNextPage
      hasPrevPage
      limit
      nextPage
      page
      prevPage
      docs {
        id
        name
        group
        participants {
          userId
          fullName
          nickname
          avatar
          isGuest
        }
        createdAt
        updatedAt
        typing
        usersTyping
        pinnedAt
        blockedBy
        isUnread
        lastMessage {
          id
          text
          from
          to
          type
          attachments {
            type
            attachmentId
            attachment {
              id
              path
              fullUrl
              type
            }
          }
          loading @client
          error @client
          seenBy
          createdAt
          updatedAt
          deletedAt
          callPayload
        }
      }
    }
  }
`,pe=x`
  query conversations(
    $status: ConversationStatus
    $search: String
    $offset: Int
    $limit: Int
    $group: String
  ) @api(name: "chat") {
    conversations(
      status: $status
      search: $search
      offset: $offset
      limit: $limit
      group: $group
    ) {
      hasNextPage
      hasPrevPage
      limit
      nextPage
      page
      prevPage
      docs {
        id
        name
        group
        participants {
          userId
          fullName
          nickname
          avatar
          isGuest
          online
        }
        createdAt
        updatedAt
        typing
        usersTyping
        pinnedAt
        blockedBy
        isUnread
        lastMessage {
          id
          text
          from
          to
          type
          attachments {
            type
            attachmentId
            attachment {
              id
              path
              fullUrl
              type
            }
          }
          loading @client
          error @client
          seenBy
          createdAt
          updatedAt
          deletedAt
          callPayload
        }
      }
    }
  }
`,hi=x`
  query conversationDetail($id: ObjectID, $userId: String) @api(name: "chat") {
    conversationDetail(id: $id, userId: $userId) {
      id
      name
      group
      typing
      usersTyping
      isUnread
      participants {
        userId
        fullName
        nickname
        avatar
        isGuest
        online
        roleId
      }
      owner {
        userId
        fullName
      }
      tags {
        id
        name
        color
      }
    }
  }
`,ke=x`
  query messages(
    $conversationId: ObjectID
    $before: ObjectID
    $after: ObjectID
    $userId: String
    $offset: Int
    $limit: Int
  ) @api(name: "chat") {
    messages(
      conversationId: $conversationId
      before: $before
      after: $after
      userId: $userId
      offset: $offset
      limit: $limit
    ) {
      hasNextPage
      hasPrevPage
      limit
      nextPage
      page
      prevPage
      totalDocs
      docs {
        id
        text
        from
        to
        type
        attachments {
          type
          attachmentId
          attachment {
            id
            path
            fullUrl
            type
          }
        }
        conversation {
          id
          participants {
            userId
            avatar
            fullName
          }
        }
        loading @client
        error @client
        seenBy
        createdAt
        updatedAt
        deletedAt
        callPayload
        emojis {
          userId
          emoji
        }
      }
    }
  }
`,mi=x`
  query notes(
    $conversationId: ObjectID
    $userId: String
    $page: Int
    $limit: Int
  ) @api(name: "chat") {
    notes(
      conversationId: $conversationId
      userId: $userId
      page: $page
      limit: $limit
    ) {
      hasNextPage
      hasPrevPage
      limit
      nextPage
      page
      pagingCounter
      prevPage
      totalDocs
      totalPages
      docs {
        id
        content
        createdAt
        createdBy
        owner {
          userId
          fullName
          avatar
        }
      }
    }
  }
`,fi=x`
  query getAttachmentsInConversation(
    $conversationId: ObjectID
    $userId: String
    $offset: Int
    $limit: Int
  ) @api(name: "chat") {
    getAttachmentsInConversation(
      conversationId: $conversationId
      userId: $userId
      offset: $offset
      limit: $limit
    ) {
      hasNextPage
      hasPrevPage
      limit
      nextPage
      page
      pagingCounter
      prevPage
      totalDocs
      totalPages
      docs {
        id
        attachments {
          type
          attachmentId
          attachment {
            id
            path
            fullUrl
            type
          }
        }
      }
    }
  }
`,kt=x`
  query @api(name: "chat") {
    statisticMessageUnread {
      zimians
      student
      customer
      open
      following
      another
      isMarkSeen
    }
  }
`,Ai=x`
  query getListTag($page: Int, $limit: Int) @api(name: "chat") {
    getListTag(page: $page, limit: $limit) {
      docs {
        id
        name
        color
      }
    }
  }
`,xi=x`
  query searchMessage($conversationId: ObjectID!, $search: String!)
  @api(name: "chat") {
    searchMessage(conversationId: $conversationId, search: $search) {
      hasNextPage
      hasPrevPage
      limit
      nextPage
      page
      pagingCounter
      prevPage
      totalDocs
      totalPages
      docs {
        id
        text
        from
        to
        type
        conversation {
          id
          participants {
            userId
            fullName
            avatar
          }
        }
        attachments {
          type
          attachmentId
          attachment {
            id
            path
            fullUrl
            type
          }
        }
        loading @client
        error @client
        seenBy
        createdAt
        updatedAt
        deletedAt
        callPayload
      }
    }
  }
`,wt=x`
  query nearbyMessages($messageId: ObjectID!, $limit: Int) @api(name: "chat") {
    nearbyMessages(messageId: $messageId, limit: $limit) {
      hasNextPage
      hasPrevPage
      limit
      nextPage
      page
      prevPage
      docs {
        id
        text
        from
        to
        type
        attachments {
          type
          attachmentId
          attachment {
            id
            path
            fullUrl
            type
          }
        }
        conversation {
          id
          participants {
            userId
            avatar
            fullName
          }
        }
        loading @client
        error @client
        seenBy
        createdAt
        updatedAt
        deletedAt
        callPayload
      }
    }
  }
`,bi=x`
  query QAPostCategoriesTree @api(name: "zim") {
    QAPostCategoriesTree {
      id
      title
    }
  }
`,fu=x`
  query (
    $school_id: Int
    $q: String
    $name: String
    $description: String
    $status: String
  ) @api(name: "appZim") {
    rooms(
      school_id: $school_id
      q: $q
      name: $name
      description: $description
      status: $status
    ) {
      id
      name
    }
  }
`,Au=x`
  query schools($city_id: Int, $status: String) @api(name: "appZim") {
    schools(city_id: $city_id, status: $status) {
      id
      city {
        id
        name
      }
      district {
        id
        name
      }
      name
      address
      phone
      status
    }
  }
`,xu=x`
  query @api(name: "appZim") {
    cities {
      id
      name
      status
    }
  }
`;x`
  query @api(name: "appZim") {
    cats {
      id
      name
    }
  }
`;const bu=x`
  query subject($name: String, $status: StatusEnum) @api(name: "appZim") {
    subjects(name: $name, status: $status) {
      id
      name
      status
      cats {
        id
        name
        status
        subject {
          id
          name
          status
        }
      }
    }
  }
`,yu=x`
  query levels($subject_id: Int!) @api(name: "appZim") {
    levels(subject_id: $subject_id, status: active) {
      id
      name
      status
      graduation
      subject_id
    }
  }
`;x`
  query shifts($q: String) @api(name: "appZim") {
    shifts(q: $q, status: "active", shift_minute: 0) {
      id
      start_time
      end_time
      status
      shift_minute
    }
  }
`;x`
  query getStatusAccount @api(name: "appZim") {
    getStatusAccount {
      key
      value
    }
  }
`;x`
  query getTypeOfEducation @api(name: "appZim") {
    getTypeOfEducation {
      key
      value
    }
  }
`;x`
  query getAcademicPurposesAll @api(name: "appZim") {
    getAcademicPurposesAll {
      id
      academicPurposesName
    }
  }
`;const Cu=x`
  query getEcs @api(name: "appZim") {
    getEcs {
      id
      user_name
      full_name
      phone
      address
      email
      role_id
      role_name
      status
      status_online
      avatar
      supporter
    }
  }
`,Eu=x`
  query getJobs @api(name: "appZim") {
    getJobs {
      id
      jobName
    }
  }
`,yi=x`
  query getSourceOfCustomer @api(name: "appZim") {
    getSourceOfCustomer {
      id
      sourceOfCustomer
    }
  }
`,$u=x`
  query districts($city_id: Int!, $streetName: String, $status: StatusEnum)
  @api(name: "appZim") {
    districts(city_id: $city_id, name: $streetName, status: $status) {
      id
      name
      status
    }
  }
`,Su=x`
  query getStreets($districtid: Int!, $streetName: String, $isHide: Boolean)
  @api(name: "appZim") {
    getStreets(
      districtid: $districtid
      streetName: $streetName
      isHide: $isHide
    ) {
      id
      streetName
    }
  }
`,vu=x`
  query getWards($districtid: Int!, $wardName: String, $isHide: Boolean)
  @api(name: "appZim") {
    getWards(districtid: $districtid, wardName: $wardName, isHide: $isHide) {
      id
      wardName
    }
  }
`;x`
  mutation createProductCategory($input: ProductCategoryInput!)
  @api(name: "zim") {
    createProductCategory(input: $input) {
      id
      title
      slug
    }
  }
`;x`
  mutation updateProductCategory(
    $id: ObjectID!
    $input: UpdateProductCategoryInput!
  ) @api(name: "zim") {
    updateProductCategory(id: $id, input: $input) {
      id
      title
    }
  }
`;x`
  mutation deleteProductCategory($id: ObjectID!) @api(name: "zim") {
    deleteProductCategory(id: $id)
  }
`;x`
  mutation ($name: String!, $type: ProductAttributeType!, $options: [String])
  @api(name: "zim") {
    createProductAttribute(name: $name, type: $type, options: $options) {
      id
      type
      name
      options
    }
  }
`;x`
  mutation (
    $id: ObjectID!
    $name: String
    $type: ProductAttributeType
    $options: [String]
  ) @api(name: "zim") {
    updateProductAttribute(
      id: $id
      name: $name
      type: $type
      options: $options
    ) {
      id
      type
      name
      options
    }
  }
`;x`
  mutation (
    $type: ProductType!
    $sku: String!
    $description: String!
    $shortDescription: String!
    $name: String!
    $featured: Boolean
    $categories: [ObjectID]
    $images: [ObjectID]
    $attributes: [ProductAttributeInput]
    $tags: [ObjectID]
    $variationAttributes: [ObjectID]
    $regularPrice: Int
    $salePrice: Int
    $salePriceFrom: DateTime
    $salePriceTo: DateTime
    $downloads: [ObjectID]
    $downloadable: Boolean
  ) @api(name: "zim") {
    createProduct(
      input: {
        type: $type
        sku: $sku
        description: $description
        shortDescription: $shortDescription
        name: $name
        featured: $featured
        categories: $categories
        images: $images
        attributes: $attributes
        tags: $tags
        variationAttributes: $variationAttributes
        regularPrice: $regularPrice
        salePrice: $salePrice
        salePriceFrom: $salePriceFrom
        salePriceTo: $salePriceTo
        downloads: $downloads
        downloadable: $downloadable
      }
    ) {
      ...ProductBaseField
      ...ProductDetailFields
    }
  }
  ${gt}
`;x`
  mutation (
    $id: ObjectID!
    $description: String!
    $shortDescription: String!
    $name: String!
    $featured: Boolean
    $categories: [ObjectID]
    $images: [ObjectID]
    $attributes: [ProductAttributeInput]
    $tags: [ObjectID]
    $regularPrice: Int
    $salePrice: Int
    $salePriceFrom: DateTime
    $salePriceTo: DateTime
    $downloads: [ObjectID]
    $downloadable: Boolean
    $variationAttributes: [ObjectID]
    $slug: String
    $sku: String
  ) @api(name: "zim") {
    updateProduct(
      id: $id
      input: {
        description: $description
        shortDescription: $shortDescription
        name: $name
        featured: $featured
        categories: $categories
        images: $images
        attributes: $attributes
        tags: $tags
        regularPrice: $regularPrice
        salePrice: $salePrice
        salePriceFrom: $salePriceFrom
        salePriceTo: $salePriceTo
        downloads: $downloads
        downloadable: $downloadable
        variationAttributes: $variationAttributes
        slug: $slug
        sku: $sku
      }
    ) {
      ...ProductBaseField
      ...ProductDetailFields
    }
  }
  ${gt}
`;x`
  mutation (
    $id: ObjectID!
    $description: String
    $shortDescription: String
    $name: String
    $categories: [ObjectID]
    $images: [ObjectID]
    $attributes: [ProductAttributeInput]
    $tags: [ObjectID]
    $regularPrice: Int
    $salePrice: Int
    $salePriceFrom: DateTime
    $salePriceTo: DateTime
    $downloads: [ObjectID]
    $downloadable: Boolean
    $slug: String
    $sku: String
  ) @api(name: "zim") {
    updateProductVariants(
      id: $id
      input: {
        description: $description
        shortDescription: $shortDescription
        name: $name
        categories: $categories
        images: $images
        attributes: $attributes
        tags: $tags
        regularPrice: $regularPrice
        salePrice: $salePrice
        salePriceFrom: $salePriceFrom
        salePriceTo: $salePriceTo
        downloads: $downloads
        downloadable: $downloadable
        slug: $slug
        sku: $sku
      }
    ) {
      ...ProductBaseField
      ...ProductDetailFields
    }
  }
  ${gt}
`;x`
  mutation ($parentProduct: ObjectID!, $input: AddVariantProductInput!)
  @api(name: "zim") {
    createProductVariants(parentProduct: $parentProduct, input: $input) {
      ...ProductBaseField
      ...ProductDetailFields
    }
  }
  ${gt}
`;x`
  mutation ($id: ObjectID!) @api(name: "zim") {
    publishProduct(id: $id) {
      ...ProductBaseField
    }
  }
  ${je}
`;x`
  mutation ($id: ObjectID!) @api(name: "zim") {
    unPublishProduct(id: $id) {
      ...ProductBaseField
    }
  }
  ${je}
`;x`
  mutation ($productId: ObjectID!, $inventories: [InventoryInput]!)
  @api(name: "zim") {
    bulkUpdateProductInventory(
      productId: $productId
      inventories: $inventories
    ) {
      id
      inStock
      inventories {
        id
        inStock
        inventory
        reserved
        manageStock
        store {
          id
          name
        }
      }
    }
  }
`;x`
  mutation ($id: ObjectID!) @api(name: "zim") {
    removeProductVariants(id: $id) {
      ...ProductBaseField
    }
  }
  ${je}
`;x`
  fragment RecruitmentPostBaseField on RecruitmentPost {
    id
    title
    area
    status
    position
    currentNumOfReplies
    maxCandidates
    startDate
    endDate
    createdAt
    updatedAt
    slug
  }
`;const Ci=x`
  fragment RecruitmentPostDetailsField on RecruitmentPost {
    id
    title
    area
    position
    numOfCandidates
    maxCandidates
    jobDescription
    currentNumOfReplies
    salaryFrom
    slug
    salaryType
    salaryTo
    status
    endDate
    createdAt
    updatedAt
    startDate
    seo {
      title
      description
      ogDescription
      ogImage {
        id
        path
      }
      ogTitle
      publisher
      noIndex
      noFollow
      canonicalUrl
      customHeadHtml
    }
  }
`;x`
  mutation ($postId: ObjectID!, $seo: PostSeoInput!) @api(name: "zim") {
    updatePostSeoConfig(postId: $postId, seo: $seo) {
      id
      title
      slug
      seo {
        title
        description
        ogDescription
        ogImage {
          id
          path
        }
        ogTitle
        publisher
        noIndex
        noFollow
        canonicalUrl
        customHeadHtml
      }
      createdAt
      updatedAt
    }
  }
`;x`
  mutation ($productId: ObjectID!, $seo: PostSeoInput!) @api(name: "zim") {
    updateProductSeoConfig(productId: $productId, seo: $seo) {
      ... on SimpleProduct {
        id
        createdAt
        updatedAt
        seo {
          title
          description
          ogDescription
          ogImage {
            id
            path
          }
          ogTitle
          publisher
          noIndex
          noFollow
          canonicalUrl
          customHeadHtml
        }
      }
      ... on VariableProduct {
        id
        createdAt
        updatedAt
        seo {
          title
          description
          ogDescription
          ogImage {
            id
            path
          }
          ogTitle
          publisher
          noIndex
          noFollow
          canonicalUrl
          customHeadHtml
        }
      }
    }
  }
`;x`
  mutation ($recruitmentPostId: ObjectID!, $seo: CommonSeoInput!)
  @api(name: "zim") {
    updateRecruitmentPostSeoConfig(
      recruitmentPostId: $recruitmentPostId
      seo: $seo
    ) {
      ...RecruitmentPostDetailsField
    }
  }
  ${Ci}
`;x`
  query redirectUrlPagination($page: Int, $limit: Int, $search: String)
  @api(name: "zim") {
    redirectUrlPagination(page: $page, limit: $limit, search: $search) {
      docs {
        id
        prevUrl
        newUrl
        redirectCode
        createdAt
        updatedAt
      }
      totalPages
      totalDocs
    }
  }
`;x`
  mutation updateRedirectUrl($id: ObjectID!, $input: UpdateRedirectUrlInput!)
  @api(name: "zim") {
    updateRedirectUrl(id: $id, input: $input) {
      id
      prevUrl
      newUrl
      redirectCode
      createdAt
      updatedAt
    }
  }
`;x`
  mutation deleteRedirectUrl($id: ObjectID!) @api(name: "zim") {
    deleteRedirectUrl(id: $id)
  }
`;x`
  mutation createRedirectUrl($input: CreateRedirectUrlInput!)
  @api(name: "zim") {
    createRedirectUrl(input: $input) {
      id
      prevUrl
      newUrl
      redirectCode
      createdAt
      updatedAt
    }
  }
`;function Wa(e){const n=e+"=",r=decodeURIComponent(document.cookie).split(";");for(let s=0;s<r.length;s++){let o=r[s];for(;o.charAt(0)===" ";)o=o.substring(1);if(o.indexOf(n)===0)return o.substring(n.length,o.length)}return""}const Ei=`
    mutation {
        refreshToken {
            token
            refreshToken
        }
    }
`,$i=e=>{},Si=()=>{},Ue=()=>Wa("token"),vi=jo(({graphQLErrors:e,networkError:n})=>{e&&e.map(({message:a,locations:r,path:s})=>console.log(`[GraphQL error]: Message: ${a}, Location: ${r}, Path: ${s}`)),n&&console.log(`[Network error]: ${n}`)}),Ii=new Nt((e,n)=>new Uo(a=>{let r;return Promise.resolve(e).then(s=>{s.setContext({headers:{authorization:`Bearer ${Ue()}`,"Access-Control-Allow-Credentials":!0,"x-app-token":"0aAUfvzhIZVSDIF0yBk53U8Vokij6xv7"}})}).then(()=>{r=n(e).subscribe({next:a.next.bind(a),error:a.error.bind(a),complete:a.complete.bind(a)})}).catch(a.error.bind(a)),()=>{r&&r.unsubscribe()}})),ki=new Go({possibleTypes:{BaseProduct:["VariableProduct","SimpleProduct"]},typePolicies:{Participant:{keyFields:["userId"]},AccountStudentInfoType:{keyFields:(e,n)=>`${e.source_name}-${e.district_name}-${e.job_name}-${e.ward_name}-${e.city_name}-${e.street_name}-${e.birthday}`},AccountType:{keyFields:(e,n)=>`${e.id}-${e.full_name}-${e.phone}`},Message:{fields:{loading:{read:()=>!1},error:{read:()=>!1}}},Query:{fields:{getAccountChatPagination:{keyArgs:["input",["q","limit","roles"]]},messages:{keyArgs:["conversationId","userId","offset","before","after"]},supportConversations:{keyArgs:["status"]},conversations:{keyArgs:["group"]},medias:Bt(["type","width","height","search"]),tags:Bt(["search"]),comments:Bt(["type","refId","parent"])}}}});global.wsClients={};const wi=e=>{const n=new Xo(e,{reconnect:!0,connectionParams:()=>({authToken:Ue()})});return global.wsClients[e]=n,n},Ti=e=>new Qo(wi(e)),Qa=new Ho({endpoints:{zim:"https://graph-api.zim.vn",appZim:"https://graphnet-api.zim.vn",analytics:{}.REACT_APP_ANALYTICS_API_ENDPOINT,chat:"https://chat.zim.vn",crawler:"https://crawler.zim.vn"},createHttpLink:()=>Vo({credentials:"include",fetch:Wo(na,(e,n,a)=>v(b({},e),{withCredentials:!0,onUploadProgress:a.onUploadProgress}))}),wsSuffix:"/graphql",createWsLink:e=>Ti(e)}),Ya=async()=>{const e={operationName:null,variables:{},query:Ei};return fetch("https://graph-api.zim.vn/graphql",{method:"POST",credentials:"include",body:JSON.stringify(e),headers:{"Access-Control-Request-Headers":"content-type","Content-Type":"application/json; charset=utf-8"}}).then(async n=>{const a=await n.json();return a==null?void 0:a.data.refreshToken})},Za=new Yo({accessTokenField:"token",isTokenValidOrUndefined:()=>{const e=Ue();if(e.length===0)return!0;try{const{exp:n}=aa(e);return Date.now()<n*1e3}catch{return!1}},fetchAccessToken:Ya,handleFetch:e=>{e?localStorage.setItem("isAuthenticated","true"):localStorage.removeItem("isAuthenticated")},handleResponse:()=>{},handleError:e=>{console.log(`handleError: ${e}`)}}),Pi=Nt.from([Za,Ii,vi,Qa]),_i=Zo(({query:e})=>{const n=Ko(e);return n.kind==="OperationDefinition"&&n.operation==="subscription"},Nt.from([Za,Qa]),Pi),ht=new Jo({ssrMode:typeof window=="undefined",link:_i,credentials:"include",cache:ki,connectToDevTools:!0}),tn=c.exports.createContext({appState:{isAuthenticated:!1,user:null,loaded:!1}}),Mi=({children:e})=>{const[n,a]=c.exports.useState({isAuthenticated:!1,user:null,loaded:!1,buildMode:"module"}),r=async()=>{var l,i;try{const d=await ht.query({query:li,fetchPolicy:"network-only"});((l=d==null?void 0:d.data)==null?void 0:l.me)?(a(v(b({},n),{user:(i=d==null?void 0:d.data)==null?void 0:i.me,isAuthenticated:!0,loaded:!0})),localStorage.setItem("isAuthenticated","true")):(localStorage.removeItem("isAuthenticated"),a(v(b({},n),{user:null,isAuthenticated:!1,loaded:!0})))}catch{a(v(b({},n),{user:null,isAuthenticated:!1,loaded:!0})),localStorage.removeItem("isAuthenticated")}},s=async l=>{await r()},o=async()=>{await ht.mutate({mutation:ci}),a(v(b({},n),{user:null,isAuthenticated:!1,loaded:!0})),localStorage.removeItem("isAuthenticated")};return t(tn.Provider,{value:{getUser:r,appState:n,setAppState:a,appSetLogin:s,appSetLogout:o,appSetAuthToken:$i,appClearAuthToken:Si},children:t(er,{client:ht,children:e})})};function qi(){return c.exports.useContext(tn)}const Oi=()=>{const e=c.exports.useRef(!1);return c.exports.useEffect(()=>(e.current=!0,()=>{e.current=!1}),[]),c.exports.useCallback(()=>e.current,[])},me={home:"/",listCourses:"/courses",courseDetail:"/courses/:id",forbiddenError:"/error/403",ticket:"/ticket",chat:"/chat",report:"/report",leadReport:"/report/lead",studentSupportByEc:"/user/student-support-by-ec",scheduleRegistration:"/schedule/schedule-registration",listSchedules:"/schedule/list-schedules",registerTestSchedule:"/schedule/test/reg",lead:"/web-data/lead",paperBasedTestBank:"/paper-based-test-bank",mockTestOnline:"/mock-test/mock-test-online",mockTestOffline:"/mock-test/mock-test-offline",mockTestDetail:"/mock-test/mock-test-detail"},Iu={lead:"/Admin/ContactCustomer/ContactList",customerInfo:"/Admin/Customer/CustomerDetai",staffInfo:"/Admin/Staff/StaffDetail",courseDetail:"/Admin/CourseManage/CourseDetail",ticketList:"/Admin/Ticket",customerList:"/Admin/Customer/CustomersList",listStudentsMockTest:"/Admin/MockTest/ListStudent",mockTestDetail:"/Admin/MockTest/MockTest",ieltsCorrecting:"/Admin/Ieltscorrect/Correcting",newsFeed:"/news-feed"},ku={testResult:"/test/result"},Ka=(e,n,a=window)=>{const r=c.exports.useRef();c.exports.useEffect(()=>{r.current=n},[n]),c.exports.useEffect(()=>{if(!(a&&a.addEventListener))return;const o=l=>r&&typeof r.current=="function"?r.current(l):null;return typeof o=="function"&&a.addEventListener(e,o),()=>{typeof o=="function"&&a.removeEventListener(e,o)}},[e,a])},Fi=(...e)=>{const n=c.exports.useRef();return c.exports.useEffect(()=>{e.forEach(a=>{!a||(typeof a=="function"?a(n.current):a.current=n.current)})},[e]),n},ve=c.exports.forwardRef((r,a)=>{var s=r,{error:e}=s,n=$e(s,["error"]);return h(bt,{isInvalid:!!e,children:[(n==null?void 0:n.label)&&t(Lt,{htmlFor:n.id,children:n.label}),t(yt,v(b({bg:k("white","slate.700"),borderColor:k("gray.300","slate.600"),_hover:{borderColor:k("gray.400","slate.700")},color:k("gray.900","slate.300"),_placeholder:{color:k("gray.400","slate.500")}},n),{ref:a})),e&&t(Ct,{children:e&&(e==null?void 0:e.message)})]})}),wu=c.exports.forwardRef((s,r)=>{var o=s,{placeholder:e,onSubmitSearch:n}=o,a=$e(o,["placeholder","onSubmitSearch"]);const l=c.exports.useRef(null),i=Fi(r,l);return t(P,{as:"form",onSubmit:f=>{f.preventDefault();const{value:m}=i.current;typeof n=="function"&&n(m)},id:"form-header-search",w:"full",children:h(oa,{flexGrow:1,w:"100%",children:[t(ra,{children:t(Y,{p:0,variant:"ghost",_hover:{backgroundColor:"transparent"},type:"submit",children:t(tr,{fontSize:18})})}),t(ve,v(b({placeholder:"T\xECm ki\u1EBFm...",variant:"filled"},a),{ref:i,pr:10}))]})})}),Ja=x`
  subscription ($conversationId: ObjectID, $status: String) @api(name: "chat") {
    conversationUpdated(conversationId: $conversationId, status: $status) {
      id
      name
      isDeleteMessage
      group
      status
      createdAt
      updatedAt
      pinnedAt
      blockedBy
      isUnread
      typing
      usersTyping
      participants {
        userId
        fullName
        nickname
        avatar
        isGuest
        online
      }
      lastMessage {
        id
        text
        from
        to
        type
        attachments {
          type
          attachmentId
          attachment {
            id
            path
            fullUrl
            type
          }
        }
        conversation {
          id
          participants {
            userId
            avatar
            fullName
          }
        }
        loading @client
        error @client
        seenBy
        createdAt
        updatedAt
        deletedAt
        callPayload
        emojis {
          userId
          emoji
        }
      }
    }
  }
`,Ni=x`
  subscription @api(name: "chat") {
    messageAdded {
      zimians
      student
      customer
      open
      following
      another
      isMarkSeen
    }
  }
`;function ae(){const{appState:{user:e},setUser:n}=c.exports.useContext(tn);return[e,n]}var Bi="/assets/notificationSound.mp3";const Li=()=>{const[e]=ae(),n=new Audio(Bi);n.volume=1;const{data:a,loading:r}=ge(kt),s=a==null?void 0:a.statisticMessageUnread;return Rt(Ni,{onSubscriptionData:({client:o,subscriptionData:l})=>{var d,f;const i=o.cache.readQuery({query:kt});if(l.data){const{messageAdded:m}=l.data;m?(o.writeQuery({query:kt,data:v(b({},i),{statisticMessageUnread:b(b({},i.statisticMessageUnread),m)})}),((f=(d=l==null?void 0:l.data)==null?void 0:d.messageAdded)==null?void 0:f.isMarkSeen)||n.play()):o.writeQuery({query:kt,data:b({},i)})}},shouldResubscribe:!!e}),{listMessageUnreadOverall:s,loading:r}},Tu={editor:[1,2,11],approve:[1,8],author:[1,8,11,4],manager:[1,2,12,8,11],productManager:[1,2],censor:[1,2,8],canEdit:[1,2,4,8,11],canConfigSeo:[1,2,11],admin:[1],adminAndCC:[1,2],canCreateTicket:[5,6,12,1,2,11,12],canCancelSchedule:[2],canEnterMockTestScores:[4],canUploadMockTestTask:[6],canCreateOrEditMockTest:[1,2],assignTeacherAndEC:[1,2],viewTableMockTestDetail:[1,2,6],canBookSchedule:[6]},nn=[{id:1,name:"Super Admin"},{id:2,name:"CC"},{id:4,name:"Gi\xE1o vi\xEAn"},{id:5,name:"Student"},{id:6,name:"EC"},{id:8,name:"AM"},{id:9,name:"K\u1EBF to\xE1n"},{id:11,name:"Marketer"},{id:12,name:"BM"},{id:13,name:"BP"},{id:14,name:"Guest"}],Ri=[5],Di=[1,6],zi={apiUrl:"https://app.zim.vn",apiVersion:"v1",access_token:"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJqc29uX3dlYl90b2tlbiIsImp0aSI6ImM5ZDFmNzA3LTk4ZmYtNGNmMC1iMGUyLTAwN2RhY2MzYTVhNSIsImlhdCI6IjEvNC8yMDIyIDM6MDE6MzIgUE0iLCJpZCI6IjM0NDg4IiwidXNlcl9uYW1lIjoiemltIiwiZnVsbF9uYW1lIjoiWklNIEFjYWRlbXkiLCJyb2xlX2lkIjoiMSIsInJvbGVfbmFtZSI6IlN1cGVyIEFkbWluIiwiZXhwIjoxNjY3NTc0MDkyLCJpc3MiOiJhcHAuemltLnZuIiwiYXVkIjoiYXBwLnppbS52biJ9.RPOVJ-aana5NoeyUEZ_WYI_i0f2gBqU0o1W68WO51Ro"},ji=()=>{var f;const[e]=ae(),n=e.roleId,{listMessageUnreadOverall:a}=Li(),r=(a==null?void 0:a.another)?Object==null?void 0:Object.values(JSON==null?void 0:JSON.parse(a==null?void 0:a.another)):[],s=[a==null?void 0:a.zimians,a==null?void 0:a.student,a==null?void 0:a.following,a==null?void 0:a.open,(f=r==null?void 0:r.map(m=>parseInt(m,10)))==null?void 0:f.reduce((m,u)=>m+u,0)],o=s[1]+s[4],l=s[0]+s[1]+s[4],i=s==null?void 0:s.reduce((m,u)=>m+u,0),d=Ri.includes(n)?o:Di.includes(n)?i:l;return c.exports.useEffect(()=>{const m=document.getElementById("noti-chat");m&&(m.innerHTML=`${d}`)},[d]),{overall:s,overallMyChats:d}},Ui=({searchString:e,setSearchString:n})=>h(P,{flex:1,maxH:"35px",borderWidth:1,borderColor:"#e4e4e4",rounded:"full",display:"flex",flexDirection:"row",alignItems:"center",px:2,overflow:"hidden",bg:"white",children:[t(yt,{variant:"unstyled",placeholder:"T\xECm ki\u1EBFm",p:2,value:e,onChange:r=>{r.target&&n(r.target.value)},color:"#373737"}),t(nr,{className:"h-5 w-5 text-gray-400"})]}),Pu=e=>`${e} is required.`,Gi="00000000-0000-0000-0000-000000000000",_u={0:{short:"CN",long:"CN"},1:{short:"2",long:"T2"},2:{short:"3",long:"T3"},3:{short:"4",long:"T4"},4:{short:"5",long:"T5"},5:{short:"6",long:"T6"},6:{short:"7",long:"T7"}},Mu={LISTENING:{short:"L",long:"Listening"},READING:{short:"R",long:"Reading"},SPEAKING:{short:"S",long:"Speaking"},WRITING:{short:"W",long:"Writing"},"WRITING TASK 1":{short:"WT1",long:"WT1"},"WRITING TASK 2":{short:"WT2",long:"WT2"}},an={ALL:"all",INCOMING:"incoming",ONGOING:"ongoing",CLOSED:"closed"},qu={ALL:{label:"All",colorScheme:"gray"},INCOMING:{label:"Incoming",colorScheme:"green"},ONGOING:{label:"Ongoing",colorScheme:"orange"},FINISHED:{label:"Finished",colorScheme:"red"}},Ou=[{label:"Incoming",value:an.INCOMING},{label:"Ongoing",value:an.ONGOING},{label:"Closed",value:an.CLOSED}],Fu=[{id:1,label:"Th\u1EE9 Hai",value:1},{id:2,label:"Th\u1EE9 Ba",value:1},{id:3,label:"Th\u1EE9 T\u01B0",value:3},{id:4,label:"Th\u1EE9 N\u0103m",value:4},{id:5,label:"Th\u1EE9 S\xE1u",value:5},{id:6,label:"Th\u1EE9 B\u1EA3y",value:6},{id:0,label:"Ch\u1EE7 nh\u1EADt",value:7}];var Je;(function(e){e.PROCESSING="Processing",e.CLOSED="Closed",e.WAITING="Waiting",e.DELETED="Deleted",e.LONGTIME="Longtime"})(Je||(Je={}));var Tt;(function(e){e.send="send",e.received="received"})(Tt||(Tt={}));var Pt;(function(e){e.active="active",e.deactivated="deactivated"})(Pt||(Pt={}));const Nu={All:{label:"All",value:"All",colorScheme:"gray"},Processing:{label:"Processing",value:"Processing",colorScheme:"green"},Waiting:{label:"Waiting",value:"Waiting",colorScheme:"orange"},Closed:{label:"Closed",value:"Closed",colorScheme:"red"},Deleted:{label:"Deleted",value:"Deleted",colorScheme:"black"}},Bu={all:{label:"All",value:"All",colorScheme:"gray"},processing:{label:"Processing",value:"Processing",colorScheme:"green"},waiting:{label:"Waiting",value:"Waiting",colorScheme:"orange"},closed:{label:"Closed",value:"Closed",colorScheme:"red"},deleted:{label:"Deleted",value:"Deleted",colorScheme:"black"},longtime:{label:"Longtime",value:"Longtime",colorScheme:"gray"}},Lu={0:{label:"Ch\u01B0a x\u1EED l\xFD",value:0,colorScheme:"red",filterValue:"pending"},1:{label:"\u0110ang theo d\xF5i",value:1,colorScheme:"orange",filterValue:"following"},2:{label:"\u0110\xE3 h\u1EB9n test",value:2,colorScheme:"blue",filterValue:"appoiment"},4:{label:"\u0110\xE3 \u0111\u1EBFn test",value:4,colorScheme:"teal",filterValue:"called"},6:{label:"\u0110\xE3 \u0111\u0103ng k\xFD",value:6,colorScheme:"green",filterValue:"registered"},3:{label:"\u0110\xE3 h\u1EE7y",value:3,colorScheme:"gray",filterValue:"cancelled"},5:{label:"\u0110\xE3 \u0111\u1ED5i EC",value:5,colorScheme:"purple",filterValue:"changeEc"}},Ru=[{label:"Processing",value:Je.PROCESSING},{label:"Waiting",value:Je.WAITING},{label:"Closed",value:Je.CLOSED},{label:"Deleted",value:Je.DELETED}],Du=[{label:"\u0110\xE3 g\u1EEDi",value:Tt.send},{label:"\u0110\xE3 nh\u1EADn",value:Tt.received}],zu=[{label:"Active",value:Pt.active},{label:"Deactivated",value:Pt.deactivated}];var Xa;(function(e){e.Other="Other",e.TeacherOff="TeacherOff",e.MOCK_TEST_ONLINE="MOCK_TEST_ONLINE",e.MOCK_TEST_OFFLINE="MOCK_TEST_OFFLINE",e.OTHER="OTHER",e.TEACHER_OFF="TEACHER_OFF"})(Xa||(Xa={}));const ju=[{id:1,label:"3 bu\u1ED5i/tu\u1EA7n"},{id:2,label:"4-6 bu\u1ED5i/tu\u1EA7n"},{id:3,label:"Tr\xEAn 6 bu\u1ED5i/tu\u1EA7n"}],Hi="https://app.zim.vn/app-assets/zimv2/images/default-logo1.png",Uu={inactive:{label:"inactive",value:"inactive",colorScheme:"gray"},active:{label:"active",value:"active",colorScheme:"green"},deactivated:{label:"deactivated",value:"deactivated",colorScheme:"red"}},Gu=[{label:"Active",value:"active"},{label:"Deactivated",value:"deactivated"}],Hu={"Listening-Reading-Writing":{long:"Listening-Reading-Writing",short:"L-R-W"},Speaking:{long:"Speaking",short:"Speaking"}};var Fe;(function(e){e.confirmed="confirmed",e.cancel="cancel",e.pending="pending"})(Fe||(Fe={}));Fe.confirmed,Fe.cancel,Fe.pending;Fe.confirmed,Fe.cancel,Fe.pending;const Vu=[{label:"5 rows",value:5},{label:"10 rows",value:10},{label:"15 rows",value:15},{label:"20 rows",value:20},{label:"25 rows",value:25},{label:"30 rows",value:30}],Ie=c.exports.memo(r=>{var s=r,{src:e="",name:n=""}=s,a=$e(s,["src","name"]);return t(sa,b({src:e===Hi?"":e,name:n,zIndex:10},a))}),Vi=({searchString:e,setSearchString:n})=>{const[a]=ae();return h(te,{w:"100%",p:{base:3},position:"sticky",top:0,zIndex:50,bg:k("white","#10172a"),alignItems:"stretch",children:[h(P,{w:"100%",display:"flex",alignItems:"start",maxW:{base:"full"},className:" space-x-2.5",bg:k("white",""),p:{base:2,md:0},rounded:"lg",children:[t(Ie,{boxSize:"2em",src:a==null?void 0:a.avatar,name:a==null?void 0:a.fullName}),h(P,{flex:1,children:[t(L,{noOfLines:1,fontSize:14,lineHeight:"18px",fontWeight:"medium",color:k("gray.800","slate.300"),children:a==null?void 0:a.fullName}),t(L,{display:{base:"block"},noOfLines:1,fontSize:14,fontWeight:500,lineHeight:"24px",color:"gray.400",children:a==null?void 0:a.role})]})]}),t(R,{children:t(Ui,{searchString:e,setSearchString:n})})]})},Wi=He.memo(function({avatarUrl:n,visibleLastOnline:a,lastOnline:r,username:s}){return t(Ie,{zIndex:"10",boxSize:"2em",src:n,name:s,children:a&&t(ia,{boxSize:"15px",bg:r==="online"?"green.500":"gray.300"})})}),mt=c.exports.memo(e=>{const p=e,{onClick:n,avatarUrl:a,username:r,usernameClassName:s,positionName:o,lastOnline:l,visibleLastOnline:i=!0,isOwner:d,channel:f,id:m}=p,u=$e(p,["onClick","avatarUrl","username","usernameClassName","positionName","lastOnline","visibleLastOnline","isOwner","channel","id"]),$=c.exports.useCallback(()=>{n(m,f)},[f,m]);return h(P,v(b({w:"100%",display:"flex",alignItems:"start",cursor:"pointer",onClick:$,maxW:{base:"full"},className:" space-x-2.5",bg:k("white",""),p:3,rounded:"lg"},u),{children:[t(Wi,{avatarUrl:a,lastOnline:l,visibleLastOnline:i,username:r}),h(P,{flex:1,children:[h(R,{children:[t(L,{noOfLines:1,fontSize:14,lineHeight:"18px",fontWeight:"medium",color:k("gray.800","slate.300"),className:`${s}`,children:r}),d&&t(ar,{})]}),t(L,{noOfLines:1,fontSize:12,lineHeight:"24px",color:"gray.400",children:o})]})]}))}),eo=()=>h(R,{px:3,py:2,borderBottomWidth:1,bg:k("white","#1e293b50"),children:[t(or,{size:"10"}),t(rr,{flex:1,noOfLines:2,spacing:"2",skeletonHeight:5})]}),to=c.exports.memo(({conversations:e,onClick:n,isLoading:a,canLoadMore:r,onLoadMore:s,loading:o})=>{const l=c.exports.useMemo(()=>e==null?void 0:e.map((i,d)=>{const f=i==null?void 0:i.user,m=(f==null?void 0:f.role_id)===5?"student":(f==null?void 0:f.role_id)===14?"customer":"zimians";return o?t(eo,{},`loading_${d}`):c.exports.createElement(mt,v(b({},f),{lastOnline:f==null?void 0:f.status_online,avatarUrl:f==null?void 0:f.avatar,username:f==null?void 0:f.full_name,positionName:f==null?void 0:f.role_name,key:d,onClick:n,channel:m}))}),[e]);return h(te,{alignItems:"stretch",children:[l,r&&t(Y,{isLoading:a,loadingText:"\u0110ang t\u1EA3i th\xEAm",onClick:s,w:"100%",rounded:"none",borderWidth:"1px 0px 0px 0px",bg:k("white","slate.600"),fontSize:14,fontWeight:400,justifyContent:"center",children:"Xem th\xEAm"})]})}),Qi=({title:e,onClick:n,children:a,overallMessageUnRead:r})=>h(la,{borderBottom:1,borderColor:"",children:[h(R,{position:"relative",spacing:0,pl:4,_hover:{backgroundColor:k("#f0f0f0","slate.600")},children:[t(P,{onClick:n,display:"flex",justifyContent:"start",flex:1,as:"button",children:t(L,{color:k("#444","white"),fontSize:16,fontWeight:"medium",children:e})}),t(ca,{display:"flex",alignItems:"center",justifyContent:"center",w:10,children:t(da,{})}),r&&t(gn,{count:r})]}),t(ua,{p:0,children:a})]}),no=c.exports.memo(({title:e="Accordion",children:n,onClick:a,overallMessageUnRead:r})=>t(pa,{defaultIndex:[0],allowMultiple:!0,children:t(Qi,{overallMessageUnRead:r,title:e,onClick:a,children:n})}));function on(...e){const n=a=>a&&typeof a=="object";return e.reduce((a,r)=>(Object.keys(r).forEach(s=>{const o=a[s],l=r[s];Array.isArray(o)&&Array.isArray(l)?a[s]=Et(o.concat(...l),"id"):n(o)&&n(l)?a[s]=on(o,l):a[s]=l}),a),{})}function ao(...e){const n=a=>a&&typeof a=="object";return e.reduce((a,r)=>(Object.keys(r).forEach(s=>{const o=a[s],l=r[s];Array.isArray(o)&&Array.isArray(l)?a[s]=Et(o.concat(...l),"user"):n(o)&&n(l)?a[s]=ao(o,l):a[s]=l}),a),{})}const Ge=({hasNextPage:e,fetchMore:n,variables:a={},isNormalLoadMore:r=!0})=>{const[s,o]=c.exports.useState(!1),l=r?on:ao,i=c.exports.useCallback(async()=>{s||!e||(await o(!0),await n({variables:a,updateQuery:(d,{fetchMoreResult:f})=>f?l(d,f):d}),await o(!1))},[s,e,o,n,a]);return c.exports.useMemo(()=>({onLoadMore:i,isLoadingMore:s}),[i,s])},oo=({roles:e,searchString:n})=>{var u,$,p;const a=se({roles:e,searchString:n,limit:10,page:1},A=>A),{data:r,loading:s,fetchMore:o}=ge(Ha,{variables:a}),l=(u=r==null?void 0:r.getAccountChatPagination)==null?void 0:u.docs,i=($=r==null?void 0:r.getAccountChatPagination)==null?void 0:$.page,d=(p=r==null?void 0:r.getAccountChatPagination)==null?void 0:p.hasNextPage,{onLoadMore:f,isLoadingMore:m}=Ge({variables:v(b({},a),{page:i+1}),fetchMore:o,hasNextPage:d,isNormalLoadMore:!1});return c.exports.useMemo(()=>({listAccount:l,loading:s,onLoadMore:f,hasNextPage:d,isLoadingMore:m}),[l,s,f,d,m])},Yi=x`
  mutation sendMessage(
    $recipient: RecipientInput!
    $message: MessageInput
    $senderAction: SenderAction
  ) @api(name: "chat") {
    sendMessage(
      recipient: $recipient
      message: $message
      senderAction: $senderAction
    ) {
      id
      createdAt
      updatedAt
      lastMessage {
        id
        text
        from
        to
        type
        attachments {
          type
          attachmentId
          attachment {
            id
            path
            fullUrl
            type
          }
        }
        loading @client
        error @client
        seenBy
        createdAt
        updatedAt
        deletedAt
        callPayload
      }
    }
  }
`,Zi=x`
  mutation removeMessage($id: ObjectID!) @api(name: "chat") {
    removeMessage(id: $id) {
      id
      name
      createdAt
      updatedAt
      isDeleteMessage
      lastMessage {
        id
        text
        from
        to
        type
        attachments {
          type
          attachmentId
          attachment {
            id
            path
            fullUrl
            type
          }
        }
        loading @client
        error @client
        seenBy
        createdAt
        updatedAt
        deletedAt
        callPayload
      }
    }
  }
`,Ki=x`
  mutation createConversation(
    $name: String
    $group: String
    $userIds: [String!]!
    $isChannel: Boolean
  ) @api(name: "chat") {
    createConversation(
      name: $name
      group: $group
      userIds: $userIds
      isChannel: $isChannel
    ) {
      id
      name
    }
  }
`,Ji=x`
  mutation updateConversation(
    $name: String
    $id: ObjectID!
    $userIds: [String!]!
  ) @api(name: "chat") {
    updateConversation(name: $name, id: $id, userIds: $userIds) {
      id
      name
    }
  }
`,Xi=x`
  mutation takeNoteForConversation(
    $conversationId: ObjectID!
    $content: String!
  ) @api(name: "chat") {
    takeNoteForConversation(
      conversationId: $conversationId
      content: $content
    ) {
      id
      content
      conversationId
      createdAt
      createdBy
    }
  }
`,el=x`
  mutation removeMemberFromParticipant($userId: String!, $id: ObjectID!)
  @api(name: "chat") {
    removeMemberFromParticipant(userId: $userId, id: $id) {
      id
    }
  }
`,tl=x`
  mutation createTagForConversation(
    $tagId: ObjectID
    $conversationId: ObjectID
    $name: String
    $color: String
  ) @api(name: "chat") {
    createTagForConversation(
      tagId: $tagId
      conversationId: $conversationId
      name: $name
      color: $color
    ) {
      id
      tags {
        id
        name
        color
      }
    }
  }
`,nl=x`
  mutation markDoneConversation($conversationId: ObjectID) @api(name: "chat") {
    markDoneConversation(id: $conversationId) {
      id
    }
  }
`,al=x`
  mutation deleteTagInConversation(
    $conversationId: ObjectID!
    $tagId: ObjectID!
  ) @api(name: "chat") {
    deleteTagInConversation(conversationId: $conversationId, tagId: $tagId) {
      id
      tags {
        id
      }
    }
  }
`,ol=x`
  mutation createQAPost($input: QAPostInput!, $userId: String)
  @api(name: "zim") {
    createQAPost(input: $input, userId: $userId) {
      id
      title
      content
    }
  }
`,rl=x`
  mutation postComment($content: String!, $type: CommentType, $ref: ObjectID!)
  @api(name: "zim") {
    postComment(content: $content, type: $type, ref: $ref) {
      ... on PostComment {
        id
      }
      ... on QAPostComment {
        id
      }
    }
  }
`,sl=x`
  mutation sendReactionMessage($messageId: ObjectID, $emoji: String)
  @api(name: "chat") {
    sendReactionMessage(messageId: $messageId, emoji: $emoji) {
      id
      emojis {
        userId
        emoji
      }
    }
  }
`,il=()=>{const{innerWidth:e,innerHeight:n}=window,[a,r]=c.exports.useState(e),[s,o]=c.exports.useState(n),l=c.exports.useCallback(()=>{r(window.innerWidth),o(window.innerHeight)},[]);return Ka("resize",l),{width:a,height:s}},ll=({item:e,onClick:n,isActive:a})=>t(P,{id:"js-toggle-profile",onClick:n,w:10,h:10,p:1,rounded:"full",bg:k(a?"blue.400":"gray.100",a?"blue.400":"#1e293b"),overflow:"hidden",display:"flex",alignItems:"center",justifyContent:"center",color:a?"white":"black",children:e.icon}),cl=()=>{const e=_(a=>a.setTab),n=_(a=>a.tab);return t(R,{w:"full",bg:k("white","#10172a"),p:2,justifyContent:"flex-end",alignItems:"center",borderBottomWidth:1,spacing:6,children:[{icon:t(sr,{}),key:"channel",onClick:()=>e("channel")},{icon:t(ir,{}),key:"conversation",onClick:()=>e("conversation")}].map((a,r)=>{const s=a.key===n;return t(ll,{isActive:s,item:a,onClick:a.onClick},r)})})},ro=(e,n=new Date)=>{var d,f;const a=Math.abs(Math.max(Date.parse(n),Date.parse(e))-Math.min(Date.parse(e),Date.parse(n))),r=1e3,s=60*r,o=60*s,l=(d=Math.floor(a/o))!=null?d:0,i=(f=Math.floor(a%o/s).toLocaleString("en-US",{minimumIntegerDigits:1}))!=null?f:"0";return l&&l<24?`${l} gi\u1EDD tr\u01B0\u1EDBc.`:l===0?`${i} ph\xFAt tr\u01B0\u1EDBc.`:l>=24?l/24>31?we(e).format("DD/MM/YYYY"):`${Math.floor(l/24)} ng\xE0y tr\u01B0\u1EDBc.`:"v\u1EEBa xong."},dl="5.5.8",ul=60,pl=0,gl=110,hl=144,ml=105,fl="typing indicator",Al=0,xl=[],bl=[{ddd:0,ind:1,ty:3,nm:"\u25BD Dots",sr:1,ks:{o:{a:0,k:100,ix:11},r:{a:0,k:0,ix:10},p:{a:0,k:[72,51,0],ix:2},a:{a:0,k:[36,9,0],ix:1},s:{a:0,k:[100,100,100],ix:6}},ao:0,ip:0,op:110,st:0,bm:0},{ddd:0,ind:2,ty:4,nm:"dot 1",parent:1,sr:1,ks:{o:{a:0,k:100,ix:11},r:{a:0,k:0,ix:10},p:{a:1,k:[{i:{x:.667,y:1},o:{x:.333,y:0},t:0,s:[9,9,0],to:[0,0,0],ti:[0,0,0]},{i:{x:.667,y:1},o:{x:.333,y:0},t:3.053,s:[9,10,0],to:[0,0,0],ti:[0,0,0]},{i:{x:.667,y:1},o:{x:.333,y:0},t:15.273,s:[9,-9,0],to:[0,0,0],ti:[0,0,0]},{i:{x:.667,y:1},o:{x:.333,y:0},t:25.174,s:[9,12,0],to:[0,0,0],ti:[0,0,0]},{t:32.744140625,s:[9,9,0]}],ix:2},a:{a:0,k:[0,0,0],ix:1},s:{a:0,k:[100,100,100],ix:6}},ao:0,shapes:[{ty:"gr",it:[{d:1,ty:"el",s:{a:0,k:[6,6],ix:2},p:{a:0,k:[0,0],ix:3},nm:"Ellipse Path 1",mn:"ADBE Vector Shape - Ellipse",hd:!1},{ty:"fl",c:{a:1,k:[{i:{x:[.472],y:[1]},o:{x:[1],y:[0]},t:0,s:[.690196078431,.725490196078,.752941176471,1]},{i:{x:[.49],y:[1]},o:{x:[1],y:[0]},t:15.273,s:[.349019616842,.392156869173,.427450984716,1]},{t:32.072265625,s:[.690196078431,.725490196078,.752941176471,1]}],ix:4},o:{a:0,k:100,ix:5},r:1,bm:0,nm:"Fill 1",mn:"ADBE Vector Graphic - Fill",hd:!1},{ty:"tr",p:{a:0,k:[0,0],ix:2},a:{a:0,k:[0,0],ix:1},s:{a:0,k:[300,300],ix:3},r:{a:0,k:0,ix:6},o:{a:0,k:100,ix:7},sk:{a:0,k:0,ix:4},sa:{a:0,k:0,ix:5},nm:"Transform"}],nm:"dot 1",np:2,cix:2,bm:0,ix:1,mn:"ADBE Vector Group",hd:!1}],ip:0,op:110,st:0,bm:0},{ddd:0,ind:3,ty:4,nm:"dot 2",parent:1,sr:1,ks:{o:{a:0,k:100,ix:11},r:{a:0,k:0,ix:10},p:{a:1,k:[{i:{x:.667,y:1},o:{x:.333,y:0},t:10.691,s:[36,9,0],to:[0,0,0],ti:[0,0,0]},{i:{x:.667,y:1},o:{x:.333,y:0},t:15.273,s:[36,10,0],to:[0,0,0],ti:[0,0,0]},{i:{x:.667,y:1},o:{x:.333,y:0},t:25.963,s:[36,-9,0],to:[0,0,0],ti:[0,0,0]},{i:{x:.667,y:1},o:{x:.333,y:0},t:36.984,s:[36,14.326,0],to:[0,0,0],ti:[0,0,0]},{t:43.97265625,s:[36,9,0]}],ix:2},a:{a:0,k:[0,0,0],ix:1},s:{a:0,k:[100,100,100],ix:6}},ao:0,shapes:[{ty:"gr",it:[{d:1,ty:"el",s:{a:0,k:[6,6],ix:2},p:{a:0,k:[0,0],ix:3},nm:"Ellipse Path 1",mn:"ADBE Vector Shape - Ellipse",hd:!1},{ty:"fl",c:{a:1,k:[{i:{x:[.472],y:[1]},o:{x:[1],y:[0]},t:10.691,s:[.690196078431,.725490196078,.752941176471,1]},{i:{x:[.49],y:[1]},o:{x:[1],y:[0]},t:25.963,s:[.349019616842,.392156869173,.427450984716,1]},{t:42.763671875,s:[.690196078431,.725490196078,.752941176471,1]}],ix:4},o:{a:0,k:100,ix:5},r:1,bm:0,nm:"Fill 1",mn:"ADBE Vector Graphic - Fill",hd:!1},{ty:"tr",p:{a:0,k:[0,0],ix:2},a:{a:0,k:[0,0],ix:1},s:{a:0,k:[300,300],ix:3},r:{a:0,k:0,ix:6},o:{a:0,k:100,ix:7},sk:{a:0,k:0,ix:4},sa:{a:0,k:0,ix:5},nm:"Transform"}],nm:"dot 2",np:2,cix:2,bm:0,ix:1,mn:"ADBE Vector Group",hd:!1}],ip:0,op:110,st:0,bm:0},{ddd:0,ind:4,ty:4,nm:"dot 3",parent:1,sr:1,ks:{o:{a:0,k:100,ix:11},r:{a:0,k:0,ix:10},p:{a:1,k:[{i:{x:.667,y:1},o:{x:.333,y:0},t:21.383,s:[63,9,0],to:[0,0,0],ti:[0,0,0]},{i:{x:.667,y:1},o:{x:.333,y:0},t:25.963,s:[63,10,0],to:[0,0,0],ti:[0,0,0]},{i:{x:.667,y:1},o:{x:.333,y:0},t:38.184,s:[63,-9,0],to:[0,0,0],ti:[0,0,0]},{i:{x:.667,y:1},o:{x:.333,y:0},t:49.594,s:[63,13.139,0],to:[0,0,0],ti:[0,0,0]},{t:56,s:[63,9,0]}],ix:2},a:{a:0,k:[0,0,0],ix:1},s:{a:0,k:[100,100,100],ix:6}},ao:0,shapes:[{ty:"gr",it:[{d:1,ty:"el",s:{a:0,k:[6,6],ix:2},p:{a:0,k:[0,0],ix:3},nm:"Ellipse Path 1",mn:"ADBE Vector Shape - Ellipse",hd:!1},{ty:"fl",c:{a:1,k:[{i:{x:[.472],y:[1]},o:{x:[1],y:[0]},t:21.383,s:[.690196078431,.725490196078,.752941176471,1]},{i:{x:[.49],y:[1]},o:{x:[1],y:[0]},t:36.656,s:[.349019616842,.392156869173,.427450984716,1]},{t:53.45703125,s:[.690196078431,.725490196078,.752941176471,1]}],ix:4},o:{a:0,k:100,ix:5},r:1,bm:0,nm:"Fill 1",mn:"ADBE Vector Graphic - Fill",hd:!1},{ty:"tr",p:{a:0,k:[0,0],ix:2},a:{a:0,k:[0,0],ix:1},s:{a:0,k:[300,300],ix:3},r:{a:0,k:0,ix:6},o:{a:0,k:100,ix:7},sk:{a:0,k:0,ix:4},sa:{a:0,k:0,ix:5},nm:"Transform"}],nm:"dot 3",np:2,cix:2,bm:0,ix:1,mn:"ADBE Vector Group",hd:!1}],ip:0,op:110,st:0,bm:0}],yl=[];var Cl={v:dl,fr:ul,ip:pl,op:gl,w:hl,h:ml,nm:fl,ddd:Al,assets:xl,layers:bl,markers:yl};const so=()=>{const e={animationData:Cl,loop:!0,autoplay:!0},{View:n}=lr(e);return n},io=({groupInfo:e})=>h(Be,{columns:2,spacing:1,children:[e==null?void 0:e.slice(0,3).map((n,a)=>t(Ie,{size:"2xs",src:n==null?void 0:n.avatar,name:n==null?void 0:n.fullName},a)),(e==null?void 0:e.length)>3&&t(P,{display:"flex",alignItems:"center",justifyContent:"center",w:"16px",h:"16px",rounded:"full",bg:"orange.500",children:t(L,{color:"white",fontSize:12,children:(e==null?void 0:e.length)-3<=9?`+${(e==null?void 0:e.length)-3}`:"..."})})]}),El=({onClick:e,username:n,usernameClassName:a,lastMessage:r,seen:s,isActive:o,updatedAt:l,groupInfo:i,seenByCount:d,isTyping:f,avatarUrl:m,online:u,userTypingName:$})=>{const p=k("gray.500","slate.300"),A=()=>{if(d===(i==null?void 0:i.length)||!i)return null;if(i)return h(L,{color:p,fontSize:12,children:[d,"/",i==null?void 0:i.length]})};return h(R,{_hover:{backgroundColor:"#e4e4e450"},w:"100%",align:"start",px:3,py:2,borderBottomWidth:1,cursor:"pointer",onClick:e,bg:k(o?"gray.100":"bg-white",o?"#1e293b":"#1e293b50"),children:[qe.exports.isEmpty(i)?t(Ie,{boxSize:"2em",src:m,name:n,children:t(ia,{boxSize:"14px",bg:u?"green.500":"gray.300"})}):t(io,{groupInfo:i}),h(ue,{flex:1,spacing:1,children:[t(L,{noOfLines:2,fontSize:15,lineHeight:"18px",color:k(s?"gray.700":"gray.900",s?"#ffffff50":"white"),className:`${a}`,textTransform:"capitalize",children:n}),f?h(R,{flex:1,children:[t(L,{noOfLines:1,fontSize:13,color:k("gray.700","white"),children:$==null?void 0:$.map(E=>E||"Someone is typing").join(",")}),t(P,{display:"flex",alignItems:"center",justifyContent:"center",w:10,h:5,children:t(so,{})})]}):t(L,{noOfLines:2,fontSize:12,lineHeight:"24px",fontWeight:s?"400":"600",color:k(s?"gray.700":"gray.900",s?"#ffffff50":"white"),children:r})]}),h(te,{spacing:2,alignItems:"flex-end",minW:"50px",children:[t(L,{color:k(s?"gray.700":"gray.900",s?"#ffffff50":"white"),fontSize:12,children:l}),h(R,{justifyContent:"space-between",children:[s&&t(cr,{}),A()]})]})]})},$l=e=>{const{isMultiple:n}=e,{getInputProps:a,getCheckboxProps:r}=n?ga(e):ha(e),s=a(),o=r();return h(P,{as:"label",rounded:"md",d:"block",bg:k("#1e293b","#1e293b50"),children:[t("input",v(b({},s),{style:{display:"none"}})),h(P,v(b({},o),{cursor:"pointer",opacity:.8,pos:"relative",children:[s.checked&&t(ma,{color:"green.500",fontSize:"xl",pos:"absolute",top:2,right:2,zIndex:2}),e.children]}))]})},Sl=[{key:"",value:"All"},{key:"zimians",value:"Zimians"},{key:"student",value:"Student"}];function ft(e,n){const[a,r]=c.exports.useState(e);return c.exports.useEffect(()=>{const s=setTimeout(()=>{r(e)},n);return()=>{clearTimeout(s)}},[e,n]),a}const vl=e=>e.scrollHeight-e.scrollTop<=e.clientHeight+10,Il=({getCheckboxProps:e,listParticipants:n,usersInfo:a,isAddSupporter:r,value:s,setValue:o})=>{var B,Q,D,H;const[l]=ae(),[i,d]=c.exports.useState(""),[f,m]=c.exports.useState(!1),[u,$]=c.exports.useState(""),p=ft(i,500),A=F=>{m(vl(F.target))},E=se({roles:r?["EC"]:u==="all"?"":u==="zimians"?Fo:u==="student"?["HV"]:"",searchString:p,limit:20},F=>F),{data:C,loading:S,fetchMore:g}=ge(Ha,{variables:E}),I=(Q=(B=C==null?void 0:C.getAccountChatPagination)==null?void 0:B.docs)==null?void 0:Q.filter(F=>{var V,O,U,W;return!(n==null?void 0:n.includes((O=(V=F==null?void 0:F.user)==null?void 0:V.id)==null?void 0:O.toString()))&&((W=(U=F==null?void 0:F.user)==null?void 0:U.id)==null?void 0:W.toString())!==(l==null?void 0:l.id)}),y=(D=C==null?void 0:C.getAccountChatPagination)==null?void 0:D.page,T=(H=C==null?void 0:C.getAccountChatPagination)==null?void 0:H.hasNextPage,{onLoadMore:M,isLoadingMore:w}=Ge({variables:v(b({},E),{page:y+1}),fetchMore:g,hasNextPage:T,isNormalLoadMore:!1});return c.exports.useEffect(()=>{f&&T&&!w&&M()},[f,T,w]),h(ue,{spacing:3,children:[t(L,{fontSize:14,children:"Danh s\xE1ch th\xE0nh vi\xEAn"}),t(R,{children:Sl.map((F,V)=>{const O=F.key===u;return t(Y,{onClick:()=>$(F.key),colorScheme:"teal",variant:O?"solid":"outline",disabled:r&&V!==1,children:F.value},V)})}),t(Dt,{my:2,children:a==null?void 0:a.map((F,V)=>h(fa,{size:"md",rounded:"md",children:[t(Aa,{children:F.full_name}),t(xa,{onClick:()=>o(s==null?void 0:s.filter(O=>{var U;return O!==((U=F==null?void 0:F.id)==null?void 0:U.toString())}))})]},V))}),t(ve,{type:"text",placeholder:"Nh\u1EADp t\xEAn ng\u01B0\u1EDDi d\xF9ng",size:"md",value:i,onChange:F=>d(F.target.value)}),h(P,{position:"relative",h:"250px",overflow:"auto",w:"full",flexGrow:1,onScroll:A,children:[h(Be,{columns:2,spacing:2,children:[(I==null?void 0:I.length)>0&&(I==null?void 0:I.map(({user:F})=>{const V=e({value:F.id});return t(P,{rounded:"lg",overflow:"hidden",children:t($l,v(b({isMultiple:!0},V),{children:t(mt,v(b({},F),{username:F==null?void 0:F.full_name,positionName:F==null?void 0:F.role_name,lastOnline:F==null?void 0:F.status_online,px:3,py:2}))}))},`${F.id}`)})),S&&h(R,{rounded:"lg",bg:"blackAlpha.300",opacity:.5,pos:"absolute",top:0,bottom:0,right:0,left:0,justifyContent:"center",children:[t(ot,{color:"#319795",size:"lg"}),t(L,{fontSize:"xl",color:"white",children:"\u0110ang t\u1EA3i ..."})]})]}),w&&h(R,{w:"full",alignItems:"center",justifyContent:"center",py:2,children:[t(ot,{color:"#319795",size:"lg"}),t(L,{fontSize:"xl",color:k("gray.700","white"),children:"\u0110ang t\u1EA3i ..."})]})]})]})},kl=(e,n=null)=>{e.forEach(a=>{n==null||n.evict({id:"ROOT_QUERY",fieldName:a})})};function Ne(){const e=ba();return n=>{kl(n,e.cache)}}const wl=({onClose:e,userId:n,isEdit:a,groupId:r,setUserId:s,isAddSupporter:o,isChannel:l})=>{const[i]=ae(),d=Te(),f=Ne(),m=_(C=>C.setConversationId),[u,{loading:$}]=Ae(Ki,{onCompleted:C=>{const{createConversation:S}=C;S&&(d({title:"Th\xE0nh c\xF4ng!",description:`T\u1EA1o ${l?"channel":"nh\xF3m chat"} ${S==null?void 0:S.name} th\xE0nh c\xF4ng`,status:"success",duration:3e3,isClosable:!0}),e(),m(S==null?void 0:S.id)),s([]),f(["conversations","conversationDetail"])},onError:C=>{var S;d({title:`T\u1EA1o ${l?"channel":"nh\xF3m chat"} kh\xF4ng th\xE0nh c\xF4ng!`,description:`${(S=C==null?void 0:C.message)!=null?S:"L\u1ED7i"}`,status:"error",duration:3e3,isClosable:!0})}}),[p,{loading:A}]=Ae(Ji,{onCompleted:C=>{const{updateConversation:S}=C;S&&d({title:"Th\xE0nh c\xF4ng!",description:o?"Th\xEAm EC h\u1ED7 tr\u1EE3 th\xE0nh c\xF4ng":`C\u1EADp nh\u1EADt ${l?"channel":"nh\xF3m chat"} ${S==null?void 0:S.name} th\xE0nh c\xF4ng`,status:"success",duration:3e3,isClosable:!0}),e(),s([]),f(["conversations","conversationDetail"])},onError:C=>{var S;d({title:`C\u1EADp nh\u1EADt ${l?"channel":"nh\xF3m chat"} kh\xF4ng th\xE0nh c\xF4ng!`,description:`${(S=C==null?void 0:C.message)!=null?S:"L\u1ED7i"}`,status:"error",duration:3e3,isClosable:!0})}});return{handleSubmitForm:c.exports.useCallback(async C=>{try{a?await p({variables:se({name:C==null?void 0:C.name,id:r,userIds:n==null?void 0:n.filter(S=>S!==(i==null?void 0:i.id))})}):await u({variables:se({isChannel:l,name:C==null?void 0:C.name,group:r,userIds:n})})}catch(S){console.log(S)}},[n,r,a,i,l]),loading:$||A}},Tl=e=>typeof e=="object"?Promise.resolve(e):ht.query({query:Ga,variables:{userId:parseInt(e,10)}}).then(({data:n})=>n.getUserById),lo=(e,n)=>qe.exports.isEmpty(e||n)?[]:Promise.allSettled(e==null?void 0:e.map(Tl)).then(a=>a.filter(r=>r.status==="fulfilled").map(r=>r==null?void 0:r.value)),rn=({onClose:e,isOpen:n,groupId:a="",listParticipants:r,name:s,isEdit:o,isAddSupporter:l,isChannel:i})=>{const[d,f]=c.exports.useState([]),m=c.exports.useRef(),[u,$]=c.exports.useState([]),[p,A]=c.exports.useState(a),{getCheckboxProps:E,value:C,setValue:S}=ya({onChange:$}),{handleSubmitForm:g,loading:I}=wl({onClose:e,userId:[...r!=null?r:[],...u!=null?u:[]],isEdit:o,groupId:p,setUserId:$,isAddSupporter:l,isChannel:i}),y=$t({mode:"onBlur",reValidateMode:"onChange"}),{register:T,formState:{errors:M}}=y,w=c.exports.useRef(null),B=y.watch("name"),Q=!qe.exports.isEqual(B,w.current)||(u==null?void 0:u.length)>0;return zt(()=>{o&&!l&&(y.reset({name:s}),w.current=s)},[n,o,w,y,s]),c.exports.useEffect(()=>{(async function(){try{const D=await lo(u,!n);f(D)}catch(D){console.log(D)}})()},[u]),c.exports.useEffect(()=>{A(a)},[a]),t(jt,v(b({},y),{children:h(Ve,{finalFocusRef:m,isOpen:n,onClose:()=>{e(),S([])},size:"2xl",children:[t(Le,{}),h(We,{bg:k(void 0,"slate.900"),children:[t(Re,{children:l?"Th\xEAm EC h\u1ED7 tr\u1EE3":o?"C\u1EADp nh\u1EADt nh\xF3m chat":i?"T\u1EA1o channel":"T\u1EA1o nh\xF3m chat"}),t(Qe,{}),h("form",{onSubmit:y.handleSubmit(g),children:[t(Ye,{children:h(ue,{spacing:6,children:[!l&&t(ve,v(b({label:i?"T\xEAn channel":"T\xEAn nh\xF3m",placeholder:"Nh\u1EADp t\xEAn nh\xF3m"},T("name",{required:!0})),{error:M==null?void 0:M.name})),t(Il,{value:C,setValue:S,isAddSupporter:l,usersInfo:d,listParticipants:r,getCheckboxProps:E})]})}),h(rt,{children:[t(Y,{disabled:!o&&qe.exports.isEmpty(u)||I||o&&!Q,leftIcon:t(st,{as:Ut}),bg:"#319795",mr:4,type:"submit",isLoading:!1,color:"white",children:o?"C\u1EADp nh\u1EADt":"T\u1EA1o nh\xF3m"}),t(Y,{disabled:I,variant:"outline",onClick:()=>{e(),S([]),$([])},children:"H\u1EE7y"})]})]})]})]})}))},sn=[{key:"open",value:"Open"},{key:"following",value:"Following"},{key:"done",value:"Done"}],Pl=({data:e,isSelected:n,setParticipants:a,setUserId:r,loading:s,setLocalTab:o})=>{var S;const{isOpen:l,onOpen:i,onClose:d}=Pe(),f=_(g=>g.setTab),m=_(g=>g.channel),u=_(g=>g.conversationId),$=_(g=>g.setConversationId),[p]=ae(),[A,E]=De({}),C=c.exports.useCallback(g=>{u===g.id&&f("message"),$(g.id),a(g==null?void 0:g.participants),r(""),f("message"),E({qsConversationId:g.id}),A.delete("qsUserId"),A.delete("qsGroup")},[u,A]);return h(dr,{onChange:g=>{var I;return o((I=sn[g])==null?void 0:I.key)},defaultIndex:sn.findIndex(g=>g.key===n),children:[h(ze,{flexDirection:{base:"row",md:"column"},position:"sticky",top:0,zIndex:20,bg:k("white","#10172a"),alignItems:"center",justifyContent:"center",children:[h(R,{w:{base:["my chats","guest"].includes(m)?"25%":"100%",md:"full"},alignItems:"center",justify:"space-between",spacing:4,px:2,borderBottomWidth:1,minH:"50px",children:[t(L,{fontSize:13,fontWeight:"bold",textColor:k("#444","slate.300"),children:(S=m==null?void 0:m.toString())==null?void 0:S.toLocaleUpperCase()}),!["my chats","guest"].includes(m)&&pn.map(g=>g.id).includes(p==null?void 0:p.roleId)&&h(P,{onClick:i,"data-tip":"React-tooltip",display:"flex",alignItems:"center",justifyContent:"center",cursor:"pointer",children:[t(ur,{fontSize:20}),t(Ca,{children:t("p",{className:"text-base font-medium",children:"T\u1EA1o group chat"}),place:"bottom",backgroundColor:"green",effect:"float"})]})]}),m==="guest"&&t(R,{spacing:0,w:"full",minH:"50px",children:sn.map((g,I)=>t(pr,{minH:"50px",flex:1,_dark:{color:"slate.300"},w:"full",fontSize:{sm:14},px:0,borderBottomColor:k("gray.200","whiteAlpha.300"),mb:0,children:g.value},g.key))})]}),t(ue,{spacing:0,children:(e==null?void 0:e.length)>0?e.map((g,I)=>{var F,V,O,U,W,ne,q,z,X,be,he,fe;const y=g==null?void 0:g.participants,T=!(y==null?void 0:y.find(ee=>ee.userId===(p==null?void 0:p.id)))&&(y==null?void 0:y.length)>=2||(y==null?void 0:y.find(ee=>ee.userId===(p==null?void 0:p.id)))&&(y==null?void 0:y.length)>=3,M=(g==null?void 0:g.name)||T?y:y==null?void 0:y.filter(ee=>ee.userId!==(p==null?void 0:p.id))[0],w=(M==null?void 0:M.isGuest)?M==null?void 0:M.fullName:(g==null?void 0:g.name)?g==null?void 0:g.name:T?y==null?void 0:y.map(ee=>ee.fullName).join(","):M==null?void 0:M.fullName,B=((V=(F=g==null?void 0:g.usersTyping)==null?void 0:F.filter(ee=>ee!==(p==null?void 0:p.id)))==null?void 0:V.length)>0,Q=(g==null?void 0:g.lastMessage)?(U=(O=g==null?void 0:g.lastMessage)==null?void 0:O.seenBy)==null?void 0:U.includes(p.id):!0,D=ro(we(g==null?void 0:g.updatedAt).toDate()),H=((ne=(W=g==null?void 0:g.lastMessage)==null?void 0:W.attachments)==null?void 0:ne.length)>0?["[T\u1EC7p \u0111\xEDnh k\xE8m]"]:(q=g==null?void 0:g.lastMessage)==null?void 0:q.text;return s?t(eo,{},I):c.exports.createElement(El,v(b({},M),{isTyping:B,isActive:g.id===u,key:g.id,onClick:()=>{C(g)},username:w,lastMessage:H,updatedAt:D,seen:Q,seenByCount:(be=(X=(z=g==null?void 0:g.lastMessage)==null?void 0:z.seenBy)==null?void 0:X.length)!=null?be:0,avatarUrl:M==null?void 0:M.avatar,groupInfo:(g==null?void 0:g.name)||T?M:null,userTypingName:(fe=(he=g==null?void 0:g.usersTyping)==null?void 0:he.filter(ee=>ee!==p.id))==null?void 0:fe.map(ee=>{var xe;return(xe=y.find(ye=>ye.userId===ee))==null?void 0:xe.fullName})}))}):t(L,{textAlign:"center",mt:4,children:`Hi\u1EC7n t\u1EA1i kh\xF4ng c\xF3 ${m==="guest"?"y\xEAu c\u1EA7u h\u1ED7 tr\u1EE3":"cu\u1ED9c tr\xF2 chuy\u1EC7n"} n\xE0o !!!`})}),t(rn,{isChannel:!1,isAddSupporter:!1,isEdit:!1,name:"",listParticipants:[],isOpen:l,onClose:d,groupId:m})]})},_l=({status:e="",search:n,channel:a})=>{var A,E,C;const r=v(b({},se({status:e,search:n},S=>S)),{limit:20,offset:0}),[s,{data:o,loading:l,fetchMore:i,called:d}]=Gt(Ee,{variables:r,fetchPolicy:"network-only",nextFetchPolicy:"cache-first"});c.exports.useEffect(()=>{a&&a==="guest"&&!d&&(async()=>await s())()},[a,d]);const f=((A=o==null?void 0:o.supportConversations)==null?void 0:A.docs)||[],m=(E=o==null?void 0:o.supportConversations)==null?void 0:E.hasNextPage,{onLoadMore:u,isLoadingMore:$}=Ge({variables:v(b({},r),{offset:(f==null?void 0:f.length)+1}),fetchMore:i,hasNextPage:m});return{listSupportConversations:((C=o==null?void 0:o.supportConversations)==null?void 0:C.docs)||[],loading:l,onLoadMore:u,hasNextPage:m,isLoadingMore:$}},Ml=({search:e,channel:n})=>{var $,p;const a=v(b({},se({group:n==="my chats"?"":n,search:e},A=>A)),{limit:20,offset:0}),[r,{data:s,loading:o,fetchMore:l,called:i}]=Gt(pe,{variables:a,fetchPolicy:"network-only",nextFetchPolicy:"cache-first"});c.exports.useEffect(()=>{n&&n!=="guest"&&!i&&(async()=>await r())()},[n,i,r]);const d=(($=s==null?void 0:s.conversations)==null?void 0:$.docs)||[],f=(p=s==null?void 0:s.conversations)==null?void 0:p.hasNextPage,{onLoadMore:m,isLoadingMore:u}=Ge({variables:v(b({},a),{offset:(d==null?void 0:d.length)+1}),fetchMore:l,hasNextPage:f});return{listConversations:d,loading:o,onLoadMore:m,hasNextPage:f,isLoadingMore:u}},ql=e=>e.scrollHeight-e.scrollTop<=e.clientHeight+10,At=({text:e="\u0110ang l\u1EA5y d\u1EEF li\u1EC7u...",color:n="gray.900"})=>h(R,{justifyContent:"center",pointerEvents:"none",userSelect:"none",children:[t(ot,{color:n}),t(L,{textAlign:"center",color:n,children:e})]}),Ol=({onClick:e,username:n,lastMessage:a,isActive:r,createdAt:s,avatarUrl:o})=>{const l=_(i=>i.searchStringMessage);return h(R,{_hover:{backgroundColor:"#e4e4e450"},w:"100%",align:"start",p:3,py:2,shadow:"md",cursor:"pointer",rounded:"lg",onClick:e,bg:k(r?"gray.100":"bg-white",r?"#1e293b":"#1e293b50"),children:[t(Ie,{boxSize:"2em",src:o,name:n}),h(ue,{flex:1,spacing:1,children:[t(L,{noOfLines:2,fontSize:15,lineHeight:"18px",color:k("gray.900","slate.300"),children:n}),t(L,{noOfLines:2,fontSize:12,lineHeight:"24px",fontWeight:"400",color:k("gray.900","slate.300"),children:t(Ea,{searchWords:[l],autoEscape:!0,textToHighlight:a})})]}),t(te,{spacing:2,alignItems:"flex-end",minW:"50px",children:t(L,{color:k("gray.900","slate.300"),fontSize:12,children:s})})]})},Fl=()=>{const e=_(i=>i.searchStringMessage),n=_(i=>i.searchResult),a=_(i=>i.setMessageId),r=_(i=>i.messageId),s=_(i=>i.setBeforeId),o=_(i=>i.setAfterId),l=i=>{a(i),s(""),o("")};return h(ue,{w:"full",h:"full",p:2,spacing:2,children:[t(R,{position:"sticky",top:0,zIndex:50,bg:k("white","#10172a"),alignItems:"center",children:t(L,{fontSize:16,fontWeight:"bold",textColor:k("#444","slate.300"),children:`C\xF3 ${n==null?void 0:n.length} k\u1EBFt qu\u1EA3 t\xECm ki\u1EBFm cho t\u1EEB kh\xF3a "${e}"`})}),n==null?void 0:n.map(i=>{var u,$;const d=($=(u=i==null?void 0:i.conversation)==null?void 0:u.participants)==null?void 0:$.find(p=>p.userId===(i==null?void 0:i.from)),f=ro(we(i==null?void 0:i.createdAt).toDate()),m=i.id===r;return t(Ol,{isActive:m,onClick:()=>l(i.id),lastMessage:i==null?void 0:i.text,avatarUrl:d==null?void 0:d.avatar,username:d==null?void 0:d.fullName,createdAt:f},i.id)})]})},Nl=()=>{const e=_(y=>y.setParticipants),n=_(y=>y.searchStringMessage),a=_(y=>y.channel),r=_(y=>y.setUserId),[s,o]=c.exports.useState(!1),[l,i]=c.exports.useState("open"),{listSupportConversations:d,onLoadMore:f,isLoadingMore:m,hasNextPage:u,loading:$}=_l({status:l,search:"",channel:a}),{listConversations:p,onLoadMore:A,hasNextPage:E,isLoadingMore:C,loading:S}=Ml({channel:a,search:""}),g=a==="guest"?{onLoadMore:f,isLoadingMore:m,hasNextPage:u,loading:$}:{onLoadMore:A,hasNextPage:E,isLoadingMore:C,loading:S},I=c.exports.useCallback(y=>{o(ql(y.target))},[]);return zt(()=>{s&&g.hasNextPage&&!g.isLoadingMore&&g.onLoadMore()},[s,g]),h(ze,{direction:"column",w:{base:"full",md:"260px",lg:"350px"},h:"100%",bg:k("white","#10172a"),onScroll:I,overflow:"auto",position:"relative",children:[n?t(Fl,{}):t(Pl,{setParticipants:e,setLocalTab:i,setUserId:r,loading:g.loading,data:g.loading?[1,2,3,4,5,6,7,8]:a==="guest"?d:p,isSelected:l}),g.isLoadingMore&&t(At,{text:"\u0110ang t\u1EA3i cu\u1ED9c tr\xF2 chuy\u1EC7n",color:k("","white")})]})};var co="/assets/weCanDoItWomen.png";const Bl=()=>({onPush:c.exports.useCallback((n,a,r)=>{if(!Notification)return;Notification.permission!=="granted"&&Notification.requestPermission();let s=new Notification("B\u1EA1n c\xF3 tin nh\u1EAFn m\u1EDBi !!",{icon:co,body:`${r}: ${a}`});s.onclick=function(){window.open(`${zi.apiUrl}/Admin/Chat?qsConversationId=${n}`)}},[])}),Ll=()=>{const[e]=ae(),{onPush:n}=Bl(),a=_(s=>s.conversationId),r=_(s=>s.userId);Rt(Ja,{onSubscriptionData:({client:s,subscriptionData:o})=>{var O,U,W,ne,q,z,X,be,he,fe,ee,xe,ye,Z,de,_e,G,ie,Me,Xe,et,tt,nt,at,hn,mn,fn,An,xn,bn,yn,Cn,En,$n,Sn,vn,In,kn,wn,Tn,Pn,_n,Mn,qn,On,Fn,Nn,Bn,Ln,Rn,Dn,zn,jn,Un,Gn,Hn,Vn,Wn,Qn,Yn,Zn,Kn,Jn;const l=s.cache.readQuery({query:ke,variables:v(b({},se({conversationId:a,userId:a?"":r==null?void 0:r.toString(),limit:30},N=>N)),{offset:0})}),i=(U=(O=l==null?void 0:l.messages)==null?void 0:O.docs)!=null?U:[],{conversationUpdated:d}=o.data,{lastMessage:f,typing:m}=d,{id:u,from:$}=f;if(!(i==null?void 0:i.find(N=>(N==null?void 0:N.id)===u))&&e.id!==$&&m===null){const N=(ne=(W=d==null?void 0:d.participants)==null?void 0:W.find(K=>K.userId===$))==null?void 0:ne.fullName;n((q=o==null?void 0:o.data)==null?void 0:q.conversationUpdated.id,(z=o==null?void 0:o.data)==null?void 0:z.conversationUpdated.lastMessage.text,N)}const A=(be=(X=o==null?void 0:o.data)==null?void 0:X.conversationUpdated)==null?void 0:be.group,E=(fe=(he=o==null?void 0:o.data)==null?void 0:he.conversationUpdated)==null?void 0:fe.status,C=s.cache.readQuery({query:pe,variables:{offset:0,limit:20}}),S=(xe=(ee=C==null?void 0:C.conversations)==null?void 0:ee.docs)!=null?xe:[];if((C==null?void 0:C.conversations)&&E!=="open"){let N=[...S];const K=N==null?void 0:N.find(j=>{var oe,re;return(j==null?void 0:j.id)===((re=(oe=o==null?void 0:o.data)==null?void 0:oe.conversationUpdated)==null?void 0:re.id)});K&&((ye=o==null?void 0:o.data)==null?void 0:ye.conversationUpdated.typing)===null?(N=N.filter(j=>j.id!==K.id),s.writeQuery({query:pe,variables:{offset:0,limit:20},data:v(b({},C),{conversations:v(b({},(Z=C.conversations)!=null?Z:[]),{docs:[(de=o==null?void 0:o.data)==null?void 0:de.conversationUpdated,...N]})})})):K?s.writeQuery({query:pe,variables:{offset:0,limit:20},data:b({},C)}):s.writeQuery({query:pe,variables:{offset:0,limit:20},data:v(b({},C),{conversations:v(b({},(_e=C.conversations)!=null?_e:[]),{docs:[(G=o==null?void 0:o.data)==null?void 0:G.conversationUpdated,...N]})})})}const g=s.cache.readQuery({query:pe,variables:{group:"zimians",offset:0,limit:20}}),I=(Me=(ie=g==null?void 0:g.conversations)==null?void 0:ie.docs)!=null?Me:[];if((g==null?void 0:g.conversations)&&A==="zimians"){let N=[...I];const K=N==null?void 0:N.find(j=>{var oe,re;return(j==null?void 0:j.id)===((re=(oe=o==null?void 0:o.data)==null?void 0:oe.conversationUpdated)==null?void 0:re.id)});K&&((Xe=o==null?void 0:o.data)==null?void 0:Xe.conversationUpdated.typing)===null?(N=N.filter(j=>j.id!==K.id),s.writeQuery({query:pe,variables:{group:"zimians",offset:0,limit:20},data:v(b({},g),{conversations:v(b({},(et=g.conversations)!=null?et:[]),{docs:[(tt=o==null?void 0:o.data)==null?void 0:tt.conversationUpdated,...N]})})})):K?s.writeQuery({query:pe,variables:{group:"zimians",offset:0,limit:20},data:b({},g)}):s.writeQuery({query:pe,variables:{group:"zimians",offset:0,limit:20},data:v(b({},g),{conversations:v(b({},(nt=g.conversations)!=null?nt:[]),{docs:[(at=o==null?void 0:o.data)==null?void 0:at.conversationUpdated,...N]})})})}const y=s.cache.readQuery({query:pe,variables:{group:"student",offset:0,limit:20}}),T=(mn=(hn=y==null?void 0:y.conversations)==null?void 0:hn.docs)!=null?mn:[];if((y==null?void 0:y.conversations)&&A==="student"){let N=[...T];const K=N==null?void 0:N.find(j=>{var oe,re;return(j==null?void 0:j.id)===((re=(oe=o==null?void 0:o.data)==null?void 0:oe.conversationUpdated)==null?void 0:re.id)});K&&((fn=o==null?void 0:o.data)==null?void 0:fn.conversationUpdated.typing)===null?(N=N.filter(j=>j.id!==K.id),s.writeQuery({query:pe,variables:{group:"student",offset:0,limit:20},data:v(b({},y),{conversations:v(b({},(An=y.conversations)!=null?An:[]),{docs:[(xn=o==null?void 0:o.data)==null?void 0:xn.conversationUpdated,...N]})})})):K?s.writeQuery({query:pe,variables:{group:"student",offset:0,limit:20},data:b({},y)}):s.writeQuery({query:pe,variables:{group:"student",offset:0,limit:20},data:v(b({},y),{conversations:v(b({},(bn=y.conversations)!=null?bn:[]),{docs:[(yn=o==null?void 0:o.data)==null?void 0:yn.conversationUpdated,...N]})})})}const M=s.cache.readQuery({query:pe,variables:{group:"customer",offset:0,limit:20}}),w=(En=(Cn=M==null?void 0:M.conversations)==null?void 0:Cn.docs)!=null?En:[];if((M==null?void 0:M.conversations)&&A==="customer"){let N=[...w];const K=N==null?void 0:N.find(j=>{var oe,re;return(j==null?void 0:j.id)===((re=(oe=o==null?void 0:o.data)==null?void 0:oe.conversationUpdated)==null?void 0:re.id)});K&&(($n=o==null?void 0:o.data)==null?void 0:$n.conversationUpdated.typing)===null?(N=N.filter(j=>j.id!==K.id),s.writeQuery({query:pe,variables:{group:"customer",offset:0,limit:20},data:v(b({},M),{conversations:v(b({},(Sn=M.conversations)!=null?Sn:[]),{docs:[(vn=o==null?void 0:o.data)==null?void 0:vn.conversationUpdated,...N]})})})):K?s.writeQuery({query:pe,variables:{group:"customer",offset:0,limit:20},data:b({},M)}):s.writeQuery({query:pe,variables:{group:"customer",offset:0,limit:20},data:v(b({},M),{conversations:v(b({},(In=M.conversations)!=null?In:[]),{docs:[(kn=o==null?void 0:o.data)==null?void 0:kn.conversationUpdated,...N]})})})}const B=s.cache.readQuery({query:Ee,variables:{status:"open",offset:0,limit:20}}),Q=(Tn=(wn=B==null?void 0:B.supportConversations)==null?void 0:wn.docs)!=null?Tn:[];if((B==null?void 0:B.supportConversations)&&A==="support"){let N=[...Q];const K=N==null?void 0:N.find(j=>{var oe,re;return(j==null?void 0:j.id)===((re=(oe=o==null?void 0:o.data)==null?void 0:oe.conversationUpdated)==null?void 0:re.id)});!K&&E==="open"?s.writeQuery({query:Ee,variables:{status:"open",offset:0,limit:20},data:v(b({},B),{supportConversations:v(b({},(Pn=B.supportConversations)!=null?Pn:[]),{docs:[(_n=o==null?void 0:o.data)==null?void 0:_n.conversationUpdated,...N]})})}):K&&((Mn=o==null?void 0:o.data)==null?void 0:Mn.conversationUpdated.typing)===null&&((Fn=(On=(qn=o==null?void 0:o.data)==null?void 0:qn.conversationUpdated)==null?void 0:On.lastMessage)==null?void 0:Fn.from)===e.id?(N=N.filter(j=>j.id!==K.id),s.writeQuery({query:Ee,variables:{status:"open",offset:0,limit:20},data:v(b({},B),{supportConversations:v(b({},(Nn=B.supportConversations)!=null?Nn:[]),{docs:[...N]})})})):s.writeQuery({query:Ee,variables:{status:"open",offset:0,limit:20},data:v(b({},B),{supportConversations:b({},(Bn=B.supportConversations)!=null?Bn:[])})})}const D=s.cache.readQuery({query:Ee,variables:{status:"following",offset:0,limit:20}}),H=(Rn=(Ln=D==null?void 0:D.supportConversations)==null?void 0:Ln.docs)!=null?Rn:[];if((D==null?void 0:D.supportConversations)&&A==="support"){let N=[...H];const K=N==null?void 0:N.find(j=>{var oe,re;return(j==null?void 0:j.id)===((re=(oe=o==null?void 0:o.data)==null?void 0:oe.conversationUpdated)==null?void 0:re.id)});!K&&E==="following"?s.writeQuery({query:Ee,variables:{status:"following",offset:0,limit:20},data:v(b({},D),{supportConversations:v(b({},(Dn=D.supportConversations)!=null?Dn:[]),{docs:[(zn=o==null?void 0:o.data)==null?void 0:zn.conversationUpdated,...N]})})}):K&&((jn=o==null?void 0:o.data)==null?void 0:jn.conversationUpdated.typing)===null?(N=N.filter(j=>j.id!==K.id),s.writeQuery({query:Ee,variables:{status:"following",offset:0,limit:20},data:v(b({},D),{supportConversations:v(b({},(Un=D.supportConversations)!=null?Un:[]),{docs:E==="following"?[(Gn=o==null?void 0:o.data)==null?void 0:Gn.conversationUpdated,...N]:[...N]})})})):s.writeQuery({query:Ee,variables:{status:"following",offset:0,limit:20},data:v(b({},D),{supportConversations:b({},(Hn=D.supportConversations)!=null?Hn:[])})})}const F=s.cache.readQuery({query:Ee,variables:{status:"done",offset:0,limit:20}}),V=(Wn=(Vn=F==null?void 0:F.supportConversations)==null?void 0:Vn.docs)!=null?Wn:[];if((F==null?void 0:F.supportConversations)&&A==="support"){let N=[...V];const K=N==null?void 0:N.find(j=>{var oe,re;return(j==null?void 0:j.id)===((re=(oe=o==null?void 0:o.data)==null?void 0:oe.conversationUpdated)==null?void 0:re.id)});K&&((Qn=o==null?void 0:o.data)==null?void 0:Qn.conversationUpdated.typing)===null?(N=N.filter(j=>j.id!==K.id),s.writeQuery({query:Ee,variables:{status:"done",offset:0,limit:20},data:v(b({},V),{supportConversations:v(b({},(Yn=V.supportConversations)!=null?Yn:[]),{docs:[...N]})})})):!K&&E==="done"?s.writeQuery({query:Ee,variables:{status:"done",offset:0,limit:20},data:v(b({},F),{supportConversations:v(b({},(Zn=F.supportConversations)!=null?Zn:[]),{docs:[(Kn=o==null?void 0:o.data)==null?void 0:Kn.conversationUpdated,...N]})})}):s.writeQuery({query:Ee,variables:{status:"done",offset:0,limit:20},data:v(b({},V),{supportConversations:b({},(Jn=V.supportConversations)!=null?Jn:[])})})}},shouldResubscribe:!!e})};function Rl(e,n){const a=e.split(" ");return a.map((r,s)=>{if(r.length>1&&r.includes(":")){if(!n&&s===a.length-1)return r;const o=gr.search(r);return o.length>0?o[0].native:r}return r}).join(" ")}const ln=({setContent:e,userId:n,group:a,setConversationId:r})=>{const[s]=ae(),o=s==null?void 0:s.id,[l,{client:i,loading:d}]=Ae(Yi),f=c.exports.useCallback(async(p,A,E,C)=>{var I;const S=E==null?void 0:E.map(({type:y,fileId:T})=>({type:y,attachmentId:T})),{data:g}=await l({variables:{recipient:se({recipientType:C?"conversation":"user",conversationId:C||null,userId:C?null:n==null?void 0:n.toString(),group:C||a==="my chats"?"":a},y=>y),message:se({text:p.trim(),attachments:qe.exports.isEmpty(S)?null:S},y=>y)},update:(y,T)=>{var w;const M=y.readQuery({query:ke,variables:v(b({},se({conversationId:C,userId:C?"":n==null?void 0:n.toString(),limit:30},B=>B)),{offset:0})});if(M==null?void 0:M.messages){const B=((w=M==null?void 0:M.messages)==null?void 0:w.docs)||[],Q=B==null?void 0:B.map(D=>D.id===A?v(b(b({},D),T==null?void 0:T.data.sendMessage.lastMessage),{loading:!1}):D);y.writeQuery({query:ke,variables:v(b({},se({conversationId:C,userId:C?"":n==null?void 0:n.toString(),limit:30},D=>D)),{offset:0}),data:v(b({},M),{messages:v(b({},M==null?void 0:M.messages),{docs:Q})})})}}});C||await r((I=g==null?void 0:g.sendMessage)==null?void 0:I.id)},[l,n,a,o,s,r]),m=c.exports.useCallback(async(p,A)=>{if(A)try{await l({variables:{recipient:{conversationId:A,recipientType:"conversation"},senderAction:p}})}catch(E){console.log(E)}},[l]),u=c.exports.useCallback(async p=>{try{await l({variables:{recipient:se({recipientType:"conversation",conversationId:p||"",userId:p?"":n==null?void 0:n.toString()},A=>A),senderAction:"markSeen"}})}catch(A){console.log(A)}},[l,n]);return{onSendMessage:c.exports.useCallback(async({nativeEvent:p,conversationId:A,file:E})=>{var T,M;const C=v(b({},se({conversationId:A,userId:A?"":n==null?void 0:n.toString(),limit:30},w=>w)),{offset:0}),S=Rl(p.text,!0),g=$a(),I=i.cache.readQuery({query:ke,variables:b({},C)}),y=(M=(T=I==null?void 0:I.messages)==null?void 0:T.docs)!=null?M:[];try{e(""),(I==null?void 0:I.messages)&&i.cache.writeQuery({query:ke,variables:b({},C),data:v(b({},I),{messages:v(b({},I==null?void 0:I.messages),{docs:[{id:g,text:S,from:o,to:null,type:"update",attachments:[],seenBy:[],createdAt:we().format(),updatedAt:we().format(),deletedAt:null,callPayload:null,loading:!0,error:!1,conversation:{id:A,participants:[{userId:o,avatar:s.avatar,fullName:s.fullName}]},emojis:{userId:s==null?void 0:s.id,emoji:""}},...y]})})}),await f(S,g,E,A);const w=document.getElementById("chat-box");w&&(w.scrollTop=w==null?void 0:w.scrollHeight)}catch{i.cache.writeQuery({query:ke,variables:b({},C),data:v(b({},I),{messages:v(b({},I==null?void 0:I.messages),{docs:[{id:g,text:S,from:o,to:null,type:"update",attachments:[],seenBy:[],createdAt:we().format(),updatedAt:we().format(),deletedAt:null,callPayload:null,loading:!1,error:!0,conversation:{id:A,participants:[{userId:o,avatar:s.avatar,fullName:s.fullName}]},emojis:{userId:s==null?void 0:s.id,emoji:""}},...y]})})})}},[e,f,n,i]),onTyping:m,onMarkSeen:u,loading:d}},Dl=({time:e})=>t(P,{py:6,display:"flex",alignItems:"center",justifyContent:"center",w:"full",children:t(L,{fontSize:14,textAlign:"center",color:k("gray.400","slate.300"),children:St(e).format("HH:mm - DD-MM-YYYY")})}),uo=e=>{if(typeof e!="string"||!e)return!1;let n;try{n=new URL(e)}catch{return!1}return n.protocol==="http:"||n.protocol==="https:"},zl="https://getopengraph.herokuapp.com/",jl=new RegExp("^(?!mailto:)(?:(?:http|https|ftp)://)(?:\\S+(?::\\S*)?@)?(?:(?:(?:[1-9]\\d?|1\\d\\d|2[01]\\d|22[0-3])(?:\\.(?:1?\\d{1,2}|2[0-4]\\d|25[0-5])){2}(?:\\.(?:[0-9]\\d?|1\\d\\d|2[0-4]\\d|25[0-4]))|(?:(?:[a-z\\u00a1-\\uffff0-9]+-?)*[a-z\\u00a1-\\uffff0-9]+)(?:\\.(?:[a-z\\u00a1-\\uffff0-9]+-?)*[a-z\\u00a1-\\uffff0-9]+)*(?:\\.(?:[a-z\\u00a1-\\uffff]{2,})))|localhost)(?::\\d{2,5})?(?:(/|\\?|#)[^\\s]*)?$"),Ul=({url:e,isMyMessage:n})=>{const[a,r]=c.exports.useState(null),[s,o]=c.exports.useState(!1);c.exports.useEffect(()=>{e&&e.match(jl)!==null&&l(e)},[e]);const l=async d=>{o(!0);let f=await na.get(`${zl}?url=${d}`,{method:"GET"});r(f.data),o(!1)},i=c.exports.useMemo(()=>{var d;return h(R,{flexDirection:"column",rounded:"8px",children:[t(Ze,{_hover:{color:n?"white":void 0},fontSize:14,target:"_blank",href:e,cursor:"pointer",color:"black",textDecoration:"underline",onClick:()=>window.open(e),rel:"noreferrer",children:(a==null?void 0:a.url)||e}),((d=a==null?void 0:a.img)==null?void 0:d.length)>0&&t(Oe,{w:"100%",h:"150px",mt:4,alt:"img",src:a==null?void 0:a.img,rounded:"8px"}),t(Ze,{_hover:{color:n?"white":void 0},fontSize:14,target:"_blank",href:e,cursor:"pointer",color:"black",textDecoration:"underline",onClick:()=>window.open(e),rel:"noreferrer",children:(a==null?void 0:a.title)||"kh\xF4ng t\xECm th\u1EA5y"})]})},[a,e,n]);return s?t(Ze,{_hover:{color:n?"white":void 0},fontSize:14,target:"_blank",href:e,cursor:"pointer",color:"black",textDecoration:"underline",onClick:()=>window.open(e),rel:"noreferrer",children:e}):(a==null?void 0:a.status)===200&&a&&i},cn=e=>{if(!e)return;const n=(e==null?void 0:e.search("-"))+1;return e==null?void 0:e.slice(n)},Gl=({files:e,onPress:n,isMyMessage:a})=>(console.log(e,"?????"),t(Be,{maxW:{base:"250px"},columns:(e==null?void 0:e.length)>3?3:e==null?void 0:e.length,spacing:"8px",children:e==null?void 0:e.map((r,s)=>{var o,l,i,d;return t(P,{rounded:"lg",borderColor:k("gray.100","gray.300"),cursor:"pointer",overflow:"hidden",bg:a?"blue.50":"gray.100",display:"flex",alignItems:"center",justifyContent:"center",children:((o=r==null?void 0:r.attachment)==null?void 0:o.type)!=="file"?t(Oe,{objectFit:"cover",w:"80px",h:"80px",onClick:()=>n(s),src:(l=r==null?void 0:r.attachment)==null?void 0:l.fullUrl},s):t(Ze,{href:(i=r==null?void 0:r.attachment)==null?void 0:i.fullUrl,target:"_blank",children:h(R,{m:2,children:[t(P,{w:6,h:6,rounded:"full",display:"flex",alignItems:"center",justifyContent:"center",borderWidth:1,children:t(Sa,{className:"w-6 h-6 text-gray-700"})}),t(L,{flex:1,color:"gray.800",fontSize:14,children:cn((d=r==null?void 0:r.attachment)==null?void 0:d.path)})]})})},s)})})),Hl=({text:e,isMyMessage:n,deletedAt:a,createdAt:r,attachments:s,setAttachmentIndex:o,setShowImageViewer:l})=>{const i=_(u=>u.searchStringMessage),d=k("gray.100","slate.300"),f=k("gray.900","slate.900"),m=k("text-gray-100","text-slate-300");return c.exports.useMemo(()=>h(P,{pointerEvents:a?"none":"auto",py:{sm:1.5,base:3},px:3,rounded:"lg",maxW:{sm:"2/3",base:"210px",lg:"250px",xl:"280px","2xl":"400px"},bg:n?"blue.50":d,pos:"relative",children:[t(hr,{className:`${n?"text-blue-50":m} absolute ${n?"top-4 -right-2.5":"bottom-1 -left-2.5"} transform ${n?void 0:"rotate-180"}`}),a?t(L,{letterSpacing:.1,fontWeight:"500",fontSize:{base:"13px"},color:f,children:"Tin nh\u1EAFn \u0111\xE3 b\u1ECB x\xF3a"}):uo(e)?t(Ul,{url:e,isMyMessage:n}):h(P,{children:[!!(s==null?void 0:s.length)&&t(Gl,{isMyMessage:n,files:s,onPress:u=>{o(u),l(!0)}}),!!e&&t(L,{pt:(s==null?void 0:s.length)?2:0,letterSpacing:.1,fontSize:{base:"15px"},textAlign:"left",color:f,children:t(Ea,{searchWords:[i],autoEscape:!0,textToHighlight:e||"Tin nh\u1EAFn \u0111\xE3 b\u1ECB x\xF3a"})})]})]}),[uo,n,e,r,a,i,f,s])},Vl=()=>{const[e,{loading:n}]=Ae(Zi),a=Ne();return{onRemoveMessage:c.exports.useCallback(async s=>{try{await e({variables:{id:s}}),await a(["messages"])}catch(o){console.log(o)}},[e,a]),loading:n}},Wl=({isMyMessage:e,isHoverMessage:n,setIsHoverMessage:a,id:r})=>{const{onRemoveMessage:s}=Vl(),o=async()=>{await s(r),a(!1)};return e&&n&&h(Ht,{isLazy:!0,boundary:document.getElementById("chat-box"),children:[t(Vt,{display:"flex",alignItems:"center",justifyContent:"center",rounded:"full",cursor:"pointer",w:4,h:4,borderWidth:1,borderColor:k("blackAlpha.800","white"),children:t(P,{display:"flex",alignItems:"center",justifyContent:"center",children:t(va,{})})}),t(vt,{children:t(Wt,{alignItems:"center",py:4,children:t(Ia,{onClick:o,icon:t(st,{as:mr,w:4,h:4}),children:"X\xF3a tin nh\u1EAFn n\xE0y"})})})]})},Ql=({lastSeenMessageId:e,id:n,userData:a,isMyMessage:r,notSeen:s,loading:o,error:l,isGroup:i})=>{const d=k("white","slate.300");return c.exports.useMemo(()=>e===n&&!i?t(P,{w:4,h:4,rounded:"full",alignItems:"center",justifyContent:"center",display:"flex",flexDirection:"column",overflow:"hidden",bg:d,children:t(Ie,{src:a==null?void 0:a.avatar,name:a==null?void 0:a.fullName,size:"xs"})}):r&&s||o||e===n&&i?t(P,{w:4,h:4,rounded:"lg",alignItems:"center",justifyContent:"center",display:"flex",flexDirection:"column",bg:o||l?"transparent":"gray.400",border:o||l?"1px solid #838b8b":void 0,children:t(fr,{fill:"#fff",className:"w-3 h-3"})}):null,[e,n,l,o,s,r,i,a])},po=({attachments:e,attachmentIndex:n,setAttachmentIndex:a,showImageViewer:r,setShowImageViewer:s})=>t(P,{w:"100%",h:"100%",position:"relative",children:t(Ar,{imgs:e.map(o=>{var l;return{src:(l=o==null?void 0:o.attachment)==null?void 0:l.fullUrl}}),currImg:n,isOpen:r,onClose:()=>s(!1),showImgCount:!1,backdropCloseable:!0,onClickPrev:()=>{n>1&&a(n-1)},onClickNext:()=>{n<(e==null?void 0:e.length)-1&&a(n+1)}})}),Yl=({isParticipant:e,onOpen:n})=>e&&h(Ht,{isLazy:!0,boundary:document.getElementById("chat-box"),children:[t(Vt,{display:"flex",alignItems:"center",justifyContent:"center",rounded:"full",cursor:"pointer",borderWidth:1,borderColor:k("blackAlpha.800","white"),w:4,h:4,mt:"-20px",children:t(P,{display:"flex",alignItems:"center",justifyContent:"center",children:t(va,{})})}),t(vt,{children:t(Wt,{alignItems:"center",py:4,children:t(Ia,{onClick:()=>{n()},icon:t(st,{as:xr,w:4,h:4}),children:"\u0110\xE1nh d\u1EA5u c\xE2u h\u1ECFi"})})})]});var Zl="data:audio/mpeg;base64,SUQzBAAAAAACbFRFTkMAAAANAAADTG9naWMgUHJvIFgAVERSQwAAAAwAAAMyMDE0LTExLTA5AFRYWFgAAAARAAADY29kaW5nX2hpc3RvcnkAAFRYWFgAAAAaAAADdGltZV9yZWZlcmVuY2UAMTU4NzYwMDAwAFRYWFgAAAEJAAADdW1pZAAweDAwMDAwMDAwMTU2NDhEODQyMUZBMDBGQzcwQjY0MDU4RkY3RjAwMDAwMEZBODMwMDAwNjAwMDAwODA0RjEyMDAwMDAwMDAwMDM2QjI1NzA4MDEwMDAwMDA3MDcwRDI3OEZGN0YwMDAwQjBBRTQwNThGRjdGMDAwMEVBRTZEQzhFAFRTU0UAAAAPAAADTGF2ZjU4Ljc2LjEwMABDVE9DAAAACgAAdG9jAAMBY2gwAENIQVAAAAAsAABjaDAAAAAAAAAAAH3//////////1RJVDIAAAAOAAADVGVtcG86IDEyMC4wAAAAAAAAAAAAAAD/+5AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABJbmZvAAAADwAAAAYAAAtsAElJSUlJSUlJSUlJSUlJSUltbW1tbW1tbW1tbW1tbW1tbZKSkpKSkpKSkpKSkpKSkpK2tra2tra2tra2tra2tra2ttvb29vb29vb29vb29vb29vb/////////////////////wAAAABMYXZjNTguMTMAAAAAAAAAAAAAAAAkAzgAAAAAAAALbGkpEGgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD/+5BkAA/wAABpAAAACAAADSAAAAEAAAGkAAAAIAAANIAAAAQCAQFAIBgQMEAAAD/BAACCQB4xqZo+NkGUSf8wEYAgMBqAFDu03xU4g5WM/zAPQD8vkYrwEFmIthKuDAAhjAD9baEDbOW3iUxkyYAxiDgA5U0HA5jr79i4Yk+BiZFIBjpJOBgxO8BrYNx8ky5wMsouwMAYWgMOALAMT4GwMKwTv0G3hk4GBYIAGGAKQAwTA3AGAIAX/+KIAUBAqpgFAMIRL//9AskTWZm9Bv//////////5o1BaczN3V/////////sTZPuAgIAwGBAGGAAAAB/1Y8YLg3xjUCp/7iM0NXU5cz8CdP9/8jC+A5MJMEPPoAcVYoGbQ/03gYEBAGn1aBqMu/4ZGAJAINQgBigHf+AMIxahOAceJQ//WQwWQVFIf/7GmXGoFAoFAoFAoEAZKKAAAMIKGKjez/1FfxgTQqGYAGAHGALCRpsh0VoYNGG+mIkABBCATmJ5ADxCAgmH3AahiGRZsJAOBgAIHEJALhgCIDgcOyDxiTgHGHwAQ3/+5JkvoAAAABpBQAACAAADSCgAAEYfhEPmfsAAYCYorM9UAA0FEcwgwow4DCuVbNhMXqLiIEkZAI6DAITBqAAMMMCQwFwFjAIIoMg0feI0j9DwOTeeVQBRGAMYGQExgWgJmB2D2ZF4EZfSAK7OFbpbRyZYIaARvLNFASDARBJZKz8lAC76rs94aqrU5vxICyAaq52E/Ua/Yzqxr/bJP45Rn9VpN388P/+W//9//43h4z////////+z/+3AcDN9rI5Ta25/gAAAAwHAGCMflQT/gn5KZWweGGBTgusm+TeYHcAQGCABaHJJysBueN0AoN4kJagYFQSAYFimgZfQmV9gGAdCCwIA9AYcw299aguADbwDQFgND3IYqgl1HpVDWBbs1MyU3+hyQaF5/UAAC8GIwCEKjAUBLMJAL0wzgszEkLhO/oIgyAg+TDWCxMNUEYwOQkDAMA8QjSgfhfRUAAMD4J0DiM1A2ARQMEAMUIFzwpZEumJYDLgX6AwaXQNfCwGC4QVI4dykEiyKRDGoGAhOCygCw8R+NM6RNRFDYyI8G6Q//uSZOuAB347zu5/wARxZPttz9mAl12jLn3qgAC3CyfPttACKBYAoQBqgtGSS1IJGQyw0wuSLnK5gzv1FkWSOA4zJbrmQ5xiXzek/SMiaOvdm+kqm6Leup/1///////6kv9SkDb+vY1SAICgA12UspYiFBMbRAwJXSzZ9pjvP/TogOcL6jf9Q/hwHv9ZiWB/w6c+76P/sQAIAAAwFKU3CUBMwBAGAYE+YVgvBicpImFsEYYfgsY3DQdT1WbnmeZig0GEyGBOmYBgHKBIMYD1DJQDArq0UzZp4y4S5hRCa4kZOqSYzlqx4Unw1x05PjynnASMM+mAga9dpFKk9y+ihpaNAAkg1hY6iDX4fzz/Hf1Iaf5YZscMz0MRiUUljdqtGq9jPe/1reH9wrc1nb0xgKg24Waz///////9BAACAoAANAAsoR6UQAbayyXP4Z54kG31F1kzwbIRnsv500H8iku7f0i8cioqDjkfV8tBE7LoFXsGAKYfkcZZiwYTCeFAMMIQgMyH7NlsqMU0BMRCgMRgHSSdKhUFMCkNWWU3fiB52v/7kmSFioUjM0tL3dHAMcLZ7Q8tGBHg2Syu6S3QvgompBysYDxwn4FSOSSBAsAGjutAeROE8NUgMsCcl55RvsqpFMmncuYyhh0bcKHyzqAguOgwxRd665BK5+pdbkkpUsiaIR0VCIhNkCC/63/c9bLKWt0otwAjrv//////9ECAFAHdlaSazo1I0CGzQ6FQVVIFuNpKHU+r//hyJQDoaQHg6CZvw7nmAoFgG/+gcgIs63QPWQgOqgQ0vMcUGCIC+YPY7Bmlo4GWSbGYzYfZhXgemAAAQoG09fip4hhLpc/0klkttx6XwQ7agBn2RiABoTQxBPERD0xvnRoBhihgOENIg2Buy6tlczorfzUFuaj0XiROBgEOAl5ky09Gsqbv3Xhh/L3cOf3LeVWzKo1Go1PSqjltNet4WEGBOpCCwijf/////9B48AABwIYB9AIKYCcERkDZzDTtQMXXvUzun/e7/j9g3HCcRRKNQeCYaHI/QDQMgsLF/6gMYIF76tal+StyaykKXSMFAPMNyQMRzpNzGPOEU8NPDEMagoJUjYSDIGL/+5JkegLUxTFJCx7SIDJC6XkLS0QREMcgtdyAAMOMpIqycACWaGfpKszQV7FuWxrHe9xhXQMLDgDbqOtYYxCSzkUAKJpjlr2Jy+3bt51abHLHWd+l1nehqPsNWFZ1G12vQ+7oSl343Y3+H/zvOfv8t45VgVBUiEhdX///////8aKgI9Bn////6xxtRlI4RjnI6d6BoALUEwOBY1X1Tsd84UkABgDgNDYOBHEsmf/dgqeCQsXqDMZAAAiUKAuZAAAvVC2ixZp3oZLJGBwCYUAM25RgITmyKmDguYvei5R0AAFSUXKDioAEAwwMG/FgfJAwGgWGLw4MNlOitSJjHk4OoNsEfCCBNksTJMC5i8DQIE+buNgPSC94uMhxkbENMSdTTdM4kFlAhEI0EFw+oavI5iaJ4pG5RIsRBuXhS45AsAyhFRZZFTE1JpReWy/5dJ8uFsijE4WSfrRRYySSMf/mZsTheJxMqF83L5yjWpJTmK0f/+Thw8XzUvqK5gaFxEvmJ9VJ1LRWySkl1v////zQ2Ln/8aWAAEAAA/8Mfy/Knnfy//uSZHaABwJ2SeZyoABAw2kVzMgAi0Q43lyTAAiPhJnLhhAAZCa28XC8FWaBdURVEXKRtzc6fRo1so0SQ9KPz7GRXTYtHCADAfuwsML/6UnBr/+uhH//6YAGukSJEilcVURM8qCIImSIEgk4GARKnIkaimjQXEFNhDAoKbBWwU3I7//+BT8TYoL4VwK////i9BTcjoQV0F6C////hXAo6KbFBfCvO//4FeC4goqEMhBXgoiwBszMfqzGoCAkGAgkDQdKnUdYKuLHiwdiVYK/+s6JQ1BqHUxBTUUzLjEwMKqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqg==";const Kl=e=>{const n=new Audio(Zl);n.volume=1;const[a]=Ae(sl);return{onReactMessage:c.exports.useCallback(async(s,o)=>{try{await a({variables:{messageId:o,emoji:s}}),await e(!1),await n.play()}catch(l){console.log(l)}},[])}},Jl=["http://chat-media.zim.vn/628dd69bb37316154a08c0ee/like-removebg-preview.png","http://chat-media.zim.vn/628dd6c1b37316154a08c0f6/heart-removebg-preview.png","http://chat-media.zim.vn/628dd718b37316154a08c113/emoji__1_-removebg-preview.png","http://chat-media.zim.vn/628dd733b37316154a08c11c/emoji-removebg-preview.png","http://chat-media.zim.vn/628dd751b37316154a08c128/sad-removebg-preview.png","http://chat-media.zim.vn/628dd76bb37316154a08c12d/happy__1_-removebg-preview.png","http://chat-media.zim.vn/628dd6e4b37316154a08c107/angry-removebg-preview.png"],Xl=({setIsHoverMessage:e,id:n})=>{const{onReactMessage:a}=Kl(e);return h(Ht,{isLazy:!0,boundary:document.getElementById("chat-box"),children:[t(Vt,{display:"flex",alignItems:"center",justifyContent:"center",rounded:"full",cursor:"pointer",w:4,h:4,children:t(P,{display:"flex",alignItems:"center",justifyContent:"center",children:t(ka,{className:"w-full h-full"})})}),t(vt,{children:t(Wt,{display:"flex",alignItems:"center",justifyContent:"center",bg:"white",rounded:"md",shadow:"sm",zIndex:1e3,children:t(R,{alignItems:"center",children:Jl.map((r,s)=>t(P,{w:5,h:5,rounded:"full",bg:"white",display:"flex",alignItems:"center",justifyContent:"center",cursor:"pointer",onClick:()=>a(r,n),children:t(Oe,{_hover:{transition:"all 0.3s ease-in-out",transform:"scale(1.3)"},flex:1,src:r})},s))})})})]})},ec=c.exports.memo(({emojis:e})=>{var r,s;const[n,a]=c.exports.useState([]);return c.exports.useEffect(()=>{(async function(){try{const o=await lo(e==null?void 0:e.map(l=>l.userId),!1);a(o)}catch(o){console.log(o)}})()},[e]),t(R,{alignItems:"center",spacing:2,children:(s=(r=Et(e,"emoji"))==null?void 0:r.slice(0,3))==null?void 0:s.map(o=>t(Qt,{openDelay:300,bg:"green.400",p:2,hasArrow:!0,label:t(te,{maxW:"150px",children:h(R,{children:[t(ue,{children:e==null?void 0:e.map((l,i)=>t(P,{display:"flex",alignItems:"center",justifyContent:"center",w:4,h:4,cursor:"pointer",fontSize:"16px",children:t(Oe,{flex:1,src:l==null?void 0:l.emoji})},i))}),t(ue,{children:n==null?void 0:n.map((l,i)=>t(L,{color:"white",fontWeight:600,children:l==null?void 0:l.full_name},i))})]})}),children:t(P,{display:"flex",alignItems:"center",justifyContent:"center",w:4,h:4,cursor:"pointer",fontSize:"16px",children:t(Oe,{flex:1,src:o==null?void 0:o.emoji})})},o==null?void 0:o.emoji))})}),tc=br.extend({addAttributes(){var e,n,a,r,s;return v(b({},(e=this.parent)==null?void 0:e.call(this)),{backgroundColor:{default:(a=(n=this.options)==null?void 0:n.backgroundColor)!=null?a:null,parseHTML:o=>o.getAttribute("data-background-color"),renderHTML:o=>({"data-background-color":o.backgroundColor,style:`background-color: ${o.backgroundColor}`})},style:{default:(s=(r=this.options)==null?void 0:r.style)!=null?s:null,parseHTML:o=>o.getAttribute("colwidth"),renderHTML:o=>({style:`${o.style?`width: ${o.colwidth}px`:null}`,colspan:o.colspan,rowspan:o.rowspan,colwidth:o.colwidth})}})}}),nc=yr.extend({renderHTML({HTMLAttributes:e}){return["div",{class:"table-tiptap"},["table",e,["tbody",0]]]}}),ac=Cr.extend({content:"paragraph*",addAttributes(){var e;return v(b({},(e=this.parent)==null?void 0:e.call(this)),{cite:{default:this.options.cite},class:{default:this.options.class},title:{default:this.options.title}})}}),oc=Er.extend({content:"paragraph*",addAttributes(){var e,n,a,r,s,o,l,i;return v(b({},(e=this.parent)==null?void 0:e.call(this)),{rel:{default:(a=(n=this==null?void 0:this.options)==null?void 0:n.rel)!=null?a:"noopener nofollow noreferrer"},target:{default:(s=(r=this==null?void 0:this.options)==null?void 0:r.target)!=null?s:"_blank"},"data-contextual":{default:((o=this==null?void 0:this.options)==null?void 0:o["data-contextual"])||void 0},"data-contextual-tag":{default:((l=this==null?void 0:this.options)==null?void 0:l["data-contextual-tag"])||void 0},"data-contextual-tag-id":{default:((i=this==null?void 0:this.options)==null?void 0:i["data-contextual-tag-id"])||void 0}})}}),rc=$r.extend({addAttributes(){var e,n,a,r,s;return v(b({},(e=this.parent)==null?void 0:e.call(this)),{alt:{default:(a=(n=this==null?void 0:this.options)==null?void 0:n.alt)!=null?a:"image-alt"},title:{default:(s=(r=this==null?void 0:this.options)==null?void 0:r.title)!=null?s:"image-title"}})}}),sc=c.exports.forwardRef((i,l)=>{var d=i,{activeKey:e="",activeOptions:n={},isActive:a=!1,icon:r,label:s=""}=d,o=$e(d,["activeKey","activeOptions","isActive","icon","label"]);return r?t(Qt,{label:s,children:t(Sr,b({ref:l,colorScheme:a?"brand":"gray",variant:"solid",fontSize:"24px",icon:r},o))}):t(Qt,{label:s,children:t(Y,v(b({ref:l,colorScheme:a?"brand":"gray",variant:"solid"},o),{children:o.children}))})});var J=c.exports.memo(sc);const ic=[{name:"H1",value:1},{name:"H2",value:2},{name:"H3",value:3},{name:"H4",value:4},{name:"H5",value:5},{name:"H6",value:6}],go=c.exports.memo(()=>{const{editor:e}=c.exports.useContext(bo);return e?t(Se,{children:h(it,{children:[t(lt,{children:t(J,{label:"H tag",icon:t(vr,{})})}),t(ct,{zIndex:99,p:0,children:t(Yt,{p:2,children:t(ze,{gap:2,children:ic.map(n=>t(J,{isActive:e.isActive("heading",{level:n.value}),activeKey:"heading",activeOptions:{level:n.value},onClick:()=>e.chain().focus().toggleHeading({level:n.value}).run(),children:n.name},n.value))})})})]})}):null}),lc=c.exports.memo(({editor:e})=>{if(!e)return null;const n=c.exports.useCallback(()=>{e.chain().focus().insertTable({rows:3,cols:3,withHeaderRow:!0}).run()},[e]),a=c.exports.useCallback(()=>{e.chain().focus().deleteTable().run()},[e]),r=c.exports.useCallback(()=>{e.chain().focus().addColumnAfter().run()},[e]),s=c.exports.useCallback(()=>{e.chain().focus().addColumnBefore().run()},[e]),o=c.exports.useCallback(()=>{e.chain().focus().deleteColumn().run()},[e]),l=c.exports.useCallback(()=>{e.chain().focus().addRowBefore().run()},[e]),i=c.exports.useCallback(()=>{e.chain().focus().addRowAfter().run()},[e]),d=c.exports.useCallback(()=>{e.chain().focus().deleteRow().run()},[e]),f=c.exports.useCallback(()=>{e.chain().focus().mergeCells().run()},[e]),m=c.exports.useCallback(()=>{e.chain().focus().splitCell().run()},[e]),u=c.exports.useCallback(()=>{e.chain().focus().toggleHeaderColumn().run()},[e]);return h(Be,{minChildWidth:38,gap:2,p:2,bg:"gray.50",w:"full",maxW:350,children:[t(J,{onClick:n,icon:t(Ir,{}),label:"Th\xEAm table"}),t(J,{onClick:a,icon:t(kr,{}),label:"X\xF3a table"}),t(J,{onClick:r,icon:t(wr,{}),label:"Th\xEAm c\u1ED9t b\xEAn ph\u1EA3i"}),t(J,{onClick:s,icon:t(Tr,{}),label:"Th\xEAm c\u1ED9t b\xEAn tr\xE1i"}),t(J,{onClick:o,icon:t(Pr,{}),label:"X\xF3a c\u1ED9t"}),t(J,{onClick:l,icon:t(_r,{}),label:"Th\xEAm h\xE0ng ph\xEDa tr\xEAn"}),t(J,{onClick:i,icon:t(Mr,{}),label:"Th\xEAm h\xE0ng b\xEAn d\u01B0\u1EDBi"}),t(J,{onClick:d,icon:t(qr,{}),label:"X\xF3a h\xE0ng"}),t(J,{onClick:f,icon:t(Or,{}),label:"G\u1ED9p c\xE1c \xF4"}),t(J,{onClick:m,icon:t(Fr,{}),label:"T\xE1ch c\xE1c \xF4"}),t(J,{onClick:u,icon:t(Nr,{}),label:"Toggle header c\u1ED9t"}),t(J,{onClick:()=>e.chain().focus().toggleHeaderRow().run(),icon:t(Br,{}),label:"Toggle header h\xE0ng"}),t(J,{onClick:()=>e.chain().focus().toggleHeaderCell().run(),icon:t(Lr,{}),label:"Toggle header cell"})]})}),ho=c.exports.memo(({editor:e})=>e?h(it,{children:[t(lt,{children:t(J,{label:"Table",icon:t(Rr,{})})}),t(ct,{zIndex:99,p:0,children:t(Yt,{p:0,children:t(lc,{editor:e})})})]}):null),mo=e=>{const d=e,{isMultiple:n,children:a}=d,r=$e(d,["isMultiple","children"]),{getInputProps:s,getCheckboxProps:o}=n?ga(r):ha(r),l=s(),i=o();return h(P,{as:"label",rounded:"base",overflow:"hidden",d:"block",children:[t("input",v(b({},l),{style:{display:"none"}})),h(P,v(b({},i),{cursor:"pointer",opacity:.7,_checked:{borderColor:"red.500",opacity:1},_focus:{boxShadow:"none"},pos:"relative",children:[l.checked&&t(ma,{color:"green.500",fontSize:"2xl",pos:"absolute",top:2,left:2,zIndex:2}),a]}))]})},cc=async()=>{const e=Ue(),n=aa(e);(!Ue()||Ue()&&Date.now()>=n*1e3)&&await Ya()};async function dc(e){const a="https://graph-api.zim.vn/upload";try{if(e)return await cc(),await(await fetch(a,{method:"POST",credentials:"include",headers:{Authorization:`Bearer ${Ue()}`,Accept:"*/*","x-no-compression":"true"},body:e})).json()}catch(r){console.log("uploadFiles : "+r)}}const uc=x`
  fragment Media on Media {
    id
    type
    path
    variants {
      id
      width
      height
      path
      type
    }
    filename
    title
    visibility
    width
    height
  }
`,pc=x`
  query medias(
    $first: Int
    $after: String
    $type: [MediaType]
    $width: Int
    $height: Int
    $search: String
    $userId: Int
  ) @api(name: "zim") {
    medias(
      first: $first
      after: $after
      type: $type
      width: $width
      height: $height
      search: $search
      userId: $userId
    ) {
      edges {
        node {
          ...Media
        }
      }
      pageInfo {
        startCursor
        endCursor
        hasNextPage
      }
    }
  }
  ${uc}
`,gc=e=>e.scrollHeight-e.scrollTop<=e.clientHeight+10,hc={control:e=>v(b({},e),{minHeight:42,backgroundColor:"white",borderColor:"var(--chakra-colors-gray-200)",borderRadius:"var(--chakra-radii-md)",":hover":{borderColor:"var(--chakra-colors-gray-300)"}}),menuPortal:e=>v(b({},e),{zIndex:2e7}),container:e=>v(b({},e),{fontSize:"16px",width:"100%",":focus":{borderWidth:2,borderColor:"var(--chakra-colors-blue-500)"},zIndex:1e5}),IndicatorsContainer:e=>v(b({},e),{minHeight:40}),option:(e,{isDisabled:n,isSelected:a})=>v(b({},e),{color:n?"#ccc":a?"white":"black",cursor:n?"not-allowed":"default",fontSize:"15px",":active":b({},e[":active"])}),multiValue:e=>v(b({},e),{backgroundColor:"#ccc"}),multiValueLabel:e=>v(b({},e),{color:"var(--chakra-colors-gray-700)",backgroundColor:"var(--chakra-colors-gray-200)"}),multiValueRemove:e=>v(b({},e),{color:"var(--chakra-colors-red-200)",backgroundColor:"var(--chakra-colors-gray-200)",borderRadius:0,":hover":{color:"var(--chakra-colors-red-500)",backgroundColor:"var(--chakra-colors-red-100)"}})},mc=(e,n)=>{const[a,r]=c.exports.useState(e);return c.exports.useEffect(()=>{const s=setTimeout(()=>r(e),n);return()=>clearTimeout(s)},[e,n]),a},fo=[{label:"T\u1EC7p tin",value:"file"},{label:"H\xECnh \u1EA3nh",value:"image"}],fc=({selectedCallback:e,onClose:n,isOpen:a,isMultiple:r=!0,defaultTypes:s=["image"],enableTypeFilter:o,enableSizeFilter:l,width:i,height:d})=>{var he,fe,ee,xe,ye,Z,de,_e;const[f,m]=c.exports.useState(!1),[u,$]=c.exports.useState([]),[p,A]=c.exports.useState(!1),[E,C]=Dr(!0),[S,g]=c.exports.useState(""),I=mc(S,500),[y,T]=c.exports.useState([...s]),[M]=ae(),w={first:20,type:wa(y),search:I,height:0,width:0,userId:parseInt(M.id,10)};E&&i&&d&&(w.width=i,w.height=d);const{data:B,fetchMore:Q,refetch:D,loading:H}=ge(pc,{variables:w,notifyOnNetworkStatusChange:!0,skip:!a}),F=async()=>{m(!0);try{await D()}catch(G){console.log({e:G})}m(!1)},V=async()=>{const G=document.createElement("input");G.setAttribute("type","file"),y.includes("file")||G.setAttribute("accept","image/*"),G.click(),G.onchange=async()=>{const ie=G.files[0],Me=new FormData;Me.append("file",ie),Me.append("visibility","public"),await dc(Me),await F()}},{getCheckboxProps:O,setValue:U}=ya({onChange:$}),{getRootProps:W,getRadioProps:ne}=zr({name:"images",onChange:(...G)=>$(G)}),q=W(),z=()=>{var G;e((G=B==null?void 0:B.medias)==null?void 0:G.edges.filter(ie=>u.includes(ie.node.id))),U([]),n()},X=()=>{n(),U([])},be=G=>{A(gc(G.target))};return c.exports.useEffect(()=>{var G;p&&typeof Q=="function"&&((G=B.medias)==null?void 0:G.pageInfo.hasNextPage)&&!H&&(async()=>{var ie;await Q({variables:{after:(ie=B.medias)==null?void 0:ie.pageInfo.endCursor,first:20,type:wa(y)}})})()},[p]),t(Se,{children:h(Ve,{closeOnOverlayClick:!1,isCentered:!0,isOpen:a,onClose:X,size:"6xl",children:[t(Le,{}),h(We,{children:[t(Re,{children:"Th\u01B0 vi\u1EC7n media"}),t(Qe,{}),h(Ye,{children:[h(R,{mb:4,justifyContent:"space-between",children:[t(Y,{as:Ze,variant:"outline",colorScheme:"black",onClick:V,leftIcon:t(jr,{}),isLoading:f,children:"Select from computer"}),o&&t(P,{width:300,children:t(Ta,{placeholder:"Ch\u1ECDn lo\u1EA1i media",styles:hc,isMulti:!0,value:y.map(G=>fo.find(ie=>ie.value===G)).filter(G=>G),onChange:G=>{T(G.map(ie=>ie.value))},options:fo})}),l&&i&&d&&h(R,{children:[h(L,{children:["Ch\u1EC9 filter nh\u1EEFng \u1EA3nh size ",i," x ",d]}),t(Ur,{isChecked:E,onChange:C})]})]}),h(P,{width:350,pos:"relative",role:"group",children:[t(yt,{_focus:{color:"gray.900",borderColor:"gray.900"},_groupHover:{color:"gray.900",borderColor:"gray.900"},type:"text",placeholder:"T\xECm ki\u1EBFm media",size:"lg",pr:12,mb:4,value:S,onChange:G=>g(G.target.value)}),t(Gr,{_groupHover:{color:"gray.900"},pos:"absolute",right:4,top:"50%",color:"gray.400",transform:"translateY(-50%)"})]}),h(P,{position:"relative",h:"calc(65vh - 50px)",overflow:f?"hidden":"auto",w:"full",flexGrow:1,p:3,onScroll:be,children:[r?t(R,{alignItems:"stretch",flexWrap:"wrap",mx:"-0.75rem",spacing:0,children:((fe=(he=B==null?void 0:B.medias)==null?void 0:he.edges)==null?void 0:fe.length)>0&&((xe=(ee=B==null?void 0:B.medias)==null?void 0:ee.edges)==null?void 0:xe.map(({node:G})=>{const ie=O({value:G.id});return t(P,{w:{lg:"25%",base:"50%"},p:3,children:t(mo,v(b({isMultiple:r},ie),{children:t(Ao,b({},G))}))},`${G.id}`)}))}):t(R,v(b({alignItems:"stretch",flexWrap:"wrap",mx:"-0.75rem",spacing:0},q),{children:((Z=(ye=B==null?void 0:B.medias)==null?void 0:ye.edges)==null?void 0:Z.length)>0&&((_e=(de=B==null?void 0:B.medias)==null?void 0:de.edges)==null?void 0:_e.map(({node:G})=>{const ie=ne({value:G.id});return t(P,{w:{lg:"25%",base:"50%"},p:3,children:t(mo,v(b({isMultiple:!1},ie),{children:t(Ao,b({},G))}))},`${G.id}`)}))})),f&&h(R,{bgColor:"whiteAlpha.800",pos:"absolute",top:0,bottom:0,right:0,left:0,spacing:2,justifyContent:"center",children:[t(ot,{color:"black",size:"xl"}),t(L,{fontSize:"2xl",color:"black",children:"\u0110ang ti\u1EBFn h\xE0nh upload"})]})]})]}),h(rt,{children:[t(Y,{colorScheme:"blue",mr:3,disabled:!u.length,onClick:z,children:"X\xE1c nh\u1EADn"}),t(Y,{colorScheme:"gray",mr:0,onClick:X,children:"\u0110\xF3ng l\u1EA1i"})]})]})]})})};function Ao({type:e,path:n,filename:a}){return e==="file"?t(P,{display:"flex",flexDirection:"column",justifyContent:"center",borderWidth:2,borderColor:"#ccc",borderRadius:10,paddingX:"12px",w:"full",h:"150",children:h(te,{spacing:6,width:"full",overflow:"hidden",children:[t(Hr,{fontSize:"40"}),t(P,{w:"full",display:"table",style:{tableLayout:"fixed"},children:t(L,{display:"table-cell",textAlign:"center",whiteSpace:"nowrap",textOverflow:"ellipsis",overflow:"hidden",children:a})})]})}):t(P,{borderColor:"#ccc",borderRadius:10,borderWidth:2,w:"full",h:"150",backgroundSize:"cover",backgroundRepeat:"no-repeat",backgroundImage:`url('${Vr("https://graph-api.zim.vn",n)}')`})}const xo=c.exports.memo(({editor:e})=>{const n=i=>{i&&e.chain().focus().setImage({src:i}).run()},[a,r]=c.exports.useState(!1);return h(Se,{children:[t(J,{activeKey:"image",onClick:()=>{r(!0)},icon:t(Wr,{}),label:"H\xECnh \u1EA3nh"}),t(fc,{isOpen:a,onClose:()=>{r(!1)},selectedCallback:i=>{i.map(d=>{n(`https://graph-api.zim.vn/${d.node.path}`)})},isMultiple:!0})]})}),Ac=c.exports.memo(({editor:e,stickyMenuBar:n})=>{const a=k("white","slate.700"),r=k("gray.200","slate.600");return t(Se,{children:h(Be,{minChildWidth:"38px",gap:2,w:"full",bg:a,zIndex:10,px:2,py:2,pos:n?"sticky":"static",top:0,borderBottom:"1px solid",borderBottomColor:r,children:[t(go,{}),t(J,{onClick:()=>e.chain().focus().toggleBold().run(),activeKey:"bold",label:"In \u0111\u1EADm",icon:t(Qr,{})}),t(J,{onClick:()=>e.chain().focus().toggleItalic().run(),activeKey:"italic",label:"In nghi\xEAng",icon:t(Yr,{})}),t(J,{onClick:()=>e.chain().focus().toggleUnderline().run(),activeKey:"underline",label:"G\u1EA1ch ch\xE2n",icon:t(Zr,{})}),t(J,{onClick:()=>e.chain().focus().toggleStrike().run(),activeKey:"strike",label:"G\u1EA1ch ngang",icon:t(Kr,{})}),t(J,{onClick:()=>e.chain().focus().setTextAlign("left").run(),label:"C\u0103n l\u1EC1 tr\xE1i",icon:t(Jr,{})}),t(J,{onClick:()=>e.chain().focus().setTextAlign("center").run(),label:"C\u0103n gi\u1EEFa",icon:t(Xr,{})}),t(J,{onClick:()=>e.chain().focus().setTextAlign("right").run(),label:"C\u0103n l\u1EC1 ph\u1EA3i",icon:t(es,{})}),t(J,{onClick:()=>e.chain().focus().toggleBulletList().run(),label:"Danh s\xE1ch bullet",icon:t(Pa,{})}),t(J,{onClick:()=>e.chain().focus().toggleOrderedList().run(),label:"Danh s\xE1ch s\u1ED1 th\u1EE9 t\u1EF1",icon:t(_a,{})}),t(ho,{editor:e}),t(xo,{editor:e})]})})});c.exports.memo(({editor:e})=>h(Be,{minChildWidth:38,gap:2,p:2,bg:"gray.50",w:{base:300,sm:300,md:450},children:[t(go,{}),t(J,{onClick:()=>e.chain().focus().toggleBulletList().run(),label:"Danh s\xE1ch bullet",icon:t(Pa,{}),isActive:e.isActive("bulletList")}),t(J,{onClick:()=>e.chain().focus().toggleOrderedList().run(),label:"Danh s\xE1ch s\u1ED1 th\u1EE9 t\u1EF1",icon:t(_a,{}),isActive:e.isActive("orderedList")}),t(ho,{editor:e}),t(xo,{editor:e})]}));const xc=c.exports.memo(({editor:e})=>{const{onOpen:n,onClose:a,isOpen:r}=Pe(),s=c.exports.useRef(null),[o,l]=c.exports.useState(""),[i,d]=c.exports.useState(""),f=async()=>{const m=e.getAttributes("image").src;e.chain().focus().setImage({src:m,alt:i,title:o}).run(),a()};return c.exports.useEffect(()=>{e.isActive("image")?(d(e.getAttributes("image").alt),l(e.getAttributes("image").title)):(d(""),l(""))},[r]),h(R,{bg:"white",shadow:"base",p:2,spacing:4,rounded:4,children:[h(P,{fontSize:"sm",children:[h(L,{as:"div",children:[t("strong",{children:"Title:"})," ",e.getAttributes("image").title]}),h(L,{as:"div",children:[t("strong",{children:"Alt:"})," ",e.getAttributes("image").alt]})]}),h(it,{isOpen:r,initialFocusRef:s,onOpen:n,onClose:a,placement:"top",closeOnBlur:!0,children:[t(lt,{children:t(J,{activeKey:"image",label:"Ch\u1EC9nh s\u1EEDa",icon:t(ts,{})})}),t(ct,{children:t(P,{p:4,bg:"white",shadow:"base",children:h(te,{spacing:4,alignItems:"flex-start",children:[t(ve,{ref:s,label:"Title",id:"title-url",value:o,onChange:m=>l(m.target.value),autoComplete:"off"}),t(ve,{label:"Alt",id:"alt-url",value:i,onChange:m=>d(m.target.value),autoComplete:"off"}),t(Y,{colorScheme:"teal",onClick:f,children:"C\u1EADp nh\u1EADt"})]})})})]})]})});var bc=Zt.create({name:"iframe",group:"block",atom:!0,defaultOptions:{allowFullscreen:!0,HTMLAttributes:{class:"iframe-wrapper"}},addAttributes(){return{class:{default:null},src:{default:null},frameborder:{default:0},allowfullscreen:{default:this.options.allowFullscreen,parseHTML:()=>this.options.allowFullscreen},width:{default:this.options.width},height:{default:this.options.height}}},parseHTML(){return[{tag:"iframe"}]},renderHTML({HTMLAttributes:e}){return["div",this.options.HTMLAttributes,["iframe",e]]},addCommands(){return{setIframe:e=>({tr:n,dispatch:a})=>{const{selection:r}=n,s=this.type.create(e);return a&&n.replaceRangeWith(r.from,r.to,s),!0}}}});Zt.create({name:"video",group:"block",atom:!0,defaultOptions:{HTMLAttributes:{class:"iframe-wrapper"}},addAttributes(){return{class:{default:this.options.class},src:{default:null},autoplay:{default:this.options.width},loop:{default:this.options.loop},muted:{default:this.options.muted},preload:{default:this.options.preload},width:{default:this.options.width},height:{default:this.options.height},controls:{default:this.options.controls}}},parseHTML(){return[{tag:"video"}]},renderHTML({HTMLAttributes:e}){return["div",this.options.HTMLAttributes,["video",e]]},addCommands(){return{Video:e=>({tr:n,dispatch:a})=>{const{selection:r}=n,s=this.type.create(e);return a&&n.replaceRangeWith(r.from,r.to,s),!0}}}});var yc=Zt.create({name:"spacing",content:"inline*",atom:!0,draggable:!0,defaultOptions:{HTMLAttributes:{class:"space-wrapper","data-question":"","data-question-group":"","data-label":"","data-type":""}},addOptions(){return{inline:!0,HTMLAttributes:{}}},inline(){return this.options.inline},group(){return this.options.inline?"inline":"block"},addAttributes(){var e,n,a,r,s,o,l;return{class:{default:((e=this==null?void 0:this.options)==null?void 0:e.class)||void 0},"data-question":{default:(a=(n=this==null?void 0:this.options)==null?void 0:n["data-question"])!=null?a:"",parseHTML:i=>i.getAttribute("data-question")},"data-question-group":{default:(s=(r=this==null?void 0:this.options)==null?void 0:r["data-question-group"])!=null?s:"",parseHTML:i=>i.getAttribute("data-question-group")},"data-type":{default:(l=(o=this==null?void 0:this.options)==null?void 0:o["data-type"])!=null?l:"",parseHTML:i=>i.getAttribute("data-type")},"data-label":{default:"",parseHTML:i=>i.getAttribute("data-label"),renderHTML:i=>i["data-label"]?{"data-label":i["data-label"]}:{"data-label":""}}}},parseHTML(){return[{tag:"span.space-wrapper"}]},renderHTML({HTMLAttributes:e}){return["span",ns(this.options.HTMLAttributes,e)]},renderText({HTMLAttributes:e}){var n;return(n=e==null?void 0:e["data-label"])!=null?n:"r\u1ED7ng"},addCommands(){return{addSpace:e=>({commands:n})=>n.insertContent({type:this.name,attrs:e})}}});const bo=c.exports.createContext({editor:null}),Cc=c.exports.forwardRef(({onChange:e,placeholder:n="Nh\u1EADp n\u1ED9i dung",defaultValue:a="",extensions:r=[],enableSpaceMenu:s=!1,showWordCount:o=!0,isDisabled:l=!1,stickyMenuBar:i=!1},d)=>{const[f,m]=c.exports.useState(0),u=as({extensions:[os,nc.configure({resizable:!0}),rs,ss,tc,ac,is,ls,rc.configure({inline:!0,HTMLAttributes:{class:"custom-image"}}),oc.configure({openOnClick:!1}),cs.configure({placeholder:n||"Nh\u1EADp n\u1ED9i dung",emptyEditorClass:"is-editor-empty"}),bc.configure({inline:!0}),ds.configure({types:["heading","paragraph"],alignments:["left","right","center"]}),yc.configure({inline:!0}),...r],content:a,onCreate:async({editor:E})=>{const C=E.state.doc.textContent.split(" ").length;m(C)},onUpdate:({editor:E})=>{const C=E.state.doc.textContent.split(" ").length;m(C);const S=E.getHTML();typeof e=="function"&&e(S)},editable:!l}),$=k("white","slate.700"),p=k("gray.200","slate.700"),A=ft(u,1e3);return h(bo.Provider,{value:{editor:A},children:[o&&h(P,{mb:4,children:["Word count: ",t("strong",{children:f})]}),t(P,{p:2,pt:0,borderColor:p,borderRadius:4,borderWidth:1,h:"full",bg:$,children:u&&h(Se,{children:[!l&&h(Se,{children:[t(Ac,{editor:u,enableSpaceMenu:s,stickyMenuBar:i}),t(Ma,{editor:u,pluginKey:"bubbleImageMenu",shouldShow:({editor:E})=>E.isActive("image"),children:t(xc,{editor:u})}),t(Ma,{editor:u,tippyOptions:{placement:"bottom"},pluginKey:"bubbleTextSelection",shouldShow:({editor:E,view:C,state:S})=>E.isActive("link",{"data-contextual":!0})})]}),t(us,{editor:u,ref:d})]})})]})});var yo=c.exports.memo(Cc);function Co(){const e=gs(),n=hs(),a=ms();return c.exports.useMemo(()=>({navigate:a,pathname:n.pathname,query:b(b({},ps.parse(n.search)),e),location:n}),[e,n,a])}const Ec=c.exports.forwardRef((l,o)=>{var i=l,{onChange:e,paramKey:n,options:a,allOption:r}=i,s=$e(i,["onChange","paramKey","options","allOption"]);var $;fs();const d=Co();De();const f=c.exports.useMemo(()=>({menuPortal:p=>v(b({},p),{zIndex:1400}),container:(p,A)=>v(b({},p),{color:k("var(--chakra-colors-gray-900)","var(--chakra-colors-slate-400)")}),placeholder:(p,A)=>v(b({},p),{color:k("var(--chakra-colors-gray-400)","var(--chakra-colors-slate-500)")}),input:(p,A)=>v(b({},p),{minHeight:30,color:k("var(--chakra-colors-gray-900)","var(--chakra-colors-slate-400)")}),control:(p,A)=>v(b({},p),{backgroundColor:A.isDisabled?"var(--chakra-colors-gray.500)":k("var(--chakra-colors-white)","var(--chakra-colors-slate-700)"),borderRadius:"var(--chakra-radii-md)",borderColor:k("var(--chakra-colors-gray.300)","var(--chakra-colors-slate-600)")}),menu:(p,A)=>v(b({},p),{zIndex:3}),menuList:(p,A)=>v(b({},p),{backgroundColor:k("var(--chakra-colors-white)","var(--chakra-colors-slate-700)")}),option:(p,A)=>v(b({},p),{color:A.isFocused?k("var(--chakra-colors-gray.900)","var(--chakra-colors-slate-900)"):k("var(--chakra-colors-gray.900)","currentColor"),"&:hover":{color:A.isSelected?k("var(--chakra-colors-white)","var(--chakra-colors-white)"):k("var(--chakra-colors-gray.900)","var(--chakra-colors-slate-900)")}}),singleValue:(p,A)=>v(b({},p),{color:k("var(--chakra-colors-gray.900)","var(--chakra-colors-slate-300)")})}),[]),m=c.exports.useCallback((p,A)=>{e(p,A)},[s,d.query,n]),u=c.exports.useMemo(()=>r?[r,...a]:a,[s,a,r]);return h(bt,{isInvalid:!!(s==null?void 0:s.error),width:"100%",children:[(s==null?void 0:s.label)&&t(Lt,{htmlFor:s.id,children:s==null?void 0:s.label}),t(Ta,v(b({ref:o,id:s.id},s),{styles:f,onChange:m,options:u,menuPortalTarget:document.body})),(s==null?void 0:s.error)&&t(Ct,{children:($=s==null?void 0:s.error)==null?void 0:$.message})]})});var _t=c.exports.memo(Ec);const $c=({questionText:e})=>{var o;const{data:n}=ge(bi),{register:a,formState:{errors:r}}=qa(),s=(o=n==null?void 0:n.QAPostCategoriesTree)!=null?o:[];return h(ze,{direction:"column",w:"full",justifyContent:"stretch",alignItems:"stretch",className:"reply-editor",gap:4,children:[t(ve,v(b({label:"Ti\xEAu \u0111\u1EC1",placeholder:"Nh\u1EADp ti\xEAu \u0111\u1EC1..."},a("title",{required:!0})),{error:r==null?void 0:r.name})),h(ue,{spacing:1,children:[t(L,{fontSize:14,children:"C\xE2u h\u1ECFi"}),t(P,{fontWeight:"400",bg:"gray.100",borderWidth:1,w:"100%",p:4,color:"black",rounded:"md",children:e})]}),t(dt,{rules:{required:"Vui l\xF2ng ch\u1ECDn ch\u1EE7 \u0111\u1EC1"},name:"categories",render:({field:l,fieldState:{error:i}})=>t(_t,b({error:i,label:"Ch\u1EE7 \u0111\u1EC1",options:s,getOptionLabel:d=>d.title,getOptionValue:d=>d.id+"",formatOptionLabel:d=>t(L,{children:d.title})},l))}),h(ue,{spacing:1,children:[t(L,{fontWeight:"400",fontSize:14,children:"C\xE2u tr\u1EA3 l\u1EDDi"}),t(dt,{name:"answer",render:({field:l})=>t(yo,b({showWordCount:!1,placeholder:"Nh\u1EADp c\xE2u tr\u1EA3 l\u1EDDi..."},l))})]})]})},Sc=({onClose:e,isOpen:n,questionText:a,messageQAOwnerId:r,setIsHoverMessage:s})=>{const o=c.exports.useRef(),l=$t({mode:"onBlur",reValidateMode:"onChange"}),i=Te(),[d]=Ae(ol),[f,{loading:m}]=Ae(rl),u=c.exports.useCallback(async({title:$,categories:p,answer:A})=>{var E;try{const{data:C}=await d({variables:{input:se({title:$,content:a,type:"QUESTION",categoryIds:(p==null?void 0:p.id)?[p==null?void 0:p.id]:""},S=>S),userId:r}});await f({variables:{ref:(E=C==null?void 0:C.createQAPost)==null?void 0:E.id,content:A,type:"QAPost"}}),e(),s(!1),await i({title:"Success.",description:"\u0110\u1EA9y c\xE2u h\u1ECFi l\xEAn forum th\xE0nh c\xF4ng",status:"success",duration:3e3,isClosable:!0})}catch(C){console.log(C)}},[a,r,e,s]);return t(jt,v(b({},l),{children:h(Ve,{finalFocusRef:o,isOpen:n,onClose:e,size:"3xl",children:[t(Le,{}),h(We,{children:[t(Re,{children:"T\u1EA1o QA "}),t(Qe,{}),h("form",{onSubmit:l.handleSubmit(u),children:[t(Ye,{children:t($c,{questionText:a})}),h(rt,{children:[t(Y,{leftIcon:t(st,{as:Ut}),colorScheme:"green",mr:4,type:"submit",isLoading:m,children:"X\xE1c nh\u1EADn"}),t(Y,{variant:"ghost",onClick:e,children:"H\u1EE7y"})]})]})]})]})}))};function vc({isMyMessage:e,text:n,loading:a,notSeen:r,error:s,createdAt:o,lastSeenMessageId:l,id:i,userData:d,attachments:f,deletedAt:m,from:u,isGroup:$,conversation:p,emojis:A}){var V,O,U;const[E]=ae(),{isOpen:C,onOpen:S,onClose:g}=Pe(),[I,y]=c.exports.useState(!1),T=ft(I,300),[M,w]=c.exports.useState(null),[B,Q]=c.exports.useState(!1),D=k("white","slate.300"),H=(V=p==null?void 0:p.participants)==null?void 0:V.find(W=>(W==null?void 0:W.userId)===u),F=c.exports.useMemo(()=>{var W;return t(P,{as:"div",w:6,h:6,rounded:"full",alignItems:"center",justifyContent:"center",display:"flex",flexDirection:"column",position:"absolute",overflow:"hidden",bg:D,className:`${e?"-right-8 top-3":"-left-[30px] bottom-6"}`,children:t(Ie,{size:"xs",src:H==null?void 0:H.avatar,name:(W=H==null?void 0:H.fullName)!=null?W:"\u1EA8n danh"})})},[e,D,H==null?void 0:H.avatar,H==null?void 0:H.fullName]);return h(Se,{children:[t(R,{onMouseEnter:()=>!m&&y(!0),onMouseLeave:()=>!m&&y(!1),w:"full",position:"relative",alignItems:"center",justifyContent:e?"flex-end":"flex-start",children:t(R,{w:"full",alignItems:"center",children:t(P,{w:"full",position:"relative",display:"flex",justifyContent:e?"flex-end":"flex-start ",children:h(P,{w:"full",display:"flex",flexDirection:"column",alignItems:e?"flex-end":"flex-start",children:[F,!e&&t(L,{color:"gray.400",fontSize:12,children:((U=(O=p==null?void 0:p.participants)==null?void 0:O.find(W=>(W==null?void 0:W.userId)===u))==null?void 0:U.fullName)||"\u1EA8n danh"}),h(it,{isOpen:T,placement:e?"left":"right",children:[t(lt,{children:t(R,{align:"center",children:t(Hl,{isMyMessage:e,text:n,deletedAt:m,createdAt:o,attachments:f,setAttachmentIndex:w,setShowImageViewer:Q})})}),t(ct,{w:"65px",bg:"transparent",border:0,boxShadow:"none",_focus:{outline:"none"},children:T&&h(R,{alignItems:"center",flexGrow:1,justifyContent:e?"flex-end":"flex-start",children:[t(Wl,{isMyMessage:e,isHoverMessage:!0,setIsHoverMessage:y,id:i}),t(Xl,{id:i,setIsHoverMessage:y}),pn.map(W=>W.id).includes(E.roleId)&&n&&t(Yl,{isParticipant:!e,onOpen:S})]})})]}),h(R,{w:"full",justify:e?"flex-end":"flex-start",mt:1,alignItems:"center",spacing:2,children:[t(L,{letterSpacing:.1,fontSize:12,color:"gray.500",children:we(o).format("HH:mm")}),t(Ql,{isGroup:$,isMyMessage:e,lastSeenMessageId:l,notSeen:r,id:i,error:s,loading:a,userData:d}),!m&&(A==null?void 0:A.length)>0&&t(ec,{emojis:A})]}),t(po,{setAttachmentIndex:w,attachments:f,attachmentIndex:M,showImageViewer:B,setShowImageViewer:Q})]})})})}),t(Sc,{isOpen:C,onClose:g,questionText:n,messageQAOwnerId:u,setIsHoverMessage:y})]})}var Ic=c.exports.memo(vc);const kc=({conversationId:e,setConversationId:n,ownerId:a,messages:r,participants:s,userId:o,isGroup:l,loading:i,channel:d,isLoadMore:f,onLoadBeforeMessage:m,hasBeforeLoadMoreButton:u,searchLoading:$,onLoadAfterMessage:p,hasAfterLoadMoreButton:A,chatBoxRef:E,totalDocs:C})=>{var V;const[S,g]=c.exports.useState(null),I=_(O=>O.searchStringMessage),[y,T]=c.exports.useState(""),{onMarkSeen:M}=ln({setContent:()=>{},userId:o,group:d,setConversationId:n}),w=c.exports.useMemo(()=>qe.exports.uniqBy(qe.exports.reverse([].concat(r)),O=>O.id),[r]),B=(V=r.find(O=>{var U;if(O.from===a&&((U=O.seenBy)==null?void 0:U.length)>=2)return!0}))==null?void 0:V.id,Q=s,D=Q==null?void 0:Q.filter(O=>O.userId!==a)[0],H=w[w.length-1],F=As();return zt(()=>{var O;e&&(s==null?void 0:s.find(U=>U.userId===a))&&!i&&!((O=H==null?void 0:H.seenBy)==null?void 0:O.includes(a==null?void 0:a.toString()))&&async function(){await F(M(e))}()},[F,a,H,s,i,e]),c.exports.useLayoutEffect(()=>{g(null)},[e]),c.exports.useEffect(()=>{let O;return(E==null?void 0:E.current)&&!I&&S!==e&&(O=setTimeout(()=>{var U;E.current.scrollTop=(U=E.current.scrollHeight)!=null?U:5e3,g(e)},50)),()=>{clearTimeout(O)}},[e,S,I]),h(te,{alignItems:"stretch",w:"full",px:10,py:4,spacing:4,children:[u&&C>=(w==null?void 0:w.length)&&t(te,{children:t(Y,{w:"100px",h:"30px",onClick:m,isLoading:$,loadingText:"Loading",colorScheme:"teal",variant:"outline",spinnerPlacement:"end",children:"Load more"})}),f&&t(xs,{in:f,animateOpacity:!0,children:t(P,{pt:2,children:t(At,{text:"\u0110ang t\u1EA3i tin nh\u1EAFn..."})})}),w==null?void 0:w.map((O,U)=>{var he,fe,ee,xe,ye;const W=O.from===a,ne=St((he=w[U])==null?void 0:he.createdAt),q=St((fe=w[U-1])==null?void 0:fe.createdAt),z=St.duration(ne.diff(q)),X=z.asMinutes(),be=X>15&&t(Dl,{time:O==null?void 0:O.createdAt});return h(He.Fragment,{children:[be,t(Ic,v(b({},O),{QAId:y,setQAId:T,isGroup:l,attachments:(ee=O==null?void 0:O.attachments)!=null?ee:[],loading:O==null?void 0:O.loading,error:O==null?void 0:O.error,isMyMessage:W,text:O==null?void 0:O.text,id:O==null?void 0:O.id,userData:D,lastSeenMessageId:B,createdAt:O==null?void 0:O.createdAt,notSeen:((xe=O==null?void 0:O.seenBy)==null?void 0:xe.length)===1&&((ye=O==null?void 0:O.seenBy)==null?void 0:ye.includes(a))}))]},U)}),A&&(w==null?void 0:w.length)<C&&t(te,{pb:2,children:t(Y,{w:"100px",h:"30px",onClick:p,isLoading:$,loadingText:"Loading",colorScheme:"teal",variant:"outline",spinnerPlacement:"end",children:"Load more"})})]})},wc=({userId:e})=>{var C,S,g;const[n]=ae(),a=Ne(),[r,s]=c.exports.useState(!1),o=_(I=>I.conversationId),l=_(I=>I.setConversationId),i=v(b({},se({conversationId:o,userId:o?"":e==null?void 0:e.toString(),limit:30},I=>I)),{offset:0}),{data:d,loading:f,fetchMore:m,error:u}=ge(ke,{variables:b({},i),notifyOnNetworkStatusChange:!0,skip:!o&&!e});Rt(Ja,{onSubscriptionData:({client:I,subscriptionData:y})=>{var M,w;const T=I.cache.readQuery({query:ke,variables:i});if(T==null?void 0:T.messages){let Q=[...(w=(M=T==null?void 0:T.messages)==null?void 0:M.docs)!=null?w:[]];const{conversationUpdated:D}=y.data,{lastMessage:H,isDeleteMessage:F,id:V}=D,{id:O,from:U}=H,W=Q==null?void 0:Q.find(ne=>(ne==null?void 0:ne.id)===O);F&&!r&&o===V&&s(!0),!W&&n.id!==U&&o===V&&(I.writeQuery({query:ke,variables:i,data:v(b({},T),{messages:v(b({},T.messages),{docs:[H,...Q]})})}),setTimeout(()=>{const ne=document.getElementById("chat-box");ne&&(ne.scrollTop=ne.scrollHeight)},50))}},shouldResubscribe:!!o});const $=((C=d==null?void 0:d.messages)==null?void 0:C.docs)||[],p=(S=d==null?void 0:d.messages)==null?void 0:S.hasNextPage,{onLoadMore:A,isLoadingMore:E}=Ge({hasNextPage:p,variables:v(b({},i),{offset:$.length+1,limit:10}),fetchMore:m});return c.exports.useEffect(()=>{r&&(a(["messages","getAttachmentsInConversation"]),s(!1))},[r,a,s]),c.exports.useEffect(()=>{var I,y;($==null?void 0:$.length)>0&&l((y=(I=$==null?void 0:$[0])==null?void 0:I.conversation)==null?void 0:y.id)},[$]),{data:d,loading:f,isLoadMore:E,loadMore:A,hasNextPage:p,messages:$,error:u,totalDocs:(g=d==null?void 0:d.messages)==null?void 0:g.totalDocs}},Tc=({userId:e,options:n})=>{var $,p,A;const a=_(E=>E.conversationId),r=_(E=>E.beforeId),s=_(E=>E.afterId),o=b({},se({conversationId:a,userId:a?"":e==null?void 0:e.toString(),limit:10,before:r,after:s},E=>E)),{data:l,loading:i,error:d}=ge(ke,v(b({variables:b({},o)},n),{skip:!r&&!s})),f=(($=l==null?void 0:l.messages)==null?void 0:$.docs)||[],m=(p=l==null?void 0:l.messages)==null?void 0:p.hasNextPage,u=(A=l==null?void 0:l.messages)==null?void 0:A.hasPrevPage;return{data:l,loading:i,hasNextPage:m,hasPrevPage:u,messages:f,error:d}},Eo=({conversationId:e="",search:n})=>{var E,C;const a=_(S=>S.setSearchResult),r=_(S=>S.messageId),s=_(S=>S.beforeId),[o,l]=c.exports.useState(!1),{refetch:i,called:d,loading:f}=ge(xi,{variables:{conversationId:e,search:n},onCompleted:S=>{var g;a((g=S==null?void 0:S.searchMessage)==null?void 0:g.docs),l(!0)},onError:()=>{a([])},skip:e.length===0||!!r||!!s||!n}),{data:m,refetch:u,loading:$}=ge(wt,{variables:{messageId:r,limit:10},skip:!r}),p=(E=m==null?void 0:m.nearbyMessages)==null?void 0:E.hasPrevPage,A=(C=m==null?void 0:m.nearbyMessages)==null?void 0:C.hasNextPage;return c.exports.useEffect(()=>{(d&&n.length||d&&o&&(n==null?void 0:n.length)===0)&&i()},[n,o,d]),c.exports.useEffect(()=>{d&&r&&u()},[r,d]),{loading:$,hasPrevPageNearbyMessages:p,hasNextPageNearbyMessages:A,getSearchResultLoading:f}},Pc=({conversationId:e,ownerId:n,userId:a,onSendMessage:r,participants:s,isGroup:o,setConversationId:l,channel:i,sendMessageLoading:d})=>{var ee,xe,ye;const f=ba(),m=c.exports.useRef(0),u=c.exports.useRef(0),$=c.exports.useRef(null),p=c.exports.useCallback(async()=>{d||await r({nativeEvent:{text:"Hi !!"},conversationId:e})},[r,e,d]),[A,E]=c.exports.useState(!1),C=_(Z=>Z.searchStringMessage),S=_(Z=>Z.setBeforeId),g=_(Z=>Z.setAfterId),I=_(Z=>Z.messageId),y=_(Z=>Z.beforeId),T=_(Z=>Z.afterId),{messages:M,loading:w,error:B,loadMore:Q,isLoadMore:D,hasNextPage:H,totalDocs:F}=wc({userId:a}),{hasPrevPageNearbyMessages:V,hasNextPageNearbyMessages:O,getSearchResultLoading:U}=Eo({conversationId:e,search:C}),{hasNextPage:W,hasPrevPage:ne}=Tc({userId:a,options:{onCompleted:Z=>{var Xe,et,tt,nt,at;E(!0);const de=(Xe=f.cache.readQuery({query:wt,variables:{messageId:I,limit:10}}))!=null?Xe:{},_e=(tt=(et=de==null?void 0:de.nearbyMessages)==null?void 0:et.docs)!=null?tt:[],G=(at=(nt=Z==null?void 0:Z.messages)==null?void 0:nt.docs)!=null?at:[],ie=y?[..._e,...G]:[...G,..._e],Me=Et(ie,"id");u.current=$.current.scrollHeight,f.cache.writeQuery({query:wt,variables:{messageId:I,limit:10},data:v(b({},de),{nearbyMessages:v(b({},de==null?void 0:de.nearbyMessages),{docs:Me})})}),E(!1),y&&($.current.scrollTop=$.current.scrollHeight-u.current)}}}),q=(ee=f.cache.readQuery({query:wt,variables:{messageId:I,limit:10}}))!=null?ee:{},z=(ye=(xe=q==null?void 0:q.nearbyMessages)==null?void 0:xe.docs)!=null?ye:[],X=(C==null?void 0:C.length)>0&&(I==null?void 0:I.length)>0?z:M,be=c.exports.useCallback(()=>{var Z;g(""),S((Z=z==null?void 0:z[z.length-1])==null?void 0:Z.id)},[z]),he=c.exports.useCallback(()=>{var Z;S(""),g((Z=z==null?void 0:z[0])==null?void 0:Z.id)},[z]),fe=c.exports.useCallback(async Z=>{const{scrollTop:de}=Z.nativeEvent.target;de<=20&&de<m.current&&H&&!D&&!C&&(await Q(),D||($.current.scrollTop=de+60)),m.current=de},[H,D,C]);return B?t(R,{bg:"blackAlpha.400",h:"100%",justifyContent:"center",children:t(At,{color:"white"})}):t(P,{ref:$,bg:k("white","#10172a"),h:"full",w:"full",id:"chat-box",overflow:"auto",position:"relative",onScroll:fe,children:(X==null?void 0:X.length)===0&&!U?h(te,{w:"100%",h:"100%",display:"flex",flexDirection:"column",alignItems:"center",justifyContent:"center",spacing:8,children:[t(te,{w:"40%",spacing:4,maxW:"360px",maxH:"400px",children:t(Oe,{src:co,objectFit:"contain"})}),(e||a)&&(X==null?void 0:X.length)===0&&!w&&!d&&t(P,{h:10,px:2,bg:"red.400",rounded:"lg",as:"button",onClick:p,fontSize:14,color:"white",fontWeight:500,children:"G\u1EEDi l\u1EDDi ch\xE0o"})]}):U?t(At,{}):t(P,{h:"full",w:"full",children:t(kc,{totalDocs:F,chatBoxRef:$,hasBeforeLoadMoreButton:(!y&&V||ne)&&(C==null?void 0:C.length)>0,hasAfterLoadMoreButton:(!T&&O||W)&&(C==null?void 0:C.length)>0,onLoadBeforeMessage:be,onLoadAfterMessage:he,isLoadMore:D,searchLoading:A,loading:w,isGroup:o,ownerId:n,messages:X,conversationId:e,participants:s,userId:a,setConversationId:l,channel:i})})})};x`
  mutation updateMedia($id: ObjectID!, $input: UpdateMediaInput!)
  @api(name: "zim") {
    updateMedia(id: $id, input: $input) {
      id
      title
    }
  }
`;x`
  mutation deleteMedia($id: ObjectID!) @api(name: "zim") {
    deleteMedia(id: $id)
  }
`;x`
  mutation uploadImages($file: [Upload]) @api(name: "appZim") {
    uploadImages(file: $file)
  }
`;const _c=x`
  mutation singleUpload($file: Upload!) @api(name: "chat") {
    singleUpload(file: $file) {
      id
      path
      fullUrl
      type
    }
  }
`,Wu=x`
  mutation uploadExamDocument($file: Upload!, $folderName: String)
  @api(name: "chat") {
    uploadExamDocument(file: $file, folderName: $folderName) {
      id
      path
      fullUrl
      type
    }
  }
`,Mc=({setMediaUrls:e})=>{const[n,{loading:a}]=Ae(_c),r=c.exports.useCallback(async o=>{var i,d,f;const l=$a();try{await e.push({fileId:l,url:URL.createObjectURL(o),name:o.name,loading:!0,progress:0,type:((i=o==null?void 0:o.type)==null?void 0:i.includes("video"))?"video":((d=o==null?void 0:o.type)==null?void 0:d.includes("image"))?"image":"file"});const{data:m}=await n({variables:{file:o},context:{fetchOptions:{onUploadProgress:u=>{var $,p;e.update(A=>A.fileId===l,{fileId:l,url:URL.createObjectURL(o),name:o.name,loading:!0,progress:u.loaded/u.total,type:(($=o==null?void 0:o.type)==null?void 0:$.includes("video"))?"video":((p=o==null?void 0:o.type)==null?void 0:p.includes("image"))?"image":"file"})}}}});await e.update(u=>u.fileId===l,{fileId:m.singleUpload.id,name:m.singleUpload.path,url:URL.createObjectURL(o),type:o.type.includes("video")?"video":((f=o==null?void 0:o.type)==null?void 0:f.includes("image"))?"image":"file"})}catch(m){console.log(m)}},[n]);return{doUploadImage:c.exports.useCallback(async o=>{await Promise.all(o.map(r))},[r]),loading:a}},qc=le(()=>ce(()=>import("./index29.js").then(function(e){return e.i}),["assets/index29.js","assets/vendor.js"])),Oc=1048576*2,Fc=({conversationId:e,setConversationId:n,userId:a,group:r,textAreaRef:s,file:o,setFile:l,conversationDetail:i,isMyTyping:d})=>{var f,m,u,$;{const p=Te(),[A]=ae(),E=c.exports.useRef(null),C=k("gray.900","slate.300"),[S,g]=c.exports.useState(!1),[I,y]=c.exports.useState(!1),[T,M]=c.exports.useState(""),[w,B]=c.exports.useState(!1),{onSendMessage:Q,onTyping:D}=ln({setContent:M,userId:a,group:r,setConversationId:n}),H=((m=(f=i==null?void 0:i.usersTyping)==null?void 0:f.filter(q=>q!==(A==null?void 0:A.id)))==null?void 0:m.length)>0,F=($=(u=i==null?void 0:i.usersTyping)==null?void 0:u.filter(q=>q!==A.id))==null?void 0:$.map(q=>{var z;return(z=i==null?void 0:i.participants.find(X=>X.userId===q))==null?void 0:z.fullName}),{doUploadImage:V}=Mc({setMediaUrls:l}),{getRootProps:O,getInputProps:U}=bs({accept:["image/*","application/vnd.ms-excel","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet","application/vnd.openxmlformats-officedocument.presentationml.presentation","application/pdf","application/xhtml+xml","application/xml","application/msword","application/vnd.openxmlformats-officedocument.wordprocessingml.document","application/zip"],multiple:!1,onDrop:(q,z)=>{var he;const X=(z==null?void 0:z.length)>0,be=z.length>0&&((he=z[0].errors)==null?void 0:he.find(fe=>fe.code==="file-too-large"));X?p({title:"Th\u1EA5t b\u1EA1i!",description:be?wd:Td,status:"error",duration:3e3,isClosable:!0}):V(q).then()},minSize:0,maxSize:Oc}),W=c.exports.useCallback(async q=>{const z=q.target.value.trim();if(q.key==="Enter"&&!q.shiftKey)(z.length!==0||(o==null?void 0:o.length)>0)&&(w&&B(!1),await Q({nativeEvent:{text:z},conversationId:e}),B(!1),M(""));else return!1},[e,T,w,o,s]),ne=()=>I&&t(Kt,{_focus:{outline:"none"},onFocus:()=>{g(!0)},onBlur:()=>{g(!1)},as:Jt,style:{resize:"none"},ref:s,minRows:1,maxRows:5,rows:1,value:T,onChange:async q=>{q.target.value!==`
`&&await M(q.target.value)},border:"none",placeholder:"Nh\u1EADp n\u1ED9i dung ...",onKeyPress:W,color:C,bg:"transparent",mr:4});return ys(()=>{I&&async function(){S&&await D("typingOn",e),S||await D("typingOff",e)}()},[S,e,D,I,s]),c.exports.useEffect(()=>{let q;return d&&(q=setTimeout(()=>{g(!1)},15e3)),()=>{clearTimeout(q)}},[d]),c.exports.useEffect(()=>{y(!0)},[]),c.exports.useEffect(()=>{const q=z=>{E.current&&!E.current.contains(z.target)&&B(!1)};return document.addEventListener("click",q,!0),()=>{document.removeEventListener("click",q,!0)}},[B]),h(P,{bg:k("white",""),position:"sticky",bottom:0,w:"100%",display:"flex",flexDirection:"column",borderTopWidth:1,borderColor:"var(--chakra-colors-whiteAlpha-300)",p:2,children:[H&&h(R,{w:"50%",px:2,rounded:4,mb:2,children:[t(L,{fontSize:13,color:k("gray.700","white"),children:F==null?void 0:F.map(q=>q||"Someone is Typing").join(",")}),t(P,{display:"flex",alignItems:"center",justifyContent:"center",w:10,h:5,children:t(so,{})})]}),h(R,{w:"full",children:[h(P,v(b({as:"div"},O({className:"dropzone"})),{children:[t("input",v(b({},U()),{style:{display:"none"},type:"file"})),t(Cs,{color:k("#DA0037","white"),className:"w-5 h-5"})]})),h(P,{w:"100%",display:"flex",flexDirection:"row",borderWidth:1,rounded:"lg",borderColor:k("gray.300","var(--chakra-colors-whiteAlpha-300)"),p:2,children:[ne(),h(R,{display:"flex",alignItems:"center",children:[t(P,{as:"button",onClick:()=>B(!w),children:t(ka,{color:k("#DA0037","white"),className:"w-5 h-5 "})}),t(P,{cursor:"pointer",pointerEvents:!T&&(o==null?void 0:o.length)===0?"none":"auto",onClick:async()=>{T.trim().length!==0||(o==null?void 0:o.length)>0?(w&&B(!1),await Q({nativeEvent:{text:T},conversationId:e})):M("")},children:t(Oa,{fill:k((T==null?void 0:T.length)>0||(o==null?void 0:o.length)>0?"#DA0037":"#686868","white"),className:"w-5 h-5"})})]})]}),w&&t(P,{ref:E,color:"slate.900",children:t(qc,{disableSkinTonePicker:!0,pickerStyle:{position:"absolute",bottom:"70px",right:"20px",overflow:"hidden",backgroundColor:"white",zIndex:"1000"},onEmojiClick:(q,z)=>{M(`${T} ${z.emoji}`)}})})]})]})}},Nc=e=>{const n=Te(),[a,{loading:r}]=Ae(nl);return{onMarkDone:c.exports.useCallback(async()=>{try{await a({variables:{conversationId:e}}),await n({title:"Th\xE0nh c\xF4ng.",description:"\u0110\xE3 chuy\u1EC3n user sang tr\u1EA1ng th\xE1i done",status:"success",duration:2e3,isClosable:!0})}catch(o){n({title:"L\u1ED7i !!!",description:o.message,status:"success",duration:2e3,isClosable:!0})}},[e,a,n]),loading:r}},Bc=a=>{var r=a,{onChange:e}=r,n=$e(r,["onChange"]);const[s,o]=c.exports.useState(""),l=ft(s,1e3);return c.exports.useEffect(()=>{e(l)},[l]),t(yt,v(b({},n),{value:s,onChange:i=>o(i.target.value)}))},$o=e=>{const o=e,{Icon:n,onClick:a,disabled:r}=o,s=$e(o,["Icon","onClick","disabled"]);return t(P,v(b({_hover:{backgroundColor:"#0085E4"},as:"button",display:"flex",alignItems:"center",justifyContent:"center",width:10,rounded:"4px",h:"100%",onClick:a,disabled:r},s),{children:t(n,{})}))},Lc=({userInfo:e,groupInfo:n,loading:a,participants:r})=>{var M;const[s]=ae(),[o,l]=c.exports.useState(!1),i=_(w=>w.searchStringMessage),d=_(w=>w.setSearchStringMessage),f=_(w=>w.visibleUserProfile),m=_(w=>w.conversationId),u=_(w=>w.setSearchResult),$=_(w=>w.setNearSearchResult),p=_(w=>w.setVisibleUserProfile),A=_(w=>w.setBeforeId),E=_(w=>w.setAfterId),C=_(w=>w.setMessageId),S=ut({base:"",xl:"visible"}),g=ut({base:"",md:"visible"}),{onMarkDone:I,loading:y}=Nc(m);Eo({conversationId:m,search:i});const T=c.exports.useMemo(()=>a?"\u0110ang t\u1EA3i ...":(n==null?void 0:n.name)?n==null?void 0:n.name:(e==null?void 0:e.full_name)||(e==null?void 0:e.fullName)||"\u1EA8n danh",[a,n==null?void 0:n.name,e==null?void 0:e.full_name,e==null?void 0:e.fullName]);return h(P,{w:"100%",minH:"35px",maxH:"50px",display:"flex",flexDirection:"row",alignItems:"center",borderBottomWidth:1,pl:2,py:3,zIndex:1,children:[!o&&h(R,{id:"js-toggle-profile",flex:1,alignItems:"center",cursor:"pointer",onClick:()=>!S&&p(!f),children:[e&&qe.exports.isEmpty(n)?t(Ie,{boxSize:"2em",src:e==null?void 0:e.avatar,name:T}):t(io,{groupInfo:n==null?void 0:n.participants}),t(P,{flex:1,maxW:"300px",children:t(L,{noOfLines:1,fontSize:14,lineHeight:"24px",fontWeight:"medium",color:k("gray.800","white"),textTransform:"capitalize",children:T})})]}),h(R,{w:o?"full":"",h:"100%",pr:o?2:4,spacing:0,minH:10,children:[[6].includes(parseInt(s==null?void 0:s.roleId,10))&&(r==null?void 0:r.find(w=>w.userId===s.id.toString()))&&!o&&((e==null?void 0:e.isGuest)||((M=n==null?void 0:n.participants)==null?void 0:M.find(w=>w.isGuest)))&&t($o,{onClick:I,disabled:y,Icon:Es}),!o&&g&&t($o,{onClick:()=>{l(!0)},Icon:$s}),o&&h(oa,{children:[t(Bc,{type:"text",placeholder:"Nh\u1EADp g\xEC \u0111\xF3 ...",onChange:d,pr:"4.5rem"}),t(ra,{width:"4.5rem",children:t(Y,{h:"1.75rem",size:"sm",onClick:()=>{l(!1),d(""),u([]),$([]),C(""),A(""),E("")},children:o?"Close":"Open"})})]})]})]})},So=(e,n)=>{var l;const{data:a,loading:r,error:s}=ge(hi,{variables:se({id:e,userId:e?"":n?n==null?void 0:n.toString():""},i=>i),skip:!e&&!n});return{conversationDetail:(l=a==null?void 0:a.conversationDetail)!=null?l:{},loading:r,error:s}},vo=(e="",n)=>{var o;const{data:a,loading:r}=ge(Ga,{variables:{userId:Number(e)},skip:n||!e||isNaN(Number(e))});return{userData:(o=a==null?void 0:a.getUserById)!=null?o:{},loading:r}},Io=({conversationDetail:e})=>{var a,r,s,o;const[n]=ae();return!((a=e==null?void 0:e.participants)==null?void 0:a.find(l=>l.userId===(n==null?void 0:n.id)))&&((r=e==null?void 0:e.participants)==null?void 0:r.length)>=2||((s=e==null?void 0:e.participants)==null?void 0:s.find(l=>l.userId===(n==null?void 0:n.id)))&&((o=e==null?void 0:e.participants)==null?void 0:o.length)>=3},Rc="_sm_183st_1",Dc="_base_183st_6",zc="_md_183st_10";var dn={sm:Rc,base:Dc,md:zc};const ko={sm:dn.sm,base:dn.base,md:dn.md},wo={primary:"bg-red-700 border-red-700 dark:bg-red-700 hover:border-red-900 hover:bg-red-900 dark:hover:bg-red-900 dark:border-red-500  hover:text-white ring-red-400 text-white ",info:"bg-blue-500 border-blue-500 hover:border-blue-700 hover:bg-blue-700 hover:text-white ring-blue-400 text-white",white:"bg-white border-transparent hover:border-red-700 hover:bg-red-700 hover:text-white ring-red-400 text-black",warning:"bg-yellow-500 hover:bg-yellow-700 border-yellow-500 hover:border-yellow-700 ring-yellow-400 text-black",success:"bg-green-500 hover:bg-green-700 ring-green-400 text-white border-green-500 hover:border-green-700","primary-outline":"border border-red-700 hover:bg-red-700 hover:text-white ring-red-400 text-red-700 dark:text-red-300 dark:border-red-300 dark:hover:bg-red-300 dark:hover:text-slate-300","info-outline":"border border-blue-700 hover:bg-blue-700 hover:text-white ring-blue-400 text-blue-700","success-outline":"border border-green-700 hover:bg-green-700 hover:text-white ring-green-400 text-green-700","warning-outline":"border border-yellow-700 text-yellow-700 hover:bg-yellow-700 hover:text-black ring-yellow-400","black-outline":"border border-black hover:bg-gray-50 text-black hover:text-black ring-gray-700 dark:border-slate-500 dark:text-slate-300 dark:hover:text-slate-900",gray:"bg-gray-200 hover:bg-gray-600 ring-gray-400 text-gray-900 hover:text-white dark:bg-slate-600 dark:text-slate-300 dark:hover:bg-slate-700 dark:border-slate-700",ghost:"hover:bg-gray-100 ring-gray-400 border-transparent hover:border-transparent dark:bg-slate-600 dark:text-slate-300 dark:hover:bg-slate-700"},To=(e,n)=>e.match(n),jc=c.exports.forwardRef((d,i)=>{var f=d,{className:e="",buttonType:n=wo.primary,buttonSize:a=ko.base,isLoading:r=!1,isDisabled:s=!1,children:o}=f,l=$e(f,["className","buttonType","buttonSize","isLoading","isDisabled","children"]);const m=c.exports.useMemo(()=>wo[n],[n]),u=c.exports.useMemo(()=>ko[a],[a]),$=c.exports.useMemo(()=>r||s,[r,s]);return h("button",v(b({disabled:s||r,ref:i,className:`whitespace-nowrap border inline-flex items-center justify-center transition-colors transition-background duration-300 ${To(e,"p-")?"":"py-2 px-4"} rounded focus:outline-none ring-opacity-75  focus:ring ${To(e,"text-")?"":"text-base"} font-medium ${$&&"opacity-60 cursor-not-allowed"} ${m} ${e} ${u}`},l),{children:[r&&h("svg",{className:"animate-spin -ml-1 mr-3 h-5 w-5 text-current",xmlns:"http://www.w3.org/2000/svg",fill:"none",viewBox:"0 0 24 24",children:[t("circle",{className:"opacity-25",cx:"12",cy:"12",r:"10",stroke:"currentColor",strokeWidth:"4"}),t("path",{className:"opacity-100",fill:"currentColor",d:"M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"})]}),o]}))}),Uc=c.exports.memo(({showUploadFileDialog:e,file:n,setFile:a,setShowUploadFileDialog:r,onSendMessage:s})=>{const o=Ne(),l=_(d=>d.conversationId),i=c.exports.useCallback(async()=>{await s({nativeEvent:{text:""},conversationId:l,file:n}),await o(["getAttachmentsInConversation"]),await a.clear()},[l,n]);return t(Ss,{appear:!0,show:e,as:"div",enter:"transition-opacity duration-75",enterFrom:"opacity-0",enterTo:"opacity-100",leave:"transition-opacity duration-150",leaveFrom:"opacity-100",leaveTo:"opacity-0",children:t(P,{p:2,zIndex:1e3,position:"absolute",bg:k("white","slate.600"),rounded:"lg",display:"flex",flexDirection:"column",justifyContent:"space-between",w:"50%",h:"40%",maxH:"300px",shadow:"md",className:"translate-y-[90%] translate-x-[50%] inset-0 ",children:n==null?void 0:n.map((d,f)=>h(ze,{flexDirection:"column",display:"flex",justifyContent:"space-between",w:"full",h:"100%",overflow:"hidden",rounded:"lg",children:[(d==null?void 0:d.type)!=="file"?t(P,{maxH:"75%",children:t(Oe,{w:"full",h:"full",src:d==null?void 0:d.url,alt:"file",objectFit:"contain"})}):t(ue,{display:"flex",maxH:"75%",h:"full",alignItems:"center",justifyContent:"center",children:h(te,{children:[t(P,{w:14,h:14,rounded:"full",display:"flex",alignItems:"center",children:t(Sa,{className:"w-[40px] h-[40px]"})}),t(L,{textAlign:"center",flex:1,color:k("gray.800","white"),fontSize:14,children:cn(d==null?void 0:d.name)})]})}),t(P,{onClick:()=>{a.removeAt(f),r(!1)},top:-2,right:-1,rounded:"full",cursor:"pointer",position:"absolute",zIndex:20,bg:k("white","#10172a"),children:t(vs,{onClick:()=>{a.removeAt(f),r(!1)},width:32,className:"text-gray-400 hover:text-red-400"})}),h(ue,{display:"flex",flexDirection:"column",height:"25%",justifyContent:"flex-end",children:[t(P,{h:"18%",w:"100%",children:t(Is,{colorScheme:(d==null?void 0:d.progress)<1?"":"green",hasStripe:(d==null?void 0:d.progress)<1,mt:2,value:(d==null?void 0:d.progress)*100})}),t(ks,{}),t(jc,{disabled:(d==null?void 0:d.progress)<1,onClick:i,buttonType:"info",className:"w-full text-center py-2 rounded-lg text-white mt-2 max-h-[40px]",children:"G\u1EEDi"})]})]},f))})})}),Gc=(e=[],n)=>{if((e==null?void 0:e.length)<2)return!0;if((e==null?void 0:e.length)>=2)return!!(e==null?void 0:e.find(a=>a.userId===n))},Hc=()=>{var I,y,T,M;const e=_(w=>w.channel),n=_(w=>w.userId),a=_(w=>w.conversationId),r=_(w=>w.setConversationId),s=_(w=>w.searchStringMessage),o=c.exports.useRef(null),[l]=ae(),[i,d]=ws([]),[f,m]=c.exports.useState(!1),{userData:u,loading:$}=vo(n,!n),{conversationDetail:p,loading:A}=So(a,n),E=((I=p==null?void 0:p.participants)==null?void 0:I.filter(w=>(w==null?void 0:w.userId)!==(l==null?void 0:l.id.toString()))[0])||u,C=Io({conversationDetail:p}),{onSendMessage:S,loading:g}=ln({setContent:()=>{},userId:n,group:e,setConversationId:r});return c.exports.useEffect(()=>{(i==null?void 0:i.length)>0?m(!0):m(!1)},[i]),h(P,{bg:k("white","#10172a"),display:"flex",flexDirection:"column",flex:1,w:"full",position:"relative",h:"full",children:[(a||n)&&t(Lc,{participants:p==null?void 0:p.participants,userInfo:E,groupInfo:C&&a?{name:(p==null?void 0:p.name)?p==null?void 0:p.name:C&&a?(T=(y=p==null?void 0:p.participants)==null?void 0:y.map(w=>w.fullName))==null?void 0:T.join(","):"",participants:p==null?void 0:p.participants}:null,loading:$||A}),t(Pc,{isGroup:p==null?void 0:p.name,ownerId:l==null?void 0:l.id,conversationId:a,userId:n,onSendMessage:S,sendMessageLoading:g,participants:p==null?void 0:p.participants,setConversationId:r,channel:e}),t(Uc,{onSendMessage:S,file:i,setFile:d,showUploadFileDialog:f,setShowUploadFileDialog:m}),(Gc(p==null?void 0:p.participants,l==null?void 0:l.id)&&(a||n)||C&&Bo.includes(l==null?void 0:l.roleId))&&!s&&!A&&t(Fc,{conversationDetail:p,file:i,setFile:d,textAreaRef:o,conversationId:a,setConversationId:r,userId:n,group:e,isMyTyping:(M=p==null?void 0:p.usersTyping)==null?void 0:M.includes(l==null?void 0:l.id)}),(i==null?void 0:i.length)>0&&t(P,{opacity:.4,zIndex:10,position:"absolute",bg:k("black","slate.300"),className:"inset-0"})]})},Vc=({participantAvatarUrl:e,fullName:n="",onClose:a,loading:r})=>h(P,{flex:1,display:"flex",alignItems:"center",cursor:"pointer",onClick:()=>a?a():()=>{},children:[t(Ie,{boxSize:"2em",src:e,name:n}),t(L,{ml:2,flex:1,noOfLines:1,fontSize:14,lineHeight:"24px",fontWeight:"medium",color:k("gray.800","slate.300"),children:r?"\u0110ang t\u1EA3i ...":n||"\u1EA8n danh"})]}),Wc=/^(?=.*\d)(?=.*[a-zA-Z])[\da-zA-Z_.\-@]{8,}$/,Qc=/^\d+$/,Po=/(84|03|05|07|08|09|01[2|6|8|9])+([0-9]{8})\b/,Qu=/^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w.-]+)+[\w\-._~:/?#[\]@!$&'()*+,;=.]+$/gm,Yc=e=>{if(typeof e!="string")return"";const n=e.split(".");return n.length>0?n[n.length-1]:""};Ke(Xt,"password",function(e){return this.matches(Wc,{message:e,excludeEmptyString:!0})});Ke(Xt,"onlyNumber",function(e){return this.matches(Qc,{message:e,excludeEmptyString:!0})});Ke(Xt,"phone",function(e){return this.matches(Po,{message:e,excludeEmptyString:!0})});Ke(en,"fileSize",function(e,n){return this.test("fileSize",n,a=>Array.isArray(a)&&a.length>0&&a[0]?!a.map(s=>s.size>e):!0)});Ke(en,"fileType",function(e=[],n){return this.test("fileType",n,a=>e.length===0?!0:Array.isArray(a)&&a.length>0&&a[0]?!a.map(s=>e.includes(s.type)):!0)});Ke(en,"file",function({size:e,type:n},a){return this.test({name:"file",test:function(r){var l,i,d,f,m,u;const s=(l=n==null?void 0:n.value)!=null?l:[],o=(i=e==null?void 0:e.value)!=null?i:5*1024*1e3;if(Array.isArray(r)&&r.length>0&&r[0]){const $=r.find(A=>!s.includes(A.type)),p=r.find(A=>A.size>o);return $?this.createError({message:`${(d=n==null?void 0:n.message)!=null?d:"Kh\xF4ng h\u1ED7 tr\u1EE3 \u0111\u1ECBnh d\u1EA1ng"} ${Yc((f=$==null?void 0:$.name)!=null?f:"")}`,path:this.path}):p?this.createError({message:(u=(m=e==null?void 0:e.message)!=null?m:`Dung l\u01B0\u1EE3ng v\u01B0\u1EE3t qu\xE1 ${o/1024e3}`+(p==null?void 0:p.name))!=null?u:"",path:this.path}):!0}else return!0}})});const Mt=x`
  fragment Ticket on TicketType {
    id
    refId
    title
    description
    createDate
    status
    createBy {
      id
      user_name
      full_name
      status
      avatar
      supporter
      role_id
      phone
      address
      supporter
    }
    receiver {
      id
      user_name
      full_name
      avatar
      role_id
      phone
      address
      supporter
    }
    reactions {
      id
      ticketId
      reaction
      count
    }
  }
`,Zc=x`
  mutation createTicket($input: TicketInputType) @api(name: "appZim") {
    createTicket(input: $input) {
      ...Ticket
    }
  }
  ${Mt}
`,Yu=x`
  mutation updateTicket($input: TicketInputType) @api(name: "appZim") {
    updateTicket(input: $input) {
      ...Ticket
    }
  }
  ${Mt}
`,Zu=x`
  mutation updateTicketStatus($input: TicketUpdateStatusType)
  @api(name: "appZim") {
    updateTicketStatus(input: $input) {
      ...Ticket
    }
  }
  ${Mt}
`,Ku=x`
  mutation createTicketReaction($input: TicketReactionInputType)
  @api(name: "appZim") {
    createTicketReaction(input: $input) {
      id
      ticketId
      reaction
      count
    }
  }
  ${Mt}
`,Kc=Ce.object().shape({title:Ce.string().required("Vui l\xF2ng nh\u1EADp ti\xEAu \u0111\u1EC1"),description:Ce.string().min(10,"T\u1ED1i thi\u1EC3u 10 k\xFD t\u1EF1").required("Vui l\xF2ng nh\u1EADp m\xF4 t\u1EA3"),receiver:Ce.mixed()}),Jc=({enableReceiver:e,users:n,onInputChange:a})=>{var l;const{register:r,formState:{errors:s},resetField:o}=qa();return h(ze,{direction:"column",w:"full",justifyContent:"stretch",alignItems:"stretch",className:"reply-editor",gap:4,children:[t(ve,v(b({label:"Ti\xEAu \u0111\u1EC1",placeholder:"Nh\u1EADp..."},r("title")),{error:s==null?void 0:s.title})),e&&t(dt,{name:"receiver",render:({field:i})=>t(_t,b({label:"Ng\u01B0\u1EDDi nh\u1EADn",options:n||[],getOptionLabel:d=>`${d.full_name} - ${d.phone}`,getOptionValue:d=>d.id+"",formatOptionLabel:d=>h(R,{children:[t(sa,{src:d.avatar,objectFit:"cover",boxSize:6,flexShrink:0}),t(L,{children:d.full_name})]}),error:s==null?void 0:s.receiver,onInputChange:a},i))}),t(P,{children:h(bt,{isInvalid:s==null?void 0:s.description,children:[t(dt,{name:"description",render:({field:i})=>t(yo,b({showWordCount:!1,placeholder:"N\u1ED9i dung ticket..."},i))}),t(Ct,{children:(l=s==null?void 0:s.description)==null?void 0:l.message})]})})]})},Xc=({onClose:e,isOpen:n,submitCallback:a=null})=>{const r=c.exports.useRef(),s=$t({resolver:Fa(Kc),mode:"onBlur",reValidateMode:"onChange"}),o=Te();Co();const[l,i]=De(),d=Ne(),[f,m]=c.exports.useState(""),[u,$]=c.exports.useState(f);Ts(()=>{m(u)},500,[u]);const[p]=ae(),A=p.roleId===6,[E,{loading:C}]=Ae(Zc,{onCompleted:y=>{const{createTicket:T}=y;T&&o({title:"Th\xE0nh c\xF4ng!",description:"Ticket \u0111\xE3 \u0111\u01B0\u1EE3c g\u1EEDi.",status:"success",duration:3e3,isClosable:!0}),e(),a&&typeof a=="function"&&a(),d(["getTicketsPagination"])},onError:y=>{var T;o({title:"G\u1EEDi kh\xF4ng th\xE0nh c\xF4ng!",description:`${(T=y==null?void 0:y.message)!=null?T:"L\u1ED7i"}`,status:"error",duration:3e3,isClosable:!0})},notifyOnNetworkStatusChange:!0}),{data:S}=ge(pi(["avatar","full_name","phone"]),{variables:{input:{limit:100,q:f}},skip:!A}),g=S==null?void 0:S.getListStudentByEc.docs,I=c.exports.useCallback(async y=>{var M;if(A&&!(y==null?void 0:y.receiver)){s.setError("receiver",{type:"manual",message:"Vui l\xF2ng ch\u1ECDn ng\u01B0\u1EDDi nh\u1EADn"});return}const T={refId:Gi,title:y==null?void 0:y.title,description:y==null?void 0:y.description,receiverId:A?parseInt((M=y==null?void 0:y.receiver)==null?void 0:M.id):null};A||delete T.receiverId,await E({variables:{input:T}})},[A]);return c.exports.useEffect(()=>{if(n){const y=l.get("receiverId");if(!y||!(g==null?void 0:g.length))return;const T=g.find(M=>M.id===Number(y));s.reset({title:"",description:"",receiver:T||null})}else l.delete("receiverId"),i(l),s.reset({title:"",description:"",receiver:null})},[n,l,g]),t(jt,v(b({},s),{children:h(Ve,{finalFocusRef:r,isOpen:n,onClose:e,size:"3xl",children:[t(Le,{}),h(We,{children:[t(Re,{children:"T\u1EA1o ticket"}),t(Qe,{}),h("form",{onSubmit:s.handleSubmit(I),children:[t(Ye,{children:t(Jc,{enableReceiver:A,users:g,onInputChange:$})}),h(rt,{children:[t(Y,{leftIcon:t(st,{as:Ut}),colorScheme:"green",mr:4,type:"submit",isLoading:C,children:"G\u1EEDi ticket"}),t(Y,{variant:"ghost",onClick:e,children:"H\u1EE7y"})]})]})]})]})}))};function ed({color:e,onClick:n,isActive:a}){return t(P,{borderWidth:a?3:1,borderColor:a?"white":"",as:"button",onClick:n,rounded:"sm",w:4,h:4,bg:`${e}`})}function td({firstFieldRef:e,onClose:n}){var S,g;const a=Te(),[r,s]=c.exports.useState("1"),[o,l]=c.exports.useState(null),i=_(I=>I.conversationId),[d,f]=c.exports.useState("#00DAA3"),[m,u]=c.exports.useState(""),{data:$}=ge(Ai,{skip:r==="2"}),[p,{loading:A}]=Ae(tl),E=c.exports.useCallback(()=>{f("#00DAA3"),u(""),l(null)},[]),C=c.exports.useCallback(async()=>{var T;const I={conversationId:i,tagId:o==null?void 0:o.id},y={conversationId:i,name:m,color:d};try{await p({variables:(o==null?void 0:o.id)?I:y}),n(),E(),a({title:"Th\xE0nh c\xF4ng!",description:"G\u1EAFn tag th\xE0nh c\xF4ng",status:"success",duration:3e3,isClosable:!0})}catch(M){a({title:"G\u1EAFn tag kh\xF4ng th\xE0nh c\xF4ng!",description:`${(T=M==null?void 0:M.message)!=null?T:"L\u1ED7i"}`,status:"error",duration:3e3,isClosable:!0})}},[m,d,i,o,E]);return h(ue,{spacing:4,children:[t(Na,{defaultValue:"1",children:h(R,{spacing:5,align:"center",children:[t(Ba,{isChecked:r==="1",colorScheme:"red",value:"1",onChange:()=>{s("1"),u(""),f("#00DAA3")},children:"Tag c\xF3 s\u1EB5n"}),t(Ba,{isChecked:r==="2",colorScheme:"green",value:"2",onChange:()=>{s("2"),l(null)},children:"Th\xEAm tag m\u1EDBi"})]})}),r==="1"&&t(_t,{value:o,label:"Tags",onChange:l,options:(g=(S=$==null?void 0:$.getListTag)==null?void 0:S.docs)!=null?g:[],getOptionLabel:I=>I.name,getOptionValue:I=>I.id+"",formatOptionLabel:I=>h(R,{children:[t(P,{bg:I.color,w:"5px",h:"5px",rounded:"full",mr:1}),t(L,{children:I.name})]})}),r==="2"&&h(ue,{spacing:4,children:[t(Na,{onChange:f,value:d,children:t(R,{spacing:3,children:Pd.map((I,y)=>{const T=I.color===d;return t(ed,{color:I.color,onClick:()=>f(I.color),isActive:T},I.color)})})}),t(ve,{label:"Tag name",id:"tag-name",ref:e,placeholder:"Nh\u1EADp t\xEAn tag",value:m,onChange:I=>u(I.target.value)})]}),h(Ps,{d:"flex",justifyContent:"flex-end",children:[t(Y,{onClick:()=>{n(),E()},variant:"outline",children:"Cancel"}),t(Y,{onClick:C,disabled:!m&&!o||A,colorScheme:"teal",children:"Save"})]})]})}const nd=({item:e})=>h(R,{children:[t(P,{w:6,h:6,p:1,rounded:"full",bg:k("gray.100","#1e293b"),overflow:"hidden",display:"flex",alignItems:"center",justifyContent:"center",children:e.icon}),typeof e.value=="function"?e==null?void 0:e.value():t(L,{flex:1,fontSize:14,color:k("gray.800","slate.300"),noOfLines:2,children:e.value||"Kh\xF4ng c\xF3 th\xF4ng tin"})]}),ad=c.exports.memo(({participantInfo:e,participantAvatarUrl:n,fullName:a,loading:r,isStudent:s,tags:o,participants:l})=>{const{isOpen:i,onClose:d,onOpen:f}=Pe(),m=He.useRef(null),[u]=ae(),$=pn.map(g=>g.id),p=_(g=>g.setVisibleUserProfile),A=_(g=>g.conversationId),E=kd(e,o),C=ut({sm:"visible",xl:""}),S=c.exports.useCallback(()=>{p(!1)},[]);return!s&&h(te,{alignItems:"stretch",spacing:2.5,px:4,py:2,borderBottomWidth:1,children:[h(ue,{spacing:2.5,children:[h(R,{justifyContent:"space-between",children:[t(Vc,{onClose:S,participantAvatarUrl:n,fullName:a,loading:r}),C&&t(P,{onClick:S,display:"flex",cursor:"pointer",w:6,h:6,alignItems:"center",justifyContent:"center",children:t(_s,{className:"w-full h-full"})})]}),E==null?void 0:E.filter(g=>g.visible).map((g,I)=>t(nd,{item:g},`${g.value}-${I}`))]}),!Ot.includes(u.roleId)&&A&&(l==null?void 0:l.find(g=>g.userId===(u==null?void 0:u.id.toString())))&&h(R,{spacing:10,children:[t(it,{children:({onClose:g})=>h(Se,{children:[t(lt,{children:t(Y,{bg:"transparent",_hover:{backgroundColor:"transparent"},_focus:{backgroundColor:"transparent"},_active:{backgroundColor:"transparent"},children:$.includes(u==null?void 0:u.roleId)&&t(_o,{iconContainerClassName:"bg-blue-500",label:"G\u1EAFn tag"})})}),t(vt,{container:document.getElementById("profile-portal"),children:h(ct,{children:[t(La,{}),t(Ms,{children:"G\u1EAFn tag"}),h(Yt,{children:[t(La,{}),t(td,{firstFieldRef:m,onClose:g})]})]})})]})}),(e==null?void 0:e.role_name)==="H\u1ECDc vi\xEAn"&&$.includes(u==null?void 0:u.roleId)&&A&&t(_o,{onClick:f,label:"T\u1EA1o ticket",iconContainerClassName:"bg-red-500"})]}),t(Xc,{onClose:d,isOpen:i})]})}),_o=({label:e="T\u1EA1o ticket",onClick:n,iconContainerClassName:a})=>h(R,{onClick:n,display:"flex",alignItems:"center",cursor:"pointer",children:[t(P,{className:a,w:5,h:5,rounded:"full",display:"flex",alignItems:"center",justifyContent:"center",children:t(qs,{color:"white"})}),t(L,{fontWeight:"400",color:k("gray.800","slate.300"),children:e})]}),od=({createdAt:e,content:n,owner:a})=>{var r;return h(R,{display:"flex",alignItems:"start",children:[t(Ie,{size:"sm",src:a==null?void 0:a.avatar,name:a==null?void 0:a.fullName}),h(te,{flex:1,alignItems:"stretch",spacing:1,children:[h(R,{spacing:3,justifyContent:{base:"start","2xl":"space-between"},children:[t(L,{noOfLines:1,fontSize:14,lineHeight:"24px",fontWeight:"medium",color:k("gray.800","slate.400"),maxW:{base:"90px","2xl":"120px"},children:(r=a==null?void 0:a.fullName)!=null?r:"Kh\xF4ng c\xF3"}),t(L,{fontSize:12,lineHeight:"24px",className:"text-gray-400",children:we(e).format("DD-MM-YYYY, HH:mm")})]}),t(L,{fontSize:12,lineHeight:"16px",color:k("gray.800","slate.200"),children:n})]})]})},rd=c.exports.memo(()=>{const e=c.exports.useRef(null),n=_(u=>u.conversationId),a=Ne(),[r,s]=c.exports.useState(!1),[o,l]=c.exports.useState(""),[i]=Ae(Xi),d=c.exports.useCallback(async()=>{try{await i({variables:{conversationId:n,content:o}}),await a(["notes"])}catch(u){console.log(u)}},[n,o]),f=c.exports.useCallback(async()=>{o.trim().length!==0&&(await d(),l(""))},[o]),m=c.exports.useCallback(async u=>{const $=u.target.value.trim();if(u.key==="Enter"&&!u.shiftKey)$.length!==0&&(d().then(),l(""));else return!1},[d]);return c.exports.useEffect(()=>{s(!0)},[]),h(R,{w:"100%",borderWidth:1,rounded:"lg",borderColor:k("gray.300","whiteAlpha.300"),p:1,children:[r&&t(Kt,{_focus:{outline:"none"},as:Jt,style:{resize:"none"},ref:e,minRows:1,maxRows:5,rows:1,value:o,onChange:u=>{u.target.value!==`
`&&l(u.target.value)},border:"none",placeholder:"Nh\u1EADp n\u1ED9i dung ghi ch\xFA ...",onKeyPress:m,color:k("gray.900","slate.300"),bg:"transparent"}),t(P,{cursor:"pointer",w:5,h:5,pointerEvents:o?"auto":"none",onClick:f,children:t(Oa,{fill:k((o==null?void 0:o.length)>0?"#DA0037":"#686868","white")})})]})}),sd=({conversationId:e,userId:n})=>{var m,u,$;const{data:a,loading:r,fetchMore:s}=ge(mi,{variables:se({conversationId:e||"",userId:e?"":n?n==null?void 0:n.toString():"",limit:5},p=>p),skip:!e}),o=(m=a==null?void 0:a.notes)==null?void 0:m.docs,l=(u=a==null?void 0:a.notes)==null?void 0:u.page,i=($=a==null?void 0:a.notes)==null?void 0:$.hasNextPage,{onLoadMore:d,isLoadingMore:f}=Ge({variables:{conversationId:e,limit:5,page:l+1},fetchMore:s,hasNextPage:i});return c.exports.useMemo(()=>({notes:o,loading:r,onLoadMore:d,isLoadingMore:f,hasNextPage:i}),[o,r,d,f,i])};const id=c.exports.memo(({isSuperAdmin:e,allowForSupperAdmin:n})=>{const a=_(u=>u.conversationId),r=_(u=>u.userId),s=c.exports.useRef(null),{notes:o,onLoadMore:l,hasNextPage:i,isLoadingMore:d}=sd({conversationId:a,userId:r}),f=c.exports.useCallback(async()=>{await l()},[l]),m=c.exports.useMemo(()=>o==null?void 0:o.map(u=>t(od,b({},u),u.id)),[o]);return c.exports.useEffect(()=>{let u;return(s==null?void 0:s.current)&&(u=new It(s==null?void 0:s.current,{wheelSpeed:1,minScrollbarLength:10})),()=>{u&&u.destroy()}},[]),h(te,{alignItems:"stretch",spacing:2.5,px:4,py:2,children:[h(L,{fontSize:14,color:k("gray.800","slate.300"),children:["Notes ",o==null?void 0:o.length]}),t(te,{alignItems:"stretch",spacing:2.5,maxH:"200px",ref:s,className:"scroll-container",overflow:"hidden",position:"relative",minH:0,children:m}),i&&t(Y,{h:"32px",isLoading:d,loadingText:"\u0110ang t\u1EA3i th\xEAm",onClick:f,w:"100%",rounded:"none",borderWidth:"1px 0px 0px 0px",bg:k("white","slate.600"),fontSize:14,fontWeight:400,justifyContent:"center",children:"Xem th\xEAm"}),(!e||n)&&t(rd,{})]})}),ld=({conversationId:e,userId:n})=>{var $,p,A;const[a,r]=c.exports.useState(!1),s=v(b({},se({conversationId:e||"",userId:e?"":n?n==null?void 0:n.toString():""},E=>E)),{offset:0}),{data:o,loading:l,fetchMore:i}=ge(fi,{variables:s}),d=(p=($=o==null?void 0:o.getAttachmentsInConversation)==null?void 0:$.docs)==null?void 0:p.map(E=>E.attachments),f=[].concat.apply([],d),m=(A=o==null?void 0:o.getAttachmentsInConversation)==null?void 0:A.hasNextPage,u=c.exports.useCallback(async()=>{var E;!m||a||(await r(!0),await i({variables:v(b({},s),{offset:((E=o==null?void 0:o.getAttachmentsInConversation)==null?void 0:E.docs.length)+1}),updateQuery:(C,{fetchMoreResult:S})=>S?on(C,S):C}),await r(!1))},[o,i,a,e]);return{attachments:f,loading:l,onLoadMore:u,isLoadingMore:a,hasNextPage:m}};function cd(e){let n=/^.+\.([^.]+)$/.exec(e);return n==null?"":n[1]}const dd=c.exports.memo(({item:e,setAttachmentIndex:n,setShowImageViewer:a,index:r})=>{var i,d,f,m;const s=c.exports.useCallback(()=>{n(r),a(!0)},[r]),o=cd((i=e==null?void 0:e.attachment)==null?void 0:i.fullUrl),l=c.exports.useMemo(()=>{switch(o){case"xls":return Ns;case"doc":return Fs;default:return Os}},[o]);return h(P,{minH:"80px",children:[(e==null?void 0:e.type)==="image"&&t(Oe,{rounded:"md",onClick:s,cursor:"pointer",flex:1,objectFit:"contain",src:(d=e==null?void 0:e.attachment)==null?void 0:d.fullUrl,alt:"",h:"full"}),(e==null?void 0:e.type)==="file"&&t(Ze,{target:"_blank",href:(f=e==null?void 0:e.attachment)==null?void 0:f.fullUrl,children:h(P,{"data-tip":"file-tooltip",flex:1,display:"flex",alignItems:"center",justifyContent:"center",bg:k("blue.50","slate.400"),rounded:"md",h:"full",children:[t(l,{className:"w-5 h-5 "}),t(Ca,{children:t("p",{children:cn((m=e==null?void 0:e.attachment)==null?void 0:m.path)}),place:"top",type:k("info","success"),effect:"float"})]})})]})}),ud=c.exports.memo(()=>{const e=_(u=>u.conversationId),n=_(u=>u.userId),[a,r]=c.exports.useState(null),[s,o]=c.exports.useState(!1),{attachments:l,isLoadingMore:i,onLoadMore:d,hasNextPage:f}=ld({conversationId:e,userId:n}),m=c.exports.useCallback(async()=>{await d()},[d]);return h(te,{alignItems:"stretch",spacing:2.5,py:2,children:[t(pa,{allowMultiple:!0,children:h(la,{children:[h(R,{position:"relative",spacing:0,pl:4,_hover:{backgroundColor:k("#f0f0f0","slate.600")},children:[t(P,{display:"flex",justifyContent:"start",flex:1,children:t(L,{color:k("#444","white"),fontSize:16,fontWeight:"medium",children:"T\xE0i li\u1EC7u"})}),t(ca,{display:"flex",alignItems:"center",justifyContent:"center",w:10,children:t(da,{})})]}),h(ua,{pb:4,children:[(l==null?void 0:l.length)===0&&t(L,{textAlign:"center",children:"Cu\u1ED9c tr\xF2 chuy\u1EC7n ch\u01B0a c\xF3 t\xE0i li\u1EC7u !!!"}),t(Be,{columns:3,spacing:2,children:l==null?void 0:l.map((u,$)=>t(dd,{item:u,setAttachmentIndex:r,setShowImageViewer:o,index:$}))}),f&&t(Y,{h:"32px",isLoading:i,loadingText:"\u0110ang t\u1EA3i th\xEAm",onClick:m,w:"100%",rounded:"none",borderWidth:"1px 0px 0px 0px",bg:k("white","slate.600"),fontSize:14,fontWeight:400,justifyContent:"center",mt:2,children:"Xem th\xEAm"})]})]})}),t(po,{attachments:l,attachmentIndex:a,setAttachmentIndex:r,showImageViewer:s,setShowImageViewer:o})]})}),pd=({isOpen:e,onClose:n,userInfo:a,isOwner:r,groupId:s,isSupperAdmin:o,visibleRemoveMemberButton:l})=>{var E;const i=Ne(),{isOpen:d,onOpen:f,onClose:m}=Pe(),u=He.useRef(),[$,{loading:p}]=Ae(el),A=c.exports.useCallback(async()=>{var C;try{await $({variables:{userId:(C=a==null?void 0:a.userId)==null?void 0:C.toString(),id:s}}),i(["conversationDetail"]),m(),n()}catch(S){console.log(S)}},[a,s]);return h(Se,{children:[h(Ve,{isOpen:e,onClose:n,children:[t(Le,{}),h(We,{children:[t(Re,{textAlign:"center",children:"Th\xF4ng tin th\xE0nh vi\xEAn"}),t(Qe,{}),h(Ye,{pb:4,children:[t(te,{p:4,pt:0,children:t(Ie,{size:"2xl",src:a.avatar,name:a.fullName})}),t(te,{alignItems:"stretch",children:h(R,{justifyContent:r||o?"space-between":"center",alignItems:"center",children:[h(L,{textAlign:r||o?"left":"center",fontSize:"xl",children:[a.fullName," ",`(${(E=nn.find(C=>C.id===Number(a==null?void 0:a.roleId)))==null?void 0:E.name})`]}),l&&t(Y,{onClick:f,size:"sm",colorScheme:"red",variant:"solid",fontSize:13,textColor:"white",children:"M\u1EDDi r\u1EDDi nh\xF3m"})]})})]})]})]}),t(Bs,{isOpen:d,leastDestructiveRef:u,onClose:m,children:t(Le,{children:h(Ls,{children:[t(Re,{fontSize:"lg",fontWeight:"bold",children:"C\u1EA3nh b\xE1o !!!"}),t(Ye,{children:"B\u1EA1n c\xF3 ch\u1EAFc kh\xF4ng? B\u1EA1n kh\xF4ng th\u1EC3 ho\xE0n t\xE1c h\xE0nh \u0111\u1ED9ng n\xE0y sau khi x\xF3a."}),h(rt,{children:[t(Y,{disabled:p,ref:u,onClick:m,children:"Tr\u1EDF l\u1EA1i"}),t(Y,{disabled:p,colorScheme:"red",onClick:A,ml:3,color:"white",loadingText:"\u0110ang x\u1EED l\xFD",children:"X\xE1c nh\u1EADn"})]})]})})})]})},gd=c.exports.memo(({conversationDetail:e})=>{var p,A,E,C;const n=c.exports.useRef(null),a=_(S=>S.conversationId),[r]=ae(),{isOpen:s,onClose:o,onOpen:l}=Pe(),{isOpen:i,onClose:d,onOpen:f}=Pe(),[m,u]=c.exports.useState(null),$=c.exports.useMemo(()=>{var S;return(S=e==null?void 0:e.participants)==null?void 0:S.map(g=>{var y,T;const I=((y=e==null?void 0:e.owner)==null?void 0:y.userId)===(g==null?void 0:g.userId);return t(mt,v(b({},g),{isOwner:I,lastOnline:g==null?void 0:g.online,avatarUrl:g==null?void 0:g.avatar,username:g==null?void 0:g.fullName,positionName:(T=nn.find(M=>M.id===Number(g==null?void 0:g.roleId)))==null?void 0:T.name,onClick:async()=>{await u(g),await f()},px:0}),g.userId)})},[e]);return c.exports.useEffect(()=>{let S;return(n==null?void 0:n.current)&&(S=new It(n==null?void 0:n.current,{wheelSpeed:1,minScrollbarLength:10})),()=>{S&&S.destroy()}},[]),h(te,{alignItems:"stretch",spacing:2.5,px:4,py:2,borderBottomWidth:1,children:[h(R,{justifyContent:"space-between",alignItems:"center",children:[t(L,{fontSize:14,color:k("gray.800","slate.300"),children:"Th\xE0nh vi\xEAn"}),((p=e==null?void 0:e.owner)==null?void 0:p.userId)===(r==null?void 0:r.id)&&t(P,{onClick:l,as:"button",w:6,h:6,display:"flex",alignItems:"center",justifyContent:"center",children:t(Rs,{className:"w-full h-full"})})]}),t(te,{alignItems:"stretch",spacing:0,maxH:"400px",ref:n,className:"scroll-container",overflow:"hidden",position:"relative",minH:0,children:$}),t(rn,{isChannel:!1,isAddSupporter:!1,isEdit:!0,isOpen:s,onClose:o,listParticipants:(A=e==null?void 0:e.participants)==null?void 0:A.map(S=>S==null?void 0:S.userId),name:e==null?void 0:e.name,groupId:a}),t(pd,{visibleRemoveMemberButton:r.id===((E=e==null?void 0:e.owner)==null?void 0:E.userId),isSupperAdmin:(r==null?void 0:r.roleId)===1,groupId:e==null?void 0:e.id,isOwner:((C=e==null?void 0:e.owner)==null?void 0:C.userId)===(m==null?void 0:m.userId),userInfo:m!=null?m:{},isOpen:i&&m,onClose:d})]})}),un={ongoing:{backgroundColor:"green.500"},incoming:{backgroundColor:"orange.400"},finished:{backgroundColor:"gray.300"}},hd=({status:e,title:n})=>h(R,{children:[t(P,{w:2.5,h:2.5,rounded:"full",bg:un==null?void 0:un[e].backgroundColor}),t(L,{flex:1,lineHeight:"20px",fontSize:14,color:k("#444","slate.300"),children:n})]}),md=c.exports.memo(({data:e})=>{const n=c.exports.useRef(null);c.exports.useEffect(()=>{let r;return(n==null?void 0:n.current)&&(r=new It(n==null?void 0:n.current,{wheelSpeed:1,minScrollbarLength:10})),()=>{r&&r.destroy()}},[]);const a=c.exports.useMemo(()=>e.map(r=>{var l;const{status:s,name:o}=(l=r==null?void 0:r.course)!=null?l:{};return c.exports.createElement(hd,v(b({},r),{key:r==null?void 0:r.id,status:s,title:o}))}),[e]);return h(te,{alignItems:"stretch",spacing:2.5,px:4,py:2,borderBottomWidth:1,maxH:"200px",ref:n,className:"scroll-container",overflow:"hidden",position:"relative",minH:0,children:[h(L,{fontSize:14,color:k("gray.800","slate.300"),children:["Kh\xF3a h\u1ECDc ",e.length]}),a]})}),Mo={create:[6],changeEC:[12,1,2],makeApointment:[6]},Ju={additionalTeacherOrCancelForStudent:[1,2],viewTableMockTestDetail:[1,2,6],canCreateOrEditMockTest:[1,2],assignTeacherAndEC:[1,2]},fd=He.forwardRef((e,n)=>{var a;return h(bt,{isInvalid:!!(e==null?void 0:e.error),width:"100%",children:[(e==null?void 0:e.label)&&t(Lt,{htmlFor:e.id,children:e==null?void 0:e.label}),t(Kt,b({minH:"unset",overflow:"hidden",w:"100%",resize:"none",ref:n,minRows:1,as:Jt,transition:"height none",bg:k("white","slate.700")},e)),(e==null?void 0:e.error)&&t(Ct,{children:(a=e==null?void 0:e.error)==null?void 0:a.message})]})}),Ad=(e={},n={})=>{const a=Te();return ge(yi,b({variables:e,onError:r=>{a({title:"L\u1EA5y d\u1EEF li\u1EC7u th\u1EA5t b\u1EA1i.",description:`GET list source status error: ${r==null?void 0:r.message}`,status:"error",duration:3e3,isClosable:!0})},fetchPolicy:"cache-first",errorPolicy:"all"},n))},Xu=x`
  mutation saveNote($rawId: Int, $note: String) @api(name: "appZim") {
    saveNote(rawId: $rawId, note: $note) {
      note
      createBy
      createDate
    }
  }
`,ep=x`
  mutation updateStatusLeads($input: updateStatusLeadsInputType)
  @api(name: "appZim") {
    updateStatusLeads(input: $input) {
      success
      error
      message
      dataString
    }
  }
`,tp=x`
  mutation makeApointment($input: MakeApointmentInputType)
  @api(name: "appZim") {
    makeApointment(input: $input) {
      success
      error
      message
      dataString
    }
  }
`,np=x`
  mutation changeEcLeads($input: ChangeEcLeadsInputType) @api(name: "appZim") {
    changeEcLeads(input: $input) {
      success
      error
      message
      dataString
    }
  }
`,xd=x`
  mutation addCustomerLeads($input: AddCustomerLeadInputType)
  @api(name: "appZim") {
    addCustomerLeads(input: $input) {
      success
      error
      message
      dataString
    }
  }
`,ap=x`
  query getReportLeads($startDate: String, $endDate: String)
  @api(name: "appZim") {
    getReportLeads(startDate: $startDate, endDate: $endDate) {
      date
      totalView
      dataLeads {
        date
        sourseName
        data
      }
    }
  }
`,qo=x`
  query getRawDataPagination(
    $q: String
    $status: RawDataStatusEnum
    $fromDate: DateTime
    $toDate: DateTime
    $orderBy: String
    $order: String
    $page: Int
    $limit: Int
    $ecId: Int
    $schoolId: Int
    $tags: [String]
    $date: DateTime
    $sourceName: String
    $isOnlyDataFacebook: Boolean
  ) @api(name: "appZim") {
    getRawDataPagination(
      q: $q
      status: $status
      fromDate: $fromDate
      toDate: $toDate
      orderBy: $orderBy
      order: $order
      page: $page
      limit: $limit
      ecId: $ecId
      schoolId: $schoolId
      tags: $tags
      date: $date
      sourceName: $sourceName
      isOnlyDataFacebook: $isOnlyDataFacebook
    ) {
      hasNextPage
      hasPrevPage
      limit
      page
      pageNext
      pagePrev
      pagingCounter
      totalPages
      totalDocs
      docs {
        id
        student {
          id
          user_name
          full_name
          phone
          address
          email
          role_id
          role_name
          supporter
          student_more_info {
            identity_card_city_id
            note_home
            birthday
            city_id
            city_name
            district_id
            district_name
            ward_id
            ward_name
            street_id
            street_name
            home_number
            source_id
            source_name
            job_id
            job_name
            identity_card
            identity_card_city_name
            identity_card_date
            learning_status_id
            work_place
            learning_status_name
          }
        }
        fb_user_id
        source_id
        ec_name
        ec_id
        source_name
        form_name
        link
        note
        created_date
        appoinment_test_id
        status
        tags {
          level_name
          name
        }
      }
    }
  }
`,op=x`
  query getRawDataNote($rawId: Int) @api(name: "appZim") {
    getRawDataNote(rawId: $rawId) {
      note
      createBy
      createDate
    }
  }
`,rp=x`
  query facebookCommentsPagination(
    $page: Int
    $limit: Int
    $fbProfileId: String
    $postId: ObjectID
    $search: String
  ) @api(name: "crawler") {
    facebookCommentsPagination(
      page: $page
      limit: $limit
      search: $search
      postId: $postId
      fbProfileId: $fbProfileId
    ) {
      totalDocs
      totalPages
      docs {
        id
        post {
          id
          link
        }
        content
        facebookId
        createdAt
        updatedAt
        owner
        linkProfile
        inDemand
        fbProfileId
      }
    }
  }
`,sp=x`
  query facebookPostsPagination(
    $page: Int
    $limit: Int
    $fbProfileId: String
    $search: String
  ) @api(name: "crawler") {
    facebookPostsPagination(
      page: $page
      limit: $limit
      search: $search
      fbProfileId: $fbProfileId
    ) {
      totalDocs
      totalPages
      docs {
        id
        content
        facebookId
        numOfComments
        numOfLikes
        numOfShares
        numOfCmtInDemand
        link
        featured
        createdAt
        updatedAt
        inDemand
        postOwnerId
        postOwnerName
      }
    }
  }
`,bd=async(e,n)=>{var a,r,s,o,l;if(e==="")return!1;try{const i=await ht.query({query:qo,variables:{limit:1,q:e,page:1}});return!(!((r=(a=i.data)==null?void 0:a.getRawDataPagination)==null?void 0:r.docs)||((l=(o=(s=i.data)==null?void 0:s.getRawDataPagination)==null?void 0:o.docs)==null?void 0:l.length)>0)}catch{return!1}},yd=Ce.object().shape({phone:Ce.string().matches(Po,"S\u1ED1 \u0111i\u1EC7n tho\u1EA1i kh\xF4ng h\u1EE3p l\u1EC7").required("S\u1ED1 \u0111i\u1EC7n tho\u1EA1i l\xE0 b\u1EAFt bu\u1ED9c").test("phone","\u0110\xE3 c\xF3 EC ch\u0103m s\xF3c S\u0110T n\xE0y. Vui l\xF2ng ki\u1EC3m tra l\u1EA1i trong lead",bd),name:Ce.string().required("Vui l\xF2ng nh\u1EADp h\u1ECD t\xEAn"),email:Ce.string().email("\u0110\u1ECBa ch\u1EC9 email kh\xF4ng h\u1EE3p l\u1EC7").required("Vui l\xF2ng nh\u1EADp email"),source:Ce.mixed().nullable(!0).required("M\u1EE5c n\xE0y l\xE0 b\u1EAFt bu\u1ED9c"),note:Ce.string().required("Vui l\xF2ng nh\u1EADp ghi ch\xFA ")}),qt=e=>t(Ra,b({w:{base:"100%",md:"50%"},p:2},e)),Cd=({onClose:e,isOpen:n})=>{const{handleSubmit:a,register:r,control:s,formState:{errors:o},reset:l}=$t({mode:"onSubmit",reValidateMode:"onBlur",resolver:Fa(yd)}),{data:i,loading:d}=Ad(),f=Te(),[m,{loading:u}]=Ae(xd,{refetchQueries:[qo],onCompleted:({addCustomerLeads:p})=>{var A;p.success?(f({title:"Th\xE0nh c\xF4ng !",description:"T\u1EA1o lead th\xE0nh c\xF4ng !",status:"success",duration:3e3,isClosable:!0}),l(),e()):f({title:"T\u1EA1o lead th\u1EA5t b\u1EA1i !",description:(A=p==null?void 0:p.message)!=null?A:"Vui l\xF2ng ki\u1EC3m tra l\u1EA1i th\xF4ng tin \u0111\xE3 nh\u1EADp",status:"error",duration:3e3,isClosable:!0})},onError:p=>{var A;f({title:"T\u1EA1o lead th\u1EA5t b\u1EA1i !",description:(A=p==null?void 0:p.message)!=null?A:"Vui l\xF2ng ki\u1EC3m tra l\u1EA1i th\xF4ng tin",status:"error",duration:3e3,isClosable:!0})}});return h(Ve,{isOpen:n,onClose:()=>{},size:"3xl",children:[t(Le,{}),t(We,{children:h("form",{onSubmit:a(async p=>{try{await m({variables:{input:{phoneNumber:p.phone,fullName:p.name,email:p.email,sourceId:p.source.id,note:p.note}}})}catch(A){console.log({e:A})}}),children:[t(Re,{children:"T\u1EA1o Lead m\u1EDBi"}),t(Qe,{onClick:e}),h(P,{px:4,pb:8,children:[h(Dt,{spacing:0,children:[t(qt,{children:t(ve,v(b({label:"S\u1ED1 \u0111i\u1EC7n tho\u1EA1i (*)",placeholder:"Nh\u1EADp...",variant:"outline",pr:10},r("phone")),{error:o==null?void 0:o.phone}))}),t(qt,{children:t(ve,v(b({},r("name")),{label:"H\u1ECD t\xEAn (*)",placeholder:"Nh\u1EADp...",error:o==null?void 0:o.name}))}),t(qt,{children:t(ve,v(b({label:"Email (*)",placeholder:"Nh\u1EADp..."},r("email")),{error:o==null?void 0:o.email}))}),t(qt,{children:t(dt,{name:"source",control:s,render:({field:p})=>t(_t,b({label:"Ngu\u1ED3n (*)",error:o==null?void 0:o.source,getOptionLabel:A=>A.sourceOfCustomer,getOptionValue:A=>A.id,options:i?i.getSourceOfCustomer:[],isLoading:d},p))})}),t(Ra,{w:"full",p:2,children:t(fd,v(b({label:"Ghi ch\xFA (*)",bg:"white",minRows:2,placeholder:"N\u1ED9i dung..."},r("note")),{error:o==null?void 0:o.note}))})]}),h(R,{px:2,mt:4,spacing:4,children:[t(Y,{type:"submit",colorScheme:"green",isLoading:u,children:"T\u1EA1o m\u1EDBi"}),t(Y,{variant:"outline",onClick:e,children:"H\u1EE7y b\u1ECF"})]})]})]})})]})},Ed=c.exports.memo(({conversationDetail:e})=>{const n=c.exports.useMemo(()=>{var a;return(a=e==null?void 0:e.participants)==null?void 0:a.map(r=>{var s;return t(mt,{avatarUrl:r==null?void 0:r.avatar,username:r==null?void 0:r.fullName,positionName:(s=nn.find(o=>{var l;return o.id===Number((l=r.roleId)!=null?l:14)}))==null?void 0:s.name},r==null?void 0:r.userId)})},[e]);return t(P,{borderBottomWidth:"1px",borderColor:"whiteAlpha.300",children:n})}),$d=()=>{var y,T,M,w,B,Q,D,H,F,V,O,U,W,ne;const{onOpen:e,isOpen:n,onClose:a}=Pe(),{isOpen:r,onOpen:s,onClose:o}=Pe(),l=_(q=>q.userId),[i]=ae(),d=_(q=>q.visibleUserProfile),f=_(q=>q.setVisibleUserProfile),m=_(q=>q.conversationId),{conversationDetail:u}=So(m,l),$=Io({conversationDetail:u}),p=(y=u==null?void 0:u.participants)==null?void 0:y.find(q=>(q==null?void 0:q.roleId)==="5"||(q==null?void 0:q.roleId)===null),A=$?p:(T=u==null?void 0:u.participants)==null?void 0:T.filter(q=>(q==null?void 0:q.userId)!==(i==null?void 0:i.id.toString()))[0],{userData:E,loading:C}=vo(l||(A==null?void 0:A.userId),u.name),S=$?(A==null?void 0:A.fullName)||((M=u==null?void 0:u.owner)==null?void 0:M.fullName):(E==null?void 0:E.full_name)?E==null?void 0:E.full_name:(A==null?void 0:A.isGuest)?`${A==null?void 0:A.fullName}`:A==null?void 0:A.fullName;Ka("click",q=>{const z=q.target;!z.closest(".profile")&&!z.closest("#js-toggle-profile")&&f(!1)});const I=ut({base:"",xl:"visible"});return!l&&!m?null:h(Se,{children:[h(ue,{pos:I?void 0:"fixed",top:I?0:"56px",right:0,bottom:0,zIndex:51,h:"full",overflowY:"auto",bg:k("white","slate.900"),w:{base:"320px","2xl":"380px"},transform:I||d?"none":"translateX(100%)",transition:"all .3s ease",className:"profile",id:"profile-portal",spacing:0,children:[!(u==null?void 0:u.name)&&(((w=u==null?void 0:u.participants)==null?void 0:w.length)===2&&((B=u==null?void 0:u.participants)==null?void 0:B.find(q=>q.userId===i.id.toString()))||l||$&&Bo.includes(i==null?void 0:i.roleId))&&h(P,{children:[t(ad,{tags:(Q=u==null?void 0:u.tags)!=null?Q:[],isStudent:Ot.includes(i==null?void 0:i.roleId),participantAvatarUrl:A==null?void 0:A.avatar,fullName:S,loading:C,participants:u==null?void 0:u.participants,participantInfo:E}),Mo.create.includes(i==null?void 0:i.roleId)&&(A==null?void 0:A.isGuest)&&h(P,{p:2,borderBottomWidth:1,borderColor:"whiteAlpha.300",children:[t(Y,{w:"full",colorScheme:"green",onClick:s,children:"Th\xEAm lead m\u1EDBi"}),Mo.create.includes(i==null?void 0:i.roleId)&&t(Cd,{isOpen:r,onClose:o})]}),(E==null?void 0:E.supporter)&&!Ot.includes(i==null?void 0:i.roleId)&&((H=(D=u==null?void 0:u.participants)==null?void 0:D.filter(q=>q.roleId!=="5"))==null?void 0:H.map(q=>{var X;const z=(q==null?void 0:q.userId)===((X=E==null?void 0:E.supporter)==null?void 0:X.id.toString());return t(mt,{username:q==null?void 0:q.fullName,positionName:`${z?"EC ch\xEDnh":"EC h\u1ED7 tr\u1EE3"}`},q==null?void 0:q.id)})),((F=E==null?void 0:E.supporter)==null?void 0:F.id)===Number(i==null?void 0:i.id)&&((V=u==null?void 0:u.participants)==null?void 0:V.length)&&t(P,{w:"full",px:2,children:t(Y,{onClick:e,w:"full",rightIcon:t(Ds,{}),colorScheme:"blue",variant:"outline",children:"Th\xEAm EC h\u1ED7 tr\u1EE3"})})]}),$&&!(u==null?void 0:u.name)&&t(Ed,{conversationDetail:u}),(u==null?void 0:u.name)&&t(gd,{conversationDetail:u}),m&&t(id,{isSuperAdmin:No.includes(i==null?void 0:i.roleId),allowForSupperAdmin:No.includes(i==null?void 0:i.roleId)&&((O=u==null?void 0:u.participants)==null?void 0:O.some(q=>(q==null?void 0:q.userId)===(i==null?void 0:i.id.toString())))}),((U=E==null?void 0:E.courseStudents)==null?void 0:U.length)>0&&!Ot.includes(i==null?void 0:i.roleId)&&t(md,{data:(W=E==null?void 0:E.courseStudents)!=null?W:[]}),t(ud,{})]}),d&&t(Se,{children:t(P,{bg:"blackAlpha.800",shadow:"md",pos:"fixed",top:14,bottom:0,zIndex:50,left:0,right:0,h:"screen"})}),t(rn,{isChannel:!1,isEdit:!0,isAddSupporter:!0,isOpen:n,onClose:a,listParticipants:(ne=u==null?void 0:u.participants)==null?void 0:ne.map(q=>q==null?void 0:q.userId),name:u==null?void 0:u.name,groupId:m})]})},_=zs(e=>({conversationId:"",setConversationId:n=>e({conversationId:n}),userId:"",setUserId:n=>e({userId:n}),tab:"conversation",setTab:n=>e({tab:n}),channel:"",setChannel:n=>e({channel:n}),participants:[],setParticipants:n=>e({participants:n}),visibleUserProfile:!1,setVisibleUserProfile:n=>e({visibleUserProfile:n}),searchStringMessage:"",setSearchStringMessage:n=>e({searchStringMessage:n}),searchResult:[],setSearchResult:n=>e({searchResult:n}),nearSearchResult:[],setNearSearchResult:n=>e({nearSearchResult:n}),messageId:"",setMessageId:n=>e({messageId:n}),beforeId:"",setBeforeId:n=>e({beforeId:n}),afterId:"",setAfterId:n=>e({afterId:n})})),Oo=()=>{const e=_(A=>A.setChannel),n=_(A=>A.userId),a=_(A=>A.setUserId),r=_(A=>A.tab),s=_(A=>A.conversationId),o=_(A=>A.setConversationId),[l]=ae(),[i,d]=De({}),{height:f}=il(),m=ut({base:"",md:"visible"});Ll();const u=i.get("qsConversationId"),$=i.get("qsUserId"),p=i.get("qsGroup");return c.exports.useEffect(()=>{(l==null?void 0:l.roleId)===6?e("guest"):e("my chats")},[l]),c.exports.useEffect(()=>{u||$?(u&&o(u),$&&a($),p&&e(p)):(i.delete("qsConversationId"),i.delete("qsUserId"),i.delete("qsGroup"))},[u,$,p]),h(te,{h:f-113,spacing:0,w:"full",children:[!m&&t(cl,{}),h(P,{w:"100%",h:"100%",display:"flex",className:"divide-x",overflow:"hidden",children:[(m||!m&&r==="channel")&&t(Fd,{}),(m||!m&&r==="conversation")&&t(Nl,{}),(m||!m&&r==="message")&&t(Hc,{}),(n||s)&&t($d,{})]})]})};var Sd=Object.freeze({__proto__:null,[Symbol.toStringTag]:"Module",useStore:_,default:Oo});const vd=()=>{const e=Te(),n=Ne(),a=_(l=>l.conversationId),[r,{loading:s}]=Ae(al);return{onDeleteTag:c.exports.useCallback(async l=>{try{await r({variables:{tagId:l,conversationId:a}}),await n(["conversationDetail"])}catch(i){e({title:"L\u1ED7i !!!",description:i.message,status:"success",duration:2e3,isClosable:!0})}},[a,r,e,n]),loading:s}};function Id({id:e,name:n,color:a}){const{onDeleteTag:r,loading:s}=vd(),o=_(l=>l.conversationId);return h(fa,{size:"md",rounded:"full",children:[t(P,{bg:a,w:"5px",h:"5px",rounded:"full",mr:1}),t(Aa,{children:n}),s?t(ot,{color:"red.500",size:"xs",ml:2.5}):o&&t(xa,{onClick:()=>r(e)})]})}const kd=(e,n)=>{const[a]=ae();return c.exports.useMemo(()=>{var r,s;return[{icon:t(js,{}),value:(r=e==null?void 0:e.address)!=null?r:"Kh\xF4ng c\xF3 th\xF4ng tin",visible:Ft.includes(a.roleId)},{icon:t(Us,{}),value:(e==null?void 0:e.phone)||"Kh\xF4ng c\xF3 th\xF4ng tin",visible:Ft.includes(a.roleId)},{icon:t(Gs,{}),value:((s=e==null?void 0:e.more_info)==null?void 0:s.facebook)||"Kh\xF4ng c\xF3 th\xF4ng tin",visible:Ft.includes(a.roleId)},{icon:t(Hs,{}),value:(e==null?void 0:e.email)||"Kh\xF4ng c\xF3 th\xF4ng tin",visible:Ft.includes(a.roleId)},{icon:t(Vs,{}),value:(n==null?void 0:n.length)?()=>t(Dt,{flex:1,children:n==null?void 0:n.map((o,l)=>t(Id,b({},o),l))}):"Kh\xF4ng c\xF3 tag",visible:!0}]},[e,n,a])},Fo=["Admin","CC","CS","GV","EC","AM","KT","KTH","MK","BM","BP"],pn=[{id:1,name:"Super Admin"},{id:2,name:"CC"},{id:4,name:"Gi\xE1o vi\xEAn"},{id:6,name:"EC"},{id:8,name:"AM"},{id:11,name:"Marketer"},{id:12,name:"BM"}],No=[1],Ot=[5],Bo=[5],Ft=[1,2,6],wd="File c\u1EE7a b\u1EA1n v\u01B0\u1EE3t qu\xE1 10MB.",Td="File c\u1EE7a b\u1EA1n kh\xF4ng \u0111\u01B0\u1EE3c h\u1ED7 tr\u1EE3.",Pd=[{color:"#00DAA3"},{color:"#21A3F3"},{color:"#F51C50"},{color:"#006E7F"},{color:"#F8CB2E"}],_d=c.exports.memo(({searchString:e,overallMessageUnRead:n})=>{const[a]=ae(),r=_(y=>y.setChannel),s=_(y=>y.setParticipants),o=_(y=>y.setConversationId),l=_(y=>y.setTab),i=_(y=>y.setUserId),[d,f]=De({}),{listAccount:m,onLoadMore:u,hasNextPage:$,isLoadingMore:p,loading:A}=oo({roles:Fo,searchString:e}),E=c.exports.useCallback(async()=>{await u()},[u]),C=c.exports.useMemo(()=>A?[1,2,3,4,5,6,7,8]:(m==null?void 0:m.filter(y=>{var T;return((T=y.user)==null?void 0:T.id)!==parseInt(a==null?void 0:a.id,10)}))||[],[a,A,m]),S=c.exports.useCallback(()=>{r("zimians"),l("conversation")},[]),g=c.exports.useCallback((y,T)=>{i(y),f({qsUserId:y,qsGroup:T}),d.delete("qsConversationId"),o(""),l("message"),r(T),s([])},[]),I=c.exports.useMemo(()=>t(to,{canLoadMore:$,isLoading:p,onLoadMore:E,onClick:g,loading:A,conversations:C}),[C,A,$,p,g,E]);return t(no,{onClick:S,title:"Zimians",overallMessageUnRead:n,children:I})}),Md=e=>{var r;const{data:n}=ge(ui,{variables:{userId:e}}),a=n==null?void 0:n.getUserById;return{supporterInfo:a,id:(r=a==null?void 0:a.supporter)==null?void 0:r.id}},qd=({search:e})=>{var f,m;const n=v(b({},se({isChannel:!0,search:e},u=>u)),{limit:100,offset:0}),[a,{data:r,loading:s,fetchMore:o,called:l}]=Gt(pe,{variables:n,fetchPolicy:"network-only",nextFetchPolicy:"cache-first"});c.exports.useEffect(()=>{l||(async()=>await a())()},[l,a]);const i=((f=r==null?void 0:r.conversations)==null?void 0:f.docs)||[],d=(m=r==null?void 0:r.conversations)==null?void 0:m.hasNextPage;return Ge({variables:v(b({},n),{offset:(i==null?void 0:i.length)+1}),fetchMore:o,hasNextPage:d}),{listChannel:i,loading:s}},Od=c.exports.memo(({searchString:e,overallMessageUnRead:n})=>{const a=_(g=>g.setChannel),r=_(g=>g.setParticipants),s=_(g=>g.setConversationId),o=_(g=>g.setTab),l=_(g=>g.setUserId),[i,d]=De({}),{listAccount:f,hasNextPage:m,isLoadingMore:u,onLoadMore:$,loading:p}=oo({roles:["HV"],searchString:e}),A=c.exports.useMemo(()=>p?[1,2,3,4,5,6,7,8]:f,[p,f]),E=c.exports.useCallback(async()=>{await $()},[$]),C=c.exports.useCallback(()=>{a("student"),o("conversation")},[]),S=c.exports.useCallback((g,I)=>{l(g),d({qsUserId:g,qsGroup:I}),i.delete("qsConversationId"),s(""),o("message"),a(I),r([])},[]);return t(no,{overallMessageUnRead:n,onClick:C,title:"Student",children:t(to,{isLoading:u,canLoadMore:m,onLoadMore:E,onClick:S,loading:p,conversations:A})})}),gn=c.exports.memo(({count:e})=>t(Ws,{w:6,h:6,variant:"solid",rounded:"full",bg:"blue.500",position:"absolute",right:8,alignItems:"center",justifyContent:"center",display:"flex",fontSize:11,children:t(P,{alignItems:"center",justifyContent:"center",display:"flex",children:e>99?"99+":e})})),Fd=c.exports.memo(()=>{var I,y;const[e]=ae(),[n,a]=De({}),r=_(T=>T.setParticipants),s=_(T=>T.setChannel),o=_(T=>T.setUserId),l=_(T=>T.setConversationId),i=_(T=>T.setTab),[d,f]=c.exports.useState(""),m=ft(d,500),{overall:u,overallMyChats:$}=ji(),{id:p}=Md(Number(e==null?void 0:e.id)),{listChannel:A}=qd(d);A==null||A.filter(T=>T.name&&!["zimians","student"].includes(T.group));const E=c.exports.useMemo(()=>n,[]),C=c.exports.useCallback(async()=>{a({qsUserId:p}),E.delete("qsConversationId"),E.delete("qsGroup"),s("student"),o(p),i("message"),l(""),r([])},[p]),S=c.exports.useRef(null),g=c.exports.useCallback(()=>{s("guest"),i("conversation")},[]);return c.exports.useEffect(()=>{let T;return(S==null?void 0:S.current)&&(T=new It(S==null?void 0:S.current,{wheelSpeed:1,minScrollbarLength:10})),()=>{T&&T.destroy()}},[]),h(ze,{direction:"column",w:{base:"full",md:"200px",lg:"250px"},bg:k("white","#10172a"),h:"100%",children:[t(Vi,{searchString:d,setSearchString:f}),h(P,{flexGrow:1,ref:S,className:"scroll-container",overflow:"hidden",position:"relative",minH:0,children:[h(ue,{zIndex:50,spacing:0,position:"sticky",top:0,children:[(e==null?void 0:e.roleId)===5&&p&&t(P,{display:"flex",alignItems:"center",px:4,h:10,cursor:"pointer",_hover:{backgroundColor:k("white","slate.500")},onClick:C,w:"100%",rounded:"none",borderWidth:"1px 0px 0px 0px",bg:k("white","#10172a"),fontSize:16,fontWeight:500,justifyContent:"start",children:t(L,{children:"Chat v\u1EDBi supporter"})}),h(P,{display:"flex",alignItems:"center",px:4,h:10,cursor:"pointer",_hover:{backgroundColor:k("white","slate.500")},onClick:()=>{s("my chats"),i("conversation")},w:"100%",rounded:"none",borderWidth:"1px 0px 0px 0px",bg:k("white","#10172a"),fontSize:16,fontWeight:500,justifyContent:"start",children:["My Chats",$>0&&t(gn,{count:$})]}),(e==null?void 0:e.roleId)!==4&&(e==null?void 0:e.roleId)!==5&&h(Y,{display:"flex",alignItems:"center",px:4,h:10,cursor:"pointer",_hover:{backgroundColor:k("white","slate.500")},onClick:g,w:"100%",rounded:"none",borderWidth:"1px 0px 0px 0px",bg:k("white","#10172a"),fontSize:16,fontWeight:500,justifyContent:"start",children:["Guest",(u==null?void 0:u[3])+(u==null?void 0:u[4])>0&&t(gn,{count:(u==null?void 0:u[3])+(u==null?void 0:u[4])})]})]}),h(ue,{display:{base:"block"},spacing:0,children:[(e==null?void 0:e.roleId)!==5&&t(_d,{overallMessageUnRead:(I=u==null?void 0:u[0])!=null?I:null,searchString:m}),t(Od,{overallMessageUnRead:(y=u==null?void 0:u[1])!=null?y:null,searchString:m})]})]})]})}),Nd=le(()=>ce(()=>import("./index5.js"),["assets/index5.js","assets/index3.js","assets/vendor.js","assets/useBuildModeValue.js"])),Bd=le(()=>ce(()=>import("./index6.js"),["assets/index6.js","assets/vendor.js","assets/index7.js","assets/index8.js","assets/dateToStringFormat.js","assets/index9.js","assets/CourseFilter.js","assets/index11.js","assets/index10.js","assets/index3.js","assets/useBuildModeValue.js"])),Ld=le(()=>ce(()=>import("./index12.js"),["assets/index12.js","assets/vendor.js","assets/dateToStringFormat.js","assets/arrayToSelectOption.js","assets/customParseFormat.js","assets/instance.js","assets/index11.js","assets/main.js"])),Rd=le(()=>ce(()=>import("./index13.js"),["assets/index13.js","assets/index10.js","assets/vendor.js","assets/index3.js","assets/useBuildModeValue.js","assets/index14.js","assets/tickets.js"])),Dd=le(()=>ce(()=>import("./index15.js"),["assets/index15.js","assets/tickets.js","assets/vendor.js","assets/index8.js","assets/index9.js","assets/useBuildModeValue.js","assets/index3.js","assets/index7.js"])),zd=le(()=>ce(()=>Promise.resolve().then(function(){return Sd}),void 0)),jd=le(()=>ce(()=>import("./index16.js"),["assets/index16.js","assets/index14.js","assets/vendor.js","assets/index8.js","assets/index3.js","assets/useBuildModeValue.js","assets/customParseFormat.js","assets/index11.js","assets/dateToStringFormat.js","assets/index10.js"])),Ud=le(()=>ce(()=>import("./index17.js"),["assets/index17.js","assets/vendor.js","assets/index9.js","assets/index8.js","assets/index7.js","assets/useBuildModeValue.js","assets/index3.js"])),Gd=le(()=>ce(()=>import("./index18.js"),["assets/index18.js","assets/index14.js","assets/vendor.js","assets/main.js","assets/index19.js","assets/utils.js","assets/isSameOrAfter.js","assets/dateToStringFormat.js","assets/index20.js","assets/index10.js","assets/index3.js","assets/useBuildModeValue.js","assets/index7.js","assets/useGetCenterByUser.js","assets/instance.js","assets/arrayToSelectOption.js","assets/customParseFormat.js"])),Hd=le(()=>ce(()=>import("./index21.js"),["assets/index21.js","assets/index22.js","assets/vendor.js","assets/useBuildModeValue.js","assets/index3.js","assets/index8.js","assets/index7.js","assets/index10.js","assets/CourseFilter.js","assets/index11.js","assets/groupByDate.js","assets/useListSubjectsOfMockTest.js","assets/arrayToSelectOption.js","assets/customParseFormat.js","assets/useGetCenterByUser.js","assets/instance.js","assets/index9.js"])),Vd=le(()=>ce(()=>import("./index23.js"),["assets/index23.js","assets/index22.js","assets/vendor.js","assets/useBuildModeValue.js","assets/index3.js","assets/index8.js","assets/index7.js","assets/index10.js","assets/CourseFilter.js","assets/index11.js","assets/groupByDate.js","assets/useListSubjectsOfMockTest.js","assets/arrayToSelectOption.js","assets/customParseFormat.js","assets/useGetCenterByUser.js","assets/instance.js","assets/index9.js"])),Wd=le(()=>ce(()=>import("./index24.js").then(function(e){return e.i}),["assets/index24.js","assets/index7.js","assets/vendor.js","assets/index8.js","assets/groupByDate.js","assets/index20.js","assets/useBuildModeValue.js"])),Qd=le(()=>ce(()=>import("./index25.js").then(function(e){return e.i}),["assets/index25.js","assets/main.js","assets/vendor.js","assets/dateToStringFormat.js","assets/arrayToSelectOption.js","assets/customParseFormat.js","assets/index19.js","assets/index11.js","assets/useGetCenterByUser.js","assets/instance.js"])),Yd=le(()=>ce(()=>import("./index26.js"),["assets/index26.js","assets/index14.js","assets/vendor.js"])),Zd=le(()=>ce(()=>import("./index27.js"),["assets/index27.js","assets/vendor.js","assets/index7.js","assets/index8.js","assets/index9.js","assets/index3.js","assets/useBuildModeValue.js","assets/index10.js","assets/index11.js"])),Kd=le(()=>ce(()=>import("./index28.js"),["assets/index28.js","assets/index7.js","assets/vendor.js","assets/index8.js","assets/arrayToSelectOption.js","assets/customParseFormat.js","assets/useListSubjectsOfMockTest.js","assets/index9.js"])),ip=[{path:me.home,exact:!0,component:Nd,allowRoles:[],breadcrumbText:"Trang ch\u1EE7"},{path:me.courseDetail,exact:!0,component:Ld,allowRoles:[],breadcrumbText:"Kh\xF3a h\u1ECDc"},{path:me.listCourses,exact:!0,component:Bd,allowRoles:[],breadcrumbText:"Danh s\xE1ch kh\xF3a h\u1ECDc"},{path:`${me.ticket}`,exact:!0,component:Dd,allowRoles:[],breadcrumbText:"Danh s\xE1ch ticket"},{path:`${me.ticket}/:id`,exact:!0,component:Rd,allowRoles:[],breadcrumbText:"Chi ti\u1EBFt ticket"},{path:`${me.leadReport}`,exact:!1,component:jd,allowRoles:[],breadcrumbText:"Lead Report"},{path:`${me.registerTestSchedule}`,exact:!1,component:Yd,allowRoles:[],breadcrumbText:"\u0110\u0103ng k\xFD l\u1ECBch tr\u1EF1c test"},{path:`${me.lead}`,exact:!1,component:Zd,allowRoles:[],breadcrumbText:"Ngu\u1ED3n kh\xE1ch h\xE0ng"},{path:`${me.studentSupportByEc}`,exact:!1,component:Ud,allowRoles:[],breadcrumbText:"Danh s\xE1ch h\u1ECDc vi\xEAn"},{path:me.chat,exact:!0,component:zd,allowRoles:[]},{path:me.scheduleRegistration,exact:!0,component:Gd,allowRoles:[],breadcrumbText:"\u0110\u0103ng k\xFD l\u1ECBch l\xE0m vi\u1EC7c"},{path:me.listSchedules,exact:!0,component:Qd,allowRoles:[],breadcrumbText:"Danh s\xE1ch l\u1ECBch l\xE0m vi\u1EC7c"},{path:me.paperBasedTestBank,exact:!0,component:Kd,allowRoles:[],breadcrumbText:"Danh s\xE1ch \u0111\u1EC1 thi"},{path:me.mockTestOnline,exact:!0,component:Hd,allowRoles:[],breadcrumbText:"Mock test online"},{path:me.mockTestOffline,exact:!0,component:Vd,allowRoles:[],breadcrumbText:"Mock test offline"},{path:`${me.mockTestDetail}/:id`,exact:!0,component:Wd,allowRoles:[],breadcrumbText:"Mock test detail"}];Ce.object().shape({username:Ce.string().required("Vui l\xF2ng nh\u1EADp t\xE0i kho\u1EA3n"),password:Ce.string().required("Vui l\xF2ng nh\u1EADp m\u1EADt kh\u1EA9u")});function Jd({error:e,componentStack:n,resetErrorBoundary:a}){return h(Qs,{role:"alert",status:"error",as:te,children:[h(Ys,{p:4,d:"block",children:[t("pre",{children:e.message}),t("pre",{children:n})]}),t(Y,{onClick:a,children:"Try again"})]})}le(()=>ce(()=>import("./index2.js"),["assets/index2.js","assets/vendor.js","assets/index3.js","assets/useBuildModeValue.js"]));le(()=>ce(()=>import("./index4.js"),["assets/index4.js","assets/vendor.js","assets/index3.js","assets/useBuildModeValue.js"]));function Xd(){const[e,n]=c.exports.useState(!0),{appSetLogin:a,appSetLogout:r,appState:s}=qi(),o=Oi(),{loaded:l}=s,i=()=>{};return c.exports.useEffect(()=>{if(!!o()){a(Wa("token")),Object.values(global.wsClients).forEach(d=>{d.connect()}),n(!1);return}},[o()]),t(Zs,{FallbackComponent:Jd,onError:i,children:t(Ks,{children:e||!l?t(R,{w:"100%",h:"100vh",justifyContent:"center",alignItems:"center",children:t(At,{text:"\u0110ang l\u1EA5y d\u1EEF li\u1EC7u..."})}):t(Js,{children:t(Xs,{path:"*",element:t(Oo,{})})})})})}const eu={secondary:"#c89934",primary:"#3463c8",gray:{50:"#F8F8F8",100:"#F0F0F0",200:"#E6E6E6",300:"#D5D5D5",400:"#B0B0B0",500:"#909090",600:"#686868",700:"#555555",800:"#373737",900:"#171717"},secondBrand:{50:"#f8fae8",100:"#eff2c5",200:"#e5e9a0",300:"#dce07d",400:"#d6da66",500:"#d1d551",600:"#cfc449",700:"#ccae3f",800:"#c89934",900:"#c07722"},brand:{50:"#e4f2ff",100:"#bedeff",200:"#94caff",300:"#6bb5ff",400:"#4fa4ff",500:"#3463c8",600:"#2e44a8",700:"#243b9c",800:"#1b3190",900:"#081f7c"},pink:{50:"#f9e4ec",100:"#f2bcd0",200:"#eb92b1",300:"#e36993",400:"#dd4d7c",500:"#d93767",600:"#c83463",700:"#b2305d",800:"#9d2c58",900:"#77264c"},blue:{50:"#E1F2FC",100:"#B7DDFA",200:"#88C8F7",300:"#55B3F4",400:"#21A3F3",500:"#0093F1",600:"#0085E4",700:"#0074D1",800:"#0063BF",900:"#0045A1"},green:{50:"#E8F5E9",100:"#C8E6C9",200:"#A5D6A7",300:"#81C784",400:"#66BB6A",500:"#4CAF50",600:"#43A047",700:"#388E3C",800:"#2E7D32",900:"#1B5E20"},red:{50:"#FFEAED",100:"#FFCBD5",200:"#F1959E",300:"#E9686B",400:"#F54258",500:"#FC243E",600:"#EC163D",700:"#DA0037",800:"#CD002F",900:"#BF0022"},slate:{50:"#f8fafc",100:"#f1f5f9",200:"#e2e8f0",300:"#cbd5e1",400:"#94a3b8",500:"#64748b",600:"#475569",700:"#334155",800:"#1e293b",900:"#0f172a"},orange:{50:"#FFF4E2",100:"#FFE1B6",200:"#FFCE88",300:"#FFBA5A",400:"#FFAB3A",500:"#FF9C27",600:"#FF9125",700:"#FA8123",800:"#F37121",900:"#E9581E"}},tu={baseStyle:({colorMode:e})=>{const n=e==="light";return{display:"block",background:n?"white":"slate.800",gap:6,border:"1px solid",borderColor:n?"gray.200":"slate.700"}},variants:{rounded:{padding:8,borderRadius:"xl"},smooth:{padding:4,borderRadius:"base"}},defaultProps:{variant:"smooth"}},nu={baseStyle:({colorMode:e})=>{const n=e==="dark";return{table:{th:{background:n?"slate.700":"gray.50",lineHeight:1.5},td:{background:n?"slate.800":"white",lineHeight:1.5}}}},variants:{simple:({colorMode:e})=>{const a=e==="dark"?"slate.700":"gray.300";return{td:{borderBottomColor:a,paddingTop:2,paddingBottom:2,paddingInlineStart:4,paddingInlineEnd:4},th:{borderBottomColor:a,paddingInlineStart:4,paddingInlineEnd:4,paddingTop:2,paddingBottom:2}}}},defaultProps:{variant:"simple",size:"sm"}},au={baseStyle:({colorMode:e})=>({textTransform:"capitalize",padding:"0.25rem 0.5rem ",borderRadius:4}),variants:{},defaultProps:{variant:"subtle",size:"sm"}},ou={baseStyle:{_invalid:{boxShadow:0}},variants:{outline:{display:"none"}},defaultProps:{variant:"outline"}},ru={baseStyle:{_hover:{textDecoration:"none",color:"brand.500"}}},su={baseStyle:({colorMode:e})=>({dialog:{background:e==="light"?"white":"slate.800"}})},iu={baseStyle:({colorMode:e})=>({dialog:{background:e==="light"?"white":"slate.800"}})};var lu={Card:tu,Table:nu,Badge:au,Input:ou,Link:ru,Modal:su,Drawer:iu};const cu={initialColorMode:"light",useSystemColorMode:!1},du={sm:"320px",md:"768px",lg:"960px",xl:"1280px","2xl":"1536px"},Lo=ei({config:cu,colors:eu,breakpoints:du,styles:{global:e=>({"html, body":{color:e.colorMode==="dark"?"gray.100":"gray.900",lineHeight:1.5},a:{color:e.colorMode==="dark"?"blue.300":"blue.500"},ul:{paddingRight:1}})},semanticTokens:{text:{default:"gray.900",_dark:"slate.400"},colors:{error:{default:"red.500",_dark:"red.200"},"gray.500":{default:"gray.500",_dark:"slate.400"}}},fonts:{body:"'Roboto', sans-serif",heading:"'Arial', sans-serif",mono:"monospace"},components:v(b({},lu),{FormLabel:{baseStyle:{fontSize:"sm",marginBottom:1}},Avatar:{defaultProps:{colorScheme:"gray"}}})},ti({colorScheme:"gray",components:["Avatar"]}));ni.render(t(He.StrictMode,{children:t(Mi,{children:h(ai,{theme:Lo,children:[t(oi,{initialColorMode:Lo.config.initialColorMode}),t(Xd,{})]})})}),document.getElementById("root"));export{Lu as $,Tu as A,Du as B,Ku as C,_u as D,Gi as E,Xc as F,gu as G,ap as H,Pt as I,hu as J,zu as K,At as L,Uu as M,Gu as N,qu as O,Vu as P,Wu as Q,eu as R,Mu as S,yo as T,Yu as U,Ju as V,Xa as W,rp as X,sp as Y,ku as Z,ce as _,Co as a,op as a0,qo as a1,ep as a2,fd as a3,Xu as a4,vu as a5,$u as a6,xu as a7,Su as a8,Eu as a9,Cu as aa,Po as ab,Ad as ac,mu as ad,tp as ae,np as af,Mo as ag,Cd as ah,Hu as ai,pi as aj,Qu as ak,fu as al,Iu as b,me as c,wu as d,_t as e,Fi as f,yu as g,Ou as h,Au as i,bu as j,ae as k,Zu as l,Ne as m,Zc as n,Nu as o,ve as p,Pu as q,ip as r,Fu as s,ju as t,Oi as u,Wa as v,zi as w,Mt as x,Bu as y,Ru as z};
