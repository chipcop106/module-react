import{bZ as a,q as e}from"./vendor.js";const i=a(e)`
  table {
    display: table;
    margin-left: auto;
    margin-right: auto;
    width: 100%;
    border: 1px solid #666666;
    border-spacing: 5px;
    border-collapse: collapse;
    th,
    td {
      border: ${t=>`1px solid ${t.borderColor||"black"}`};
      min-width: 100px;
      padding: 2px;
    }
    p {
      margin: 0;
    }
  }
  .custom-image {
    max-width: ${t=>`${t.maxWidthImage||"100%"}`};
    height: ${t=>`${t.heightImage||"auto"}`};
  }
`;export{i as S};
