import{g as e,j as s,ah as n,m as u,H as c,eA as p,q as o}from"./vendor.js";const l=e`
  query ($type: EnumShiftsType) @api(name: "appZim") {
    getShiftRegisterSchedule(type: $type) {
      id
      start_time
      end_time
      name
    }
  }
`;e`
  query shifts($q: String, $status: String, $shift_minute: Int)
  @api(name: "appZim") {
    shifts(q: $q, status: $status, shift_minute: $shift_minute) {
      id
      start_time
      end_time
    }
  }
`;const m=e`
  query getScheduleDetail(
    $registerScheduleId: Int!
    $teacherId: CustomUserInputType
    $schoolId: Int
    $status: EnumStatusScheduleType
  ) @api(name: "appZim") {
    getScheduleDetail(
      registerScheduleId: $registerScheduleId
      teacherId: $teacherId
      schoolId: $schoolId
      status: $status
    ) {
      id
      type
      date
      schoolId
      startTime
      endTime
      students {
        id
        full_name
      }
      teacher {
        id
        full_name
      }
      eC {
        id
        full_name
      }
      linkMeeting
      description
    }
  }
`,h=e`
  mutation createRegisterSchedule($input: RegisterScheduleInputCreateType)
  @api(name: "appZim") {
    createRegisterSchedule(input: $input) {
      success
      error
      message
      dataString
    }
  }
`,S=e`
  mutation cancelRegisterSchedule($input: CancelRegisterInputType)
  @api(name: "appZim") {
    cancelRegisterSchedule(input: $input) {
      success
      error
      message
    }
  }
`,E=e`
  mutation createOrUpdateRegisterScheduleDetail(
    $input: CreateRegisterScheduleDetailInputType
  ) @api(name: "appZim") {
    createOrUpdateRegisterScheduleDetail(input: $input) {
      success
      error
      message
    }
  }
`,_=e`
  mutation approveOrRejectRequestCancel(
    $input: RejectRequestCancelScheduleInputType
  ) @api(name: "appZim") {
    approveOrRejectRequestCancel(input: $input) {
      success
      message
      dataString
    }
  }
`,$=e`
  mutation changeScheduleForTeacher($input: ChangeScheduleForTeacherInputType)
  @api(name: "appZim") {
    changeScheduleForTeacher(input: $input) {
      success
      error
      message
    }
  }
`,I=e`
  mutation cancelScheduleDetail($input: ScheduleDetailInputType)
  @api(name: "appZim") {
    cancelScheduleDetail(input: $input) {
      message
      success
      dataString
      error
    }
  }
`,T=e`
  mutation copyRegisterSchedule($input: CopyRegisterScheduleInputType)
  @api(name: "appZim") {
    copyRegisterSchedule(input: $input) {
      success
      message
    }
  }
`,y=e`
  mutation reopenScheduleDetail($input: ReopenScheduleDetailInputType)
  @api(name: "appZim") {
    reopenScheduleDetail(input: $input) {
      success
      error
    }
  }
`,C=e`
  mutation forceCancelRegisterSchedule($input: CancelRegisterInputType)
  @api(name: "appZim") {
    forceCancelRegisterSchedule(input: $input) {
      success
      message
    }
  }
`,r=e`
  fragment AllScheduleCopyType on ScheduleCopyType {
    type
    id
    configFunction {
      isAutoSchedule
    }
    date
    shift {
      id
      start_time
      end_time
    }
    teachers {
      id
      full_name
    }
    school {
      id
      name
    }
  }
`;e`
  query getTodoScheduleByEc($idEc: Int, $date: String) @api(name: "appZim") {
    getTodoScheduleByEc(idEc: $idEc, date: $date) {
      id
      type
      content
      customer {
        id
        role_id
        user_name
        full_name
        avatar
      }
      ec {
        id
        user_name
        full_name
        avatar
      }
      instructor {
        id
        role_id
        user_name
        full_name
        avatar
      }
      file_attached
      link
      start_time
      end_time
      date
      is_delete
      create_by
      create_date
      update_by
      update_date
    }
  }
`;const g=e`
  query getConfigFunctionByType($type: EnumConfigFunctionType!)
  @api(name: "appZim") {
    getConfigFunctionByType(type: $type) {
      id
      name
      type
      description
      timePerShift
      color
      isAutoSchedule
      shiftType
    }
  }
`,D=e`
  query getListRegisterSchedule(
    $fromDate: CustomDateInputType
    $toDate: CustomDateInputType
  ) @api(name: "appZim") {
    getListRegisterSchedule(fromDate: $fromDate, toDate: $toDate) {
      id
      type
      date
      shift {
        id
        start_time
        end_time
      }
      isFull
      requestsCancel {
        teacherId
        id
        reason
      }
      school {
        id
        name
      }
      configFunction {
        autoCancel
      }
    }
  }
`,R=e`
  query ($teacherId: CustomUserInputType!) @api(name: "appZim") {
    getFunctionAssignedTeacher(teacherId: $teacherId) {
      id
      name
      color
      isAutoSchedule
      shiftType
      needToCenter
    }
  }
`,f=e`
  query getListRegisterSchedule(
    $fromDate: CustomDateInputType
    $toDate: CustomDateInputType
    $type: Int
    $schoolId: Int
    $teacherId: CustomUserInputType
    $status: EnumStatusScheduleType
  ) @api(name: "appZim") {
    getListRegisterSchedule(
      fromDate: $fromDate
      toDate: $toDate
      type: $type
      schoolId: $schoolId
      teacherId: $teacherId
      status: $status
    ) {
      id
      type
      date
      shift {
        id
        start_time
        end_time
        name
      }
      school {
        id
        name
      }
      teachers {
        id
        full_name
      }
      requestsCancel {
        teacherId
        id
        reason
      }
      configFunction {
        isAutoSchedule
        timePerShift
        id
        color
        name
      }
      isFull
    }
  }
`,L=e`
  query checkListCopySchedule($input: CopyRegisterScheduleInputType)
  @api(name: "appZim") {
    checkListCopySchedule(input: $input) {
      scheduleConflict {
        ...AllScheduleCopyType
      }
      scheduleApprove {
        ...AllScheduleCopyType
      }
    }
  }
  ${r}
`,A=e`
  query getListScheduleByStatus(
    $fromDate: String
    $toDate: String
    $type: Int
    $teacherId: CustomUserInputType
    $schoolId: Int
    $status: EnumStatusScheduleType
  ) @api(name: "appZim") {
    getListScheduleByStatus(
      fromDate: $fromDate
      toDate: $toDate
      type: $type
      teacherId: $teacherId
      schoolId: $schoolId
      status: $status
    ) {
      id
      type
      date
      configFunction {
        id
        isAutoSchedule
      }
      teacher {
        id
        full_name
      }
      students {
        id
        full_name
      }
      startTime
      endTime
      description
      roomId
      linkMeeting
    }
  }
`;e`
  query getRequestCancelByStatus(
    $fromDate: String
    $toDate: String
    $type: Int
    $teacherId: CustomUserInputType
    $schoolId: Int
    $status: EnumStatusScheduleType
  ) @api(name: "appZim") {
    getRequestCancelByStatus(
      fromDate: $fromDate
      toDate: $toDate
      type: $type
      teacherId: $teacherId
      schoolId: $schoolId
      status: $status
    ) {
      id
      type
      date
    }
  }
`;const U=({typeRegister:t})=>(t==null?void 0:t.length)?s(n,{columns:{xl:4,base:1},maxW:"xl",rowGap:2,columnGap:{xl:6,base:0},children:t.map(a=>u(c,{spacing:2,children:[s(p,{size:3,bg:`${a.color}.500`}),s(o,{whiteSpace:"nowrap",children:a.name})]},a.id))}):null,q=3,H=4;var i;(function(t){t.register="register",t.function="function"})(i||(i={}));export{_ as A,h as C,i as E,C as F,l as G,H as O,y as R,U as S,S as a,m as b,L as c,g as d,R as e,D as f,T as g,$ as h,I as i,f as j,A as k,q as l,E as m};
