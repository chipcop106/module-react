import{g as t}from"./vendor.js";t`
  query schools($city_id: Int, $status: String) @api(name: "appZim") {
    schools(city_id: $city_id, status: $status) {
      id
      city {
        id
      }
      district {
        id
      }
      name
      address
      phone
      color_code
      lat
      lon
      status
    }
  }
`;const a=t`
  query cats @api(name: "appZim") {
    cats {
      id
      name
    }
  }
`,e=t`
  query subjects($name: String, $status: StatusEnum) @api(name: "appZim") {
    subjects(name: $name, status: $status) {
      id
      name
      status
      cats {
        id
        name
        status
        subject {
          id
          name
          status
        }
      }
    }
  }
`;t`
  query subcatsBySubjectId($subject_id: Int!) @api(name: "appZim") {
    subcatsBySubjectId(subject_id: $subject_id) {
      id
      name
      status
      cat_id
    }
  }
`;const i=t`
  query curriculums($name: String, $status: StatusEnum, $program_id: Int)
  @api(name: "appZim") {
    curriculums(name: $name, status: $status, program_id: $program_id) {
      id
      name
      status
      shift_minute
      total_lesson
    }
  }
`,n=t`
  query shifts($q: String, $status: String, $shift_minute: Int)
  @api(name: "appZim") {
    shifts(q: $q, status: $status, shift_minute: $shift_minute) {
      id
      status
      shift_minute
      start_time
      end_time
    }
  }
`,u=t`
  query sizes($q: String, $status: String) @api(name: "appZim") {
    sizes(q: $q, status: $status) {
      id
      status
      name
      size
    }
  }
`,c=t`
  query levels(
    $subject_id: Int!
    $graduation: Float
    $status: EnumLevelStatus
    $name: String
    $type: String
  ) @api(name: "appZim") {
    levels(
      subject_id: $subject_id
      graduation: $graduation
      status: $status
      name: $name
      type: $type
    ) {
      id
      name
      status
      graduation
    }
  }
`,d=t`
  query @api(name: "appZim") {
    getExerciseTypes {
      id
      name
    }
  }
`,m=t`
  query @api(name: "appZim") {
    getQuestionType {
      id
      name
    }
  }
`,$=t`
  query subcatsBySubjectId($subject_id: Int!) @api(name: "appZim") {
    subcatsBySubjectId(subject_id: $subject_id) {
      id
      name
    }
  }
`;t`
  query levels(
    $name: String
    $status: EnumLevelStatus
    $type: String
    $subject_id: Int
    $graduation: Float
  ) @api(name: "appZim") {
    levels(
      name: $name
      status: $status
      type: $type
      subject_id: $subject_id
      graduation: $graduation
    ) {
      id
      name
    }
  }
`;const o=t`
  query getLessonByConfig(
    $subjectId: Int!
    $catId: Int!
    $subcatId: Int!
    $levelIn: Int!
    $levelOut: Int!
  ) @api(name: "appZim") {
    getLessonByConfig(
      subjectId: $subjectId
      catId: $catId
      subcatId: $subcatId
      levelIn: $levelIn
      levelOut: $levelOut
    ) {
      id
      title
    }
  }
`,r=t`
  query subcats($cat_id: Int, $status: String) @api(name: "appZim") {
    subcats(cat_id: $cat_id, status: $status) {
      id
      name
    }
  }
`,p=t`
  query getListSchoolAssignForTeacher(
    $teacherId: CustomUserInputType
    $functionId: Int
  ) @api(name: "appZim") {
    getListSchoolAssignForTeacher(
      teacherId: $teacherId
      functionId: $functionId
    ) {
      id
      name
    }
  }
`,_=t`
  query getAccountsByRole(
    $userId: CustomUserInputType
    $schoolId: Int
    $roleId: Int!
  ) @api(name: "appZim") {
    getAccountsByRole(userId: $userId, schoolId: $schoolId, roleId: $roleId) {
      id
      full_name
    }
  }
`;export{i as G,n as a,u as b,$ as c,e as d,c as e,p as f,_ as g,d as h,a as i,o as j,r as k,m as l};
