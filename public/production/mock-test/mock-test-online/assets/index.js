var at=Object.defineProperty,ot=Object.defineProperties;var lt=Object.getOwnPropertyDescriptors;var Re=Object.getOwnPropertySymbols;var it=Object.prototype.hasOwnProperty,nt=Object.prototype.propertyIsEnumerable;var st=(e,r,n)=>r in e?at(e,r,{enumerable:!0,configurable:!0,writable:!0,value:n}):e[r]=n,N=(e,r)=>{for(var n in r||(r={}))it.call(r,n)&&st(e,n,r[n]);if(Re)for(var n of Re(r))nt.call(r,n)&&st(e,n,r[n]);return e},H=(e,r)=>ot(e,lt(r));var Te=(e,r)=>{var n={};for(var a in e)it.call(e,a)&&r.indexOf(a)<0&&(n[a]=e[a]);if(e!=null&&Re)for(var a of Re(e))r.indexOf(a)<0&&nt.call(e,a)&&(n[a]=e[a]);return n};import{g as gql,o as onError,A as ApolloLink,O as Observable,I as InMemoryCache,r as relayStylePagination,M as MultiAPILink,c as createUploadLink,b as buildAxiosFetch_1,a as axios,W as WebSocketLink,T as TokenRefreshLink,d as o,s as split$1,e as getMainDefinition,f as ApolloClient,S as SubscriptionClient_1,h as react,j as jsx,i as ApolloProvider,L as Link$1,k as Link$2,u as useColorModeValue,F as Fragment,l as Accordion,m as jsxs,n as AccordionItem,p as AccordionButton,H as HStack,q as Text,t as AccordionIcon,v as AccordionPanel,B as Box,w as Flex,x as Tabs,y as TabList,V as VStack,z as Tab,C as TabPanels,D as TabPanel,E as ImNewspaper,G as SiGoogleclassroom,J as IoLibraryOutline,K as HiUserGroup,N as FaUserFriends,P as BiMoney,Q as MdOutlineMiscellaneousServices,R as IoSettingsOutline,U as FormControl,X as FormLabel,Y as Input$1,Z as FormErrorMessage,_ as InputGroup,$ as InputRightElement,a0 as Button$1,a1 as VscSearch,a2 as useQuery,a3 as useSubscription,a4 as SearchIcon,a5 as Avatar,a6 as t,a7 as AvatarBadge,a8 as FcKey,a9 as SkeletonCircle,aa as SkeletonText,ab as uniqBy_1,ac as pickBy_1,ad as FiGrid,ae as RiChat3Line,af as dayjs,ag as commonjsGlobal$1,ah as SimpleGrid,ai as lodash,aj as Stack,ak as BsCheck2,al as PropTypes,am as useCheckbox,an as useRadio,ao as CheckCircleIcon,ap as Wrap,aq as Tag,ar as TagLabel,as as TagCloseButton,at as Spinner,au as useApolloClient,av as useToast,aw as useMutation,ax as useCheckboxGroup,ay as useForm,az as useDeepCompareEffect$2,aA as FormProvider,aB as Modal$1,aC as ModalOverlay,aD as ModalContent,aE as ModalHeader,aF as ModalCloseButton,aG as ModalBody,aH as ModalFooter,aI as Icon,aJ as AiOutlineSend,aK as useDisclosure,aL as useSearchParams,aM as MdOutlineGroupAdd,aN as useLazyQuery,aO as Highlighter,aP as emojiIndex,aQ as v4$1,aR as moment,aS as Image$1,aT as AiOutlineFileZip,aU as IoCaretForwardSharp,aV as Menu,aW as MenuButton,aX as FiMoreHorizontal,aY as Portal,aZ as MenuList,a_ as MenuItem,a$ as AiOutlineDelete,b0 as BiCheck,b1 as ImgsViewer,b2 as BsQuestionCircle,b3 as BsEmojiSmile,b4 as Tooltip,b5 as TableCell,b6 as Table$1,b7 as Blockquote,b8 as Link$3,b9 as Image$2,ba as IconButton,bb as Popover,bc as PopoverTrigger,bd as BiHeading,be as PopoverContent,bf as PopoverBody,bg as GrTableAdd,bh as AiOutlineClear,bi as RiInsertColumnRight,bj as RiInsertColumnLeft,bk as RiDeleteColumn,bl as RiInsertRowTop,bm as RiInsertRowBottom,bn as RiDeleteRow,bo as RiMergeCellsHorizontal,bp as RiSplitCellsHorizontal,bq as AiOutlineInsertRowLeft,br as AiOutlineInsertRowAbove,bs as RiCellphoneFill,bt as BiTable,bu as queryString,bv as useParams,bw as useLocation,bx as useNavigate,by as useTheme,bz as Select$2,bA as useToggle$1,bB as sortBy_1,bC as useRadioGroup,bD as AiOutlineCloudUpload,bE as Switch,bF as SearchIcon$1,bG as AiOutlineFile,bH as urlJoin,bI as BiImages,bJ as create$1,bK as MdSpaceBar,bL as BiBold,bM as BiItalic,bN as AiOutlineUnderline,bO as BiStrikethrough,bP as BiAlignLeft,bQ as BiAlignMiddle,bR as BiAlignRight,bS as BiListUl,bT as BiListOl,bU as BiEdit,bV as Node,bW as mergeAttributes,bX as MdOutlineQuickreply,bY as HiOutlineEmojiSad,bZ as newStyled,b_ as FiSend,b$ as Badge$1,c0 as AiOutlineMore,c1 as AiOutlineEdit,c2 as AiOutlineHeart,c3 as MdOutlineReport,c4 as AiOutlineDislike,c5 as AiOutlineLike,c6 as AiOutlineLock,c7 as AiOutlineArrowDown,c8 as MdReply,c9 as useStyleConfig,ca as DeleteIcon,cb as MdSubdirectoryArrowRight,cc as PlusSquareIcon,cd as useFormContext,ce as Controller,cf as Checkbox,cg as reactTable,ch as p$1,ci as Table$2,cj as Thead,ck as Tr,cl as Th,cm as chakra,cn as TriangleDownIcon,co as TriangleUpIcon,cp as Tbody,cq as Td,cr as CopyIcon,cs as EditIcon,ct as BiTrashAlt,cu as Heading,cv as AlertDialog,cw as AlertDialogContent,cx as cheerio,cy as useEditor,cz as StarterKit,cA as TableRow,cB as TableHeader,cC as Typography,cD as Underline,cE as Placeholder,cF as TextAlign,cG as BubbleMenu,cH as EditorContent,cI as _default$2,cJ as Collapse,cK as __awaiter,cL as __generator,cM as __spread,cN as react$1,cO as require$$0,cP as loadable$2,cQ as BsImages,cR as Textarea,cS as MdSend,cT as useBreakpointValue,cU as MdDoneOutline,cV as FiSearch,cW as mt,cX as XCircleIcon,cY as Progress,cZ as Spacer,c_ as useList,c$ as addMethod,d0 as yup,d1 as create,d2 as create$2,d3 as o$1,d4 as useDebounce$2,d5 as RadioGroup,d6 as Radio,d7 as ButtonGroup,d8 as CgCloseR,d9 as PopoverArrow,da as PopoverHeader,db as IoMdAddCircleOutline,dc as PerfectScrollbar,dd as AiOutlineFileText,de as FaRegFileWord,df as RiFileExcel2Line,dg as MdGroupAdd,dh as WrapItem,di as TiUserAdd,dj as GoLocation,dk as BsTelephone,dl as FiFacebook,dm as FiMail,dn as BsTags,dp as useColorMode,dq as Container,dr as HamburgerIcon,ds as RiMoonClearLine,dt as SunIcon,du as ChatIcon,dv as Center,dw as MenuDivider,dx as AiOutlineUser,dy as AiOutlineLogout,dz as Breadcrumb,dA as BreadcrumbItem,dB as BreadcrumbLink,dC as matchPath,dD as Navigate,dE as Alert,dF as Code,dG as AiFillEdit,dH as ViewIcon,dI as Wt,dJ as MdClear,dK as customParseFormat,dL as ReactPaginate,dM as ChevronRightIcon,dN as ChevronLeftIcon,dO as MdFilterListAlt,dP as ErrorBoundary,dQ as BrowserRouter,dR as Routes,dS as Route,dT as extendTheme,dU as withDefaultColorScheme,dV as ReactDOM,dW as ChakraProvider,dX as ColorModeScript}from"./vendor.js";const p=function(){const r=document.createElement("link").relList;if(r&&r.supports&&r.supports("modulepreload"))return;for(const l of document.querySelectorAll('link[rel="modulepreload"]'))a(l);new MutationObserver(l=>{for(const c of l)if(c.type==="childList")for(const u of c.addedNodes)u.tagName==="LINK"&&u.rel==="modulepreload"&&a(u)}).observe(document,{childList:!0,subtree:!0});function n(l){const c={};return l.integrity&&(c.integrity=l.integrity),l.referrerpolicy&&(c.referrerPolicy=l.referrerpolicy),l.crossorigin==="use-credentials"?c.credentials="include":l.crossorigin==="anonymous"?c.credentials="omit":c.credentials="same-origin",c}function a(l){if(l.ep)return;l.ep=!0;const c=n(l);fetch(l.href,c)}};p();var index$5="";const scriptRel="modulepreload",seen={},base$1="/",__vitePreload=function(r,n){return!n||n.length===0?r():Promise.all(n.map(a=>{if(a=`${base$1}${a}`,a in seen)return;seen[a]=!0;const l=a.endsWith(".css"),c=l?'[rel="stylesheet"]':"";if(document.querySelector(`link[href="${a}"]${c}`))return;const u=document.createElement("link");if(u.rel=l?"stylesheet":scriptRel,l||(u.as="script",u.crossOrigin=""),u.href=a,document.head.appendChild(u),l)return new Promise((d,m)=>{u.addEventListener("load",d),u.addEventListener("error",m)})})).then(()=>r())};var global$1="";const PostFragment=gql`
  fragment Post on Post {
    id
    title
    slug
    excerpt
    content
    status
    numOfComments
    categories {
      id
      title
      slug
    }
    tags {
      id
      name
      slug
    }
    postGroups {
      id
      title
      slug
    }
    publishedAt
    featureMedias {
      id
      type
      path
      variants {
        id
        type
        width
        height
        path
      }
    }
    user {
      id
      role
      username
      fullName
      phone
      gender
      avatar
    }
    censor {
      id
      fullName
    }
    createdAt
    updatedAt
    numOfContextualLink
  }
`;gql`
  query posts(
    $after: String
    $before: String
    $first: Int
    $last: Int
    $categorySlugs: [String]
    $relatedPost: ObjectID
  ) @api(name: "zim") {
    posts(
      after: $after
      before: $before
      first: $first
      last: $last
      categorySlugs: $categorySlugs
      relatedPost: $relatedPost
    ) {
      totalCount
      edges {
        cursor
        node {
          ...Post
        }
      }
      pageInfo {
        startCursor
        endCursor
        hasNextPage
        hasPreviousPage
      }
    }
  }
  ${PostFragment}
`;gql`
  query ($idOrSlug: String!) @api(name: "zim") {
    post(idOrSlug: $idOrSlug) {
      ...Post
      seo {
        title
        description
        ogDescription
        ogImage {
          id
          path
        }
        ogTitle
        publisher
        noIndex
        noFollow
        canonicalUrl
        customHeadHtml
      }
      rejectReason
    }
  }
  ${PostFragment}
`;gql`
  query postPagination(
    $searchString: String
    $page: Int
    $limit: Int
    $categoryIds: [ObjectID]
    $tagIds: [ObjectID]
    $postGroupIds: [ObjectID]
    $userIds: [Int]
    $statuses: [PostStatus]
    $censorUserIds: [Int]
    $orderBy: PostOrderBy
    $order: OrderDirection
  ) @api(name: "zim") {
    postPagination(
      searchString: $searchString
      page: $page
      limit: $limit
      categoryIds: $categoryIds
      tagIds: $tagIds
      postGroupIds: $postGroupIds
      userIds: $userIds
      statuses: $statuses
      censorUserIds: $censorUserIds
      orderBy: $orderBy
      order: $order
    ) {
      hasNextPage
      hasPrevPage
      limit
      nextPage
      prevPage
      totalDocs
      totalPages
      docs {
        ...Post
      }
    }
  }
  ${PostFragment}
`;gql`
  mutation updatePost($postId: ObjectID!, $input: UpdatePostData!)
  @api(name: "zim") {
    updatePost(input: $input, postId: $postId) {
      ...Post
    }
  }
  ${PostFragment}
`;gql`
  mutation createPost($input: CreatePostData!) @api(name: "zim") {
    createPost(input: $input) {
      ...Post
    }
  }
  ${PostFragment}
`;gql`
  mutation createTag($name: String!, $description: String) @api(name: "zim") {
    createTag(description: $description, name: $name) {
      id
      name
      slug
      description
    }
  }
`;gql`
  mutation publishPost($postId: ObjectID!) @api(name: "zim") {
    publishPost(postId: $postId) {
      id
      title
      status
    }
  }
`;gql`
  mutation unpublishPost($postId: ObjectID!) @api(name: "zim") {
    unpublishPost(postId: $postId) {
      id
      title
      status
    }
  }
`;gql`
  mutation requestPostForApproval($postId: ObjectID!) @api(name: "zim") {
    requestPostForApproval(postId: $postId) {
      id
      title
      status
    }
  }
`;gql`
  mutation approveOrRejectPost(
    $postId: ObjectID!
    $status: ApproveOrRejectPostStatus
    $rejectReason: String
    $updatedContent: String
  ) @api(name: "zim") {
    approveOrRejectPost(
      postId: $postId
      status: $status
      rejectReason: $rejectReason
      updatedContent: $updatedContent
    ) {
      id
      title
      status
      rejectReason
      content
    }
  }
`;gql`
  mutation reassignPost($postId: ObjectID!, $userId: Int!) @api(name: "zim") {
    reassignPost(postId: $postId, userId: $userId) {
      id
      user {
        id
        birthday
        email
        fullName
        avatar
        roleId
        role
      }
    }
  }
`;const SIGN_IN=gql`
  mutation signInV2($username: String!, $password: String!) @api(name: "zim") {
    signInV2(username: $username, password: $password) {
      token
      expired_at
      refreshToken
    }
  }
`,GET_USER=gql`
  query @api(name: "zim") {
    me {
      id
      role
      roleId
      username
      fullName
      phone
      birthday
      gender
      avatar
      email
      canCensor
    }
  }
`,SIGN_OUT=gql`
  mutation signOut @api(name: "zim") {
    signOut
  }
`;gql`
  mutation createPostCategory($input: PostCategoryInput!) @api(name: "zim") {
    createPostCategory(input: $input) {
      id
      title
      slug
      description
    }
  }
`;const POST_CATEGORY_FRAGMENT=gql`
  fragment CategoryFields on PostCategory {
    id
    title
    slug
    parentCategory {
      id
      title
      slug
    }
    description
  }
`;gql`
  query postCategoriesTree @api(name: "zim") {
    postCategoriesTree {
      ...CategoryFields
      children {
        ...CategoryFields
        children {
          ...CategoryFields
          children {
            ...CategoryFields
          }
        }
      }
    }
  }
  ${POST_CATEGORY_FRAGMENT}
`;gql`
  query postCategories($flatten: Boolean, $search: String, $parent: ObjectID)
  @api(name: "zim") {
    postCategories(search: $search, parent: $parent, flatten: $flatten) {
      ...CategoryFields
      children {
        ...CategoryFields
      }
    }
  }
  ${POST_CATEGORY_FRAGMENT}
`;gql`
  mutation updatePostCategory($id: ObjectID!, $input: UpdatePostCategoryInput!)
  @api(name: "zim") {
    updatePostCategory(id: $id, input: $input) {
      id
      title
      slug
      description
    }
  }
`;gql`
  mutation deletePostCategory($id: ObjectID!) @api(name: "zim") {
    deletePostCategory(id: $id)
  }
`;gql`
  query comments(
    $type: CommentType!
    $after: String
    $before: String
    $first: Int
    $last: Int
    $refId: String!
    $parent: ObjectID
  ) @api(name: "zim") {
    comments(
      type: $type
      after: $after
      before: $before
      first: $first
      last: $last
      refId: $refId
      parent: $parent
    ) {
      totalCount
      edges {
        cursor
        node {
          ... on PostComment {
            id
            user {
              id
              role
              username
              fullName
              phone
              birthday
              gender
              avatar
            }
            content
            parent {
              id
              content
              user {
                id
                role
                username
                fullName
                phone
                birthday
                gender
                avatar
              }
            }
            liked
            numOfLikes
            createdAt
            updatedAt
          }
          __typename
        }
      }
      pageInfo {
        startCursor
        endCursor
        hasNextPage
        hasPreviousPage
      }
    }
  }
`;gql`
  mutation postComment(
    $content: String!
    $type: CommentType
    $ref: ObjectID!
    $parent: ObjectID
  ) @api(name: "zim") {
    postComment(content: $content, type: $type, ref: $ref, parent: $parent) {
      ... on PostComment {
        id
        content
        user {
          id
          role
          username
          fullName
          phone
          birthday
          gender
          avatar
        }
        parent {
          id
          content
          user {
            id
            role
            username
            fullName
            phone
            birthday
            gender
            avatar
          }
          liked
          numOfLikes
          createdAt
          updatedAt
        }
        liked
        numOfLikes
        createdAt
        updatedAt
      }
      __typename
    }
  }
`;gql`
  mutation likeOrUnlikeComment($commentId: ObjectID!, $liked: Boolean!)
  @api(name: "zim") {
    likeOrUnlikeComment(commentId: $commentId, liked: $liked) {
      ... on PostComment {
        id
        liked
        numOfLikes
      }
    }
  }
`;const CourseFragment=gql`
  fragment Course on CoursePlanType {
    id
    slug
    start_date
    program {
      id
      name
      cat {
        id
        name
      }
      subject {
        id
        name
      }
      program_type {
        id
        name
      }
      level {
        subject_id
        id
        name
        graduation
        status
      }
      fee
      status
      sub_cats {
        id
        cat_id
        name
        status
        parent_id
      }
    }
    subject {
      id
      name
      status
    }
    level_out {
      subject_id
      id
      name
      graduation
    }
    type
    size {
      id
      name
      size
      status
    }
    school {
      id
      city {
        id
      }
      district {
        id
      }
      name
      address
      phone
      color_code
      lat
      lon
      status
    }
    curriculum {
      id
      name
      program {
        id
      }
      shift_minute
      total_lesson
      status
    }
    shifts {
      id
      start_time
      end_time
      status
      shift_minute
    }
    day_of_weeks {
      id
      name
    }
    config_fee {
      detail_name
      detail_id
      type
      fee
    }
    fee
    status
    ec_id
    teacher_lead_id
    description
    featured_image
  }
`,GET_COURSES=gql`
  query getListCoursePagination(
    $order_by: String
    $order: String
    $q: String
    $school_id: Int
    $city_id: Int
    $program_id: Int
    $program_type_id: Int
    $limit: Int
    $cat_id: Int
    $level_id: Int
    $subject_id: Int
    $status: [CourseStatusEnum]
    $type: EnumCourseTypeEnum
    $fromdate: String
    $todate: String
    $page: Int
    $bussiness_partner_id: Int
  ) @api(name: "appZim") {
    getListCoursePagination(
      q: $q
      status: $status
      subject_id: $subject_id
      level_id: $level_id
      cat_id: $cat_id
      school_id: $school_id
      city_id: $city_id
      program_id: $program_id
      program_type_id: $program_type_id
      type: $type
      fromdate: $fromdate
      todate: $todate
      order_by: $order_by
      order: $order
      page: $page
      limit: $limit
      bussiness_partner_id: $bussiness_partner_id
    ) {
      hasNextPage
      hasPrevPage
      limit
      page
      pageNext
      pagePrev
      pagingCounter
      totalPages
      totalDocs
      docs {
        id
        slug
        name
        start_date
        subcats {
          id
          cat_id
          name
          status
          parent_id
        }
        subject {
          id
          name
          status
        }
        school {
          id
          name
        }

        shifts {
          id
          start_time
          end_time
        }
        level_out {
          subject_id
          id
          name
          graduation
        }
        day_of_weeks {
          id
          name
        }
        size {
          id
          name
          size
          status
        }
        fee
        status
        total_register
        total_paid
        config_fee {
          type
          fee
          detail_id
          detail_name
        }
        end_date
        type
        ec {
          id
          full_name
        }
        teachers {
          id
          user_name
          full_name
        }
      }
    }
  }
`,GET_COURSE=gql`
  query course($idOrSlug: String) @api(name: "appZim") {
    course(idOrSlug: $idOrSlug) {
      id
      slug
      name
      start_date
      subcats {
        id
        cat_id
        name
        status
        parent_id
      }
      subject {
        id
        name
        status
      }
      school {
        id
        name
      }
      coefficient_range {
        id
        name
      }
      shifts {
        id
        start_time
        end_time
      }
      level_in {
        subject_id
        id
        name
        graduation
      }
      level_out {
        subject_id
        id
        name
        graduation
      }
      day_of_weeks {
        id
        name
      }
      size {
        id
        name
        size
        status
      }

      fee
      status
      curriculum {
        id
        name
      }
      rooms {
        id
        name
      }
      total_register
      total_paid
      config_fee {
        type
        fee
        detail_id
        detail_name
      }
      end_date
      type
      ec {
        id
        full_name
      }
      teachers {
        id
        user_name
        full_name
      }
    }
  }
  ${CourseFragment}
`,GET_COURSE_PLAN=gql`
  query coursePlan($idOrSlug: String) @api(name: "appZim") {
    coursePlan(idOrSlug: $idOrSlug) {
      id
      slug
      name
      start_date
      subcats {
        id
        cat_id
        name
        status
        parent_id
      }
      subject {
        id
        name
        status
      }
      school {
        id
        name
      }
      coefficient_range {
        id
        name
      }
      shifts {
        id
        start_time
        end_time
      }
      level_in {
        subject_id
        id
        name
        graduation
      }
      level_out {
        subject_id
        id
        name
        graduation
      }
      day_of_weeks {
        id
        name
      }
      size {
        id
        name
        size
        status
      }

      fee
      status
      curriculum {
        id
        name
      }
      rooms {
        id
        name
      }
      config_fee {
        type
        fee
        detail_id
        detail_name
      }
      type
      ec {
        id
        full_name
      }
      teachers {
        id
        user_name
        full_name
      }
    }
  }
  ${CourseFragment}
`,GET_COURSE_FEE_SUGGESTION=gql`
  query getCourseFeeSuggestion($input: CourseFeeSuggestionInput)
  @api(name: "appZim") {
    getCourseFeeSuggestion(input: $input)
  }
`;gql`
  query coursesPlan($offset: Int, $limit: Int, $school_id: Int, $city_id: Int)
  @api(name: "appZim") {
    coursesPlan(
      school_id: $school_id
      city_id: $city_id
      offset: $offset
      limit: $limit
    ) {
      totalCount
      edges {
        node {
          ...Course
        }
      }
      pageInfo {
        hasNextPage
        hasPreviousPage
      }
    }
  }
  ${CourseFragment}
`;gql`
  query coursesPlan($limit: Int) @api(name: "appZim") {
    coursesPlan(status: incoming, limit: $limit) {
      edges {
        node {
          id
          slug
        }
      }
    }
  }
`;gql`
  query coursePlan($idOrSlug: String!) @api(name: "appZim") {
    coursePlan(idOrSlug: $idOrSlug) {
      ...Course
    }
  }
  ${CourseFragment}
`;gql`
  mutation createAdvisory(
    $name: String
    $phone: String!
    $email: String!
    $city_id: Int!
    $gclid: String
    $fbclid: String
    $link: String
    $note: String
  ) @api(name: "appZim") {
    createAdvisory(
      name: $name
      phone: $phone
      email: $email
      city_id: $city_id
      gclid: $gclid
      fbclid: $fbclid
      link: $link
      note: $note
    ) {
      success
      error
      message
    }
  }
`;gql`
  mutation checkout($input: CheckoutInput!) @api(name: "zim") {
    checkout(input: $input) {
      paymentUrl
      order {
        id
        status
        paymentMethod
        paymentStatus
        shippingMethod
        orderId
        email
        shippingAddress {
          recipientName
          recipientPhone
          email
          street1
          street2
          city
          state
          zip
          country
        }
        total
      }
    }
  }
`;const OrderBaseFieldsFragment=gql`
  fragment OrderBaseFields on Order {
    id
    status
    paymentMethod
    paymentStatus
    shippingMethod
    orderId
    status
    email
    shippingAddress {
      recipientName
      recipientPhone
      email
      street1
      street2
      city
      state
      zip
      country
    }
    total
    createdAt
    updatedAt
  }
`,OrderDetailsFields=gql`
  fragment OrderDetailFields on Order {
    ...OrderBaseFields
    user {
      id
      fullName
    }
    billingAddress {
      recipientName
      recipientPhone
      email
      street1
      street2
      city
      state
      zip
      country
    }
    attachments {
      id
      filename
      filepath
    }
    total
    totalPreDiscount
    discount
    coupons
    items {
      quantity
      total
      preTaxTotal
      price
      salePrice
      tax
      type
      item {
        ... on SimpleProduct {
          id
          name
          slug
          images {
            id
            path
          }
          sku
          attributes {
            isVariantAttribute
            attribute {
              id
              name
            }
            value
          }
        }
        ... on Post {
          id
          excerpt
          featureMedias {
            id
            path
          }
          title
          slug
        }
        ... on MockTestItem {
          data
        }
      }
    }
  }
  ${OrderBaseFieldsFragment}
`;gql`
  query order($idOrCode: String!) @api(name: "zim") {
    order(idOrCode: $idOrCode) {
      ...OrderBaseFields
      ...OrderDetailFields
    }
  }
  ${OrderDetailsFields}
`;gql`
  query orderPagination(
    $searchString: String
    $statuses: [OrderStatus]
    $userIds: [Int]
    $orderBy: OrderOrderBy
    $order: OrderDirection
    $page: Int
    $limit: Int
    $itemType: [CheckoutProduct]
  ) @api(name: "zim") {
    orderPagination(
      searchString: $searchString
      statuses: $statuses
      userIds: $userIds
      order: $order
      orderBy: $orderBy
      page: $page
      limit: $limit
      itemType: $itemType
    ) {
      totalPages
      totalDocs
      docs {
        ...OrderBaseFields
      }
    }
  }
  ${OrderBaseFieldsFragment}
`;gql`
  query tags(
    $after: String
    $before: String
    $first: Int
    $last: Int
    $search: String
  ) @api(name: "zim") {
    tags(
      after: $after
      before: $before
      first: $first
      last: $last
      search: $search
    ) {
      edges {
        node {
          id
          name
          description
          slug
        }
      }
      pageInfo {
        hasNextPage
        hasPreviousPage
        startCursor
        endCursor
      }
    }
  }
`;gql`
  query postCategories($search: String, $parent: ObjectID) @api(name: "zim") {
    postCategories(search: $search, parent: $parent) {
      id
      title
      slug
      parentCategory {
        id
        title
        slug
      }
      children {
        id
        title
        slug
        parentCategory {
          id
          title
          slug
        }
      }
    }
  }
`;const ProductBaseFieldsFragment=gql`
  fragment ProductBaseField on BaseProduct {
    ... on SimpleProduct {
      id
      sku
      name
      slug
      regularPrice
      salePrice
      salePriceFrom
      salePriceTo
      description
      shortDescription
      numOfContextualLink
      attributes {
        id
        attribute {
          id
          name
          type
          options
        }
        value
        isVariantAttribute
      }
      parentProduct {
        id
      }
      price
      categories {
        id
        title
        slug
      }
      seo {
        title
        description
        ogDescription
        ogImage {
          id
          path
        }
        ogTitle
        publisher
        noIndex
        noFollow
        canonicalUrl
        customHeadHtml
      }
      images {
        id
        path
      }
      status
      productType
      tags {
        id
        name
        description
        slug
      }
      downloadable
      attributes {
        attribute {
          id
          type
          name
          options
        }
        value
      }
      inStock
      inventories {
        id
        inStock
        manageStock
        reserved
        storeId
        inventory
        store {
          id
          name
        }
      }
      createdAt
      updatedAt
    }
    ... on VariableProduct {
      id
      sku
      name
      slug
      categories {
        id
        title
        slug
      }
      images {
        id
        filename
        path
      }
      description
      shortDescription
      status
      minPrice
      maxPrice
      minRegularPrice
      maxRegularPrice
      productType
      inStock
      variationAttributes {
        id
        name
        type
        options
      }
      createdAt
      updatedAt
      seo {
        title
        description
        ogDescription
        ogImage {
          id
          path
        }
        ogTitle
        publisher
        noIndex
        noFollow
        canonicalUrl
        customHeadHtml
      }
    }
  }
`,ProductDetailsFields=gql`
  fragment ProductDetailFields on BaseProduct {
    ...ProductBaseField
    ... on SimpleProduct {
      tags {
        id
        name
      }
      downloadable
      downloads {
        id
        filename
        path
      }

      attributes {
        id
        attribute {
          id
          name
          type
          options
        }
        value
        isVariantAttribute
        media {
          id
          type
          filename
          path
        }
      }
      parentProduct {
        id
        slug
        name
        status
        variationAttributes {
          id
          name
          type
          options
        }
      }
    }
    ... on VariableProduct {
      childCount
      childPublishedCount
      attributes {
        id
        attribute {
          id
          name
          type
          options
        }
        value
        media {
          id
          type
          filename
          path
        }
      }
      tags {
        id
        name
      }
    }
  }
  ${ProductBaseFieldsFragment}
`;gql`
  query product($id: ObjectID!) @api(name: "zim") {
    productAdmin(id: $id) {
      ...ProductBaseField
      ...ProductDetailFields
    }
  }
  ${ProductDetailsFields}
`;gql`
  query productPagination(
    $searchString: String
    $statuses: [ProductStatus]
    $orderBy: ProductOrderBy
    $order: OrderDirection
    $page: Int
    $limit: Int
    $parentProduct: ObjectID
    $tagSlugs: [String]
    $flatten: Boolean
    $simpleOnly: Boolean
    $tagIds: [ObjectID]
  ) @api(name: "zim") {
    productPagination(
      searchString: $searchString
      statuses: $statuses
      parentProduct: $parentProduct
      order: $order
      orderBy: $orderBy
      page: $page
      limit: $limit
      tagSlugs: $tagSlugs
      flatten: $flatten
      simpleOnly: $simpleOnly
      tagIds: $tagIds
    ) {
      totalPages
      totalDocs
      docs {
        ...ProductBaseField
      }
    }
  }
  ${ProductBaseFieldsFragment}
`;gql`
  query publishedProducts(
    $search: String
    $after: String
    $before: String
    $first: Int
    $last: Int
  ) @api(name: "zim") {
    publishedProducts(
      after: $after
      before: $before
      first: $first
      last: $last
      search: $search
    ) {
      totalCount
      pageInfo {
        hasNextPage
        hasPreviousPage
        startCursor
        endCursor
      }
      edges {
        cursor
        node {
          ...ProductBaseField
        }
      }
    }
  }
  ${ProductBaseFieldsFragment}
`;gql`
  query productVariants($parentProductId: ObjectID!) @api(name: "zim") {
    productVariants(parentProductId: $parentProductId) {
      ...ProductBaseField
    }
  }
  ${ProductBaseFieldsFragment}
`;gql`
  query (
    $searchString: String
    $excludeIds: [ObjectID]
    $page: Int
    $limit: Int
  ) @api(name: "zim") {
    productAttributePagination(
      page: $page
      limit: $limit
      excludeIds: $excludeIds
      searchString: $searchString
    ) {
      totalDocs
      docs {
        id
        name
        options
        type
      }
    }
  }
`;gql`
  query ($id: ObjectID!) @api(name: "zim") {
    productAttribute(id: $id) {
      id
      name
      options
      type
    }
  }
`;gql`
  query @api(name: "zim") {
    productCategoriesTree {
      id
      title
      slug
      parentCategory {
        id
        title
        slug
      }
      children {
        id
        title
        slug
        parentCategory {
          id
          title
          slug
        }
      }
    }
  }
`;gql`
  query productCategories($search: String, $parent: ObjectID)
  @api(name: "zim") {
    productCategories(search: $search, parent: $parent) {
      id
      title
      slug
      parentCategory {
        id
        title
        slug
      }
      children {
        id
        title
        slug
        parentCategory {
          id
          title
          slug
        }
      }
    }
  }
`;gql`
  query slugExist($slug: String!, $type: SlugType, $refId: ObjectID)
  @api(name: "zim") {
    slugExists(type: $type, slug: $slug, refId: $refId)
  }
`;gql`
  query titleExists($title: String!, $type: SlugType, $refId: ObjectID)
  @api(name: "zim") {
    titleExists(type: $type, title: $title, refId: $refId)
  }
`;gql`
  query postViewReport(
    $startTime: DateTime
    $endTime: DateTime
    $page: Int
    $limit: Int
    $userId: Int
  ) @api(name: "zim") {
    postViewReport(
      startTime: $startTime
      endTime: $endTime
      page: $page
      limit: $limit
      userId: $userId
    ) {
      totalDocs
      docs {
        post {
          id
          title
          slug
          user {
            id
            fullName
          }
        }
        viewCount
      }
    }
  }
`;gql`
  query generalPostViewByDate(
    $range: FromToDateInput
    $dateViewMode: DateViewMode
  ) @api(name: "zim") {
    generalPostViewByDate(range: $range, dateViewMode: $dateViewMode) {
      label
      viewCount
      postCount
    }
  }
`;gql`
  query postViewReportByUser(
    $startTime: DateTime
    $endTime: DateTime
    $page: Int
    $limit: Int
  ) @api(name: "zim") {
    postViewReportByUser(
      startTime: $startTime
      endTime: $endTime
      page: $page
      limit: $limit
    ) {
      totalDocs
      docs {
        user {
          id
          fullName
        }
        postCount
        viewCount
      }
    }
  }
`;gql`
  query postReportBySpecificUser(
    $userId: Int!
    $startTime: DateTime
    $endTime: DateTime
  ) @api(name: "zim") {
    postReportBySpecificUser(
      startTime: $startTime
      endTime: $endTime
      userId: $userId
    ) {
      totalLifetimePosts
      totalLifetimePublishedPosts
      totalLifetimeViews
      totalPosts
      totalPublishedPosts
    }
  }
`;gql`
  query postReportByStatus($userId: Int) @api(name: "zim") {
    postReportByStatus(userId: $userId) {
      status
      count
    }
  }
`;gql`
  query postByUser(
    $page: Int
    $limit: Int
    $order: OrderDirection
    $orderBy: PostByUserSort
    $userFilter: UserWithPostUserFilter
  ) @api(name: "zim") {
    postByUser(
      page: $page
      limit: $limit
      order: $order
      orderBy: $orderBy
      userFilter: $userFilter
    ) {
      limit
      page
      totalPages
      totalDocs
      docs {
        user {
          id
          email
          fullName
          avatar
          role
        }
        reviewsAverage
        postCount
        viewCount
        employeeInfo {
          id
          showInFrontPage
          position
          priority
          userId
          user {
            id
            avatar
            role
            username
            fullName
          }
        }
      }
    }
  }
`;gql`
  query commentsPagination(
    $type: CommentType!
    $refId: String!
    $parent: ObjectID
    $page: Int
  ) @api(name: "zim") {
    commentsPagination(
      type: $type
      refId: $refId
      parent: $parent
      page: $page
    ) {
      totalDocs
      totalPages
      limit
      page
      docs {
        ... on PostComment {
          id
          content
          numOfLikes
          createdAt
          numOfChildComments
          user {
            id
            email
            avatar
            fullName
          }
        }
      }
    }
  }
`;gql`
  query comments(
    $first: Int
    $type: CommentType!
    $refId: String!
    $parent: ObjectID
    $after: String
  ) @api(name: "zim") {
    comments(
      type: $type
      refId: $refId
      parent: $parent
      first: $first
      after: $after
    ) {
      pageInfo {
        hasNextPage
        hasPreviousPage
        endCursor
        startCursor
      }
      totalCount
      edges {
        node {
          ... on PostComment {
            id
            content
            numOfLikes
            createdAt
            numOfChildComments
            user {
              id
              email
              avatar
              fullName
            }
          }
        }
        cursor
      }
    }
  }
`;const ACCOUNT_FRAGMENT=gql`
  fragment AccountField on AccountType {
    id
    user_name
    full_name
    phone
    address
    email
    role_id
    role_name
    status
    status_online
    avatar
    supporter
    student_more_info {
      city_id
      city_name
      district_id
      district_name
      ward_id
      ward_name
      street_id
      street_name
      home_number
      source_id
      source_name
      job_id
      job_name
      identity_card
      identity_card_city_id
      identity_card_city_name
      identity_card_date
      learning_status_id
      work_place
      birthday
      note_home
      learning_status_name
    }
  }
`;gql`
  query getUserList($roleIds: [Int], $search: String) @api(name: "zim") {
    users(role_ids: $roleIds, search: $search) {
      totalDocs
      docs {
        id
        fullName
        role
      }
    }
  }
`;gql`
  query user($userId: Int!) @api(name: "zim") {
    user(id: $userId) {
      id
      fullName
      avatar
      role
      roleId
      username
      birthday
      gender
    }
  }
`;gql`
  query user($userId: Int!) @api(name: "zim") {
    user(id: $userId) {
      id
      fullName
      avatar
      role
      roleId
      username
      birthday
      gender
      email
      phone
      moreInfo
    }
  }
`;const GRAPH_NET_GET_USER_INFO=gql`
  query getUserById($userId: Int!) @api(name: "appZim") {
    getUserById(id: $userId) {
      id
      full_name
      avatar
      role_name
      address
      email
      phone
      status_online
      supporter {
        id
        full_name
        avatar
        role_name
      }
      more_info {
        facebook
      }
      courseStudents {
        id
        course {
          status
          id
          slug
          name
        }
      }
    }
  }
`,GET_SUPPORTER_USER_INFO=gql`
  query getUserById($userId: Int!) @api(name: "appZim") {
    getUserById(id: $userId) {
      id
      supporter {
        id
      }
    }
  }
`,GET_LIST_ACCOUNT_CHAT=gql`
  query getAccountChatPagination(
    $searchString: String
    $roles: [EnumRoleType]
    $page: Int
    $limit: Int
  ) @api(name: "appZim") {
    getAccountChatPagination(
      input: { q: $searchString, roles: $roles, page: $page, limit: $limit }
    ) {
      hasNextPage
      hasPrevPage
      limit
      page
      pageNext
      pagePrev
      pagingCounter
      totalPages
      totalDocs
      docs {
        user {
          id
          full_name
          role_id
          role_name
          status
          avatar
          status_online
        }
      }
    }
  }
`;gql`
  query getListStudentsPagination($input: TicketGetListStudentsInputType)
  @api(name: "appZim") {
    getListStudentsPagination(input: $input) {
      hasNextPage
      hasPrevPage
      limit
      page
      pageNext
      pagePrev
      pagingCounter
      totalPages
      totalDocs
      docs {
        supportUId
        userId
        schoolAddress
        course {
          name
        }
        status
        user {
          id
          user_name
          full_name
          phone
          address
          email
          role_id
          role_name
          status
          avatar
        }
      }
    }
  }
`;const GET_STUDENT_SUPPORTED_BY_EC_FOR_DATA_TABLE=gql`
  query getListStudentsPagination($input: TicketGetListStudentsInputType)
  @api(name: "appZim") {
    getListStudentsPagination(input: $input) {
      hasNextPage
      hasPrevPage
      limit
      page
      pageNext
      pagePrev
      pagingCounter
      totalPages
      totalDocs
      docs {
        supportUId
        userId
        schoolAddress
        course {
          name
          code
          #          day_of_weeks {
          #            id
          #            name
          #          }
          #          shifts {
          #            id
          #            start_time
          #            end_time
          #          }
          ec {
            id
            full_name
          }
        }
        status
        user {
          id
          user_name
          full_name
          phone
          address
          email
          role_id
          role_name
        }
      }
    }
  }
`;gql`
  query getListStudentByEc($input: AccountByEcInputType) @api(name: "appZim") {
    getListStudentByEc(input: $input) {
      page
      hasNextPage
      totalPages
      totalDocs
      docs {
        id
        full_name
        avatar
        phone
        address
        email
        status
        supporter
      }
    }
  }
`;const GET_LIST_STUDENT_BY_EC_BY_FIELD=(e=[])=>gql`
  query getListStudentByEc($input: AccountByEcInputType)
  @api(name: "appZim") {
    getListStudentByEc(input: $input) {
      page
      hasNextPage
      docs {
        id
        ${e.join()}
      }
    }
  }
`,GET_PERSONAL_INFO=gql`
  query getPersonalInfo($id: Int!) @api(name: "appZim") {
    getPersonalInfo(id: $id) {
      id
      userName
      sourceOfCustomer {
        id
        sourceOfCustomer
      }
      supporter {
        id
        user_name
        full_name
        phone
        address
        email
      }
      city {
        id
        name
        status
      }
      district {
        id
        name
        status
      }
      ward {
        id
        wardName
      }
      street {
        id
        streetName
      }
      homeNumber
      fullName
      phone
      email
      birthday
      address
      identityCard
      placeOfIssue {
        id
        name
        status
      }
      identityCardDate
      job {
        id
        jobName
      }
      workPlace
      academicPurposes {
        id
        academicPurposesName
      }
      noteHome
      typeOfEducation
      scoreIn
      scoreOut
      status
      statusId
    }
  }
`,GET_USER_BY_PHONE=gql`
  query searchPhone($phone: String!) @api(name: "appZim") {
    searchPhone(phone: $phone) {
      id
      user_name
      full_name
      phone
      address
      email
      role_id
      role_name
      status
      status_online
      avatar
      supporter
      student_more_info {
        identity_card_city_id
        note_home
        birthday
        city_id
        city_name
        district_id
        district_name
        ward_id
        ward_name
        street_id
        street_name
        home_number
        source_id
        source_name
        job_id
        job_name
        identity_card
        identity_card_city_name
        identity_card_date
        learning_status_id
        work_place
        learning_status_name
      }
    }
  }
`;gql`
  query getAccountChatPagination($input: AccountInputGraphNetType)
  @api(name: "appZim") {
    getAccountChatPagination(input: $input) {
      hasNextPage
      hasPrevPage
      limit
      page
      pageNext
      pagePrev
      pagingCounter
      totalPages
      totalDocs
      docs {
        ...AccountField
      }
    }
  }
  ${ACCOUNT_FRAGMENT}
`;const GET_TEACHERS=gql`
  query getListTeacherPagination($input: GetListTeacherInputType)
  @api(name: "appZim") {
    getListTeacherPagination(input: $input) {
      hasNextPage
      hasPrevPage
      limit
      page
      pageNext
      pagePrev
      pagingCounter
      totalPages
      totalDocs
      docs {
        ...AccountField
      }
    }
  }
  ${ACCOUNT_FRAGMENT}
`;gql`
  query isUserExist($field: ExistFieldCheck!, $value: String!)
  @api(name: "zim") {
    isUserExist(field: $field, value: $value)
  }
`;const GET_STUDENT_INVOICE=gql`
  query getStudentInvoices($student_id: Int, $status: EnumInvoiceStatus)
  @api(name: "appZim") {
    getStudentInvoices(student_id: $student_id, status: $status) {
      id
      student {
        id
        user_name
        full_name
        phone
        email
      }
      code
      number_invoice
      paid
      status
      payment
      qr_code
      ec_id
      ec_name
      type
      create_date
      create_by
      update_by
      update_date
      note
      appointment_date
      discount_price
      invoice_detail {
        id
        detail_id
        detail_type
        detail_name
        detail_fee
        subcat_name
      }
    }
  }
`,ADVERTISEMENT_BASE_FIELDS=gql`
  fragment AdvertisementBaseFields on Advertisement {
    id
    type
    status
    title
    description
    image {
      id
      filename
      path
      width
      height
    }
    url
    clicks
    showIn
    startDate
    endDate
    createdAt
    updatedAt
  }
`,ADVERTISEMENT_DETAIL_FIELDS=gql`
  fragment AdvertisementDetailFields on Advertisement {
    ...AdvertisementBaseFields
    postCategories {
      id
      title
    }
    productCategories {
      id
      title
    }
    posts {
      id
      title
    }
    tags {
      id
      name
    }
    products {
      ... on SimpleProduct {
        id
        name
      }
      ... on VariableProduct {
        id
        name
      }
    }
    excludeUrls
    position
    tagHtml
    tagPosition
  }
  ${ADVERTISEMENT_BASE_FIELDS}
`;gql`
  query advertisementPagination(
    $page: Int
    $limit: Int
    $order: OrderDirection
    $orderBy: AdvertisementSortBy
    $statuses: [AdvertisementStatus]
  ) @api(name: "zim") {
    advertisementPagination(
      page: $page
      limit: $limit
      order: $order
      orderBy: $orderBy
      statuses: $statuses
    ) {
      totalDocs
      totalPages
      docs {
        ...AdvertisementBaseFields
      }
    }
  }
  ${ADVERTISEMENT_BASE_FIELDS}
`;gql`
  query advertisement($id: ObjectID!) @api(name: "zim") {
    advertisement(id: $id) {
      ...AdvertisementDetailFields
    }
  }
  ${ADVERTISEMENT_DETAIL_FIELDS}
`;const SUPPORT_CONVERSATIONS=gql`
  query supportConversations(
    $status: ConversationStatus
    $search: String
    $offset: Int
    $limit: Int
  ) @api(name: "chat") {
    supportConversations(
      status: $status
      search: $search
      offset: $offset
      limit: $limit
    ) {
      hasNextPage
      hasPrevPage
      limit
      nextPage
      page
      prevPage
      docs {
        id
        name
        group
        participants {
          userId
          fullName
          nickname
          avatar
          isGuest
        }
        createdAt
        updatedAt
        typing
        usersTyping
        pinnedAt
        blockedBy
        isUnread
        lastMessage {
          id
          text
          from
          to
          type
          attachments {
            type
            attachmentId
            attachment {
              id
              path
              fullUrl
              type
            }
          }
          loading @client
          error @client
          seenBy
          createdAt
          updatedAt
          deletedAt
          callPayload
        }
      }
    }
  }
`,CONVERSATIONS=gql`
  query conversations(
    $status: ConversationStatus
    $search: String
    $offset: Int
    $limit: Int
    $group: String
  ) @api(name: "chat") {
    conversations(
      status: $status
      search: $search
      offset: $offset
      limit: $limit
      group: $group
    ) {
      hasNextPage
      hasPrevPage
      limit
      nextPage
      page
      prevPage
      docs {
        id
        name
        group
        participants {
          userId
          fullName
          nickname
          avatar
          isGuest
          online
        }
        createdAt
        updatedAt
        typing
        usersTyping
        pinnedAt
        blockedBy
        isUnread
        lastMessage {
          id
          text
          from
          to
          type
          attachments {
            type
            attachmentId
            attachment {
              id
              path
              fullUrl
              type
            }
          }
          loading @client
          error @client
          seenBy
          createdAt
          updatedAt
          deletedAt
          callPayload
        }
      }
    }
  }
`,CONVERSATION_DETAIL=gql`
  query conversationDetail($id: ObjectID, $userId: String) @api(name: "chat") {
    conversationDetail(id: $id, userId: $userId) {
      id
      name
      group
      typing
      usersTyping
      isUnread
      participants {
        userId
        fullName
        nickname
        avatar
        isGuest
        online
        roleId
      }
      owner {
        userId
        fullName
      }
      tags {
        id
        name
        color
      }
    }
  }
`,MESSAGES=gql`
  query messages(
    $conversationId: ObjectID
    $before: ObjectID
    $after: ObjectID
    $userId: String
    $offset: Int
    $limit: Int
  ) @api(name: "chat") {
    messages(
      conversationId: $conversationId
      before: $before
      after: $after
      userId: $userId
      offset: $offset
      limit: $limit
    ) {
      hasNextPage
      hasPrevPage
      limit
      nextPage
      page
      prevPage
      totalDocs
      docs {
        id
        text
        from
        to
        type
        attachments {
          type
          attachmentId
          attachment {
            id
            path
            fullUrl
            type
          }
        }
        conversation {
          id
          participants {
            userId
            avatar
            fullName
          }
        }
        loading @client
        error @client
        seenBy
        createdAt
        updatedAt
        deletedAt
        callPayload
        emojis {
          userId
          emoji
        }
      }
    }
  }
`,GET_NOTES=gql`
  query notes(
    $conversationId: ObjectID
    $userId: String
    $page: Int
    $limit: Int
  ) @api(name: "chat") {
    notes(
      conversationId: $conversationId
      userId: $userId
      page: $page
      limit: $limit
    ) {
      hasNextPage
      hasPrevPage
      limit
      nextPage
      page
      pagingCounter
      prevPage
      totalDocs
      totalPages
      docs {
        id
        content
        createdAt
        createdBy
        owner {
          userId
          fullName
          avatar
        }
      }
    }
  }
`,GET_ATTACHMENTS=gql`
  query getAttachmentsInConversation(
    $conversationId: ObjectID
    $userId: String
    $offset: Int
    $limit: Int
  ) @api(name: "chat") {
    getAttachmentsInConversation(
      conversationId: $conversationId
      userId: $userId
      offset: $offset
      limit: $limit
    ) {
      hasNextPage
      hasPrevPage
      limit
      nextPage
      page
      pagingCounter
      prevPage
      totalDocs
      totalPages
      docs {
        id
        attachments {
          type
          attachmentId
          attachment {
            id
            path
            fullUrl
            type
          }
        }
      }
    }
  }
`,GET_MESSAGES_UNREAD=gql`
  query @api(name: "chat") {
    statisticMessageUnread {
      zimians
      student
      customer
      open
      following
      another
      isMarkSeen
    }
  }
`,GET_TAGS=gql`
  query getListTag($page: Int, $limit: Int) @api(name: "chat") {
    getListTag(page: $page, limit: $limit) {
      docs {
        id
        name
        color
      }
    }
  }
`,SEARCH_MESSAGE=gql`
  query searchMessage($conversationId: ObjectID!, $search: String!)
  @api(name: "chat") {
    searchMessage(conversationId: $conversationId, search: $search) {
      hasNextPage
      hasPrevPage
      limit
      nextPage
      page
      pagingCounter
      prevPage
      totalDocs
      totalPages
      docs {
        id
        text
        from
        to
        type
        conversation {
          id
          participants {
            userId
            fullName
            avatar
          }
        }
        attachments {
          type
          attachmentId
          attachment {
            id
            path
            fullUrl
            type
          }
        }
        loading @client
        error @client
        seenBy
        createdAt
        updatedAt
        deletedAt
        callPayload
      }
    }
  }
`,NEAR_MESSAGE=gql`
  query nearbyMessages($messageId: ObjectID!, $limit: Int) @api(name: "chat") {
    nearbyMessages(messageId: $messageId, limit: $limit) {
      hasNextPage
      hasPrevPage
      limit
      nextPage
      page
      prevPage
      docs {
        id
        text
        from
        to
        type
        attachments {
          type
          attachmentId
          attachment {
            id
            path
            fullUrl
            type
          }
        }
        conversation {
          id
          participants {
            userId
            avatar
            fullName
          }
        }
        loading @client
        error @client
        seenBy
        createdAt
        updatedAt
        deletedAt
        callPayload
      }
    }
  }
`,QA_POST_CATEGORIES=gql`
  query QAPostCategoriesTree @api(name: "zim") {
    QAPostCategoriesTree {
      id
      title
    }
  }
`,ROOMS=gql`
  query (
    $school_id: Int
    $q: String
    $name: String
    $description: String
    $status: String
  ) @api(name: "appZim") {
    rooms(
      school_id: $school_id
      q: $q
      name: $name
      description: $description
      status: $status
    ) {
      id
      name
    }
  }
`,GET_SCHOOLS=gql`
  query schools($city_id: Int, $status: String) @api(name: "appZim") {
    schools(city_id: $city_id, status: $status) {
      id
      city {
        id
        name
      }
      district {
        id
        name
      }
      name
      address
      phone
      status
    }
  }
`,GET_CITIES=gql`
  query @api(name: "appZim") {
    cities {
      id
      name
      status
    }
  }
`;gql`
  query @api(name: "appZim") {
    cats {
      id
      name
    }
  }
`;const GET_SUBJECTS$1=gql`
  query subject($name: String, $status: StatusEnum) @api(name: "appZim") {
    subjects(name: $name, status: $status) {
      id
      name
      status
      cats {
        id
        name
        status
        subject {
          id
          name
          status
        }
      }
    }
  }
`,GET_LEVELS$1=gql`
  query levels($subject_id: Int!) @api(name: "appZim") {
    levels(subject_id: $subject_id, status: active) {
      id
      name
      status
      graduation
      subject_id
    }
  }
`,GET_ROOMS=gql`
  query rooms(
    $school_id: Int!
    $q: String
    $name: String
    $description: String
    $status: String
  ) @api(name: "appZim") {
    rooms(
      school_id: $school_id
      status: $status
      name: $name
      description: $description
      q: $q
    ) {
      id
      name
      status
      description
    }
  }
`;gql`
  query shifts($q: String) @api(name: "appZim") {
    shifts(q: $q, status: "active", shift_minute: 0) {
      id
      start_time
      end_time
      status
      shift_minute
    }
  }
`;gql`
  query getStatusAccount @api(name: "appZim") {
    getStatusAccount {
      key
      value
    }
  }
`;gql`
  query getTypeOfEducation @api(name: "appZim") {
    getTypeOfEducation {
      key
      value
    }
  }
`;gql`
  query getAcademicPurposesAll @api(name: "appZim") {
    getAcademicPurposesAll {
      id
      academicPurposesName
    }
  }
`;const GET_ECS=gql`
  query getEcs @api(name: "appZim") {
    getEcs {
      id
      user_name
      full_name
      phone
      address
      email
      role_id
      role_name
      status
      status_online
      avatar
      supporter
    }
  }
`,GET_ECS_BY_SCHOOL_ID=gql`
  query getListEcForCenter($schoolId: Int) @api(name: "appZim") {
    getListEcForCenter(schoolId: $schoolId) {
      id
      user_name
      full_name
      phone
      address
      email
      role_id
      role_name
      status
      status_online
      avatar
      supporter
    }
  }
`,GET_JOBS=gql`
  query getJobs @api(name: "appZim") {
    getJobs {
      id
      jobName
    }
  }
`,GET_SOURCES=gql`
  query getSourceOfCustomer @api(name: "appZim") {
    getSourceOfCustomer {
      id
      sourceOfCustomer
    }
  }
`,GET_DISTRICTS=gql`
  query districts($city_id: Int!, $streetName: String, $status: StatusEnum)
  @api(name: "appZim") {
    districts(city_id: $city_id, name: $streetName, status: $status) {
      id
      name
      status
    }
  }
`,GET_STREETS_BY_DISTRICT=gql`
  query getStreets($districtid: Int!, $streetName: String, $isHide: Boolean)
  @api(name: "appZim") {
    getStreets(
      districtid: $districtid
      streetName: $streetName
      isHide: $isHide
    ) {
      id
      streetName
    }
  }
`,GET_WARDS_BY_DISTRICT=gql`
  query getWards($districtid: Int!, $wardName: String, $isHide: Boolean)
  @api(name: "appZim") {
    getWards(districtid: $districtid, wardName: $wardName, isHide: $isHide) {
      id
      wardName
    }
  }
`;gql`
  mutation createProductCategory($input: ProductCategoryInput!)
  @api(name: "zim") {
    createProductCategory(input: $input) {
      id
      title
      slug
    }
  }
`;gql`
  mutation updateProductCategory(
    $id: ObjectID!
    $input: UpdateProductCategoryInput!
  ) @api(name: "zim") {
    updateProductCategory(id: $id, input: $input) {
      id
      title
    }
  }
`;gql`
  mutation deleteProductCategory($id: ObjectID!) @api(name: "zim") {
    deleteProductCategory(id: $id)
  }
`;gql`
  mutation ($name: String!, $type: ProductAttributeType!, $options: [String])
  @api(name: "zim") {
    createProductAttribute(name: $name, type: $type, options: $options) {
      id
      type
      name
      options
    }
  }
`;gql`
  mutation (
    $id: ObjectID!
    $name: String
    $type: ProductAttributeType
    $options: [String]
  ) @api(name: "zim") {
    updateProductAttribute(
      id: $id
      name: $name
      type: $type
      options: $options
    ) {
      id
      type
      name
      options
    }
  }
`;gql`
  mutation (
    $type: ProductType!
    $sku: String!
    $description: String!
    $shortDescription: String!
    $name: String!
    $featured: Boolean
    $categories: [ObjectID]
    $images: [ObjectID]
    $attributes: [ProductAttributeInput]
    $tags: [ObjectID]
    $variationAttributes: [ObjectID]
    $regularPrice: Int
    $salePrice: Int
    $salePriceFrom: DateTime
    $salePriceTo: DateTime
    $downloads: [ObjectID]
    $downloadable: Boolean
  ) @api(name: "zim") {
    createProduct(
      input: {
        type: $type
        sku: $sku
        description: $description
        shortDescription: $shortDescription
        name: $name
        featured: $featured
        categories: $categories
        images: $images
        attributes: $attributes
        tags: $tags
        variationAttributes: $variationAttributes
        regularPrice: $regularPrice
        salePrice: $salePrice
        salePriceFrom: $salePriceFrom
        salePriceTo: $salePriceTo
        downloads: $downloads
        downloadable: $downloadable
      }
    ) {
      ...ProductBaseField
      ...ProductDetailFields
    }
  }
  ${ProductDetailsFields}
`;gql`
  mutation (
    $id: ObjectID!
    $description: String!
    $shortDescription: String!
    $name: String!
    $featured: Boolean
    $categories: [ObjectID]
    $images: [ObjectID]
    $attributes: [ProductAttributeInput]
    $tags: [ObjectID]
    $regularPrice: Int
    $salePrice: Int
    $salePriceFrom: DateTime
    $salePriceTo: DateTime
    $downloads: [ObjectID]
    $downloadable: Boolean
    $variationAttributes: [ObjectID]
    $slug: String
    $sku: String
  ) @api(name: "zim") {
    updateProduct(
      id: $id
      input: {
        description: $description
        shortDescription: $shortDescription
        name: $name
        featured: $featured
        categories: $categories
        images: $images
        attributes: $attributes
        tags: $tags
        regularPrice: $regularPrice
        salePrice: $salePrice
        salePriceFrom: $salePriceFrom
        salePriceTo: $salePriceTo
        downloads: $downloads
        downloadable: $downloadable
        variationAttributes: $variationAttributes
        slug: $slug
        sku: $sku
      }
    ) {
      ...ProductBaseField
      ...ProductDetailFields
    }
  }
  ${ProductDetailsFields}
`;gql`
  mutation (
    $id: ObjectID!
    $description: String
    $shortDescription: String
    $name: String
    $categories: [ObjectID]
    $images: [ObjectID]
    $attributes: [ProductAttributeInput]
    $tags: [ObjectID]
    $regularPrice: Int
    $salePrice: Int
    $salePriceFrom: DateTime
    $salePriceTo: DateTime
    $downloads: [ObjectID]
    $downloadable: Boolean
    $slug: String
    $sku: String
  ) @api(name: "zim") {
    updateProductVariants(
      id: $id
      input: {
        description: $description
        shortDescription: $shortDescription
        name: $name
        categories: $categories
        images: $images
        attributes: $attributes
        tags: $tags
        regularPrice: $regularPrice
        salePrice: $salePrice
        salePriceFrom: $salePriceFrom
        salePriceTo: $salePriceTo
        downloads: $downloads
        downloadable: $downloadable
        slug: $slug
        sku: $sku
      }
    ) {
      ...ProductBaseField
      ...ProductDetailFields
    }
  }
  ${ProductDetailsFields}
`;gql`
  mutation ($parentProduct: ObjectID!, $input: AddVariantProductInput!)
  @api(name: "zim") {
    createProductVariants(parentProduct: $parentProduct, input: $input) {
      ...ProductBaseField
      ...ProductDetailFields
    }
  }
  ${ProductDetailsFields}
`;gql`
  mutation ($id: ObjectID!) @api(name: "zim") {
    publishProduct(id: $id) {
      ...ProductBaseField
    }
  }
  ${ProductBaseFieldsFragment}
`;gql`
  mutation ($id: ObjectID!) @api(name: "zim") {
    unPublishProduct(id: $id) {
      ...ProductBaseField
    }
  }
  ${ProductBaseFieldsFragment}
`;gql`
  mutation ($productId: ObjectID!, $inventories: [InventoryInput]!)
  @api(name: "zim") {
    bulkUpdateProductInventory(
      productId: $productId
      inventories: $inventories
    ) {
      id
      inStock
      inventories {
        id
        inStock
        inventory
        reserved
        manageStock
        store {
          id
          name
        }
      }
    }
  }
`;gql`
  mutation ($id: ObjectID!) @api(name: "zim") {
    removeProductVariants(id: $id) {
      ...ProductBaseField
    }
  }
  ${ProductBaseFieldsFragment}
`;gql`
  fragment RecruitmentPostBaseField on RecruitmentPost {
    id
    title
    area
    status
    position
    currentNumOfReplies
    maxCandidates
    startDate
    endDate
    createdAt
    updatedAt
    slug
  }
`;const RECRUITMENT_POST_DETAILS_FIELDS_FRAGMENTS=gql`
  fragment RecruitmentPostDetailsField on RecruitmentPost {
    id
    title
    area
    position
    numOfCandidates
    maxCandidates
    jobDescription
    currentNumOfReplies
    salaryFrom
    slug
    salaryType
    salaryTo
    status
    endDate
    createdAt
    updatedAt
    startDate
    seo {
      title
      description
      ogDescription
      ogImage {
        id
        path
      }
      ogTitle
      publisher
      noIndex
      noFollow
      canonicalUrl
      customHeadHtml
    }
  }
`;gql`
  mutation ($postId: ObjectID!, $seo: PostSeoInput!) @api(name: "zim") {
    updatePostSeoConfig(postId: $postId, seo: $seo) {
      id
      title
      slug
      seo {
        title
        description
        ogDescription
        ogImage {
          id
          path
        }
        ogTitle
        publisher
        noIndex
        noFollow
        canonicalUrl
        customHeadHtml
      }
      createdAt
      updatedAt
    }
  }
`;gql`
  mutation ($productId: ObjectID!, $seo: PostSeoInput!) @api(name: "zim") {
    updateProductSeoConfig(productId: $productId, seo: $seo) {
      ... on SimpleProduct {
        id
        createdAt
        updatedAt
        seo {
          title
          description
          ogDescription
          ogImage {
            id
            path
          }
          ogTitle
          publisher
          noIndex
          noFollow
          canonicalUrl
          customHeadHtml
        }
      }
      ... on VariableProduct {
        id
        createdAt
        updatedAt
        seo {
          title
          description
          ogDescription
          ogImage {
            id
            path
          }
          ogTitle
          publisher
          noIndex
          noFollow
          canonicalUrl
          customHeadHtml
        }
      }
    }
  }
`;gql`
  mutation ($recruitmentPostId: ObjectID!, $seo: CommonSeoInput!)
  @api(name: "zim") {
    updateRecruitmentPostSeoConfig(
      recruitmentPostId: $recruitmentPostId
      seo: $seo
    ) {
      ...RecruitmentPostDetailsField
    }
  }
  ${RECRUITMENT_POST_DETAILS_FIELDS_FRAGMENTS}
`;gql`
  query redirectUrlPagination($page: Int, $limit: Int, $search: String)
  @api(name: "zim") {
    redirectUrlPagination(page: $page, limit: $limit, search: $search) {
      docs {
        id
        prevUrl
        newUrl
        redirectCode
        createdAt
        updatedAt
      }
      totalPages
      totalDocs
    }
  }
`;gql`
  mutation updateRedirectUrl($id: ObjectID!, $input: UpdateRedirectUrlInput!)
  @api(name: "zim") {
    updateRedirectUrl(id: $id, input: $input) {
      id
      prevUrl
      newUrl
      redirectCode
      createdAt
      updatedAt
    }
  }
`;gql`
  mutation deleteRedirectUrl($id: ObjectID!) @api(name: "zim") {
    deleteRedirectUrl(id: $id)
  }
`;gql`
  mutation createRedirectUrl($input: CreateRedirectUrlInput!)
  @api(name: "zim") {
    createRedirectUrl(input: $input) {
      id
      prevUrl
      newUrl
      redirectCode
      createdAt
      updatedAt
    }
  }
`;function getCookie(e){const r=e+"=",a=decodeURIComponent(document.cookie).split(";");for(let l=0;l<a.length;l++){let c=a[l];for(;c.charAt(0)===" ";)c=c.substring(1);if(c.indexOf(r)===0)return c.substring(r.length,c.length)}return""}const getEnviroment=e=>{var n;let r=(n={ZIM_BUILD_ENV:"production",ZIM_BUILD_MODE:"module",ZIM_BASE_URL:"https://app.zim.vn",ZIM_IMAGE_ENDPOINT:"https://graph-api.zim.vn",ZIM_CHAT_ENDPOINT:"https://chat.zim.vn",ZIM_GRAPH_API_ENDPOINT:"https://graph-api.zim.vn",ZIM_GRAPH_NET_API_ENDPOINT:"https://graphnet-api.zim.vn",ZIM_CRAWLER_ENDPOINT:"https://crawler.zim.vn",ZIM_PORT:"3000",ZIM_FRONT_PAGE_URL:"https://zim.vn",ZIM_RECAPTCHA_KEYS:"6LcB3-AcAAAAALD_an7FPdBHvKBYLAA3N8gp3Djk",ZIM_TOKEN:"",ZIM_CHAT_MEDIA_URL:"https://chat-media.zim.vn",ZIM_ADMIN_PAGE_URL:"https://admin.zim.vn",ZIM_MEDIA_APP_ZIM:"https://media-app.zim.vn",BASE_URL:"/",MODE:"production",DEV:!1,PROD:!0}[e])!=null?n:"";if(typeof window!="undefined"&&localStorage){const a=localStorage.getItem(e);return a||r}return r},isModuleMode$1=getEnviroment("ZIM_BUILD_MODE")==="module";getEnviroment("ZIM_TOKEN");const ZIM_TOKEN=getEnviroment("ZIM_TOKEN"),queryRefresh=`
    mutation {
        refreshToken {
            token
            refreshToken
        }
    }
`;let authToken="";const appSetAuthToken=e=>{authToken=e},appClearAuthToken=()=>{authToken=""},appGetAuthToken=()=>ZIM_TOKEN||(isModuleMode$1?getCookie("token"):authToken),errorLink=onError(({graphQLErrors:e,networkError:r})=>{e&&e.map(({message:n,locations:a,path:l})=>console.log(`[GraphQL error]: Message: ${n}, Location: ${a}, Path: ${l}`)),r&&console.log(`[Network error]: ${r}`)}),requestLink=new ApolloLink((e,r)=>new Observable(n=>{let a;const l=e.getContext().hasOwnProperty("headers")?e.getContext().headers:{};return Promise.resolve(e).then(c=>{c.setContext({headers:N({authorization:`Bearer ${appGetAuthToken()}`,"Access-Control-Allow-Credentials":!0,"x-app-token":"0aAUfvzhIZVSDIF0yBk53U8Vokij6xv7"},l)})}).then(()=>{a=r(e).subscribe({next:n.next.bind(n),error:n.error.bind(n),complete:n.complete.bind(n)})}).catch(n.error.bind(n)),()=>{a&&a.unsubscribe()}})),cache=new InMemoryCache({possibleTypes:{BaseProduct:["VariableProduct","SimpleProduct"]},typePolicies:{Participant:{keyFields:["userId"]},AccountStudentInfoType:{keyFields:(e,r)=>`${e.source_name}-${e.district_name}-${e.job_name}-${e.ward_name}-${e.city_name}-${e.street_name}-${e.birthday}`},AccountType:{keyFields:(e,r)=>`${e.id}-${e.full_name}-${e.phone}`},Message:{fields:{loading:{read:()=>!1},error:{read:()=>!1}}},Query:{fields:{getAccountChatPagination:{keyArgs:["input",["q","limit","roles"]]},messages:{keyArgs:["conversationId","userId","offset","before","after"]},supportConversations:{keyArgs:["status"]},conversations:{keyArgs:["group"]},medias:relayStylePagination(["type","width","height","search"]),tags:relayStylePagination(["search"]),comments:relayStylePagination(["type","refId","parent"])}}}});global.wsClients={};const wsClient=e=>{const r=new SubscriptionClient_1(e,{reconnect:!0,connectionParams:()=>({authToken:appGetAuthToken()})});return global.wsClients[e]=r,r},wsLink=e=>new WebSocketLink(wsClient(e)),multiApiLink=new MultiAPILink({endpoints:{zim:getEnviroment("ZIM_GRAPH_API_ENDPOINT"),appZim:getEnviroment("ZIM_GRAPH_NET_API_ENDPOINT"),analytics:getEnviroment("REACT_APP_ANALYTICS_API_ENDPOINT"),chat:getEnviroment("ZIM_CHAT_ENDPOINT"),crawler:getEnviroment("ZIM_CRAWLER_ENDPOINT")},createHttpLink:()=>createUploadLink({credentials:"include",fetch:buildAxiosFetch_1(axios,(e,r,n)=>H(N({},e),{withCredentials:!0,onUploadProgress:n.onUploadProgress}))}),wsSuffix:"/graphql",createWsLink:e=>wsLink(e)}),fetchAccessToken=async()=>{if(ZIM_TOKEN)return{token:ZIM_TOKEN,refreshToken:"2kp09Du2CzEN6JkN3V7DwTbOdslfRNjk"};const e={operationName:null,variables:{},query:queryRefresh};return isModuleMode$1?{token:getCookie("token"),refreshToken:"2kp09Du2CzEN6JkN3V7DwTbOdslfRNjk"}:fetch(`${getEnviroment("ZIM_GRAPH_API_ENDPOINT")}/graphql`,{method:"POST",credentials:"include",body:JSON.stringify(e),headers:{"Access-Control-Request-Headers":"content-type","Content-Type":"application/json; charset=utf-8"}}).then(async r=>{const n=await r.json();return n==null?void 0:n.data.refreshToken})},refreshTokenLink=new TokenRefreshLink({accessTokenField:"token",isTokenValidOrUndefined:()=>{if(isModuleMode$1)return!0;const e=appGetAuthToken();if(e.length===0)return!0;try{const{exp:r}=o(e);return Date.now()<r*1e3}catch{return!1}},fetchAccessToken,handleFetch:e=>{e?(localStorage.setItem("isAuthenticated","true"),appSetAuthToken(e)):localStorage.removeItem("isAuthenticated")},handleResponse:()=>{},handleError:e=>{console.log(`handleError: ${e}`)}}),normalLink=ApolloLink.from([refreshTokenLink,requestLink,errorLink,multiApiLink]),splitLink=split$1(({query:e})=>{const r=getMainDefinition(e);return r.kind==="OperationDefinition"&&r.operation==="subscription"},ApolloLink.from([refreshTokenLink,multiApiLink]),normalLink),apolloClient=new ApolloClient({ssrMode:typeof window=="undefined",link:splitLink,credentials:"include",cache,connectToDevTools:!0}),authContext=react.exports.createContext({appState:{isAuthenticated:!1,user:null,loaded:!1}}),ProvideAuth=({children:e})=>{const[r,n]=react.exports.useState({isAuthenticated:!1,user:null,loaded:!1,buildMode:getEnviroment("ZIM_BUILD_MODE")}),a=async()=>{var u,d;try{const m=await apolloClient.query({query:GET_USER,fetchPolicy:"network-only"});((u=m==null?void 0:m.data)==null?void 0:u.me)?(n(H(N({},r),{user:(d=m==null?void 0:m.data)==null?void 0:d.me,isAuthenticated:!0,loaded:!0})),localStorage.setItem("isAuthenticated","true")):(localStorage.removeItem("isAuthenticated"),n(H(N({},r),{user:null,isAuthenticated:!1,loaded:!0})))}catch{n(H(N({},r),{user:null,isAuthenticated:!1,loaded:!0})),localStorage.removeItem("isAuthenticated")}},l=async u=>{appSetAuthToken(u),await a()},c=async()=>{appSetAuthToken(""),await apolloClient.mutate({mutation:SIGN_OUT}),n(H(N({},r),{user:null,isAuthenticated:!1,loaded:!0})),localStorage.removeItem("isAuthenticated")};return jsx(authContext.Provider,{value:{getUser:a,appState:r,setAppState:n,appSetLogin:l,appSetLogout:c,appSetAuthToken,appClearAuthToken},children:jsx(ApolloProvider,{client:apolloClient,children:e})})};function useAppContext(){return react.exports.useContext(authContext)}const useIsMounted=()=>{const e=react.exports.useRef(!1);return react.exports.useEffect(()=>(e.current=!0,()=>{e.current=!1}),[]),react.exports.useCallback(()=>e.current,[])};var logoSrc="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACsAAAArCAYAAADhXXHAAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAVLSURBVHgBxVlZTJxVFD4MAxRkjQFpQ2UIOyTsOw1LKqExMSUkjYGEqBF8aaI1Gk0jidEHfND2gRf7YGJ4MPpgrH1p0ZTUFtoEZN93BkpQWexggixh8Tu3/BM7673/UPiSO+f+858z//ffe+45597xIg+QlpYWsbW1dRHdHLQUtDNoEWin0LbQFry8vJYPDg7m0b9jMBg6x8fHzaQTXqQIk8kU6uvr+y5IXACJQlIE7Ib39/evGY3GX8bGxv5QspVVPCT5HrpX0ELJc6yhfYP21eTk5KqMgRTZuLi41zCFN+jpNB81zJihD6ampn5yp+iSLEgGY9o+Q7tCzxkg/CUIf+RKxynZ5OTk6L29vZvoZtIxAYQfbG9vVy0sLDxxdN8h2ZiYmJd8fHx+QzeJjh/9WBsXhoeH/7K9YXCkDaK36GSIMjJ2dnZ+xII+ZXvDjmxCQsJ1iHw6WZzD6DbZfvmMG4Do6xA/kCTgY1ReXk4VFRUUEREha0ZLS0vU1NRE8E+Xet7e3hWIxXfJlmxiYuIZPPwhuiaSAII6tbS0UE5ODulBZWUlmc1md2pmuEQm9Cx8YXUDjnWyRBkNDQ26ia6srNDc3JyMqgnr57J2IcjGxsaeRSx9ixTA068XAwMDnHaldKH3IWdP7guyYH8eIxtGkggJCaH09HTSi56eHhX1ULjcm9wRZFFYfKxi7QlRRldXl5A1NTXU2NgoFqorYHS5siNDUlKSiRRjalZWFukFFgyhcBH92tpaqqurc2sDsmXsCgaMqnJMzc/XH4a7u7sF4fDwcA6V1NfXJ+W/iLsX2Q1eJQWg+uK6gfSiv79fSC2S9Pb2ypqmGfBWL5MCsrOzyd/fn/RCI1tcXCxkR0eHrGkKj2w4KeCoFld8fLyQ09PTsqanDViJZ0kBGRkZpBdInbS5uUmBgYHid0ZHR2l5eVnW/EUeWT9ZbeRqKikpIb1ob28XUpsdlIEK1hTIZDdltXnq/Pyk380Og4ODQmqhTzE5WJis1GaNUVRURJ5AW/mFhU83xewGClhiso9ltT1JBkxsdXVVZCsOfYuLi4QzBGl72O3wApOy4Id4srhmZmZE8M/Ly6OAgAAaGRkRMVsW0P3dwB8yyikpKSLr6IUWT1NTU4Vsa2sjFWCwHhmQxn6WUY6OjiZPoC0mTiqM+fl5JXvstB8Zh4aGniBHP8C1y5hUVlZm7VssFuFzqCtIBuyr2F6LfkFBAa2vrwvyHAolcRdutGw8vLjjjmxkZKSQvBWprq6mjY0NUgH7K7b4FBwcLFxCgSi7wHcshYejCuKjIYsrAy3TtLa2KhPVUFpaKmRnZ6e0DYg+RvueNLK8IcObX3dlNDExISTvZKOiomh3d1epcQTIzc0Vv6HVBzJAALiG+kFsg62F5OEpYR852TRy5mpubrb6LhNQAYcpbuznmZmZhHNdGTMzCvVYSLE4jNZvMboI1u9g1f3qyIr3+PX19VRVVSUqfL1hjN1IkiijTiPKsCvRD09k3qeTx+cY1U///4XdkgwKCrqP6ToHHzbRyeE2iDbYfmmX7+AOWyB6CV2lkuiogGc/hF/XOLrnMDnzsTnC2Svo3qNjBGb0JtbMeaz+fxzdd7uthA/zad5Vev74AoP0CaTTQwSpMxwc2vE/M1+TwlmYAmYw7W9jNO+7U5TKeWtra9NhYWE34E+cuvj/riDyEPitv9Guwt0uz87OTkjZkCLwR90LeMAl+NYbfFJCioDNPczStxjNW85806kteQDUuJHIZAUgwIcAXPv5gshpyAC0f3n0cP0n+sOQg3jB2xjFddKJ/wBliiZzlCtvyQAAAABJRU5ErkJggg==";const useBuildModeValue=(e,r)=>getEnviroment("ZIM_BUILD_MODE")==="module"?r:e,NavLink=e=>useBuildModeValue(jsx(Link$1,H(N({rounded:"base",_hover:{textDecoration:"none"},as:Link$2,color:useColorModeValue("blue.500","blue.300")},e),{children:e.children})),jsx(Link$1,H(N({href:e.to,isExternal:!0},e),{children:e.children}))),SideMenu=({data:e,onClose:r})=>jsx(Fragment,{children:jsx(Accordion,{allowToggle:!0,children:e.map((n,a)=>jsxs(AccordionItem,{id:n.link,children:[jsxs(AccordionButton,{justifyContent:"space-between",children:[jsxs(HStack,{justifyContent:"start",children:[n.icon&&jsx(n.icon,{}),jsx(Text,{whiteSpace:"nowrap",children:n.label})]}),jsx(AccordionIcon,{})]}),jsx(AccordionPanel,{p:0,children:n.submenus.map((l,c)=>jsxs(Box,{p:4,children:[l.showLabel&&jsx(Text,{fontWeight:600,mb:4,children:l.label}),jsx(Flex,{direction:"column",gap:4,children:l.children.map((u,d)=>jsx(NavLink,{to:u.link,children:jsxs(Text,{onClick:r,children:[" ",u.label]})},d))})]},`${l.link}-${c}`))})]},a))})}),MegaMenu=({data:e,onClose:r})=>jsx(Fragment,{children:jsxs(Tabs,{d:"flex",children:[jsx(TabList,{borderRight:"1px solid",borderRightColor:useColorModeValue("gray.200","slate.700"),borderBottom:0,pr:4,children:jsx(VStack,{justifyContent:"stretch",alignItems:"stretch",children:e.map((n,a)=>jsx(Tab,{justifyContent:"start",children:jsxs(HStack,{justifyContent:"start",children:[n.icon&&jsx(n.icon,{}),jsx(Text,{whiteSpace:"nowrap",children:n.label})]})},a))})}),jsx(TabPanels,{children:e.map((n,a)=>jsx(TabPanel,{px:4,py:0,children:jsx(HStack,{spacing:0,alignItems:"stretch",justifyContent:"start",mx:-4,flexWrap:"wrap",children:n.submenus.map((l,c)=>jsxs(VStack,{w:{base:"50%",xl:"33%","2xl":"25%"},alignItems:"stretch",justifyContent:"start",spacing:2,p:4,children:[l.showLabel&&jsx(Text,{fontWeight:600,children:l.label}),jsx(VStack,{alignItems:"stretch",justifyContent:"start",spacing:2,children:l.children.map((u,d)=>jsx(NavLink,{to:u.link,children:jsx(Text,{onClick:r,children:u.label})},d))})]},c))})},a))})]})}),routePaths={home:"/",account:"/account",listCourses:"/course",listCoursesPlan:"/course-plan",courseDetail:"/course/detail",coursePlanDetail:"/course-plan/detail",forbiddenError:"/error/403",ticket:"/ticket",chat:"/chat",report:"/report",leadReport:"/report/lead",studentSupportByEc:"/user/student-support-by-ec",scheduleRegistration:"/schedule/schedule-registration",listSchedules:"/schedule/list-schedules",registerTestSchedule:"/schedule/test/reg",lead:"/web-data/lead",paperBasedTestBank:"/paper-based-test-bank",mockTestOnline:"/mock-test/mock-test-online",mockTestOffline:"/mock-test/mock-test-offline",mockTestDetail:"/mock-test/mock-test-detail",exercises:"/exercise",updateExercise:"/exercise/update",coursePlanner:"/course/course-planner",invoice:"/invoice"},appZimPaths={lead:"/Admin/ContactCustomer/ContactList",customerInfo:"/Admin/Customer/CustomerDetai",staffInfo:"/Admin/Staff/StaffDetail",courseDetail:"/Admin/CourseManage/CourseDetail",coursePlanDetail:"/Admin/CourseManage/CoursePlanDetail",ticketList:"/Admin/Ticket",customerList:"/Admin/Customer/CustomersList",listStudentsMockTest:"/Admin/MockTest/ListStudent",mockTestDetail:"/Admin/MockTest/MockTest",ieltsCorrecting:"/Admin/Ieltscorrect/Correcting",newsFeed:"/news-feed",invoice:"/invoice",exerciseDetail:"/Admin/config/ExerciseDetail"},ZimVnPaths={testResult:"/test/result",previewTestModule:"/test/module/preview"},menuData=[{label:"Chung",link:"#",icon:ImNewspaper,submenus:[{label:"",link:"/",icon:null,showLabel:!1,children:[{label:"News",link:"/",icon:null,children:[]},{label:"Dashboard",link:"/",icon:null,children:[]}]}]},{label:"Kh\xF3a h\u1ECDc",link:"#",icon:SiGoogleclassroom,submenus:[{label:"Qu\u1EA3n l\xFD kh\xF3a h\u1ECDc",link:"/",icon:null,showLabel:!0,children:[{label:"T\u1EA1o kh\xF3a h\u1ECDc",link:"/",icon:null,children:[]},{label:"Danh s\xE1ch kh\xF3a h\u1ECDc",link:routePaths.listCourses,icon:null,children:[]},{label:"Kh\xF3a h\u1ECDc d\u1EF1 ki\u1EBFn",link:"/",icon:null,children:[]},{label:"L\u1ECBch d\u1EA1y gi\xE1o vi\xEAn",link:"/",icon:null,children:[]}]}]},{label:"Th\u01B0 vi\u1EC7n",link:"#",icon:IoLibraryOutline,submenus:[{label:"T\xE0i li\u1EC7u",link:"/",icon:null,showLabel:!0,children:[{label:"Document",link:"/",icon:null,children:[]},{label:"T\xE0i li\u1EC7u s\xE1ch",link:"/",icon:null,children:[]},{label:"Video",link:"/",icon:null,children:[]}]}]},{label:"Kh\xE1ch h\xE0ng",link:"#",icon:HiUserGroup,submenus:[{label:"H\u1ECDc vi\xEAn",link:"/",icon:null,showLabel:!0,children:[{label:"Danh s\xE1ch h\u1ECDc vi\xEAn",link:"/",icon:null,children:[]},{label:"Danh s\xE1ch trong kh\xF3a",link:"/",icon:null,children:[]},{label:"Danh s\xE1ch HV b\u1EA3o l\u01B0u",link:"/",icon:null,children:[]},{label:"Danh s\xE1ch kh\xE1ch h\u1EB9n test",link:"/",icon:null,children:[]}]},{label:"\u0110\u0103ng k\xFD",link:"/",icon:null,showLabel:!0,children:[{label:"Leads",link:"/",icon:null,children:[]},{label:"\u0110\u0103ng k\xFD kh\xF3a d\u1EF1 ki\u1EBFn",link:"/",icon:null,children:[]},{label:"\u0110\u0103ng k\xFD ch\u01B0\u01A1ng tr\xECnh",link:"/",icon:null,children:[]},{label:"\u0110\u0103ng k\xFD thi th\u1EED",link:"/",icon:null,children:[]},{label:"\u0110\u0103ng k\xFD thi t\u1EA1i BC",link:"/",icon:null,children:[]},{label:"\u0110\u0103ng k\xFD x\xE9t h\u1ECDc b\u1ED5ng",link:"/",icon:null,children:[]},{label:"HV y\xEAu c\u1EA7u v\xE0o kh\xF3a",link:"/",icon:null,children:[]}]},{label:"H\u1EE3p \u0111\u1ED3ng",link:"/",icon:null,showLabel:!0,children:[{label:"H\u1ECDc vi\xEAn c\xF3 h\u1EE3p \u0111\u1ED3ng",link:"/",icon:null,children:[]},{label:"H\u1EE3p \u0111\u1ED3ng ch\u1EDD duy\u1EC7t",link:"/",icon:null,children:[]}]},{label:"H\u1ECDc t\u1EADp",link:"/",icon:null,showLabel:!0,children:[{label:"C\u1EA3nh b\xE1o h\u1ECDc vi\xEAn",link:"/",icon:null,children:[]},{label:"H\u1ECDc vi\xEAn s\u1EAFp thi",link:"/",icon:null,children:[]},{label:"K\u1EBFt qu\u1EA3 thi th\u1EF1c t\u1EBF",link:"/",icon:null,children:[]},{label:"Th\u01B0\u1EDFng t\xE0i tr\u1EE3",link:"/",icon:null,children:[]}]}]},{label:"Nh\xE2n vi\xEAn",link:"#",icon:FaUserFriends,submenus:[{label:"Kh\u1ED1i kinh doanh",link:"/",icon:null,showLabel:!0,children:[{label:"Danh s\xE1ch nh\xE2n vi\xEAn",link:"/",icon:null,children:[]},{label:"Y\xEAu c\u1EA7u thay \u0111\u1ED5i ca",link:"/",icon:null,children:[]},{label:"Qu\u1EA3n l\xFD ca l\xE0m vi\u1EC7c EC",link:"/",icon:null,children:[]}]},{label:"Gi\u1EA3ng vi\xEAn",link:"/",icon:null,showLabel:!0,children:[{label:"Danh s\xE1ch gi\u1EA3ng vi\xEAn",link:"/",icon:null,children:[]},{label:"B\xE0i \u0111\xE3 ch\u1EA5m",link:"/",icon:null,children:[]},{label:"Gi\u1EDD d\u1EA1y gi\xE1o vi\xEAn",link:"/",icon:null,children:[]},{label:"Y\xEAu c\u1EA7u thay \u0111\u1ED5i l\u1ECBch ngh\u1EC9",link:"/",icon:null,children:[]},{label:"B\xE1o c\xE1 gi\xE1 v\u1ED1n h\xE0ng b\xE1n",link:"/",icon:null,children:[]},{label:"Chi ph\xED l\u01B0\u01A1ng gi\u1EA3ng vi\xEAn",link:"/",icon:null,children:[]}]}]},{label:"T\xE0i ch\xEDnh",link:"#",icon:BiMoney,submenus:[{label:"L\u01B0\u01A1ng kinh doanh",link:"/",icon:null,showLabel:!0,children:[{label:"Chi\u1EBFn d\u1ECBch sale",link:"/",icon:null,children:[]},{label:"Doanh thu chi\u1EBFn d\u1ECBch sale",link:"/",icon:null,children:[]},{label:"L\u01B0\u01A1ng office",link:"/",icon:null,children:[]},{label:"Duy\u1EC7t l\u01B0\u01A1ng office",link:"/",icon:null,children:[]},{label:"L\u1ECBch s\u1EED duy\u1EC7t",link:"/",icon:null,children:[]},{label:"L\u01B0\u01A1ng gi\u1EA3ng vi\xEAn",link:"/",icon:null,children:[]}]},{label:"Phi\u1EBFu thu",link:"/",icon:null,showLabel:!0,children:[{label:"Qu\u1EA3n l\xFD phi\u1EBFu thu",link:"/",icon:null,children:[]},{label:"Y\xEAu c\u1EA7u h\u1EE7y phi\u1EBFu thu",link:"/",icon:null,children:[]}]}]},{label:"D\u1ECBch v\u1EE5",link:"#",icon:MdOutlineMiscellaneousServices,submenus:[{label:"Ch\u1EA5m b\xE0i IELTS Correct",link:"/",icon:null,showLabel:!0,children:[{label:"Danh s\xE1ch b\xE0i ch\u1EA5m",link:"/",icon:null,children:[]},{label:"DS gi\u1EA3ng vi\xEAn ch\u1EA5m",link:"/",icon:null,children:[]},{label:"C\u1EA5u h\xECnh l\u01B0\u01A1ng b\xE0i ch\u1EA5m",link:"/",icon:null,children:[]}]}]},{label:"C\u1EA5u h\xECnh",link:"#",icon:IoSettingsOutline,submenus:[{label:"Kh\xF3a h\u1ECDc",link:"/",icon:null,showLabel:!0,children:[{label:"M\xF4n h\u1ECDc",link:"/",icon:null,children:[]},{label:"Ch\u01B0\u01A1ng tr\xECnh h\u1ECDc",link:"/",icon:null,children:[]},{label:"L\u1ED9 tr\xECnh h\u1ECDc",link:"/",icon:null,children:[]},{label:"H\u1EC7 s\u1ED1",link:"/",icon:null,children:[]},{label:"M\xE3 khuy\u1EBFn m\xE3i",link:"/",icon:null,children:[]}]},{label:"H\u1EC7 th\u1ED1ng",link:"/",icon:null,showLabel:!0,children:[{label:"Trung t\xE2m",link:"/",icon:null,children:[]},{label:"T\u1EC9nh / Th\xE0nh ph\u1ED1",link:"/",icon:null,children:[]},{label:"Qu\u1EADn huy\u1EC7n",link:"/",icon:null,children:[]},{label:"Ngu\u1ED3n kh\xE1ch h\xE0ng",link:"/",icon:null,children:[]},{label:"Ng\xE0y ngh\u1EC9",link:"/",icon:null,children:[]},{label:"Ngh\u1EC1 nghi\u1EC7p",link:"/",icon:null,children:[]},{label:"D\u1ECBch v\u1EE5",link:"/",icon:null,children:[]},{label:"Lo\u1EA1i ph\u1EA3n h\u1ED3i",link:"/",icon:null,children:[]},{label:"Nh\xE0 cung c\u1EA5p",link:"/",icon:null,children:[]},{label:"M\u1EE5c \u0111\xEDch h\u1ECDc",link:"/",icon:null,children:[]},{label:"Th\xE0nh ng\u1EEF l\u1ECBch",link:"/",icon:null,children:[]},{label:"M\u1EABu h\u1EE3p \u0111\u1ED3ng",link:"/",icon:null,children:[]},{label:"\u0110i\u1EC1u kho\u1EA3n",link:"/",icon:null,children:[]}]}]}],useToggle=(e=!1)=>{const[r,n]=react.exports.useState(e),a=react.exports.useCallback(l=>n(c=>l!==void 0?l:!c),[]);return[r,a]},useEventListener=(e,r,n=window)=>{const a=react.exports.useRef();react.exports.useEffect(()=>{a.current=r},[r]),react.exports.useEffect(()=>{if(!(n&&n.addEventListener))return;const c=u=>a&&typeof a.current=="function"?a.current(u):null;return typeof c=="function"&&n.addEventListener(e,c),()=>{typeof c=="function"&&n.removeEventListener(e,c)}},[e,n])},useCombinedRefs=(...e)=>{const r=react.exports.useRef();return react.exports.useEffect(()=>{e.forEach(n=>{!n||(typeof n=="function"?n(r.current):n.current=r.current)})},[e]),r},TextInput=react.exports.forwardRef((a,n)=>{var l=a,{error:e}=l,r=Te(l,["error"]);return jsxs(FormControl,{isInvalid:!!e,children:[(r==null?void 0:r.label)&&jsx(FormLabel,{htmlFor:r.id,children:r.label}),jsx(Input$1,H(N({bg:useColorModeValue("white","slate.700"),borderColor:useColorModeValue("gray.300","slate.600"),_hover:{borderColor:useColorModeValue("gray.400","slate.700")},color:useColorModeValue("gray.900","slate.300"),_placeholder:{color:useColorModeValue("gray.400","slate.500")}},r),{ref:n})),e&&jsx(FormErrorMessage,{children:e&&(e==null?void 0:e.message)})]})}),SearchInput=react.exports.forwardRef((l,a)=>{var c=l,{placeholder:e,onSubmitSearch:r}=c,n=Te(c,["placeholder","onSubmitSearch"]);const u=react.exports.useRef(null),d=useCombinedRefs(a,u);return jsx(Box,{as:"form",onSubmit:b=>{b.preventDefault();const{value:A}=d.current;typeof r=="function"&&r(A)},id:"form-header-search",w:"full",children:jsxs(InputGroup,{flexGrow:1,w:"100%",children:[jsx(InputRightElement,{children:jsx(Button$1,{p:0,variant:"ghost",_hover:{backgroundColor:"transparent"},type:"submit",children:jsx(VscSearch,{fontSize:18})})}),jsx(TextInput,H(N({placeholder:"T\xECm ki\u1EBFm...",variant:"filled"},n),{ref:d,pr:10}))]})})}),CONVERSATION_UPDATED=gql`
  subscription ($conversationId: ObjectID, $status: String) @api(name: "chat") {
    conversationUpdated(conversationId: $conversationId, status: $status) {
      id
      name
      isDeleteMessage
      group
      status
      createdAt
      updatedAt
      pinnedAt
      blockedBy
      isUnread
      typing
      usersTyping
      participants {
        userId
        fullName
        nickname
        avatar
        isGuest
        online
      }
      lastMessage {
        id
        text
        from
        to
        type
        attachments {
          type
          attachmentId
          attachment {
            id
            path
            fullUrl
            type
          }
        }
        conversation {
          id
          participants {
            userId
            avatar
            fullName
          }
        }
        loading @client
        error @client
        seenBy
        createdAt
        updatedAt
        deletedAt
        callPayload
        emojis {
          userId
          emoji
        }
      }
    }
  }
`,MESSAGES_UNREAD=gql`
  subscription @api(name: "chat") {
    messageAdded {
      zimians
      student
      customer
      open
      following
      another
      isMarkSeen
    }
  }
`;function useUser(){const{appState:{user:e},setUser:r}=react.exports.useContext(authContext);return[e,r]}var ring$1="/assets/notificationSound.mp3";const useGetMessageUnread=()=>{const[e]=useUser(),r=new Audio(ring$1);r.volume=1;const{data:n,loading:a}=useQuery(GET_MESSAGES_UNREAD),l=n==null?void 0:n.statisticMessageUnread;return useSubscription(MESSAGES_UNREAD,{onSubscriptionData:({client:c,subscriptionData:u})=>{var m,b;const d=c.cache.readQuery({query:GET_MESSAGES_UNREAD});if(u.data){const{messageAdded:A}=u.data;A?(c.writeQuery({query:GET_MESSAGES_UNREAD,data:H(N({},d),{statisticMessageUnread:N(N({},d.statisticMessageUnread),A)})}),((b=(m=u==null?void 0:u.data)==null?void 0:m.messageAdded)==null?void 0:b.isMarkSeen)||r.play()):c.writeQuery({query:GET_MESSAGES_UNREAD,data:N({},d)})}},shouldResubscribe:!!e}),{listMessageUnreadOverall:l,loading:a}},GROUP_ROLES={editor:[1,2,11],approve:[1,8],author:[1,8,11,4],manager:[1,2,12,8,11],productManager:[1,2],censor:[1,2,8],canEdit:[1,2,4,8,11],canConfigSeo:[1,2,11],admin:[1],adminAndCC:[1,2],canCreateTicket:[5,6,12,1,2,11,12],canCancelSchedule:[2],canEnterMockTestScores:[4],canUploadMockTestTask:[6],canCreateOrEditMockTest:[1,2],assignTeacherAndEC:[1,2],viewTableMockTestDetail:[1,2,6],canBookSchedule:[6],canCancelEmptyScheduleOfTeacher:[1]},ROLES=[{id:1,name:"Super Admin"},{id:2,name:"CC"},{id:4,name:"Gi\xE1o vi\xEAn"},{id:5,name:"Student"},{id:6,name:"EC"},{id:8,name:"AM"},{id:9,name:"K\u1EBF to\xE1n"},{id:11,name:"Marketer"},{id:12,name:"BM"},{id:13,name:"BP"},{id:14,name:"Guest"}],NOTI_STUDENT_ROLE=[5],NOTI_EC_ADMIN_ROLE=[1,6],appConfigs={apiUrl:"https://app.zim.vn",apiVersion:"v1",access_token:"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJqc29uX3dlYl90b2tlbiIsImp0aSI6ImM5ZDFmNzA3LTk4ZmYtNGNmMC1iMGUyLTAwN2RhY2MzYTVhNSIsImlhdCI6IjEvNC8yMDIyIDM6MDE6MzIgUE0iLCJpZCI6IjM0NDg4IiwidXNlcl9uYW1lIjoiemltIiwiZnVsbF9uYW1lIjoiWklNIEFjYWRlbXkiLCJyb2xlX2lkIjoiMSIsInJvbGVfbmFtZSI6IlN1cGVyIEFkbWluIiwiZXhwIjoxNjY3NTc0MDkyLCJpc3MiOiJhcHAuemltLnZuIiwiYXVkIjoiYXBwLnppbS52biJ9.RPOVJ-aana5NoeyUEZ_WYI_i0f2gBqU0o1W68WO51Ro"},useGetOverallUnreadMessage=()=>{var b;const[e]=useUser(),r=e.roleId,{listMessageUnreadOverall:n}=useGetMessageUnread(),a=(n==null?void 0:n.another)?Object==null?void 0:Object.values(JSON==null?void 0:JSON.parse(n==null?void 0:n.another)):[],l=[n==null?void 0:n.zimians,n==null?void 0:n.student,n==null?void 0:n.following,n==null?void 0:n.open,(b=a==null?void 0:a.map(A=>parseInt(A,10)))==null?void 0:b.reduce((A,_)=>A+_,0)],c=l[1]+l[4],u=l[0]+l[1]+l[4],d=l==null?void 0:l.reduce((A,_)=>A+_,0),m=NOTI_STUDENT_ROLE.includes(r)?c:NOTI_EC_ADMIN_ROLE.includes(r)?d:u;return react.exports.useEffect(()=>{const A=document.getElementById("noti-chat");A&&getEnviroment("ZIM_BUILD_MODE")==="module"&&(A.innerHTML=`${m}`)},[m]),{overall:l,overallMyChats:m}},SearchBox=({searchString:e,setSearchString:r})=>jsxs(Box,{flex:1,maxH:"35px",borderWidth:1,borderColor:"#e4e4e4",rounded:"full",display:"flex",flexDirection:"row",alignItems:"center",px:2,overflow:"hidden",bg:"white",children:[jsx(Input$1,{variant:"unstyled",placeholder:"T\xECm ki\u1EBFm",p:2,value:e,onChange:a=>{a.target&&r(a.target.value)},color:"#373737"}),jsx(SearchIcon,{className:"h-5 w-5 text-gray-400"})]});var AdReportSort;(function(e){e.ClickRatio="clickRatio",e.Clicks="clicks",e.Displays="displays"})(AdReportSort||(AdReportSort={}));var AdvertisementShowIn;(function(e){e.Post="Post",e.Product="Product"})(AdvertisementShowIn||(AdvertisementShowIn={}));var AdvertisementSortBy;(function(e){e.Clicks="clicks",e.CreatedAt="createdAt",e.UpdatedAt="updatedAt"})(AdvertisementSortBy||(AdvertisementSortBy={}));var AdvertisementStatus;(function(e){e.Draft="draft",e.Publish="publish"})(AdvertisementStatus||(AdvertisementStatus={}));var AdvertisementType;(function(e){e.DesktopBannerXs="desktopBannerXS",e.FreeStyle="freeStyle",e.LargeRectangle="largeRectangle",e.Leaderboard="leaderboard",e.LeaderboardMd="leaderboardMD",e.MediumRectangle="mediumRectangle",e.MobileBanner="mobileBanner",e.MobileBannerMd="mobileBannerMD",e.MobileSquare="mobileSquare",e.MobileSquareSm="mobileSquareSM",e.Sidebar="sidebar",e.SidebarXl="sidebarXL"})(AdvertisementType||(AdvertisementType={}));var ApproveOrRejectPostStatus;(function(e){e.Approved="approved",e.Rejected="rejected"})(ApproveOrRejectPostStatus||(ApproveOrRejectPostStatus={}));var AttachmentType;(function(e){e.File="file",e.Image="image",e.Video="video"})(AttachmentType||(AttachmentType={}));var CacheControlScope;(function(e){e.Private="PRIVATE",e.Public="PUBLIC"})(CacheControlScope||(CacheControlScope={}));var CheckoutProduct;(function(e){e.IeltsCorrection="IeltsCorrection",e.MockTest="MockTest",e.Post="Post",e.Product="Product",e.VideoCourse="VideoCourse"})(CheckoutProduct||(CheckoutProduct={}));var CommentType;(function(e){e.Post="Post",e.PracticeTestGroup="PracticeTestGroup",e.QaPost="QAPost"})(CommentType||(CommentType={}));var CommonStatusEnum;(function(e){e.Approved="approved",e.Draft="draft",e.PendingApproval="pendingApproval",e.Published="published",e.Rejected="rejected"})(CommonStatusEnum||(CommonStatusEnum={}));var CommonTypeAccessEnum;(function(e){e.Paid="paid",e.Trial="trial"})(CommonTypeAccessEnum||(CommonTypeAccessEnum={}));var ConfigCourseFeeTypeEnum;(function(e){e.ForAll="forAll",e.ForMonth="forMonth",e.ForSubcat="forSubcat"})(ConfigCourseFeeTypeEnum||(ConfigCourseFeeTypeEnum={}));var ConversationGroup;(function(e){e.Customer="customer",e.Student="student",e.Zimians="zimians"})(ConversationGroup||(ConversationGroup={}));var ConversationStatus;(function(e){e.Done="done",e.Following="following",e.Open="open"})(ConversationStatus||(ConversationStatus={}));var CouponOrderBy;(function(e){e.CreatedAt="createdAt",e.UpdatedAt="updatedAt"})(CouponOrderBy||(CouponOrderBy={}));var CouponStatus;(function(e){e.Draft="draft",e.Publish="publish"})(CouponStatus||(CouponStatus={}));var CouponType;(function(e){e.Fixed="fixed",e.Percentage="percentage"})(CouponType||(CouponType={}));var CourseOrderByEnum;(function(e){e.Fee="fee",e.Id="id",e.ProgramType="program_type",e.StartDate="start_date",e.Status="status"})(CourseOrderByEnum||(CourseOrderByEnum={}));var CourseStatusEnum;(function(e){e.All="all",e.Closed="closed",e.Incoming="incoming",e.Ongoing="ongoing"})(CourseStatusEnum||(CourseStatusEnum={}));var CrawlReviewType;(function(e){e.MockTest="MockTest",e.Other="Other"})(CrawlReviewType||(CrawlReviewType={}));var DateViewMode;(function(e){e.Daily="daily",e.Monthly="monthly",e.Yearly="yearly"})(DateViewMode||(DateViewMode={}));var EmployeeInfoSort;(function(e){e.CreatedAt="createdAt",e.Priority="priority",e.UpdatedAt="updatedAt"})(EmployeeInfoSort||(EmployeeInfoSort={}));var EnumBaseStatus;(function(e){e.Active="active",e.Deactivated="deactivated"})(EnumBaseStatus||(EnumBaseStatus={}));var EnumCertificateRefType;(function(e){e.MockTestOrder="MockTestOrder"})(EnumCertificateRefType||(EnumCertificateRefType={}));var EnumConfigFunctionType;(function(e){e.Function="function"})(EnumConfigFunctionType||(EnumConfigFunctionType={}));var EnumConfigSystemNames;(function(e){e.PercentageForFeeByMonth="PercentageForFeeByMonth"})(EnumConfigSystemNames||(EnumConfigSystemNames={}));var EnumCouponProductType;(function(e){e.Course="course",e.CoursePlan="coursePlan",e.Program="program"})(EnumCouponProductType||(EnumCouponProductType={}));var EnumCouponProductTypes;(function(e){e.Course="course",e.CoursePlan="coursePlan",e.Program="program"})(EnumCouponProductTypes||(EnumCouponProductTypes={}));var EnumCouponStatusTypes;(function(e){e.Active="active",e.Deactivated="deactivated"})(EnumCouponStatusTypes||(EnumCouponStatusTypes={}));var EnumCouponTypes;(function(e){e.Fixed="fixed",e.Percentage="percentage"})(EnumCouponTypes||(EnumCouponTypes={}));var EnumCoursePlanerTypeEnum;(function(e){e.Course="Course",e.CoursePlan="CoursePlan"})(EnumCoursePlanerTypeEnum||(EnumCoursePlanerTypeEnum={}));var EnumCourseTypeEnum;(function(e){e.All="All",e.ClassRoom="ClassRoom",e.Online="Online"})(EnumCourseTypeEnum||(EnumCourseTypeEnum={}));var EnumCustomerLearningStatus;(function(e){e.All="all",e.Done="done",e.Registed="registed",e.Unregistration="unregistration"})(EnumCustomerLearningStatus||(EnumCustomerLearningStatus={}));var EnumDayOfWeek;(function(e){e.Fri="Fri",e.Mon="Mon",e.Sat="Sat",e.Sun="Sun",e.Thu="Thu",e.Tue="Tue",e.Wed="Wed"})(EnumDayOfWeek||(EnumDayOfWeek={}));var EnumHistoryChangeStudentSupporterStatusType;(function(e){e.Active="active",e.Deactivated="deactivated"})(EnumHistoryChangeStudentSupporterStatusType||(EnumHistoryChangeStudentSupporterStatusType={}));var EnumInvoiceRefundStatus;(function(e){e.All="All",e.Accepted="accepted",e.Confirmed="confirmed",e.Pending="pending",e.Rejected="rejected"})(EnumInvoiceRefundStatus||(EnumInvoiceRefundStatus={}));var EnumInvoiceStatus;(function(e){e.All="all",e.Paid="paid",e.Refund="refund"})(EnumInvoiceStatus||(EnumInvoiceStatus={}));var EnumLevelStatus;(function(e){e.Active="active",e.Deactivated="deactivated",e.Deleted="deleted",e.Inactive="inactive"})(EnumLevelStatus||(EnumLevelStatus={}));var EnumMockTestStatus;(function(e){e.All="all",e.Finished="finished",e.Incoming="incoming",e.Marked="marked",e.Ongoing="ongoing"})(EnumMockTestStatus||(EnumMockTestStatus={}));var EnumMockTestSubjectType;(function(e){e.Listening="Listening",e.Reading="Reading",e.Speaking="Speaking",e.Writing="Writing"})(EnumMockTestSubjectType||(EnumMockTestSubjectType={}));var EnumMockTestType;(function(e){e.Offline="offline",e.Online="online"})(EnumMockTestType||(EnumMockTestType={}));var EnumModule;(function(e){e.Academic="Academic",e.General="General"})(EnumModule||(EnumModule={}));var EnumOrder;(function(e){e.Asc="asc",e.Desc="desc"})(EnumOrder||(EnumOrder={}));var EnumPaymentType;(function(e){e.Atm="atm",e.Banking="banking",e.Cost="cost",e.Online="online",e.Other="other"})(EnumPaymentType||(EnumPaymentType={}));var EnumProcessRequestType;(function(e){e.Accept="accept",e.Reject="reject"})(EnumProcessRequestType||(EnumProcessRequestType={}));var EnumProvider;(function(e){e.Facebook="facebook",e.Google="google"})(EnumProvider||(EnumProvider={}));var EnumRoleType;(function(e){e.Am="AM",e.Admin="Admin",e.Bm="BM",e.Bp="BP",e.Cc="CC",e.Cs="CS",e.Ec="EC",e.Editor="Editor",e.Gv="GV",e.Guest="Guest",e.Hv="HV",e.HVu="HVu",e.Kt="KT",e.Kth="KTH",e.Mk="MK",e.RD="R_D"})(EnumRoleType||(EnumRoleType={}));var EnumShiftsType$1;(function(e){e.Instructor="INSTRUCTOR",e.MockTestOffline="MOCK_TEST_OFFLINE",e.MockTestOnline="MOCK_TEST_ONLINE",e.Other="OTHER",e.PlacementTest="PLACEMENT_TEST",e.SelfStudyRoom="SELF_STUDY_ROOM",e.TeacherOff="TEACHER_OFF"})(EnumShiftsType$1||(EnumShiftsType$1={}));var EnumSkill;(function(e){e.Listening="Listening",e.Reading="Reading"})(EnumSkill||(EnumSkill={}));var EnumStatusAccount;(function(e){e.Active="active",e.Deactivated="deactivated",e.Notactive="notactive"})(EnumStatusAccount||(EnumStatusAccount={}));var EnumStatusGrapNet;(function(e){e.Active="active",e.Deactivated="deactivated"})(EnumStatusGrapNet||(EnumStatusGrapNet={}));var EnumStatusScheduleType;(function(e){e.Active="ACTIVE",e.All="ALL",e.Approval="APPROVAL",e.Cancel="CANCEL",e.Deactivated="DEACTIVATED",e.Reject="REJECT",e.Reopened="REOPENED",e.RequestedCancel="REQUESTED_CANCEL"})(EnumStatusScheduleType||(EnumStatusScheduleType={}));var EnumTypeOfEducation;(function(e){e.Academic="Academic",e.General="General"})(EnumTypeOfEducation||(EnumTypeOfEducation={}));var EnumTypeSentEmail;(function(e){e.All="all",e.One="one"})(EnumTypeSentEmail||(EnumTypeSentEmail={}));var ExistFieldCheck;(function(e){e.Email="email",e.Phone="phone",e.Username="username"})(ExistFieldCheck||(ExistFieldCheck={}));var FacebookPostOrderBy;(function(e){e.CreatedAt="createdAt",e.InteractiveScore="interactiveScore",e.NumOfComments="numOfComments",e.NumOfLikes="numOfLikes",e.NumOfShares="numOfShares"})(FacebookPostOrderBy||(FacebookPostOrderBy={}));var ForumRoleEnum;(function(e){e.Admin="admin",e.Manager="manager",e.Mod="mod"})(ForumRoleEnum||(ForumRoleEnum={}));var IeltsPracticeTestGroupOrderBy;(function(e){e.CreatedAt="createdAt",e.Title="title",e.UpdatedAt="updatedAt"})(IeltsPracticeTestGroupOrderBy||(IeltsPracticeTestGroupOrderBy={}));var IeltsPracticeTestOrderBy;(function(e){e.CreatedAt="createdAt",e.Title="title",e.UpdatedAt="updatedAt"})(IeltsPracticeTestOrderBy||(IeltsPracticeTestOrderBy={}));var IeltsBcCoupons;(function(e){e.CouponsBook="COUPONS_BOOK",e.CouponsIeltsCorrect="COUPONS_IELTS_CORRECT",e.CouponsMocktest="COUPONS_MOCKTEST"})(IeltsBcCoupons||(IeltsBcCoupons={}));var IeltsPracticeQuestionType;(function(e){e.GapsFilling="gapsFilling",e.IdentifyInformation="identifyInformation",e.MapLabelling="mapLabelling",e.Matching="matching",e.MultipleChoice="multipleChoice",e.MultipleQuestions="multipleQuestions",e.SingleChoice="singleChoice",e.Speaking="speaking",e.Writing="writing"})(IeltsPracticeQuestionType||(IeltsPracticeQuestionType={}));var IeltsPracticeTestFormat;(function(e){e.Exercise="Exercise",e.MockTest="MockTest",e.PracticeTest="PracticeTest"})(IeltsPracticeTestFormat||(IeltsPracticeTestFormat={}));var IeltsPracticeTestGroupStatus;(function(e){e.Draft="draft",e.Publish="publish"})(IeltsPracticeTestGroupStatus||(IeltsPracticeTestGroupStatus={}));var IeltsPracticeTestLevel;(function(e){e.Academic="Academic",e.Exercise="Exercise",e.General="General"})(IeltsPracticeTestLevel||(IeltsPracticeTestLevel={}));var IeltsPracticeTestSessionRefKey;(function(e){e.MockTest="MockTest"})(IeltsPracticeTestSessionRefKey||(IeltsPracticeTestSessionRefKey={}));var IeltsPracticeTestStatus;(function(e){e.Draft="draft",e.Publish="publish"})(IeltsPracticeTestStatus||(IeltsPracticeTestStatus={}));var IeltsPracticeTestType;(function(e){e.Listening="Listening",e.Other="Other",e.Reading="Reading",e.Speaking="Speaking",e.Writing="Writing"})(IeltsPracticeTestType||(IeltsPracticeTestType={}));var InPostPosition;(function(e){e.After="after",e.Before="before"})(InPostPosition||(InPostPosition={}));var InPostTag;(function(e){e.Div="div",e.H1="h1",e.H2="h2",e.H3="h3",e.H4="h4",e.H5="h5",e.H6="h6",e.P="p",e.Span="span"})(InPostTag||(InPostTag={}));var JobStatus;(function(e){e.Crawling="crawling",e.Pending="pending",e.Waiting="waiting"})(JobStatus||(JobStatus={}));var JobType;(function(e){e.Group="group",e.Page="page"})(JobType||(JobType={}));var MediaEnumType;(function(e){e.File="file",e.Image="image",e.Video="video"})(MediaEnumType||(MediaEnumType={}));var MediaSortBy;(function(e){e.CreatedAt="createdAt",e.UpdatedAt="updatedAt"})(MediaSortBy||(MediaSortBy={}));var MediaType;(function(e){e.File="file",e.Image="image",e.Video="video"})(MediaType||(MediaType={}));var MediaVisibility;(function(e){e.Private="private",e.Public="public"})(MediaVisibility||(MediaVisibility={}));var MessageType;(function(e){e.Call="call",e.Response="response",e.Update="update"})(MessageType||(MessageType={}));var MiniGameStatusEnum;(function(e){e.Draft="draft",e.Published="published"})(MiniGameStatusEnum||(MiniGameStatusEnum={}));var MockTestOrderBy;(function(e){e.CreateDate="create_date",e.Id="id",e.UpdateDate="update_date"})(MockTestOrderBy||(MockTestOrderBy={}));var MockTestStatusEnum;(function(e){e.All="all",e.Cancelled="cancelled",e.Confirmed="confirmed",e.Reserved="reserved"})(MockTestStatusEnum||(MockTestStatusEnum={}));var NotificationType;(function(e){e.CommentAdded="commentAdded",e.CommentReplied="commentReplied",e.NewPost="newPost",e.PostApproved="postApproved",e.PostPublished="postPublished",e.PostRejected="postRejected",e.PostRequestApproval="postRequestApproval"})(NotificationType||(NotificationType={}));var OrderActionInput;(function(e){e.Cancel="cancel",e.Confirm="confirm"})(OrderActionInput||(OrderActionInput={}));var OrderDirection;(function(e){e.Asc="asc",e.Desc="desc"})(OrderDirection||(OrderDirection={}));var OrderOrderBy;(function(e){e.CreatedAt="createdAt",e.OrderId="orderId",e.UpdatedAt="updatedAt"})(OrderOrderBy||(OrderOrderBy={}));var OrderReportByProductSort;(function(e){e.PurchaseCount="purchaseCount",e.Total="total"})(OrderReportByProductSort||(OrderReportByProductSort={}));var OrderStatus;(function(e){e.Cancelled="cancelled",e.Confirmed="confirmed",e.Draft="draft",e.Dropped="dropped",e.Finished="finished",e.OnShipping="onShipping",e.Pending="pending",e.PendingShipping="pendingShipping",e.Picked="picked",e.PreparingFiles="preparingFiles"})(OrderStatus||(OrderStatus={}));var PageIndexStatus;(function(e){e.Deleted="DELETED",e.Updated="UPDATED"})(PageIndexStatus||(PageIndexStatus={}));var PaymentMethod;(function(e){e.AtStore="atStore",e.BankTransfer="bankTransfer",e.Cod="cod",e.Momo="momo",e.Onepay="onepay"})(PaymentMethod||(PaymentMethod={}));var PaymentStatus;(function(e){e.Paid="paid",e.Pending="pending",e.Refund="refund"})(PaymentStatus||(PaymentStatus={}));var PaymentStatusEnum;(function(e){e.Cancelled="cancelled",e.Confirmed="confirmed",e.Pending="pending"})(PaymentStatusEnum||(PaymentStatusEnum={}));var PostByUserSort;(function(e){e.PostCount="postCount",e.ReviewsAverage="reviewsAverage",e.ViewCount="viewCount"})(PostByUserSort||(PostByUserSort={}));var PostCategoryOrderBy;(function(e){e.CreatedAt="createdAt",e.Usage="usage"})(PostCategoryOrderBy||(PostCategoryOrderBy={}));var PostGroupOrderBy;(function(e){e.CreatedAt="createdAt",e.Title="title",e.UpdatedAt="updatedAt"})(PostGroupOrderBy||(PostGroupOrderBy={}));var PostOrderBy;(function(e){e.CreatedAt="createdAt",e.NumOfComments="numOfComments",e.NumOfContextualLink="numOfContextualLink",e.PublishedAt="publishedAt",e.Title="title",e.UpdatedAt="updatedAt",e.Views="views"})(PostOrderBy||(PostOrderBy={}));var PostStatus;(function(e){e.Approved="approved",e.Deleted="deleted",e.Draft="draft",e.PendingApproval="pendingApproval",e.Published="published",e.Rejected="rejected"})(PostStatus||(PostStatus={}));var ProductAttributeOrderBy;(function(e){e.CreatedAt="createdAt",e.Name="name",e.UpdatedAt="updatedAt",e.Usage="usage"})(ProductAttributeOrderBy||(ProductAttributeOrderBy={}));var ProductAttributeType;(function(e){e.Media="media",e.Number="number",e.Select="select",e.String="string"})(ProductAttributeType||(ProductAttributeType={}));var ProductOrderBy;(function(e){e.CreatedAt="createdAt",e.Name="name",e.NumOfContextualLink="numOfContextualLink",e.Price="price",e.UpdatedAt="updatedAt"})(ProductOrderBy||(ProductOrderBy={}));var ProductStatus;(function(e){e.Deleted="deleted",e.Draft="draft",e.Publish="publish"})(ProductStatus||(ProductStatus={}));var ProductType;(function(e){e.SimpleProduct="SimpleProduct",e.VariableProduct="VariableProduct"})(ProductType||(ProductType={}));var QaPostType;(function(e){e.Question="QUESTION",e.Voting="VOTING"})(QaPostType||(QaPostType={}));var RawDataStatusEnum;(function(e){e.All="all",e.Appoiment="appoiment",e.Arrived="arrived",e.Cancelled="cancelled",e.ChangeEc="changeEc",e.Following="following",e.Pending="pending",e.Registered="registered"})(RawDataStatusEnum||(RawDataStatusEnum={}));var RecipientType;(function(e){e.Conversation="conversation",e.Support="support",e.User="user"})(RecipientType||(RecipientType={}));var RecruitmentPostReplySort;(function(e){e.CreatedAt="createdAt",e.UpdatedAt="updatedAt"})(RecruitmentPostReplySort||(RecruitmentPostReplySort={}));var RecruitmentPostReplyStatus;(function(e){e.Pending="pending",e.Processed="processed"})(RecruitmentPostReplyStatus||(RecruitmentPostReplyStatus={}));var RecruitmentPostSalaryType;(function(e){e.Hidden="hidden",e.Public="public"})(RecruitmentPostSalaryType||(RecruitmentPostSalaryType={}));var RecruitmentPostSort;(function(e){e.CreatedAt="createdAt",e.UpdatedAt="updatedAt"})(RecruitmentPostSort||(RecruitmentPostSort={}));var RecruitmentPostStatus;(function(e){e.Draft="draft",e.Publish="publish"})(RecruitmentPostStatus||(RecruitmentPostStatus={}));var RequestStudentToCourseStatusEnumeration;(function(e){e.Accepted="accepted",e.Pending="pending",e.Rejected="rejected"})(RequestStudentToCourseStatusEnumeration||(RequestStudentToCourseStatusEnumeration={}));var RequestStudentToCourseTypeEnumeration;(function(e){e.JoinCourse="joinCourse",e.LeftCourse="leftCourse",e.ReEnroll="reEnroll"})(RequestStudentToCourseTypeEnumeration||(RequestStudentToCourseTypeEnumeration={}));var ReviewOrderBy;(function(e){e.CreatedAt="createdAt",e.Rating="rating"})(ReviewOrderBy||(ReviewOrderBy={}));var ReviewType;(function(e){e.Level="Level",e.MockTest="MockTest",e.Other="Other",e.Post="Post",e.Product="Product",e.Writing="Writing"})(ReviewType||(ReviewType={}));var SenderAction;(function(e){e.MarkSeen="markSeen",e.TypingOff="typingOff",e.TypingOn="typingOn"})(SenderAction||(SenderAction={}));var ShippingMethod;(function(e){e.Delivery="delivery",e.StorePickup="storePickup",e.Virtual="virtual"})(ShippingMethod||(ShippingMethod={}));var SlugType;(function(e){e.IeltsPracticeTestGroup="IeltsPracticeTestGroup",e.Post="Post",e.PostGroup="PostGroup",e.Product="Product",e.RecruitmentPost="RecruitmentPost",e.Tag="Tag"})(SlugType||(SlugType={}));var SourceEnum;(function(e){e.App="App",e.Zim="Zim",e.ZimMobile="ZimMobile"})(SourceEnum||(SourceEnum={}));var Status;(function(e){e.Active="active",e.All="all",e.Deactivated="deactivated",e.Inactive="inactive"})(Status||(Status={}));var StatusAccount;(function(e){e.Active="active",e.Deactivated="deactivated",e.Notactive="notactive"})(StatusAccount||(StatusAccount={}));var StatusEnum;(function(e){e.Active="active",e.Deactivated="deactivated",e.Inactive="inactive"})(StatusEnum||(StatusEnum={}));var StatusExerciseGrapNet;(function(e){e.Active="active",e.Draft="draft"})(StatusExerciseGrapNet||(StatusExerciseGrapNet={}));var StockStatus;(function(e){e.InStock="inStock",e.OutOfStock="outOfStock"})(StockStatus||(StockStatus={}));var SubfolderS3Enum;(function(e){e.Exercise="Exercise",e.ExerciseAnswer="ExerciseAnswer",e.ExerciseLearningMaterials="ExerciseLearningMaterials",e.ExerciseQuestion="ExerciseQuestion"})(SubfolderS3Enum||(SubfolderS3Enum={}));var SurveyQuestionEnum;(function(e){e.MultipleChoice="multipleChoice",e.SingleChoice="singleChoice",e.Text="text"})(SurveyQuestionEnum||(SurveyQuestionEnum={}));var TagAssignJobStatus;(function(e){e.Finished="finished",e.Pending="pending",e.Scanning="scanning"})(TagAssignJobStatus||(TagAssignJobStatus={}));var TagObject;(function(e){e.Post="Post",e.Product="Product"})(TagObject||(TagObject={}));var TagSort;(function(e){e.CreatedAt="createdAt",e.Name="name",e.PostUsage="postUsage",e.ProductUsage="productUsage",e.UpdatedAt="updatedAt",e.Usage="usage"})(TagSort||(TagSort={}));var TicketFilterTypeEnum;(function(e){e.Received="received",e.Send="send"})(TicketFilterTypeEnum||(TicketFilterTypeEnum={}));var TicketReactionTypeEnum;(function(e){e.Love="Love",e.NotOk="NotOk",e.Ok="Ok",e.Report="Report"})(TicketReactionTypeEnum||(TicketReactionTypeEnum={}));var TicketStatusTypeEnum;(function(e){e.Closed="Closed",e.Deleted="Deleted",e.Processing="Processing",e.Waiting="Waiting"})(TicketStatusTypeEnum||(TicketStatusTypeEnum={}));var TodoScheduleTypeEnum;(function(e){e.Apointment="Apointment",e.FinalTest="FinalTest",e.ToDo="ToDo"})(TodoScheduleTypeEnum||(TodoScheduleTypeEnum={}));var ToeicMockTestStatusEnumType;(function(e){e.Draft="draft",e.Published="published"})(ToeicMockTestStatusEnumType||(ToeicMockTestStatusEnumType={}));var ToeicModuleEnumType;(function(e){e.Listening="listening",e.Reading="reading"})(ToeicModuleEnumType||(ToeicModuleEnumType={}));var VideoCourseQuestionEnum;(function(e){e.FillInTheBlank="fillInTheBlank",e.MultipleChoice="multipleChoice",e.SingleChoice="singleChoice"})(VideoCourseQuestionEnum||(VideoCourseQuestionEnum={}));const EMPTY_ERROR=e=>`${e} is required.`,EMPTY_GUID="00000000-0000-0000-0000-000000000000",DATE_OF_WEEKS={0:{short:"CN",long:"CN",full:"Ch\u1EE7 nh\u1EADt"},1:{short:"2",long:"T2",full:"Th\u1EE9 2"},2:{short:"3",long:"T3",full:"Th\u1EE9 3"},3:{short:"4",long:"T4",full:"Th\u1EE9 4"},4:{short:"5",long:"T5",full:"Th\u1EE9 5"},5:{short:"6",long:"T6",full:"Th\u1EE9 6"},6:{short:"7",long:"T7",full:"Th\u1EE9 7"}},SKILLS={LISTENING:{short:"L",long:"Listening"},READING:{short:"R",long:"Reading"},SPEAKING:{short:"S",long:"Speaking"},WRITING:{short:"W",long:"Writing"},"WRITING TASK 1":{short:"WT1",long:"Writing Task 1"},"WRITING TASK 2":{short:"WT2",long:"Writing Task 2"},"BASIC GRAMMAR":{short:"BG",long:"Basic Grammar"},"FINAL TEST":{short:"FT",long:"Final Test"}},COURSE_STATUSES={ALL:"all",INCOMING:"incoming",ONGOING:"ongoing",CLOSED:"closed"},OPEN_STATUSES={ALL:{label:"All",colorScheme:"gray"},INCOMING:{label:"Incoming",colorScheme:"green"},ONGOING:{label:"Ongoing",colorScheme:"orange"},FINISHED:{label:"Finished",colorScheme:"red"}},courseStatusOptions=[{label:"Incoming",value:COURSE_STATUSES.INCOMING},{label:"Ongoing",value:COURSE_STATUSES.ONGOING},{label:"Closed",value:COURSE_STATUSES.CLOSED}];var ETicketStatus$1;(function(e){e.PROCESSING="Processing",e.CLOSED="Closed",e.WAITING="Waiting",e.DELETED="Deleted",e.LONGTIME="Longtime"})(ETicketStatus$1||(ETicketStatus$1={}));var ETicketType;(function(e){e.send="send",e.received="received"})(ETicketType||(ETicketType={}));var UserStatusEnum;(function(e){e.active="active",e.deactivated="deactivated"})(UserStatusEnum||(UserStatusEnum={}));const TICKET_STATUS={All:{label:"All",value:"All",colorScheme:"gray"},Processing:{label:"Processing",value:"Processing",colorScheme:"green"},Waiting:{label:"Waiting",value:"Waiting",colorScheme:"orange"},Closed:{label:"Closed",value:"Closed",colorScheme:"red"},Deleted:{label:"Deleted",value:"Deleted",colorScheme:"black"}},TICKET_STATUS_LOWERCASE={all:{label:"All",value:"All",colorScheme:"gray"},processing:{label:"Processing",value:"Processing",colorScheme:"green"},waiting:{label:"Waiting",value:"Waiting",colorScheme:"orange"},closed:{label:"Closed",value:"Closed",colorScheme:"red"},deleted:{label:"Deleted",value:"Deleted",colorScheme:"black"},longtime:{label:"Longtime",value:"Longtime",colorScheme:"gray"}},LEAD_STATUS={0:{label:"Ch\u01B0a x\u1EED l\xFD",value:0,colorScheme:"red",filterValue:"pending"},1:{label:"\u0110ang theo d\xF5i",value:1,colorScheme:"orange",filterValue:"following"},2:{label:"\u0110\xE3 h\u1EB9n test",value:2,colorScheme:"blue",filterValue:"appoiment"},4:{label:"\u0110\xE3 \u0111\u1EBFn test",value:4,colorScheme:"teal",filterValue:"called"},6:{label:"\u0110\xE3 \u0111\u0103ng k\xFD",value:6,colorScheme:"green",filterValue:"registered"},3:{label:"\u0110\xE3 h\u1EE7y",value:3,colorScheme:"gray",filterValue:"cancelled"},5:{label:"\u0110\xE3 \u0111\u1ED5i EC",value:5,colorScheme:"purple",filterValue:"changeEc"}},ticketStatusOptions=[{label:"Processing",value:ETicketStatus$1.PROCESSING},{label:"Waiting",value:ETicketStatus$1.WAITING},{label:"Closed",value:ETicketStatus$1.CLOSED},{label:"Deleted",value:ETicketStatus$1.DELETED}],ticketFilterTypeOptions=[{label:"\u0110\xE3 g\u1EEDi",value:ETicketType.send},{label:"\u0110\xE3 nh\u1EADn",value:ETicketType.received}],userStatusOptions=[{label:"Active",value:UserStatusEnum.active},{label:"Deactivated",value:UserStatusEnum.deactivated}];var EnumShiftsType;(function(e){e.Other="Other",e.TeacherOff="TeacherOff",e.MOCK_TEST_ONLINE="MOCK_TEST_ONLINE",e.MOCK_TEST_OFFLINE="MOCK_TEST_OFFLINE",e.OTHER="OTHER",e.TEACHER_OFF="TEACHER_OFF"})(EnumShiftsType||(EnumShiftsType={}));const DEFAULT_AVATAR_URL="https://app.zim.vn/app-assets/zimv2/images/default-logo1.png",MOCK_TEST_STATUS={inactive:{label:"inactive",value:"inactive",colorScheme:"gray"},active:{label:"active",value:"active",colorScheme:"green"},deactivated:{label:"deactivated",value:"deactivated",colorScheme:"red"}},MockTestStatusOptions=[{label:"Active",value:"active"},{label:"Deactivated",value:"deactivated"}],MockTestSubjectsType={"Listening-Reading-Writing":{long:"Listening-Reading-Writing",short:"L-R-W"},Speaking:{long:"Speaking",short:"Speaking"}};var EOrderStatus;(function(e){e.confirmed="confirmed",e.cancel="cancel",e.pending="pending"})(EOrderStatus||(EOrderStatus={}));EOrderStatus.confirmed,EOrderStatus.cancel,EOrderStatus.pending;EOrderStatus.confirmed,EOrderStatus.cancel,EOrderStatus.pending;const ExerciseStatus={active:{label:"Active",value:StatusExerciseGrapNet.Active,colorScheme:"green"},draft:{label:"Draft",value:StatusExerciseGrapNet.Draft,colorScheme:"gray"}},ExerciseOptions=[{label:ExerciseStatus.draft.label,value:StatusExerciseGrapNet.Draft},{label:ExerciseStatus.active.label,value:StatusExerciseGrapNet.Active}],tableDisplayRowsOptions=[{label:"5 rows",value:5},{label:"10 rows",value:10},{label:"15 rows",value:15},{label:"20 rows",value:20},{label:"25 rows",value:25},{label:"30 rows",value:30}],MyAvatar=react.exports.memo(a=>{var l=a,{src:e="",name:r=""}=l,n=Te(l,["src","name"]);return jsx(Avatar,N({src:e===DEFAULT_AVATAR_URL?"":e,name:r,zIndex:10},n))}),MyInfoSection=({searchString:e,setSearchString:r})=>{const[n]=useUser();return jsxs(VStack,{w:"100%",p:{base:3},position:"sticky",top:0,zIndex:50,bg:useColorModeValue("white","#10172a"),alignItems:"stretch",children:[jsxs(Box,{w:"100%",display:"flex",alignItems:"start",maxW:{base:"full"},className:" space-x-2.5",bg:useColorModeValue("white",""),p:{base:2,md:0},rounded:"lg",children:[jsx(MyAvatar,{boxSize:"2em",src:n==null?void 0:n.avatar,name:n==null?void 0:n.fullName}),jsxs(Box,{flex:1,children:[jsx(Text,{noOfLines:1,fontSize:14,lineHeight:"18px",fontWeight:"medium",color:useColorModeValue("gray.800","slate.300"),children:n==null?void 0:n.fullName}),jsx(Text,{display:{base:"block"},noOfLines:1,fontSize:14,fontWeight:500,lineHeight:"24px",color:"gray.400",children:n==null?void 0:n.role})]})]}),jsx(HStack,{children:jsx(SearchBox,{searchString:e,setSearchString:r})})]})},AvatarComponent$1=t.memo(function({avatarUrl:r,visibleLastOnline:n,lastOnline:a,username:l}){return jsx(MyAvatar,{zIndex:"10",boxSize:"2em",src:r,name:l,children:n&&jsx(AvatarBadge,{boxSize:"15px",bg:a==="online"?"green.500":"gray.300"})})}),UserItemInfo=react.exports.memo(e=>{const C=e,{onClick:r,avatarUrl:n,username:a,usernameClassName:l,positionName:c,lastOnline:u,visibleLastOnline:d=!0,isOwner:m,channel:b,id:A}=C,_=Te(C,["onClick","avatarUrl","username","usernameClassName","positionName","lastOnline","visibleLastOnline","isOwner","channel","id"]),T=react.exports.useCallback(()=>{r(A,b)},[b,A]);return jsxs(Box,H(N({w:"100%",display:"flex",alignItems:"start",cursor:"pointer",onClick:T,maxW:{base:"full"},className:" space-x-2.5",bg:useColorModeValue("white",""),p:3,rounded:"lg"},_),{children:[jsx(AvatarComponent$1,{avatarUrl:n,lastOnline:u,visibleLastOnline:d,username:a}),jsxs(Box,{flex:1,children:[jsxs(HStack,{children:[jsx(Text,{noOfLines:1,fontSize:14,lineHeight:"18px",fontWeight:"medium",color:useColorModeValue("gray.800","slate.300"),className:`${l}`,children:a}),m&&jsx(FcKey,{})]}),jsx(Text,{noOfLines:1,fontSize:12,lineHeight:"24px",color:"gray.400",children:c})]})]}))}),ConversationLoading=()=>jsxs(HStack,{px:3,py:2,borderBottomWidth:1,bg:useColorModeValue("white","#1e293b50"),children:[jsx(SkeletonCircle,{size:"10"}),jsx(SkeletonText,{flex:1,noOfLines:2,spacing:"2",skeletonHeight:5})]}),ChannelItem=react.exports.memo(({conversations:e,onClick:r,isLoading:n,canLoadMore:a,onLoadMore:l,loading:c})=>{const u=react.exports.useMemo(()=>e==null?void 0:e.map((d,m)=>{const b=d==null?void 0:d.user,A=(b==null?void 0:b.role_id)===5?"student":(b==null?void 0:b.role_id)===14?"customer":"zimians";return c?jsx(ConversationLoading,{},`loading_${m}`):react.exports.createElement(UserItemInfo,H(N({},b),{lastOnline:b==null?void 0:b.status_online,avatarUrl:b==null?void 0:b.avatar,username:b==null?void 0:b.full_name,positionName:b==null?void 0:b.role_name,key:m,onClick:r,channel:A}))}),[e]);return jsxs(VStack,{alignItems:"stretch",children:[u,a&&jsx(Button$1,{isLoading:n,loadingText:"\u0110ang t\u1EA3i th\xEAm",onClick:l,w:"100%",rounded:"none",borderWidth:"1px 0px 0px 0px",bg:useColorModeValue("white","slate.600"),fontSize:14,fontWeight:400,justifyContent:"center",children:"Xem th\xEAm"})]})}),AccordionContent=({title:e,onClick:r,children:n,overallMessageUnRead:a})=>jsxs(AccordionItem,{borderBottom:1,borderColor:"",children:[jsxs(HStack,{position:"relative",spacing:0,pl:4,_hover:{backgroundColor:useColorModeValue("#f0f0f0","slate.600")},children:[jsx(Box,{onClick:r,display:"flex",justifyContent:"start",flex:1,as:"button",children:jsx(Text,{color:useColorModeValue("#444","white"),fontSize:16,fontWeight:"medium",children:e})}),jsx(AccordionButton,{display:"flex",alignItems:"center",justifyContent:"center",w:10,children:jsx(AccordionIcon,{})}),a&&jsx(NotiCount,{count:a})]}),jsx(AccordionPanel,{p:0,children:n})]}),MyAccordion=react.exports.memo(({title:e="Accordion",children:r,onClick:n,overallMessageUnRead:a})=>jsx(Accordion,{defaultIndex:[0],allowMultiple:!0,children:jsx(AccordionContent,{overallMessageUnRead:a,title:e,onClick:n,children:r})}));function mergeDeep(...e){const r=n=>n&&typeof n=="object";return e.reduce((n,a)=>(Object.keys(a).forEach(l=>{const c=n[l],u=a[l];Array.isArray(c)&&Array.isArray(u)?n[l]=uniqBy_1(c.concat(...u),"id"):r(c)&&r(u)?n[l]=mergeDeep(c,u):n[l]=u}),n),{})}function mergeDeepWithoutId(...e){const r=n=>n&&typeof n=="object";return e.reduce((n,a)=>(Object.keys(a).forEach(l=>{const c=n[l],u=a[l];Array.isArray(c)&&Array.isArray(u)?n[l]=uniqBy_1(c.concat(...u),"user"):r(c)&&r(u)?n[l]=mergeDeepWithoutId(c,u):n[l]=u}),n),{})}const useLoadMore=({hasNextPage:e,fetchMore:r,variables:n={},isNormalLoadMore:a=!0})=>{const[l,c]=react.exports.useState(!1),u=a?mergeDeep:mergeDeepWithoutId,d=react.exports.useCallback(async()=>{l||!e||(await c(!0),await r({variables:n,updateQuery:(m,{fetchMoreResult:b})=>b?u(m,b):m}),await c(!1))},[l,e,c,r,n]);return react.exports.useMemo(()=>({onLoadMore:d,isLoadingMore:l}),[d,l])},useGetListAccountChat=({roles:e,searchString:r})=>{var _,T,C;const n=pickBy_1({roles:e,searchString:r,limit:10,page:1},S=>S),{data:a,loading:l,fetchMore:c}=useQuery(GET_LIST_ACCOUNT_CHAT,{variables:n}),u=(_=a==null?void 0:a.getAccountChatPagination)==null?void 0:_.docs,d=(T=a==null?void 0:a.getAccountChatPagination)==null?void 0:T.page,m=(C=a==null?void 0:a.getAccountChatPagination)==null?void 0:C.hasNextPage,{onLoadMore:b,isLoadingMore:A}=useLoadMore({variables:H(N({},n),{page:d+1}),fetchMore:c,hasNextPage:m,isNormalLoadMore:!1});return react.exports.useMemo(()=>({listAccount:u,loading:l,onLoadMore:b,hasNextPage:m,isLoadingMore:A}),[u,l,b,m,A])},SEND_MESSAGE=gql`
  mutation sendMessage(
    $recipient: RecipientInput!
    $message: MessageInput
    $senderAction: SenderAction
  ) @api(name: "chat") {
    sendMessage(
      recipient: $recipient
      message: $message
      senderAction: $senderAction
    ) {
      id
      createdAt
      updatedAt
      lastMessage {
        id
        text
        from
        to
        type
        attachments {
          type
          attachmentId
          attachment {
            id
            path
            fullUrl
            type
          }
        }
        loading @client
        error @client
        seenBy
        createdAt
        updatedAt
        deletedAt
        callPayload
      }
    }
  }
`,REMOVE_MESSAGE=gql`
  mutation removeMessage($id: ObjectID!) @api(name: "chat") {
    removeMessage(id: $id) {
      id
      name
      createdAt
      updatedAt
      isDeleteMessage
      lastMessage {
        id
        text
        from
        to
        type
        attachments {
          type
          attachmentId
          attachment {
            id
            path
            fullUrl
            type
          }
        }
        loading @client
        error @client
        seenBy
        createdAt
        updatedAt
        deletedAt
        callPayload
      }
    }
  }
`,CREATE_CONVERSATION=gql`
  mutation createConversation(
    $name: String
    $group: String
    $userIds: [String!]!
    $isChannel: Boolean
  ) @api(name: "chat") {
    createConversation(
      name: $name
      group: $group
      userIds: $userIds
      isChannel: $isChannel
    ) {
      id
      name
    }
  }
`,UPDATE_CONVERSATION=gql`
  mutation updateConversation(
    $name: String
    $id: ObjectID!
    $userIds: [String!]!
  ) @api(name: "chat") {
    updateConversation(name: $name, id: $id, userIds: $userIds) {
      id
      name
    }
  }
`,CREATE_NOTE=gql`
  mutation takeNoteForConversation(
    $conversationId: ObjectID!
    $content: String!
  ) @api(name: "chat") {
    takeNoteForConversation(
      conversationId: $conversationId
      content: $content
    ) {
      id
      content
      conversationId
      createdAt
      createdBy
    }
  }
`,REMOVE_MEMBER=gql`
  mutation removeMemberFromParticipant($userId: String!, $id: ObjectID!)
  @api(name: "chat") {
    removeMemberFromParticipant(userId: $userId, id: $id) {
      id
    }
  }
`,CREATE_TAG_FOR_CONVERSATION=gql`
  mutation createTagForConversation(
    $tagId: ObjectID
    $conversationId: ObjectID
    $name: String
    $color: String
  ) @api(name: "chat") {
    createTagForConversation(
      tagId: $tagId
      conversationId: $conversationId
      name: $name
      color: $color
    ) {
      id
      tags {
        id
        name
        color
      }
    }
  }
`,MARK_DONE_CONVERSATION=gql`
  mutation markDoneConversation($conversationId: ObjectID) @api(name: "chat") {
    markDoneConversation(id: $conversationId) {
      id
    }
  }
`,DELETE_TAG_IN_CONVERSATION=gql`
  mutation deleteTagInConversation(
    $conversationId: ObjectID!
    $tagId: ObjectID!
  ) @api(name: "chat") {
    deleteTagInConversation(conversationId: $conversationId, tagId: $tagId) {
      id
      tags {
        id
      }
    }
  }
`,CREATE_QA_POST=gql`
  mutation createQAPost($input: QAPostInput!, $userId: String)
  @api(name: "zim") {
    createQAPost(input: $input, userId: $userId) {
      id
      title
      content
    }
  }
`,POST_COMMENT=gql`
  mutation postComment($content: String!, $type: CommentType, $ref: ObjectID!)
  @api(name: "zim") {
    postComment(content: $content, type: $type, ref: $ref) {
      ... on PostComment {
        id
      }
      ... on QAPostComment {
        id
      }
    }
  }
`,SEND_REACT_MESSAGE=gql`
  mutation sendReactionMessage($messageId: ObjectID, $emoji: String)
  @api(name: "chat") {
    sendReactionMessage(messageId: $messageId, emoji: $emoji) {
      id
      emojis {
        userId
        emoji
      }
    }
  }
`,useGetWindowDimensions=()=>{const{innerWidth:e,innerHeight:r}=window,[n,a]=react.exports.useState(e),[l,c]=react.exports.useState(r),u=react.exports.useCallback(()=>{a(window.innerWidth),c(window.innerHeight)},[]);return useEventListener("resize",u),{width:n,height:l}},NavigateButton=({item:e,onClick:r,isActive:n})=>jsx(Box,{id:"js-toggle-profile",onClick:r,w:10,h:10,p:1,rounded:"full",bg:useColorModeValue(n?"blue.400":"gray.100",n?"blue.400":"#1e293b"),overflow:"hidden",display:"flex",alignItems:"center",justifyContent:"center",color:n?"white":"black",children:e.icon}),HeaderMobileChat=()=>{const e=useStore(n=>n.setTab),r=useStore(n=>n.tab);return jsx(HStack,{w:"full",bg:useColorModeValue("white","#10172a"),p:2,justifyContent:"flex-end",alignItems:"center",borderBottomWidth:1,spacing:6,children:[{icon:jsx(FiGrid,{}),key:"channel",onClick:()=>e("channel")},{icon:jsx(RiChat3Line,{}),key:"conversation",onClick:()=>e("conversation")}].map((n,a)=>{const l=n.key===r;return jsx(NavigateButton,{isActive:l,item:n,onClick:n.onClick},a)})})},timeDifferenceCalculator=(e,r=new Date)=>{var m,b;const n=Math.abs(Math.max(Date.parse(r),Date.parse(e))-Math.min(Date.parse(e),Date.parse(r))),a=1e3,l=60*a,c=60*l,u=(m=Math.floor(n/c))!=null?m:0,d=(b=Math.floor(n%c/l).toLocaleString("en-US",{minimumIntegerDigits:1}))!=null?b:"0";return u&&u<24?`${u} gi\u1EDD tr\u01B0\u1EDBc.`:u===0?`${d} ph\xFAt tr\u01B0\u1EDBc.`:u>=24?u/24>31?dayjs(e).format("DD/MM/YYYY"):`${Math.floor(u/24)} ng\xE0y tr\u01B0\u1EDBc.`:"v\u1EEBa xong."};var lottie$1={exports:{}};(function(module,exports){typeof navigator!="undefined"&&function(e,r){module.exports=r()}(commonjsGlobal$1,function(){var svgNS="http://www.w3.org/2000/svg",locationHref="",_useWebWorker=!1,initialDefaultFrame=-999999,setWebWorker=function(r){_useWebWorker=!!r},getWebWorker=function(){return _useWebWorker},setLocationHref=function(r){locationHref=r},getLocationHref=function(){return locationHref};function createTag(e){return document.createElement(e)}function extendPrototype(e,r){var n,a=e.length,l;for(n=0;n<a;n+=1){l=e[n].prototype;for(var c in l)Object.prototype.hasOwnProperty.call(l,c)&&(r.prototype[c]=l[c])}}function getDescriptor(e,r){return Object.getOwnPropertyDescriptor(e,r)}function createProxyFunction(e){function r(){}return r.prototype=e,r}var audioControllerFactory=function(){function e(r){this.audios=[],this.audioFactory=r,this._volume=1,this._isMuted=!1}return e.prototype={addAudio:function(n){this.audios.push(n)},pause:function(){var n,a=this.audios.length;for(n=0;n<a;n+=1)this.audios[n].pause()},resume:function(){var n,a=this.audios.length;for(n=0;n<a;n+=1)this.audios[n].resume()},setRate:function(n){var a,l=this.audios.length;for(a=0;a<l;a+=1)this.audios[a].setRate(n)},createAudio:function(n){return this.audioFactory?this.audioFactory(n):window.Howl?new window.Howl({src:[n]}):{isPlaying:!1,play:function(){this.isPlaying=!0},seek:function(){this.isPlaying=!1},playing:function(){},rate:function(){},setVolume:function(){}}},setAudioFactory:function(n){this.audioFactory=n},setVolume:function(n){this._volume=n,this._updateVolume()},mute:function(){this._isMuted=!0,this._updateVolume()},unmute:function(){this._isMuted=!1,this._updateVolume()},getVolume:function(){return this._volume},_updateVolume:function(){var n,a=this.audios.length;for(n=0;n<a;n+=1)this.audios[n].volume(this._volume*(this._isMuted?0:1))}},function(){return new e}}(),createTypedArray=function(){function e(n,a){var l=0,c=[],u;switch(n){case"int16":case"uint8c":u=1;break;default:u=1.1;break}for(l=0;l<a;l+=1)c.push(u);return c}function r(n,a){return n==="float32"?new Float32Array(a):n==="int16"?new Int16Array(a):n==="uint8c"?new Uint8ClampedArray(a):e(n,a)}return typeof Uint8ClampedArray=="function"&&typeof Float32Array=="function"?r:e}();function createSizedArray(e){return Array.apply(null,{length:e})}function _typeof$6(e){return typeof Symbol=="function"&&typeof Symbol.iterator=="symbol"?_typeof$6=function(n){return typeof n}:_typeof$6=function(n){return n&&typeof Symbol=="function"&&n.constructor===Symbol&&n!==Symbol.prototype?"symbol":typeof n},_typeof$6(e)}var subframeEnabled=!0,expressionsPlugin=null,idPrefix="",isSafari=/^((?!chrome|android).)*safari/i.test(navigator.userAgent),bmPow=Math.pow,bmSqrt=Math.sqrt,bmFloor=Math.floor,bmMax=Math.max,bmMin=Math.min,BMMath={};(function(){var e=["abs","acos","acosh","asin","asinh","atan","atanh","atan2","ceil","cbrt","expm1","clz32","cos","cosh","exp","floor","fround","hypot","imul","log","log1p","log2","log10","max","min","pow","random","round","sign","sin","sinh","sqrt","tan","tanh","trunc","E","LN10","LN2","LOG10E","LOG2E","PI","SQRT1_2","SQRT2"],r,n=e.length;for(r=0;r<n;r+=1)BMMath[e[r]]=Math[e[r]]})(),BMMath.random=Math.random,BMMath.abs=function(e){var r=_typeof$6(e);if(r==="object"&&e.length){var n=createSizedArray(e.length),a,l=e.length;for(a=0;a<l;a+=1)n[a]=Math.abs(e[a]);return n}return Math.abs(e)};var defaultCurveSegments=150,degToRads=Math.PI/180,roundCorner=.5519;function styleDiv(e){e.style.position="absolute",e.style.top=0,e.style.left=0,e.style.display="block",e.style.transformOrigin="0 0",e.style.webkitTransformOrigin="0 0",e.style.backfaceVisibility="visible",e.style.webkitBackfaceVisibility="visible",e.style.transformStyle="preserve-3d",e.style.webkitTransformStyle="preserve-3d",e.style.mozTransformStyle="preserve-3d"}function BMEnterFrameEvent(e,r,n,a){this.type=e,this.currentTime=r,this.totalTime=n,this.direction=a<0?-1:1}function BMCompleteEvent(e,r){this.type=e,this.direction=r<0?-1:1}function BMCompleteLoopEvent(e,r,n,a){this.type=e,this.currentLoop=n,this.totalLoops=r,this.direction=a<0?-1:1}function BMSegmentStartEvent(e,r,n){this.type=e,this.firstFrame=r,this.totalFrames=n}function BMDestroyEvent(e,r){this.type=e,this.target=r}function BMRenderFrameErrorEvent(e,r){this.type="renderFrameError",this.nativeError=e,this.currentTime=r}function BMConfigErrorEvent(e){this.type="configError",this.nativeError=e}var createElementID=function(){var e=0;return function(){return e+=1,idPrefix+"__lottie_element_"+e}}();function HSVtoRGB(e,r,n){var a,l,c,u,d,m,b,A;switch(u=Math.floor(e*6),d=e*6-u,m=n*(1-r),b=n*(1-d*r),A=n*(1-(1-d)*r),u%6){case 0:a=n,l=A,c=m;break;case 1:a=b,l=n,c=m;break;case 2:a=m,l=n,c=A;break;case 3:a=m,l=b,c=n;break;case 4:a=A,l=m,c=n;break;case 5:a=n,l=m,c=b;break}return[a,l,c]}function RGBtoHSV(e,r,n){var a=Math.max(e,r,n),l=Math.min(e,r,n),c=a-l,u,d=a===0?0:c/a,m=a/255;switch(a){case l:u=0;break;case e:u=r-n+c*(r<n?6:0),u/=6*c;break;case r:u=n-e+c*2,u/=6*c;break;case n:u=e-r+c*4,u/=6*c;break}return[u,d,m]}function addSaturationToRGB(e,r){var n=RGBtoHSV(e[0]*255,e[1]*255,e[2]*255);return n[1]+=r,n[1]>1?n[1]=1:n[1]<=0&&(n[1]=0),HSVtoRGB(n[0],n[1],n[2])}function addBrightnessToRGB(e,r){var n=RGBtoHSV(e[0]*255,e[1]*255,e[2]*255);return n[2]+=r,n[2]>1?n[2]=1:n[2]<0&&(n[2]=0),HSVtoRGB(n[0],n[1],n[2])}function addHueToRGB(e,r){var n=RGBtoHSV(e[0]*255,e[1]*255,e[2]*255);return n[0]+=r/360,n[0]>1?n[0]-=1:n[0]<0&&(n[0]+=1),HSVtoRGB(n[0],n[1],n[2])}var rgbToHex=function(){var e=[],r,n;for(r=0;r<256;r+=1)n=r.toString(16),e[r]=n.length===1?"0"+n:n;return function(a,l,c){return a<0&&(a=0),l<0&&(l=0),c<0&&(c=0),"#"+e[a]+e[l]+e[c]}}(),setSubframeEnabled=function(r){subframeEnabled=!!r},getSubframeEnabled=function(){return subframeEnabled},setExpressionsPlugin=function(r){expressionsPlugin=r},getExpressionsPlugin=function(){return expressionsPlugin},setDefaultCurveSegments=function(r){defaultCurveSegments=r},getDefaultCurveSegments=function(){return defaultCurveSegments},setIdPrefix=function(r){idPrefix=r};function createNS(e){return document.createElementNS(svgNS,e)}function _typeof$5(e){return typeof Symbol=="function"&&typeof Symbol.iterator=="symbol"?_typeof$5=function(n){return typeof n}:_typeof$5=function(n){return n&&typeof Symbol=="function"&&n.constructor===Symbol&&n!==Symbol.prototype?"symbol":typeof n},_typeof$5(e)}var dataManager=function(){var e=1,r=[],n,a,l={onmessage:function(){},postMessage:function(C){n({data:C})}},c={postMessage:function(C){l.onmessage({data:C})}};function u(T){if(window.Worker&&window.Blob&&getWebWorker()){var C=new Blob(["var _workerSelf = self; self.onmessage = ",T.toString()],{type:"text/javascript"}),S=URL.createObjectURL(C);return new Worker(S)}return n=T,l}function d(){a||(a=u(function(C){function S(){function k(q,L){var D,F,$=q.length,U,z,K,Z;for(F=0;F<$;F+=1)if(D=q[F],"ks"in D&&!D.completed){if(D.completed=!0,D.tt&&(q[F-1].td=D.tt),D.hasMask){var Y=D.masksProperties;for(z=Y.length,U=0;U<z;U+=1)if(Y[U].pt.k.i)I(Y[U].pt.k);else for(Z=Y[U].pt.k.length,K=0;K<Z;K+=1)Y[U].pt.k[K].s&&I(Y[U].pt.k[K].s[0]),Y[U].pt.k[K].e&&I(Y[U].pt.k[K].e[0])}D.ty===0?(D.layers=g(D.refId,L),k(D.layers,L)):D.ty===4?y(D.shapes):D.ty===5&&Q(D)}}function x(q,L){if(q){var D=0,F=q.length;for(D=0;D<F;D+=1)q[D].t===1&&(q[D].data.layers=g(q[D].data.refId,L),k(q[D].data.layers,L))}}function E(q,L){for(var D=0,F=L.length;D<F;){if(L[D].id===q)return L[D];D+=1}return null}function g(q,L){var D=E(q,L);return D?D.layers.__used?JSON.parse(JSON.stringify(D.layers)):(D.layers.__used=!0,D.layers):null}function y(q){var L,D=q.length,F,$;for(L=D-1;L>=0;L-=1)if(q[L].ty==="sh")if(q[L].ks.k.i)I(q[L].ks.k);else for($=q[L].ks.k.length,F=0;F<$;F+=1)q[L].ks.k[F].s&&I(q[L].ks.k[F].s[0]),q[L].ks.k[F].e&&I(q[L].ks.k[F].e[0]);else q[L].ty==="gr"&&y(q[L].it)}function I(q){var L,D=q.i.length;for(L=0;L<D;L+=1)q.i[L][0]+=q.v[L][0],q.i[L][1]+=q.v[L][1],q.o[L][0]+=q.v[L][0],q.o[L][1]+=q.v[L][1]}function M(q,L){var D=L?L.split("."):[100,100,100];return q[0]>D[0]?!0:D[0]>q[0]?!1:q[1]>D[1]?!0:D[1]>q[1]?!1:q[2]>D[2]?!0:D[2]>q[2]?!1:null}var j=function(){var q=[4,4,14];function L(F){var $=F.t.d;F.t.d={k:[{s:$,t:0}]}}function D(F){var $,U=F.length;for($=0;$<U;$+=1)F[$].ty===5&&L(F[$])}return function(F){if(M(q,F.v)&&(D(F.layers),F.assets)){var $,U=F.assets.length;for($=0;$<U;$+=1)F.assets[$].layers&&D(F.assets[$].layers)}}}(),O=function(){var q=[4,7,99];return function(L){if(L.chars&&!M(q,L.v)){var D,F=L.chars.length;for(D=0;D<F;D+=1){var $=L.chars[D];$.data&&$.data.shapes&&(y($.data.shapes),$.data.ip=0,$.data.op=99999,$.data.st=0,$.data.sr=1,$.data.ks={p:{k:[0,0],a:0},s:{k:[100,100],a:0},a:{k:[0,0],a:0},r:{k:0,a:0},o:{k:100,a:0}},L.chars[D].t||($.data.shapes.push({ty:"no"}),$.data.shapes[0].it.push({p:{k:[0,0],a:0},s:{k:[100,100],a:0},a:{k:[0,0],a:0},r:{k:0,a:0},o:{k:100,a:0},sk:{k:0,a:0},sa:{k:0,a:0},ty:"tr"})))}}}}(),R=function(){var q=[5,7,15];function L(F){var $=F.t.p;typeof $.a=="number"&&($.a={a:0,k:$.a}),typeof $.p=="number"&&($.p={a:0,k:$.p}),typeof $.r=="number"&&($.r={a:0,k:$.r})}function D(F){var $,U=F.length;for($=0;$<U;$+=1)F[$].ty===5&&L(F[$])}return function(F){if(M(q,F.v)&&(D(F.layers),F.assets)){var $,U=F.assets.length;for($=0;$<U;$+=1)F.assets[$].layers&&D(F.assets[$].layers)}}}(),B=function(){var q=[4,1,9];function L(F){var $,U=F.length,z,K;for($=0;$<U;$+=1)if(F[$].ty==="gr")L(F[$].it);else if(F[$].ty==="fl"||F[$].ty==="st")if(F[$].c.k&&F[$].c.k[0].i)for(K=F[$].c.k.length,z=0;z<K;z+=1)F[$].c.k[z].s&&(F[$].c.k[z].s[0]/=255,F[$].c.k[z].s[1]/=255,F[$].c.k[z].s[2]/=255,F[$].c.k[z].s[3]/=255),F[$].c.k[z].e&&(F[$].c.k[z].e[0]/=255,F[$].c.k[z].e[1]/=255,F[$].c.k[z].e[2]/=255,F[$].c.k[z].e[3]/=255);else F[$].c.k[0]/=255,F[$].c.k[1]/=255,F[$].c.k[2]/=255,F[$].c.k[3]/=255}function D(F){var $,U=F.length;for($=0;$<U;$+=1)F[$].ty===4&&L(F[$].shapes)}return function(F){if(M(q,F.v)&&(D(F.layers),F.assets)){var $,U=F.assets.length;for($=0;$<U;$+=1)F.assets[$].layers&&D(F.assets[$].layers)}}}(),G=function(){var q=[4,4,18];function L(F){var $,U=F.length,z,K;for($=U-1;$>=0;$-=1)if(F[$].ty==="sh")if(F[$].ks.k.i)F[$].ks.k.c=F[$].closed;else for(K=F[$].ks.k.length,z=0;z<K;z+=1)F[$].ks.k[z].s&&(F[$].ks.k[z].s[0].c=F[$].closed),F[$].ks.k[z].e&&(F[$].ks.k[z].e[0].c=F[$].closed);else F[$].ty==="gr"&&L(F[$].it)}function D(F){var $,U,z=F.length,K,Z,Y,le;for(U=0;U<z;U+=1){if($=F[U],$.hasMask){var re=$.masksProperties;for(Z=re.length,K=0;K<Z;K+=1)if(re[K].pt.k.i)re[K].pt.k.c=re[K].cl;else for(le=re[K].pt.k.length,Y=0;Y<le;Y+=1)re[K].pt.k[Y].s&&(re[K].pt.k[Y].s[0].c=re[K].cl),re[K].pt.k[Y].e&&(re[K].pt.k[Y].e[0].c=re[K].cl)}$.ty===4&&L($.shapes)}}return function(F){if(M(q,F.v)&&(D(F.layers),F.assets)){var $,U=F.assets.length;for($=0;$<U;$+=1)F.assets[$].layers&&D(F.assets[$].layers)}}}();function V(q){q.__complete||(B(q),j(q),O(q),R(q),G(q),k(q.layers,q.assets),x(q.chars,q.assets),q.__complete=!0)}function Q(q){q.t.a.length===0&&!("m"in q.t.p)}var W={};return W.completeData=V,W.checkColors=B,W.checkChars=O,W.checkPathProperties=R,W.checkShapes=G,W.completeLayers=k,W}if(c.dataManager||(c.dataManager=S()),c.assetLoader||(c.assetLoader=function(){function k(E){var g=E.getResponseHeader("content-type");return g&&E.responseType==="json"&&g.indexOf("json")!==-1||E.response&&_typeof$5(E.response)==="object"?E.response:E.response&&typeof E.response=="string"?JSON.parse(E.response):E.responseText?JSON.parse(E.responseText):null}function x(E,g,y,I){var M,j=new XMLHttpRequest;try{j.responseType="json"}catch{}j.onreadystatechange=function(){if(j.readyState===4)if(j.status===200)M=k(j),y(M);else try{M=k(j),y(M)}catch(O){I&&I(O)}};try{j.open("GET",E,!0)}catch{j.open("GET",g+"/"+E,!0)}j.send()}return{load:x}}()),C.data.type==="loadAnimation")c.assetLoader.load(C.data.path,C.data.fullPath,function(k){c.dataManager.completeData(k),c.postMessage({id:C.data.id,payload:k,status:"success"})},function(){c.postMessage({id:C.data.id,status:"error"})});else if(C.data.type==="complete"){var P=C.data.animation;c.dataManager.completeData(P),c.postMessage({id:C.data.id,payload:P,status:"success"})}else C.data.type==="loadData"&&c.assetLoader.load(C.data.path,C.data.fullPath,function(k){c.postMessage({id:C.data.id,payload:k,status:"success"})},function(){c.postMessage({id:C.data.id,status:"error"})})}),a.onmessage=function(T){var C=T.data,S=C.id,P=r[S];r[S]=null,C.status==="success"?P.onComplete(C.payload):P.onError&&P.onError()})}function m(T,C){e+=1;var S="processId_"+e;return r[S]={onComplete:T,onError:C},S}function b(T,C,S){d();var P=m(C,S);a.postMessage({type:"loadAnimation",path:T,fullPath:window.location.origin+window.location.pathname,id:P})}function A(T,C,S){d();var P=m(C,S);a.postMessage({type:"loadData",path:T,fullPath:window.location.origin+window.location.pathname,id:P})}function _(T,C,S){d();var P=m(C,S);a.postMessage({type:"complete",animation:T,id:P})}return{loadAnimation:b,loadData:A,completeAnimation:_}}(),ImagePreloader=function(){var e=function(){var x=createTag("canvas");x.width=1,x.height=1;var E=x.getContext("2d");return E.fillStyle="rgba(0,0,0,0)",E.fillRect(0,0,1,1),x}();function r(){this.loadedAssets+=1,this.loadedAssets===this.totalImages&&this.loadedFootagesCount===this.totalFootages&&this.imagesLoadedCb&&this.imagesLoadedCb(null)}function n(){this.loadedFootagesCount+=1,this.loadedAssets===this.totalImages&&this.loadedFootagesCount===this.totalFootages&&this.imagesLoadedCb&&this.imagesLoadedCb(null)}function a(x,E,g){var y="";if(x.e)y=x.p;else if(E){var I=x.p;I.indexOf("images/")!==-1&&(I=I.split("/")[1]),y=E+I}else y=g,y+=x.u?x.u:"",y+=x.p;return y}function l(x){var E=0,g=setInterval(function(){var y=x.getBBox();(y.width||E>500)&&(this._imageLoaded(),clearInterval(g)),E+=1}.bind(this),50)}function c(x){var E=a(x,this.assetsPath,this.path),g=createNS("image");isSafari?this.testImageLoaded(g):g.addEventListener("load",this._imageLoaded,!1),g.addEventListener("error",function(){y.img=e,this._imageLoaded()}.bind(this),!1),g.setAttributeNS("http://www.w3.org/1999/xlink","href",E),this._elementHelper.append?this._elementHelper.append(g):this._elementHelper.appendChild(g);var y={img:g,assetData:x};return y}function u(x){var E=a(x,this.assetsPath,this.path),g=createTag("img");g.crossOrigin="anonymous",g.addEventListener("load",this._imageLoaded,!1),g.addEventListener("error",function(){y.img=e,this._imageLoaded()}.bind(this),!1),g.src=E;var y={img:g,assetData:x};return y}function d(x){var E={assetData:x},g=a(x,this.assetsPath,this.path);return dataManager.loadData(g,function(y){E.img=y,this._footageLoaded()}.bind(this),function(){E.img={},this._footageLoaded()}.bind(this)),E}function m(x,E){this.imagesLoadedCb=E;var g,y=x.length;for(g=0;g<y;g+=1)x[g].layers||(!x[g].t||x[g].t==="seq"?(this.totalImages+=1,this.images.push(this._createImageData(x[g]))):x[g].t===3&&(this.totalFootages+=1,this.images.push(this.createFootageData(x[g]))))}function b(x){this.path=x||""}function A(x){this.assetsPath=x||""}function _(x){for(var E=0,g=this.images.length;E<g;){if(this.images[E].assetData===x)return this.images[E].img;E+=1}return null}function T(){this.imagesLoadedCb=null,this.images.length=0}function C(){return this.totalImages===this.loadedAssets}function S(){return this.totalFootages===this.loadedFootagesCount}function P(x,E){x==="svg"?(this._elementHelper=E,this._createImageData=this.createImageData.bind(this)):this._createImageData=this.createImgData.bind(this)}function k(){this._imageLoaded=r.bind(this),this._footageLoaded=n.bind(this),this.testImageLoaded=l.bind(this),this.createFootageData=d.bind(this),this.assetsPath="",this.path="",this.totalImages=0,this.totalFootages=0,this.loadedAssets=0,this.loadedFootagesCount=0,this.imagesLoadedCb=null,this.images=[]}return k.prototype={loadAssets:m,setAssetsPath:A,setPath:b,loadedImages:C,loadedFootages:S,destroy:T,getAsset:_,createImgData:u,createImageData:c,imageLoaded:r,footageLoaded:n,setCacheType:P},k}();function BaseEvent(){}BaseEvent.prototype={triggerEvent:function(r,n){if(this._cbs[r])for(var a=this._cbs[r],l=0;l<a.length;l+=1)a[l](n)},addEventListener:function(r,n){return this._cbs[r]||(this._cbs[r]=[]),this._cbs[r].push(n),function(){this.removeEventListener(r,n)}.bind(this)},removeEventListener:function(r,n){if(!n)this._cbs[r]=null;else if(this._cbs[r]){for(var a=0,l=this._cbs[r].length;a<l;)this._cbs[r][a]===n&&(this._cbs[r].splice(a,1),a-=1,l-=1),a+=1;this._cbs[r].length||(this._cbs[r]=null)}}};var markerParser=function(){function e(r){for(var n=r.split(`\r
`),a={},l,c=0,u=0;u<n.length;u+=1)l=n[u].split(":"),l.length===2&&(a[l[0]]=l[1].trim(),c+=1);if(c===0)throw new Error;return a}return function(r){for(var n=[],a=0;a<r.length;a+=1){var l=r[a],c={time:l.tm,duration:l.dr};try{c.payload=JSON.parse(r[a].cm)}catch{try{c.payload=e(r[a].cm)}catch{c.payload={name:r[a]}}}n.push(c)}return n}}(),ProjectInterface=function(){function e(r){this.compositions.push(r)}return function(){function r(n){for(var a=0,l=this.compositions.length;a<l;){if(this.compositions[a].data&&this.compositions[a].data.nm===n)return this.compositions[a].prepareFrame&&this.compositions[a].data.xt&&this.compositions[a].prepareFrame(this.currentFrame),this.compositions[a].compInterface;a+=1}return null}return r.compositions=[],r.currentFrame=0,r.registerComposition=e,r}}(),renderers={},registerRenderer=function(r,n){renderers[r]=n};function getRenderer(e){return renderers[e]}function _typeof$4(e){return typeof Symbol=="function"&&typeof Symbol.iterator=="symbol"?_typeof$4=function(n){return typeof n}:_typeof$4=function(n){return n&&typeof Symbol=="function"&&n.constructor===Symbol&&n!==Symbol.prototype?"symbol":typeof n},_typeof$4(e)}var AnimationItem=function(){this._cbs=[],this.name="",this.path="",this.isLoaded=!1,this.currentFrame=0,this.currentRawFrame=0,this.firstFrame=0,this.totalFrames=0,this.frameRate=0,this.frameMult=0,this.playSpeed=1,this.playDirection=1,this.playCount=0,this.animationData={},this.assets=[],this.isPaused=!0,this.autoplay=!1,this.loop=!0,this.renderer=null,this.animationID=createElementID(),this.assetsPath="",this.timeCompleted=0,this.segmentPos=0,this.isSubframeEnabled=getSubframeEnabled(),this.segments=[],this._idle=!0,this._completedLoop=!1,this.projectInterface=ProjectInterface(),this.imagePreloader=new ImagePreloader,this.audioController=audioControllerFactory(),this.markers=[],this.configAnimation=this.configAnimation.bind(this),this.onSetupError=this.onSetupError.bind(this),this.onSegmentComplete=this.onSegmentComplete.bind(this),this.drawnFrameEvent=new BMEnterFrameEvent("drawnFrame",0,0,0)};extendPrototype([BaseEvent],AnimationItem),AnimationItem.prototype.setParams=function(e){(e.wrapper||e.container)&&(this.wrapper=e.wrapper||e.container);var r="svg";e.animType?r=e.animType:e.renderer&&(r=e.renderer);var n=getRenderer(r);this.renderer=new n(this,e.rendererSettings),this.imagePreloader.setCacheType(r,this.renderer.globalData.defs),this.renderer.setProjectInterface(this.projectInterface),this.animType=r,e.loop===""||e.loop===null||e.loop===void 0||e.loop===!0?this.loop=!0:e.loop===!1?this.loop=!1:this.loop=parseInt(e.loop,10),this.autoplay="autoplay"in e?e.autoplay:!0,this.name=e.name?e.name:"",this.autoloadSegments=Object.prototype.hasOwnProperty.call(e,"autoloadSegments")?e.autoloadSegments:!0,this.assetsPath=e.assetsPath,this.initialSegment=e.initialSegment,e.audioFactory&&this.audioController.setAudioFactory(e.audioFactory),e.animationData?this.setupAnimation(e.animationData):e.path&&(e.path.lastIndexOf("\\")!==-1?this.path=e.path.substr(0,e.path.lastIndexOf("\\")+1):this.path=e.path.substr(0,e.path.lastIndexOf("/")+1),this.fileName=e.path.substr(e.path.lastIndexOf("/")+1),this.fileName=this.fileName.substr(0,this.fileName.lastIndexOf(".json")),dataManager.loadAnimation(e.path,this.configAnimation,this.onSetupError))},AnimationItem.prototype.onSetupError=function(){this.trigger("data_failed")},AnimationItem.prototype.setupAnimation=function(e){dataManager.completeAnimation(e,this.configAnimation)},AnimationItem.prototype.setData=function(e,r){r&&_typeof$4(r)!=="object"&&(r=JSON.parse(r));var n={wrapper:e,animationData:r},a=e.attributes;n.path=a.getNamedItem("data-animation-path")?a.getNamedItem("data-animation-path").value:a.getNamedItem("data-bm-path")?a.getNamedItem("data-bm-path").value:a.getNamedItem("bm-path")?a.getNamedItem("bm-path").value:"",n.animType=a.getNamedItem("data-anim-type")?a.getNamedItem("data-anim-type").value:a.getNamedItem("data-bm-type")?a.getNamedItem("data-bm-type").value:a.getNamedItem("bm-type")?a.getNamedItem("bm-type").value:a.getNamedItem("data-bm-renderer")?a.getNamedItem("data-bm-renderer").value:a.getNamedItem("bm-renderer")?a.getNamedItem("bm-renderer").value:"canvas";var l=a.getNamedItem("data-anim-loop")?a.getNamedItem("data-anim-loop").value:a.getNamedItem("data-bm-loop")?a.getNamedItem("data-bm-loop").value:a.getNamedItem("bm-loop")?a.getNamedItem("bm-loop").value:"";l==="false"?n.loop=!1:l==="true"?n.loop=!0:l!==""&&(n.loop=parseInt(l,10));var c=a.getNamedItem("data-anim-autoplay")?a.getNamedItem("data-anim-autoplay").value:a.getNamedItem("data-bm-autoplay")?a.getNamedItem("data-bm-autoplay").value:a.getNamedItem("bm-autoplay")?a.getNamedItem("bm-autoplay").value:!0;n.autoplay=c!=="false",n.name=a.getNamedItem("data-name")?a.getNamedItem("data-name").value:a.getNamedItem("data-bm-name")?a.getNamedItem("data-bm-name").value:a.getNamedItem("bm-name")?a.getNamedItem("bm-name").value:"";var u=a.getNamedItem("data-anim-prerender")?a.getNamedItem("data-anim-prerender").value:a.getNamedItem("data-bm-prerender")?a.getNamedItem("data-bm-prerender").value:a.getNamedItem("bm-prerender")?a.getNamedItem("bm-prerender").value:"";u==="false"&&(n.prerender=!1),this.setParams(n)},AnimationItem.prototype.includeLayers=function(e){e.op>this.animationData.op&&(this.animationData.op=e.op,this.totalFrames=Math.floor(e.op-this.animationData.ip));var r=this.animationData.layers,n,a=r.length,l=e.layers,c,u=l.length;for(c=0;c<u;c+=1)for(n=0;n<a;){if(r[n].id===l[c].id){r[n]=l[c];break}n+=1}if((e.chars||e.fonts)&&(this.renderer.globalData.fontManager.addChars(e.chars),this.renderer.globalData.fontManager.addFonts(e.fonts,this.renderer.globalData.defs)),e.assets)for(a=e.assets.length,n=0;n<a;n+=1)this.animationData.assets.push(e.assets[n]);this.animationData.__complete=!1,dataManager.completeAnimation(this.animationData,this.onSegmentComplete)},AnimationItem.prototype.onSegmentComplete=function(e){this.animationData=e;var r=getExpressionsPlugin();r&&r.initExpressions(this),this.loadNextSegment()},AnimationItem.prototype.loadNextSegment=function(){var e=this.animationData.segments;if(!e||e.length===0||!this.autoloadSegments){this.trigger("data_ready"),this.timeCompleted=this.totalFrames;return}var r=e.shift();this.timeCompleted=r.time*this.frameRate;var n=this.path+this.fileName+"_"+this.segmentPos+".json";this.segmentPos+=1,dataManager.loadData(n,this.includeLayers.bind(this),function(){this.trigger("data_failed")}.bind(this))},AnimationItem.prototype.loadSegments=function(){var e=this.animationData.segments;e||(this.timeCompleted=this.totalFrames),this.loadNextSegment()},AnimationItem.prototype.imagesLoaded=function(){this.trigger("loaded_images"),this.checkLoaded()},AnimationItem.prototype.preloadImages=function(){this.imagePreloader.setAssetsPath(this.assetsPath),this.imagePreloader.setPath(this.path),this.imagePreloader.loadAssets(this.animationData.assets,this.imagesLoaded.bind(this))},AnimationItem.prototype.configAnimation=function(e){if(!!this.renderer)try{this.animationData=e,this.initialSegment?(this.totalFrames=Math.floor(this.initialSegment[1]-this.initialSegment[0]),this.firstFrame=Math.round(this.initialSegment[0])):(this.totalFrames=Math.floor(this.animationData.op-this.animationData.ip),this.firstFrame=Math.round(this.animationData.ip)),this.renderer.configAnimation(e),e.assets||(e.assets=[]),this.assets=this.animationData.assets,this.frameRate=this.animationData.fr,this.frameMult=this.animationData.fr/1e3,this.renderer.searchExtraCompositions(e.assets),this.markers=markerParser(e.markers||[]),this.trigger("config_ready"),this.preloadImages(),this.loadSegments(),this.updaFrameModifier(),this.waitForFontsLoaded(),this.isPaused&&this.audioController.pause()}catch(r){this.triggerConfigError(r)}},AnimationItem.prototype.waitForFontsLoaded=function(){!this.renderer||(this.renderer.globalData.fontManager.isLoaded?this.checkLoaded():setTimeout(this.waitForFontsLoaded.bind(this),20))},AnimationItem.prototype.checkLoaded=function(){if(!this.isLoaded&&this.renderer.globalData.fontManager.isLoaded&&(this.imagePreloader.loadedImages()||this.renderer.rendererType!=="canvas")&&this.imagePreloader.loadedFootages()){this.isLoaded=!0;var e=getExpressionsPlugin();e&&e.initExpressions(this),this.renderer.initItems(),setTimeout(function(){this.trigger("DOMLoaded")}.bind(this),0),this.gotoFrame(),this.autoplay&&this.play()}},AnimationItem.prototype.resize=function(){this.renderer.updateContainerSize()},AnimationItem.prototype.setSubframe=function(e){this.isSubframeEnabled=!!e},AnimationItem.prototype.gotoFrame=function(){this.currentFrame=this.isSubframeEnabled?this.currentRawFrame:~~this.currentRawFrame,this.timeCompleted!==this.totalFrames&&this.currentFrame>this.timeCompleted&&(this.currentFrame=this.timeCompleted),this.trigger("enterFrame"),this.renderFrame(),this.trigger("drawnFrame")},AnimationItem.prototype.renderFrame=function(){if(!(this.isLoaded===!1||!this.renderer))try{this.renderer.renderFrame(this.currentFrame+this.firstFrame)}catch(e){this.triggerRenderFrameError(e)}},AnimationItem.prototype.play=function(e){e&&this.name!==e||this.isPaused===!0&&(this.isPaused=!1,this.trigger("_pause"),this.audioController.resume(),this._idle&&(this._idle=!1,this.trigger("_active")))},AnimationItem.prototype.pause=function(e){e&&this.name!==e||this.isPaused===!1&&(this.isPaused=!0,this.trigger("_play"),this._idle=!0,this.trigger("_idle"),this.audioController.pause())},AnimationItem.prototype.togglePause=function(e){e&&this.name!==e||(this.isPaused===!0?this.play():this.pause())},AnimationItem.prototype.stop=function(e){e&&this.name!==e||(this.pause(),this.playCount=0,this._completedLoop=!1,this.setCurrentRawFrameValue(0))},AnimationItem.prototype.getMarkerData=function(e){for(var r,n=0;n<this.markers.length;n+=1)if(r=this.markers[n],r.payload&&r.payload.name.cm===e)return r;return null},AnimationItem.prototype.goToAndStop=function(e,r,n){if(!(n&&this.name!==n)){var a=Number(e);if(isNaN(a)){var l=this.getMarkerData(e);l&&this.goToAndStop(l.time,!0)}else r?this.setCurrentRawFrameValue(e):this.setCurrentRawFrameValue(e*this.frameModifier);this.pause()}},AnimationItem.prototype.goToAndPlay=function(e,r,n){if(!(n&&this.name!==n)){var a=Number(e);if(isNaN(a)){var l=this.getMarkerData(e);l&&(l.duration?this.playSegments([l.time,l.time+l.duration],!0):this.goToAndStop(l.time,!0))}else this.goToAndStop(a,r,n);this.play()}},AnimationItem.prototype.advanceTime=function(e){if(!(this.isPaused===!0||this.isLoaded===!1)){var r=this.currentRawFrame+e*this.frameModifier,n=!1;r>=this.totalFrames-1&&this.frameModifier>0?!this.loop||this.playCount===this.loop?this.checkSegments(r>this.totalFrames?r%this.totalFrames:0)||(n=!0,r=this.totalFrames-1):r>=this.totalFrames?(this.playCount+=1,this.checkSegments(r%this.totalFrames)||(this.setCurrentRawFrameValue(r%this.totalFrames),this._completedLoop=!0,this.trigger("loopComplete"))):this.setCurrentRawFrameValue(r):r<0?this.checkSegments(r%this.totalFrames)||(this.loop&&!(this.playCount--<=0&&this.loop!==!0)?(this.setCurrentRawFrameValue(this.totalFrames+r%this.totalFrames),this._completedLoop?this.trigger("loopComplete"):this._completedLoop=!0):(n=!0,r=0)):this.setCurrentRawFrameValue(r),n&&(this.setCurrentRawFrameValue(r),this.pause(),this.trigger("complete"))}},AnimationItem.prototype.adjustSegment=function(e,r){this.playCount=0,e[1]<e[0]?(this.frameModifier>0&&(this.playSpeed<0?this.setSpeed(-this.playSpeed):this.setDirection(-1)),this.totalFrames=e[0]-e[1],this.timeCompleted=this.totalFrames,this.firstFrame=e[1],this.setCurrentRawFrameValue(this.totalFrames-.001-r)):e[1]>e[0]&&(this.frameModifier<0&&(this.playSpeed<0?this.setSpeed(-this.playSpeed):this.setDirection(1)),this.totalFrames=e[1]-e[0],this.timeCompleted=this.totalFrames,this.firstFrame=e[0],this.setCurrentRawFrameValue(.001+r)),this.trigger("segmentStart")},AnimationItem.prototype.setSegment=function(e,r){var n=-1;this.isPaused&&(this.currentRawFrame+this.firstFrame<e?n=e:this.currentRawFrame+this.firstFrame>r&&(n=r-e)),this.firstFrame=e,this.totalFrames=r-e,this.timeCompleted=this.totalFrames,n!==-1&&this.goToAndStop(n,!0)},AnimationItem.prototype.playSegments=function(e,r){if(r&&(this.segments.length=0),_typeof$4(e[0])==="object"){var n,a=e.length;for(n=0;n<a;n+=1)this.segments.push(e[n])}else this.segments.push(e);this.segments.length&&r&&this.adjustSegment(this.segments.shift(),0),this.isPaused&&this.play()},AnimationItem.prototype.resetSegments=function(e){this.segments.length=0,this.segments.push([this.animationData.ip,this.animationData.op]),e&&this.checkSegments(0)},AnimationItem.prototype.checkSegments=function(e){return this.segments.length?(this.adjustSegment(this.segments.shift(),e),!0):!1},AnimationItem.prototype.destroy=function(e){e&&this.name!==e||!this.renderer||(this.renderer.destroy(),this.imagePreloader.destroy(),this.trigger("destroy"),this._cbs=null,this.onEnterFrame=null,this.onLoopComplete=null,this.onComplete=null,this.onSegmentStart=null,this.onDestroy=null,this.renderer=null,this.renderer=null,this.imagePreloader=null,this.projectInterface=null)},AnimationItem.prototype.setCurrentRawFrameValue=function(e){this.currentRawFrame=e,this.gotoFrame()},AnimationItem.prototype.setSpeed=function(e){this.playSpeed=e,this.updaFrameModifier()},AnimationItem.prototype.setDirection=function(e){this.playDirection=e<0?-1:1,this.updaFrameModifier()},AnimationItem.prototype.setVolume=function(e,r){r&&this.name!==r||this.audioController.setVolume(e)},AnimationItem.prototype.getVolume=function(){return this.audioController.getVolume()},AnimationItem.prototype.mute=function(e){e&&this.name!==e||this.audioController.mute()},AnimationItem.prototype.unmute=function(e){e&&this.name!==e||this.audioController.unmute()},AnimationItem.prototype.updaFrameModifier=function(){this.frameModifier=this.frameMult*this.playSpeed*this.playDirection,this.audioController.setRate(this.playSpeed*this.playDirection)},AnimationItem.prototype.getPath=function(){return this.path},AnimationItem.prototype.getAssetsPath=function(e){var r="";if(e.e)r=e.p;else if(this.assetsPath){var n=e.p;n.indexOf("images/")!==-1&&(n=n.split("/")[1]),r=this.assetsPath+n}else r=this.path,r+=e.u?e.u:"",r+=e.p;return r},AnimationItem.prototype.getAssetData=function(e){for(var r=0,n=this.assets.length;r<n;){if(e===this.assets[r].id)return this.assets[r];r+=1}return null},AnimationItem.prototype.hide=function(){this.renderer.hide()},AnimationItem.prototype.show=function(){this.renderer.show()},AnimationItem.prototype.getDuration=function(e){return e?this.totalFrames:this.totalFrames/this.frameRate},AnimationItem.prototype.updateDocumentData=function(e,r,n){try{var a=this.renderer.getElementByPath(e);a.updateDocumentData(r,n)}catch{}},AnimationItem.prototype.trigger=function(e){if(this._cbs&&this._cbs[e])switch(e){case"enterFrame":this.triggerEvent(e,new BMEnterFrameEvent(e,this.currentFrame,this.totalFrames,this.frameModifier));break;case"drawnFrame":this.drawnFrameEvent.currentTime=this.currentFrame,this.drawnFrameEvent.totalTime=this.totalFrames,this.drawnFrameEvent.direction=this.frameModifier,this.triggerEvent(e,this.drawnFrameEvent);break;case"loopComplete":this.triggerEvent(e,new BMCompleteLoopEvent(e,this.loop,this.playCount,this.frameMult));break;case"complete":this.triggerEvent(e,new BMCompleteEvent(e,this.frameMult));break;case"segmentStart":this.triggerEvent(e,new BMSegmentStartEvent(e,this.firstFrame,this.totalFrames));break;case"destroy":this.triggerEvent(e,new BMDestroyEvent(e,this));break;default:this.triggerEvent(e)}e==="enterFrame"&&this.onEnterFrame&&this.onEnterFrame.call(this,new BMEnterFrameEvent(e,this.currentFrame,this.totalFrames,this.frameMult)),e==="loopComplete"&&this.onLoopComplete&&this.onLoopComplete.call(this,new BMCompleteLoopEvent(e,this.loop,this.playCount,this.frameMult)),e==="complete"&&this.onComplete&&this.onComplete.call(this,new BMCompleteEvent(e,this.frameMult)),e==="segmentStart"&&this.onSegmentStart&&this.onSegmentStart.call(this,new BMSegmentStartEvent(e,this.firstFrame,this.totalFrames)),e==="destroy"&&this.onDestroy&&this.onDestroy.call(this,new BMDestroyEvent(e,this))},AnimationItem.prototype.triggerRenderFrameError=function(e){var r=new BMRenderFrameErrorEvent(e,this.currentFrame);this.triggerEvent("error",r),this.onError&&this.onError.call(this,r)},AnimationItem.prototype.triggerConfigError=function(e){var r=new BMConfigErrorEvent(e,this.currentFrame);this.triggerEvent("error",r),this.onError&&this.onError.call(this,r)};var animationManager=function(){var e={},r=[],n=0,a=0,l=0,c=!0,u=!1;function d(L){for(var D=0,F=L.target;D<a;)r[D].animation===F&&(r.splice(D,1),D-=1,a-=1,F.isPaused||_()),D+=1}function m(L,D){if(!L)return null;for(var F=0;F<a;){if(r[F].elem===L&&r[F].elem!==null)return r[F].animation;F+=1}var $=new AnimationItem;return T($,L),$.setData(L,D),$}function b(){var L,D=r.length,F=[];for(L=0;L<D;L+=1)F.push(r[L].animation);return F}function A(){l+=1,B()}function _(){l-=1}function T(L,D){L.addEventListener("destroy",d),L.addEventListener("_active",A),L.addEventListener("_idle",_),r.push({elem:D,animation:L}),a+=1}function C(L){var D=new AnimationItem;return T(D,null),D.setParams(L),D}function S(L,D){var F;for(F=0;F<a;F+=1)r[F].animation.setSpeed(L,D)}function P(L,D){var F;for(F=0;F<a;F+=1)r[F].animation.setDirection(L,D)}function k(L){var D;for(D=0;D<a;D+=1)r[D].animation.play(L)}function x(L){var D=L-n,F;for(F=0;F<a;F+=1)r[F].animation.advanceTime(D);n=L,l&&!u?window.requestAnimationFrame(x):c=!0}function E(L){n=L,window.requestAnimationFrame(x)}function g(L){var D;for(D=0;D<a;D+=1)r[D].animation.pause(L)}function y(L,D,F){var $;for($=0;$<a;$+=1)r[$].animation.goToAndStop(L,D,F)}function I(L){var D;for(D=0;D<a;D+=1)r[D].animation.stop(L)}function M(L){var D;for(D=0;D<a;D+=1)r[D].animation.togglePause(L)}function j(L){var D;for(D=a-1;D>=0;D-=1)r[D].animation.destroy(L)}function O(L,D,F){var $=[].concat([].slice.call(document.getElementsByClassName("lottie")),[].slice.call(document.getElementsByClassName("bodymovin"))),U,z=$.length;for(U=0;U<z;U+=1)F&&$[U].setAttribute("data-bm-type",F),m($[U],L);if(D&&z===0){F||(F="svg");var K=document.getElementsByTagName("body")[0];K.innerText="";var Z=createTag("div");Z.style.width="100%",Z.style.height="100%",Z.setAttribute("data-bm-type",F),K.appendChild(Z),m(Z,L)}}function R(){var L;for(L=0;L<a;L+=1)r[L].animation.resize()}function B(){!u&&l&&c&&(window.requestAnimationFrame(E),c=!1)}function G(){u=!0}function V(){u=!1,B()}function Q(L,D){var F;for(F=0;F<a;F+=1)r[F].animation.setVolume(L,D)}function W(L){var D;for(D=0;D<a;D+=1)r[D].animation.mute(L)}function q(L){var D;for(D=0;D<a;D+=1)r[D].animation.unmute(L)}return e.registerAnimation=m,e.loadAnimation=C,e.setSpeed=S,e.setDirection=P,e.play=k,e.pause=g,e.stop=I,e.togglePause=M,e.searchAnimations=O,e.resize=R,e.goToAndStop=y,e.destroy=j,e.freeze=G,e.unfreeze=V,e.setVolume=Q,e.mute=W,e.unmute=q,e.getRegisteredAnimations=b,e}(),BezierFactory=function(){var e={};e.getBezierEasing=n;var r={};function n(E,g,y,I,M){var j=M||("bez_"+E+"_"+g+"_"+y+"_"+I).replace(/\./g,"p");if(r[j])return r[j];var O=new x([E,g,y,I]);return r[j]=O,O}var a=4,l=.001,c=1e-7,u=10,d=11,m=1/(d-1),b=typeof Float32Array=="function";function A(E,g){return 1-3*g+3*E}function _(E,g){return 3*g-6*E}function T(E){return 3*E}function C(E,g,y){return((A(g,y)*E+_(g,y))*E+T(g))*E}function S(E,g,y){return 3*A(g,y)*E*E+2*_(g,y)*E+T(g)}function P(E,g,y,I,M){var j,O,R=0;do O=g+(y-g)/2,j=C(O,I,M)-E,j>0?y=O:g=O;while(Math.abs(j)>c&&++R<u);return O}function k(E,g,y,I){for(var M=0;M<a;++M){var j=S(g,y,I);if(j===0)return g;var O=C(g,y,I)-E;g-=O/j}return g}function x(E){this._p=E,this._mSampleValues=b?new Float32Array(d):new Array(d),this._precomputed=!1,this.get=this.get.bind(this)}return x.prototype={get:function(g){var y=this._p[0],I=this._p[1],M=this._p[2],j=this._p[3];return this._precomputed||this._precompute(),y===I&&M===j?g:g===0?0:g===1?1:C(this._getTForX(g),I,j)},_precompute:function(){var g=this._p[0],y=this._p[1],I=this._p[2],M=this._p[3];this._precomputed=!0,(g!==y||I!==M)&&this._calcSampleValues()},_calcSampleValues:function(){for(var g=this._p[0],y=this._p[2],I=0;I<d;++I)this._mSampleValues[I]=C(I*m,g,y)},_getTForX:function(g){for(var y=this._p[0],I=this._p[2],M=this._mSampleValues,j=0,O=1,R=d-1;O!==R&&M[O]<=g;++O)j+=m;--O;var B=(g-M[O])/(M[O+1]-M[O]),G=j+B*m,V=S(G,y,I);return V>=l?k(g,G,y,I):V===0?G:P(g,j,j+m,y,I)}},e}(),pooling=function(){function e(r){return r.concat(createSizedArray(r.length))}return{double:e}}(),poolFactory=function(){return function(e,r,n){var a=0,l=e,c=createSizedArray(l),u={newElement:d,release:m};function d(){var b;return a?(a-=1,b=c[a]):b=r(),b}function m(b){a===l&&(c=pooling.double(c),l*=2),n&&n(b),c[a]=b,a+=1}return u}}(),bezierLengthPool=function(){function e(){return{addedLength:0,percents:createTypedArray("float32",getDefaultCurveSegments()),lengths:createTypedArray("float32",getDefaultCurveSegments())}}return poolFactory(8,e)}(),segmentsLengthPool=function(){function e(){return{lengths:[],totalLength:0}}function r(n){var a,l=n.lengths.length;for(a=0;a<l;a+=1)bezierLengthPool.release(n.lengths[a]);n.lengths.length=0}return poolFactory(8,e,r)}();function bezFunction(){var e=Math;function r(T,C,S,P,k,x){var E=T*P+C*k+S*x-k*P-x*T-S*C;return E>-.001&&E<.001}function n(T,C,S,P,k,x,E,g,y){if(S===0&&x===0&&y===0)return r(T,C,P,k,E,g);var I=e.sqrt(e.pow(P-T,2)+e.pow(k-C,2)+e.pow(x-S,2)),M=e.sqrt(e.pow(E-T,2)+e.pow(g-C,2)+e.pow(y-S,2)),j=e.sqrt(e.pow(E-P,2)+e.pow(g-k,2)+e.pow(y-x,2)),O;return I>M?I>j?O=I-M-j:O=j-M-I:j>M?O=j-M-I:O=M-I-j,O>-1e-4&&O<1e-4}var a=function(){return function(T,C,S,P){var k=getDefaultCurveSegments(),x,E,g,y,I,M=0,j,O=[],R=[],B=bezierLengthPool.newElement();for(g=S.length,x=0;x<k;x+=1){for(I=x/(k-1),j=0,E=0;E<g;E+=1)y=bmPow(1-I,3)*T[E]+3*bmPow(1-I,2)*I*S[E]+3*(1-I)*bmPow(I,2)*P[E]+bmPow(I,3)*C[E],O[E]=y,R[E]!==null&&(j+=bmPow(O[E]-R[E],2)),R[E]=O[E];j&&(j=bmSqrt(j),M+=j),B.percents[x]=I,B.lengths[x]=M}return B.addedLength=M,B}}();function l(T){var C=segmentsLengthPool.newElement(),S=T.c,P=T.v,k=T.o,x=T.i,E,g=T._length,y=C.lengths,I=0;for(E=0;E<g-1;E+=1)y[E]=a(P[E],P[E+1],k[E],x[E+1]),I+=y[E].addedLength;return S&&g&&(y[E]=a(P[E],P[0],k[E],x[0]),I+=y[E].addedLength),C.totalLength=I,C}function c(T){this.segmentLength=0,this.points=new Array(T)}function u(T,C){this.partialLength=T,this.point=C}var d=function(){var T={};return function(C,S,P,k){var x=(C[0]+"_"+C[1]+"_"+S[0]+"_"+S[1]+"_"+P[0]+"_"+P[1]+"_"+k[0]+"_"+k[1]).replace(/\./g,"p");if(!T[x]){var E=getDefaultCurveSegments(),g,y,I,M,j,O=0,R,B,G=null;C.length===2&&(C[0]!==S[0]||C[1]!==S[1])&&r(C[0],C[1],S[0],S[1],C[0]+P[0],C[1]+P[1])&&r(C[0],C[1],S[0],S[1],S[0]+k[0],S[1]+k[1])&&(E=2);var V=new c(E);for(I=P.length,g=0;g<E;g+=1){for(B=createSizedArray(I),j=g/(E-1),R=0,y=0;y<I;y+=1)M=bmPow(1-j,3)*C[y]+3*bmPow(1-j,2)*j*(C[y]+P[y])+3*(1-j)*bmPow(j,2)*(S[y]+k[y])+bmPow(j,3)*S[y],B[y]=M,G!==null&&(R+=bmPow(B[y]-G[y],2));R=bmSqrt(R),O+=R,V.points[g]=new u(R,B),G=B}V.segmentLength=O,T[x]=V}return T[x]}}();function m(T,C){var S=C.percents,P=C.lengths,k=S.length,x=bmFloor((k-1)*T),E=T*C.addedLength,g=0;if(x===k-1||x===0||E===P[x])return S[x];for(var y=P[x]>E?-1:1,I=!0;I;)if(P[x]<=E&&P[x+1]>E?(g=(E-P[x])/(P[x+1]-P[x]),I=!1):x+=y,x<0||x>=k-1){if(x===k-1)return S[x];I=!1}return S[x]+(S[x+1]-S[x])*g}function b(T,C,S,P,k,x){var E=m(k,x),g=1-E,y=e.round((g*g*g*T[0]+(E*g*g+g*E*g+g*g*E)*S[0]+(E*E*g+g*E*E+E*g*E)*P[0]+E*E*E*C[0])*1e3)/1e3,I=e.round((g*g*g*T[1]+(E*g*g+g*E*g+g*g*E)*S[1]+(E*E*g+g*E*E+E*g*E)*P[1]+E*E*E*C[1])*1e3)/1e3;return[y,I]}var A=createTypedArray("float32",8);function _(T,C,S,P,k,x,E){k<0?k=0:k>1&&(k=1);var g=m(k,E);x=x>1?1:x;var y=m(x,E),I,M=T.length,j=1-g,O=1-y,R=j*j*j,B=g*j*j*3,G=g*g*j*3,V=g*g*g,Q=j*j*O,W=g*j*O+j*g*O+j*j*y,q=g*g*O+j*g*y+g*j*y,L=g*g*y,D=j*O*O,F=g*O*O+j*y*O+j*O*y,$=g*y*O+j*y*y+g*O*y,U=g*y*y,z=O*O*O,K=y*O*O+O*y*O+O*O*y,Z=y*y*O+O*y*y+y*O*y,Y=y*y*y;for(I=0;I<M;I+=1)A[I*4]=e.round((R*T[I]+B*S[I]+G*P[I]+V*C[I])*1e3)/1e3,A[I*4+1]=e.round((Q*T[I]+W*S[I]+q*P[I]+L*C[I])*1e3)/1e3,A[I*4+2]=e.round((D*T[I]+F*S[I]+$*P[I]+U*C[I])*1e3)/1e3,A[I*4+3]=e.round((z*T[I]+K*S[I]+Z*P[I]+Y*C[I])*1e3)/1e3;return A}return{getSegmentsLength:l,getNewSegment:_,getPointInSegment:b,buildBezierData:d,pointOnLine2D:r,pointOnLine3D:n}}var bez=bezFunction(),PropertyFactory=function(){var e=initialDefaultFrame,r=Math.abs;function n(k,x){var E=this.offsetTime,g;this.propType==="multidimensional"&&(g=createTypedArray("float32",this.pv.length));for(var y=x.lastIndex,I=y,M=this.keyframes.length-1,j=!0,O,R,B;j;){if(O=this.keyframes[I],R=this.keyframes[I+1],I===M-1&&k>=R.t-E){O.h&&(O=R),y=0;break}if(R.t-E>k){y=I;break}I<M-1?I+=1:(y=0,j=!1)}B=this.keyframesMetadata[I]||{};var G,V,Q,W,q,L,D=R.t-E,F=O.t-E,$;if(O.to){B.bezierData||(B.bezierData=bez.buildBezierData(O.s,R.s||O.e,O.to,O.ti));var U=B.bezierData;if(k>=D||k<F){var z=k>=D?U.points.length-1:0;for(V=U.points[z].point.length,G=0;G<V;G+=1)g[G]=U.points[z].point[G]}else{B.__fnct?L=B.__fnct:(L=BezierFactory.getBezierEasing(O.o.x,O.o.y,O.i.x,O.i.y,O.n).get,B.__fnct=L),Q=L((k-F)/(D-F));var K=U.segmentLength*Q,Z,Y=x.lastFrame<k&&x._lastKeyframeIndex===I?x._lastAddedLength:0;for(q=x.lastFrame<k&&x._lastKeyframeIndex===I?x._lastPoint:0,j=!0,W=U.points.length;j;){if(Y+=U.points[q].partialLength,K===0||Q===0||q===U.points.length-1){for(V=U.points[q].point.length,G=0;G<V;G+=1)g[G]=U.points[q].point[G];break}else if(K>=Y&&K<Y+U.points[q+1].partialLength){for(Z=(K-Y)/U.points[q+1].partialLength,V=U.points[q].point.length,G=0;G<V;G+=1)g[G]=U.points[q].point[G]+(U.points[q+1].point[G]-U.points[q].point[G])*Z;break}q<W-1?q+=1:j=!1}x._lastPoint=q,x._lastAddedLength=Y-U.points[q].partialLength,x._lastKeyframeIndex=I}}else{var le,re,ee,oe,me;if(M=O.s.length,$=R.s||O.e,this.sh&&O.h!==1)if(k>=D)g[0]=$[0],g[1]=$[1],g[2]=$[2];else if(k<=F)g[0]=O.s[0],g[1]=O.s[1],g[2]=O.s[2];else{var ne=c(O.s),ce=c($),fe=(k-F)/(D-F);l(g,a(ne,ce,fe))}else for(I=0;I<M;I+=1)O.h!==1&&(k>=D?Q=1:k<F?Q=0:(O.o.x.constructor===Array?(B.__fnct||(B.__fnct=[]),B.__fnct[I]?L=B.__fnct[I]:(le=O.o.x[I]===void 0?O.o.x[0]:O.o.x[I],re=O.o.y[I]===void 0?O.o.y[0]:O.o.y[I],ee=O.i.x[I]===void 0?O.i.x[0]:O.i.x[I],oe=O.i.y[I]===void 0?O.i.y[0]:O.i.y[I],L=BezierFactory.getBezierEasing(le,re,ee,oe).get,B.__fnct[I]=L)):B.__fnct?L=B.__fnct:(le=O.o.x,re=O.o.y,ee=O.i.x,oe=O.i.y,L=BezierFactory.getBezierEasing(le,re,ee,oe).get,O.keyframeMetadata=L),Q=L((k-F)/(D-F)))),$=R.s||O.e,me=O.h===1?O.s[I]:O.s[I]+($[I]-O.s[I])*Q,this.propType==="multidimensional"?g[I]=me:g=me}return x.lastIndex=y,g}function a(k,x,E){var g=[],y=k[0],I=k[1],M=k[2],j=k[3],O=x[0],R=x[1],B=x[2],G=x[3],V,Q,W,q,L;return Q=y*O+I*R+M*B+j*G,Q<0&&(Q=-Q,O=-O,R=-R,B=-B,G=-G),1-Q>1e-6?(V=Math.acos(Q),W=Math.sin(V),q=Math.sin((1-E)*V)/W,L=Math.sin(E*V)/W):(q=1-E,L=E),g[0]=q*y+L*O,g[1]=q*I+L*R,g[2]=q*M+L*B,g[3]=q*j+L*G,g}function l(k,x){var E=x[0],g=x[1],y=x[2],I=x[3],M=Math.atan2(2*g*I-2*E*y,1-2*g*g-2*y*y),j=Math.asin(2*E*g+2*y*I),O=Math.atan2(2*E*I-2*g*y,1-2*E*E-2*y*y);k[0]=M/degToRads,k[1]=j/degToRads,k[2]=O/degToRads}function c(k){var x=k[0]*degToRads,E=k[1]*degToRads,g=k[2]*degToRads,y=Math.cos(x/2),I=Math.cos(E/2),M=Math.cos(g/2),j=Math.sin(x/2),O=Math.sin(E/2),R=Math.sin(g/2),B=y*I*M-j*O*R,G=j*O*M+y*I*R,V=j*I*M+y*O*R,Q=y*O*M-j*I*R;return[G,V,Q,B]}function u(){var k=this.comp.renderedFrame-this.offsetTime,x=this.keyframes[0].t-this.offsetTime,E=this.keyframes[this.keyframes.length-1].t-this.offsetTime;if(!(k===this._caching.lastFrame||this._caching.lastFrame!==e&&(this._caching.lastFrame>=E&&k>=E||this._caching.lastFrame<x&&k<x))){this._caching.lastFrame>=k&&(this._caching._lastKeyframeIndex=-1,this._caching.lastIndex=0);var g=this.interpolateValue(k,this._caching);this.pv=g}return this._caching.lastFrame=k,this.pv}function d(k){var x;if(this.propType==="unidimensional")x=k*this.mult,r(this.v-x)>1e-5&&(this.v=x,this._mdf=!0);else for(var E=0,g=this.v.length;E<g;)x=k[E]*this.mult,r(this.v[E]-x)>1e-5&&(this.v[E]=x,this._mdf=!0),E+=1}function m(){if(!(this.elem.globalData.frameId===this.frameId||!this.effectsSequence.length)){if(this.lock){this.setVValue(this.pv);return}this.lock=!0,this._mdf=this._isFirstFrame;var k,x=this.effectsSequence.length,E=this.kf?this.pv:this.data.k;for(k=0;k<x;k+=1)E=this.effectsSequence[k](E);this.setVValue(E),this._isFirstFrame=!1,this.lock=!1,this.frameId=this.elem.globalData.frameId}}function b(k){this.effectsSequence.push(k),this.container.addDynamicProperty(this)}function A(k,x,E,g){this.propType="unidimensional",this.mult=E||1,this.data=x,this.v=E?x.k*E:x.k,this.pv=x.k,this._mdf=!1,this.elem=k,this.container=g,this.comp=k.comp,this.k=!1,this.kf=!1,this.vel=0,this.effectsSequence=[],this._isFirstFrame=!0,this.getValue=m,this.setVValue=d,this.addEffect=b}function _(k,x,E,g){this.propType="multidimensional",this.mult=E||1,this.data=x,this._mdf=!1,this.elem=k,this.container=g,this.comp=k.comp,this.k=!1,this.kf=!1,this.frameId=-1;var y,I=x.k.length;for(this.v=createTypedArray("float32",I),this.pv=createTypedArray("float32",I),this.vel=createTypedArray("float32",I),y=0;y<I;y+=1)this.v[y]=x.k[y]*this.mult,this.pv[y]=x.k[y];this._isFirstFrame=!0,this.effectsSequence=[],this.getValue=m,this.setVValue=d,this.addEffect=b}function T(k,x,E,g){this.propType="unidimensional",this.keyframes=x.k,this.keyframesMetadata=[],this.offsetTime=k.data.st,this.frameId=-1,this._caching={lastFrame:e,lastIndex:0,value:0,_lastKeyframeIndex:-1},this.k=!0,this.kf=!0,this.data=x,this.mult=E||1,this.elem=k,this.container=g,this.comp=k.comp,this.v=e,this.pv=e,this._isFirstFrame=!0,this.getValue=m,this.setVValue=d,this.interpolateValue=n,this.effectsSequence=[u.bind(this)],this.addEffect=b}function C(k,x,E,g){this.propType="multidimensional";var y,I=x.k.length,M,j,O,R;for(y=0;y<I-1;y+=1)x.k[y].to&&x.k[y].s&&x.k[y+1]&&x.k[y+1].s&&(M=x.k[y].s,j=x.k[y+1].s,O=x.k[y].to,R=x.k[y].ti,(M.length===2&&!(M[0]===j[0]&&M[1]===j[1])&&bez.pointOnLine2D(M[0],M[1],j[0],j[1],M[0]+O[0],M[1]+O[1])&&bez.pointOnLine2D(M[0],M[1],j[0],j[1],j[0]+R[0],j[1]+R[1])||M.length===3&&!(M[0]===j[0]&&M[1]===j[1]&&M[2]===j[2])&&bez.pointOnLine3D(M[0],M[1],M[2],j[0],j[1],j[2],M[0]+O[0],M[1]+O[1],M[2]+O[2])&&bez.pointOnLine3D(M[0],M[1],M[2],j[0],j[1],j[2],j[0]+R[0],j[1]+R[1],j[2]+R[2]))&&(x.k[y].to=null,x.k[y].ti=null),M[0]===j[0]&&M[1]===j[1]&&O[0]===0&&O[1]===0&&R[0]===0&&R[1]===0&&(M.length===2||M[2]===j[2]&&O[2]===0&&R[2]===0)&&(x.k[y].to=null,x.k[y].ti=null));this.effectsSequence=[u.bind(this)],this.data=x,this.keyframes=x.k,this.keyframesMetadata=[],this.offsetTime=k.data.st,this.k=!0,this.kf=!0,this._isFirstFrame=!0,this.mult=E||1,this.elem=k,this.container=g,this.comp=k.comp,this.getValue=m,this.setVValue=d,this.interpolateValue=n,this.frameId=-1;var B=x.k[0].s.length;for(this.v=createTypedArray("float32",B),this.pv=createTypedArray("float32",B),y=0;y<B;y+=1)this.v[y]=e,this.pv[y]=e;this._caching={lastFrame:e,lastIndex:0,value:createTypedArray("float32",B)},this.addEffect=b}function S(k,x,E,g,y){var I;if(!x.k.length)I=new A(k,x,g,y);else if(typeof x.k[0]=="number")I=new _(k,x,g,y);else switch(E){case 0:I=new T(k,x,g,y);break;case 1:I=new C(k,x,g,y);break}return I.effectsSequence.length&&y.addDynamicProperty(I),I}var P={getProp:S};return P}();function DynamicPropertyContainer(){}DynamicPropertyContainer.prototype={addDynamicProperty:function(r){this.dynamicProperties.indexOf(r)===-1&&(this.dynamicProperties.push(r),this.container.addDynamicProperty(this),this._isAnimated=!0)},iterateDynamicProperties:function(){this._mdf=!1;var r,n=this.dynamicProperties.length;for(r=0;r<n;r+=1)this.dynamicProperties[r].getValue(),this.dynamicProperties[r]._mdf&&(this._mdf=!0)},initDynamicPropertyContainer:function(r){this.container=r,this.dynamicProperties=[],this._mdf=!1,this._isAnimated=!1}};var pointPool=function(){function e(){return createTypedArray("float32",2)}return poolFactory(8,e)}();function ShapePath(){this.c=!1,this._length=0,this._maxLength=8,this.v=createSizedArray(this._maxLength),this.o=createSizedArray(this._maxLength),this.i=createSizedArray(this._maxLength)}ShapePath.prototype.setPathData=function(e,r){this.c=e,this.setLength(r);for(var n=0;n<r;)this.v[n]=pointPool.newElement(),this.o[n]=pointPool.newElement(),this.i[n]=pointPool.newElement(),n+=1},ShapePath.prototype.setLength=function(e){for(;this._maxLength<e;)this.doubleArrayLength();this._length=e},ShapePath.prototype.doubleArrayLength=function(){this.v=this.v.concat(createSizedArray(this._maxLength)),this.i=this.i.concat(createSizedArray(this._maxLength)),this.o=this.o.concat(createSizedArray(this._maxLength)),this._maxLength*=2},ShapePath.prototype.setXYAt=function(e,r,n,a,l){var c;switch(this._length=Math.max(this._length,a+1),this._length>=this._maxLength&&this.doubleArrayLength(),n){case"v":c=this.v;break;case"i":c=this.i;break;case"o":c=this.o;break;default:c=[];break}(!c[a]||c[a]&&!l)&&(c[a]=pointPool.newElement()),c[a][0]=e,c[a][1]=r},ShapePath.prototype.setTripleAt=function(e,r,n,a,l,c,u,d){this.setXYAt(e,r,"v",u,d),this.setXYAt(n,a,"o",u,d),this.setXYAt(l,c,"i",u,d)},ShapePath.prototype.reverse=function(){var e=new ShapePath;e.setPathData(this.c,this._length);var r=this.v,n=this.o,a=this.i,l=0;this.c&&(e.setTripleAt(r[0][0],r[0][1],a[0][0],a[0][1],n[0][0],n[0][1],0,!1),l=1);var c=this._length-1,u=this._length,d;for(d=l;d<u;d+=1)e.setTripleAt(r[c][0],r[c][1],a[c][0],a[c][1],n[c][0],n[c][1],d,!1),c-=1;return e};var shapePool=function(){function e(){return new ShapePath}function r(l){var c=l._length,u;for(u=0;u<c;u+=1)pointPool.release(l.v[u]),pointPool.release(l.i[u]),pointPool.release(l.o[u]),l.v[u]=null,l.i[u]=null,l.o[u]=null;l._length=0,l.c=!1}function n(l){var c=a.newElement(),u,d=l._length===void 0?l.v.length:l._length;for(c.setLength(d),c.c=l.c,u=0;u<d;u+=1)c.setTripleAt(l.v[u][0],l.v[u][1],l.o[u][0],l.o[u][1],l.i[u][0],l.i[u][1],u);return c}var a=poolFactory(4,e,r);return a.clone=n,a}();function ShapeCollection(){this._length=0,this._maxLength=4,this.shapes=createSizedArray(this._maxLength)}ShapeCollection.prototype.addShape=function(e){this._length===this._maxLength&&(this.shapes=this.shapes.concat(createSizedArray(this._maxLength)),this._maxLength*=2),this.shapes[this._length]=e,this._length+=1},ShapeCollection.prototype.releaseShapes=function(){var e;for(e=0;e<this._length;e+=1)shapePool.release(this.shapes[e]);this._length=0};var shapeCollectionPool=function(){var e={newShapeCollection:l,release:c},r=0,n=4,a=createSizedArray(n);function l(){var u;return r?(r-=1,u=a[r]):u=new ShapeCollection,u}function c(u){var d,m=u._length;for(d=0;d<m;d+=1)shapePool.release(u.shapes[d]);u._length=0,r===n&&(a=pooling.double(a),n*=2),a[r]=u,r+=1}return e}(),ShapePropertyFactory=function(){var e=-999999;function r(x,E,g){var y=g.lastIndex,I,M,j,O,R,B,G,V,Q,W=this.keyframes;if(x<W[0].t-this.offsetTime)I=W[0].s[0],j=!0,y=0;else if(x>=W[W.length-1].t-this.offsetTime)I=W[W.length-1].s?W[W.length-1].s[0]:W[W.length-2].e[0],j=!0;else{for(var q=y,L=W.length-1,D=!0,F,$,U;D&&(F=W[q],$=W[q+1],!($.t-this.offsetTime>x));)q<L-1?q+=1:D=!1;if(U=this.keyframesMetadata[q]||{},j=F.h===1,y=q,!j){if(x>=$.t-this.offsetTime)V=1;else if(x<F.t-this.offsetTime)V=0;else{var z;U.__fnct?z=U.__fnct:(z=BezierFactory.getBezierEasing(F.o.x,F.o.y,F.i.x,F.i.y).get,U.__fnct=z),V=z((x-(F.t-this.offsetTime))/($.t-this.offsetTime-(F.t-this.offsetTime)))}M=$.s?$.s[0]:F.e[0]}I=F.s[0]}for(B=E._length,G=I.i[0].length,g.lastIndex=y,O=0;O<B;O+=1)for(R=0;R<G;R+=1)Q=j?I.i[O][R]:I.i[O][R]+(M.i[O][R]-I.i[O][R])*V,E.i[O][R]=Q,Q=j?I.o[O][R]:I.o[O][R]+(M.o[O][R]-I.o[O][R])*V,E.o[O][R]=Q,Q=j?I.v[O][R]:I.v[O][R]+(M.v[O][R]-I.v[O][R])*V,E.v[O][R]=Q}function n(){var x=this.comp.renderedFrame-this.offsetTime,E=this.keyframes[0].t-this.offsetTime,g=this.keyframes[this.keyframes.length-1].t-this.offsetTime,y=this._caching.lastFrame;return y!==e&&(y<E&&x<E||y>g&&x>g)||(this._caching.lastIndex=y<x?this._caching.lastIndex:0,this.interpolateShape(x,this.pv,this._caching)),this._caching.lastFrame=x,this.pv}function a(){this.paths=this.localShapeCollection}function l(x,E){if(x._length!==E._length||x.c!==E.c)return!1;var g,y=x._length;for(g=0;g<y;g+=1)if(x.v[g][0]!==E.v[g][0]||x.v[g][1]!==E.v[g][1]||x.o[g][0]!==E.o[g][0]||x.o[g][1]!==E.o[g][1]||x.i[g][0]!==E.i[g][0]||x.i[g][1]!==E.i[g][1])return!1;return!0}function c(x){l(this.v,x)||(this.v=shapePool.clone(x),this.localShapeCollection.releaseShapes(),this.localShapeCollection.addShape(this.v),this._mdf=!0,this.paths=this.localShapeCollection)}function u(){if(this.elem.globalData.frameId!==this.frameId){if(!this.effectsSequence.length){this._mdf=!1;return}if(this.lock){this.setVValue(this.pv);return}this.lock=!0,this._mdf=!1;var x;this.kf?x=this.pv:this.data.ks?x=this.data.ks.k:x=this.data.pt.k;var E,g=this.effectsSequence.length;for(E=0;E<g;E+=1)x=this.effectsSequence[E](x);this.setVValue(x),this.lock=!1,this.frameId=this.elem.globalData.frameId}}function d(x,E,g){this.propType="shape",this.comp=x.comp,this.container=x,this.elem=x,this.data=E,this.k=!1,this.kf=!1,this._mdf=!1;var y=g===3?E.pt.k:E.ks.k;this.v=shapePool.clone(y),this.pv=shapePool.clone(this.v),this.localShapeCollection=shapeCollectionPool.newShapeCollection(),this.paths=this.localShapeCollection,this.paths.addShape(this.v),this.reset=a,this.effectsSequence=[]}function m(x){this.effectsSequence.push(x),this.container.addDynamicProperty(this)}d.prototype.interpolateShape=r,d.prototype.getValue=u,d.prototype.setVValue=c,d.prototype.addEffect=m;function b(x,E,g){this.propType="shape",this.comp=x.comp,this.elem=x,this.container=x,this.offsetTime=x.data.st,this.keyframes=g===3?E.pt.k:E.ks.k,this.keyframesMetadata=[],this.k=!0,this.kf=!0;var y=this.keyframes[0].s[0].i.length;this.v=shapePool.newElement(),this.v.setPathData(this.keyframes[0].s[0].c,y),this.pv=shapePool.clone(this.v),this.localShapeCollection=shapeCollectionPool.newShapeCollection(),this.paths=this.localShapeCollection,this.paths.addShape(this.v),this.lastFrame=e,this.reset=a,this._caching={lastFrame:e,lastIndex:0},this.effectsSequence=[n.bind(this)]}b.prototype.getValue=u,b.prototype.interpolateShape=r,b.prototype.setVValue=c,b.prototype.addEffect=m;var A=function(){var x=roundCorner;function E(g,y){this.v=shapePool.newElement(),this.v.setPathData(!0,4),this.localShapeCollection=shapeCollectionPool.newShapeCollection(),this.paths=this.localShapeCollection,this.localShapeCollection.addShape(this.v),this.d=y.d,this.elem=g,this.comp=g.comp,this.frameId=-1,this.initDynamicPropertyContainer(g),this.p=PropertyFactory.getProp(g,y.p,1,0,this),this.s=PropertyFactory.getProp(g,y.s,1,0,this),this.dynamicProperties.length?this.k=!0:(this.k=!1,this.convertEllToPath())}return E.prototype={reset:a,getValue:function(){this.elem.globalData.frameId!==this.frameId&&(this.frameId=this.elem.globalData.frameId,this.iterateDynamicProperties(),this._mdf&&this.convertEllToPath())},convertEllToPath:function(){var y=this.p.v[0],I=this.p.v[1],M=this.s.v[0]/2,j=this.s.v[1]/2,O=this.d!==3,R=this.v;R.v[0][0]=y,R.v[0][1]=I-j,R.v[1][0]=O?y+M:y-M,R.v[1][1]=I,R.v[2][0]=y,R.v[2][1]=I+j,R.v[3][0]=O?y-M:y+M,R.v[3][1]=I,R.i[0][0]=O?y-M*x:y+M*x,R.i[0][1]=I-j,R.i[1][0]=O?y+M:y-M,R.i[1][1]=I-j*x,R.i[2][0]=O?y+M*x:y-M*x,R.i[2][1]=I+j,R.i[3][0]=O?y-M:y+M,R.i[3][1]=I+j*x,R.o[0][0]=O?y+M*x:y-M*x,R.o[0][1]=I-j,R.o[1][0]=O?y+M:y-M,R.o[1][1]=I+j*x,R.o[2][0]=O?y-M*x:y+M*x,R.o[2][1]=I+j,R.o[3][0]=O?y-M:y+M,R.o[3][1]=I-j*x}},extendPrototype([DynamicPropertyContainer],E),E}(),_=function(){function x(E,g){this.v=shapePool.newElement(),this.v.setPathData(!0,0),this.elem=E,this.comp=E.comp,this.data=g,this.frameId=-1,this.d=g.d,this.initDynamicPropertyContainer(E),g.sy===1?(this.ir=PropertyFactory.getProp(E,g.ir,0,0,this),this.is=PropertyFactory.getProp(E,g.is,0,.01,this),this.convertToPath=this.convertStarToPath):this.convertToPath=this.convertPolygonToPath,this.pt=PropertyFactory.getProp(E,g.pt,0,0,this),this.p=PropertyFactory.getProp(E,g.p,1,0,this),this.r=PropertyFactory.getProp(E,g.r,0,degToRads,this),this.or=PropertyFactory.getProp(E,g.or,0,0,this),this.os=PropertyFactory.getProp(E,g.os,0,.01,this),this.localShapeCollection=shapeCollectionPool.newShapeCollection(),this.localShapeCollection.addShape(this.v),this.paths=this.localShapeCollection,this.dynamicProperties.length?this.k=!0:(this.k=!1,this.convertToPath())}return x.prototype={reset:a,getValue:function(){this.elem.globalData.frameId!==this.frameId&&(this.frameId=this.elem.globalData.frameId,this.iterateDynamicProperties(),this._mdf&&this.convertToPath())},convertStarToPath:function(){var g=Math.floor(this.pt.v)*2,y=Math.PI*2/g,I=!0,M=this.or.v,j=this.ir.v,O=this.os.v,R=this.is.v,B=2*Math.PI*M/(g*2),G=2*Math.PI*j/(g*2),V,Q,W,q,L=-Math.PI/2;L+=this.r.v;var D=this.data.d===3?-1:1;for(this.v._length=0,V=0;V<g;V+=1){Q=I?M:j,W=I?O:R,q=I?B:G;var F=Q*Math.cos(L),$=Q*Math.sin(L),U=F===0&&$===0?0:$/Math.sqrt(F*F+$*$),z=F===0&&$===0?0:-F/Math.sqrt(F*F+$*$);F+=+this.p.v[0],$+=+this.p.v[1],this.v.setTripleAt(F,$,F-U*q*W*D,$-z*q*W*D,F+U*q*W*D,$+z*q*W*D,V,!0),I=!I,L+=y*D}},convertPolygonToPath:function(){var g=Math.floor(this.pt.v),y=Math.PI*2/g,I=this.or.v,M=this.os.v,j=2*Math.PI*I/(g*4),O,R=-Math.PI*.5,B=this.data.d===3?-1:1;for(R+=this.r.v,this.v._length=0,O=0;O<g;O+=1){var G=I*Math.cos(R),V=I*Math.sin(R),Q=G===0&&V===0?0:V/Math.sqrt(G*G+V*V),W=G===0&&V===0?0:-G/Math.sqrt(G*G+V*V);G+=+this.p.v[0],V+=+this.p.v[1],this.v.setTripleAt(G,V,G-Q*j*M*B,V-W*j*M*B,G+Q*j*M*B,V+W*j*M*B,O,!0),R+=y*B}this.paths.length=0,this.paths[0]=this.v}},extendPrototype([DynamicPropertyContainer],x),x}(),T=function(){function x(E,g){this.v=shapePool.newElement(),this.v.c=!0,this.localShapeCollection=shapeCollectionPool.newShapeCollection(),this.localShapeCollection.addShape(this.v),this.paths=this.localShapeCollection,this.elem=E,this.comp=E.comp,this.frameId=-1,this.d=g.d,this.initDynamicPropertyContainer(E),this.p=PropertyFactory.getProp(E,g.p,1,0,this),this.s=PropertyFactory.getProp(E,g.s,1,0,this),this.r=PropertyFactory.getProp(E,g.r,0,0,this),this.dynamicProperties.length?this.k=!0:(this.k=!1,this.convertRectToPath())}return x.prototype={convertRectToPath:function(){var g=this.p.v[0],y=this.p.v[1],I=this.s.v[0]/2,M=this.s.v[1]/2,j=bmMin(I,M,this.r.v),O=j*(1-roundCorner);this.v._length=0,this.d===2||this.d===1?(this.v.setTripleAt(g+I,y-M+j,g+I,y-M+j,g+I,y-M+O,0,!0),this.v.setTripleAt(g+I,y+M-j,g+I,y+M-O,g+I,y+M-j,1,!0),j!==0?(this.v.setTripleAt(g+I-j,y+M,g+I-j,y+M,g+I-O,y+M,2,!0),this.v.setTripleAt(g-I+j,y+M,g-I+O,y+M,g-I+j,y+M,3,!0),this.v.setTripleAt(g-I,y+M-j,g-I,y+M-j,g-I,y+M-O,4,!0),this.v.setTripleAt(g-I,y-M+j,g-I,y-M+O,g-I,y-M+j,5,!0),this.v.setTripleAt(g-I+j,y-M,g-I+j,y-M,g-I+O,y-M,6,!0),this.v.setTripleAt(g+I-j,y-M,g+I-O,y-M,g+I-j,y-M,7,!0)):(this.v.setTripleAt(g-I,y+M,g-I+O,y+M,g-I,y+M,2),this.v.setTripleAt(g-I,y-M,g-I,y-M+O,g-I,y-M,3))):(this.v.setTripleAt(g+I,y-M+j,g+I,y-M+O,g+I,y-M+j,0,!0),j!==0?(this.v.setTripleAt(g+I-j,y-M,g+I-j,y-M,g+I-O,y-M,1,!0),this.v.setTripleAt(g-I+j,y-M,g-I+O,y-M,g-I+j,y-M,2,!0),this.v.setTripleAt(g-I,y-M+j,g-I,y-M+j,g-I,y-M+O,3,!0),this.v.setTripleAt(g-I,y+M-j,g-I,y+M-O,g-I,y+M-j,4,!0),this.v.setTripleAt(g-I+j,y+M,g-I+j,y+M,g-I+O,y+M,5,!0),this.v.setTripleAt(g+I-j,y+M,g+I-O,y+M,g+I-j,y+M,6,!0),this.v.setTripleAt(g+I,y+M-j,g+I,y+M-j,g+I,y+M-O,7,!0)):(this.v.setTripleAt(g-I,y-M,g-I+O,y-M,g-I,y-M,1,!0),this.v.setTripleAt(g-I,y+M,g-I,y+M-O,g-I,y+M,2,!0),this.v.setTripleAt(g+I,y+M,g+I-O,y+M,g+I,y+M,3,!0)))},getValue:function(){this.elem.globalData.frameId!==this.frameId&&(this.frameId=this.elem.globalData.frameId,this.iterateDynamicProperties(),this._mdf&&this.convertRectToPath())},reset:a},extendPrototype([DynamicPropertyContainer],x),x}();function C(x,E,g){var y;if(g===3||g===4){var I=g===3?E.pt:E.ks,M=I.k;M.length?y=new b(x,E,g):y=new d(x,E,g)}else g===5?y=new T(x,E):g===6?y=new A(x,E):g===7&&(y=new _(x,E));return y.k&&x.addDynamicProperty(y),y}function S(){return d}function P(){return b}var k={};return k.getShapeProp=C,k.getConstructorFunction=S,k.getKeyframedConstructorFunction=P,k}();/*!
 Transformation Matrix v2.0
 (c) Epistemex 2014-2015
 www.epistemex.com
 By Ken Fyrstenberg
 Contributions by leeoniya.
 License: MIT, header required.
 */var Matrix=function(){var e=Math.cos,r=Math.sin,n=Math.tan,a=Math.round;function l(){return this.props[0]=1,this.props[1]=0,this.props[2]=0,this.props[3]=0,this.props[4]=0,this.props[5]=1,this.props[6]=0,this.props[7]=0,this.props[8]=0,this.props[9]=0,this.props[10]=1,this.props[11]=0,this.props[12]=0,this.props[13]=0,this.props[14]=0,this.props[15]=1,this}function c(D){if(D===0)return this;var F=e(D),$=r(D);return this._t(F,-$,0,0,$,F,0,0,0,0,1,0,0,0,0,1)}function u(D){if(D===0)return this;var F=e(D),$=r(D);return this._t(1,0,0,0,0,F,-$,0,0,$,F,0,0,0,0,1)}function d(D){if(D===0)return this;var F=e(D),$=r(D);return this._t(F,0,$,0,0,1,0,0,-$,0,F,0,0,0,0,1)}function m(D){if(D===0)return this;var F=e(D),$=r(D);return this._t(F,-$,0,0,$,F,0,0,0,0,1,0,0,0,0,1)}function b(D,F){return this._t(1,F,D,1,0,0)}function A(D,F){return this.shear(n(D),n(F))}function _(D,F){var $=e(F),U=r(F);return this._t($,U,0,0,-U,$,0,0,0,0,1,0,0,0,0,1)._t(1,0,0,0,n(D),1,0,0,0,0,1,0,0,0,0,1)._t($,-U,0,0,U,$,0,0,0,0,1,0,0,0,0,1)}function T(D,F,$){return!$&&$!==0&&($=1),D===1&&F===1&&$===1?this:this._t(D,0,0,0,0,F,0,0,0,0,$,0,0,0,0,1)}function C(D,F,$,U,z,K,Z,Y,le,re,ee,oe,me,ne,ce,fe){return this.props[0]=D,this.props[1]=F,this.props[2]=$,this.props[3]=U,this.props[4]=z,this.props[5]=K,this.props[6]=Z,this.props[7]=Y,this.props[8]=le,this.props[9]=re,this.props[10]=ee,this.props[11]=oe,this.props[12]=me,this.props[13]=ne,this.props[14]=ce,this.props[15]=fe,this}function S(D,F,$){return $=$||0,D!==0||F!==0||$!==0?this._t(1,0,0,0,0,1,0,0,0,0,1,0,D,F,$,1):this}function P(D,F,$,U,z,K,Z,Y,le,re,ee,oe,me,ne,ce,fe){var X=this.props;if(D===1&&F===0&&$===0&&U===0&&z===0&&K===1&&Z===0&&Y===0&&le===0&&re===0&&ee===1&&oe===0)return X[12]=X[12]*D+X[15]*me,X[13]=X[13]*K+X[15]*ne,X[14]=X[14]*ee+X[15]*ce,X[15]*=fe,this._identityCalculated=!1,this;var de=X[0],se=X[1],te=X[2],pe=X[3],ge=X[4],Ce=X[5],he=X[6],Ee=X[7],_e=X[8],J=X[9],ae=X[10],ve=X[11],ye=X[12],be=X[13],Ie=X[14],Pe=X[15];return X[0]=de*D+se*z+te*le+pe*me,X[1]=de*F+se*K+te*re+pe*ne,X[2]=de*$+se*Z+te*ee+pe*ce,X[3]=de*U+se*Y+te*oe+pe*fe,X[4]=ge*D+Ce*z+he*le+Ee*me,X[5]=ge*F+Ce*K+he*re+Ee*ne,X[6]=ge*$+Ce*Z+he*ee+Ee*ce,X[7]=ge*U+Ce*Y+he*oe+Ee*fe,X[8]=_e*D+J*z+ae*le+ve*me,X[9]=_e*F+J*K+ae*re+ve*ne,X[10]=_e*$+J*Z+ae*ee+ve*ce,X[11]=_e*U+J*Y+ae*oe+ve*fe,X[12]=ye*D+be*z+Ie*le+Pe*me,X[13]=ye*F+be*K+Ie*re+Pe*ne,X[14]=ye*$+be*Z+Ie*ee+Pe*ce,X[15]=ye*U+be*Y+Ie*oe+Pe*fe,this._identityCalculated=!1,this}function k(){return this._identityCalculated||(this._identity=!(this.props[0]!==1||this.props[1]!==0||this.props[2]!==0||this.props[3]!==0||this.props[4]!==0||this.props[5]!==1||this.props[6]!==0||this.props[7]!==0||this.props[8]!==0||this.props[9]!==0||this.props[10]!==1||this.props[11]!==0||this.props[12]!==0||this.props[13]!==0||this.props[14]!==0||this.props[15]!==1),this._identityCalculated=!0),this._identity}function x(D){for(var F=0;F<16;){if(D.props[F]!==this.props[F])return!1;F+=1}return!0}function E(D){var F;for(F=0;F<16;F+=1)D.props[F]=this.props[F];return D}function g(D){var F;for(F=0;F<16;F+=1)this.props[F]=D[F]}function y(D,F,$){return{x:D*this.props[0]+F*this.props[4]+$*this.props[8]+this.props[12],y:D*this.props[1]+F*this.props[5]+$*this.props[9]+this.props[13],z:D*this.props[2]+F*this.props[6]+$*this.props[10]+this.props[14]}}function I(D,F,$){return D*this.props[0]+F*this.props[4]+$*this.props[8]+this.props[12]}function M(D,F,$){return D*this.props[1]+F*this.props[5]+$*this.props[9]+this.props[13]}function j(D,F,$){return D*this.props[2]+F*this.props[6]+$*this.props[10]+this.props[14]}function O(){var D=this.props[0]*this.props[5]-this.props[1]*this.props[4],F=this.props[5]/D,$=-this.props[1]/D,U=-this.props[4]/D,z=this.props[0]/D,K=(this.props[4]*this.props[13]-this.props[5]*this.props[12])/D,Z=-(this.props[0]*this.props[13]-this.props[1]*this.props[12])/D,Y=new Matrix;return Y.props[0]=F,Y.props[1]=$,Y.props[4]=U,Y.props[5]=z,Y.props[12]=K,Y.props[13]=Z,Y}function R(D){var F=this.getInverseMatrix();return F.applyToPointArray(D[0],D[1],D[2]||0)}function B(D){var F,$=D.length,U=[];for(F=0;F<$;F+=1)U[F]=R(D[F]);return U}function G(D,F,$){var U=createTypedArray("float32",6);if(this.isIdentity())U[0]=D[0],U[1]=D[1],U[2]=F[0],U[3]=F[1],U[4]=$[0],U[5]=$[1];else{var z=this.props[0],K=this.props[1],Z=this.props[4],Y=this.props[5],le=this.props[12],re=this.props[13];U[0]=D[0]*z+D[1]*Z+le,U[1]=D[0]*K+D[1]*Y+re,U[2]=F[0]*z+F[1]*Z+le,U[3]=F[0]*K+F[1]*Y+re,U[4]=$[0]*z+$[1]*Z+le,U[5]=$[0]*K+$[1]*Y+re}return U}function V(D,F,$){var U;return this.isIdentity()?U=[D,F,$]:U=[D*this.props[0]+F*this.props[4]+$*this.props[8]+this.props[12],D*this.props[1]+F*this.props[5]+$*this.props[9]+this.props[13],D*this.props[2]+F*this.props[6]+$*this.props[10]+this.props[14]],U}function Q(D,F){if(this.isIdentity())return D+","+F;var $=this.props;return Math.round((D*$[0]+F*$[4]+$[12])*100)/100+","+Math.round((D*$[1]+F*$[5]+$[13])*100)/100}function W(){for(var D=0,F=this.props,$="matrix3d(",U=1e4;D<16;)$+=a(F[D]*U)/U,$+=D===15?")":",",D+=1;return $}function q(D){var F=1e4;return D<1e-6&&D>0||D>-1e-6&&D<0?a(D*F)/F:D}function L(){var D=this.props,F=q(D[0]),$=q(D[1]),U=q(D[4]),z=q(D[5]),K=q(D[12]),Z=q(D[13]);return"matrix("+F+","+$+","+U+","+z+","+K+","+Z+")"}return function(){this.reset=l,this.rotate=c,this.rotateX=u,this.rotateY=d,this.rotateZ=m,this.skew=A,this.skewFromAxis=_,this.shear=b,this.scale=T,this.setTransform=C,this.translate=S,this.transform=P,this.applyToPoint=y,this.applyToX=I,this.applyToY=M,this.applyToZ=j,this.applyToPointArray=V,this.applyToTriplePoints=G,this.applyToPointStringified=Q,this.toCSS=W,this.to2dCSS=L,this.clone=E,this.cloneFromProps=g,this.equals=x,this.inversePoints=B,this.inversePoint=R,this.getInverseMatrix=O,this._t=this.transform,this.isIdentity=k,this._identity=!0,this._identityCalculated=!1,this.props=createTypedArray("float32",16),this.reset()}}();function _typeof$3(e){return typeof Symbol=="function"&&typeof Symbol.iterator=="symbol"?_typeof$3=function(n){return typeof n}:_typeof$3=function(n){return n&&typeof Symbol=="function"&&n.constructor===Symbol&&n!==Symbol.prototype?"symbol":typeof n},_typeof$3(e)}var lottie={};function setLocation(e){setLocationHref(e)}function searchAnimations(){animationManager.searchAnimations()}function setSubframeRendering(e){setSubframeEnabled(e)}function setPrefix(e){setIdPrefix(e)}function loadAnimation(e){return animationManager.loadAnimation(e)}function setQuality(e){if(typeof e=="string")switch(e){case"high":setDefaultCurveSegments(200);break;default:case"medium":setDefaultCurveSegments(50);break;case"low":setDefaultCurveSegments(10);break}else!isNaN(e)&&e>1&&setDefaultCurveSegments(e)}function inBrowser(){return typeof navigator!="undefined"}function installPlugin(e,r){e==="expressions"&&setExpressionsPlugin(r)}function getFactory(e){switch(e){case"propertyFactory":return PropertyFactory;case"shapePropertyFactory":return ShapePropertyFactory;case"matrix":return Matrix;default:return null}}lottie.play=animationManager.play,lottie.pause=animationManager.pause,lottie.setLocationHref=setLocation,lottie.togglePause=animationManager.togglePause,lottie.setSpeed=animationManager.setSpeed,lottie.setDirection=animationManager.setDirection,lottie.stop=animationManager.stop,lottie.searchAnimations=searchAnimations,lottie.registerAnimation=animationManager.registerAnimation,lottie.loadAnimation=loadAnimation,lottie.setSubframeRendering=setSubframeRendering,lottie.resize=animationManager.resize,lottie.goToAndStop=animationManager.goToAndStop,lottie.destroy=animationManager.destroy,lottie.setQuality=setQuality,lottie.inBrowser=inBrowser,lottie.installPlugin=installPlugin,lottie.freeze=animationManager.freeze,lottie.unfreeze=animationManager.unfreeze,lottie.setVolume=animationManager.setVolume,lottie.mute=animationManager.mute,lottie.unmute=animationManager.unmute,lottie.getRegisteredAnimations=animationManager.getRegisteredAnimations,lottie.useWebWorker=setWebWorker,lottie.setIDPrefix=setPrefix,lottie.__getFactory=getFactory,lottie.version="5.9.3";function checkReady(){document.readyState==="complete"&&(clearInterval(readyStateCheckInterval),searchAnimations())}function getQueryVariable(e){for(var r=queryString.split("&"),n=0;n<r.length;n+=1){var a=r[n].split("=");if(decodeURIComponent(a[0])==e)return decodeURIComponent(a[1])}return null}var queryString="";{var scripts=document.getElementsByTagName("script"),index=scripts.length-1,myScript=scripts[index]||{src:""};queryString=myScript.src?myScript.src.replace(/^[^\?]+\??/,""):"",getQueryVariable("renderer")}var readyStateCheckInterval=setInterval(checkReady,100);try{_typeof$3(exports)!=="object"&&(window.bodymovin=lottie)}catch(e){}var ShapeModifiers=function(){var e={},r={};e.registerModifier=n,e.getModifier=a;function n(l,c){r[l]||(r[l]=c)}function a(l,c,u){return new r[l](c,u)}return e}();function ShapeModifier(){}ShapeModifier.prototype.initModifierProperties=function(){},ShapeModifier.prototype.addShapeToModifier=function(){},ShapeModifier.prototype.addShape=function(e){if(!this.closed){e.sh.container.addDynamicProperty(e.sh);var r={shape:e.sh,data:e,localShapeCollection:shapeCollectionPool.newShapeCollection()};this.shapes.push(r),this.addShapeToModifier(r),this._isAnimated&&e.setAsAnimated()}},ShapeModifier.prototype.init=function(e,r){this.shapes=[],this.elem=e,this.initDynamicPropertyContainer(e),this.initModifierProperties(e,r),this.frameId=initialDefaultFrame,this.closed=!1,this.k=!1,this.dynamicProperties.length?this.k=!0:this.getValue(!0)},ShapeModifier.prototype.processKeys=function(){this.elem.globalData.frameId!==this.frameId&&(this.frameId=this.elem.globalData.frameId,this.iterateDynamicProperties())},extendPrototype([DynamicPropertyContainer],ShapeModifier);function TrimModifier(){}extendPrototype([ShapeModifier],TrimModifier),TrimModifier.prototype.initModifierProperties=function(e,r){this.s=PropertyFactory.getProp(e,r.s,0,.01,this),this.e=PropertyFactory.getProp(e,r.e,0,.01,this),this.o=PropertyFactory.getProp(e,r.o,0,0,this),this.sValue=0,this.eValue=0,this.getValue=this.processKeys,this.m=r.m,this._isAnimated=!!this.s.effectsSequence.length||!!this.e.effectsSequence.length||!!this.o.effectsSequence.length},TrimModifier.prototype.addShapeToModifier=function(e){e.pathsData=[]},TrimModifier.prototype.calculateShapeEdges=function(e,r,n,a,l){var c=[];r<=1?c.push({s:e,e:r}):e>=1?c.push({s:e-1,e:r-1}):(c.push({s:e,e:1}),c.push({s:0,e:r-1}));var u=[],d,m=c.length,b;for(d=0;d<m;d+=1)if(b=c[d],!(b.e*l<a||b.s*l>a+n)){var A,_;b.s*l<=a?A=0:A=(b.s*l-a)/n,b.e*l>=a+n?_=1:_=(b.e*l-a)/n,u.push([A,_])}return u.length||u.push([0,0]),u},TrimModifier.prototype.releasePathsData=function(e){var r,n=e.length;for(r=0;r<n;r+=1)segmentsLengthPool.release(e[r]);return e.length=0,e},TrimModifier.prototype.processShapes=function(e){var r,n;if(this._mdf||e){var a=this.o.v%360/360;if(a<0&&(a+=1),this.s.v>1?r=1+a:this.s.v<0?r=0+a:r=this.s.v+a,this.e.v>1?n=1+a:this.e.v<0?n=0+a:n=this.e.v+a,r>n){var l=r;r=n,n=l}r=Math.round(r*1e4)*1e-4,n=Math.round(n*1e4)*1e-4,this.sValue=r,this.eValue=n}else r=this.sValue,n=this.eValue;var c,u,d=this.shapes.length,m,b,A,_,T,C=0;if(n===r)for(u=0;u<d;u+=1)this.shapes[u].localShapeCollection.releaseShapes(),this.shapes[u].shape._mdf=!0,this.shapes[u].shape.paths=this.shapes[u].localShapeCollection,this._mdf&&(this.shapes[u].pathsData.length=0);else if(n===1&&r===0||n===0&&r===1){if(this._mdf)for(u=0;u<d;u+=1)this.shapes[u].pathsData.length=0,this.shapes[u].shape._mdf=!0}else{var S=[],P,k;for(u=0;u<d;u+=1)if(P=this.shapes[u],!P.shape._mdf&&!this._mdf&&!e&&this.m!==2)P.shape.paths=P.localShapeCollection;else{if(c=P.shape.paths,b=c._length,T=0,!P.shape._mdf&&P.pathsData.length)T=P.totalShapeLength;else{for(A=this.releasePathsData(P.pathsData),m=0;m<b;m+=1)_=bez.getSegmentsLength(c.shapes[m]),A.push(_),T+=_.totalLength;P.totalShapeLength=T,P.pathsData=A}C+=T,P.shape._mdf=!0}var x=r,E=n,g=0,y;for(u=d-1;u>=0;u-=1)if(P=this.shapes[u],P.shape._mdf){for(k=P.localShapeCollection,k.releaseShapes(),this.m===2&&d>1?(y=this.calculateShapeEdges(r,n,P.totalShapeLength,g,C),g+=P.totalShapeLength):y=[[x,E]],b=y.length,m=0;m<b;m+=1){x=y[m][0],E=y[m][1],S.length=0,E<=1?S.push({s:P.totalShapeLength*x,e:P.totalShapeLength*E}):x>=1?S.push({s:P.totalShapeLength*(x-1),e:P.totalShapeLength*(E-1)}):(S.push({s:P.totalShapeLength*x,e:P.totalShapeLength}),S.push({s:0,e:P.totalShapeLength*(E-1)}));var I=this.addShapes(P,S[0]);if(S[0].s!==S[0].e){if(S.length>1){var M=P.shape.paths.shapes[P.shape.paths._length-1];if(M.c){var j=I.pop();this.addPaths(I,k),I=this.addShapes(P,S[1],j)}else this.addPaths(I,k),I=this.addShapes(P,S[1])}this.addPaths(I,k)}}P.shape.paths=k}}},TrimModifier.prototype.addPaths=function(e,r){var n,a=e.length;for(n=0;n<a;n+=1)r.addShape(e[n])},TrimModifier.prototype.addSegment=function(e,r,n,a,l,c,u){l.setXYAt(r[0],r[1],"o",c),l.setXYAt(n[0],n[1],"i",c+1),u&&l.setXYAt(e[0],e[1],"v",c),l.setXYAt(a[0],a[1],"v",c+1)},TrimModifier.prototype.addSegmentFromArray=function(e,r,n,a){r.setXYAt(e[1],e[5],"o",n),r.setXYAt(e[2],e[6],"i",n+1),a&&r.setXYAt(e[0],e[4],"v",n),r.setXYAt(e[3],e[7],"v",n+1)},TrimModifier.prototype.addShapes=function(e,r,n){var a=e.pathsData,l=e.shape.paths.shapes,c,u=e.shape.paths._length,d,m,b=0,A,_,T,C,S=[],P,k=!0;for(n?(_=n._length,P=n._length):(n=shapePool.newElement(),_=0,P=0),S.push(n),c=0;c<u;c+=1){for(T=a[c].lengths,n.c=l[c].c,m=l[c].c?T.length:T.length+1,d=1;d<m;d+=1)if(A=T[d-1],b+A.addedLength<r.s)b+=A.addedLength,n.c=!1;else if(b>r.e){n.c=!1;break}else r.s<=b&&r.e>=b+A.addedLength?(this.addSegment(l[c].v[d-1],l[c].o[d-1],l[c].i[d],l[c].v[d],n,_,k),k=!1):(C=bez.getNewSegment(l[c].v[d-1],l[c].v[d],l[c].o[d-1],l[c].i[d],(r.s-b)/A.addedLength,(r.e-b)/A.addedLength,T[d-1]),this.addSegmentFromArray(C,n,_,k),k=!1,n.c=!1),b+=A.addedLength,_+=1;if(l[c].c&&T.length){if(A=T[d-1],b<=r.e){var x=T[d-1].addedLength;r.s<=b&&r.e>=b+x?(this.addSegment(l[c].v[d-1],l[c].o[d-1],l[c].i[0],l[c].v[0],n,_,k),k=!1):(C=bez.getNewSegment(l[c].v[d-1],l[c].v[0],l[c].o[d-1],l[c].i[0],(r.s-b)/x,(r.e-b)/x,T[d-1]),this.addSegmentFromArray(C,n,_,k),k=!1,n.c=!1)}else n.c=!1;b+=A.addedLength,_+=1}if(n._length&&(n.setXYAt(n.v[P][0],n.v[P][1],"i",P),n.setXYAt(n.v[n._length-1][0],n.v[n._length-1][1],"o",n._length-1)),b>r.e)break;c<u-1&&(n=shapePool.newElement(),k=!0,S.push(n),_=0)}return S};function PuckerAndBloatModifier(){}extendPrototype([ShapeModifier],PuckerAndBloatModifier),PuckerAndBloatModifier.prototype.initModifierProperties=function(e,r){this.getValue=this.processKeys,this.amount=PropertyFactory.getProp(e,r.a,0,null,this),this._isAnimated=!!this.amount.effectsSequence.length},PuckerAndBloatModifier.prototype.processPath=function(e,r){var n=r/100,a=[0,0],l=e._length,c=0;for(c=0;c<l;c+=1)a[0]+=e.v[c][0],a[1]+=e.v[c][1];a[0]/=l,a[1]/=l;var u=shapePool.newElement();u.c=e.c;var d,m,b,A,_,T;for(c=0;c<l;c+=1)d=e.v[c][0]+(a[0]-e.v[c][0])*n,m=e.v[c][1]+(a[1]-e.v[c][1])*n,b=e.o[c][0]+(a[0]-e.o[c][0])*-n,A=e.o[c][1]+(a[1]-e.o[c][1])*-n,_=e.i[c][0]+(a[0]-e.i[c][0])*-n,T=e.i[c][1]+(a[1]-e.i[c][1])*-n,u.setTripleAt(d,m,b,A,_,T,c);return u},PuckerAndBloatModifier.prototype.processShapes=function(e){var r,n,a=this.shapes.length,l,c,u=this.amount.v;if(u!==0){var d,m;for(n=0;n<a;n+=1){if(d=this.shapes[n],m=d.localShapeCollection,!(!d.shape._mdf&&!this._mdf&&!e))for(m.releaseShapes(),d.shape._mdf=!0,r=d.shape.paths.shapes,c=d.shape.paths._length,l=0;l<c;l+=1)m.addShape(this.processPath(r[l],u));d.shape.paths=d.localShapeCollection}}this.dynamicProperties.length||(this._mdf=!1)};var TransformPropertyFactory=function(){var e=[0,0];function r(m){var b=this._mdf;this.iterateDynamicProperties(),this._mdf=this._mdf||b,this.a&&m.translate(-this.a.v[0],-this.a.v[1],this.a.v[2]),this.s&&m.scale(this.s.v[0],this.s.v[1],this.s.v[2]),this.sk&&m.skewFromAxis(-this.sk.v,this.sa.v),this.r?m.rotate(-this.r.v):m.rotateZ(-this.rz.v).rotateY(this.ry.v).rotateX(this.rx.v).rotateZ(-this.or.v[2]).rotateY(this.or.v[1]).rotateX(this.or.v[0]),this.data.p.s?this.data.p.z?m.translate(this.px.v,this.py.v,-this.pz.v):m.translate(this.px.v,this.py.v,0):m.translate(this.p.v[0],this.p.v[1],-this.p.v[2])}function n(m){if(this.elem.globalData.frameId!==this.frameId){if(this._isDirty&&(this.precalculateMatrix(),this._isDirty=!1),this.iterateDynamicProperties(),this._mdf||m){var b;if(this.v.cloneFromProps(this.pre.props),this.appliedTransformations<1&&this.v.translate(-this.a.v[0],-this.a.v[1],this.a.v[2]),this.appliedTransformations<2&&this.v.scale(this.s.v[0],this.s.v[1],this.s.v[2]),this.sk&&this.appliedTransformations<3&&this.v.skewFromAxis(-this.sk.v,this.sa.v),this.r&&this.appliedTransformations<4?this.v.rotate(-this.r.v):!this.r&&this.appliedTransformations<4&&this.v.rotateZ(-this.rz.v).rotateY(this.ry.v).rotateX(this.rx.v).rotateZ(-this.or.v[2]).rotateY(this.or.v[1]).rotateX(this.or.v[0]),this.autoOriented){var A,_;if(b=this.elem.globalData.frameRate,this.p&&this.p.keyframes&&this.p.getValueAtTime)this.p._caching.lastFrame+this.p.offsetTime<=this.p.keyframes[0].t?(A=this.p.getValueAtTime((this.p.keyframes[0].t+.01)/b,0),_=this.p.getValueAtTime(this.p.keyframes[0].t/b,0)):this.p._caching.lastFrame+this.p.offsetTime>=this.p.keyframes[this.p.keyframes.length-1].t?(A=this.p.getValueAtTime(this.p.keyframes[this.p.keyframes.length-1].t/b,0),_=this.p.getValueAtTime((this.p.keyframes[this.p.keyframes.length-1].t-.05)/b,0)):(A=this.p.pv,_=this.p.getValueAtTime((this.p._caching.lastFrame+this.p.offsetTime-.01)/b,this.p.offsetTime));else if(this.px&&this.px.keyframes&&this.py.keyframes&&this.px.getValueAtTime&&this.py.getValueAtTime){A=[],_=[];var T=this.px,C=this.py;T._caching.lastFrame+T.offsetTime<=T.keyframes[0].t?(A[0]=T.getValueAtTime((T.keyframes[0].t+.01)/b,0),A[1]=C.getValueAtTime((C.keyframes[0].t+.01)/b,0),_[0]=T.getValueAtTime(T.keyframes[0].t/b,0),_[1]=C.getValueAtTime(C.keyframes[0].t/b,0)):T._caching.lastFrame+T.offsetTime>=T.keyframes[T.keyframes.length-1].t?(A[0]=T.getValueAtTime(T.keyframes[T.keyframes.length-1].t/b,0),A[1]=C.getValueAtTime(C.keyframes[C.keyframes.length-1].t/b,0),_[0]=T.getValueAtTime((T.keyframes[T.keyframes.length-1].t-.01)/b,0),_[1]=C.getValueAtTime((C.keyframes[C.keyframes.length-1].t-.01)/b,0)):(A=[T.pv,C.pv],_[0]=T.getValueAtTime((T._caching.lastFrame+T.offsetTime-.01)/b,T.offsetTime),_[1]=C.getValueAtTime((C._caching.lastFrame+C.offsetTime-.01)/b,C.offsetTime))}else _=e,A=_;this.v.rotate(-Math.atan2(A[1]-_[1],A[0]-_[0]))}this.data.p&&this.data.p.s?this.data.p.z?this.v.translate(this.px.v,this.py.v,-this.pz.v):this.v.translate(this.px.v,this.py.v,0):this.v.translate(this.p.v[0],this.p.v[1],-this.p.v[2])}this.frameId=this.elem.globalData.frameId}}function a(){if(!this.a.k)this.pre.translate(-this.a.v[0],-this.a.v[1],this.a.v[2]),this.appliedTransformations=1;else return;if(!this.s.effectsSequence.length)this.pre.scale(this.s.v[0],this.s.v[1],this.s.v[2]),this.appliedTransformations=2;else return;if(this.sk)if(!this.sk.effectsSequence.length&&!this.sa.effectsSequence.length)this.pre.skewFromAxis(-this.sk.v,this.sa.v),this.appliedTransformations=3;else return;this.r?this.r.effectsSequence.length||(this.pre.rotate(-this.r.v),this.appliedTransformations=4):!this.rz.effectsSequence.length&&!this.ry.effectsSequence.length&&!this.rx.effectsSequence.length&&!this.or.effectsSequence.length&&(this.pre.rotateZ(-this.rz.v).rotateY(this.ry.v).rotateX(this.rx.v).rotateZ(-this.or.v[2]).rotateY(this.or.v[1]).rotateX(this.or.v[0]),this.appliedTransformations=4)}function l(){}function c(m){this._addDynamicProperty(m),this.elem.addDynamicProperty(m),this._isDirty=!0}function u(m,b,A){if(this.elem=m,this.frameId=-1,this.propType="transform",this.data=b,this.v=new Matrix,this.pre=new Matrix,this.appliedTransformations=0,this.initDynamicPropertyContainer(A||m),b.p&&b.p.s?(this.px=PropertyFactory.getProp(m,b.p.x,0,0,this),this.py=PropertyFactory.getProp(m,b.p.y,0,0,this),b.p.z&&(this.pz=PropertyFactory.getProp(m,b.p.z,0,0,this))):this.p=PropertyFactory.getProp(m,b.p||{k:[0,0,0]},1,0,this),b.rx){if(this.rx=PropertyFactory.getProp(m,b.rx,0,degToRads,this),this.ry=PropertyFactory.getProp(m,b.ry,0,degToRads,this),this.rz=PropertyFactory.getProp(m,b.rz,0,degToRads,this),b.or.k[0].ti){var _,T=b.or.k.length;for(_=0;_<T;_+=1)b.or.k[_].to=null,b.or.k[_].ti=null}this.or=PropertyFactory.getProp(m,b.or,1,degToRads,this),this.or.sh=!0}else this.r=PropertyFactory.getProp(m,b.r||{k:0},0,degToRads,this);b.sk&&(this.sk=PropertyFactory.getProp(m,b.sk,0,degToRads,this),this.sa=PropertyFactory.getProp(m,b.sa,0,degToRads,this)),this.a=PropertyFactory.getProp(m,b.a||{k:[0,0,0]},1,0,this),this.s=PropertyFactory.getProp(m,b.s||{k:[100,100,100]},1,.01,this),b.o?this.o=PropertyFactory.getProp(m,b.o,0,.01,m):this.o={_mdf:!1,v:1},this._isDirty=!0,this.dynamicProperties.length||this.getValue(!0)}u.prototype={applyToMatrix:r,getValue:n,precalculateMatrix:a,autoOrient:l},extendPrototype([DynamicPropertyContainer],u),u.prototype.addDynamicProperty=c,u.prototype._addDynamicProperty=DynamicPropertyContainer.prototype.addDynamicProperty;function d(m,b,A){return new u(m,b,A)}return{getTransformProperty:d}}();function RepeaterModifier(){}extendPrototype([ShapeModifier],RepeaterModifier),RepeaterModifier.prototype.initModifierProperties=function(e,r){this.getValue=this.processKeys,this.c=PropertyFactory.getProp(e,r.c,0,null,this),this.o=PropertyFactory.getProp(e,r.o,0,null,this),this.tr=TransformPropertyFactory.getTransformProperty(e,r.tr,this),this.so=PropertyFactory.getProp(e,r.tr.so,0,.01,this),this.eo=PropertyFactory.getProp(e,r.tr.eo,0,.01,this),this.data=r,this.dynamicProperties.length||this.getValue(!0),this._isAnimated=!!this.dynamicProperties.length,this.pMatrix=new Matrix,this.rMatrix=new Matrix,this.sMatrix=new Matrix,this.tMatrix=new Matrix,this.matrix=new Matrix},RepeaterModifier.prototype.applyTransforms=function(e,r,n,a,l,c){var u=c?-1:1,d=a.s.v[0]+(1-a.s.v[0])*(1-l),m=a.s.v[1]+(1-a.s.v[1])*(1-l);e.translate(a.p.v[0]*u*l,a.p.v[1]*u*l,a.p.v[2]),r.translate(-a.a.v[0],-a.a.v[1],a.a.v[2]),r.rotate(-a.r.v*u*l),r.translate(a.a.v[0],a.a.v[1],a.a.v[2]),n.translate(-a.a.v[0],-a.a.v[1],a.a.v[2]),n.scale(c?1/d:d,c?1/m:m),n.translate(a.a.v[0],a.a.v[1],a.a.v[2])},RepeaterModifier.prototype.init=function(e,r,n,a){for(this.elem=e,this.arr=r,this.pos=n,this.elemsData=a,this._currentCopies=0,this._elements=[],this._groups=[],this.frameId=-1,this.initDynamicPropertyContainer(e),this.initModifierProperties(e,r[n]);n>0;)n-=1,this._elements.unshift(r[n]);this.dynamicProperties.length?this.k=!0:this.getValue(!0)},RepeaterModifier.prototype.resetElements=function(e){var r,n=e.length;for(r=0;r<n;r+=1)e[r]._processed=!1,e[r].ty==="gr"&&this.resetElements(e[r].it)},RepeaterModifier.prototype.cloneElements=function(e){var r=JSON.parse(JSON.stringify(e));return this.resetElements(r),r},RepeaterModifier.prototype.changeGroupRender=function(e,r){var n,a=e.length;for(n=0;n<a;n+=1)e[n]._render=r,e[n].ty==="gr"&&this.changeGroupRender(e[n].it,r)},RepeaterModifier.prototype.processShapes=function(e){var r,n,a,l,c,u=!1;if(this._mdf||e){var d=Math.ceil(this.c.v);if(this._groups.length<d){for(;this._groups.length<d;){var m={it:this.cloneElements(this._elements),ty:"gr"};m.it.push({a:{a:0,ix:1,k:[0,0]},nm:"Transform",o:{a:0,ix:7,k:100},p:{a:0,ix:2,k:[0,0]},r:{a:1,ix:6,k:[{s:0,e:0,t:0},{s:0,e:0,t:1}]},s:{a:0,ix:3,k:[100,100]},sa:{a:0,ix:5,k:0},sk:{a:0,ix:4,k:0},ty:"tr"}),this.arr.splice(0,0,m),this._groups.splice(0,0,m),this._currentCopies+=1}this.elem.reloadShapes(),u=!0}c=0;var b;for(a=0;a<=this._groups.length-1;a+=1){if(b=c<d,this._groups[a]._render=b,this.changeGroupRender(this._groups[a].it,b),!b){var A=this.elemsData[a].it,_=A[A.length-1];_.transform.op.v!==0?(_.transform.op._mdf=!0,_.transform.op.v=0):_.transform.op._mdf=!1}c+=1}this._currentCopies=d;var T=this.o.v,C=T%1,S=T>0?Math.floor(T):Math.ceil(T),P=this.pMatrix.props,k=this.rMatrix.props,x=this.sMatrix.props;this.pMatrix.reset(),this.rMatrix.reset(),this.sMatrix.reset(),this.tMatrix.reset(),this.matrix.reset();var E=0;if(T>0){for(;E<S;)this.applyTransforms(this.pMatrix,this.rMatrix,this.sMatrix,this.tr,1,!1),E+=1;C&&(this.applyTransforms(this.pMatrix,this.rMatrix,this.sMatrix,this.tr,C,!1),E+=C)}else if(T<0){for(;E>S;)this.applyTransforms(this.pMatrix,this.rMatrix,this.sMatrix,this.tr,1,!0),E-=1;C&&(this.applyTransforms(this.pMatrix,this.rMatrix,this.sMatrix,this.tr,-C,!0),E-=C)}a=this.data.m===1?0:this._currentCopies-1,l=this.data.m===1?1:-1,c=this._currentCopies;for(var g,y;c;){if(r=this.elemsData[a].it,n=r[r.length-1].transform.mProps.v.props,y=n.length,r[r.length-1].transform.mProps._mdf=!0,r[r.length-1].transform.op._mdf=!0,r[r.length-1].transform.op.v=this._currentCopies===1?this.so.v:this.so.v+(this.eo.v-this.so.v)*(a/(this._currentCopies-1)),E!==0){for((a!==0&&l===1||a!==this._currentCopies-1&&l===-1)&&this.applyTransforms(this.pMatrix,this.rMatrix,this.sMatrix,this.tr,1,!1),this.matrix.transform(k[0],k[1],k[2],k[3],k[4],k[5],k[6],k[7],k[8],k[9],k[10],k[11],k[12],k[13],k[14],k[15]),this.matrix.transform(x[0],x[1],x[2],x[3],x[4],x[5],x[6],x[7],x[8],x[9],x[10],x[11],x[12],x[13],x[14],x[15]),this.matrix.transform(P[0],P[1],P[2],P[3],P[4],P[5],P[6],P[7],P[8],P[9],P[10],P[11],P[12],P[13],P[14],P[15]),g=0;g<y;g+=1)n[g]=this.matrix.props[g];this.matrix.reset()}else for(this.matrix.reset(),g=0;g<y;g+=1)n[g]=this.matrix.props[g];E+=1,c-=1,a+=l}}else for(c=this._currentCopies,a=0,l=1;c;)r=this.elemsData[a].it,n=r[r.length-1].transform.mProps.v.props,r[r.length-1].transform.mProps._mdf=!1,r[r.length-1].transform.op._mdf=!1,c-=1,a+=l;return u},RepeaterModifier.prototype.addShape=function(){};function RoundCornersModifier(){}extendPrototype([ShapeModifier],RoundCornersModifier),RoundCornersModifier.prototype.initModifierProperties=function(e,r){this.getValue=this.processKeys,this.rd=PropertyFactory.getProp(e,r.r,0,null,this),this._isAnimated=!!this.rd.effectsSequence.length},RoundCornersModifier.prototype.processPath=function(e,r){var n=shapePool.newElement();n.c=e.c;var a,l=e._length,c,u,d,m,b,A,_=0,T,C,S,P,k,x;for(a=0;a<l;a+=1)c=e.v[a],d=e.o[a],u=e.i[a],c[0]===d[0]&&c[1]===d[1]&&c[0]===u[0]&&c[1]===u[1]?(a===0||a===l-1)&&!e.c?(n.setTripleAt(c[0],c[1],d[0],d[1],u[0],u[1],_),_+=1):(a===0?m=e.v[l-1]:m=e.v[a-1],b=Math.sqrt(Math.pow(c[0]-m[0],2)+Math.pow(c[1]-m[1],2)),A=b?Math.min(b/2,r)/b:0,k=c[0]+(m[0]-c[0])*A,T=k,x=c[1]-(c[1]-m[1])*A,C=x,S=T-(T-c[0])*roundCorner,P=C-(C-c[1])*roundCorner,n.setTripleAt(T,C,S,P,k,x,_),_+=1,a===l-1?m=e.v[0]:m=e.v[a+1],b=Math.sqrt(Math.pow(c[0]-m[0],2)+Math.pow(c[1]-m[1],2)),A=b?Math.min(b/2,r)/b:0,S=c[0]+(m[0]-c[0])*A,T=S,P=c[1]+(m[1]-c[1])*A,C=P,k=T-(T-c[0])*roundCorner,x=C-(C-c[1])*roundCorner,n.setTripleAt(T,C,S,P,k,x,_),_+=1):(n.setTripleAt(e.v[a][0],e.v[a][1],e.o[a][0],e.o[a][1],e.i[a][0],e.i[a][1],_),_+=1);return n},RoundCornersModifier.prototype.processShapes=function(e){var r,n,a=this.shapes.length,l,c,u=this.rd.v;if(u!==0){var d,m;for(n=0;n<a;n+=1){if(d=this.shapes[n],m=d.localShapeCollection,!(!d.shape._mdf&&!this._mdf&&!e))for(m.releaseShapes(),d.shape._mdf=!0,r=d.shape.paths.shapes,c=d.shape.paths._length,l=0;l<c;l+=1)m.addShape(this.processPath(r[l],u));d.shape.paths=d.localShapeCollection}}this.dynamicProperties.length||(this._mdf=!1)};function getFontProperties(e){for(var r=e.fStyle?e.fStyle.split(" "):[],n="normal",a="normal",l=r.length,c,u=0;u<l;u+=1)switch(c=r[u].toLowerCase(),c){case"italic":a="italic";break;case"bold":n="700";break;case"black":n="900";break;case"medium":n="500";break;case"regular":case"normal":n="400";break;case"light":case"thin":n="200";break}return{style:a,weight:e.fWeight||n}}var FontManager=function(){var e=5e3,r={w:0,size:0,shapes:[],data:{shapes:[]}},n=[];n=n.concat([2304,2305,2306,2307,2362,2363,2364,2364,2366,2367,2368,2369,2370,2371,2372,2373,2374,2375,2376,2377,2378,2379,2380,2381,2382,2383,2387,2388,2389,2390,2391,2402,2403]);var a=["d83cdffb","d83cdffc","d83cdffd","d83cdffe","d83cdfff"],l=[65039,8205];function c(y){var I=y.split(","),M,j=I.length,O=[];for(M=0;M<j;M+=1)I[M]!=="sans-serif"&&I[M]!=="monospace"&&O.push(I[M]);return O.join(",")}function u(y,I){var M=createTag("span");M.setAttribute("aria-hidden",!0),M.style.fontFamily=I;var j=createTag("span");j.innerText="giItT1WQy@!-/#",M.style.position="absolute",M.style.left="-10000px",M.style.top="-10000px",M.style.fontSize="300px",M.style.fontVariant="normal",M.style.fontStyle="normal",M.style.fontWeight="normal",M.style.letterSpacing="0",M.appendChild(j),document.body.appendChild(M);var O=j.offsetWidth;return j.style.fontFamily=c(y)+", "+I,{node:j,w:O,parent:M}}function d(){var y,I=this.fonts.length,M,j,O=I;for(y=0;y<I;y+=1)this.fonts[y].loaded?O-=1:this.fonts[y].fOrigin==="n"||this.fonts[y].origin===0?this.fonts[y].loaded=!0:(M=this.fonts[y].monoCase.node,j=this.fonts[y].monoCase.w,M.offsetWidth!==j?(O-=1,this.fonts[y].loaded=!0):(M=this.fonts[y].sansCase.node,j=this.fonts[y].sansCase.w,M.offsetWidth!==j&&(O-=1,this.fonts[y].loaded=!0)),this.fonts[y].loaded&&(this.fonts[y].sansCase.parent.parentNode.removeChild(this.fonts[y].sansCase.parent),this.fonts[y].monoCase.parent.parentNode.removeChild(this.fonts[y].monoCase.parent)));O!==0&&Date.now()-this.initTime<e?setTimeout(this.checkLoadedFontsBinded,20):setTimeout(this.setIsLoadedBinded,10)}function m(y,I){var M=document.body&&I?"svg":"canvas",j,O=getFontProperties(y);if(M==="svg"){var R=createNS("text");R.style.fontSize="100px",R.setAttribute("font-family",y.fFamily),R.setAttribute("font-style",O.style),R.setAttribute("font-weight",O.weight),R.textContent="1",y.fClass?(R.style.fontFamily="inherit",R.setAttribute("class",y.fClass)):R.style.fontFamily=y.fFamily,I.appendChild(R),j=R}else{var B=new OffscreenCanvas(500,500).getContext("2d");B.font=O.style+" "+O.weight+" 100px "+y.fFamily,j=B}function G(V){return M==="svg"?(j.textContent=V,j.getComputedTextLength()):j.measureText(V).width}return{measureText:G}}function b(y,I){if(!y){this.isLoaded=!0;return}if(this.chars){this.isLoaded=!0,this.fonts=y.list;return}if(!document.body){this.isLoaded=!0,y.list.forEach(function(L){L.helper=m(L),L.cache={}}),this.fonts=y.list;return}var M=y.list,j,O=M.length,R=O;for(j=0;j<O;j+=1){var B=!0,G,V;if(M[j].loaded=!1,M[j].monoCase=u(M[j].fFamily,"monospace"),M[j].sansCase=u(M[j].fFamily,"sans-serif"),!M[j].fPath)M[j].loaded=!0,R-=1;else if(M[j].fOrigin==="p"||M[j].origin===3){if(G=document.querySelectorAll('style[f-forigin="p"][f-family="'+M[j].fFamily+'"], style[f-origin="3"][f-family="'+M[j].fFamily+'"]'),G.length>0&&(B=!1),B){var Q=createTag("style");Q.setAttribute("f-forigin",M[j].fOrigin),Q.setAttribute("f-origin",M[j].origin),Q.setAttribute("f-family",M[j].fFamily),Q.type="text/css",Q.innerText="@font-face {font-family: "+M[j].fFamily+"; font-style: normal; src: url('"+M[j].fPath+"');}",I.appendChild(Q)}}else if(M[j].fOrigin==="g"||M[j].origin===1){for(G=document.querySelectorAll('link[f-forigin="g"], link[f-origin="1"]'),V=0;V<G.length;V+=1)G[V].href.indexOf(M[j].fPath)!==-1&&(B=!1);if(B){var W=createTag("link");W.setAttribute("f-forigin",M[j].fOrigin),W.setAttribute("f-origin",M[j].origin),W.type="text/css",W.rel="stylesheet",W.href=M[j].fPath,document.body.appendChild(W)}}else if(M[j].fOrigin==="t"||M[j].origin===2){for(G=document.querySelectorAll('script[f-forigin="t"], script[f-origin="2"]'),V=0;V<G.length;V+=1)M[j].fPath===G[V].src&&(B=!1);if(B){var q=createTag("link");q.setAttribute("f-forigin",M[j].fOrigin),q.setAttribute("f-origin",M[j].origin),q.setAttribute("rel","stylesheet"),q.setAttribute("href",M[j].fPath),I.appendChild(q)}}M[j].helper=m(M[j],I),M[j].cache={},this.fonts.push(M[j])}R===0?this.isLoaded=!0:setTimeout(this.checkLoadedFonts.bind(this),100)}function A(y){if(!!y){this.chars||(this.chars=[]);var I,M=y.length,j,O=this.chars.length,R;for(I=0;I<M;I+=1){for(j=0,R=!1;j<O;)this.chars[j].style===y[I].style&&this.chars[j].fFamily===y[I].fFamily&&this.chars[j].ch===y[I].ch&&(R=!0),j+=1;R||(this.chars.push(y[I]),O+=1)}}}function _(y,I,M){for(var j=0,O=this.chars.length;j<O;){if(this.chars[j].ch===y&&this.chars[j].style===I&&this.chars[j].fFamily===M)return this.chars[j];j+=1}return(typeof y=="string"&&y.charCodeAt(0)!==13||!y)&&console&&console.warn&&!this._warned&&(this._warned=!0,console.warn("Missing character from exported characters list: ",y,I,M)),r}function T(y,I,M){var j=this.getFontByName(I),O=y.charCodeAt(0);if(!j.cache[O+1]){var R=j.helper;if(y===" "){var B=R.measureText("|"+y+"|"),G=R.measureText("||");j.cache[O+1]=(B-G)/100}else j.cache[O+1]=R.measureText(y)/100}return j.cache[O+1]*M}function C(y){for(var I=0,M=this.fonts.length;I<M;){if(this.fonts[I].fName===y)return this.fonts[I];I+=1}return this.fonts[0]}function S(y,I){var M=y.toString(16)+I.toString(16);return a.indexOf(M)!==-1}function P(y,I){return I?y===l[0]&&I===l[1]:y===l[1]}function k(y){return n.indexOf(y)!==-1}function x(){this.isLoaded=!0}var E=function(){this.fonts=[],this.chars=null,this.typekitLoaded=0,this.isLoaded=!1,this._warned=!1,this.initTime=Date.now(),this.setIsLoadedBinded=this.setIsLoaded.bind(this),this.checkLoadedFontsBinded=this.checkLoadedFonts.bind(this)};E.isModifier=S,E.isZeroWidthJoiner=P,E.isCombinedCharacter=k;var g={addChars:A,addFonts:b,getCharData:_,getFontByName:C,measureText:T,checkLoadedFonts:d,setIsLoaded:x};return E.prototype=g,E}();function RenderableElement(){}RenderableElement.prototype={initRenderable:function(){this.isInRange=!1,this.hidden=!1,this.isTransparent=!1,this.renderableComponents=[]},addRenderableComponent:function(r){this.renderableComponents.indexOf(r)===-1&&this.renderableComponents.push(r)},removeRenderableComponent:function(r){this.renderableComponents.indexOf(r)!==-1&&this.renderableComponents.splice(this.renderableComponents.indexOf(r),1)},prepareRenderableFrame:function(r){this.checkLayerLimits(r)},checkTransparency:function(){this.finalTransform.mProp.o.v<=0?!this.isTransparent&&this.globalData.renderConfig.hideOnTransparent&&(this.isTransparent=!0,this.hide()):this.isTransparent&&(this.isTransparent=!1,this.show())},checkLayerLimits:function(r){this.data.ip-this.data.st<=r&&this.data.op-this.data.st>r?this.isInRange!==!0&&(this.globalData._mdf=!0,this._mdf=!0,this.isInRange=!0,this.show()):this.isInRange!==!1&&(this.globalData._mdf=!0,this.isInRange=!1,this.hide())},renderRenderable:function(){var r,n=this.renderableComponents.length;for(r=0;r<n;r+=1)this.renderableComponents[r].renderFrame(this._isFirstFrame)},sourceRectAtTime:function(){return{top:0,left:0,width:100,height:100}},getLayerSize:function(){return this.data.ty===5?{w:this.data.textData.width,h:this.data.textData.height}:{w:this.data.width,h:this.data.height}}};var MaskManagerInterface=function(){function e(n,a){this._mask=n,this._data=a}Object.defineProperty(e.prototype,"maskPath",{get:function(){return this._mask.prop.k&&this._mask.prop.getValue(),this._mask.prop}}),Object.defineProperty(e.prototype,"maskOpacity",{get:function(){return this._mask.op.k&&this._mask.op.getValue(),this._mask.op.v*100}});var r=function(a){var l=createSizedArray(a.viewData.length),c,u=a.viewData.length;for(c=0;c<u;c+=1)l[c]=new e(a.viewData[c],a.masksProperties[c]);var d=function(b){for(c=0;c<u;){if(a.masksProperties[c].nm===b)return l[c];c+=1}return null};return d};return r}(),ExpressionPropertyInterface=function(){var e={pv:0,v:0,mult:1},r={pv:[0,0,0],v:[0,0,0],mult:1};function n(u,d,m){Object.defineProperty(u,"velocity",{get:function(){return d.getVelocityAtTime(d.comp.currentFrame)}}),u.numKeys=d.keyframes?d.keyframes.length:0,u.key=function(b){if(!u.numKeys)return 0;var A="";"s"in d.keyframes[b-1]?A=d.keyframes[b-1].s:"e"in d.keyframes[b-2]?A=d.keyframes[b-2].e:A=d.keyframes[b-2].s;var _=m==="unidimensional"?new Number(A):Object.assign({},A);return _.time=d.keyframes[b-1].t/d.elem.comp.globalData.frameRate,_.value=m==="unidimensional"?A[0]:A,_},u.valueAtTime=d.getValueAtTime,u.speedAtTime=d.getSpeedAtTime,u.velocityAtTime=d.getVelocityAtTime,u.propertyGroup=d.propertyGroup}function a(u){(!u||!("pv"in u))&&(u=e);var d=1/u.mult,m=u.pv*d,b=new Number(m);return b.value=m,n(b,u,"unidimensional"),function(){return u.k&&u.getValue(),m=u.v*d,b.value!==m&&(b=new Number(m),b.value=m,n(b,u,"unidimensional")),b}}function l(u){(!u||!("pv"in u))&&(u=r);var d=1/u.mult,m=u.data&&u.data.l||u.pv.length,b=createTypedArray("float32",m),A=createTypedArray("float32",m);return b.value=A,n(b,u,"multidimensional"),function(){u.k&&u.getValue();for(var _=0;_<m;_+=1)A[_]=u.v[_]*d,b[_]=A[_];return b}}function c(){return e}return function(u){return u?u.propType==="unidimensional"?a(u):l(u):c}}(),TransformExpressionInterface=function(){return function(e){function r(u){switch(u){case"scale":case"Scale":case"ADBE Scale":case 6:return r.scale;case"rotation":case"Rotation":case"ADBE Rotation":case"ADBE Rotate Z":case 10:return r.rotation;case"ADBE Rotate X":return r.xRotation;case"ADBE Rotate Y":return r.yRotation;case"position":case"Position":case"ADBE Position":case 2:return r.position;case"ADBE Position_0":return r.xPosition;case"ADBE Position_1":return r.yPosition;case"ADBE Position_2":return r.zPosition;case"anchorPoint":case"AnchorPoint":case"Anchor Point":case"ADBE AnchorPoint":case 1:return r.anchorPoint;case"opacity":case"Opacity":case 11:return r.opacity;default:return null}}Object.defineProperty(r,"rotation",{get:ExpressionPropertyInterface(e.r||e.rz)}),Object.defineProperty(r,"zRotation",{get:ExpressionPropertyInterface(e.rz||e.r)}),Object.defineProperty(r,"xRotation",{get:ExpressionPropertyInterface(e.rx)}),Object.defineProperty(r,"yRotation",{get:ExpressionPropertyInterface(e.ry)}),Object.defineProperty(r,"scale",{get:ExpressionPropertyInterface(e.s)});var n,a,l,c;return e.p?c=ExpressionPropertyInterface(e.p):(n=ExpressionPropertyInterface(e.px),a=ExpressionPropertyInterface(e.py),e.pz&&(l=ExpressionPropertyInterface(e.pz))),Object.defineProperty(r,"position",{get:function(){return e.p?c():[n(),a(),l?l():0]}}),Object.defineProperty(r,"xPosition",{get:ExpressionPropertyInterface(e.px)}),Object.defineProperty(r,"yPosition",{get:ExpressionPropertyInterface(e.py)}),Object.defineProperty(r,"zPosition",{get:ExpressionPropertyInterface(e.pz)}),Object.defineProperty(r,"anchorPoint",{get:ExpressionPropertyInterface(e.a)}),Object.defineProperty(r,"opacity",{get:ExpressionPropertyInterface(e.o)}),Object.defineProperty(r,"skew",{get:ExpressionPropertyInterface(e.sk)}),Object.defineProperty(r,"skewAxis",{get:ExpressionPropertyInterface(e.sa)}),Object.defineProperty(r,"orientation",{get:ExpressionPropertyInterface(e.or)}),r}}(),LayerExpressionInterface=function(){function e(b){var A=new Matrix;if(b!==void 0){var _=this._elem.finalTransform.mProp.getValueAtTime(b);_.clone(A)}else{var T=this._elem.finalTransform.mProp;T.applyToMatrix(A)}return A}function r(b,A){var _=this.getMatrix(A);return _.props[12]=0,_.props[13]=0,_.props[14]=0,this.applyPoint(_,b)}function n(b,A){var _=this.getMatrix(A);return this.applyPoint(_,b)}function a(b,A){var _=this.getMatrix(A);return _.props[12]=0,_.props[13]=0,_.props[14]=0,this.invertPoint(_,b)}function l(b,A){var _=this.getMatrix(A);return this.invertPoint(_,b)}function c(b,A){if(this._elem.hierarchy&&this._elem.hierarchy.length){var _,T=this._elem.hierarchy.length;for(_=0;_<T;_+=1)this._elem.hierarchy[_].finalTransform.mProp.applyToMatrix(b)}return b.applyToPointArray(A[0],A[1],A[2]||0)}function u(b,A){if(this._elem.hierarchy&&this._elem.hierarchy.length){var _,T=this._elem.hierarchy.length;for(_=0;_<T;_+=1)this._elem.hierarchy[_].finalTransform.mProp.applyToMatrix(b)}return b.inversePoint(A)}function d(b){var A=new Matrix;if(A.reset(),this._elem.finalTransform.mProp.applyToMatrix(A),this._elem.hierarchy&&this._elem.hierarchy.length){var _,T=this._elem.hierarchy.length;for(_=0;_<T;_+=1)this._elem.hierarchy[_].finalTransform.mProp.applyToMatrix(A);return A.inversePoint(b)}return A.inversePoint(b)}function m(){return[1,1,1,1]}return function(b){var A;function _(P){C.mask=new MaskManagerInterface(P,b)}function T(P){C.effect=P}function C(P){switch(P){case"ADBE Root Vectors Group":case"Contents":case 2:return C.shapeInterface;case 1:case 6:case"Transform":case"transform":case"ADBE Transform Group":return A;case 4:case"ADBE Effect Parade":case"effects":case"Effects":return C.effect;case"ADBE Text Properties":return C.textInterface;default:return null}}C.getMatrix=e,C.invertPoint=u,C.applyPoint=c,C.toWorld=n,C.toWorldVec=r,C.fromWorld=l,C.fromWorldVec=a,C.toComp=n,C.fromComp=d,C.sampleImage=m,C.sourceRectAtTime=b.sourceRectAtTime.bind(b),C._elem=b,A=TransformExpressionInterface(b.finalTransform.mProp);var S=getDescriptor(A,"anchorPoint");return Object.defineProperties(C,{hasParent:{get:function(){return b.hierarchy.length}},parent:{get:function(){return b.hierarchy[0].layerInterface}},rotation:getDescriptor(A,"rotation"),scale:getDescriptor(A,"scale"),position:getDescriptor(A,"position"),opacity:getDescriptor(A,"opacity"),anchorPoint:S,anchor_point:S,transform:{get:function(){return A}},active:{get:function(){return b.isInRange}}}),C.startTime=b.data.st,C.index=b.data.ind,C.source=b.data.refId,C.height=b.data.ty===0?b.data.h:100,C.width=b.data.ty===0?b.data.w:100,C.inPoint=b.data.ip/b.comp.globalData.frameRate,C.outPoint=b.data.op/b.comp.globalData.frameRate,C._name=b.data.nm,C.registerMaskInterface=_,C.registerEffectsInterface=T,C}}(),propertyGroupFactory=function(){return function(e,r){return function(n){return n=n===void 0?1:n,n<=0?e:r(n-1)}}}(),PropertyInterface=function(){return function(e,r){var n={_name:e};function a(l){return l=l===void 0?1:l,l<=0?n:r(l-1)}return a}}(),EffectsExpressionInterface=function(){var e={createEffectsInterface:r};function r(l,c){if(l.effectsManager){var u=[],d=l.data.ef,m,b=l.effectsManager.effectElements.length;for(m=0;m<b;m+=1)u.push(n(d[m],l.effectsManager.effectElements[m],c,l));var A=l.data.ef||[],_=function(C){for(m=0,b=A.length;m<b;){if(C===A[m].nm||C===A[m].mn||C===A[m].ix)return u[m];m+=1}return null};return Object.defineProperty(_,"numProperties",{get:function(){return A.length}}),_}return null}function n(l,c,u,d){function m(C){for(var S=l.ef,P=0,k=S.length;P<k;){if(C===S[P].nm||C===S[P].mn||C===S[P].ix)return S[P].ty===5?A[P]:A[P]();P+=1}throw new Error}var b=propertyGroupFactory(m,u),A=[],_,T=l.ef.length;for(_=0;_<T;_+=1)l.ef[_].ty===5?A.push(n(l.ef[_],c.effectElements[_],c.effectElements[_].propertyGroup,d)):A.push(a(c.effectElements[_],l.ef[_].ty,d,b));return l.mn==="ADBE Color Control"&&Object.defineProperty(m,"color",{get:function(){return A[0]()}}),Object.defineProperties(m,{numProperties:{get:function(){return l.np}},_name:{value:l.nm},propertyGroup:{value:b}}),m.enabled=l.en!==0,m.active=m.enabled,m}function a(l,c,u,d){var m=ExpressionPropertyInterface(l.p);function b(){return c===10?u.comp.compInterface(l.p.v):m()}return l.p.setGroupProperty&&l.p.setGroupProperty(PropertyInterface("",d)),b}return e}(),CompExpressionInterface=function(){return function(e){function r(n){for(var a=0,l=e.layers.length;a<l;){if(e.layers[a].nm===n||e.layers[a].ind===n)return e.elements[a].layerInterface;a+=1}return null}return Object.defineProperty(r,"_name",{value:e.data.nm}),r.layer=r,r.pixelAspect=1,r.height=e.data.h||e.globalData.compSize.h,r.width=e.data.w||e.globalData.compSize.w,r.pixelAspect=1,r.frameDuration=1/e.globalData.frameRate,r.displayStartTime=0,r.numLayers=e.layers.length,r}}(),ShapePathInterface=function(){return function(r,n,a){var l=n.sh;function c(d){return d==="Shape"||d==="shape"||d==="Path"||d==="path"||d==="ADBE Vector Shape"||d===2?c.path:null}var u=propertyGroupFactory(c,a);return l.setGroupProperty(PropertyInterface("Path",u)),Object.defineProperties(c,{path:{get:function(){return l.k&&l.getValue(),l}},shape:{get:function(){return l.k&&l.getValue(),l}},_name:{value:r.nm},ix:{value:r.ix},propertyIndex:{value:r.ix},mn:{value:r.mn},propertyGroup:{value:a}}),c}}(),ShapeExpressionInterface=function(){function e(S,P,k){var x=[],E,g=S?S.length:0;for(E=0;E<g;E+=1)S[E].ty==="gr"?x.push(n(S[E],P[E],k)):S[E].ty==="fl"?x.push(a(S[E],P[E],k)):S[E].ty==="st"?x.push(u(S[E],P[E],k)):S[E].ty==="tm"?x.push(d(S[E],P[E],k)):S[E].ty==="tr"||(S[E].ty==="el"?x.push(b(S[E],P[E],k)):S[E].ty==="sr"?x.push(A(S[E],P[E],k)):S[E].ty==="sh"?x.push(ShapePathInterface(S[E],P[E],k)):S[E].ty==="rc"?x.push(_(S[E],P[E],k)):S[E].ty==="rd"?x.push(T(S[E],P[E],k)):S[E].ty==="rp"?x.push(C(S[E],P[E],k)):S[E].ty==="gf"?x.push(l(S[E],P[E],k)):x.push(c(S[E],P[E])));return x}function r(S,P,k){var x,E=function(I){for(var M=0,j=x.length;M<j;){if(x[M]._name===I||x[M].mn===I||x[M].propertyIndex===I||x[M].ix===I||x[M].ind===I)return x[M];M+=1}return typeof I=="number"?x[I-1]:null};E.propertyGroup=propertyGroupFactory(E,k),x=e(S.it,P.it,E.propertyGroup),E.numProperties=x.length;var g=m(S.it[S.it.length-1],P.it[P.it.length-1],E.propertyGroup);return E.transform=g,E.propertyIndex=S.cix,E._name=S.nm,E}function n(S,P,k){var x=function(I){switch(I){case"ADBE Vectors Group":case"Contents":case 2:return x.content;default:return x.transform}};x.propertyGroup=propertyGroupFactory(x,k);var E=r(S,P,x.propertyGroup),g=m(S.it[S.it.length-1],P.it[P.it.length-1],x.propertyGroup);return x.content=E,x.transform=g,Object.defineProperty(x,"_name",{get:function(){return S.nm}}),x.numProperties=S.np,x.propertyIndex=S.ix,x.nm=S.nm,x.mn=S.mn,x}function a(S,P,k){function x(E){return E==="Color"||E==="color"?x.color:E==="Opacity"||E==="opacity"?x.opacity:null}return Object.defineProperties(x,{color:{get:ExpressionPropertyInterface(P.c)},opacity:{get:ExpressionPropertyInterface(P.o)},_name:{value:S.nm},mn:{value:S.mn}}),P.c.setGroupProperty(PropertyInterface("Color",k)),P.o.setGroupProperty(PropertyInterface("Opacity",k)),x}function l(S,P,k){function x(E){return E==="Start Point"||E==="start point"?x.startPoint:E==="End Point"||E==="end point"?x.endPoint:E==="Opacity"||E==="opacity"?x.opacity:null}return Object.defineProperties(x,{startPoint:{get:ExpressionPropertyInterface(P.s)},endPoint:{get:ExpressionPropertyInterface(P.e)},opacity:{get:ExpressionPropertyInterface(P.o)},type:{get:function(){return"a"}},_name:{value:S.nm},mn:{value:S.mn}}),P.s.setGroupProperty(PropertyInterface("Start Point",k)),P.e.setGroupProperty(PropertyInterface("End Point",k)),P.o.setGroupProperty(PropertyInterface("Opacity",k)),x}function c(){function S(){return null}return S}function u(S,P,k){var x=propertyGroupFactory(j,k),E=propertyGroupFactory(M,x);function g(O){Object.defineProperty(M,S.d[O].nm,{get:ExpressionPropertyInterface(P.d.dataProps[O].p)})}var y,I=S.d?S.d.length:0,M={};for(y=0;y<I;y+=1)g(y),P.d.dataProps[y].p.setGroupProperty(E);function j(O){return O==="Color"||O==="color"?j.color:O==="Opacity"||O==="opacity"?j.opacity:O==="Stroke Width"||O==="stroke width"?j.strokeWidth:null}return Object.defineProperties(j,{color:{get:ExpressionPropertyInterface(P.c)},opacity:{get:ExpressionPropertyInterface(P.o)},strokeWidth:{get:ExpressionPropertyInterface(P.w)},dash:{get:function(){return M}},_name:{value:S.nm},mn:{value:S.mn}}),P.c.setGroupProperty(PropertyInterface("Color",x)),P.o.setGroupProperty(PropertyInterface("Opacity",x)),P.w.setGroupProperty(PropertyInterface("Stroke Width",x)),j}function d(S,P,k){function x(g){return g===S.e.ix||g==="End"||g==="end"?x.end:g===S.s.ix?x.start:g===S.o.ix?x.offset:null}var E=propertyGroupFactory(x,k);return x.propertyIndex=S.ix,P.s.setGroupProperty(PropertyInterface("Start",E)),P.e.setGroupProperty(PropertyInterface("End",E)),P.o.setGroupProperty(PropertyInterface("Offset",E)),x.propertyIndex=S.ix,x.propertyGroup=k,Object.defineProperties(x,{start:{get:ExpressionPropertyInterface(P.s)},end:{get:ExpressionPropertyInterface(P.e)},offset:{get:ExpressionPropertyInterface(P.o)},_name:{value:S.nm}}),x.mn=S.mn,x}function m(S,P,k){function x(g){return S.a.ix===g||g==="Anchor Point"?x.anchorPoint:S.o.ix===g||g==="Opacity"?x.opacity:S.p.ix===g||g==="Position"?x.position:S.r.ix===g||g==="Rotation"||g==="ADBE Vector Rotation"?x.rotation:S.s.ix===g||g==="Scale"?x.scale:S.sk&&S.sk.ix===g||g==="Skew"?x.skew:S.sa&&S.sa.ix===g||g==="Skew Axis"?x.skewAxis:null}var E=propertyGroupFactory(x,k);return P.transform.mProps.o.setGroupProperty(PropertyInterface("Opacity",E)),P.transform.mProps.p.setGroupProperty(PropertyInterface("Position",E)),P.transform.mProps.a.setGroupProperty(PropertyInterface("Anchor Point",E)),P.transform.mProps.s.setGroupProperty(PropertyInterface("Scale",E)),P.transform.mProps.r.setGroupProperty(PropertyInterface("Rotation",E)),P.transform.mProps.sk&&(P.transform.mProps.sk.setGroupProperty(PropertyInterface("Skew",E)),P.transform.mProps.sa.setGroupProperty(PropertyInterface("Skew Angle",E))),P.transform.op.setGroupProperty(PropertyInterface("Opacity",E)),Object.defineProperties(x,{opacity:{get:ExpressionPropertyInterface(P.transform.mProps.o)},position:{get:ExpressionPropertyInterface(P.transform.mProps.p)},anchorPoint:{get:ExpressionPropertyInterface(P.transform.mProps.a)},scale:{get:ExpressionPropertyInterface(P.transform.mProps.s)},rotation:{get:ExpressionPropertyInterface(P.transform.mProps.r)},skew:{get:ExpressionPropertyInterface(P.transform.mProps.sk)},skewAxis:{get:ExpressionPropertyInterface(P.transform.mProps.sa)},_name:{value:S.nm}}),x.ty="tr",x.mn=S.mn,x.propertyGroup=k,x}function b(S,P,k){function x(y){return S.p.ix===y?x.position:S.s.ix===y?x.size:null}var E=propertyGroupFactory(x,k);x.propertyIndex=S.ix;var g=P.sh.ty==="tm"?P.sh.prop:P.sh;return g.s.setGroupProperty(PropertyInterface("Size",E)),g.p.setGroupProperty(PropertyInterface("Position",E)),Object.defineProperties(x,{size:{get:ExpressionPropertyInterface(g.s)},position:{get:ExpressionPropertyInterface(g.p)},_name:{value:S.nm}}),x.mn=S.mn,x}function A(S,P,k){function x(y){return S.p.ix===y?x.position:S.r.ix===y?x.rotation:S.pt.ix===y?x.points:S.or.ix===y||y==="ADBE Vector Star Outer Radius"?x.outerRadius:S.os.ix===y?x.outerRoundness:S.ir&&(S.ir.ix===y||y==="ADBE Vector Star Inner Radius")?x.innerRadius:S.is&&S.is.ix===y?x.innerRoundness:null}var E=propertyGroupFactory(x,k),g=P.sh.ty==="tm"?P.sh.prop:P.sh;return x.propertyIndex=S.ix,g.or.setGroupProperty(PropertyInterface("Outer Radius",E)),g.os.setGroupProperty(PropertyInterface("Outer Roundness",E)),g.pt.setGroupProperty(PropertyInterface("Points",E)),g.p.setGroupProperty(PropertyInterface("Position",E)),g.r.setGroupProperty(PropertyInterface("Rotation",E)),S.ir&&(g.ir.setGroupProperty(PropertyInterface("Inner Radius",E)),g.is.setGroupProperty(PropertyInterface("Inner Roundness",E))),Object.defineProperties(x,{position:{get:ExpressionPropertyInterface(g.p)},rotation:{get:ExpressionPropertyInterface(g.r)},points:{get:ExpressionPropertyInterface(g.pt)},outerRadius:{get:ExpressionPropertyInterface(g.or)},outerRoundness:{get:ExpressionPropertyInterface(g.os)},innerRadius:{get:ExpressionPropertyInterface(g.ir)},innerRoundness:{get:ExpressionPropertyInterface(g.is)},_name:{value:S.nm}}),x.mn=S.mn,x}function _(S,P,k){function x(y){return S.p.ix===y?x.position:S.r.ix===y?x.roundness:S.s.ix===y||y==="Size"||y==="ADBE Vector Rect Size"?x.size:null}var E=propertyGroupFactory(x,k),g=P.sh.ty==="tm"?P.sh.prop:P.sh;return x.propertyIndex=S.ix,g.p.setGroupProperty(PropertyInterface("Position",E)),g.s.setGroupProperty(PropertyInterface("Size",E)),g.r.setGroupProperty(PropertyInterface("Rotation",E)),Object.defineProperties(x,{position:{get:ExpressionPropertyInterface(g.p)},roundness:{get:ExpressionPropertyInterface(g.r)},size:{get:ExpressionPropertyInterface(g.s)},_name:{value:S.nm}}),x.mn=S.mn,x}function T(S,P,k){function x(y){return S.r.ix===y||y==="Round Corners 1"?x.radius:null}var E=propertyGroupFactory(x,k),g=P;return x.propertyIndex=S.ix,g.rd.setGroupProperty(PropertyInterface("Radius",E)),Object.defineProperties(x,{radius:{get:ExpressionPropertyInterface(g.rd)},_name:{value:S.nm}}),x.mn=S.mn,x}function C(S,P,k){function x(y){return S.c.ix===y||y==="Copies"?x.copies:S.o.ix===y||y==="Offset"?x.offset:null}var E=propertyGroupFactory(x,k),g=P;return x.propertyIndex=S.ix,g.c.setGroupProperty(PropertyInterface("Copies",E)),g.o.setGroupProperty(PropertyInterface("Offset",E)),Object.defineProperties(x,{copies:{get:ExpressionPropertyInterface(g.c)},offset:{get:ExpressionPropertyInterface(g.o)},_name:{value:S.nm}}),x.mn=S.mn,x}return function(S,P,k){var x;function E(y){if(typeof y=="number")return y=y===void 0?1:y,y===0?k:x[y-1];for(var I=0,M=x.length;I<M;){if(x[I]._name===y)return x[I];I+=1}return null}function g(){return k}return E.propertyGroup=propertyGroupFactory(E,g),x=e(S,P,E.propertyGroup),E.numProperties=x.length,E._name="Contents",E}}(),TextExpressionInterface=function(){return function(e){var r,n;function a(l){switch(l){case"ADBE Text Document":return a.sourceText;default:return null}}return Object.defineProperty(a,"sourceText",{get:function(){e.textProperty.getValue();var c=e.textProperty.currentData.t;return c!==r&&(e.textProperty.currentData.t=r,n=new String(c),n.value=c||new String(c)),n}}),a}}(),getBlendMode=function(){var e={0:"source-over",1:"multiply",2:"screen",3:"overlay",4:"darken",5:"lighten",6:"color-dodge",7:"color-burn",8:"hard-light",9:"soft-light",10:"difference",11:"exclusion",12:"hue",13:"saturation",14:"color",15:"luminosity"};return function(r){return e[r]||""}}();function SliderEffect(e,r,n){this.p=PropertyFactory.getProp(r,e.v,0,0,n)}function AngleEffect(e,r,n){this.p=PropertyFactory.getProp(r,e.v,0,0,n)}function ColorEffect(e,r,n){this.p=PropertyFactory.getProp(r,e.v,1,0,n)}function PointEffect(e,r,n){this.p=PropertyFactory.getProp(r,e.v,1,0,n)}function LayerIndexEffect(e,r,n){this.p=PropertyFactory.getProp(r,e.v,0,0,n)}function MaskIndexEffect(e,r,n){this.p=PropertyFactory.getProp(r,e.v,0,0,n)}function CheckboxEffect(e,r,n){this.p=PropertyFactory.getProp(r,e.v,0,0,n)}function NoValueEffect(){this.p={}}function EffectsManager(e,r){var n=e.ef||[];this.effectElements=[];var a,l=n.length,c;for(a=0;a<l;a+=1)c=new GroupEffect(n[a],r),this.effectElements.push(c)}function GroupEffect(e,r){this.init(e,r)}extendPrototype([DynamicPropertyContainer],GroupEffect),GroupEffect.prototype.getValue=GroupEffect.prototype.iterateDynamicProperties,GroupEffect.prototype.init=function(e,r){this.data=e,this.effectElements=[],this.initDynamicPropertyContainer(r);var n,a=this.data.ef.length,l,c=this.data.ef;for(n=0;n<a;n+=1){switch(l=null,c[n].ty){case 0:l=new SliderEffect(c[n],r,this);break;case 1:l=new AngleEffect(c[n],r,this);break;case 2:l=new ColorEffect(c[n],r,this);break;case 3:l=new PointEffect(c[n],r,this);break;case 4:case 7:l=new CheckboxEffect(c[n],r,this);break;case 10:l=new LayerIndexEffect(c[n],r,this);break;case 11:l=new MaskIndexEffect(c[n],r,this);break;case 5:l=new EffectsManager(c[n],r,this);break;default:l=new NoValueEffect(c[n],r,this);break}l&&this.effectElements.push(l)}};function BaseElement(){}BaseElement.prototype={checkMasks:function(){if(!this.data.hasMask)return!1;for(var r=0,n=this.data.masksProperties.length;r<n;){if(this.data.masksProperties[r].mode!=="n"&&this.data.masksProperties[r].cl!==!1)return!0;r+=1}return!1},initExpressions:function(){this.layerInterface=LayerExpressionInterface(this),this.data.hasMask&&this.maskManager&&this.layerInterface.registerMaskInterface(this.maskManager);var r=EffectsExpressionInterface.createEffectsInterface(this,this.layerInterface);this.layerInterface.registerEffectsInterface(r),this.data.ty===0||this.data.xt?this.compInterface=CompExpressionInterface(this):this.data.ty===4?(this.layerInterface.shapeInterface=ShapeExpressionInterface(this.shapesData,this.itemsData,this.layerInterface),this.layerInterface.content=this.layerInterface.shapeInterface):this.data.ty===5&&(this.layerInterface.textInterface=TextExpressionInterface(this),this.layerInterface.text=this.layerInterface.textInterface)},setBlendMode:function(){var r=getBlendMode(this.data.bm),n=this.baseElement||this.layerElement;n.style["mix-blend-mode"]=r},initBaseData:function(r,n,a){this.globalData=n,this.comp=a,this.data=r,this.layerId=createElementID(),this.data.sr||(this.data.sr=1),this.effectsManager=new EffectsManager(this.data,this,this.dynamicProperties)},getType:function(){return this.type},sourceRectAtTime:function(){}};function FrameElement(){}FrameElement.prototype={initFrame:function(){this._isFirstFrame=!1,this.dynamicProperties=[],this._mdf=!1},prepareProperties:function(r,n){var a,l=this.dynamicProperties.length;for(a=0;a<l;a+=1)(n||this._isParent&&this.dynamicProperties[a].propType==="transform")&&(this.dynamicProperties[a].getValue(),this.dynamicProperties[a]._mdf&&(this.globalData._mdf=!0,this._mdf=!0))},addDynamicProperty:function(r){this.dynamicProperties.indexOf(r)===-1&&this.dynamicProperties.push(r)}};function _typeof$2(e){return typeof Symbol=="function"&&typeof Symbol.iterator=="symbol"?_typeof$2=function(n){return typeof n}:_typeof$2=function(n){return n&&typeof Symbol=="function"&&n.constructor===Symbol&&n!==Symbol.prototype?"symbol":typeof n},_typeof$2(e)}var FootageInterface=function(){var e=function(a){var l="",c=a.getFootageData();function u(){return l="",c=a.getFootageData(),d}function d(m){if(c[m])return l=m,c=c[m],_typeof$2(c)==="object"?d:c;var b=m.indexOf(l);if(b!==-1){var A=parseInt(m.substr(b+l.length),10);return c=c[A],_typeof$2(c)==="object"?d:c}return""}return u},r=function(a){function l(c){return c==="Outline"?l.outlineInterface():null}return l._name="Outline",l.outlineInterface=e(a),l};return function(n){function a(l){return l==="Data"?a.dataInterface:null}return a._name="Data",a.dataInterface=r(n),a}}();function FootageElement(e,r,n){this.initFrame(),this.initRenderable(),this.assetData=r.getAssetData(e.refId),this.footageData=r.imageLoader.getAsset(this.assetData),this.initBaseData(e,r,n)}FootageElement.prototype.prepareFrame=function(){},extendPrototype([RenderableElement,BaseElement,FrameElement],FootageElement),FootageElement.prototype.getBaseElement=function(){return null},FootageElement.prototype.renderFrame=function(){},FootageElement.prototype.destroy=function(){},FootageElement.prototype.initExpressions=function(){this.layerInterface=FootageInterface(this)},FootageElement.prototype.getFootageData=function(){return this.footageData};function AudioElement(e,r,n){this.initFrame(),this.initRenderable(),this.assetData=r.getAssetData(e.refId),this.initBaseData(e,r,n),this._isPlaying=!1,this._canPlay=!1;var a=this.globalData.getAssetsPath(this.assetData);this.audio=this.globalData.audioController.createAudio(a),this._currentTime=0,this.globalData.audioController.addAudio(this),this._volumeMultiplier=1,this._volume=1,this._previousVolume=null,this.tm=e.tm?PropertyFactory.getProp(this,e.tm,0,r.frameRate,this):{_placeholder:!0},this.lv=PropertyFactory.getProp(this,e.au&&e.au.lv?e.au.lv:{k:[100]},1,.01,this)}AudioElement.prototype.prepareFrame=function(e){if(this.prepareRenderableFrame(e,!0),this.prepareProperties(e,!0),this.tm._placeholder)this._currentTime=e/this.data.sr;else{var r=this.tm.v;this._currentTime=r}this._volume=this.lv.v[0];var n=this._volume*this._volumeMultiplier;this._previousVolume!==n&&(this._previousVolume=n,this.audio.volume(n))},extendPrototype([RenderableElement,BaseElement,FrameElement],AudioElement),AudioElement.prototype.renderFrame=function(){this.isInRange&&this._canPlay&&(this._isPlaying?(!this.audio.playing()||Math.abs(this._currentTime/this.globalData.frameRate-this.audio.seek())>.1)&&this.audio.seek(this._currentTime/this.globalData.frameRate):(this.audio.play(),this.audio.seek(this._currentTime/this.globalData.frameRate),this._isPlaying=!0))},AudioElement.prototype.show=function(){},AudioElement.prototype.hide=function(){this.audio.pause(),this._isPlaying=!1},AudioElement.prototype.pause=function(){this.audio.pause(),this._isPlaying=!1,this._canPlay=!1},AudioElement.prototype.resume=function(){this._canPlay=!0},AudioElement.prototype.setRate=function(e){this.audio.rate(e)},AudioElement.prototype.volume=function(e){this._volumeMultiplier=e,this._previousVolume=e*this._volume,this.audio.volume(this._previousVolume)},AudioElement.prototype.getBaseElement=function(){return null},AudioElement.prototype.destroy=function(){},AudioElement.prototype.sourceRectAtTime=function(){},AudioElement.prototype.initExpressions=function(){};function BaseRenderer(){}BaseRenderer.prototype.checkLayers=function(e){var r,n=this.layers.length,a;for(this.completeLayers=!0,r=n-1;r>=0;r-=1)this.elements[r]||(a=this.layers[r],a.ip-a.st<=e-this.layers[r].st&&a.op-a.st>e-this.layers[r].st&&this.buildItem(r)),this.completeLayers=this.elements[r]?this.completeLayers:!1;this.checkPendingElements()},BaseRenderer.prototype.createItem=function(e){switch(e.ty){case 2:return this.createImage(e);case 0:return this.createComp(e);case 1:return this.createSolid(e);case 3:return this.createNull(e);case 4:return this.createShape(e);case 5:return this.createText(e);case 6:return this.createAudio(e);case 13:return this.createCamera(e);case 15:return this.createFootage(e);default:return this.createNull(e)}},BaseRenderer.prototype.createCamera=function(){throw new Error("You're using a 3d camera. Try the html renderer.")},BaseRenderer.prototype.createAudio=function(e){return new AudioElement(e,this.globalData,this)},BaseRenderer.prototype.createFootage=function(e){return new FootageElement(e,this.globalData,this)},BaseRenderer.prototype.buildAllItems=function(){var e,r=this.layers.length;for(e=0;e<r;e+=1)this.buildItem(e);this.checkPendingElements()},BaseRenderer.prototype.includeLayers=function(e){this.completeLayers=!1;var r,n=e.length,a,l=this.layers.length;for(r=0;r<n;r+=1)for(a=0;a<l;){if(this.layers[a].id===e[r].id){this.layers[a]=e[r];break}a+=1}},BaseRenderer.prototype.setProjectInterface=function(e){this.globalData.projectInterface=e},BaseRenderer.prototype.initItems=function(){this.globalData.progressiveLoad||this.buildAllItems()},BaseRenderer.prototype.buildElementParenting=function(e,r,n){for(var a=this.elements,l=this.layers,c=0,u=l.length;c<u;)l[c].ind==r&&(!a[c]||a[c]===!0?(this.buildItem(c),this.addPendingElement(e)):(n.push(a[c]),a[c].setAsParent(),l[c].parent!==void 0?this.buildElementParenting(e,l[c].parent,n):e.setHierarchy(n))),c+=1},BaseRenderer.prototype.addPendingElement=function(e){this.pendingElements.push(e)},BaseRenderer.prototype.searchExtraCompositions=function(e){var r,n=e.length;for(r=0;r<n;r+=1)if(e[r].xt){var a=this.createComp(e[r]);a.initExpressions(),this.globalData.projectInterface.registerComposition(a)}},BaseRenderer.prototype.getElementByPath=function(e){var r=e.shift(),n;if(typeof r=="number")n=this.elements[r];else{var a,l=this.elements.length;for(a=0;a<l;a+=1)if(this.elements[a].data.nm===r){n=this.elements[a];break}}return e.length===0?n:n.getElementByPath(e)},BaseRenderer.prototype.setupGlobalData=function(e,r){this.globalData.fontManager=new FontManager,this.globalData.fontManager.addChars(e.chars),this.globalData.fontManager.addFonts(e.fonts,r),this.globalData.getAssetData=this.animationItem.getAssetData.bind(this.animationItem),this.globalData.getAssetsPath=this.animationItem.getAssetsPath.bind(this.animationItem),this.globalData.imageLoader=this.animationItem.imagePreloader,this.globalData.audioController=this.animationItem.audioController,this.globalData.frameId=0,this.globalData.frameRate=e.fr,this.globalData.nm=e.nm,this.globalData.compSize={w:e.w,h:e.h}};function TransformElement(){}TransformElement.prototype={initTransform:function(){this.finalTransform={mProp:this.data.ks?TransformPropertyFactory.getTransformProperty(this,this.data.ks,this):{o:0},_matMdf:!1,_opMdf:!1,mat:new Matrix},this.data.ao&&(this.finalTransform.mProp.autoOriented=!0),this.data.ty!==11},renderTransform:function(){if(this.finalTransform._opMdf=this.finalTransform.mProp.o._mdf||this._isFirstFrame,this.finalTransform._matMdf=this.finalTransform.mProp._mdf||this._isFirstFrame,this.hierarchy){var r,n=this.finalTransform.mat,a=0,l=this.hierarchy.length;if(!this.finalTransform._matMdf)for(;a<l;){if(this.hierarchy[a].finalTransform.mProp._mdf){this.finalTransform._matMdf=!0;break}a+=1}if(this.finalTransform._matMdf)for(r=this.finalTransform.mProp.v.props,n.cloneFromProps(r),a=0;a<l;a+=1)r=this.hierarchy[a].finalTransform.mProp.v.props,n.transform(r[0],r[1],r[2],r[3],r[4],r[5],r[6],r[7],r[8],r[9],r[10],r[11],r[12],r[13],r[14],r[15])}},globalToLocal:function(r){var n=[];n.push(this.finalTransform);for(var a=!0,l=this.comp;a;)l.finalTransform?(l.data.hasMask&&n.splice(0,0,l.finalTransform),l=l.comp):a=!1;var c,u=n.length,d;for(c=0;c<u;c+=1)d=n[c].mat.applyToPointArray(0,0,0),r=[r[0]-d[0],r[1]-d[1],0];return r},mHelper:new Matrix};function MaskElement(e,r,n){this.data=e,this.element=r,this.globalData=n,this.storedData=[],this.masksProperties=this.data.masksProperties||[],this.maskElement=null;var a=this.globalData.defs,l,c=this.masksProperties?this.masksProperties.length:0;this.viewData=createSizedArray(c),this.solidPath="";var u,d=this.masksProperties,m=0,b=[],A,_,T=createElementID(),C,S,P,k,x="clipPath",E="clip-path";for(l=0;l<c;l+=1)if((d[l].mode!=="a"&&d[l].mode!=="n"||d[l].inv||d[l].o.k!==100||d[l].o.x)&&(x="mask",E="mask"),(d[l].mode==="s"||d[l].mode==="i")&&m===0?(C=createNS("rect"),C.setAttribute("fill","#ffffff"),C.setAttribute("width",this.element.comp.data.w||0),C.setAttribute("height",this.element.comp.data.h||0),b.push(C)):C=null,u=createNS("path"),d[l].mode==="n")this.viewData[l]={op:PropertyFactory.getProp(this.element,d[l].o,0,.01,this.element),prop:ShapePropertyFactory.getShapeProp(this.element,d[l],3),elem:u,lastPath:""},a.appendChild(u);else{m+=1,u.setAttribute("fill",d[l].mode==="s"?"#000000":"#ffffff"),u.setAttribute("clip-rule","nonzero");var g;if(d[l].x.k!==0?(x="mask",E="mask",k=PropertyFactory.getProp(this.element,d[l].x,0,null,this.element),g=createElementID(),S=createNS("filter"),S.setAttribute("id",g),P=createNS("feMorphology"),P.setAttribute("operator","erode"),P.setAttribute("in","SourceGraphic"),P.setAttribute("radius","0"),S.appendChild(P),a.appendChild(S),u.setAttribute("stroke",d[l].mode==="s"?"#000000":"#ffffff")):(P=null,k=null),this.storedData[l]={elem:u,x:k,expan:P,lastPath:"",lastOperator:"",filterId:g,lastRadius:0},d[l].mode==="i"){_=b.length;var y=createNS("g");for(A=0;A<_;A+=1)y.appendChild(b[A]);var I=createNS("mask");I.setAttribute("mask-type","alpha"),I.setAttribute("id",T+"_"+m),I.appendChild(u),a.appendChild(I),y.setAttribute("mask","url("+getLocationHref()+"#"+T+"_"+m+")"),b.length=0,b.push(y)}else b.push(u);d[l].inv&&!this.solidPath&&(this.solidPath=this.createLayerSolidPath()),this.viewData[l]={elem:u,lastPath:"",op:PropertyFactory.getProp(this.element,d[l].o,0,.01,this.element),prop:ShapePropertyFactory.getShapeProp(this.element,d[l],3),invRect:C},this.viewData[l].prop.k||this.drawPath(d[l],this.viewData[l].prop.v,this.viewData[l])}for(this.maskElement=createNS(x),c=b.length,l=0;l<c;l+=1)this.maskElement.appendChild(b[l]);m>0&&(this.maskElement.setAttribute("id",T),this.element.maskedElement.setAttribute(E,"url("+getLocationHref()+"#"+T+")"),a.appendChild(this.maskElement)),this.viewData.length&&this.element.addRenderableComponent(this)}MaskElement.prototype.getMaskProperty=function(e){return this.viewData[e].prop},MaskElement.prototype.renderFrame=function(e){var r=this.element.finalTransform.mat,n,a=this.masksProperties.length;for(n=0;n<a;n+=1)if((this.viewData[n].prop._mdf||e)&&this.drawPath(this.masksProperties[n],this.viewData[n].prop.v,this.viewData[n]),(this.viewData[n].op._mdf||e)&&this.viewData[n].elem.setAttribute("fill-opacity",this.viewData[n].op.v),this.masksProperties[n].mode!=="n"&&(this.viewData[n].invRect&&(this.element.finalTransform.mProp._mdf||e)&&this.viewData[n].invRect.setAttribute("transform",r.getInverseMatrix().to2dCSS()),this.storedData[n].x&&(this.storedData[n].x._mdf||e))){var l=this.storedData[n].expan;this.storedData[n].x.v<0?(this.storedData[n].lastOperator!=="erode"&&(this.storedData[n].lastOperator="erode",this.storedData[n].elem.setAttribute("filter","url("+getLocationHref()+"#"+this.storedData[n].filterId+")")),l.setAttribute("radius",-this.storedData[n].x.v)):(this.storedData[n].lastOperator!=="dilate"&&(this.storedData[n].lastOperator="dilate",this.storedData[n].elem.setAttribute("filter",null)),this.storedData[n].elem.setAttribute("stroke-width",this.storedData[n].x.v*2))}},MaskElement.prototype.getMaskelement=function(){return this.maskElement},MaskElement.prototype.createLayerSolidPath=function(){var e="M0,0 ";return e+=" h"+this.globalData.compSize.w,e+=" v"+this.globalData.compSize.h,e+=" h-"+this.globalData.compSize.w,e+=" v-"+this.globalData.compSize.h+" ",e},MaskElement.prototype.drawPath=function(e,r,n){var a=" M"+r.v[0][0]+","+r.v[0][1],l,c;for(c=r._length,l=1;l<c;l+=1)a+=" C"+r.o[l-1][0]+","+r.o[l-1][1]+" "+r.i[l][0]+","+r.i[l][1]+" "+r.v[l][0]+","+r.v[l][1];if(r.c&&c>1&&(a+=" C"+r.o[l-1][0]+","+r.o[l-1][1]+" "+r.i[0][0]+","+r.i[0][1]+" "+r.v[0][0]+","+r.v[0][1]),n.lastPath!==a){var u="";n.elem&&(r.c&&(u=e.inv?this.solidPath+a:a),n.elem.setAttribute("d",u)),n.lastPath=a}},MaskElement.prototype.destroy=function(){this.element=null,this.globalData=null,this.maskElement=null,this.data=null,this.masksProperties=null};var filtersFactory=function(){var e={};e.createFilter=r,e.createAlphaToLuminanceFilter=n;function r(a,l){var c=createNS("filter");return c.setAttribute("id",a),l!==!0&&(c.setAttribute("filterUnits","objectBoundingBox"),c.setAttribute("x","0%"),c.setAttribute("y","0%"),c.setAttribute("width","100%"),c.setAttribute("height","100%")),c}function n(){var a=createNS("feColorMatrix");return a.setAttribute("type","matrix"),a.setAttribute("color-interpolation-filters","sRGB"),a.setAttribute("values","0 0 0 1 0  0 0 0 1 0  0 0 0 1 0  0 0 0 1 1"),a}return e}(),featureSupport=function(){var e={maskType:!0};return(/MSIE 10/i.test(navigator.userAgent)||/MSIE 9/i.test(navigator.userAgent)||/rv:11.0/i.test(navigator.userAgent)||/Edge\/\d./i.test(navigator.userAgent))&&(e.maskType=!1),e}();function SVGTintFilter(e,r){this.filterManager=r;var n=createNS("feColorMatrix");if(n.setAttribute("type","matrix"),n.setAttribute("color-interpolation-filters","linearRGB"),n.setAttribute("values","0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0 0 0 1 0"),n.setAttribute("result","f1"),e.appendChild(n),n=createNS("feColorMatrix"),n.setAttribute("type","matrix"),n.setAttribute("color-interpolation-filters","sRGB"),n.setAttribute("values","1 0 0 0 0 0 1 0 0 0 0 0 1 0 0 0 0 0 1 0"),n.setAttribute("result","f2"),e.appendChild(n),this.matrixFilter=n,r.effectElements[2].p.v!==100||r.effectElements[2].p.k){var a=createNS("feMerge");e.appendChild(a);var l;l=createNS("feMergeNode"),l.setAttribute("in","SourceGraphic"),a.appendChild(l),l=createNS("feMergeNode"),l.setAttribute("in","f2"),a.appendChild(l)}}SVGTintFilter.prototype.renderFrame=function(e){if(e||this.filterManager._mdf){var r=this.filterManager.effectElements[0].p.v,n=this.filterManager.effectElements[1].p.v,a=this.filterManager.effectElements[2].p.v/100;this.matrixFilter.setAttribute("values",n[0]-r[0]+" 0 0 0 "+r[0]+" "+(n[1]-r[1])+" 0 0 0 "+r[1]+" "+(n[2]-r[2])+" 0 0 0 "+r[2]+" 0 0 0 "+a+" 0")}};function SVGFillFilter(e,r){this.filterManager=r;var n=createNS("feColorMatrix");n.setAttribute("type","matrix"),n.setAttribute("color-interpolation-filters","sRGB"),n.setAttribute("values","1 0 0 0 0 0 1 0 0 0 0 0 1 0 0 0 0 0 1 0"),e.appendChild(n),this.matrixFilter=n}SVGFillFilter.prototype.renderFrame=function(e){if(e||this.filterManager._mdf){var r=this.filterManager.effectElements[2].p.v,n=this.filterManager.effectElements[6].p.v;this.matrixFilter.setAttribute("values","0 0 0 0 "+r[0]+" 0 0 0 0 "+r[1]+" 0 0 0 0 "+r[2]+" 0 0 0 "+n+" 0")}};function SVGStrokeEffect(e,r){this.initialized=!1,this.filterManager=r,this.elem=e,this.paths=[]}SVGStrokeEffect.prototype.initialize=function(){var e=this.elem.layerElement.children||this.elem.layerElement.childNodes,r,n,a,l;for(this.filterManager.effectElements[1].p.v===1?(l=this.elem.maskManager.masksProperties.length,a=0):(a=this.filterManager.effectElements[0].p.v-1,l=a+1),n=createNS("g"),n.setAttribute("fill","none"),n.setAttribute("stroke-linecap","round"),n.setAttribute("stroke-dashoffset",1),a;a<l;a+=1)r=createNS("path"),n.appendChild(r),this.paths.push({p:r,m:a});if(this.filterManager.effectElements[10].p.v===3){var c=createNS("mask"),u=createElementID();c.setAttribute("id",u),c.setAttribute("mask-type","alpha"),c.appendChild(n),this.elem.globalData.defs.appendChild(c);var d=createNS("g");for(d.setAttribute("mask","url("+getLocationHref()+"#"+u+")");e[0];)d.appendChild(e[0]);this.elem.layerElement.appendChild(d),this.masker=c,n.setAttribute("stroke","#fff")}else if(this.filterManager.effectElements[10].p.v===1||this.filterManager.effectElements[10].p.v===2){if(this.filterManager.effectElements[10].p.v===2)for(e=this.elem.layerElement.children||this.elem.layerElement.childNodes;e.length;)this.elem.layerElement.removeChild(e[0]);this.elem.layerElement.appendChild(n),this.elem.layerElement.removeAttribute("mask"),n.setAttribute("stroke","#fff")}this.initialized=!0,this.pathMasker=n},SVGStrokeEffect.prototype.renderFrame=function(e){this.initialized||this.initialize();var r,n=this.paths.length,a,l;for(r=0;r<n;r+=1)if(this.paths[r].m!==-1&&(a=this.elem.maskManager.viewData[this.paths[r].m],l=this.paths[r].p,(e||this.filterManager._mdf||a.prop._mdf)&&l.setAttribute("d",a.lastPath),e||this.filterManager.effectElements[9].p._mdf||this.filterManager.effectElements[4].p._mdf||this.filterManager.effectElements[7].p._mdf||this.filterManager.effectElements[8].p._mdf||a.prop._mdf)){var c;if(this.filterManager.effectElements[7].p.v!==0||this.filterManager.effectElements[8].p.v!==100){var u=Math.min(this.filterManager.effectElements[7].p.v,this.filterManager.effectElements[8].p.v)*.01,d=Math.max(this.filterManager.effectElements[7].p.v,this.filterManager.effectElements[8].p.v)*.01,m=l.getTotalLength();c="0 0 0 "+m*u+" ";var b=m*(d-u),A=1+this.filterManager.effectElements[4].p.v*2*this.filterManager.effectElements[9].p.v*.01,_=Math.floor(b/A),T;for(T=0;T<_;T+=1)c+="1 "+this.filterManager.effectElements[4].p.v*2*this.filterManager.effectElements[9].p.v*.01+" ";c+="0 "+m*10+" 0 0"}else c="1 "+this.filterManager.effectElements[4].p.v*2*this.filterManager.effectElements[9].p.v*.01;l.setAttribute("stroke-dasharray",c)}if((e||this.filterManager.effectElements[4].p._mdf)&&this.pathMasker.setAttribute("stroke-width",this.filterManager.effectElements[4].p.v*2),(e||this.filterManager.effectElements[6].p._mdf)&&this.pathMasker.setAttribute("opacity",this.filterManager.effectElements[6].p.v),(this.filterManager.effectElements[10].p.v===1||this.filterManager.effectElements[10].p.v===2)&&(e||this.filterManager.effectElements[3].p._mdf)){var C=this.filterManager.effectElements[3].p.v;this.pathMasker.setAttribute("stroke","rgb("+bmFloor(C[0]*255)+","+bmFloor(C[1]*255)+","+bmFloor(C[2]*255)+")")}};function SVGTritoneFilter(e,r){this.filterManager=r;var n=createNS("feColorMatrix");n.setAttribute("type","matrix"),n.setAttribute("color-interpolation-filters","linearRGB"),n.setAttribute("values","0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0 0 0 1 0"),n.setAttribute("result","f1"),e.appendChild(n);var a=createNS("feComponentTransfer");a.setAttribute("color-interpolation-filters","sRGB"),e.appendChild(a),this.matrixFilter=a;var l=createNS("feFuncR");l.setAttribute("type","table"),a.appendChild(l),this.feFuncR=l;var c=createNS("feFuncG");c.setAttribute("type","table"),a.appendChild(c),this.feFuncG=c;var u=createNS("feFuncB");u.setAttribute("type","table"),a.appendChild(u),this.feFuncB=u}SVGTritoneFilter.prototype.renderFrame=function(e){if(e||this.filterManager._mdf){var r=this.filterManager.effectElements[0].p.v,n=this.filterManager.effectElements[1].p.v,a=this.filterManager.effectElements[2].p.v,l=a[0]+" "+n[0]+" "+r[0],c=a[1]+" "+n[1]+" "+r[1],u=a[2]+" "+n[2]+" "+r[2];this.feFuncR.setAttribute("tableValues",l),this.feFuncG.setAttribute("tableValues",c),this.feFuncB.setAttribute("tableValues",u)}};function SVGProLevelsFilter(e,r){this.filterManager=r;var n=this.filterManager.effectElements,a=createNS("feComponentTransfer");(n[10].p.k||n[10].p.v!==0||n[11].p.k||n[11].p.v!==1||n[12].p.k||n[12].p.v!==1||n[13].p.k||n[13].p.v!==0||n[14].p.k||n[14].p.v!==1)&&(this.feFuncR=this.createFeFunc("feFuncR",a)),(n[17].p.k||n[17].p.v!==0||n[18].p.k||n[18].p.v!==1||n[19].p.k||n[19].p.v!==1||n[20].p.k||n[20].p.v!==0||n[21].p.k||n[21].p.v!==1)&&(this.feFuncG=this.createFeFunc("feFuncG",a)),(n[24].p.k||n[24].p.v!==0||n[25].p.k||n[25].p.v!==1||n[26].p.k||n[26].p.v!==1||n[27].p.k||n[27].p.v!==0||n[28].p.k||n[28].p.v!==1)&&(this.feFuncB=this.createFeFunc("feFuncB",a)),(n[31].p.k||n[31].p.v!==0||n[32].p.k||n[32].p.v!==1||n[33].p.k||n[33].p.v!==1||n[34].p.k||n[34].p.v!==0||n[35].p.k||n[35].p.v!==1)&&(this.feFuncA=this.createFeFunc("feFuncA",a)),(this.feFuncR||this.feFuncG||this.feFuncB||this.feFuncA)&&(a.setAttribute("color-interpolation-filters","sRGB"),e.appendChild(a),a=createNS("feComponentTransfer")),(n[3].p.k||n[3].p.v!==0||n[4].p.k||n[4].p.v!==1||n[5].p.k||n[5].p.v!==1||n[6].p.k||n[6].p.v!==0||n[7].p.k||n[7].p.v!==1)&&(a.setAttribute("color-interpolation-filters","sRGB"),e.appendChild(a),this.feFuncRComposed=this.createFeFunc("feFuncR",a),this.feFuncGComposed=this.createFeFunc("feFuncG",a),this.feFuncBComposed=this.createFeFunc("feFuncB",a))}SVGProLevelsFilter.prototype.createFeFunc=function(e,r){var n=createNS(e);return n.setAttribute("type","table"),r.appendChild(n),n},SVGProLevelsFilter.prototype.getTableValue=function(e,r,n,a,l){for(var c=0,u=256,d,m=Math.min(e,r),b=Math.max(e,r),A=Array.call(null,{length:u}),_,T=0,C=l-a,S=r-e;c<=256;)d=c/256,d<=m?_=S<0?l:a:d>=b?_=S<0?a:l:_=a+C*Math.pow((d-e)/S,1/n),A[T]=_,T+=1,c+=256/(u-1);return A.join(" ")},SVGProLevelsFilter.prototype.renderFrame=function(e){if(e||this.filterManager._mdf){var r,n=this.filterManager.effectElements;this.feFuncRComposed&&(e||n[3].p._mdf||n[4].p._mdf||n[5].p._mdf||n[6].p._mdf||n[7].p._mdf)&&(r=this.getTableValue(n[3].p.v,n[4].p.v,n[5].p.v,n[6].p.v,n[7].p.v),this.feFuncRComposed.setAttribute("tableValues",r),this.feFuncGComposed.setAttribute("tableValues",r),this.feFuncBComposed.setAttribute("tableValues",r)),this.feFuncR&&(e||n[10].p._mdf||n[11].p._mdf||n[12].p._mdf||n[13].p._mdf||n[14].p._mdf)&&(r=this.getTableValue(n[10].p.v,n[11].p.v,n[12].p.v,n[13].p.v,n[14].p.v),this.feFuncR.setAttribute("tableValues",r)),this.feFuncG&&(e||n[17].p._mdf||n[18].p._mdf||n[19].p._mdf||n[20].p._mdf||n[21].p._mdf)&&(r=this.getTableValue(n[17].p.v,n[18].p.v,n[19].p.v,n[20].p.v,n[21].p.v),this.feFuncG.setAttribute("tableValues",r)),this.feFuncB&&(e||n[24].p._mdf||n[25].p._mdf||n[26].p._mdf||n[27].p._mdf||n[28].p._mdf)&&(r=this.getTableValue(n[24].p.v,n[25].p.v,n[26].p.v,n[27].p.v,n[28].p.v),this.feFuncB.setAttribute("tableValues",r)),this.feFuncA&&(e||n[31].p._mdf||n[32].p._mdf||n[33].p._mdf||n[34].p._mdf||n[35].p._mdf)&&(r=this.getTableValue(n[31].p.v,n[32].p.v,n[33].p.v,n[34].p.v,n[35].p.v),this.feFuncA.setAttribute("tableValues",r))}};function SVGDropShadowEffect(e,r){var n=r.container.globalData.renderConfig.filterSize;e.setAttribute("x",n.x),e.setAttribute("y",n.y),e.setAttribute("width",n.width),e.setAttribute("height",n.height),this.filterManager=r;var a=createNS("feGaussianBlur");a.setAttribute("in","SourceAlpha"),a.setAttribute("result","drop_shadow_1"),a.setAttribute("stdDeviation","0"),this.feGaussianBlur=a,e.appendChild(a);var l=createNS("feOffset");l.setAttribute("dx","25"),l.setAttribute("dy","0"),l.setAttribute("in","drop_shadow_1"),l.setAttribute("result","drop_shadow_2"),this.feOffset=l,e.appendChild(l);var c=createNS("feFlood");c.setAttribute("flood-color","#00ff00"),c.setAttribute("flood-opacity","1"),c.setAttribute("result","drop_shadow_3"),this.feFlood=c,e.appendChild(c);var u=createNS("feComposite");u.setAttribute("in","drop_shadow_3"),u.setAttribute("in2","drop_shadow_2"),u.setAttribute("operator","in"),u.setAttribute("result","drop_shadow_4"),e.appendChild(u);var d=createNS("feMerge");e.appendChild(d);var m;m=createNS("feMergeNode"),d.appendChild(m),m=createNS("feMergeNode"),m.setAttribute("in","SourceGraphic"),this.feMergeNode=m,this.feMerge=d,this.originalNodeAdded=!1,d.appendChild(m)}SVGDropShadowEffect.prototype.renderFrame=function(e){if(e||this.filterManager._mdf){if((e||this.filterManager.effectElements[4].p._mdf)&&this.feGaussianBlur.setAttribute("stdDeviation",this.filterManager.effectElements[4].p.v/4),e||this.filterManager.effectElements[0].p._mdf){var r=this.filterManager.effectElements[0].p.v;this.feFlood.setAttribute("flood-color",rgbToHex(Math.round(r[0]*255),Math.round(r[1]*255),Math.round(r[2]*255)))}if((e||this.filterManager.effectElements[1].p._mdf)&&this.feFlood.setAttribute("flood-opacity",this.filterManager.effectElements[1].p.v/255),e||this.filterManager.effectElements[2].p._mdf||this.filterManager.effectElements[3].p._mdf){var n=this.filterManager.effectElements[3].p.v,a=(this.filterManager.effectElements[2].p.v-90)*degToRads,l=n*Math.cos(a),c=n*Math.sin(a);this.feOffset.setAttribute("dx",l),this.feOffset.setAttribute("dy",c)}}};var _svgMatteSymbols=[];function SVGMatte3Effect(e,r,n){this.initialized=!1,this.filterManager=r,this.filterElem=e,this.elem=n,n.matteElement=createNS("g"),n.matteElement.appendChild(n.layerElement),n.matteElement.appendChild(n.transformedElement),n.baseElement=n.matteElement}SVGMatte3Effect.prototype.findSymbol=function(e){for(var r=0,n=_svgMatteSymbols.length;r<n;){if(_svgMatteSymbols[r]===e)return _svgMatteSymbols[r];r+=1}return null},SVGMatte3Effect.prototype.replaceInParent=function(e,r){var n=e.layerElement.parentNode;if(!!n){for(var a=n.children,l=0,c=a.length;l<c&&a[l]!==e.layerElement;)l+=1;var u;l<=c-2&&(u=a[l+1]);var d=createNS("use");d.setAttribute("href","#"+r),u?n.insertBefore(d,u):n.appendChild(d)}},SVGMatte3Effect.prototype.setElementAsMask=function(e,r){if(!this.findSymbol(r)){var n=createElementID(),a=createNS("mask");a.setAttribute("id",r.layerId),a.setAttribute("mask-type","alpha"),_svgMatteSymbols.push(r);var l=e.globalData.defs;l.appendChild(a);var c=createNS("symbol");c.setAttribute("id",n),this.replaceInParent(r,n),c.appendChild(r.layerElement),l.appendChild(c);var u=createNS("use");u.setAttribute("href","#"+n),a.appendChild(u),r.data.hd=!1,r.show()}e.setMatte(r.layerId)},SVGMatte3Effect.prototype.initialize=function(){for(var e=this.filterManager.effectElements[0].p.v,r=this.elem.comp.elements,n=0,a=r.length;n<a;)r[n]&&r[n].data.ind===e&&this.setElementAsMask(this.elem,r[n]),n+=1;this.initialized=!0},SVGMatte3Effect.prototype.renderFrame=function(){this.initialized||this.initialize()};function SVGGaussianBlurEffect(e,r){e.setAttribute("x","-100%"),e.setAttribute("y","-100%"),e.setAttribute("width","300%"),e.setAttribute("height","300%"),this.filterManager=r;var n=createNS("feGaussianBlur");e.appendChild(n),this.feGaussianBlur=n}SVGGaussianBlurEffect.prototype.renderFrame=function(e){if(e||this.filterManager._mdf){var r=.3,n=this.filterManager.effectElements[0].p.v*r,a=this.filterManager.effectElements[1].p.v,l=a==3?0:n,c=a==2?0:n;this.feGaussianBlur.setAttribute("stdDeviation",l+" "+c);var u=this.filterManager.effectElements[2].p.v==1?"wrap":"duplicate";this.feGaussianBlur.setAttribute("edgeMode",u)}};var registeredEffects={};function SVGEffects(e){var r,n=e.data.ef?e.data.ef.length:0,a=createElementID(),l=filtersFactory.createFilter(a,!0),c=0;this.filters=[];var u;for(r=0;r<n;r+=1){u=null;var d=e.data.ef[r].ty;if(registeredEffects[d]){var m=registeredEffects[d].effect;u=new m(l,e.effectsManager.effectElements[r],e),registeredEffects[d].countsAsEffect&&(c+=1)}e.data.ef[r].ty===20?(c+=1,u=new SVGTintFilter(l,e.effectsManager.effectElements[r])):e.data.ef[r].ty===21?(c+=1,u=new SVGFillFilter(l,e.effectsManager.effectElements[r])):e.data.ef[r].ty===22?u=new SVGStrokeEffect(e,e.effectsManager.effectElements[r]):e.data.ef[r].ty===23?(c+=1,u=new SVGTritoneFilter(l,e.effectsManager.effectElements[r])):e.data.ef[r].ty===24?(c+=1,u=new SVGProLevelsFilter(l,e.effectsManager.effectElements[r])):e.data.ef[r].ty===25?(c+=1,u=new SVGDropShadowEffect(l,e.effectsManager.effectElements[r])):e.data.ef[r].ty===28?u=new SVGMatte3Effect(l,e.effectsManager.effectElements[r],e):e.data.ef[r].ty===29&&(c+=1,u=new SVGGaussianBlurEffect(l,e.effectsManager.effectElements[r])),u&&this.filters.push(u)}c&&(e.globalData.defs.appendChild(l),e.layerElement.setAttribute("filter","url("+getLocationHref()+"#"+a+")")),this.filters.length&&e.addRenderableComponent(this)}SVGEffects.prototype.renderFrame=function(e){var r,n=this.filters.length;for(r=0;r<n;r+=1)this.filters[r].renderFrame(e)};function registerEffect(e,r,n){registeredEffects[e]={effect:r,countsAsEffect:n}}function SVGBaseElement(){}SVGBaseElement.prototype={initRendererElement:function(){this.layerElement=createNS("g")},createContainerElements:function(){this.matteElement=createNS("g"),this.transformedElement=this.layerElement,this.maskedElement=this.layerElement,this._sizeChanged=!1;var r=null,n,a,l;if(this.data.td){if(this.data.td==3||this.data.td==1){var c=createNS("mask");c.setAttribute("id",this.layerId),c.setAttribute("mask-type",this.data.td==3?"luminance":"alpha"),c.appendChild(this.layerElement),r=c,this.globalData.defs.appendChild(c),!featureSupport.maskType&&this.data.td==1&&(c.setAttribute("mask-type","luminance"),n=createElementID(),a=filtersFactory.createFilter(n),this.globalData.defs.appendChild(a),a.appendChild(filtersFactory.createAlphaToLuminanceFilter()),l=createNS("g"),l.appendChild(this.layerElement),r=l,c.appendChild(l),l.setAttribute("filter","url("+getLocationHref()+"#"+n+")"))}else if(this.data.td==2){var u=createNS("mask");u.setAttribute("id",this.layerId),u.setAttribute("mask-type","alpha");var d=createNS("g");u.appendChild(d),n=createElementID(),a=filtersFactory.createFilter(n);var m=createNS("feComponentTransfer");m.setAttribute("in","SourceGraphic"),a.appendChild(m);var b=createNS("feFuncA");b.setAttribute("type","table"),b.setAttribute("tableValues","1.0 0.0"),m.appendChild(b),this.globalData.defs.appendChild(a);var A=createNS("rect");A.setAttribute("width",this.comp.data.w),A.setAttribute("height",this.comp.data.h),A.setAttribute("x","0"),A.setAttribute("y","0"),A.setAttribute("fill","#ffffff"),A.setAttribute("opacity","0"),d.setAttribute("filter","url("+getLocationHref()+"#"+n+")"),d.appendChild(A),d.appendChild(this.layerElement),r=d,featureSupport.maskType||(u.setAttribute("mask-type","luminance"),a.appendChild(filtersFactory.createAlphaToLuminanceFilter()),l=createNS("g"),d.appendChild(A),l.appendChild(this.layerElement),r=l,d.appendChild(l)),this.globalData.defs.appendChild(u)}}else this.data.tt?(this.matteElement.appendChild(this.layerElement),r=this.matteElement,this.baseElement=this.matteElement):this.baseElement=this.layerElement;if(this.data.ln&&this.layerElement.setAttribute("id",this.data.ln),this.data.cl&&this.layerElement.setAttribute("class",this.data.cl),this.data.ty===0&&!this.data.hd){var _=createNS("clipPath"),T=createNS("path");T.setAttribute("d","M0,0 L"+this.data.w+",0 L"+this.data.w+","+this.data.h+" L0,"+this.data.h+"z");var C=createElementID();if(_.setAttribute("id",C),_.appendChild(T),this.globalData.defs.appendChild(_),this.checkMasks()){var S=createNS("g");S.setAttribute("clip-path","url("+getLocationHref()+"#"+C+")"),S.appendChild(this.layerElement),this.transformedElement=S,r?r.appendChild(this.transformedElement):this.baseElement=this.transformedElement}else this.layerElement.setAttribute("clip-path","url("+getLocationHref()+"#"+C+")")}this.data.bm!==0&&this.setBlendMode()},renderElement:function(){this.finalTransform._matMdf&&this.transformedElement.setAttribute("transform",this.finalTransform.mat.to2dCSS()),this.finalTransform._opMdf&&this.transformedElement.setAttribute("opacity",this.finalTransform.mProp.o.v)},destroyBaseElement:function(){this.layerElement=null,this.matteElement=null,this.maskManager.destroy()},getBaseElement:function(){return this.data.hd?null:this.baseElement},createRenderableComponents:function(){this.maskManager=new MaskElement(this.data,this,this.globalData),this.renderableEffectsManager=new SVGEffects(this)},setMatte:function(r){!this.matteElement||this.matteElement.setAttribute("mask","url("+getLocationHref()+"#"+r+")")}};function HierarchyElement(){}HierarchyElement.prototype={initHierarchy:function(){this.hierarchy=[],this._isParent=!1,this.checkParenting()},setHierarchy:function(r){this.hierarchy=r},setAsParent:function(){this._isParent=!0},checkParenting:function(){this.data.parent!==void 0&&this.comp.buildElementParenting(this,this.data.parent,[])}};function RenderableDOMElement(){}(function(){var e={initElement:function(n,a,l){this.initFrame(),this.initBaseData(n,a,l),this.initTransform(n,a,l),this.initHierarchy(),this.initRenderable(),this.initRendererElement(),this.createContainerElements(),this.createRenderableComponents(),this.createContent(),this.hide()},hide:function(){if(!this.hidden&&(!this.isInRange||this.isTransparent)){var n=this.baseElement||this.layerElement;n.style.display="none",this.hidden=!0}},show:function(){if(this.isInRange&&!this.isTransparent){if(!this.data.hd){var n=this.baseElement||this.layerElement;n.style.display="block"}this.hidden=!1,this._isFirstFrame=!0}},renderFrame:function(){this.data.hd||this.hidden||(this.renderTransform(),this.renderRenderable(),this.renderElement(),this.renderInnerContent(),this._isFirstFrame&&(this._isFirstFrame=!1))},renderInnerContent:function(){},prepareFrame:function(n){this._mdf=!1,this.prepareRenderableFrame(n),this.prepareProperties(n,this.isInRange),this.checkTransparency()},destroy:function(){this.innerElem=null,this.destroyBaseElement()}};extendPrototype([RenderableElement,createProxyFunction(e)],RenderableDOMElement)})();function IImageElement(e,r,n){this.assetData=r.getAssetData(e.refId),this.initElement(e,r,n),this.sourceRect={top:0,left:0,width:this.assetData.w,height:this.assetData.h}}extendPrototype([BaseElement,TransformElement,SVGBaseElement,HierarchyElement,FrameElement,RenderableDOMElement],IImageElement),IImageElement.prototype.createContent=function(){var e=this.globalData.getAssetsPath(this.assetData);this.innerElem=createNS("image"),this.innerElem.setAttribute("width",this.assetData.w+"px"),this.innerElem.setAttribute("height",this.assetData.h+"px"),this.innerElem.setAttribute("preserveAspectRatio",this.assetData.pr||this.globalData.renderConfig.imagePreserveAspectRatio),this.innerElem.setAttributeNS("http://www.w3.org/1999/xlink","href",e),this.layerElement.appendChild(this.innerElem)},IImageElement.prototype.sourceRectAtTime=function(){return this.sourceRect};function ProcessedElement(e,r){this.elem=e,this.pos=r}function IShapeElement(){}IShapeElement.prototype={addShapeToModifiers:function(r){var n,a=this.shapeModifiers.length;for(n=0;n<a;n+=1)this.shapeModifiers[n].addShape(r)},isShapeInAnimatedModifiers:function(r){for(var n=0,a=this.shapeModifiers.length;n<a;)if(this.shapeModifiers[n].isAnimatedWithShape(r))return!0;return!1},renderModifiers:function(){if(!!this.shapeModifiers.length){var r,n=this.shapes.length;for(r=0;r<n;r+=1)this.shapes[r].sh.reset();n=this.shapeModifiers.length;var a;for(r=n-1;r>=0&&(a=this.shapeModifiers[r].processShapes(this._isFirstFrame),!a);r-=1);}},searchProcessedElement:function(r){for(var n=this.processedElements,a=0,l=n.length;a<l;){if(n[a].elem===r)return n[a].pos;a+=1}return 0},addProcessedElement:function(r,n){for(var a=this.processedElements,l=a.length;l;)if(l-=1,a[l].elem===r){a[l].pos=n;return}a.push(new ProcessedElement(r,n))},prepareFrame:function(r){this.prepareRenderableFrame(r),this.prepareProperties(r,this.isInRange)}};var lineCapEnum={1:"butt",2:"round",3:"square"},lineJoinEnum={1:"miter",2:"round",3:"bevel"};function SVGShapeData(e,r,n){this.caches=[],this.styles=[],this.transformers=e,this.lStr="",this.sh=n,this.lvl=r,this._isAnimated=!!n.k;for(var a=0,l=e.length;a<l;){if(e[a].mProps.dynamicProperties.length){this._isAnimated=!0;break}a+=1}}SVGShapeData.prototype.setAsAnimated=function(){this._isAnimated=!0};function SVGStyleData(e,r){this.data=e,this.type=e.ty,this.d="",this.lvl=r,this._mdf=!1,this.closed=e.hd===!0,this.pElem=createNS("path"),this.msElem=null}SVGStyleData.prototype.reset=function(){this.d="",this._mdf=!1};function DashProperty(e,r,n,a){this.elem=e,this.frameId=-1,this.dataProps=createSizedArray(r.length),this.renderer=n,this.k=!1,this.dashStr="",this.dashArray=createTypedArray("float32",r.length?r.length-1:0),this.dashoffset=createTypedArray("float32",1),this.initDynamicPropertyContainer(a);var l,c=r.length||0,u;for(l=0;l<c;l+=1)u=PropertyFactory.getProp(e,r[l].v,0,0,this),this.k=u.k||this.k,this.dataProps[l]={n:r[l].n,p:u};this.k||this.getValue(!0),this._isAnimated=this.k}DashProperty.prototype.getValue=function(e){if(!(this.elem.globalData.frameId===this.frameId&&!e)&&(this.frameId=this.elem.globalData.frameId,this.iterateDynamicProperties(),this._mdf=this._mdf||e,this._mdf)){var r=0,n=this.dataProps.length;for(this.renderer==="svg"&&(this.dashStr=""),r=0;r<n;r+=1)this.dataProps[r].n!=="o"?this.renderer==="svg"?this.dashStr+=" "+this.dataProps[r].p.v:this.dashArray[r]=this.dataProps[r].p.v:this.dashoffset[0]=this.dataProps[r].p.v}},extendPrototype([DynamicPropertyContainer],DashProperty);function SVGStrokeStyleData(e,r,n){this.initDynamicPropertyContainer(e),this.getValue=this.iterateDynamicProperties,this.o=PropertyFactory.getProp(e,r.o,0,.01,this),this.w=PropertyFactory.getProp(e,r.w,0,null,this),this.d=new DashProperty(e,r.d||{},"svg",this),this.c=PropertyFactory.getProp(e,r.c,1,255,this),this.style=n,this._isAnimated=!!this._isAnimated}extendPrototype([DynamicPropertyContainer],SVGStrokeStyleData);function SVGFillStyleData(e,r,n){this.initDynamicPropertyContainer(e),this.getValue=this.iterateDynamicProperties,this.o=PropertyFactory.getProp(e,r.o,0,.01,this),this.c=PropertyFactory.getProp(e,r.c,1,255,this),this.style=n}extendPrototype([DynamicPropertyContainer],SVGFillStyleData);function SVGNoStyleData(e,r,n){this.initDynamicPropertyContainer(e),this.getValue=this.iterateDynamicProperties,this.style=n}extendPrototype([DynamicPropertyContainer],SVGNoStyleData);function GradientProperty(e,r,n){this.data=r,this.c=createTypedArray("uint8c",r.p*4);var a=r.k.k[0].s?r.k.k[0].s.length-r.p*4:r.k.k.length-r.p*4;this.o=createTypedArray("float32",a),this._cmdf=!1,this._omdf=!1,this._collapsable=this.checkCollapsable(),this._hasOpacity=a,this.initDynamicPropertyContainer(n),this.prop=PropertyFactory.getProp(e,r.k,1,null,this),this.k=this.prop.k,this.getValue(!0)}GradientProperty.prototype.comparePoints=function(e,r){for(var n=0,a=this.o.length/2,l;n<a;){if(l=Math.abs(e[n*4]-e[r*4+n*2]),l>.01)return!1;n+=1}return!0},GradientProperty.prototype.checkCollapsable=function(){if(this.o.length/2!=this.c.length/4)return!1;if(this.data.k.k[0].s)for(var e=0,r=this.data.k.k.length;e<r;){if(!this.comparePoints(this.data.k.k[e].s,this.data.p))return!1;e+=1}else if(!this.comparePoints(this.data.k.k,this.data.p))return!1;return!0},GradientProperty.prototype.getValue=function(e){if(this.prop.getValue(),this._mdf=!1,this._cmdf=!1,this._omdf=!1,this.prop._mdf||e){var r,n=this.data.p*4,a,l;for(r=0;r<n;r+=1)a=r%4==0?100:255,l=Math.round(this.prop.v[r]*a),this.c[r]!==l&&(this.c[r]=l,this._cmdf=!e);if(this.o.length)for(n=this.prop.v.length,r=this.data.p*4;r<n;r+=1)a=r%2==0?100:1,l=r%2==0?Math.round(this.prop.v[r]*100):this.prop.v[r],this.o[r-this.data.p*4]!==l&&(this.o[r-this.data.p*4]=l,this._omdf=!e);this._mdf=!e}},extendPrototype([DynamicPropertyContainer],GradientProperty);function SVGGradientFillStyleData(e,r,n){this.initDynamicPropertyContainer(e),this.getValue=this.iterateDynamicProperties,this.initGradientData(e,r,n)}SVGGradientFillStyleData.prototype.initGradientData=function(e,r,n){this.o=PropertyFactory.getProp(e,r.o,0,.01,this),this.s=PropertyFactory.getProp(e,r.s,1,null,this),this.e=PropertyFactory.getProp(e,r.e,1,null,this),this.h=PropertyFactory.getProp(e,r.h||{k:0},0,.01,this),this.a=PropertyFactory.getProp(e,r.a||{k:0},0,degToRads,this),this.g=new GradientProperty(e,r.g,this),this.style=n,this.stops=[],this.setGradientData(n.pElem,r),this.setGradientOpacity(r,n),this._isAnimated=!!this._isAnimated},SVGGradientFillStyleData.prototype.setGradientData=function(e,r){var n=createElementID(),a=createNS(r.t===1?"linearGradient":"radialGradient");a.setAttribute("id",n),a.setAttribute("spreadMethod","pad"),a.setAttribute("gradientUnits","userSpaceOnUse");var l=[],c,u,d;for(d=r.g.p*4,u=0;u<d;u+=4)c=createNS("stop"),a.appendChild(c),l.push(c);e.setAttribute(r.ty==="gf"?"fill":"stroke","url("+getLocationHref()+"#"+n+")"),this.gf=a,this.cst=l},SVGGradientFillStyleData.prototype.setGradientOpacity=function(e,r){if(this.g._hasOpacity&&!this.g._collapsable){var n,a,l,c=createNS("mask"),u=createNS("path");c.appendChild(u);var d=createElementID(),m=createElementID();c.setAttribute("id",m);var b=createNS(e.t===1?"linearGradient":"radialGradient");b.setAttribute("id",d),b.setAttribute("spreadMethod","pad"),b.setAttribute("gradientUnits","userSpaceOnUse"),l=e.g.k.k[0].s?e.g.k.k[0].s.length:e.g.k.k.length;var A=this.stops;for(a=e.g.p*4;a<l;a+=2)n=createNS("stop"),n.setAttribute("stop-color","rgb(255,255,255)"),b.appendChild(n),A.push(n);u.setAttribute(e.ty==="gf"?"fill":"stroke","url("+getLocationHref()+"#"+d+")"),e.ty==="gs"&&(u.setAttribute("stroke-linecap",lineCapEnum[e.lc||2]),u.setAttribute("stroke-linejoin",lineJoinEnum[e.lj||2]),e.lj===1&&u.setAttribute("stroke-miterlimit",e.ml)),this.of=b,this.ms=c,this.ost=A,this.maskId=m,r.msElem=u}},extendPrototype([DynamicPropertyContainer],SVGGradientFillStyleData);function SVGGradientStrokeStyleData(e,r,n){this.initDynamicPropertyContainer(e),this.getValue=this.iterateDynamicProperties,this.w=PropertyFactory.getProp(e,r.w,0,null,this),this.d=new DashProperty(e,r.d||{},"svg",this),this.initGradientData(e,r,n),this._isAnimated=!!this._isAnimated}extendPrototype([SVGGradientFillStyleData,DynamicPropertyContainer],SVGGradientStrokeStyleData);function ShapeGroupData(){this.it=[],this.prevViewData=[],this.gr=createNS("g")}function SVGTransformData(e,r,n){this.transform={mProps:e,op:r,container:n},this.elements=[],this._isAnimated=this.transform.mProps.dynamicProperties.length||this.transform.op.effectsSequence.length}var buildShapeString=function(r,n,a,l){if(n===0)return"";var c=r.o,u=r.i,d=r.v,m,b=" M"+l.applyToPointStringified(d[0][0],d[0][1]);for(m=1;m<n;m+=1)b+=" C"+l.applyToPointStringified(c[m-1][0],c[m-1][1])+" "+l.applyToPointStringified(u[m][0],u[m][1])+" "+l.applyToPointStringified(d[m][0],d[m][1]);return a&&n&&(b+=" C"+l.applyToPointStringified(c[m-1][0],c[m-1][1])+" "+l.applyToPointStringified(u[0][0],u[0][1])+" "+l.applyToPointStringified(d[0][0],d[0][1]),b+="z"),b},SVGElementsRenderer=function(){var e=new Matrix,r=new Matrix,n={createRenderFunction:a};function a(_){switch(_.ty){case"fl":return d;case"gf":return b;case"gs":return m;case"st":return A;case"sh":case"el":case"rc":case"sr":return u;case"tr":return l;case"no":return c;default:return null}}function l(_,T,C){(C||T.transform.op._mdf)&&T.transform.container.setAttribute("opacity",T.transform.op.v),(C||T.transform.mProps._mdf)&&T.transform.container.setAttribute("transform",T.transform.mProps.v.to2dCSS())}function c(){}function u(_,T,C){var S,P,k,x,E,g,y=T.styles.length,I=T.lvl,M,j,O,R,B;for(g=0;g<y;g+=1){if(x=T.sh._mdf||C,T.styles[g].lvl<I){for(j=r.reset(),R=I-T.styles[g].lvl,B=T.transformers.length-1;!x&&R>0;)x=T.transformers[B].mProps._mdf||x,R-=1,B-=1;if(x)for(R=I-T.styles[g].lvl,B=T.transformers.length-1;R>0;)O=T.transformers[B].mProps.v.props,j.transform(O[0],O[1],O[2],O[3],O[4],O[5],O[6],O[7],O[8],O[9],O[10],O[11],O[12],O[13],O[14],O[15]),R-=1,B-=1}else j=e;if(M=T.sh.paths,P=M._length,x){for(k="",S=0;S<P;S+=1)E=M.shapes[S],E&&E._length&&(k+=buildShapeString(E,E._length,E.c,j));T.caches[g]=k}else k=T.caches[g];T.styles[g].d+=_.hd===!0?"":k,T.styles[g]._mdf=x||T.styles[g]._mdf}}function d(_,T,C){var S=T.style;(T.c._mdf||C)&&S.pElem.setAttribute("fill","rgb("+bmFloor(T.c.v[0])+","+bmFloor(T.c.v[1])+","+bmFloor(T.c.v[2])+")"),(T.o._mdf||C)&&S.pElem.setAttribute("fill-opacity",T.o.v)}function m(_,T,C){b(_,T,C),A(_,T,C)}function b(_,T,C){var S=T.gf,P=T.g._hasOpacity,k=T.s.v,x=T.e.v;if(T.o._mdf||C){var E=_.ty==="gf"?"fill-opacity":"stroke-opacity";T.style.pElem.setAttribute(E,T.o.v)}if(T.s._mdf||C){var g=_.t===1?"x1":"cx",y=g==="x1"?"y1":"cy";S.setAttribute(g,k[0]),S.setAttribute(y,k[1]),P&&!T.g._collapsable&&(T.of.setAttribute(g,k[0]),T.of.setAttribute(y,k[1]))}var I,M,j,O;if(T.g._cmdf||C){I=T.cst;var R=T.g.c;for(j=I.length,M=0;M<j;M+=1)O=I[M],O.setAttribute("offset",R[M*4]+"%"),O.setAttribute("stop-color","rgb("+R[M*4+1]+","+R[M*4+2]+","+R[M*4+3]+")")}if(P&&(T.g._omdf||C)){var B=T.g.o;for(T.g._collapsable?I=T.cst:I=T.ost,j=I.length,M=0;M<j;M+=1)O=I[M],T.g._collapsable||O.setAttribute("offset",B[M*2]+"%"),O.setAttribute("stop-opacity",B[M*2+1])}if(_.t===1)(T.e._mdf||C)&&(S.setAttribute("x2",x[0]),S.setAttribute("y2",x[1]),P&&!T.g._collapsable&&(T.of.setAttribute("x2",x[0]),T.of.setAttribute("y2",x[1])));else{var G;if((T.s._mdf||T.e._mdf||C)&&(G=Math.sqrt(Math.pow(k[0]-x[0],2)+Math.pow(k[1]-x[1],2)),S.setAttribute("r",G),P&&!T.g._collapsable&&T.of.setAttribute("r",G)),T.e._mdf||T.h._mdf||T.a._mdf||C){G||(G=Math.sqrt(Math.pow(k[0]-x[0],2)+Math.pow(k[1]-x[1],2)));var V=Math.atan2(x[1]-k[1],x[0]-k[0]),Q=T.h.v;Q>=1?Q=.99:Q<=-1&&(Q=-.99);var W=G*Q,q=Math.cos(V+T.a.v)*W+k[0],L=Math.sin(V+T.a.v)*W+k[1];S.setAttribute("fx",q),S.setAttribute("fy",L),P&&!T.g._collapsable&&(T.of.setAttribute("fx",q),T.of.setAttribute("fy",L))}}}function A(_,T,C){var S=T.style,P=T.d;P&&(P._mdf||C)&&P.dashStr&&(S.pElem.setAttribute("stroke-dasharray",P.dashStr),S.pElem.setAttribute("stroke-dashoffset",P.dashoffset[0])),T.c&&(T.c._mdf||C)&&S.pElem.setAttribute("stroke","rgb("+bmFloor(T.c.v[0])+","+bmFloor(T.c.v[1])+","+bmFloor(T.c.v[2])+")"),(T.o._mdf||C)&&S.pElem.setAttribute("stroke-opacity",T.o.v),(T.w._mdf||C)&&(S.pElem.setAttribute("stroke-width",T.w.v),S.msElem&&S.msElem.setAttribute("stroke-width",T.w.v))}return n}();function SVGShapeElement(e,r,n){this.shapes=[],this.shapesData=e.shapes,this.stylesList=[],this.shapeModifiers=[],this.itemsData=[],this.processedElements=[],this.animatedContents=[],this.initElement(e,r,n),this.prevViewData=[]}extendPrototype([BaseElement,TransformElement,SVGBaseElement,IShapeElement,HierarchyElement,FrameElement,RenderableDOMElement],SVGShapeElement),SVGShapeElement.prototype.initSecondaryElement=function(){},SVGShapeElement.prototype.identityMatrix=new Matrix,SVGShapeElement.prototype.buildExpressionInterface=function(){},SVGShapeElement.prototype.createContent=function(){this.searchShapes(this.shapesData,this.itemsData,this.prevViewData,this.layerElement,0,[],!0),this.filterUniqueShapes()},SVGShapeElement.prototype.filterUniqueShapes=function(){var e,r=this.shapes.length,n,a,l=this.stylesList.length,c,u=[],d=!1;for(a=0;a<l;a+=1){for(c=this.stylesList[a],d=!1,u.length=0,e=0;e<r;e+=1)n=this.shapes[e],n.styles.indexOf(c)!==-1&&(u.push(n),d=n._isAnimated||d);u.length>1&&d&&this.setShapesAsAnimated(u)}},SVGShapeElement.prototype.setShapesAsAnimated=function(e){var r,n=e.length;for(r=0;r<n;r+=1)e[r].setAsAnimated()},SVGShapeElement.prototype.createStyleElement=function(e,r){var n,a=new SVGStyleData(e,r),l=a.pElem;if(e.ty==="st")n=new SVGStrokeStyleData(this,e,a);else if(e.ty==="fl")n=new SVGFillStyleData(this,e,a);else if(e.ty==="gf"||e.ty==="gs"){var c=e.ty==="gf"?SVGGradientFillStyleData:SVGGradientStrokeStyleData;n=new c(this,e,a),this.globalData.defs.appendChild(n.gf),n.maskId&&(this.globalData.defs.appendChild(n.ms),this.globalData.defs.appendChild(n.of),l.setAttribute("mask","url("+getLocationHref()+"#"+n.maskId+")"))}else e.ty==="no"&&(n=new SVGNoStyleData(this,e,a));return(e.ty==="st"||e.ty==="gs")&&(l.setAttribute("stroke-linecap",lineCapEnum[e.lc||2]),l.setAttribute("stroke-linejoin",lineJoinEnum[e.lj||2]),l.setAttribute("fill-opacity","0"),e.lj===1&&l.setAttribute("stroke-miterlimit",e.ml)),e.r===2&&l.setAttribute("fill-rule","evenodd"),e.ln&&l.setAttribute("id",e.ln),e.cl&&l.setAttribute("class",e.cl),e.bm&&(l.style["mix-blend-mode"]=getBlendMode(e.bm)),this.stylesList.push(a),this.addToAnimatedContents(e,n),n},SVGShapeElement.prototype.createGroupElement=function(e){var r=new ShapeGroupData;return e.ln&&r.gr.setAttribute("id",e.ln),e.cl&&r.gr.setAttribute("class",e.cl),e.bm&&(r.gr.style["mix-blend-mode"]=getBlendMode(e.bm)),r},SVGShapeElement.prototype.createTransformElement=function(e,r){var n=TransformPropertyFactory.getTransformProperty(this,e,this),a=new SVGTransformData(n,n.o,r);return this.addToAnimatedContents(e,a),a},SVGShapeElement.prototype.createShapeElement=function(e,r,n){var a=4;e.ty==="rc"?a=5:e.ty==="el"?a=6:e.ty==="sr"&&(a=7);var l=ShapePropertyFactory.getShapeProp(this,e,a,this),c=new SVGShapeData(r,n,l);return this.shapes.push(c),this.addShapeToModifiers(c),this.addToAnimatedContents(e,c),c},SVGShapeElement.prototype.addToAnimatedContents=function(e,r){for(var n=0,a=this.animatedContents.length;n<a;){if(this.animatedContents[n].element===r)return;n+=1}this.animatedContents.push({fn:SVGElementsRenderer.createRenderFunction(e),element:r,data:e})},SVGShapeElement.prototype.setElementStyles=function(e){var r=e.styles,n,a=this.stylesList.length;for(n=0;n<a;n+=1)this.stylesList[n].closed||r.push(this.stylesList[n])},SVGShapeElement.prototype.reloadShapes=function(){this._isFirstFrame=!0;var e,r=this.itemsData.length;for(e=0;e<r;e+=1)this.prevViewData[e]=this.itemsData[e];for(this.searchShapes(this.shapesData,this.itemsData,this.prevViewData,this.layerElement,0,[],!0),this.filterUniqueShapes(),r=this.dynamicProperties.length,e=0;e<r;e+=1)this.dynamicProperties[e].getValue();this.renderModifiers()},SVGShapeElement.prototype.searchShapes=function(e,r,n,a,l,c,u){var d=[].concat(c),m,b=e.length-1,A,_,T=[],C=[],S,P,k;for(m=b;m>=0;m-=1){if(k=this.searchProcessedElement(e[m]),k?r[m]=n[k-1]:e[m]._render=u,e[m].ty==="fl"||e[m].ty==="st"||e[m].ty==="gf"||e[m].ty==="gs"||e[m].ty==="no")k?r[m].style.closed=!1:r[m]=this.createStyleElement(e[m],l),e[m]._render&&r[m].style.pElem.parentNode!==a&&a.appendChild(r[m].style.pElem),T.push(r[m].style);else if(e[m].ty==="gr"){if(!k)r[m]=this.createGroupElement(e[m]);else for(_=r[m].it.length,A=0;A<_;A+=1)r[m].prevViewData[A]=r[m].it[A];this.searchShapes(e[m].it,r[m].it,r[m].prevViewData,r[m].gr,l+1,d,u),e[m]._render&&r[m].gr.parentNode!==a&&a.appendChild(r[m].gr)}else e[m].ty==="tr"?(k||(r[m]=this.createTransformElement(e[m],a)),S=r[m].transform,d.push(S)):e[m].ty==="sh"||e[m].ty==="rc"||e[m].ty==="el"||e[m].ty==="sr"?(k||(r[m]=this.createShapeElement(e[m],d,l)),this.setElementStyles(r[m])):e[m].ty==="tm"||e[m].ty==="rd"||e[m].ty==="ms"||e[m].ty==="pb"?(k?(P=r[m],P.closed=!1):(P=ShapeModifiers.getModifier(e[m].ty),P.init(this,e[m]),r[m]=P,this.shapeModifiers.push(P)),C.push(P)):e[m].ty==="rp"&&(k?(P=r[m],P.closed=!0):(P=ShapeModifiers.getModifier(e[m].ty),r[m]=P,P.init(this,e,m,r),this.shapeModifiers.push(P),u=!1),C.push(P));this.addProcessedElement(e[m],m+1)}for(b=T.length,m=0;m<b;m+=1)T[m].closed=!0;for(b=C.length,m=0;m<b;m+=1)C[m].closed=!0},SVGShapeElement.prototype.renderInnerContent=function(){this.renderModifiers();var e,r=this.stylesList.length;for(e=0;e<r;e+=1)this.stylesList[e].reset();for(this.renderShape(),e=0;e<r;e+=1)(this.stylesList[e]._mdf||this._isFirstFrame)&&(this.stylesList[e].msElem&&(this.stylesList[e].msElem.setAttribute("d",this.stylesList[e].d),this.stylesList[e].d="M0 0"+this.stylesList[e].d),this.stylesList[e].pElem.setAttribute("d",this.stylesList[e].d||"M0 0"))},SVGShapeElement.prototype.renderShape=function(){var e,r=this.animatedContents.length,n;for(e=0;e<r;e+=1)n=this.animatedContents[e],(this._isFirstFrame||n.element._isAnimated)&&n.data!==!0&&n.fn(n.data,n.element,this._isFirstFrame)},SVGShapeElement.prototype.destroy=function(){this.destroyBaseElement(),this.shapesData=null,this.itemsData=null};function LetterProps(e,r,n,a,l,c){this.o=e,this.sw=r,this.sc=n,this.fc=a,this.m=l,this.p=c,this._mdf={o:!0,sw:!!r,sc:!!n,fc:!!a,m:!0,p:!0}}LetterProps.prototype.update=function(e,r,n,a,l,c){this._mdf.o=!1,this._mdf.sw=!1,this._mdf.sc=!1,this._mdf.fc=!1,this._mdf.m=!1,this._mdf.p=!1;var u=!1;return this.o!==e&&(this.o=e,this._mdf.o=!0,u=!0),this.sw!==r&&(this.sw=r,this._mdf.sw=!0,u=!0),this.sc!==n&&(this.sc=n,this._mdf.sc=!0,u=!0),this.fc!==a&&(this.fc=a,this._mdf.fc=!0,u=!0),this.m!==l&&(this.m=l,this._mdf.m=!0,u=!0),c.length&&(this.p[0]!==c[0]||this.p[1]!==c[1]||this.p[4]!==c[4]||this.p[5]!==c[5]||this.p[12]!==c[12]||this.p[13]!==c[13])&&(this.p=c,this._mdf.p=!0,u=!0),u};function TextProperty(e,r){this._frameId=initialDefaultFrame,this.pv="",this.v="",this.kf=!1,this._isFirstFrame=!0,this._mdf=!1,this.data=r,this.elem=e,this.comp=this.elem.comp,this.keysIndex=0,this.canResize=!1,this.minimumFontSize=1,this.effectsSequence=[],this.currentData={ascent:0,boxWidth:this.defaultBoxWidth,f:"",fStyle:"",fWeight:"",fc:"",j:"",justifyOffset:"",l:[],lh:0,lineWidths:[],ls:"",of:"",s:"",sc:"",sw:0,t:0,tr:0,sz:0,ps:null,fillColorAnim:!1,strokeColorAnim:!1,strokeWidthAnim:!1,yOffset:0,finalSize:0,finalText:[],finalLineHeight:0,__complete:!1},this.copyData(this.currentData,this.data.d.k[0].s),this.searchProperty()||this.completeTextData(this.currentData)}TextProperty.prototype.defaultBoxWidth=[0,0],TextProperty.prototype.copyData=function(e,r){for(var n in r)Object.prototype.hasOwnProperty.call(r,n)&&(e[n]=r[n]);return e},TextProperty.prototype.setCurrentData=function(e){e.__complete||this.completeTextData(e),this.currentData=e,this.currentData.boxWidth=this.currentData.boxWidth||this.defaultBoxWidth,this._mdf=!0},TextProperty.prototype.searchProperty=function(){return this.searchKeyframes()},TextProperty.prototype.searchKeyframes=function(){return this.kf=this.data.d.k.length>1,this.kf&&this.addEffect(this.getKeyframeValue.bind(this)),this.kf},TextProperty.prototype.addEffect=function(e){this.effectsSequence.push(e),this.elem.addDynamicProperty(this)},TextProperty.prototype.getValue=function(e){if(!((this.elem.globalData.frameId===this.frameId||!this.effectsSequence.length)&&!e)){this.currentData.t=this.data.d.k[this.keysIndex].s.t;var r=this.currentData,n=this.keysIndex;if(this.lock){this.setCurrentData(this.currentData);return}this.lock=!0,this._mdf=!1;var a,l=this.effectsSequence.length,c=e||this.data.d.k[this.keysIndex].s;for(a=0;a<l;a+=1)n!==this.keysIndex?c=this.effectsSequence[a](c,c.t):c=this.effectsSequence[a](this.currentData,c.t);r!==c&&this.setCurrentData(c),this.v=this.currentData,this.pv=this.v,this.lock=!1,this.frameId=this.elem.globalData.frameId}},TextProperty.prototype.getKeyframeValue=function(){for(var e=this.data.d.k,r=this.elem.comp.renderedFrame,n=0,a=e.length;n<=a-1&&!(n===a-1||e[n+1].t>r);)n+=1;return this.keysIndex!==n&&(this.keysIndex=n),this.data.d.k[this.keysIndex].s},TextProperty.prototype.buildFinalText=function(e){for(var r=[],n=0,a=e.length,l,c,u=!1;n<a;)l=e.charCodeAt(n),FontManager.isCombinedCharacter(l)?r[r.length-1]+=e.charAt(n):l>=55296&&l<=56319?(c=e.charCodeAt(n+1),c>=56320&&c<=57343?(u||FontManager.isModifier(l,c)?(r[r.length-1]+=e.substr(n,2),u=!1):r.push(e.substr(n,2)),n+=1):r.push(e.charAt(n))):l>56319?(c=e.charCodeAt(n+1),FontManager.isZeroWidthJoiner(l,c)?(u=!0,r[r.length-1]+=e.substr(n,2),n+=1):r.push(e.charAt(n))):FontManager.isZeroWidthJoiner(l)?(r[r.length-1]+=e.charAt(n),u=!0):r.push(e.charAt(n)),n+=1;return r},TextProperty.prototype.completeTextData=function(e){e.__complete=!0;var r=this.elem.globalData.fontManager,n=this.data,a=[],l,c,u,d=0,m,b=n.m.g,A=0,_=0,T=0,C=[],S=0,P=0,k,x,E=r.getFontByName(e.f),g,y=0,I=getFontProperties(E);e.fWeight=I.weight,e.fStyle=I.style,e.finalSize=e.s,e.finalText=this.buildFinalText(e.t),c=e.finalText.length,e.finalLineHeight=e.lh;var M=e.tr/1e3*e.finalSize,j;if(e.sz)for(var O=!0,R=e.sz[0],B=e.sz[1],G,V;O;){V=this.buildFinalText(e.t),G=0,S=0,c=V.length,M=e.tr/1e3*e.finalSize;var Q=-1;for(l=0;l<c;l+=1)j=V[l].charCodeAt(0),u=!1,V[l]===" "?Q=l:(j===13||j===3)&&(S=0,u=!0,G+=e.finalLineHeight||e.finalSize*1.2),r.chars?(g=r.getCharData(V[l],E.fStyle,E.fFamily),y=u?0:g.w*e.finalSize/100):y=r.measureText(V[l],e.f,e.finalSize),S+y>R&&V[l]!==" "?(Q===-1?c+=1:l=Q,G+=e.finalLineHeight||e.finalSize*1.2,V.splice(l,Q===l?1:0,"\r"),Q=-1,S=0):(S+=y,S+=M);G+=E.ascent*e.finalSize/100,this.canResize&&e.finalSize>this.minimumFontSize&&B<G?(e.finalSize-=1,e.finalLineHeight=e.finalSize*e.lh/e.s):(e.finalText=V,c=e.finalText.length,O=!1)}S=-M,y=0;var W=0,q;for(l=0;l<c;l+=1)if(u=!1,q=e.finalText[l],j=q.charCodeAt(0),j===13||j===3?(W=0,C.push(S),P=S>P?S:P,S=-2*M,m="",u=!0,T+=1):m=q,r.chars?(g=r.getCharData(q,E.fStyle,r.getFontByName(e.f).fFamily),y=u?0:g.w*e.finalSize/100):y=r.measureText(m,e.f,e.finalSize),q===" "?W+=y+M:(S+=y+M+W,W=0),a.push({l:y,an:y,add:A,n:u,anIndexes:[],val:m,line:T,animatorJustifyOffset:0}),b==2){if(A+=y,m===""||m===" "||l===c-1){for((m===""||m===" ")&&(A-=y);_<=l;)a[_].an=A,a[_].ind=d,a[_].extra=y,_+=1;d+=1,A=0}}else if(b==3){if(A+=y,m===""||l===c-1){for(m===""&&(A-=y);_<=l;)a[_].an=A,a[_].ind=d,a[_].extra=y,_+=1;A=0,d+=1}}else a[d].ind=d,a[d].extra=0,d+=1;if(e.l=a,P=S>P?S:P,C.push(S),e.sz)e.boxWidth=e.sz[0],e.justifyOffset=0;else switch(e.boxWidth=P,e.j){case 1:e.justifyOffset=-e.boxWidth;break;case 2:e.justifyOffset=-e.boxWidth/2;break;default:e.justifyOffset=0}e.lineWidths=C;var L=n.a,D,F;x=L.length;var $,U,z=[];for(k=0;k<x;k+=1){for(D=L[k],D.a.sc&&(e.strokeColorAnim=!0),D.a.sw&&(e.strokeWidthAnim=!0),(D.a.fc||D.a.fh||D.a.fs||D.a.fb)&&(e.fillColorAnim=!0),U=0,$=D.s.b,l=0;l<c;l+=1)F=a[l],F.anIndexes[k]=U,($==1&&F.val!==""||$==2&&F.val!==""&&F.val!==" "||$==3&&(F.n||F.val==" "||l==c-1)||$==4&&(F.n||l==c-1))&&(D.s.rn===1&&z.push(U),U+=1);n.a[k].s.totalChars=U;var K=-1,Z;if(D.s.rn===1)for(l=0;l<c;l+=1)F=a[l],K!=F.anIndexes[k]&&(K=F.anIndexes[k],Z=z.splice(Math.floor(Math.random()*z.length),1)[0]),F.anIndexes[k]=Z}e.yOffset=e.finalLineHeight||e.finalSize*1.2,e.ls=e.ls||0,e.ascent=E.ascent*e.finalSize/100},TextProperty.prototype.updateDocumentData=function(e,r){r=r===void 0?this.keysIndex:r;var n=this.copyData({},this.data.d.k[r].s);n=this.copyData(n,e),this.data.d.k[r].s=n,this.recalculate(r),this.elem.addDynamicProperty(this)},TextProperty.prototype.recalculate=function(e){var r=this.data.d.k[e].s;r.__complete=!1,this.keysIndex=0,this._isFirstFrame=!0,this.getValue(r)},TextProperty.prototype.canResizeFont=function(e){this.canResize=e,this.recalculate(this.keysIndex),this.elem.addDynamicProperty(this)},TextProperty.prototype.setMinimumFontSize=function(e){this.minimumFontSize=Math.floor(e)||1,this.recalculate(this.keysIndex),this.elem.addDynamicProperty(this)};var TextSelectorProp=function(){var e=Math.max,r=Math.min,n=Math.floor;function a(c,u){this._currentTextLength=-1,this.k=!1,this.data=u,this.elem=c,this.comp=c.comp,this.finalS=0,this.finalE=0,this.initDynamicPropertyContainer(c),this.s=PropertyFactory.getProp(c,u.s||{k:0},0,0,this),"e"in u?this.e=PropertyFactory.getProp(c,u.e,0,0,this):this.e={v:100},this.o=PropertyFactory.getProp(c,u.o||{k:0},0,0,this),this.xe=PropertyFactory.getProp(c,u.xe||{k:0},0,0,this),this.ne=PropertyFactory.getProp(c,u.ne||{k:0},0,0,this),this.sm=PropertyFactory.getProp(c,u.sm||{k:100},0,0,this),this.a=PropertyFactory.getProp(c,u.a,0,.01,this),this.dynamicProperties.length||this.getValue()}a.prototype={getMult:function(u){this._currentTextLength!==this.elem.textProperty.currentData.l.length&&this.getValue();var d=0,m=0,b=1,A=1;this.ne.v>0?d=this.ne.v/100:m=-this.ne.v/100,this.xe.v>0?b=1-this.xe.v/100:A=1+this.xe.v/100;var _=BezierFactory.getBezierEasing(d,m,b,A).get,T=0,C=this.finalS,S=this.finalE,P=this.data.sh;if(P===2)S===C?T=u>=S?1:0:T=e(0,r(.5/(S-C)+(u-C)/(S-C),1)),T=_(T);else if(P===3)S===C?T=u>=S?0:1:T=1-e(0,r(.5/(S-C)+(u-C)/(S-C),1)),T=_(T);else if(P===4)S===C?T=0:(T=e(0,r(.5/(S-C)+(u-C)/(S-C),1)),T<.5?T*=2:T=1-2*(T-.5)),T=_(T);else if(P===5){if(S===C)T=0;else{var k=S-C;u=r(e(0,u+.5-C),S-C);var x=-k/2+u,E=k/2;T=Math.sqrt(1-x*x/(E*E))}T=_(T)}else P===6?(S===C?T=0:(u=r(e(0,u+.5-C),S-C),T=(1+Math.cos(Math.PI+Math.PI*2*u/(S-C)))/2),T=_(T)):(u>=n(C)&&(u-C<0?T=e(0,r(r(S,1)-(C-u),1)):T=e(0,r(S-u,1))),T=_(T));if(this.sm.v!==100){var g=this.sm.v*.01;g===0&&(g=1e-8);var y=.5-g*.5;T<y?T=0:(T=(T-y)/g,T>1&&(T=1))}return T*this.a.v},getValue:function(u){this.iterateDynamicProperties(),this._mdf=u||this._mdf,this._currentTextLength=this.elem.textProperty.currentData.l.length||0,u&&this.data.r===2&&(this.e.v=this._currentTextLength);var d=this.data.r===2?1:100/this.data.totalChars,m=this.o.v/d,b=this.s.v/d+m,A=this.e.v/d+m;if(b>A){var _=b;b=A,A=_}this.finalS=b,this.finalE=A}},extendPrototype([DynamicPropertyContainer],a);function l(c,u,d){return new a(c,u,d)}return{getTextSelectorProp:l}}();function TextAnimatorDataProperty(e,r,n){var a={propType:!1},l=PropertyFactory.getProp,c=r.a;this.a={r:c.r?l(e,c.r,0,degToRads,n):a,rx:c.rx?l(e,c.rx,0,degToRads,n):a,ry:c.ry?l(e,c.ry,0,degToRads,n):a,sk:c.sk?l(e,c.sk,0,degToRads,n):a,sa:c.sa?l(e,c.sa,0,degToRads,n):a,s:c.s?l(e,c.s,1,.01,n):a,a:c.a?l(e,c.a,1,0,n):a,o:c.o?l(e,c.o,0,.01,n):a,p:c.p?l(e,c.p,1,0,n):a,sw:c.sw?l(e,c.sw,0,0,n):a,sc:c.sc?l(e,c.sc,1,0,n):a,fc:c.fc?l(e,c.fc,1,0,n):a,fh:c.fh?l(e,c.fh,0,0,n):a,fs:c.fs?l(e,c.fs,0,.01,n):a,fb:c.fb?l(e,c.fb,0,.01,n):a,t:c.t?l(e,c.t,0,0,n):a},this.s=TextSelectorProp.getTextSelectorProp(e,r.s,n),this.s.t=r.s.t}function TextAnimatorProperty(e,r,n){this._isFirstFrame=!0,this._hasMaskedPath=!1,this._frameId=-1,this._textData=e,this._renderType=r,this._elem=n,this._animatorsData=createSizedArray(this._textData.a.length),this._pathData={},this._moreOptions={alignment:{}},this.renderedLetters=[],this.lettersChangedFlag=!1,this.initDynamicPropertyContainer(n)}TextAnimatorProperty.prototype.searchProperties=function(){var e,r=this._textData.a.length,n,a=PropertyFactory.getProp;for(e=0;e<r;e+=1)n=this._textData.a[e],this._animatorsData[e]=new TextAnimatorDataProperty(this._elem,n,this);this._textData.p&&"m"in this._textData.p?(this._pathData={a:a(this._elem,this._textData.p.a,0,0,this),f:a(this._elem,this._textData.p.f,0,0,this),l:a(this._elem,this._textData.p.l,0,0,this),r:a(this._elem,this._textData.p.r,0,0,this),p:a(this._elem,this._textData.p.p,0,0,this),m:this._elem.maskManager.getMaskProperty(this._textData.p.m)},this._hasMaskedPath=!0):this._hasMaskedPath=!1,this._moreOptions.alignment=a(this._elem,this._textData.m.a,1,0,this)},TextAnimatorProperty.prototype.getMeasures=function(e,r){if(this.lettersChangedFlag=r,!(!this._mdf&&!this._isFirstFrame&&!r&&(!this._hasMaskedPath||!this._pathData.m._mdf))){this._isFirstFrame=!1;var n=this._moreOptions.alignment.v,a=this._animatorsData,l=this._textData,c=this.mHelper,u=this._renderType,d=this.renderedLetters.length,m,b,A,_,T=e.l,C,S,P,k,x,E,g,y,I,M,j,O,R,B,G;if(this._hasMaskedPath){if(G=this._pathData.m,!this._pathData.n||this._pathData._mdf){var V=G.v;this._pathData.r.v&&(V=V.reverse()),C={tLength:0,segments:[]},_=V._length-1;var Q;for(O=0,A=0;A<_;A+=1)Q=bez.buildBezierData(V.v[A],V.v[A+1],[V.o[A][0]-V.v[A][0],V.o[A][1]-V.v[A][1]],[V.i[A+1][0]-V.v[A+1][0],V.i[A+1][1]-V.v[A+1][1]]),C.tLength+=Q.segmentLength,C.segments.push(Q),O+=Q.segmentLength;A=_,G.v.c&&(Q=bez.buildBezierData(V.v[A],V.v[0],[V.o[A][0]-V.v[A][0],V.o[A][1]-V.v[A][1]],[V.i[0][0]-V.v[0][0],V.i[0][1]-V.v[0][1]]),C.tLength+=Q.segmentLength,C.segments.push(Q),O+=Q.segmentLength),this._pathData.pi=C}if(C=this._pathData.pi,S=this._pathData.f.v,g=0,E=1,k=0,x=!0,M=C.segments,S<0&&G.v.c)for(C.tLength<Math.abs(S)&&(S=-Math.abs(S)%C.tLength),g=M.length-1,I=M[g].points,E=I.length-1;S<0;)S+=I[E].partialLength,E-=1,E<0&&(g-=1,I=M[g].points,E=I.length-1);I=M[g].points,y=I[E-1],P=I[E],j=P.partialLength}_=T.length,m=0,b=0;var W=e.finalSize*1.2*.714,q=!0,L,D,F,$,U;$=a.length;var z,K=-1,Z,Y,le,re=S,ee=g,oe=E,me=-1,ne,ce,fe,X,de,se,te,pe,ge="",Ce=this.defaultPropsArray,he;if(e.j===2||e.j===1){var Ee=0,_e=0,J=e.j===2?-.5:-1,ae=0,ve=!0;for(A=0;A<_;A+=1)if(T[A].n){for(Ee&&(Ee+=_e);ae<A;)T[ae].animatorJustifyOffset=Ee,ae+=1;Ee=0,ve=!0}else{for(F=0;F<$;F+=1)L=a[F].a,L.t.propType&&(ve&&e.j===2&&(_e+=L.t.v*J),D=a[F].s,z=D.getMult(T[A].anIndexes[F],l.a[F].s.totalChars),z.length?Ee+=L.t.v*z[0]*J:Ee+=L.t.v*z*J);ve=!1}for(Ee&&(Ee+=_e);ae<A;)T[ae].animatorJustifyOffset=Ee,ae+=1}for(A=0;A<_;A+=1){if(c.reset(),ne=1,T[A].n)m=0,b+=e.yOffset,b+=q?1:0,S=re,q=!1,this._hasMaskedPath&&(g=ee,E=oe,I=M[g].points,y=I[E-1],P=I[E],j=P.partialLength,k=0),ge="",pe="",se="",he="",Ce=this.defaultPropsArray;else{if(this._hasMaskedPath){if(me!==T[A].line){switch(e.j){case 1:S+=O-e.lineWidths[T[A].line];break;case 2:S+=(O-e.lineWidths[T[A].line])/2;break}me=T[A].line}K!==T[A].ind&&(T[K]&&(S+=T[K].extra),S+=T[A].an/2,K=T[A].ind),S+=n[0]*T[A].an*.005;var ye=0;for(F=0;F<$;F+=1)L=a[F].a,L.p.propType&&(D=a[F].s,z=D.getMult(T[A].anIndexes[F],l.a[F].s.totalChars),z.length?ye+=L.p.v[0]*z[0]:ye+=L.p.v[0]*z),L.a.propType&&(D=a[F].s,z=D.getMult(T[A].anIndexes[F],l.a[F].s.totalChars),z.length?ye+=L.a.v[0]*z[0]:ye+=L.a.v[0]*z);for(x=!0,this._pathData.a.v&&(S=T[0].an*.5+(O-this._pathData.f.v-T[0].an*.5-T[T.length-1].an*.5)*K/(_-1),S+=this._pathData.f.v);x;)k+j>=S+ye||!I?(R=(S+ye-k)/P.partialLength,Y=y.point[0]+(P.point[0]-y.point[0])*R,le=y.point[1]+(P.point[1]-y.point[1])*R,c.translate(-n[0]*T[A].an*.005,-(n[1]*W)*.01),x=!1):I&&(k+=P.partialLength,E+=1,E>=I.length&&(E=0,g+=1,M[g]?I=M[g].points:G.v.c?(E=0,g=0,I=M[g].points):(k-=P.partialLength,I=null)),I&&(y=P,P=I[E],j=P.partialLength));Z=T[A].an/2-T[A].add,c.translate(-Z,0,0)}else Z=T[A].an/2-T[A].add,c.translate(-Z,0,0),c.translate(-n[0]*T[A].an*.005,-n[1]*W*.01,0);for(F=0;F<$;F+=1)L=a[F].a,L.t.propType&&(D=a[F].s,z=D.getMult(T[A].anIndexes[F],l.a[F].s.totalChars),(m!==0||e.j!==0)&&(this._hasMaskedPath?z.length?S+=L.t.v*z[0]:S+=L.t.v*z:z.length?m+=L.t.v*z[0]:m+=L.t.v*z));for(e.strokeWidthAnim&&(fe=e.sw||0),e.strokeColorAnim&&(e.sc?ce=[e.sc[0],e.sc[1],e.sc[2]]:ce=[0,0,0]),e.fillColorAnim&&e.fc&&(X=[e.fc[0],e.fc[1],e.fc[2]]),F=0;F<$;F+=1)L=a[F].a,L.a.propType&&(D=a[F].s,z=D.getMult(T[A].anIndexes[F],l.a[F].s.totalChars),z.length?c.translate(-L.a.v[0]*z[0],-L.a.v[1]*z[1],L.a.v[2]*z[2]):c.translate(-L.a.v[0]*z,-L.a.v[1]*z,L.a.v[2]*z));for(F=0;F<$;F+=1)L=a[F].a,L.s.propType&&(D=a[F].s,z=D.getMult(T[A].anIndexes[F],l.a[F].s.totalChars),z.length?c.scale(1+(L.s.v[0]-1)*z[0],1+(L.s.v[1]-1)*z[1],1):c.scale(1+(L.s.v[0]-1)*z,1+(L.s.v[1]-1)*z,1));for(F=0;F<$;F+=1){if(L=a[F].a,D=a[F].s,z=D.getMult(T[A].anIndexes[F],l.a[F].s.totalChars),L.sk.propType&&(z.length?c.skewFromAxis(-L.sk.v*z[0],L.sa.v*z[1]):c.skewFromAxis(-L.sk.v*z,L.sa.v*z)),L.r.propType&&(z.length?c.rotateZ(-L.r.v*z[2]):c.rotateZ(-L.r.v*z)),L.ry.propType&&(z.length?c.rotateY(L.ry.v*z[1]):c.rotateY(L.ry.v*z)),L.rx.propType&&(z.length?c.rotateX(L.rx.v*z[0]):c.rotateX(L.rx.v*z)),L.o.propType&&(z.length?ne+=(L.o.v*z[0]-ne)*z[0]:ne+=(L.o.v*z-ne)*z),e.strokeWidthAnim&&L.sw.propType&&(z.length?fe+=L.sw.v*z[0]:fe+=L.sw.v*z),e.strokeColorAnim&&L.sc.propType)for(de=0;de<3;de+=1)z.length?ce[de]+=(L.sc.v[de]-ce[de])*z[0]:ce[de]+=(L.sc.v[de]-ce[de])*z;if(e.fillColorAnim&&e.fc){if(L.fc.propType)for(de=0;de<3;de+=1)z.length?X[de]+=(L.fc.v[de]-X[de])*z[0]:X[de]+=(L.fc.v[de]-X[de])*z;L.fh.propType&&(z.length?X=addHueToRGB(X,L.fh.v*z[0]):X=addHueToRGB(X,L.fh.v*z)),L.fs.propType&&(z.length?X=addSaturationToRGB(X,L.fs.v*z[0]):X=addSaturationToRGB(X,L.fs.v*z)),L.fb.propType&&(z.length?X=addBrightnessToRGB(X,L.fb.v*z[0]):X=addBrightnessToRGB(X,L.fb.v*z))}}for(F=0;F<$;F+=1)L=a[F].a,L.p.propType&&(D=a[F].s,z=D.getMult(T[A].anIndexes[F],l.a[F].s.totalChars),this._hasMaskedPath?z.length?c.translate(0,L.p.v[1]*z[0],-L.p.v[2]*z[1]):c.translate(0,L.p.v[1]*z,-L.p.v[2]*z):z.length?c.translate(L.p.v[0]*z[0],L.p.v[1]*z[1],-L.p.v[2]*z[2]):c.translate(L.p.v[0]*z,L.p.v[1]*z,-L.p.v[2]*z));if(e.strokeWidthAnim&&(se=fe<0?0:fe),e.strokeColorAnim&&(te="rgb("+Math.round(ce[0]*255)+","+Math.round(ce[1]*255)+","+Math.round(ce[2]*255)+")"),e.fillColorAnim&&e.fc&&(pe="rgb("+Math.round(X[0]*255)+","+Math.round(X[1]*255)+","+Math.round(X[2]*255)+")"),this._hasMaskedPath){if(c.translate(0,-e.ls),c.translate(0,n[1]*W*.01+b,0),this._pathData.p.v){B=(P.point[1]-y.point[1])/(P.point[0]-y.point[0]);var be=Math.atan(B)*180/Math.PI;P.point[0]<y.point[0]&&(be+=180),c.rotate(-be*Math.PI/180)}c.translate(Y,le,0),S-=n[0]*T[A].an*.005,T[A+1]&&K!==T[A+1].ind&&(S+=T[A].an/2,S+=e.tr*.001*e.finalSize)}else{switch(c.translate(m,b,0),e.ps&&c.translate(e.ps[0],e.ps[1]+e.ascent,0),e.j){case 1:c.translate(T[A].animatorJustifyOffset+e.justifyOffset+(e.boxWidth-e.lineWidths[T[A].line]),0,0);break;case 2:c.translate(T[A].animatorJustifyOffset+e.justifyOffset+(e.boxWidth-e.lineWidths[T[A].line])/2,0,0);break}c.translate(0,-e.ls),c.translate(Z,0,0),c.translate(n[0]*T[A].an*.005,n[1]*W*.01,0),m+=T[A].l+e.tr*.001*e.finalSize}u==="html"?ge=c.toCSS():u==="svg"?ge=c.to2dCSS():Ce=[c.props[0],c.props[1],c.props[2],c.props[3],c.props[4],c.props[5],c.props[6],c.props[7],c.props[8],c.props[9],c.props[10],c.props[11],c.props[12],c.props[13],c.props[14],c.props[15]],he=ne}d<=A?(U=new LetterProps(he,se,te,pe,ge,Ce),this.renderedLetters.push(U),d+=1,this.lettersChangedFlag=!0):(U=this.renderedLetters[A],this.lettersChangedFlag=U.update(he,se,te,pe,ge,Ce)||this.lettersChangedFlag)}}},TextAnimatorProperty.prototype.getValue=function(){this._elem.globalData.frameId!==this._frameId&&(this._frameId=this._elem.globalData.frameId,this.iterateDynamicProperties())},TextAnimatorProperty.prototype.mHelper=new Matrix,TextAnimatorProperty.prototype.defaultPropsArray=[],extendPrototype([DynamicPropertyContainer],TextAnimatorProperty);function ITextElement(){}ITextElement.prototype.initElement=function(e,r,n){this.lettersChangedFlag=!0,this.initFrame(),this.initBaseData(e,r,n),this.textProperty=new TextProperty(this,e.t,this.dynamicProperties),this.textAnimator=new TextAnimatorProperty(e.t,this.renderType,this),this.initTransform(e,r,n),this.initHierarchy(),this.initRenderable(),this.initRendererElement(),this.createContainerElements(),this.createRenderableComponents(),this.createContent(),this.hide(),this.textAnimator.searchProperties(this.dynamicProperties)},ITextElement.prototype.prepareFrame=function(e){this._mdf=!1,this.prepareRenderableFrame(e),this.prepareProperties(e,this.isInRange),(this.textProperty._mdf||this.textProperty._isFirstFrame)&&(this.buildNewText(),this.textProperty._isFirstFrame=!1,this.textProperty._mdf=!1)},ITextElement.prototype.createPathShape=function(e,r){var n,a=r.length,l,c="";for(n=0;n<a;n+=1)r[n].ty==="sh"&&(l=r[n].ks.k,c+=buildShapeString(l,l.i.length,!0,e));return c},ITextElement.prototype.updateDocumentData=function(e,r){this.textProperty.updateDocumentData(e,r)},ITextElement.prototype.canResizeFont=function(e){this.textProperty.canResizeFont(e)},ITextElement.prototype.setMinimumFontSize=function(e){this.textProperty.setMinimumFontSize(e)},ITextElement.prototype.applyTextPropertiesToMatrix=function(e,r,n,a,l){switch(e.ps&&r.translate(e.ps[0],e.ps[1]+e.ascent,0),r.translate(0,-e.ls,0),e.j){case 1:r.translate(e.justifyOffset+(e.boxWidth-e.lineWidths[n]),0,0);break;case 2:r.translate(e.justifyOffset+(e.boxWidth-e.lineWidths[n])/2,0,0);break}r.translate(a,l,0)},ITextElement.prototype.buildColor=function(e){return"rgb("+Math.round(e[0]*255)+","+Math.round(e[1]*255)+","+Math.round(e[2]*255)+")"},ITextElement.prototype.emptyProp=new LetterProps,ITextElement.prototype.destroy=function(){};var emptyShapeData={shapes:[]};function SVGTextLottieElement(e,r,n){this.textSpans=[],this.renderType="svg",this.initElement(e,r,n)}extendPrototype([BaseElement,TransformElement,SVGBaseElement,HierarchyElement,FrameElement,RenderableDOMElement,ITextElement],SVGTextLottieElement),SVGTextLottieElement.prototype.createContent=function(){this.data.singleShape&&!this.globalData.fontManager.chars&&(this.textContainer=createNS("text"))},SVGTextLottieElement.prototype.buildTextContents=function(e){for(var r=0,n=e.length,a=[],l="";r<n;)e[r]===String.fromCharCode(13)||e[r]===String.fromCharCode(3)?(a.push(l),l=""):l+=e[r],r+=1;return a.push(l),a},SVGTextLottieElement.prototype.buildNewText=function(){this.addDynamicProperty(this);var e,r,n=this.textProperty.currentData;this.renderedLetters=createSizedArray(n?n.l.length:0),n.fc?this.layerElement.setAttribute("fill",this.buildColor(n.fc)):this.layerElement.setAttribute("fill","rgba(0,0,0,0)"),n.sc&&(this.layerElement.setAttribute("stroke",this.buildColor(n.sc)),this.layerElement.setAttribute("stroke-width",n.sw)),this.layerElement.setAttribute("font-size",n.finalSize);var a=this.globalData.fontManager.getFontByName(n.f);if(a.fClass)this.layerElement.setAttribute("class",a.fClass);else{this.layerElement.setAttribute("font-family",a.fFamily);var l=n.fWeight,c=n.fStyle;this.layerElement.setAttribute("font-style",c),this.layerElement.setAttribute("font-weight",l)}this.layerElement.setAttribute("aria-label",n.t);var u=n.l||[],d=!!this.globalData.fontManager.chars;r=u.length;var m,b=this.mHelper,A="",_=this.data.singleShape,T=0,C=0,S=!0,P=n.tr*.001*n.finalSize;if(_&&!d&&!n.sz){var k=this.textContainer,x="start";switch(n.j){case 1:x="end";break;case 2:x="middle";break;default:x="start";break}k.setAttribute("text-anchor",x),k.setAttribute("letter-spacing",P);var E=this.buildTextContents(n.finalText);for(r=E.length,C=n.ps?n.ps[1]+n.ascent:0,e=0;e<r;e+=1)m=this.textSpans[e].span||createNS("tspan"),m.textContent=E[e],m.setAttribute("x",0),m.setAttribute("y",C),m.style.display="inherit",k.appendChild(m),this.textSpans[e]||(this.textSpans[e]={span:null,glyph:null}),this.textSpans[e].span=m,C+=n.finalLineHeight;this.layerElement.appendChild(k)}else{var g=this.textSpans.length,y;for(e=0;e<r;e+=1){if(this.textSpans[e]||(this.textSpans[e]={span:null,childSpan:null,glyph:null}),!d||!_||e===0){if(m=g>e?this.textSpans[e].span:createNS(d?"g":"text"),g<=e){if(m.setAttribute("stroke-linecap","butt"),m.setAttribute("stroke-linejoin","round"),m.setAttribute("stroke-miterlimit","4"),this.textSpans[e].span=m,d){var I=createNS("g");m.appendChild(I),this.textSpans[e].childSpan=I}this.textSpans[e].span=m,this.layerElement.appendChild(m)}m.style.display="inherit"}if(b.reset(),b.scale(n.finalSize/100,n.finalSize/100),_&&(u[e].n&&(T=-P,C+=n.yOffset,C+=S?1:0,S=!1),this.applyTextPropertiesToMatrix(n,b,u[e].line,T,C),T+=u[e].l||0,T+=P),d){y=this.globalData.fontManager.getCharData(n.finalText[e],a.fStyle,this.globalData.fontManager.getFontByName(n.f).fFamily);var M;if(y.t===1)M=new SVGCompElement(y.data,this.globalData,this);else{var j=emptyShapeData;y.data&&y.data.shapes&&(j=y.data),M=new SVGShapeElement(j,this.globalData,this)}if(this.textSpans[e].glyph){var O=this.textSpans[e].glyph;this.textSpans[e].childSpan.removeChild(O.layerElement),O.destroy()}this.textSpans[e].glyph=M,M._debug=!0,M.prepareFrame(0),M.renderFrame(),this.textSpans[e].childSpan.appendChild(M.layerElement),this.textSpans[e].childSpan.setAttribute("transform","scale("+n.finalSize/100+","+n.finalSize/100+")")}else _&&m.setAttribute("transform","translate("+b.props[12]+","+b.props[13]+")"),m.textContent=u[e].val,m.setAttributeNS("http://www.w3.org/XML/1998/namespace","xml:space","preserve")}_&&m&&m.setAttribute("d",A)}for(;e<this.textSpans.length;)this.textSpans[e].span.style.display="none",e+=1;this._sizeChanged=!0},SVGTextLottieElement.prototype.sourceRectAtTime=function(){if(this.prepareFrame(this.comp.renderedFrame-this.data.st),this.renderInnerContent(),this._sizeChanged){this._sizeChanged=!1;var e=this.layerElement.getBBox();this.bbox={top:e.y,left:e.x,width:e.width,height:e.height}}return this.bbox},SVGTextLottieElement.prototype.getValue=function(){var e,r=this.textSpans.length,n;for(this.renderedFrame=this.comp.renderedFrame,e=0;e<r;e+=1)n=this.textSpans[e].glyph,n&&(n.prepareFrame(this.comp.renderedFrame-this.data.st),n._mdf&&(this._mdf=!0))},SVGTextLottieElement.prototype.renderInnerContent=function(){if((!this.data.singleShape||this._mdf)&&(this.textAnimator.getMeasures(this.textProperty.currentData,this.lettersChangedFlag),this.lettersChangedFlag||this.textAnimator.lettersChangedFlag)){this._sizeChanged=!0;var e,r,n=this.textAnimator.renderedLetters,a=this.textProperty.currentData.l;r=a.length;var l,c,u;for(e=0;e<r;e+=1)a[e].n||(l=n[e],c=this.textSpans[e].span,u=this.textSpans[e].glyph,u&&u.renderFrame(),l._mdf.m&&c.setAttribute("transform",l.m),l._mdf.o&&c.setAttribute("opacity",l.o),l._mdf.sw&&c.setAttribute("stroke-width",l.sw),l._mdf.sc&&c.setAttribute("stroke",l.sc),l._mdf.fc&&c.setAttribute("fill",l.fc))}};function ISolidElement(e,r,n){this.initElement(e,r,n)}extendPrototype([IImageElement],ISolidElement),ISolidElement.prototype.createContent=function(){var e=createNS("rect");e.setAttribute("width",this.data.sw),e.setAttribute("height",this.data.sh),e.setAttribute("fill",this.data.sc),this.layerElement.appendChild(e)};function NullElement(e,r,n){this.initFrame(),this.initBaseData(e,r,n),this.initFrame(),this.initTransform(e,r,n),this.initHierarchy()}NullElement.prototype.prepareFrame=function(e){this.prepareProperties(e,!0)},NullElement.prototype.renderFrame=function(){},NullElement.prototype.getBaseElement=function(){return null},NullElement.prototype.destroy=function(){},NullElement.prototype.sourceRectAtTime=function(){},NullElement.prototype.hide=function(){},extendPrototype([BaseElement,TransformElement,HierarchyElement,FrameElement],NullElement);function SVGRendererBase(){}extendPrototype([BaseRenderer],SVGRendererBase),SVGRendererBase.prototype.createNull=function(e){return new NullElement(e,this.globalData,this)},SVGRendererBase.prototype.createShape=function(e){return new SVGShapeElement(e,this.globalData,this)},SVGRendererBase.prototype.createText=function(e){return new SVGTextLottieElement(e,this.globalData,this)},SVGRendererBase.prototype.createImage=function(e){return new IImageElement(e,this.globalData,this)},SVGRendererBase.prototype.createSolid=function(e){return new ISolidElement(e,this.globalData,this)},SVGRendererBase.prototype.configAnimation=function(e){this.svgElement.setAttribute("xmlns","http://www.w3.org/2000/svg"),this.renderConfig.viewBoxSize?this.svgElement.setAttribute("viewBox",this.renderConfig.viewBoxSize):this.svgElement.setAttribute("viewBox","0 0 "+e.w+" "+e.h),this.renderConfig.viewBoxOnly||(this.svgElement.setAttribute("width",e.w),this.svgElement.setAttribute("height",e.h),this.svgElement.style.width="100%",this.svgElement.style.height="100%",this.svgElement.style.transform="translate3d(0,0,0)",this.svgElement.style.contentVisibility=this.renderConfig.contentVisibility),this.renderConfig.width&&this.svgElement.setAttribute("width",this.renderConfig.width),this.renderConfig.height&&this.svgElement.setAttribute("height",this.renderConfig.height),this.renderConfig.className&&this.svgElement.setAttribute("class",this.renderConfig.className),this.renderConfig.id&&this.svgElement.setAttribute("id",this.renderConfig.id),this.renderConfig.focusable!==void 0&&this.svgElement.setAttribute("focusable",this.renderConfig.focusable),this.svgElement.setAttribute("preserveAspectRatio",this.renderConfig.preserveAspectRatio),this.animationItem.wrapper.appendChild(this.svgElement);var r=this.globalData.defs;this.setupGlobalData(e,r),this.globalData.progressiveLoad=this.renderConfig.progressiveLoad,this.data=e;var n=createNS("clipPath"),a=createNS("rect");a.setAttribute("width",e.w),a.setAttribute("height",e.h),a.setAttribute("x",0),a.setAttribute("y",0);var l=createElementID();n.setAttribute("id",l),n.appendChild(a),this.layerElement.setAttribute("clip-path","url("+getLocationHref()+"#"+l+")"),r.appendChild(n),this.layers=e.layers,this.elements=createSizedArray(e.layers.length)},SVGRendererBase.prototype.destroy=function(){this.animationItem.wrapper&&(this.animationItem.wrapper.innerText=""),this.layerElement=null,this.globalData.defs=null;var e,r=this.layers?this.layers.length:0;for(e=0;e<r;e+=1)this.elements[e]&&this.elements[e].destroy();this.elements.length=0,this.destroyed=!0,this.animationItem=null},SVGRendererBase.prototype.updateContainerSize=function(){},SVGRendererBase.prototype.buildItem=function(e){var r=this.elements;if(!(r[e]||this.layers[e].ty===99)){r[e]=!0;var n=this.createItem(this.layers[e]);r[e]=n,getExpressionsPlugin()&&(this.layers[e].ty===0&&this.globalData.projectInterface.registerComposition(n),n.initExpressions()),this.appendElementInPos(n,e),this.layers[e].tt&&(!this.elements[e-1]||this.elements[e-1]===!0?(this.buildItem(e-1),this.addPendingElement(n)):n.setMatte(r[e-1].layerId))}},SVGRendererBase.prototype.checkPendingElements=function(){for(;this.pendingElements.length;){var e=this.pendingElements.pop();if(e.checkParenting(),e.data.tt)for(var r=0,n=this.elements.length;r<n;){if(this.elements[r]===e){e.setMatte(this.elements[r-1].layerId);break}r+=1}}},SVGRendererBase.prototype.renderFrame=function(e){if(!(this.renderedFrame===e||this.destroyed)){e===null?e=this.renderedFrame:this.renderedFrame=e,this.globalData.frameNum=e,this.globalData.frameId+=1,this.globalData.projectInterface.currentFrame=e,this.globalData._mdf=!1;var r,n=this.layers.length;for(this.completeLayers||this.checkLayers(e),r=n-1;r>=0;r-=1)(this.completeLayers||this.elements[r])&&this.elements[r].prepareFrame(e-this.layers[r].st);if(this.globalData._mdf)for(r=0;r<n;r+=1)(this.completeLayers||this.elements[r])&&this.elements[r].renderFrame()}},SVGRendererBase.prototype.appendElementInPos=function(e,r){var n=e.getBaseElement();if(!!n){for(var a=0,l;a<r;)this.elements[a]&&this.elements[a]!==!0&&this.elements[a].getBaseElement()&&(l=this.elements[a].getBaseElement()),a+=1;l?this.layerElement.insertBefore(n,l):this.layerElement.appendChild(n)}},SVGRendererBase.prototype.hide=function(){this.layerElement.style.display="none"},SVGRendererBase.prototype.show=function(){this.layerElement.style.display="block"};function ICompElement(){}extendPrototype([BaseElement,TransformElement,HierarchyElement,FrameElement,RenderableDOMElement],ICompElement),ICompElement.prototype.initElement=function(e,r,n){this.initFrame(),this.initBaseData(e,r,n),this.initTransform(e,r,n),this.initRenderable(),this.initHierarchy(),this.initRendererElement(),this.createContainerElements(),this.createRenderableComponents(),(this.data.xt||!r.progressiveLoad)&&this.buildAllItems(),this.hide()},ICompElement.prototype.prepareFrame=function(e){if(this._mdf=!1,this.prepareRenderableFrame(e),this.prepareProperties(e,this.isInRange),!(!this.isInRange&&!this.data.xt)){if(this.tm._placeholder)this.renderedFrame=e/this.data.sr;else{var r=this.tm.v;r===this.data.op&&(r=this.data.op-1),this.renderedFrame=r}var n,a=this.elements.length;for(this.completeLayers||this.checkLayers(this.renderedFrame),n=a-1;n>=0;n-=1)(this.completeLayers||this.elements[n])&&(this.elements[n].prepareFrame(this.renderedFrame-this.layers[n].st),this.elements[n]._mdf&&(this._mdf=!0))}},ICompElement.prototype.renderInnerContent=function(){var e,r=this.layers.length;for(e=0;e<r;e+=1)(this.completeLayers||this.elements[e])&&this.elements[e].renderFrame()},ICompElement.prototype.setElements=function(e){this.elements=e},ICompElement.prototype.getElements=function(){return this.elements},ICompElement.prototype.destroyElements=function(){var e,r=this.layers.length;for(e=0;e<r;e+=1)this.elements[e]&&this.elements[e].destroy()},ICompElement.prototype.destroy=function(){this.destroyElements(),this.destroyBaseElement()};function SVGCompElement(e,r,n){this.layers=e.layers,this.supports3d=!0,this.completeLayers=!1,this.pendingElements=[],this.elements=this.layers?createSizedArray(this.layers.length):[],this.initElement(e,r,n),this.tm=e.tm?PropertyFactory.getProp(this,e.tm,0,r.frameRate,this):{_placeholder:!0}}extendPrototype([SVGRendererBase,ICompElement,SVGBaseElement],SVGCompElement),SVGCompElement.prototype.createComp=function(e){return new SVGCompElement(e,this.globalData,this)};function SVGRenderer(e,r){this.animationItem=e,this.layers=null,this.renderedFrame=-1,this.svgElement=createNS("svg");var n="";if(r&&r.title){var a=createNS("title"),l=createElementID();a.setAttribute("id",l),a.textContent=r.title,this.svgElement.appendChild(a),n+=l}if(r&&r.description){var c=createNS("desc"),u=createElementID();c.setAttribute("id",u),c.textContent=r.description,this.svgElement.appendChild(c),n+=" "+u}n&&this.svgElement.setAttribute("aria-labelledby",n);var d=createNS("defs");this.svgElement.appendChild(d);var m=createNS("g");this.svgElement.appendChild(m),this.layerElement=m,this.renderConfig={preserveAspectRatio:r&&r.preserveAspectRatio||"xMidYMid meet",imagePreserveAspectRatio:r&&r.imagePreserveAspectRatio||"xMidYMid slice",contentVisibility:r&&r.contentVisibility||"visible",progressiveLoad:r&&r.progressiveLoad||!1,hideOnTransparent:!(r&&r.hideOnTransparent===!1),viewBoxOnly:r&&r.viewBoxOnly||!1,viewBoxSize:r&&r.viewBoxSize||!1,className:r&&r.className||"",id:r&&r.id||"",focusable:r&&r.focusable,filterSize:{width:r&&r.filterSize&&r.filterSize.width||"100%",height:r&&r.filterSize&&r.filterSize.height||"100%",x:r&&r.filterSize&&r.filterSize.x||"0%",y:r&&r.filterSize&&r.filterSize.y||"0%"},width:r&&r.width,height:r&&r.height},this.globalData={_mdf:!1,frameNum:-1,defs:d,renderConfig:this.renderConfig},this.elements=[],this.pendingElements=[],this.destroyed=!1,this.rendererType="svg"}extendPrototype([SVGRendererBase],SVGRenderer),SVGRenderer.prototype.createComp=function(e){return new SVGCompElement(e,this.globalData,this)};function CVContextData(){this.saved=[],this.cArrPos=0,this.cTr=new Matrix,this.cO=1;var e,r=15;for(this.savedOp=createTypedArray("float32",r),e=0;e<r;e+=1)this.saved[e]=createTypedArray("float32",16);this._length=r}CVContextData.prototype.duplicate=function(){var e=this._length*2,r=this.savedOp;this.savedOp=createTypedArray("float32",e),this.savedOp.set(r);var n=0;for(n=this._length;n<e;n+=1)this.saved[n]=createTypedArray("float32",16);this._length=e},CVContextData.prototype.reset=function(){this.cArrPos=0,this.cTr.reset(),this.cO=1};function ShapeTransformManager(){this.sequences={},this.sequenceList=[],this.transform_key_count=0}ShapeTransformManager.prototype={addTransformSequence:function(r){var n,a=r.length,l="_";for(n=0;n<a;n+=1)l+=r[n].transform.key+"_";var c=this.sequences[l];return c||(c={transforms:[].concat(r),finalTransform:new Matrix,_mdf:!1},this.sequences[l]=c,this.sequenceList.push(c)),c},processSequence:function(r,n){for(var a=0,l=r.transforms.length,c=n;a<l&&!n;){if(r.transforms[a].transform.mProps._mdf){c=!0;break}a+=1}if(c){var u;for(r.finalTransform.reset(),a=l-1;a>=0;a-=1)u=r.transforms[a].transform.mProps.v.props,r.finalTransform.transform(u[0],u[1],u[2],u[3],u[4],u[5],u[6],u[7],u[8],u[9],u[10],u[11],u[12],u[13],u[14],u[15])}r._mdf=c},processSequences:function(r){var n,a=this.sequenceList.length;for(n=0;n<a;n+=1)this.processSequence(this.sequenceList[n],r)},getNewKey:function(){return this.transform_key_count+=1,"_"+this.transform_key_count}};function CVEffects(){}CVEffects.prototype.renderFrame=function(){};function CVMaskElement(e,r){this.data=e,this.element=r,this.masksProperties=this.data.masksProperties||[],this.viewData=createSizedArray(this.masksProperties.length);var n,a=this.masksProperties.length,l=!1;for(n=0;n<a;n+=1)this.masksProperties[n].mode!=="n"&&(l=!0),this.viewData[n]=ShapePropertyFactory.getShapeProp(this.element,this.masksProperties[n],3);this.hasMasks=l,l&&this.element.addRenderableComponent(this)}CVMaskElement.prototype.renderFrame=function(){if(!!this.hasMasks){var e=this.element.finalTransform.mat,r=this.element.canvasContext,n,a=this.masksProperties.length,l,c,u;for(r.beginPath(),n=0;n<a;n+=1)if(this.masksProperties[n].mode!=="n"){this.masksProperties[n].inv&&(r.moveTo(0,0),r.lineTo(this.element.globalData.compSize.w,0),r.lineTo(this.element.globalData.compSize.w,this.element.globalData.compSize.h),r.lineTo(0,this.element.globalData.compSize.h),r.lineTo(0,0)),u=this.viewData[n].v,l=e.applyToPointArray(u.v[0][0],u.v[0][1],0),r.moveTo(l[0],l[1]);var d,m=u._length;for(d=1;d<m;d+=1)c=e.applyToTriplePoints(u.o[d-1],u.i[d],u.v[d]),r.bezierCurveTo(c[0],c[1],c[2],c[3],c[4],c[5]);c=e.applyToTriplePoints(u.o[d-1],u.i[0],u.v[0]),r.bezierCurveTo(c[0],c[1],c[2],c[3],c[4],c[5])}this.element.globalData.renderer.save(!0),r.clip()}},CVMaskElement.prototype.getMaskProperty=MaskElement.prototype.getMaskProperty,CVMaskElement.prototype.destroy=function(){this.element=null};function CVBaseElement(){}CVBaseElement.prototype={createElements:function(){},initRendererElement:function(){},createContainerElements:function(){this.canvasContext=this.globalData.canvasContext,this.renderableEffectsManager=new CVEffects(this)},createContent:function(){},setBlendMode:function(){var r=this.globalData;if(r.blendMode!==this.data.bm){r.blendMode=this.data.bm;var n=getBlendMode(this.data.bm);r.canvasContext.globalCompositeOperation=n}},createRenderableComponents:function(){this.maskManager=new CVMaskElement(this.data,this)},hideElement:function(){!this.hidden&&(!this.isInRange||this.isTransparent)&&(this.hidden=!0)},showElement:function(){this.isInRange&&!this.isTransparent&&(this.hidden=!1,this._isFirstFrame=!0,this.maskManager._isFirstFrame=!0)},renderFrame:function(){if(!(this.hidden||this.data.hd)){this.renderTransform(),this.renderRenderable(),this.setBlendMode();var r=this.data.ty===0;this.globalData.renderer.save(r),this.globalData.renderer.ctxTransform(this.finalTransform.mat.props),this.globalData.renderer.ctxOpacity(this.finalTransform.mProp.o.v),this.renderInnerContent(),this.globalData.renderer.restore(r),this.maskManager.hasMasks&&this.globalData.renderer.restore(!0),this._isFirstFrame&&(this._isFirstFrame=!1)}},destroy:function(){this.canvasContext=null,this.data=null,this.globalData=null,this.maskManager.destroy()},mHelper:new Matrix},CVBaseElement.prototype.hide=CVBaseElement.prototype.hideElement,CVBaseElement.prototype.show=CVBaseElement.prototype.showElement;function CVShapeData(e,r,n,a){this.styledShapes=[],this.tr=[0,0,0,0,0,0];var l=4;r.ty==="rc"?l=5:r.ty==="el"?l=6:r.ty==="sr"&&(l=7),this.sh=ShapePropertyFactory.getShapeProp(e,r,l,e);var c,u=n.length,d;for(c=0;c<u;c+=1)n[c].closed||(d={transforms:a.addTransformSequence(n[c].transforms),trNodes:[]},this.styledShapes.push(d),n[c].elements.push(d))}CVShapeData.prototype.setAsAnimated=SVGShapeData.prototype.setAsAnimated;function CVShapeElement(e,r,n){this.shapes=[],this.shapesData=e.shapes,this.stylesList=[],this.itemsData=[],this.prevViewData=[],this.shapeModifiers=[],this.processedElements=[],this.transformsManager=new ShapeTransformManager,this.initElement(e,r,n)}extendPrototype([BaseElement,TransformElement,CVBaseElement,IShapeElement,HierarchyElement,FrameElement,RenderableElement],CVShapeElement),CVShapeElement.prototype.initElement=RenderableDOMElement.prototype.initElement,CVShapeElement.prototype.transformHelper={opacity:1,_opMdf:!1},CVShapeElement.prototype.dashResetter=[],CVShapeElement.prototype.createContent=function(){this.searchShapes(this.shapesData,this.itemsData,this.prevViewData,!0,[])},CVShapeElement.prototype.createStyleElement=function(e,r){var n={data:e,type:e.ty,preTransforms:this.transformsManager.addTransformSequence(r),transforms:[],elements:[],closed:e.hd===!0},a={};if(e.ty==="fl"||e.ty==="st"?(a.c=PropertyFactory.getProp(this,e.c,1,255,this),a.c.k||(n.co="rgb("+bmFloor(a.c.v[0])+","+bmFloor(a.c.v[1])+","+bmFloor(a.c.v[2])+")")):(e.ty==="gf"||e.ty==="gs")&&(a.s=PropertyFactory.getProp(this,e.s,1,null,this),a.e=PropertyFactory.getProp(this,e.e,1,null,this),a.h=PropertyFactory.getProp(this,e.h||{k:0},0,.01,this),a.a=PropertyFactory.getProp(this,e.a||{k:0},0,degToRads,this),a.g=new GradientProperty(this,e.g,this)),a.o=PropertyFactory.getProp(this,e.o,0,.01,this),e.ty==="st"||e.ty==="gs"){if(n.lc=lineCapEnum[e.lc||2],n.lj=lineJoinEnum[e.lj||2],e.lj==1&&(n.ml=e.ml),a.w=PropertyFactory.getProp(this,e.w,0,null,this),a.w.k||(n.wi=a.w.v),e.d){var l=new DashProperty(this,e.d,"canvas",this);a.d=l,a.d.k||(n.da=a.d.dashArray,n.do=a.d.dashoffset[0])}}else n.r=e.r===2?"evenodd":"nonzero";return this.stylesList.push(n),a.style=n,a},CVShapeElement.prototype.createGroupElement=function(){var e={it:[],prevViewData:[]};return e},CVShapeElement.prototype.createTransformElement=function(e){var r={transform:{opacity:1,_opMdf:!1,key:this.transformsManager.getNewKey(),op:PropertyFactory.getProp(this,e.o,0,.01,this),mProps:TransformPropertyFactory.getTransformProperty(this,e,this)}};return r},CVShapeElement.prototype.createShapeElement=function(e){var r=new CVShapeData(this,e,this.stylesList,this.transformsManager);return this.shapes.push(r),this.addShapeToModifiers(r),r},CVShapeElement.prototype.reloadShapes=function(){this._isFirstFrame=!0;var e,r=this.itemsData.length;for(e=0;e<r;e+=1)this.prevViewData[e]=this.itemsData[e];for(this.searchShapes(this.shapesData,this.itemsData,this.prevViewData,!0,[]),r=this.dynamicProperties.length,e=0;e<r;e+=1)this.dynamicProperties[e].getValue();this.renderModifiers(),this.transformsManager.processSequences(this._isFirstFrame)},CVShapeElement.prototype.addTransformToStyleList=function(e){var r,n=this.stylesList.length;for(r=0;r<n;r+=1)this.stylesList[r].closed||this.stylesList[r].transforms.push(e)},CVShapeElement.prototype.removeTransformFromStyleList=function(){var e,r=this.stylesList.length;for(e=0;e<r;e+=1)this.stylesList[e].closed||this.stylesList[e].transforms.pop()},CVShapeElement.prototype.closeStyles=function(e){var r,n=e.length;for(r=0;r<n;r+=1)e[r].closed=!0},CVShapeElement.prototype.searchShapes=function(e,r,n,a,l){var c,u=e.length-1,d,m,b=[],A=[],_,T,C,S=[].concat(l);for(c=u;c>=0;c-=1){if(_=this.searchProcessedElement(e[c]),_?r[c]=n[_-1]:e[c]._shouldRender=a,e[c].ty==="fl"||e[c].ty==="st"||e[c].ty==="gf"||e[c].ty==="gs")_?r[c].style.closed=!1:r[c]=this.createStyleElement(e[c],S),b.push(r[c].style);else if(e[c].ty==="gr"){if(!_)r[c]=this.createGroupElement(e[c]);else for(m=r[c].it.length,d=0;d<m;d+=1)r[c].prevViewData[d]=r[c].it[d];this.searchShapes(e[c].it,r[c].it,r[c].prevViewData,a,S)}else e[c].ty==="tr"?(_||(C=this.createTransformElement(e[c]),r[c]=C),S.push(r[c]),this.addTransformToStyleList(r[c])):e[c].ty==="sh"||e[c].ty==="rc"||e[c].ty==="el"||e[c].ty==="sr"?_||(r[c]=this.createShapeElement(e[c])):e[c].ty==="tm"||e[c].ty==="rd"||e[c].ty==="pb"?(_?(T=r[c],T.closed=!1):(T=ShapeModifiers.getModifier(e[c].ty),T.init(this,e[c]),r[c]=T,this.shapeModifiers.push(T)),A.push(T)):e[c].ty==="rp"&&(_?(T=r[c],T.closed=!0):(T=ShapeModifiers.getModifier(e[c].ty),r[c]=T,T.init(this,e,c,r),this.shapeModifiers.push(T),a=!1),A.push(T));this.addProcessedElement(e[c],c+1)}for(this.removeTransformFromStyleList(),this.closeStyles(b),u=A.length,c=0;c<u;c+=1)A[c].closed=!0},CVShapeElement.prototype.renderInnerContent=function(){this.transformHelper.opacity=1,this.transformHelper._opMdf=!1,this.renderModifiers(),this.transformsManager.processSequences(this._isFirstFrame),this.renderShape(this.transformHelper,this.shapesData,this.itemsData,!0)},CVShapeElement.prototype.renderShapeTransform=function(e,r){(e._opMdf||r.op._mdf||this._isFirstFrame)&&(r.opacity=e.opacity,r.opacity*=r.op.v,r._opMdf=!0)},CVShapeElement.prototype.drawLayer=function(){var e,r=this.stylesList.length,n,a,l,c,u,d,m=this.globalData.renderer,b=this.globalData.canvasContext,A,_;for(e=0;e<r;e+=1)if(_=this.stylesList[e],A=_.type,!((A==="st"||A==="gs")&&_.wi===0||!_.data._shouldRender||_.coOp===0||this.globalData.currentGlobalAlpha===0)){for(m.save(),u=_.elements,A==="st"||A==="gs"?(b.strokeStyle=A==="st"?_.co:_.grd,b.lineWidth=_.wi,b.lineCap=_.lc,b.lineJoin=_.lj,b.miterLimit=_.ml||0):b.fillStyle=A==="fl"?_.co:_.grd,m.ctxOpacity(_.coOp),A!=="st"&&A!=="gs"&&b.beginPath(),m.ctxTransform(_.preTransforms.finalTransform.props),a=u.length,n=0;n<a;n+=1){for((A==="st"||A==="gs")&&(b.beginPath(),_.da&&(b.setLineDash(_.da),b.lineDashOffset=_.do)),d=u[n].trNodes,c=d.length,l=0;l<c;l+=1)d[l].t==="m"?b.moveTo(d[l].p[0],d[l].p[1]):d[l].t==="c"?b.bezierCurveTo(d[l].pts[0],d[l].pts[1],d[l].pts[2],d[l].pts[3],d[l].pts[4],d[l].pts[5]):b.closePath();(A==="st"||A==="gs")&&(b.stroke(),_.da&&b.setLineDash(this.dashResetter))}A!=="st"&&A!=="gs"&&b.fill(_.r),m.restore()}},CVShapeElement.prototype.renderShape=function(e,r,n,a){var l,c=r.length-1,u;for(u=e,l=c;l>=0;l-=1)r[l].ty==="tr"?(u=n[l].transform,this.renderShapeTransform(e,u)):r[l].ty==="sh"||r[l].ty==="el"||r[l].ty==="rc"||r[l].ty==="sr"?this.renderPath(r[l],n[l]):r[l].ty==="fl"?this.renderFill(r[l],n[l],u):r[l].ty==="st"?this.renderStroke(r[l],n[l],u):r[l].ty==="gf"||r[l].ty==="gs"?this.renderGradientFill(r[l],n[l],u):r[l].ty==="gr"?this.renderShape(u,r[l].it,n[l].it):r[l].ty==="tm";a&&this.drawLayer()},CVShapeElement.prototype.renderStyledShape=function(e,r){if(this._isFirstFrame||r._mdf||e.transforms._mdf){var n=e.trNodes,a=r.paths,l,c,u,d=a._length;n.length=0;var m=e.transforms.finalTransform;for(u=0;u<d;u+=1){var b=a.shapes[u];if(b&&b.v){for(c=b._length,l=1;l<c;l+=1)l===1&&n.push({t:"m",p:m.applyToPointArray(b.v[0][0],b.v[0][1],0)}),n.push({t:"c",pts:m.applyToTriplePoints(b.o[l-1],b.i[l],b.v[l])});c===1&&n.push({t:"m",p:m.applyToPointArray(b.v[0][0],b.v[0][1],0)}),b.c&&c&&(n.push({t:"c",pts:m.applyToTriplePoints(b.o[l-1],b.i[0],b.v[0])}),n.push({t:"z"}))}}e.trNodes=n}},CVShapeElement.prototype.renderPath=function(e,r){if(e.hd!==!0&&e._shouldRender){var n,a=r.styledShapes.length;for(n=0;n<a;n+=1)this.renderStyledShape(r.styledShapes[n],r.sh)}},CVShapeElement.prototype.renderFill=function(e,r,n){var a=r.style;(r.c._mdf||this._isFirstFrame)&&(a.co="rgb("+bmFloor(r.c.v[0])+","+bmFloor(r.c.v[1])+","+bmFloor(r.c.v[2])+")"),(r.o._mdf||n._opMdf||this._isFirstFrame)&&(a.coOp=r.o.v*n.opacity)},CVShapeElement.prototype.renderGradientFill=function(e,r,n){var a=r.style,l;if(!a.grd||r.g._mdf||r.s._mdf||r.e._mdf||e.t!==1&&(r.h._mdf||r.a._mdf)){var c=this.globalData.canvasContext,u=r.s.v,d=r.e.v;if(e.t===1)l=c.createLinearGradient(u[0],u[1],d[0],d[1]);else{var m=Math.sqrt(Math.pow(u[0]-d[0],2)+Math.pow(u[1]-d[1],2)),b=Math.atan2(d[1]-u[1],d[0]-u[0]),A=r.h.v;A>=1?A=.99:A<=-1&&(A=-.99);var _=m*A,T=Math.cos(b+r.a.v)*_+u[0],C=Math.sin(b+r.a.v)*_+u[1];l=c.createRadialGradient(T,C,0,u[0],u[1],m)}var S,P=e.g.p,k=r.g.c,x=1;for(S=0;S<P;S+=1)r.g._hasOpacity&&r.g._collapsable&&(x=r.g.o[S*2+1]),l.addColorStop(k[S*4]/100,"rgba("+k[S*4+1]+","+k[S*4+2]+","+k[S*4+3]+","+x+")");a.grd=l}a.coOp=r.o.v*n.opacity},CVShapeElement.prototype.renderStroke=function(e,r,n){var a=r.style,l=r.d;l&&(l._mdf||this._isFirstFrame)&&(a.da=l.dashArray,a.do=l.dashoffset[0]),(r.c._mdf||this._isFirstFrame)&&(a.co="rgb("+bmFloor(r.c.v[0])+","+bmFloor(r.c.v[1])+","+bmFloor(r.c.v[2])+")"),(r.o._mdf||n._opMdf||this._isFirstFrame)&&(a.coOp=r.o.v*n.opacity),(r.w._mdf||this._isFirstFrame)&&(a.wi=r.w.v)},CVShapeElement.prototype.destroy=function(){this.shapesData=null,this.globalData=null,this.canvasContext=null,this.stylesList.length=0,this.itemsData.length=0};function CVTextElement(e,r,n){this.textSpans=[],this.yOffset=0,this.fillColorAnim=!1,this.strokeColorAnim=!1,this.strokeWidthAnim=!1,this.stroke=!1,this.fill=!1,this.justifyOffset=0,this.currentRender=null,this.renderType="canvas",this.values={fill:"rgba(0,0,0,0)",stroke:"rgba(0,0,0,0)",sWidth:0,fValue:""},this.initElement(e,r,n)}extendPrototype([BaseElement,TransformElement,CVBaseElement,HierarchyElement,FrameElement,RenderableElement,ITextElement],CVTextElement),CVTextElement.prototype.tHelper=createTag("canvas").getContext("2d"),CVTextElement.prototype.buildNewText=function(){var e=this.textProperty.currentData;this.renderedLetters=createSizedArray(e.l?e.l.length:0);var r=!1;e.fc?(r=!0,this.values.fill=this.buildColor(e.fc)):this.values.fill="rgba(0,0,0,0)",this.fill=r;var n=!1;e.sc&&(n=!0,this.values.stroke=this.buildColor(e.sc),this.values.sWidth=e.sw);var a=this.globalData.fontManager.getFontByName(e.f),l,c,u=e.l,d=this.mHelper;this.stroke=n,this.values.fValue=e.finalSize+"px "+this.globalData.fontManager.getFontByName(e.f).fFamily,c=e.finalText.length;var m,b,A,_,T,C,S,P,k,x,E=this.data.singleShape,g=e.tr*.001*e.finalSize,y=0,I=0,M=!0,j=0;for(l=0;l<c;l+=1){m=this.globalData.fontManager.getCharData(e.finalText[l],a.fStyle,this.globalData.fontManager.getFontByName(e.f).fFamily),b=m&&m.data||{},d.reset(),E&&u[l].n&&(y=-g,I+=e.yOffset,I+=M?1:0,M=!1),T=b.shapes?b.shapes[0].it:[],S=T.length,d.scale(e.finalSize/100,e.finalSize/100),E&&this.applyTextPropertiesToMatrix(e,d,u[l].line,y,I),k=createSizedArray(S-1);var O=0;for(C=0;C<S;C+=1)if(T[C].ty==="sh"){for(_=T[C].ks.k.i.length,P=T[C].ks.k,x=[],A=1;A<_;A+=1)A===1&&x.push(d.applyToX(P.v[0][0],P.v[0][1],0),d.applyToY(P.v[0][0],P.v[0][1],0)),x.push(d.applyToX(P.o[A-1][0],P.o[A-1][1],0),d.applyToY(P.o[A-1][0],P.o[A-1][1],0),d.applyToX(P.i[A][0],P.i[A][1],0),d.applyToY(P.i[A][0],P.i[A][1],0),d.applyToX(P.v[A][0],P.v[A][1],0),d.applyToY(P.v[A][0],P.v[A][1],0));x.push(d.applyToX(P.o[A-1][0],P.o[A-1][1],0),d.applyToY(P.o[A-1][0],P.o[A-1][1],0),d.applyToX(P.i[0][0],P.i[0][1],0),d.applyToY(P.i[0][0],P.i[0][1],0),d.applyToX(P.v[0][0],P.v[0][1],0),d.applyToY(P.v[0][0],P.v[0][1],0)),k[O]=x,O+=1}E&&(y+=u[l].l,y+=g),this.textSpans[j]?this.textSpans[j].elem=k:this.textSpans[j]={elem:k},j+=1}},CVTextElement.prototype.renderInnerContent=function(){var e=this.canvasContext;e.font=this.values.fValue,e.lineCap="butt",e.lineJoin="miter",e.miterLimit=4,this.data.singleShape||this.textAnimator.getMeasures(this.textProperty.currentData,this.lettersChangedFlag);var r,n,a,l,c,u,d=this.textAnimator.renderedLetters,m=this.textProperty.currentData.l;n=m.length;var b,A=null,_=null,T=null,C,S;for(r=0;r<n;r+=1)if(!m[r].n){if(b=d[r],b&&(this.globalData.renderer.save(),this.globalData.renderer.ctxTransform(b.p),this.globalData.renderer.ctxOpacity(b.o)),this.fill){for(b&&b.fc?A!==b.fc&&(A=b.fc,e.fillStyle=b.fc):A!==this.values.fill&&(A=this.values.fill,e.fillStyle=this.values.fill),C=this.textSpans[r].elem,l=C.length,this.globalData.canvasContext.beginPath(),a=0;a<l;a+=1)for(S=C[a],u=S.length,this.globalData.canvasContext.moveTo(S[0],S[1]),c=2;c<u;c+=6)this.globalData.canvasContext.bezierCurveTo(S[c],S[c+1],S[c+2],S[c+3],S[c+4],S[c+5]);this.globalData.canvasContext.closePath(),this.globalData.canvasContext.fill()}if(this.stroke){for(b&&b.sw?T!==b.sw&&(T=b.sw,e.lineWidth=b.sw):T!==this.values.sWidth&&(T=this.values.sWidth,e.lineWidth=this.values.sWidth),b&&b.sc?_!==b.sc&&(_=b.sc,e.strokeStyle=b.sc):_!==this.values.stroke&&(_=this.values.stroke,e.strokeStyle=this.values.stroke),C=this.textSpans[r].elem,l=C.length,this.globalData.canvasContext.beginPath(),a=0;a<l;a+=1)for(S=C[a],u=S.length,this.globalData.canvasContext.moveTo(S[0],S[1]),c=2;c<u;c+=6)this.globalData.canvasContext.bezierCurveTo(S[c],S[c+1],S[c+2],S[c+3],S[c+4],S[c+5]);this.globalData.canvasContext.closePath(),this.globalData.canvasContext.stroke()}b&&this.globalData.renderer.restore()}};function CVImageElement(e,r,n){this.assetData=r.getAssetData(e.refId),this.img=r.imageLoader.getAsset(this.assetData),this.initElement(e,r,n)}extendPrototype([BaseElement,TransformElement,CVBaseElement,HierarchyElement,FrameElement,RenderableElement],CVImageElement),CVImageElement.prototype.initElement=SVGShapeElement.prototype.initElement,CVImageElement.prototype.prepareFrame=IImageElement.prototype.prepareFrame,CVImageElement.prototype.createContent=function(){if(this.img.width&&(this.assetData.w!==this.img.width||this.assetData.h!==this.img.height)){var e=createTag("canvas");e.width=this.assetData.w,e.height=this.assetData.h;var r=e.getContext("2d"),n=this.img.width,a=this.img.height,l=n/a,c=this.assetData.w/this.assetData.h,u,d,m=this.assetData.pr||this.globalData.renderConfig.imagePreserveAspectRatio;l>c&&m==="xMidYMid slice"||l<c&&m!=="xMidYMid slice"?(d=a,u=d*c):(u=n,d=u/c),r.drawImage(this.img,(n-u)/2,(a-d)/2,u,d,0,0,this.assetData.w,this.assetData.h),this.img=e}},CVImageElement.prototype.renderInnerContent=function(){this.canvasContext.drawImage(this.img,0,0)},CVImageElement.prototype.destroy=function(){this.img=null};function CVSolidElement(e,r,n){this.initElement(e,r,n)}extendPrototype([BaseElement,TransformElement,CVBaseElement,HierarchyElement,FrameElement,RenderableElement],CVSolidElement),CVSolidElement.prototype.initElement=SVGShapeElement.prototype.initElement,CVSolidElement.prototype.prepareFrame=IImageElement.prototype.prepareFrame,CVSolidElement.prototype.renderInnerContent=function(){var e=this.canvasContext;e.fillStyle=this.data.sc,e.fillRect(0,0,this.data.sw,this.data.sh)};function CanvasRendererBase(e,r){this.animationItem=e,this.renderConfig={clearCanvas:r&&r.clearCanvas!==void 0?r.clearCanvas:!0,context:r&&r.context||null,progressiveLoad:r&&r.progressiveLoad||!1,preserveAspectRatio:r&&r.preserveAspectRatio||"xMidYMid meet",imagePreserveAspectRatio:r&&r.imagePreserveAspectRatio||"xMidYMid slice",contentVisibility:r&&r.contentVisibility||"visible",className:r&&r.className||"",id:r&&r.id||""},this.renderConfig.dpr=r&&r.dpr||1,this.animationItem.wrapper&&(this.renderConfig.dpr=r&&r.dpr||window.devicePixelRatio||1),this.renderedFrame=-1,this.globalData={frameNum:-1,_mdf:!1,renderConfig:this.renderConfig,currentGlobalAlpha:-1},this.contextData=new CVContextData,this.elements=[],this.pendingElements=[],this.transformMat=new Matrix,this.completeLayers=!1,this.rendererType="canvas"}extendPrototype([BaseRenderer],CanvasRendererBase),CanvasRendererBase.prototype.createShape=function(e){return new CVShapeElement(e,this.globalData,this)},CanvasRendererBase.prototype.createText=function(e){return new CVTextElement(e,this.globalData,this)},CanvasRendererBase.prototype.createImage=function(e){return new CVImageElement(e,this.globalData,this)},CanvasRendererBase.prototype.createSolid=function(e){return new CVSolidElement(e,this.globalData,this)},CanvasRendererBase.prototype.createNull=SVGRenderer.prototype.createNull,CanvasRendererBase.prototype.ctxTransform=function(e){if(!(e[0]===1&&e[1]===0&&e[4]===0&&e[5]===1&&e[12]===0&&e[13]===0)){if(!this.renderConfig.clearCanvas){this.canvasContext.transform(e[0],e[1],e[4],e[5],e[12],e[13]);return}this.transformMat.cloneFromProps(e);var r=this.contextData.cTr.props;this.transformMat.transform(r[0],r[1],r[2],r[3],r[4],r[5],r[6],r[7],r[8],r[9],r[10],r[11],r[12],r[13],r[14],r[15]),this.contextData.cTr.cloneFromProps(this.transformMat.props);var n=this.contextData.cTr.props;this.canvasContext.setTransform(n[0],n[1],n[4],n[5],n[12],n[13])}},CanvasRendererBase.prototype.ctxOpacity=function(e){if(!this.renderConfig.clearCanvas){this.canvasContext.globalAlpha*=e<0?0:e,this.globalData.currentGlobalAlpha=this.contextData.cO;return}this.contextData.cO*=e<0?0:e,this.globalData.currentGlobalAlpha!==this.contextData.cO&&(this.canvasContext.globalAlpha=this.contextData.cO,this.globalData.currentGlobalAlpha=this.contextData.cO)},CanvasRendererBase.prototype.reset=function(){if(!this.renderConfig.clearCanvas){this.canvasContext.restore();return}this.contextData.reset()},CanvasRendererBase.prototype.save=function(e){if(!this.renderConfig.clearCanvas){this.canvasContext.save();return}e&&this.canvasContext.save();var r=this.contextData.cTr.props;this.contextData._length<=this.contextData.cArrPos&&this.contextData.duplicate();var n,a=this.contextData.saved[this.contextData.cArrPos];for(n=0;n<16;n+=1)a[n]=r[n];this.contextData.savedOp[this.contextData.cArrPos]=this.contextData.cO,this.contextData.cArrPos+=1},CanvasRendererBase.prototype.restore=function(e){if(!this.renderConfig.clearCanvas){this.canvasContext.restore();return}e&&(this.canvasContext.restore(),this.globalData.blendMode="source-over"),this.contextData.cArrPos-=1;var r=this.contextData.saved[this.contextData.cArrPos],n,a=this.contextData.cTr.props;for(n=0;n<16;n+=1)a[n]=r[n];this.canvasContext.setTransform(r[0],r[1],r[4],r[5],r[12],r[13]),r=this.contextData.savedOp[this.contextData.cArrPos],this.contextData.cO=r,this.globalData.currentGlobalAlpha!==r&&(this.canvasContext.globalAlpha=r,this.globalData.currentGlobalAlpha=r)},CanvasRendererBase.prototype.configAnimation=function(e){if(this.animationItem.wrapper){this.animationItem.container=createTag("canvas");var r=this.animationItem.container.style;r.width="100%",r.height="100%";var n="0px 0px 0px";r.transformOrigin=n,r.mozTransformOrigin=n,r.webkitTransformOrigin=n,r["-webkit-transform"]=n,r.contentVisibility=this.renderConfig.contentVisibility,this.animationItem.wrapper.appendChild(this.animationItem.container),this.canvasContext=this.animationItem.container.getContext("2d"),this.renderConfig.className&&this.animationItem.container.setAttribute("class",this.renderConfig.className),this.renderConfig.id&&this.animationItem.container.setAttribute("id",this.renderConfig.id)}else this.canvasContext=this.renderConfig.context;this.data=e,this.layers=e.layers,this.transformCanvas={w:e.w,h:e.h,sx:0,sy:0,tx:0,ty:0},this.setupGlobalData(e,document.body),this.globalData.canvasContext=this.canvasContext,this.globalData.renderer=this,this.globalData.isDashed=!1,this.globalData.progressiveLoad=this.renderConfig.progressiveLoad,this.globalData.transformCanvas=this.transformCanvas,this.elements=createSizedArray(e.layers.length),this.updateContainerSize()},CanvasRendererBase.prototype.updateContainerSize=function(){this.reset();var e,r;this.animationItem.wrapper&&this.animationItem.container?(e=this.animationItem.wrapper.offsetWidth,r=this.animationItem.wrapper.offsetHeight,this.animationItem.container.setAttribute("width",e*this.renderConfig.dpr),this.animationItem.container.setAttribute("height",r*this.renderConfig.dpr)):(e=this.canvasContext.canvas.width*this.renderConfig.dpr,r=this.canvasContext.canvas.height*this.renderConfig.dpr);var n,a;if(this.renderConfig.preserveAspectRatio.indexOf("meet")!==-1||this.renderConfig.preserveAspectRatio.indexOf("slice")!==-1){var l=this.renderConfig.preserveAspectRatio.split(" "),c=l[1]||"meet",u=l[0]||"xMidYMid",d=u.substr(0,4),m=u.substr(4);n=e/r,a=this.transformCanvas.w/this.transformCanvas.h,a>n&&c==="meet"||a<n&&c==="slice"?(this.transformCanvas.sx=e/(this.transformCanvas.w/this.renderConfig.dpr),this.transformCanvas.sy=e/(this.transformCanvas.w/this.renderConfig.dpr)):(this.transformCanvas.sx=r/(this.transformCanvas.h/this.renderConfig.dpr),this.transformCanvas.sy=r/(this.transformCanvas.h/this.renderConfig.dpr)),d==="xMid"&&(a<n&&c==="meet"||a>n&&c==="slice")?this.transformCanvas.tx=(e-this.transformCanvas.w*(r/this.transformCanvas.h))/2*this.renderConfig.dpr:d==="xMax"&&(a<n&&c==="meet"||a>n&&c==="slice")?this.transformCanvas.tx=(e-this.transformCanvas.w*(r/this.transformCanvas.h))*this.renderConfig.dpr:this.transformCanvas.tx=0,m==="YMid"&&(a>n&&c==="meet"||a<n&&c==="slice")?this.transformCanvas.ty=(r-this.transformCanvas.h*(e/this.transformCanvas.w))/2*this.renderConfig.dpr:m==="YMax"&&(a>n&&c==="meet"||a<n&&c==="slice")?this.transformCanvas.ty=(r-this.transformCanvas.h*(e/this.transformCanvas.w))*this.renderConfig.dpr:this.transformCanvas.ty=0}else this.renderConfig.preserveAspectRatio==="none"?(this.transformCanvas.sx=e/(this.transformCanvas.w/this.renderConfig.dpr),this.transformCanvas.sy=r/(this.transformCanvas.h/this.renderConfig.dpr),this.transformCanvas.tx=0,this.transformCanvas.ty=0):(this.transformCanvas.sx=this.renderConfig.dpr,this.transformCanvas.sy=this.renderConfig.dpr,this.transformCanvas.tx=0,this.transformCanvas.ty=0);this.transformCanvas.props=[this.transformCanvas.sx,0,0,0,0,this.transformCanvas.sy,0,0,0,0,1,0,this.transformCanvas.tx,this.transformCanvas.ty,0,1],this.ctxTransform(this.transformCanvas.props),this.canvasContext.beginPath(),this.canvasContext.rect(0,0,this.transformCanvas.w,this.transformCanvas.h),this.canvasContext.closePath(),this.canvasContext.clip(),this.renderFrame(this.renderedFrame,!0)},CanvasRendererBase.prototype.destroy=function(){this.renderConfig.clearCanvas&&this.animationItem.wrapper&&(this.animationItem.wrapper.innerText="");var e,r=this.layers?this.layers.length:0;for(e=r-1;e>=0;e-=1)this.elements[e]&&this.elements[e].destroy();this.elements.length=0,this.globalData.canvasContext=null,this.animationItem.container=null,this.destroyed=!0},CanvasRendererBase.prototype.renderFrame=function(e,r){if(!(this.renderedFrame===e&&this.renderConfig.clearCanvas===!0&&!r||this.destroyed||e===-1)){this.renderedFrame=e,this.globalData.frameNum=e-this.animationItem._isFirstFrame,this.globalData.frameId+=1,this.globalData._mdf=!this.renderConfig.clearCanvas||r,this.globalData.projectInterface.currentFrame=e;var n,a=this.layers.length;for(this.completeLayers||this.checkLayers(e),n=0;n<a;n+=1)(this.completeLayers||this.elements[n])&&this.elements[n].prepareFrame(e-this.layers[n].st);if(this.globalData._mdf){for(this.renderConfig.clearCanvas===!0?this.canvasContext.clearRect(0,0,this.transformCanvas.w,this.transformCanvas.h):this.save(),n=a-1;n>=0;n-=1)(this.completeLayers||this.elements[n])&&this.elements[n].renderFrame();this.renderConfig.clearCanvas!==!0&&this.restore()}}},CanvasRendererBase.prototype.buildItem=function(e){var r=this.elements;if(!(r[e]||this.layers[e].ty===99)){var n=this.createItem(this.layers[e],this,this.globalData);r[e]=n,n.initExpressions()}},CanvasRendererBase.prototype.checkPendingElements=function(){for(;this.pendingElements.length;){var e=this.pendingElements.pop();e.checkParenting()}},CanvasRendererBase.prototype.hide=function(){this.animationItem.container.style.display="none"},CanvasRendererBase.prototype.show=function(){this.animationItem.container.style.display="block"};function CVCompElement(e,r,n){this.completeLayers=!1,this.layers=e.layers,this.pendingElements=[],this.elements=createSizedArray(this.layers.length),this.initElement(e,r,n),this.tm=e.tm?PropertyFactory.getProp(this,e.tm,0,r.frameRate,this):{_placeholder:!0}}extendPrototype([CanvasRendererBase,ICompElement,CVBaseElement],CVCompElement),CVCompElement.prototype.renderInnerContent=function(){var e=this.canvasContext;e.beginPath(),e.moveTo(0,0),e.lineTo(this.data.w,0),e.lineTo(this.data.w,this.data.h),e.lineTo(0,this.data.h),e.lineTo(0,0),e.clip();var r,n=this.layers.length;for(r=n-1;r>=0;r-=1)(this.completeLayers||this.elements[r])&&this.elements[r].renderFrame()},CVCompElement.prototype.destroy=function(){var e,r=this.layers.length;for(e=r-1;e>=0;e-=1)this.elements[e]&&this.elements[e].destroy();this.layers=null,this.elements=null},CVCompElement.prototype.createComp=function(e){return new CVCompElement(e,this.globalData,this)};function CanvasRenderer(e,r){this.animationItem=e,this.renderConfig={clearCanvas:r&&r.clearCanvas!==void 0?r.clearCanvas:!0,context:r&&r.context||null,progressiveLoad:r&&r.progressiveLoad||!1,preserveAspectRatio:r&&r.preserveAspectRatio||"xMidYMid meet",imagePreserveAspectRatio:r&&r.imagePreserveAspectRatio||"xMidYMid slice",contentVisibility:r&&r.contentVisibility||"visible",className:r&&r.className||"",id:r&&r.id||""},this.renderConfig.dpr=r&&r.dpr||1,this.animationItem.wrapper&&(this.renderConfig.dpr=r&&r.dpr||window.devicePixelRatio||1),this.renderedFrame=-1,this.globalData={frameNum:-1,_mdf:!1,renderConfig:this.renderConfig,currentGlobalAlpha:-1},this.contextData=new CVContextData,this.elements=[],this.pendingElements=[],this.transformMat=new Matrix,this.completeLayers=!1,this.rendererType="canvas"}extendPrototype([CanvasRendererBase],CanvasRenderer),CanvasRenderer.prototype.createComp=function(e){return new CVCompElement(e,this.globalData,this)};function HBaseElement(){}HBaseElement.prototype={checkBlendMode:function(){},initRendererElement:function(){this.baseElement=createTag(this.data.tg||"div"),this.data.hasMask?(this.svgElement=createNS("svg"),this.layerElement=createNS("g"),this.maskedElement=this.layerElement,this.svgElement.appendChild(this.layerElement),this.baseElement.appendChild(this.svgElement)):this.layerElement=this.baseElement,styleDiv(this.baseElement)},createContainerElements:function(){this.renderableEffectsManager=new CVEffects(this),this.transformedElement=this.baseElement,this.maskedElement=this.layerElement,this.data.ln&&this.layerElement.setAttribute("id",this.data.ln),this.data.cl&&this.layerElement.setAttribute("class",this.data.cl),this.data.bm!==0&&this.setBlendMode()},renderElement:function(){var r=this.transformedElement?this.transformedElement.style:{};if(this.finalTransform._matMdf){var n=this.finalTransform.mat.toCSS();r.transform=n,r.webkitTransform=n}this.finalTransform._opMdf&&(r.opacity=this.finalTransform.mProp.o.v)},renderFrame:function(){this.data.hd||this.hidden||(this.renderTransform(),this.renderRenderable(),this.renderElement(),this.renderInnerContent(),this._isFirstFrame&&(this._isFirstFrame=!1))},destroy:function(){this.layerElement=null,this.transformedElement=null,this.matteElement&&(this.matteElement=null),this.maskManager&&(this.maskManager.destroy(),this.maskManager=null)},createRenderableComponents:function(){this.maskManager=new MaskElement(this.data,this,this.globalData)},addEffects:function(){},setMatte:function(){}},HBaseElement.prototype.getBaseElement=SVGBaseElement.prototype.getBaseElement,HBaseElement.prototype.destroyBaseElement=HBaseElement.prototype.destroy,HBaseElement.prototype.buildElementParenting=BaseRenderer.prototype.buildElementParenting;function HSolidElement(e,r,n){this.initElement(e,r,n)}extendPrototype([BaseElement,TransformElement,HBaseElement,HierarchyElement,FrameElement,RenderableDOMElement],HSolidElement),HSolidElement.prototype.createContent=function(){var e;this.data.hasMask?(e=createNS("rect"),e.setAttribute("width",this.data.sw),e.setAttribute("height",this.data.sh),e.setAttribute("fill",this.data.sc),this.svgElement.setAttribute("width",this.data.sw),this.svgElement.setAttribute("height",this.data.sh)):(e=createTag("div"),e.style.width=this.data.sw+"px",e.style.height=this.data.sh+"px",e.style.backgroundColor=this.data.sc),this.layerElement.appendChild(e)};function HShapeElement(e,r,n){this.shapes=[],this.shapesData=e.shapes,this.stylesList=[],this.shapeModifiers=[],this.itemsData=[],this.processedElements=[],this.animatedContents=[],this.shapesContainer=createNS("g"),this.initElement(e,r,n),this.prevViewData=[],this.currentBBox={x:999999,y:-999999,h:0,w:0}}extendPrototype([BaseElement,TransformElement,HSolidElement,SVGShapeElement,HBaseElement,HierarchyElement,FrameElement,RenderableElement],HShapeElement),HShapeElement.prototype._renderShapeFrame=HShapeElement.prototype.renderInnerContent,HShapeElement.prototype.createContent=function(){var e;if(this.baseElement.style.fontSize=0,this.data.hasMask)this.layerElement.appendChild(this.shapesContainer),e=this.svgElement;else{e=createNS("svg");var r=this.comp.data?this.comp.data:this.globalData.compSize;e.setAttribute("width",r.w),e.setAttribute("height",r.h),e.appendChild(this.shapesContainer),this.layerElement.appendChild(e)}this.searchShapes(this.shapesData,this.itemsData,this.prevViewData,this.shapesContainer,0,[],!0),this.filterUniqueShapes(),this.shapeCont=e},HShapeElement.prototype.getTransformedPoint=function(e,r){var n,a=e.length;for(n=0;n<a;n+=1)r=e[n].mProps.v.applyToPointArray(r[0],r[1],0);return r},HShapeElement.prototype.calculateShapeBoundingBox=function(e,r){var n=e.sh.v,a=e.transformers,l,c=n._length,u,d,m,b;if(!(c<=1)){for(l=0;l<c-1;l+=1)u=this.getTransformedPoint(a,n.v[l]),d=this.getTransformedPoint(a,n.o[l]),m=this.getTransformedPoint(a,n.i[l+1]),b=this.getTransformedPoint(a,n.v[l+1]),this.checkBounds(u,d,m,b,r);n.c&&(u=this.getTransformedPoint(a,n.v[l]),d=this.getTransformedPoint(a,n.o[l]),m=this.getTransformedPoint(a,n.i[0]),b=this.getTransformedPoint(a,n.v[0]),this.checkBounds(u,d,m,b,r))}},HShapeElement.prototype.checkBounds=function(e,r,n,a,l){this.getBoundsOfCurve(e,r,n,a);var c=this.shapeBoundingBox;l.x=bmMin(c.left,l.x),l.xMax=bmMax(c.right,l.xMax),l.y=bmMin(c.top,l.y),l.yMax=bmMax(c.bottom,l.yMax)},HShapeElement.prototype.shapeBoundingBox={left:0,right:0,top:0,bottom:0},HShapeElement.prototype.tempBoundingBox={x:0,xMax:0,y:0,yMax:0,width:0,height:0},HShapeElement.prototype.getBoundsOfCurve=function(e,r,n,a){for(var l=[[e[0],a[0]],[e[1],a[1]]],c,u,d,m,b,A,_,T=0;T<2;++T)u=6*e[T]-12*r[T]+6*n[T],c=-3*e[T]+9*r[T]-9*n[T]+3*a[T],d=3*r[T]-3*e[T],u|=0,c|=0,d|=0,c===0&&u===0||(c===0?(m=-d/u,m>0&&m<1&&l[T].push(this.calculateF(m,e,r,n,a,T))):(b=u*u-4*d*c,b>=0&&(A=(-u+bmSqrt(b))/(2*c),A>0&&A<1&&l[T].push(this.calculateF(A,e,r,n,a,T)),_=(-u-bmSqrt(b))/(2*c),_>0&&_<1&&l[T].push(this.calculateF(_,e,r,n,a,T)))));this.shapeBoundingBox.left=bmMin.apply(null,l[0]),this.shapeBoundingBox.top=bmMin.apply(null,l[1]),this.shapeBoundingBox.right=bmMax.apply(null,l[0]),this.shapeBoundingBox.bottom=bmMax.apply(null,l[1])},HShapeElement.prototype.calculateF=function(e,r,n,a,l,c){return bmPow(1-e,3)*r[c]+3*bmPow(1-e,2)*e*n[c]+3*(1-e)*bmPow(e,2)*a[c]+bmPow(e,3)*l[c]},HShapeElement.prototype.calculateBoundingBox=function(e,r){var n,a=e.length;for(n=0;n<a;n+=1)e[n]&&e[n].sh?this.calculateShapeBoundingBox(e[n],r):e[n]&&e[n].it&&this.calculateBoundingBox(e[n].it,r)},HShapeElement.prototype.currentBoxContains=function(e){return this.currentBBox.x<=e.x&&this.currentBBox.y<=e.y&&this.currentBBox.width+this.currentBBox.x>=e.x+e.width&&this.currentBBox.height+this.currentBBox.y>=e.y+e.height},HShapeElement.prototype.renderInnerContent=function(){if(this._renderShapeFrame(),!this.hidden&&(this._isFirstFrame||this._mdf)){var e=this.tempBoundingBox,r=999999;if(e.x=r,e.xMax=-r,e.y=r,e.yMax=-r,this.calculateBoundingBox(this.itemsData,e),e.width=e.xMax<e.x?0:e.xMax-e.x,e.height=e.yMax<e.y?0:e.yMax-e.y,this.currentBoxContains(e))return;var n=!1;if(this.currentBBox.w!==e.width&&(this.currentBBox.w=e.width,this.shapeCont.setAttribute("width",e.width),n=!0),this.currentBBox.h!==e.height&&(this.currentBBox.h=e.height,this.shapeCont.setAttribute("height",e.height),n=!0),n||this.currentBBox.x!==e.x||this.currentBBox.y!==e.y){this.currentBBox.w=e.width,this.currentBBox.h=e.height,this.currentBBox.x=e.x,this.currentBBox.y=e.y,this.shapeCont.setAttribute("viewBox",this.currentBBox.x+" "+this.currentBBox.y+" "+this.currentBBox.w+" "+this.currentBBox.h);var a=this.shapeCont.style,l="translate("+this.currentBBox.x+"px,"+this.currentBBox.y+"px)";a.transform=l,a.webkitTransform=l}}};function HTextElement(e,r,n){this.textSpans=[],this.textPaths=[],this.currentBBox={x:999999,y:-999999,h:0,w:0},this.renderType="svg",this.isMasked=!1,this.initElement(e,r,n)}extendPrototype([BaseElement,TransformElement,HBaseElement,HierarchyElement,FrameElement,RenderableDOMElement,ITextElement],HTextElement),HTextElement.prototype.createContent=function(){if(this.isMasked=this.checkMasks(),this.isMasked){this.renderType="svg",this.compW=this.comp.data.w,this.compH=this.comp.data.h,this.svgElement.setAttribute("width",this.compW),this.svgElement.setAttribute("height",this.compH);var e=createNS("g");this.maskedElement.appendChild(e),this.innerElem=e}else this.renderType="html",this.innerElem=this.layerElement;this.checkParenting()},HTextElement.prototype.buildNewText=function(){var e=this.textProperty.currentData;this.renderedLetters=createSizedArray(e.l?e.l.length:0);var r=this.innerElem.style,n=e.fc?this.buildColor(e.fc):"rgba(0,0,0,0)";r.fill=n,r.color=n,e.sc&&(r.stroke=this.buildColor(e.sc),r.strokeWidth=e.sw+"px");var a=this.globalData.fontManager.getFontByName(e.f);if(!this.globalData.fontManager.chars)if(r.fontSize=e.finalSize+"px",r.lineHeight=e.finalSize+"px",a.fClass)this.innerElem.className=a.fClass;else{r.fontFamily=a.fFamily;var l=e.fWeight,c=e.fStyle;r.fontStyle=c,r.fontWeight=l}var u,d,m=e.l;d=m.length;var b,A,_,T=this.mHelper,C,S="",P=0;for(u=0;u<d;u+=1){if(this.globalData.fontManager.chars?(this.textPaths[P]?b=this.textPaths[P]:(b=createNS("path"),b.setAttribute("stroke-linecap",lineCapEnum[1]),b.setAttribute("stroke-linejoin",lineJoinEnum[2]),b.setAttribute("stroke-miterlimit","4")),this.isMasked||(this.textSpans[P]?(A=this.textSpans[P],_=A.children[0]):(A=createTag("div"),A.style.lineHeight=0,_=createNS("svg"),_.appendChild(b),styleDiv(A)))):this.isMasked?b=this.textPaths[P]?this.textPaths[P]:createNS("text"):this.textSpans[P]?(A=this.textSpans[P],b=this.textPaths[P]):(A=createTag("span"),styleDiv(A),b=createTag("span"),styleDiv(b),A.appendChild(b)),this.globalData.fontManager.chars){var k=this.globalData.fontManager.getCharData(e.finalText[u],a.fStyle,this.globalData.fontManager.getFontByName(e.f).fFamily),x;if(k?x=k.data:x=null,T.reset(),x&&x.shapes&&x.shapes.length&&(C=x.shapes[0].it,T.scale(e.finalSize/100,e.finalSize/100),S=this.createPathShape(T,C),b.setAttribute("d",S)),this.isMasked)this.innerElem.appendChild(b);else{if(this.innerElem.appendChild(A),x&&x.shapes){document.body.appendChild(_);var E=_.getBBox();_.setAttribute("width",E.width+2),_.setAttribute("height",E.height+2),_.setAttribute("viewBox",E.x-1+" "+(E.y-1)+" "+(E.width+2)+" "+(E.height+2));var g=_.style,y="translate("+(E.x-1)+"px,"+(E.y-1)+"px)";g.transform=y,g.webkitTransform=y,m[u].yOffset=E.y-1}else _.setAttribute("width",1),_.setAttribute("height",1);A.appendChild(_)}}else if(b.textContent=m[u].val,b.setAttributeNS("http://www.w3.org/XML/1998/namespace","xml:space","preserve"),this.isMasked)this.innerElem.appendChild(b);else{this.innerElem.appendChild(A);var I=b.style,M="translate3d(0,"+-e.finalSize/1.2+"px,0)";I.transform=M,I.webkitTransform=M}this.isMasked?this.textSpans[P]=b:this.textSpans[P]=A,this.textSpans[P].style.display="block",this.textPaths[P]=b,P+=1}for(;P<this.textSpans.length;)this.textSpans[P].style.display="none",P+=1},HTextElement.prototype.renderInnerContent=function(){var e;if(this.data.singleShape){if(!this._isFirstFrame&&!this.lettersChangedFlag)return;if(this.isMasked&&this.finalTransform._matMdf){this.svgElement.setAttribute("viewBox",-this.finalTransform.mProp.p.v[0]+" "+-this.finalTransform.mProp.p.v[1]+" "+this.compW+" "+this.compH),e=this.svgElement.style;var r="translate("+-this.finalTransform.mProp.p.v[0]+"px,"+-this.finalTransform.mProp.p.v[1]+"px)";e.transform=r,e.webkitTransform=r}}if(this.textAnimator.getMeasures(this.textProperty.currentData,this.lettersChangedFlag),!(!this.lettersChangedFlag&&!this.textAnimator.lettersChangedFlag)){var n,a,l=0,c=this.textAnimator.renderedLetters,u=this.textProperty.currentData.l;a=u.length;var d,m,b;for(n=0;n<a;n+=1)u[n].n?l+=1:(m=this.textSpans[n],b=this.textPaths[n],d=c[l],l+=1,d._mdf.m&&(this.isMasked?m.setAttribute("transform",d.m):(m.style.webkitTransform=d.m,m.style.transform=d.m)),m.style.opacity=d.o,d.sw&&d._mdf.sw&&b.setAttribute("stroke-width",d.sw),d.sc&&d._mdf.sc&&b.setAttribute("stroke",d.sc),d.fc&&d._mdf.fc&&(b.setAttribute("fill",d.fc),b.style.color=d.fc));if(this.innerElem.getBBox&&!this.hidden&&(this._isFirstFrame||this._mdf)){var A=this.innerElem.getBBox();this.currentBBox.w!==A.width&&(this.currentBBox.w=A.width,this.svgElement.setAttribute("width",A.width)),this.currentBBox.h!==A.height&&(this.currentBBox.h=A.height,this.svgElement.setAttribute("height",A.height));var _=1;if(this.currentBBox.w!==A.width+_*2||this.currentBBox.h!==A.height+_*2||this.currentBBox.x!==A.x-_||this.currentBBox.y!==A.y-_){this.currentBBox.w=A.width+_*2,this.currentBBox.h=A.height+_*2,this.currentBBox.x=A.x-_,this.currentBBox.y=A.y-_,this.svgElement.setAttribute("viewBox",this.currentBBox.x+" "+this.currentBBox.y+" "+this.currentBBox.w+" "+this.currentBBox.h),e=this.svgElement.style;var T="translate("+this.currentBBox.x+"px,"+this.currentBBox.y+"px)";e.transform=T,e.webkitTransform=T}}}};function HCameraElement(e,r,n){this.initFrame(),this.initBaseData(e,r,n),this.initHierarchy();var a=PropertyFactory.getProp;if(this.pe=a(this,e.pe,0,0,this),e.ks.p.s?(this.px=a(this,e.ks.p.x,1,0,this),this.py=a(this,e.ks.p.y,1,0,this),this.pz=a(this,e.ks.p.z,1,0,this)):this.p=a(this,e.ks.p,1,0,this),e.ks.a&&(this.a=a(this,e.ks.a,1,0,this)),e.ks.or.k.length&&e.ks.or.k[0].to){var l,c=e.ks.or.k.length;for(l=0;l<c;l+=1)e.ks.or.k[l].to=null,e.ks.or.k[l].ti=null}this.or=a(this,e.ks.or,1,degToRads,this),this.or.sh=!0,this.rx=a(this,e.ks.rx,0,degToRads,this),this.ry=a(this,e.ks.ry,0,degToRads,this),this.rz=a(this,e.ks.rz,0,degToRads,this),this.mat=new Matrix,this._prevMat=new Matrix,this._isFirstFrame=!0,this.finalTransform={mProp:this}}extendPrototype([BaseElement,FrameElement,HierarchyElement],HCameraElement),HCameraElement.prototype.setup=function(){var e,r=this.comp.threeDElements.length,n,a,l;for(e=0;e<r;e+=1)if(n=this.comp.threeDElements[e],n.type==="3d"){a=n.perspectiveElem.style,l=n.container.style;var c=this.pe.v+"px",u="0px 0px 0px",d="matrix3d(1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1)";a.perspective=c,a.webkitPerspective=c,l.transformOrigin=u,l.mozTransformOrigin=u,l.webkitTransformOrigin=u,a.transform=d,a.webkitTransform=d}},HCameraElement.prototype.createElements=function(){},HCameraElement.prototype.hide=function(){},HCameraElement.prototype.renderFrame=function(){var e=this._isFirstFrame,r,n;if(this.hierarchy)for(n=this.hierarchy.length,r=0;r<n;r+=1)e=this.hierarchy[r].finalTransform.mProp._mdf||e;if(e||this.pe._mdf||this.p&&this.p._mdf||this.px&&(this.px._mdf||this.py._mdf||this.pz._mdf)||this.rx._mdf||this.ry._mdf||this.rz._mdf||this.or._mdf||this.a&&this.a._mdf){if(this.mat.reset(),this.hierarchy)for(n=this.hierarchy.length-1,r=n;r>=0;r-=1){var a=this.hierarchy[r].finalTransform.mProp;this.mat.translate(-a.p.v[0],-a.p.v[1],a.p.v[2]),this.mat.rotateX(-a.or.v[0]).rotateY(-a.or.v[1]).rotateZ(a.or.v[2]),this.mat.rotateX(-a.rx.v).rotateY(-a.ry.v).rotateZ(a.rz.v),this.mat.scale(1/a.s.v[0],1/a.s.v[1],1/a.s.v[2]),this.mat.translate(a.a.v[0],a.a.v[1],a.a.v[2])}if(this.p?this.mat.translate(-this.p.v[0],-this.p.v[1],this.p.v[2]):this.mat.translate(-this.px.v,-this.py.v,this.pz.v),this.a){var l;this.p?l=[this.p.v[0]-this.a.v[0],this.p.v[1]-this.a.v[1],this.p.v[2]-this.a.v[2]]:l=[this.px.v-this.a.v[0],this.py.v-this.a.v[1],this.pz.v-this.a.v[2]];var c=Math.sqrt(Math.pow(l[0],2)+Math.pow(l[1],2)+Math.pow(l[2],2)),u=[l[0]/c,l[1]/c,l[2]/c],d=Math.sqrt(u[2]*u[2]+u[0]*u[0]),m=Math.atan2(u[1],d),b=Math.atan2(u[0],-u[2]);this.mat.rotateY(b).rotateX(-m)}this.mat.rotateX(-this.rx.v).rotateY(-this.ry.v).rotateZ(this.rz.v),this.mat.rotateX(-this.or.v[0]).rotateY(-this.or.v[1]).rotateZ(this.or.v[2]),this.mat.translate(this.globalData.compSize.w/2,this.globalData.compSize.h/2,0),this.mat.translate(0,0,this.pe.v);var A=!this._prevMat.equals(this.mat);if((A||this.pe._mdf)&&this.comp.threeDElements){n=this.comp.threeDElements.length;var _,T,C;for(r=0;r<n;r+=1)if(_=this.comp.threeDElements[r],_.type==="3d"){if(A){var S=this.mat.toCSS();C=_.container.style,C.transform=S,C.webkitTransform=S}this.pe._mdf&&(T=_.perspectiveElem.style,T.perspective=this.pe.v+"px",T.webkitPerspective=this.pe.v+"px")}this.mat.clone(this._prevMat)}}this._isFirstFrame=!1},HCameraElement.prototype.prepareFrame=function(e){this.prepareProperties(e,!0)},HCameraElement.prototype.destroy=function(){},HCameraElement.prototype.getBaseElement=function(){return null};function HImageElement(e,r,n){this.assetData=r.getAssetData(e.refId),this.initElement(e,r,n)}extendPrototype([BaseElement,TransformElement,HBaseElement,HSolidElement,HierarchyElement,FrameElement,RenderableElement],HImageElement),HImageElement.prototype.createContent=function(){var e=this.globalData.getAssetsPath(this.assetData),r=new Image;this.data.hasMask?(this.imageElem=createNS("image"),this.imageElem.setAttribute("width",this.assetData.w+"px"),this.imageElem.setAttribute("height",this.assetData.h+"px"),this.imageElem.setAttributeNS("http://www.w3.org/1999/xlink","href",e),this.layerElement.appendChild(this.imageElem),this.baseElement.setAttribute("width",this.assetData.w),this.baseElement.setAttribute("height",this.assetData.h)):this.layerElement.appendChild(r),r.crossOrigin="anonymous",r.src=e,this.data.ln&&this.baseElement.setAttribute("id",this.data.ln)};function HybridRendererBase(e,r){this.animationItem=e,this.layers=null,this.renderedFrame=-1,this.renderConfig={className:r&&r.className||"",imagePreserveAspectRatio:r&&r.imagePreserveAspectRatio||"xMidYMid slice",hideOnTransparent:!(r&&r.hideOnTransparent===!1),filterSize:{width:r&&r.filterSize&&r.filterSize.width||"400%",height:r&&r.filterSize&&r.filterSize.height||"400%",x:r&&r.filterSize&&r.filterSize.x||"-100%",y:r&&r.filterSize&&r.filterSize.y||"-100%"}},this.globalData={_mdf:!1,frameNum:-1,renderConfig:this.renderConfig},this.pendingElements=[],this.elements=[],this.threeDElements=[],this.destroyed=!1,this.camera=null,this.supports3d=!0,this.rendererType="html"}extendPrototype([BaseRenderer],HybridRendererBase),HybridRendererBase.prototype.buildItem=SVGRenderer.prototype.buildItem,HybridRendererBase.prototype.checkPendingElements=function(){for(;this.pendingElements.length;){var e=this.pendingElements.pop();e.checkParenting()}},HybridRendererBase.prototype.appendElementInPos=function(e,r){var n=e.getBaseElement();if(!!n){var a=this.layers[r];if(!a.ddd||!this.supports3d)if(this.threeDElements)this.addTo3dContainer(n,r);else{for(var l=0,c,u,d;l<r;)this.elements[l]&&this.elements[l]!==!0&&this.elements[l].getBaseElement&&(u=this.elements[l],d=this.layers[l].ddd?this.getThreeDContainerByPos(l):u.getBaseElement(),c=d||c),l+=1;c?(!a.ddd||!this.supports3d)&&this.layerElement.insertBefore(n,c):(!a.ddd||!this.supports3d)&&this.layerElement.appendChild(n)}else this.addTo3dContainer(n,r)}},HybridRendererBase.prototype.createShape=function(e){return this.supports3d?new HShapeElement(e,this.globalData,this):new SVGShapeElement(e,this.globalData,this)},HybridRendererBase.prototype.createText=function(e){return this.supports3d?new HTextElement(e,this.globalData,this):new SVGTextLottieElement(e,this.globalData,this)},HybridRendererBase.prototype.createCamera=function(e){return this.camera=new HCameraElement(e,this.globalData,this),this.camera},HybridRendererBase.prototype.createImage=function(e){return this.supports3d?new HImageElement(e,this.globalData,this):new IImageElement(e,this.globalData,this)},HybridRendererBase.prototype.createSolid=function(e){return this.supports3d?new HSolidElement(e,this.globalData,this):new ISolidElement(e,this.globalData,this)},HybridRendererBase.prototype.createNull=SVGRenderer.prototype.createNull,HybridRendererBase.prototype.getThreeDContainerByPos=function(e){for(var r=0,n=this.threeDElements.length;r<n;){if(this.threeDElements[r].startPos<=e&&this.threeDElements[r].endPos>=e)return this.threeDElements[r].perspectiveElem;r+=1}return null},HybridRendererBase.prototype.createThreeDContainer=function(e,r){var n=createTag("div"),a,l;styleDiv(n);var c=createTag("div");if(styleDiv(c),r==="3d"){a=n.style,a.width=this.globalData.compSize.w+"px",a.height=this.globalData.compSize.h+"px";var u="50% 50%";a.webkitTransformOrigin=u,a.mozTransformOrigin=u,a.transformOrigin=u,l=c.style;var d="matrix3d(1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1)";l.transform=d,l.webkitTransform=d}n.appendChild(c);var m={container:c,perspectiveElem:n,startPos:e,endPos:e,type:r};return this.threeDElements.push(m),m},HybridRendererBase.prototype.build3dContainers=function(){var e,r=this.layers.length,n,a="";for(e=0;e<r;e+=1)this.layers[e].ddd&&this.layers[e].ty!==3?(a!=="3d"&&(a="3d",n=this.createThreeDContainer(e,"3d")),n.endPos=Math.max(n.endPos,e)):(a!=="2d"&&(a="2d",n=this.createThreeDContainer(e,"2d")),n.endPos=Math.max(n.endPos,e));for(r=this.threeDElements.length,e=r-1;e>=0;e-=1)this.resizerElem.appendChild(this.threeDElements[e].perspectiveElem)},HybridRendererBase.prototype.addTo3dContainer=function(e,r){for(var n=0,a=this.threeDElements.length;n<a;){if(r<=this.threeDElements[n].endPos){for(var l=this.threeDElements[n].startPos,c;l<r;)this.elements[l]&&this.elements[l].getBaseElement&&(c=this.elements[l].getBaseElement()),l+=1;c?this.threeDElements[n].container.insertBefore(e,c):this.threeDElements[n].container.appendChild(e);break}n+=1}},HybridRendererBase.prototype.configAnimation=function(e){var r=createTag("div"),n=this.animationItem.wrapper,a=r.style;a.width=e.w+"px",a.height=e.h+"px",this.resizerElem=r,styleDiv(r),a.transformStyle="flat",a.mozTransformStyle="flat",a.webkitTransformStyle="flat",this.renderConfig.className&&r.setAttribute("class",this.renderConfig.className),n.appendChild(r),a.overflow="hidden";var l=createNS("svg");l.setAttribute("width","1"),l.setAttribute("height","1"),styleDiv(l),this.resizerElem.appendChild(l);var c=createNS("defs");l.appendChild(c),this.data=e,this.setupGlobalData(e,l),this.globalData.defs=c,this.layers=e.layers,this.layerElement=this.resizerElem,this.build3dContainers(),this.updateContainerSize()},HybridRendererBase.prototype.destroy=function(){this.animationItem.wrapper&&(this.animationItem.wrapper.innerText=""),this.animationItem.container=null,this.globalData.defs=null;var e,r=this.layers?this.layers.length:0;for(e=0;e<r;e+=1)this.elements[e].destroy();this.elements.length=0,this.destroyed=!0,this.animationItem=null},HybridRendererBase.prototype.updateContainerSize=function(){var e=this.animationItem.wrapper.offsetWidth,r=this.animationItem.wrapper.offsetHeight,n=e/r,a=this.globalData.compSize.w/this.globalData.compSize.h,l,c,u,d;a>n?(l=e/this.globalData.compSize.w,c=e/this.globalData.compSize.w,u=0,d=(r-this.globalData.compSize.h*(e/this.globalData.compSize.w))/2):(l=r/this.globalData.compSize.h,c=r/this.globalData.compSize.h,u=(e-this.globalData.compSize.w*(r/this.globalData.compSize.h))/2,d=0);var m=this.resizerElem.style;m.webkitTransform="matrix3d("+l+",0,0,0,0,"+c+",0,0,0,0,1,0,"+u+","+d+",0,1)",m.transform=m.webkitTransform},HybridRendererBase.prototype.renderFrame=SVGRenderer.prototype.renderFrame,HybridRendererBase.prototype.hide=function(){this.resizerElem.style.display="none"},HybridRendererBase.prototype.show=function(){this.resizerElem.style.display="block"},HybridRendererBase.prototype.initItems=function(){if(this.buildAllItems(),this.camera)this.camera.setup();else{var e=this.globalData.compSize.w,r=this.globalData.compSize.h,n,a=this.threeDElements.length;for(n=0;n<a;n+=1){var l=this.threeDElements[n].perspectiveElem.style;l.webkitPerspective=Math.sqrt(Math.pow(e,2)+Math.pow(r,2))+"px",l.perspective=l.webkitPerspective}}},HybridRendererBase.prototype.searchExtraCompositions=function(e){var r,n=e.length,a=createTag("div");for(r=0;r<n;r+=1)if(e[r].xt){var l=this.createComp(e[r],a,this.globalData.comp,null);l.initExpressions(),this.globalData.projectInterface.registerComposition(l)}};function HCompElement(e,r,n){this.layers=e.layers,this.supports3d=!e.hasMask,this.completeLayers=!1,this.pendingElements=[],this.elements=this.layers?createSizedArray(this.layers.length):[],this.initElement(e,r,n),this.tm=e.tm?PropertyFactory.getProp(this,e.tm,0,r.frameRate,this):{_placeholder:!0}}extendPrototype([HybridRendererBase,ICompElement,HBaseElement],HCompElement),HCompElement.prototype._createBaseContainerElements=HCompElement.prototype.createContainerElements,HCompElement.prototype.createContainerElements=function(){this._createBaseContainerElements(),this.data.hasMask?(this.svgElement.setAttribute("width",this.data.w),this.svgElement.setAttribute("height",this.data.h),this.transformedElement=this.baseElement):this.transformedElement=this.layerElement},HCompElement.prototype.addTo3dContainer=function(e,r){for(var n=0,a;n<r;)this.elements[n]&&this.elements[n].getBaseElement&&(a=this.elements[n].getBaseElement()),n+=1;a?this.layerElement.insertBefore(e,a):this.layerElement.appendChild(e)},HCompElement.prototype.createComp=function(e){return this.supports3d?new HCompElement(e,this.globalData,this):new SVGCompElement(e,this.globalData,this)};function HybridRenderer(e,r){this.animationItem=e,this.layers=null,this.renderedFrame=-1,this.renderConfig={className:r&&r.className||"",imagePreserveAspectRatio:r&&r.imagePreserveAspectRatio||"xMidYMid slice",hideOnTransparent:!(r&&r.hideOnTransparent===!1),filterSize:{width:r&&r.filterSize&&r.filterSize.width||"400%",height:r&&r.filterSize&&r.filterSize.height||"400%",x:r&&r.filterSize&&r.filterSize.x||"-100%",y:r&&r.filterSize&&r.filterSize.y||"-100%"}},this.globalData={_mdf:!1,frameNum:-1,renderConfig:this.renderConfig},this.pendingElements=[],this.elements=[],this.threeDElements=[],this.destroyed=!1,this.camera=null,this.supports3d=!0,this.rendererType="html"}extendPrototype([HybridRendererBase],HybridRenderer),HybridRenderer.prototype.createComp=function(e){return this.supports3d?new HCompElement(e,this.globalData,this):new SVGCompElement(e,this.globalData,this)};var Expressions=function(){var e={};e.initExpressions=r;function r(n){var a=0,l=[];function c(){a+=1}function u(){a-=1,a===0&&m()}function d(b){l.indexOf(b)===-1&&l.push(b)}function m(){var b,A=l.length;for(b=0;b<A;b+=1)l[b].release();l.length=0}n.renderer.compInterface=CompExpressionInterface(n.renderer),n.renderer.globalData.projectInterface.registerComposition(n.renderer),n.renderer.globalData.pushExpression=c,n.renderer.globalData.popExpression=u,n.renderer.globalData.registerExpressionProperty=d}return e}();function _typeof$1(e){return typeof Symbol=="function"&&typeof Symbol.iterator=="symbol"?_typeof$1=function(n){return typeof n}:_typeof$1=function(n){return n&&typeof Symbol=="function"&&n.constructor===Symbol&&n!==Symbol.prototype?"symbol":typeof n},_typeof$1(e)}function seedRandom(e,r){var n=this,a=256,l=6,c=52,u="random",d=r.pow(a,l),m=r.pow(2,c),b=m*2,A=a-1,_;function T(g,y,I){var M=[];y=y===!0?{entropy:!0}:y||{};var j=k(P(y.entropy?[g,E(e)]:g===null?x():g,3),M),O=new C(M),R=function(){for(var G=O.g(l),V=d,Q=0;G<m;)G=(G+Q)*a,V*=a,Q=O.g(1);for(;G>=b;)G/=2,V/=2,Q>>>=1;return(G+Q)/V};return R.int32=function(){return O.g(4)|0},R.quick=function(){return O.g(4)/4294967296},R.double=R,k(E(O.S),e),(y.pass||I||function(B,G,V,Q){return Q&&(Q.S&&S(Q,O),B.state=function(){return S(O,{})}),V?(r[u]=B,G):B})(R,j,"global"in y?y.global:this==r,y.state)}r["seed"+u]=T;function C(g){var y,I=g.length,M=this,j=0,O=M.i=M.j=0,R=M.S=[];for(I||(g=[I++]);j<a;)R[j]=j++;for(j=0;j<a;j++)R[j]=R[O=A&O+g[j%I]+(y=R[j])],R[O]=y;M.g=function(B){for(var G,V=0,Q=M.i,W=M.j,q=M.S;B--;)G=q[Q=A&Q+1],V=V*a+q[A&(q[Q]=q[W=A&W+G])+(q[W]=G)];return M.i=Q,M.j=W,V}}function S(g,y){return y.i=g.i,y.j=g.j,y.S=g.S.slice(),y}function P(g,y){var I=[],M=_typeof$1(g),j;if(y&&M=="object")for(j in g)try{I.push(P(g[j],y-1))}catch{}return I.length?I:M=="string"?g:g+"\0"}function k(g,y){for(var I=g+"",M,j=0;j<I.length;)y[A&j]=A&(M^=y[A&j]*19)+I.charCodeAt(j++);return E(y)}function x(){try{var g=new Uint8Array(a);return(n.crypto||n.msCrypto).getRandomValues(g),E(g)}catch{var y=n.navigator,I=y&&y.plugins;return[+new Date,n,I,n.screen,E(e)]}}function E(g){return String.fromCharCode.apply(0,g)}k(r.random(),e)}function initialize$2(e){seedRandom([],e)}var propTypes={SHAPE:"shape"};function _typeof(e){return typeof Symbol=="function"&&typeof Symbol.iterator=="symbol"?_typeof=function(n){return typeof n}:_typeof=function(n){return n&&typeof Symbol=="function"&&n.constructor===Symbol&&n!==Symbol.prototype?"symbol":typeof n},_typeof(e)}var ExpressionManager=function(){var ob={},Math=BMMath,window=null,document=null,XMLHttpRequest=null,fetch=null,frames=null;initialize$2(BMMath);function $bm_isInstanceOfArray(e){return e.constructor===Array||e.constructor===Float32Array}function isNumerable(e,r){return e==="number"||e==="boolean"||e==="string"||r instanceof Number}function $bm_neg(e){var r=_typeof(e);if(r==="number"||r==="boolean"||e instanceof Number)return-e;if($bm_isInstanceOfArray(e)){var n,a=e.length,l=[];for(n=0;n<a;n+=1)l[n]=-e[n];return l}return e.propType?e.v:-e}var easeInBez=BezierFactory.getBezierEasing(.333,0,.833,.833,"easeIn").get,easeOutBez=BezierFactory.getBezierEasing(.167,.167,.667,1,"easeOut").get,easeInOutBez=BezierFactory.getBezierEasing(.33,0,.667,1,"easeInOut").get;function sum(e,r){var n=_typeof(e),a=_typeof(r);if(n==="string"||a==="string"||isNumerable(n,e)&&isNumerable(a,r))return e+r;if($bm_isInstanceOfArray(e)&&isNumerable(a,r))return e=e.slice(0),e[0]+=r,e;if(isNumerable(n,e)&&$bm_isInstanceOfArray(r))return r=r.slice(0),r[0]=e+r[0],r;if($bm_isInstanceOfArray(e)&&$bm_isInstanceOfArray(r)){for(var l=0,c=e.length,u=r.length,d=[];l<c||l<u;)(typeof e[l]=="number"||e[l]instanceof Number)&&(typeof r[l]=="number"||r[l]instanceof Number)?d[l]=e[l]+r[l]:d[l]=r[l]===void 0?e[l]:e[l]||r[l],l+=1;return d}return 0}var add=sum;function sub(e,r){var n=_typeof(e),a=_typeof(r);if(isNumerable(n,e)&&isNumerable(a,r))return n==="string"&&(e=parseInt(e,10)),a==="string"&&(r=parseInt(r,10)),e-r;if($bm_isInstanceOfArray(e)&&isNumerable(a,r))return e=e.slice(0),e[0]-=r,e;if(isNumerable(n,e)&&$bm_isInstanceOfArray(r))return r=r.slice(0),r[0]=e-r[0],r;if($bm_isInstanceOfArray(e)&&$bm_isInstanceOfArray(r)){for(var l=0,c=e.length,u=r.length,d=[];l<c||l<u;)(typeof e[l]=="number"||e[l]instanceof Number)&&(typeof r[l]=="number"||r[l]instanceof Number)?d[l]=e[l]-r[l]:d[l]=r[l]===void 0?e[l]:e[l]||r[l],l+=1;return d}return 0}function mul(e,r){var n=_typeof(e),a=_typeof(r),l;if(isNumerable(n,e)&&isNumerable(a,r))return e*r;var c,u;if($bm_isInstanceOfArray(e)&&isNumerable(a,r)){for(u=e.length,l=createTypedArray("float32",u),c=0;c<u;c+=1)l[c]=e[c]*r;return l}if(isNumerable(n,e)&&$bm_isInstanceOfArray(r)){for(u=r.length,l=createTypedArray("float32",u),c=0;c<u;c+=1)l[c]=e*r[c];return l}return 0}function div(e,r){var n=_typeof(e),a=_typeof(r),l;if(isNumerable(n,e)&&isNumerable(a,r))return e/r;var c,u;if($bm_isInstanceOfArray(e)&&isNumerable(a,r)){for(u=e.length,l=createTypedArray("float32",u),c=0;c<u;c+=1)l[c]=e[c]/r;return l}if(isNumerable(n,e)&&$bm_isInstanceOfArray(r)){for(u=r.length,l=createTypedArray("float32",u),c=0;c<u;c+=1)l[c]=e/r[c];return l}return 0}function mod(e,r){return typeof e=="string"&&(e=parseInt(e,10)),typeof r=="string"&&(r=parseInt(r,10)),e%r}var $bm_sum=sum,$bm_sub=sub,$bm_mul=mul,$bm_div=div,$bm_mod=mod;function clamp(e,r,n){if(r>n){var a=n;n=r,r=a}return Math.min(Math.max(e,r),n)}function radiansToDegrees(e){return e/degToRads}var radians_to_degrees=radiansToDegrees;function degreesToRadians(e){return e*degToRads}var degrees_to_radians=radiansToDegrees,helperLengthArray=[0,0,0,0,0,0];function length(e,r){if(typeof e=="number"||e instanceof Number)return r=r||0,Math.abs(e-r);r||(r=helperLengthArray);var n,a=Math.min(e.length,r.length),l=0;for(n=0;n<a;n+=1)l+=Math.pow(r[n]-e[n],2);return Math.sqrt(l)}function normalize(e){return div(e,length(e))}function rgbToHsl(e){var r=e[0],n=e[1],a=e[2],l=Math.max(r,n,a),c=Math.min(r,n,a),u,d,m=(l+c)/2;if(l===c)u=0,d=0;else{var b=l-c;switch(d=m>.5?b/(2-l-c):b/(l+c),l){case r:u=(n-a)/b+(n<a?6:0);break;case n:u=(a-r)/b+2;break;case a:u=(r-n)/b+4;break}u/=6}return[u,d,m,e[3]]}function hue2rgb(e,r,n){return n<0&&(n+=1),n>1&&(n-=1),n<1/6?e+(r-e)*6*n:n<1/2?r:n<2/3?e+(r-e)*(2/3-n)*6:e}function hslToRgb(e){var r=e[0],n=e[1],a=e[2],l,c,u;if(n===0)l=a,u=a,c=a;else{var d=a<.5?a*(1+n):a+n-a*n,m=2*a-d;l=hue2rgb(m,d,r+1/3),c=hue2rgb(m,d,r),u=hue2rgb(m,d,r-1/3)}return[l,c,u,e[3]]}function linear(e,r,n,a,l){if((a===void 0||l===void 0)&&(a=r,l=n,r=0,n=1),n<r){var c=n;n=r,r=c}if(e<=r)return a;if(e>=n)return l;var u=n===r?0:(e-r)/(n-r);if(!a.length)return a+(l-a)*u;var d,m=a.length,b=createTypedArray("float32",m);for(d=0;d<m;d+=1)b[d]=a[d]+(l[d]-a[d])*u;return b}function random(e,r){if(r===void 0&&(e===void 0?(e=0,r=1):(r=e,e=void 0)),r.length){var n,a=r.length;e||(e=createTypedArray("float32",a));var l=createTypedArray("float32",a),c=BMMath.random();for(n=0;n<a;n+=1)l[n]=e[n]+c*(r[n]-e[n]);return l}e===void 0&&(e=0);var u=BMMath.random();return e+u*(r-e)}function createPath(e,r,n,a){var l,c=e.length,u=shapePool.newElement();u.setPathData(!!a,c);var d=[0,0],m,b;for(l=0;l<c;l+=1)m=r&&r[l]?r[l]:d,b=n&&n[l]?n[l]:d,u.setTripleAt(e[l][0],e[l][1],b[0]+e[l][0],b[1]+e[l][1],m[0]+e[l][0],m[1]+e[l][1],l,!0);return u}function initiateExpression(elem,data,property){var val=data.x,needsVelocity=/velocity(?![\w\d])/.test(val),_needsRandom=val.indexOf("random")!==-1,elemType=elem.data.ty,transform,$bm_transform,content,effect,thisProperty=property;thisProperty.valueAtTime=thisProperty.getValueAtTime,Object.defineProperty(thisProperty,"value",{get:function(){return thisProperty.v}}),elem.comp.frameDuration=1/elem.comp.globalData.frameRate,elem.comp.displayStartTime=0;var inPoint=elem.data.ip/elem.comp.globalData.frameRate,outPoint=elem.data.op/elem.comp.globalData.frameRate,width=elem.data.sw?elem.data.sw:0,height=elem.data.sh?elem.data.sh:0,name=elem.data.nm,loopIn,loop_in,loopOut,loop_out,smooth,toWorld,fromWorld,fromComp,toComp,fromCompToSurface,position,rotation,anchorPoint,scale,thisLayer,thisComp,mask,valueAtTime,velocityAtTime,scoped_bm_rt,expression_function=eval("[function _expression_function(){"+val+";scoped_bm_rt=$bm_rt}]")[0],numKeys=property.kf?data.k.length:0,active=!this.data||this.data.hd!==!0,wiggle=function e(r,n){var a,l,c=this.pv.length?this.pv.length:1,u=createTypedArray("float32",c);r=5;var d=Math.floor(time*r);for(a=0,l=0;a<d;){for(l=0;l<c;l+=1)u[l]+=-n+n*2*BMMath.random();a+=1}var m=time*r,b=m-Math.floor(m),A=createTypedArray("float32",c);if(c>1){for(l=0;l<c;l+=1)A[l]=this.pv[l]+u[l]+(-n+n*2*BMMath.random())*b;return A}return this.pv+u[0]+(-n+n*2*BMMath.random())*b}.bind(this);thisProperty.loopIn&&(loopIn=thisProperty.loopIn.bind(thisProperty),loop_in=loopIn),thisProperty.loopOut&&(loopOut=thisProperty.loopOut.bind(thisProperty),loop_out=loopOut),thisProperty.smooth&&(smooth=thisProperty.smooth.bind(thisProperty));function loopInDuration(e,r){return loopIn(e,r,!0)}function loopOutDuration(e,r){return loopOut(e,r,!0)}this.getValueAtTime&&(valueAtTime=this.getValueAtTime.bind(this)),this.getVelocityAtTime&&(velocityAtTime=this.getVelocityAtTime.bind(this));var comp=elem.comp.globalData.projectInterface.bind(elem.comp.globalData.projectInterface);function lookAt(e,r){var n=[r[0]-e[0],r[1]-e[1],r[2]-e[2]],a=Math.atan2(n[0],Math.sqrt(n[1]*n[1]+n[2]*n[2]))/degToRads,l=-Math.atan2(n[1],n[2])/degToRads;return[l,a,0]}function easeOut(e,r,n,a,l){return applyEase(easeOutBez,e,r,n,a,l)}function easeIn(e,r,n,a,l){return applyEase(easeInBez,e,r,n,a,l)}function ease(e,r,n,a,l){return applyEase(easeInOutBez,e,r,n,a,l)}function applyEase(e,r,n,a,l,c){l===void 0?(l=n,c=a):r=(r-n)/(a-n),r>1?r=1:r<0&&(r=0);var u=e(r);if($bm_isInstanceOfArray(l)){var d,m=l.length,b=createTypedArray("float32",m);for(d=0;d<m;d+=1)b[d]=(c[d]-l[d])*u+l[d];return b}return(c-l)*u+l}function nearestKey(e){var r,n=data.k.length,a,l;if(!data.k.length||typeof data.k[0]=="number")a=0,l=0;else if(a=-1,e*=elem.comp.globalData.frameRate,e<data.k[0].t)a=1,l=data.k[0].t;else{for(r=0;r<n-1;r+=1)if(e===data.k[r].t){a=r+1,l=data.k[r].t;break}else if(e>data.k[r].t&&e<data.k[r+1].t){e-data.k[r].t>data.k[r+1].t-e?(a=r+2,l=data.k[r+1].t):(a=r+1,l=data.k[r].t);break}a===-1&&(a=r+1,l=data.k[r].t)}var c={};return c.index=a,c.time=l/elem.comp.globalData.frameRate,c}function key(e){var r,n,a;if(!data.k.length||typeof data.k[0]=="number")throw new Error("The property has no keyframe at index "+e);e-=1,r={time:data.k[e].t/elem.comp.globalData.frameRate,value:[]};var l=Object.prototype.hasOwnProperty.call(data.k[e],"s")?data.k[e].s:data.k[e-1].e;for(a=l.length,n=0;n<a;n+=1)r[n]=l[n],r.value[n]=l[n];return r}function framesToTime(e,r){return r||(r=elem.comp.globalData.frameRate),e/r}function timeToFrames(e,r){return!e&&e!==0&&(e=time),r||(r=elem.comp.globalData.frameRate),e*r}function seedRandom(e){BMMath.seedrandom(randSeed+e)}function sourceRectAtTime(){return elem.sourceRectAtTime()}function substring(e,r){return typeof value=="string"?r===void 0?value.substring(e):value.substring(e,r):""}function substr(e,r){return typeof value=="string"?r===void 0?value.substr(e):value.substr(e,r):""}function posterizeTime(e){time=e===0?0:Math.floor(time*e)/e,value=valueAtTime(time)}var time,velocity,value,text,textIndex,textTotal,selectorValue,index=elem.data.ind,hasParent=!!(elem.hierarchy&&elem.hierarchy.length),parent,randSeed=Math.floor(Math.random()*1e6),globalData=elem.globalData;function executeExpression(e){return value=e,this.frameExpressionId===elem.globalData.frameId&&this.propType!=="textSelector"?value:(this.propType==="textSelector"&&(textIndex=this.textIndex,textTotal=this.textTotal,selectorValue=this.selectorValue),thisLayer||(text=elem.layerInterface.text,thisLayer=elem.layerInterface,thisComp=elem.comp.compInterface,toWorld=thisLayer.toWorld.bind(thisLayer),fromWorld=thisLayer.fromWorld.bind(thisLayer),fromComp=thisLayer.fromComp.bind(thisLayer),toComp=thisLayer.toComp.bind(thisLayer),mask=thisLayer.mask?thisLayer.mask.bind(thisLayer):null,fromCompToSurface=fromComp),transform||(transform=elem.layerInterface("ADBE Transform Group"),$bm_transform=transform,transform&&(anchorPoint=transform.anchorPoint)),elemType===4&&!content&&(content=thisLayer("ADBE Root Vectors Group")),effect||(effect=thisLayer(4)),hasParent=!!(elem.hierarchy&&elem.hierarchy.length),hasParent&&!parent&&(parent=elem.hierarchy[0].layerInterface),time=this.comp.renderedFrame/this.comp.globalData.frameRate,_needsRandom&&seedRandom(randSeed+time),needsVelocity&&(velocity=velocityAtTime(time)),expression_function(),this.frameExpressionId=elem.globalData.frameId,scoped_bm_rt=scoped_bm_rt.propType===propTypes.SHAPE?scoped_bm_rt.v:scoped_bm_rt,scoped_bm_rt)}return executeExpression.__preventDeadCodeRemoval=[$bm_transform,anchorPoint,time,velocity,inPoint,outPoint,width,height,name,loop_in,loop_out,smooth,toComp,fromCompToSurface,toWorld,fromWorld,mask,position,rotation,scale,thisComp,numKeys,active,wiggle,loopInDuration,loopOutDuration,comp,lookAt,easeOut,easeIn,ease,nearestKey,key,text,textIndex,textTotal,selectorValue,framesToTime,timeToFrames,sourceRectAtTime,substring,substr,posterizeTime,index,globalData],executeExpression}return ob.initiateExpression=initiateExpression,ob.__preventDeadCodeRemoval=[window,document,XMLHttpRequest,fetch,frames,$bm_neg,add,$bm_sum,$bm_sub,$bm_mul,$bm_div,$bm_mod,clamp,radians_to_degrees,degreesToRadians,degrees_to_radians,normalize,rgbToHsl,hslToRgb,linear,random,createPath],ob}(),expressionHelpers=function(){function e(u,d,m){d.x&&(m.k=!0,m.x=!0,m.initiateExpression=ExpressionManager.initiateExpression,m.effectsSequence.push(m.initiateExpression(u,d,m).bind(m)))}function r(u){return u*=this.elem.globalData.frameRate,u-=this.offsetTime,u!==this._cachingAtTime.lastFrame&&(this._cachingAtTime.lastIndex=this._cachingAtTime.lastFrame<u?this._cachingAtTime.lastIndex:0,this._cachingAtTime.value=this.interpolateValue(u,this._cachingAtTime),this._cachingAtTime.lastFrame=u),this._cachingAtTime.value}function n(u){var d=-.01,m=this.getValueAtTime(u),b=this.getValueAtTime(u+d),A=0;if(m.length){var _;for(_=0;_<m.length;_+=1)A+=Math.pow(b[_]-m[_],2);A=Math.sqrt(A)*100}else A=0;return A}function a(u){if(this.vel!==void 0)return this.vel;var d=-.001,m=this.getValueAtTime(u),b=this.getValueAtTime(u+d),A;if(m.length){A=createTypedArray("float32",m.length);var _;for(_=0;_<m.length;_+=1)A[_]=(b[_]-m[_])/d}else A=(b-m)/d;return A}function l(){return this.pv}function c(u){this.propertyGroup=u}return{searchExpressions:e,getSpeedAtTime:n,getVelocityAtTime:a,getValueAtTime:r,getStaticValueAtTime:l,setGroupProperty:c}}();function addPropertyDecorator(){function e(T,C,S){if(!this.k||!this.keyframes)return this.pv;T=T?T.toLowerCase():"";var P=this.comp.renderedFrame,k=this.keyframes,x=k[k.length-1].t;if(P<=x)return this.pv;var E,g;S?(C?E=Math.abs(x-this.elem.comp.globalData.frameRate*C):E=Math.max(0,x-this.elem.data.ip),g=x-E):((!C||C>k.length-1)&&(C=k.length-1),g=k[k.length-1-C].t,E=x-g);var y,I,M;if(T==="pingpong"){var j=Math.floor((P-g)/E);if(j%2!=0)return this.getValueAtTime((E-(P-g)%E+g)/this.comp.globalData.frameRate,0)}else if(T==="offset"){var O=this.getValueAtTime(g/this.comp.globalData.frameRate,0),R=this.getValueAtTime(x/this.comp.globalData.frameRate,0),B=this.getValueAtTime(((P-g)%E+g)/this.comp.globalData.frameRate,0),G=Math.floor((P-g)/E);if(this.pv.length){for(M=new Array(O.length),I=M.length,y=0;y<I;y+=1)M[y]=(R[y]-O[y])*G+B[y];return M}return(R-O)*G+B}else if(T==="continue"){var V=this.getValueAtTime(x/this.comp.globalData.frameRate,0),Q=this.getValueAtTime((x-.001)/this.comp.globalData.frameRate,0);if(this.pv.length){for(M=new Array(V.length),I=M.length,y=0;y<I;y+=1)M[y]=V[y]+(V[y]-Q[y])*((P-x)/this.comp.globalData.frameRate)/5e-4;return M}return V+(V-Q)*((P-x)/.001)}return this.getValueAtTime(((P-g)%E+g)/this.comp.globalData.frameRate,0)}function r(T,C,S){if(!this.k)return this.pv;T=T?T.toLowerCase():"";var P=this.comp.renderedFrame,k=this.keyframes,x=k[0].t;if(P>=x)return this.pv;var E,g;S?(C?E=Math.abs(this.elem.comp.globalData.frameRate*C):E=Math.max(0,this.elem.data.op-x),g=x+E):((!C||C>k.length-1)&&(C=k.length-1),g=k[C].t,E=g-x);var y,I,M;if(T==="pingpong"){var j=Math.floor((x-P)/E);if(j%2==0)return this.getValueAtTime(((x-P)%E+x)/this.comp.globalData.frameRate,0)}else if(T==="offset"){var O=this.getValueAtTime(x/this.comp.globalData.frameRate,0),R=this.getValueAtTime(g/this.comp.globalData.frameRate,0),B=this.getValueAtTime((E-(x-P)%E+x)/this.comp.globalData.frameRate,0),G=Math.floor((x-P)/E)+1;if(this.pv.length){for(M=new Array(O.length),I=M.length,y=0;y<I;y+=1)M[y]=B[y]-(R[y]-O[y])*G;return M}return B-(R-O)*G}else if(T==="continue"){var V=this.getValueAtTime(x/this.comp.globalData.frameRate,0),Q=this.getValueAtTime((x+.001)/this.comp.globalData.frameRate,0);if(this.pv.length){for(M=new Array(V.length),I=M.length,y=0;y<I;y+=1)M[y]=V[y]+(V[y]-Q[y])*(x-P)/.001;return M}return V+(V-Q)*(x-P)/.001}return this.getValueAtTime((E-((x-P)%E+x))/this.comp.globalData.frameRate,0)}function n(T,C){if(!this.k)return this.pv;if(T=(T||.4)*.5,C=Math.floor(C||5),C<=1)return this.pv;var S=this.comp.renderedFrame/this.comp.globalData.frameRate,P=S-T,k=S+T,x=C>1?(k-P)/(C-1):1,E=0,g=0,y;this.pv.length?y=createTypedArray("float32",this.pv.length):y=0;for(var I;E<C;){if(I=this.getValueAtTime(P+E*x),this.pv.length)for(g=0;g<this.pv.length;g+=1)y[g]+=I[g];else y+=I;E+=1}if(this.pv.length)for(g=0;g<this.pv.length;g+=1)y[g]/=C;else y/=C;return y}function a(T){this._transformCachingAtTime||(this._transformCachingAtTime={v:new Matrix});var C=this._transformCachingAtTime.v;if(C.cloneFromProps(this.pre.props),this.appliedTransformations<1){var S=this.a.getValueAtTime(T);C.translate(-S[0]*this.a.mult,-S[1]*this.a.mult,S[2]*this.a.mult)}if(this.appliedTransformations<2){var P=this.s.getValueAtTime(T);C.scale(P[0]*this.s.mult,P[1]*this.s.mult,P[2]*this.s.mult)}if(this.sk&&this.appliedTransformations<3){var k=this.sk.getValueAtTime(T),x=this.sa.getValueAtTime(T);C.skewFromAxis(-k*this.sk.mult,x*this.sa.mult)}if(this.r&&this.appliedTransformations<4){var E=this.r.getValueAtTime(T);C.rotate(-E*this.r.mult)}else if(!this.r&&this.appliedTransformations<4){var g=this.rz.getValueAtTime(T),y=this.ry.getValueAtTime(T),I=this.rx.getValueAtTime(T),M=this.or.getValueAtTime(T);C.rotateZ(-g*this.rz.mult).rotateY(y*this.ry.mult).rotateX(I*this.rx.mult).rotateZ(-M[2]*this.or.mult).rotateY(M[1]*this.or.mult).rotateX(M[0]*this.or.mult)}if(this.data.p&&this.data.p.s){var j=this.px.getValueAtTime(T),O=this.py.getValueAtTime(T);if(this.data.p.z){var R=this.pz.getValueAtTime(T);C.translate(j*this.px.mult,O*this.py.mult,-R*this.pz.mult)}else C.translate(j*this.px.mult,O*this.py.mult,0)}else{var B=this.p.getValueAtTime(T);C.translate(B[0]*this.p.mult,B[1]*this.p.mult,-B[2]*this.p.mult)}return C}function l(){return this.v.clone(new Matrix)}var c=TransformPropertyFactory.getTransformProperty;TransformPropertyFactory.getTransformProperty=function(T,C,S){var P=c(T,C,S);return P.dynamicProperties.length?P.getValueAtTime=a.bind(P):P.getValueAtTime=l.bind(P),P.setGroupProperty=expressionHelpers.setGroupProperty,P};var u=PropertyFactory.getProp;PropertyFactory.getProp=function(T,C,S,P,k){var x=u(T,C,S,P,k);x.kf?x.getValueAtTime=expressionHelpers.getValueAtTime.bind(x):x.getValueAtTime=expressionHelpers.getStaticValueAtTime.bind(x),x.setGroupProperty=expressionHelpers.setGroupProperty,x.loopOut=e,x.loopIn=r,x.smooth=n,x.getVelocityAtTime=expressionHelpers.getVelocityAtTime.bind(x),x.getSpeedAtTime=expressionHelpers.getSpeedAtTime.bind(x),x.numKeys=C.a===1?C.k.length:0,x.propertyIndex=C.ix;var E=0;return S!==0&&(E=createTypedArray("float32",C.a===1?C.k[0].s.length:C.k.length)),x._cachingAtTime={lastFrame:initialDefaultFrame,lastIndex:0,value:E},expressionHelpers.searchExpressions(T,C,x),x.k&&k.addDynamicProperty(x),x};function d(T){return this._cachingAtTime||(this._cachingAtTime={shapeValue:shapePool.clone(this.pv),lastIndex:0,lastTime:initialDefaultFrame}),T*=this.elem.globalData.frameRate,T-=this.offsetTime,T!==this._cachingAtTime.lastTime&&(this._cachingAtTime.lastIndex=this._cachingAtTime.lastTime<T?this._caching.lastIndex:0,this._cachingAtTime.lastTime=T,this.interpolateShape(T,this._cachingAtTime.shapeValue,this._cachingAtTime)),this._cachingAtTime.shapeValue}var m=ShapePropertyFactory.getConstructorFunction(),b=ShapePropertyFactory.getKeyframedConstructorFunction();function A(){}A.prototype={vertices:function(C,S){this.k&&this.getValue();var P=this.v;S!==void 0&&(P=this.getValueAtTime(S,0));var k,x=P._length,E=P[C],g=P.v,y=createSizedArray(x);for(k=0;k<x;k+=1)C==="i"||C==="o"?y[k]=[E[k][0]-g[k][0],E[k][1]-g[k][1]]:y[k]=[E[k][0],E[k][1]];return y},points:function(C){return this.vertices("v",C)},inTangents:function(C){return this.vertices("i",C)},outTangents:function(C){return this.vertices("o",C)},isClosed:function(){return this.v.c},pointOnPath:function(C,S){var P=this.v;S!==void 0&&(P=this.getValueAtTime(S,0)),this._segmentsLength||(this._segmentsLength=bez.getSegmentsLength(P));for(var k=this._segmentsLength,x=k.lengths,E=k.totalLength*C,g=0,y=x.length,I=0,M;g<y;){if(I+x[g].addedLength>E){var j=g,O=P.c&&g===y-1?0:g+1,R=(E-I)/x[g].addedLength;M=bez.getPointInSegment(P.v[j],P.v[O],P.o[j],P.i[O],R,x[g]);break}else I+=x[g].addedLength;g+=1}return M||(M=P.c?[P.v[0][0],P.v[0][1]]:[P.v[P._length-1][0],P.v[P._length-1][1]]),M},vectorOnPath:function(C,S,P){C==1?C=this.v.c:C==0&&(C=.999);var k=this.pointOnPath(C,S),x=this.pointOnPath(C+.001,S),E=x[0]-k[0],g=x[1]-k[1],y=Math.sqrt(Math.pow(E,2)+Math.pow(g,2));if(y===0)return[0,0];var I=P==="tangent"?[E/y,g/y]:[-g/y,E/y];return I},tangentOnPath:function(C,S){return this.vectorOnPath(C,S,"tangent")},normalOnPath:function(C,S){return this.vectorOnPath(C,S,"normal")},setGroupProperty:expressionHelpers.setGroupProperty,getValueAtTime:expressionHelpers.getStaticValueAtTime},extendPrototype([A],m),extendPrototype([A],b),b.prototype.getValueAtTime=d,b.prototype.initiateExpression=ExpressionManager.initiateExpression;var _=ShapePropertyFactory.getShapeProp;ShapePropertyFactory.getShapeProp=function(T,C,S,P,k){var x=_(T,C,S,P,k);return x.propertyIndex=C.ix,x.lock=!1,S===3?expressionHelpers.searchExpressions(T,C.pt,x):S===4&&expressionHelpers.searchExpressions(T,C.ks,x),x.k&&T.addDynamicProperty(x),x}}function initialize$1(){addPropertyDecorator()}function addDecorator(){function e(){return this.data.d.x?(this.calculateExpression=ExpressionManager.initiateExpression.bind(this)(this.elem,this.data.d,this),this.addEffect(this.getExpressionValue.bind(this)),!0):null}TextProperty.prototype.getExpressionValue=function(r,n){var a=this.calculateExpression(n);if(r.t!==a){var l={};return this.copyData(l,r),l.t=a.toString(),l.__complete=!1,l}return r},TextProperty.prototype.searchProperty=function(){var r=this.searchKeyframes(),n=this.searchExpressions();return this.kf=r||n,this.kf},TextProperty.prototype.searchExpressions=e}function initialize(){addDecorator()}return registerRenderer("canvas",CanvasRenderer),registerRenderer("html",HybridRenderer),registerRenderer("svg",SVGRenderer),ShapeModifiers.registerModifier("tm",TrimModifier),ShapeModifiers.registerModifier("pb",PuckerAndBloatModifier),ShapeModifiers.registerModifier("rp",RepeaterModifier),ShapeModifiers.registerModifier("rd",RoundCornersModifier),setExpressionsPlugin(Expressions),initialize$1(),initialize(),registerEffect(20,SVGTintFilter,!0),registerEffect(21,SVGFillFilter,!0),registerEffect(22,SVGStrokeEffect,!1),registerEffect(23,SVGTritoneFilter,!0),registerEffect(24,SVGProLevelsFilter,!0),registerEffect(25,SVGDropShadowEffect,!0),registerEffect(28,SVGMatte3Effect,!1),registerEffect(29,SVGGaussianBlurEffect,!0),lottie})})(lottie$1,lottie$1.exports);var lottie=lottie$1.exports;function ownKeys$3(e,r){var n=Object.keys(e);if(Object.getOwnPropertySymbols){var a=Object.getOwnPropertySymbols(e);r&&(a=a.filter(function(l){return Object.getOwnPropertyDescriptor(e,l).enumerable})),n.push.apply(n,a)}return n}function _objectSpread2$1(e){for(var r=1;r<arguments.length;r++){var n=arguments[r]!=null?arguments[r]:{};r%2?ownKeys$3(Object(n),!0).forEach(function(a){_defineProperty$3(e,a,n[a])}):Object.getOwnPropertyDescriptors?Object.defineProperties(e,Object.getOwnPropertyDescriptors(n)):ownKeys$3(Object(n)).forEach(function(a){Object.defineProperty(e,a,Object.getOwnPropertyDescriptor(n,a))})}return e}function _defineProperty$3(e,r,n){return r in e?Object.defineProperty(e,r,{value:n,enumerable:!0,configurable:!0,writable:!0}):e[r]=n,e}function _objectWithoutPropertiesLoose$2(e,r){if(e==null)return{};var n={},a=Object.keys(e),l,c;for(c=0;c<a.length;c++)l=a[c],!(r.indexOf(l)>=0)&&(n[l]=e[l]);return n}function _objectWithoutProperties$1(e,r){if(e==null)return{};var n=_objectWithoutPropertiesLoose$2(e,r),a,l;if(Object.getOwnPropertySymbols){var c=Object.getOwnPropertySymbols(e);for(l=0;l<c.length;l++)a=c[l],!(r.indexOf(a)>=0)&&(!Object.prototype.propertyIsEnumerable.call(e,a)||(n[a]=e[a]))}return n}function _slicedToArray$2(e,r){return _arrayWithHoles$2(e)||_iterableToArrayLimit$2(e,r)||_unsupportedIterableToArray$2(e,r)||_nonIterableRest$2()}function _arrayWithHoles$2(e){if(Array.isArray(e))return e}function _iterableToArrayLimit$2(e,r){var n=e==null?null:typeof Symbol!="undefined"&&e[Symbol.iterator]||e["@@iterator"];if(n!=null){var a=[],l=!0,c=!1,u,d;try{for(n=n.call(e);!(l=(u=n.next()).done)&&(a.push(u.value),!(r&&a.length===r));l=!0);}catch(m){c=!0,d=m}finally{try{!l&&n.return!=null&&n.return()}finally{if(c)throw d}}return a}}function _unsupportedIterableToArray$2(e,r){if(!!e){if(typeof e=="string")return _arrayLikeToArray$2(e,r);var n=Object.prototype.toString.call(e).slice(8,-1);if(n==="Object"&&e.constructor&&(n=e.constructor.name),n==="Map"||n==="Set")return Array.from(e);if(n==="Arguments"||/^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n))return _arrayLikeToArray$2(e,r)}}function _arrayLikeToArray$2(e,r){(r==null||r>e.length)&&(r=e.length);for(var n=0,a=new Array(r);n<r;n++)a[n]=e[n];return a}function _nonIterableRest$2(){throw new TypeError(`Invalid attempt to destructure non-iterable instance.
In order to be iterable, non-array objects must have a [Symbol.iterator]() method.`)}var _excluded$1=["animationData","loop","autoplay","initialSegment","onComplete","onLoopComplete","onEnterFrame","onSegmentStart","onConfigReady","onDataReady","onDataFailed","onLoadedImages","onDOMLoaded","onDestroy","lottieRef","renderer","name","assetsPath","rendererSettings"],useLottie=function e(r,n){var a=r.animationData,l=r.loop,c=r.autoplay,u=r.initialSegment,d=r.onComplete,m=r.onLoopComplete,b=r.onEnterFrame,A=r.onSegmentStart,_=r.onConfigReady,T=r.onDataReady,C=r.onDataFailed,S=r.onLoadedImages,P=r.onDOMLoaded,k=r.onDestroy;r.lottieRef,r.renderer,r.name,r.assetsPath,r.rendererSettings;var x=_objectWithoutProperties$1(r,_excluded$1),E=react.exports.useState(!1),g=_slicedToArray$2(E,2),y=g[0],I=g[1],M=react.exports.useRef(),j=react.exports.useRef(null),O=function(){var K;(K=M.current)===null||K===void 0||K.play()},R=function(){var K;(K=M.current)===null||K===void 0||K.stop()},B=function(){var K;(K=M.current)===null||K===void 0||K.pause()},G=function(K){var Z;(Z=M.current)===null||Z===void 0||Z.setSpeed(K)},V=function(K,Z){var Y;(Y=M.current)===null||Y===void 0||Y.goToAndPlay(K,Z)},Q=function(K,Z){var Y;(Y=M.current)===null||Y===void 0||Y.goToAndStop(K,Z)},W=function(K){var Z;(Z=M.current)===null||Z===void 0||Z.setDirection(K)},q=function(K,Z){var Y;(Y=M.current)===null||Y===void 0||Y.playSegments(K,Z)},L=function(K){var Z;(Z=M.current)===null||Z===void 0||Z.setSubframe(K)},D=function(K){var Z;return(Z=M.current)===null||Z===void 0?void 0:Z.getDuration(K)},F=function(){var K;(K=M.current)===null||K===void 0||K.destroy()},$=function(){var K=arguments.length>0&&arguments[0]!==void 0?arguments[0]:{},Z;if(!!j.current){(Z=M.current)===null||Z===void 0||Z.destroy();var Y=_objectSpread2$1(_objectSpread2$1(_objectSpread2$1({},r),K),{},{container:j.current});M.current=lottie.loadAnimation(Y),I(!!M.current)}};react.exports.useEffect(function(){$()},[a]),react.exports.useEffect(function(){!M.current||(M.current.loop=!!l,l&&M.current.isPaused&&M.current.play())},[l]),react.exports.useEffect(function(){!M.current||(M.current.autoplay=!!c)},[c]),react.exports.useEffect(function(){if(!!M.current){if(!u){M.current.resetSegments(!1);return}!Array.isArray(u)||!u.length||((M.current.currentRawFrame<u[0]||M.current.currentRawFrame>u[1])&&(M.current.currentRawFrame=u[0]),M.current.setSegment(u[0],u[1]))}},[u]),react.exports.useEffect(function(){var z=[{name:"complete",handler:d},{name:"loopComplete",handler:m},{name:"enterFrame",handler:b},{name:"segmentStart",handler:A},{name:"config_ready",handler:_},{name:"data_ready",handler:T},{name:"data_failed",handler:C},{name:"loaded_images",handler:S},{name:"DOMLoaded",handler:P},{name:"destroy",handler:k}],K=z.filter(function(Y){return Y.handler!=null});if(!!K.length){var Z=K.map(function(Y){var le;return(le=M.current)===null||le===void 0||le.addEventListener(Y.name,Y.handler),function(){var re;(re=M.current)===null||re===void 0||re.removeEventListener(Y.name,Y.handler)}});return function(){Z.forEach(function(Y){return Y()})}}},[d,m,b,A,_,T,C,S,P,k]);var U=jsx("div",N({},_objectSpread2$1({style:n,ref:j},x)));return{View:U,play:O,stop:R,pause:B,setSpeed:G,goToAndStop:Q,goToAndPlay:V,setDirection:W,playSegments:q,setSubframe:L,getDuration:D,destroy:F,animationContainerRef:j,animationLoaded:y,animationItem:M.current}};const v="5.5.8",fr=60,ip=0,op=110,w=144,h=105,nm="typing indicator",ddd=0,assets=[],layers=[{ddd:0,ind:1,ty:3,nm:"\u25BD Dots",sr:1,ks:{o:{a:0,k:100,ix:11},r:{a:0,k:0,ix:10},p:{a:0,k:[72,51,0],ix:2},a:{a:0,k:[36,9,0],ix:1},s:{a:0,k:[100,100,100],ix:6}},ao:0,ip:0,op:110,st:0,bm:0},{ddd:0,ind:2,ty:4,nm:"dot 1",parent:1,sr:1,ks:{o:{a:0,k:100,ix:11},r:{a:0,k:0,ix:10},p:{a:1,k:[{i:{x:.667,y:1},o:{x:.333,y:0},t:0,s:[9,9,0],to:[0,0,0],ti:[0,0,0]},{i:{x:.667,y:1},o:{x:.333,y:0},t:3.053,s:[9,10,0],to:[0,0,0],ti:[0,0,0]},{i:{x:.667,y:1},o:{x:.333,y:0},t:15.273,s:[9,-9,0],to:[0,0,0],ti:[0,0,0]},{i:{x:.667,y:1},o:{x:.333,y:0},t:25.174,s:[9,12,0],to:[0,0,0],ti:[0,0,0]},{t:32.744140625,s:[9,9,0]}],ix:2},a:{a:0,k:[0,0,0],ix:1},s:{a:0,k:[100,100,100],ix:6}},ao:0,shapes:[{ty:"gr",it:[{d:1,ty:"el",s:{a:0,k:[6,6],ix:2},p:{a:0,k:[0,0],ix:3},nm:"Ellipse Path 1",mn:"ADBE Vector Shape - Ellipse",hd:!1},{ty:"fl",c:{a:1,k:[{i:{x:[.472],y:[1]},o:{x:[1],y:[0]},t:0,s:[.690196078431,.725490196078,.752941176471,1]},{i:{x:[.49],y:[1]},o:{x:[1],y:[0]},t:15.273,s:[.349019616842,.392156869173,.427450984716,1]},{t:32.072265625,s:[.690196078431,.725490196078,.752941176471,1]}],ix:4},o:{a:0,k:100,ix:5},r:1,bm:0,nm:"Fill 1",mn:"ADBE Vector Graphic - Fill",hd:!1},{ty:"tr",p:{a:0,k:[0,0],ix:2},a:{a:0,k:[0,0],ix:1},s:{a:0,k:[300,300],ix:3},r:{a:0,k:0,ix:6},o:{a:0,k:100,ix:7},sk:{a:0,k:0,ix:4},sa:{a:0,k:0,ix:5},nm:"Transform"}],nm:"dot 1",np:2,cix:2,bm:0,ix:1,mn:"ADBE Vector Group",hd:!1}],ip:0,op:110,st:0,bm:0},{ddd:0,ind:3,ty:4,nm:"dot 2",parent:1,sr:1,ks:{o:{a:0,k:100,ix:11},r:{a:0,k:0,ix:10},p:{a:1,k:[{i:{x:.667,y:1},o:{x:.333,y:0},t:10.691,s:[36,9,0],to:[0,0,0],ti:[0,0,0]},{i:{x:.667,y:1},o:{x:.333,y:0},t:15.273,s:[36,10,0],to:[0,0,0],ti:[0,0,0]},{i:{x:.667,y:1},o:{x:.333,y:0},t:25.963,s:[36,-9,0],to:[0,0,0],ti:[0,0,0]},{i:{x:.667,y:1},o:{x:.333,y:0},t:36.984,s:[36,14.326,0],to:[0,0,0],ti:[0,0,0]},{t:43.97265625,s:[36,9,0]}],ix:2},a:{a:0,k:[0,0,0],ix:1},s:{a:0,k:[100,100,100],ix:6}},ao:0,shapes:[{ty:"gr",it:[{d:1,ty:"el",s:{a:0,k:[6,6],ix:2},p:{a:0,k:[0,0],ix:3},nm:"Ellipse Path 1",mn:"ADBE Vector Shape - Ellipse",hd:!1},{ty:"fl",c:{a:1,k:[{i:{x:[.472],y:[1]},o:{x:[1],y:[0]},t:10.691,s:[.690196078431,.725490196078,.752941176471,1]},{i:{x:[.49],y:[1]},o:{x:[1],y:[0]},t:25.963,s:[.349019616842,.392156869173,.427450984716,1]},{t:42.763671875,s:[.690196078431,.725490196078,.752941176471,1]}],ix:4},o:{a:0,k:100,ix:5},r:1,bm:0,nm:"Fill 1",mn:"ADBE Vector Graphic - Fill",hd:!1},{ty:"tr",p:{a:0,k:[0,0],ix:2},a:{a:0,k:[0,0],ix:1},s:{a:0,k:[300,300],ix:3},r:{a:0,k:0,ix:6},o:{a:0,k:100,ix:7},sk:{a:0,k:0,ix:4},sa:{a:0,k:0,ix:5},nm:"Transform"}],nm:"dot 2",np:2,cix:2,bm:0,ix:1,mn:"ADBE Vector Group",hd:!1}],ip:0,op:110,st:0,bm:0},{ddd:0,ind:4,ty:4,nm:"dot 3",parent:1,sr:1,ks:{o:{a:0,k:100,ix:11},r:{a:0,k:0,ix:10},p:{a:1,k:[{i:{x:.667,y:1},o:{x:.333,y:0},t:21.383,s:[63,9,0],to:[0,0,0],ti:[0,0,0]},{i:{x:.667,y:1},o:{x:.333,y:0},t:25.963,s:[63,10,0],to:[0,0,0],ti:[0,0,0]},{i:{x:.667,y:1},o:{x:.333,y:0},t:38.184,s:[63,-9,0],to:[0,0,0],ti:[0,0,0]},{i:{x:.667,y:1},o:{x:.333,y:0},t:49.594,s:[63,13.139,0],to:[0,0,0],ti:[0,0,0]},{t:56,s:[63,9,0]}],ix:2},a:{a:0,k:[0,0,0],ix:1},s:{a:0,k:[100,100,100],ix:6}},ao:0,shapes:[{ty:"gr",it:[{d:1,ty:"el",s:{a:0,k:[6,6],ix:2},p:{a:0,k:[0,0],ix:3},nm:"Ellipse Path 1",mn:"ADBE Vector Shape - Ellipse",hd:!1},{ty:"fl",c:{a:1,k:[{i:{x:[.472],y:[1]},o:{x:[1],y:[0]},t:21.383,s:[.690196078431,.725490196078,.752941176471,1]},{i:{x:[.49],y:[1]},o:{x:[1],y:[0]},t:36.656,s:[.349019616842,.392156869173,.427450984716,1]},{t:53.45703125,s:[.690196078431,.725490196078,.752941176471,1]}],ix:4},o:{a:0,k:100,ix:5},r:1,bm:0,nm:"Fill 1",mn:"ADBE Vector Graphic - Fill",hd:!1},{ty:"tr",p:{a:0,k:[0,0],ix:2},a:{a:0,k:[0,0],ix:1},s:{a:0,k:[300,300],ix:3},r:{a:0,k:0,ix:6},o:{a:0,k:100,ix:7},sk:{a:0,k:0,ix:4},sa:{a:0,k:0,ix:5},nm:"Transform"}],nm:"dot 3",np:2,cix:2,bm:0,ix:1,mn:"ADBE Vector Group",hd:!1}],ip:0,op:110,st:0,bm:0}],markers=[];var typingJson={v,fr,ip,op,w,h,nm,ddd,assets,layers,markers};const Typing=()=>{const e={animationData:typingJson,loop:!0,autoplay:!0},{View:r}=useLottie(e);return r},GroupAvatar=({groupInfo:e})=>jsxs(SimpleGrid,{columns:2,spacing:1,children:[e==null?void 0:e.slice(0,3).map((r,n)=>jsx(MyAvatar,{size:"2xs",src:r==null?void 0:r.avatar,name:r==null?void 0:r.fullName},n)),(e==null?void 0:e.length)>3&&jsx(Box,{display:"flex",alignItems:"center",justifyContent:"center",w:"16px",h:"16px",rounded:"full",bg:"orange.500",children:jsx(Text,{color:"white",fontSize:12,children:(e==null?void 0:e.length)-3<=9?`+${(e==null?void 0:e.length)-3}`:"..."})})]}),ConversationItem=({onClick:e,username:r,usernameClassName:n,lastMessage:a,seen:l,isActive:c,updatedAt:u,groupInfo:d,seenByCount:m,isTyping:b,avatarUrl:A,online:_,userTypingName:T})=>{const C=useColorModeValue("gray.500","slate.300"),S=()=>{if(m===(d==null?void 0:d.length)||!d)return null;if(d)return jsxs(Text,{color:C,fontSize:12,children:[m,"/",d==null?void 0:d.length]})};return jsxs(HStack,{_hover:{backgroundColor:"#e4e4e450"},w:"100%",align:"start",px:3,py:2,borderBottomWidth:1,cursor:"pointer",onClick:e,bg:useColorModeValue(c?"gray.100":"bg-white",c?"#1e293b":"#1e293b50"),children:[lodash.exports.isEmpty(d)?jsx(MyAvatar,{boxSize:"2em",src:A,name:r,children:jsx(AvatarBadge,{boxSize:"14px",bg:_?"green.500":"gray.300"})}):jsx(GroupAvatar,{groupInfo:d}),jsxs(Stack,{flex:1,spacing:1,children:[jsx(Text,{noOfLines:2,fontSize:15,lineHeight:"18px",color:useColorModeValue(l?"gray.700":"gray.900",l?"#ffffff50":"white"),className:`${n}`,textTransform:"capitalize",children:r}),b?jsxs(HStack,{flex:1,children:[jsx(Text,{noOfLines:1,fontSize:13,color:useColorModeValue("gray.700","white"),children:T==null?void 0:T.map(P=>P||"Someone is typing").join(",")}),jsx(Box,{display:"flex",alignItems:"center",justifyContent:"center",w:10,h:5,children:jsx(Typing,{})})]}):jsx(Text,{noOfLines:2,fontSize:12,lineHeight:"24px",fontWeight:l?"400":"600",color:useColorModeValue(l?"gray.700":"gray.900",l?"#ffffff50":"white"),children:a})]}),jsxs(VStack,{spacing:2,alignItems:"flex-end",minW:"50px",children:[jsx(Text,{color:useColorModeValue(l?"gray.700":"gray.900",l?"#ffffff50":"white"),fontSize:12,children:u}),jsxs(HStack,{justifyContent:"space-between",children:[l&&jsx(BsCheck2,{}),S()]})]})]})};var getRandomValues=typeof crypto!="undefined"&&crypto.getRandomValues&&crypto.getRandomValues.bind(crypto)||typeof msCrypto!="undefined"&&typeof msCrypto.getRandomValues=="function"&&msCrypto.getRandomValues.bind(msCrypto),rnds8=new Uint8Array(16);function rng(){if(!getRandomValues)throw new Error("crypto.getRandomValues() not supported. See https://github.com/uuidjs/uuid#getrandomvalues-not-supported");return getRandomValues(rnds8)}var byteToHex=[];for(var i=0;i<256;++i)byteToHex[i]=(i+256).toString(16).substr(1);function bytesToUuid(e,r){var n=r||0,a=byteToHex;return[a[e[n++]],a[e[n++]],a[e[n++]],a[e[n++]],"-",a[e[n++]],a[e[n++]],"-",a[e[n++]],a[e[n++]],"-",a[e[n++]],a[e[n++]],"-",a[e[n++]],a[e[n++]],a[e[n++]],a[e[n++]],a[e[n++]],a[e[n++]]].join("")}function v4(e,r,n){var a=r&&n||0;typeof e=="string"&&(r=e==="binary"?new Array(16):null,e=null),e=e||{};var l=e.random||(e.rng||rng)();if(l[6]=l[6]&15|64,l[8]=l[8]&63|128,r)for(var c=0;c<16;++c)r[a+c]=l[c];return r||bytesToUuid(l)}function _classCallCheck(e,r){if(!(e instanceof r))throw new TypeError("Cannot call a class as a function")}function _defineProperties(e,r){for(var n=0;n<r.length;n++){var a=r[n];a.enumerable=a.enumerable||!1,a.configurable=!0,"value"in a&&(a.writable=!0),Object.defineProperty(e,a.key,a)}}function _createClass(e,r,n){return r&&_defineProperties(e.prototype,r),n&&_defineProperties(e,n),e}function _defineProperty$2(e,r,n){return r in e?Object.defineProperty(e,r,{value:n,enumerable:!0,configurable:!0,writable:!0}):e[r]=n,e}function _extends$1(){return _extends$1=Object.assign||function(e){for(var r=1;r<arguments.length;r++){var n=arguments[r];for(var a in n)Object.prototype.hasOwnProperty.call(n,a)&&(e[a]=n[a])}return e},_extends$1.apply(this,arguments)}function ownKeys$2(e,r){var n=Object.keys(e);if(Object.getOwnPropertySymbols){var a=Object.getOwnPropertySymbols(e);r&&(a=a.filter(function(l){return Object.getOwnPropertyDescriptor(e,l).enumerable})),n.push.apply(n,a)}return n}function _objectSpread2(e){for(var r=1;r<arguments.length;r++){var n=arguments[r]!=null?arguments[r]:{};r%2?ownKeys$2(Object(n),!0).forEach(function(a){_defineProperty$2(e,a,n[a])}):Object.getOwnPropertyDescriptors?Object.defineProperties(e,Object.getOwnPropertyDescriptors(n)):ownKeys$2(Object(n)).forEach(function(a){Object.defineProperty(e,a,Object.getOwnPropertyDescriptor(n,a))})}return e}function _inherits(e,r){if(typeof r!="function"&&r!==null)throw new TypeError("Super expression must either be null or a function");e.prototype=Object.create(r&&r.prototype,{constructor:{value:e,writable:!0,configurable:!0}}),r&&_setPrototypeOf(e,r)}function _getPrototypeOf(e){return _getPrototypeOf=Object.setPrototypeOf?Object.getPrototypeOf:function(n){return n.__proto__||Object.getPrototypeOf(n)},_getPrototypeOf(e)}function _setPrototypeOf(e,r){return _setPrototypeOf=Object.setPrototypeOf||function(a,l){return a.__proto__=l,a},_setPrototypeOf(e,r)}function _assertThisInitialized(e){if(e===void 0)throw new ReferenceError("this hasn't been initialised - super() hasn't been called");return e}function _possibleConstructorReturn(e,r){return r&&(typeof r=="object"||typeof r=="function")?r:_assertThisInitialized(e)}var CONSTANT={GLOBAL:{HIDE:"__react_tooltip_hide_event",REBUILD:"__react_tooltip_rebuild_event",SHOW:"__react_tooltip_show_event"}},dispatchGlobalEvent=function e(r,n){var a;typeof window.CustomEvent=="function"?a=new window.CustomEvent(r,{detail:n}):(a=document.createEvent("Event"),a.initEvent(r,!1,!0,n)),window.dispatchEvent(a)};function staticMethods(e){e.hide=function(r){dispatchGlobalEvent(CONSTANT.GLOBAL.HIDE,{target:r})},e.rebuild=function(){dispatchGlobalEvent(CONSTANT.GLOBAL.REBUILD)},e.show=function(r){dispatchGlobalEvent(CONSTANT.GLOBAL.SHOW,{target:r})},e.prototype.globalRebuild=function(){this.mount&&(this.unbindListener(),this.bindListener())},e.prototype.globalShow=function(r){if(this.mount){var n=r&&r.detail&&r.detail.target&&!0||!1;this.showTooltip({currentTarget:n&&r.detail.target},!0)}},e.prototype.globalHide=function(r){if(this.mount){var n=r&&r.detail&&r.detail.target&&!0||!1;this.hideTooltip({currentTarget:n&&r.detail.target},n)}}}function windowListener(e){e.prototype.bindWindowEvents=function(r){window.removeEventListener(CONSTANT.GLOBAL.HIDE,this.globalHide),window.addEventListener(CONSTANT.GLOBAL.HIDE,this.globalHide,!1),window.removeEventListener(CONSTANT.GLOBAL.REBUILD,this.globalRebuild),window.addEventListener(CONSTANT.GLOBAL.REBUILD,this.globalRebuild,!1),window.removeEventListener(CONSTANT.GLOBAL.SHOW,this.globalShow),window.addEventListener(CONSTANT.GLOBAL.SHOW,this.globalShow,!1),r&&(window.removeEventListener("resize",this.onWindowResize),window.addEventListener("resize",this.onWindowResize,!1))},e.prototype.unbindWindowEvents=function(){window.removeEventListener(CONSTANT.GLOBAL.HIDE,this.globalHide),window.removeEventListener(CONSTANT.GLOBAL.REBUILD,this.globalRebuild),window.removeEventListener(CONSTANT.GLOBAL.SHOW,this.globalShow),window.removeEventListener("resize",this.onWindowResize)},e.prototype.onWindowResize=function(){!this.mount||this.hideTooltip()}}var checkStatus=function e(r,n){var a=this.state.show,l=this.props.id,c=this.isCapture(n.currentTarget),u=n.currentTarget.getAttribute("currentItem");c||n.stopPropagation(),a&&u==="true"?r||this.hideTooltip(n):(n.currentTarget.setAttribute("currentItem","true"),setUntargetItems(n.currentTarget,this.getTargetArray(l)),this.showTooltip(n))},setUntargetItems=function e(r,n){for(var a=0;a<n.length;a++)r!==n[a]?n[a].setAttribute("currentItem","false"):n[a].setAttribute("currentItem","true")},customListeners={id:"9b69f92e-d3fe-498b-b1b4-c5e63a51b0cf",set:function e(r,n,a){if(this.id in r){var l=r[this.id];l[n]=a}else Object.defineProperty(r,this.id,{configurable:!0,value:_defineProperty$2({},n,a)})},get:function e(r,n){var a=r[this.id];if(a!==void 0)return a[n]}};function customEvent(e){e.prototype.isCustomEvent=function(r){var n=this.state.event;return n||!!r.getAttribute("data-event")},e.prototype.customBindListener=function(r){var n=this,a=this.state,l=a.event,c=a.eventOff,u=r.getAttribute("data-event")||l,d=r.getAttribute("data-event-off")||c;u.split(" ").forEach(function(m){r.removeEventListener(m,customListeners.get(r,m));var b=checkStatus.bind(n,d);customListeners.set(r,m,b),r.addEventListener(m,b,!1)}),d&&d.split(" ").forEach(function(m){r.removeEventListener(m,n.hideTooltip),r.addEventListener(m,n.hideTooltip,!1)})},e.prototype.customUnbindListener=function(r){var n=this.state,a=n.event,l=n.eventOff,c=a||r.getAttribute("data-event"),u=l||r.getAttribute("data-event-off");r.removeEventListener(c,customListeners.get(r,a)),u&&r.removeEventListener(u,this.hideTooltip)}}function isCapture(e){e.prototype.isCapture=function(r){return r&&r.getAttribute("data-iscapture")==="true"||this.props.isCapture||!1}}function getEffect(e){e.prototype.getEffect=function(r){var n=r.getAttribute("data-effect");return n||this.props.effect||"float"}}var makeProxy=function e(r){var n={};for(var a in r)typeof r[a]=="function"?n[a]=r[a].bind(r):n[a]=r[a];return n},bodyListener=function e(r,n,a){var l=n.respectEffect,c=l===void 0?!1:l,u=n.customEvent,d=u===void 0?!1:u,m=this.props.id,b=a.target.getAttribute("data-tip")||null,A=a.target.getAttribute("data-for")||null,_=a.target;if(!(this.isCustomEvent(_)&&!d)){var T=m==null&&A==null||A===m;if(b!=null&&(!c||this.getEffect(_)==="float")&&T){var C=makeProxy(a);C.currentTarget=_,r(C)}}},findCustomEvents=function e(r,n){var a={};return r.forEach(function(l){var c=l.getAttribute(n);c&&c.split(" ").forEach(function(u){return a[u]=!0})}),a},getBody=function e(){return document.getElementsByTagName("body")[0]};function bodyMode(e){e.prototype.isBodyMode=function(){return!!this.props.bodyMode},e.prototype.bindBodyListener=function(r){var n=this,a=this.state,l=a.event,c=a.eventOff,u=a.possibleCustomEvents,d=a.possibleCustomEventsOff,m=getBody(),b=findCustomEvents(r,"data-event"),A=findCustomEvents(r,"data-event-off");l!=null&&(b[l]=!0),c!=null&&(A[c]=!0),u.split(" ").forEach(function(P){return b[P]=!0}),d.split(" ").forEach(function(P){return A[P]=!0}),this.unbindBodyListener(m);var _=this.bodyModeListeners={};l==null&&(_.mouseover=bodyListener.bind(this,this.showTooltip,{}),_.mousemove=bodyListener.bind(this,this.updateTooltip,{respectEffect:!0}),_.mouseout=bodyListener.bind(this,this.hideTooltip,{}));for(var T in b)_[T]=bodyListener.bind(this,function(P){var k=P.currentTarget.getAttribute("data-event-off")||c;checkStatus.call(n,k,P)},{customEvent:!0});for(var C in A)_[C]=bodyListener.bind(this,this.hideTooltip,{customEvent:!0});for(var S in _)m.addEventListener(S,_[S])},e.prototype.unbindBodyListener=function(r){r=r||getBody();var n=this.bodyModeListeners;for(var a in n)r.removeEventListener(a,n[a])}}var getMutationObserverClass=function e(){return window.MutationObserver||window.WebKitMutationObserver||window.MozMutationObserver};function trackRemoval(e){e.prototype.bindRemovalTracker=function(){var r=this,n=getMutationObserverClass();if(n!=null){var a=new n(function(l){for(var c=0;c<l.length;c++)for(var u=l[c],d=0;d<u.removedNodes.length;d++){var m=u.removedNodes[d];if(m===r.state.currentTarget){r.hideTooltip();return}}});a.observe(window.document,{childList:!0,subtree:!0}),this.removalTracker=a}},e.prototype.unbindRemovalTracker=function(){this.removalTracker&&(this.removalTracker.disconnect(),this.removalTracker=null)}}function getPosition(e,r,n,a,l,c,u){for(var d=getDimensions(n),m=d.width,b=d.height,A=getDimensions(r),_=A.width,T=A.height,C=getCurrentOffset(e,r,c),S=C.mouseX,P=C.mouseY,k=getDefaultPosition(c,_,T,m,b),x=calculateOffset(u),E=x.extraOffsetX,g=x.extraOffsetY,y=window.innerWidth,I=window.innerHeight,M=getParent(n),j=M.parentTop,O=M.parentLeft,R=function(ee){var oe=k[ee].l;return S+oe+E},B=function(ee){var oe=k[ee].r;return S+oe+E},G=function(ee){var oe=k[ee].t;return P+oe+g},V=function(ee){var oe=k[ee].b;return P+oe+g},Q=function(ee){return R(ee)<0},W=function(ee){return B(ee)>y},q=function(ee){return G(ee)<0},L=function(ee){return V(ee)>I},D=function(ee){return Q(ee)||W(ee)||q(ee)||L(ee)},F=function(ee){return!D(ee)},$=["top","bottom","left","right"],U=[],z=0;z<4;z++){var K=$[z];F(K)&&U.push(K)}var Z=!1,Y,le=l!==a;return F(l)&&le?(Z=!0,Y=l):U.length>0&&D(l)&&D(a)&&(Z=!0,Y=U[0]),Z?{isNewState:!0,newState:{place:Y}}:{isNewState:!1,position:{left:parseInt(R(a)-O,10),top:parseInt(G(a)-j,10)}}}var getDimensions=function e(r){var n=r.getBoundingClientRect(),a=n.height,l=n.width;return{height:parseInt(a,10),width:parseInt(l,10)}},getCurrentOffset=function e(r,n,a){var l=n.getBoundingClientRect(),c=l.top,u=l.left,d=getDimensions(n),m=d.width,b=d.height;return a==="float"?{mouseX:r.clientX,mouseY:r.clientY}:{mouseX:u+m/2,mouseY:c+b/2}},getDefaultPosition=function e(r,n,a,l,c){var u,d,m,b,A=3,_=2,T=12;return r==="float"?(u={l:-(l/2),r:l/2,t:-(c+A+_),b:-A},m={l:-(l/2),r:l/2,t:A+T,b:c+A+_+T},b={l:-(l+A+_),r:-A,t:-(c/2),b:c/2},d={l:A,r:l+A+_,t:-(c/2),b:c/2}):r==="solid"&&(u={l:-(l/2),r:l/2,t:-(a/2+c+_),b:-(a/2)},m={l:-(l/2),r:l/2,t:a/2,b:a/2+c+_},b={l:-(l+n/2+_),r:-(n/2),t:-(c/2),b:c/2},d={l:n/2,r:l+n/2+_,t:-(c/2),b:c/2}),{top:u,bottom:m,left:b,right:d}},calculateOffset=function e(r){var n=0,a=0;Object.prototype.toString.apply(r)==="[object String]"&&(r=JSON.parse(r.toString().replace(/'/g,'"')));for(var l in r)l==="top"?a-=parseInt(r[l],10):l==="bottom"?a+=parseInt(r[l],10):l==="left"?n-=parseInt(r[l],10):l==="right"&&(n+=parseInt(r[l],10));return{extraOffsetX:n,extraOffsetY:a}},getParent=function e(r){for(var n=r;n;){var a=window.getComputedStyle(n);if(a.getPropertyValue("transform")!=="none"||a.getPropertyValue("will-change")==="transform")break;n=n.parentElement}var l=n&&n.getBoundingClientRect().top||0,c=n&&n.getBoundingClientRect().left||0;return{parentTop:l,parentLeft:c}};function getTipContent(e,r,n,a){if(r)return r;if(n!=null)return n;if(n===null)return null;var l=/<br\s*\/?>/;return!a||a==="false"||!l.test(e)?e:e.split(l).map(function(c,u){return t.createElement("span",{key:u,className:"multi-line"},c)})}function parseAria(e){var r={};return Object.keys(e).filter(function(n){return/(^aria-\w+$|^role$)/.test(n)}).forEach(function(n){r[n]=e[n]}),r}function nodeListToArray(e){var r=e.length;return e.hasOwnProperty?Array.prototype.slice.call(e):new Array(r).fill().map(function(n){return e[n]})}function generateUUID(){return"t"+v4()}var baseCss=`.__react_component_tooltip {
  border-radius: 3px;
  display: inline-block;
  font-size: 13px;
  left: -999em;
  opacity: 0;
  padding: 8px 21px;
  position: fixed;
  pointer-events: none;
  transition: opacity 0.3s ease-out;
  top: -999em;
  visibility: hidden;
  z-index: 999;
}
.__react_component_tooltip.allow_hover, .__react_component_tooltip.allow_click {
  pointer-events: auto;
}
.__react_component_tooltip::before, .__react_component_tooltip::after {
  content: "";
  width: 0;
  height: 0;
  position: absolute;
}
.__react_component_tooltip.show {
  opacity: 0.9;
  margin-top: 0;
  margin-left: 0;
  visibility: visible;
}
.__react_component_tooltip.place-top::before {
  border-left: 10px solid transparent;
  border-right: 10px solid transparent;
  bottom: -8px;
  left: 50%;
  margin-left: -10px;
}
.__react_component_tooltip.place-bottom::before {
  border-left: 10px solid transparent;
  border-right: 10px solid transparent;
  top: -8px;
  left: 50%;
  margin-left: -10px;
}
.__react_component_tooltip.place-left::before {
  border-top: 6px solid transparent;
  border-bottom: 6px solid transparent;
  right: -8px;
  top: 50%;
  margin-top: -5px;
}
.__react_component_tooltip.place-right::before {
  border-top: 6px solid transparent;
  border-bottom: 6px solid transparent;
  left: -8px;
  top: 50%;
  margin-top: -5px;
}
.__react_component_tooltip .multi-line {
  display: block;
  padding: 2px 0;
  text-align: center;
}`,defaultColors={dark:{text:"#fff",background:"#222",border:"transparent",arrow:"#222"},success:{text:"#fff",background:"#8DC572",border:"transparent",arrow:"#8DC572"},warning:{text:"#fff",background:"#F0AD4E",border:"transparent",arrow:"#F0AD4E"},error:{text:"#fff",background:"#BE6464",border:"transparent",arrow:"#BE6464"},info:{text:"#fff",background:"#337AB7",border:"transparent",arrow:"#337AB7"},light:{text:"#222",background:"#fff",border:"transparent",arrow:"#fff"}};function getDefaultPopupColors(e){return defaultColors[e]?_objectSpread2({},defaultColors[e]):void 0}function generateTooltipStyle(e,r,n,a){return generateStyle(e,getPopupColors(r,n,a))}function generateStyle(e,r){var n=r.text,a=r.background,l=r.border,c=r.arrow;return`
  	.`.concat(e,` {
	    color: `).concat(n,`;
	    background: `).concat(a,`;
	    border: 1px solid `).concat(l,`;
  	}

  	.`).concat(e,`.place-top {
        margin-top: -10px;
    }
    .`).concat(e,`.place-top::before {
        border-top: 8px solid `).concat(l,`;
    }
    .`).concat(e,`.place-top::after {
        border-left: 8px solid transparent;
        border-right: 8px solid transparent;
        bottom: -6px;
        left: 50%;
        margin-left: -8px;
        border-top-color: `).concat(c,`;
        border-top-style: solid;
        border-top-width: 6px;
    }

    .`).concat(e,`.place-bottom {
        margin-top: 10px;
    }
    .`).concat(e,`.place-bottom::before {
        border-bottom: 8px solid `).concat(l,`;
    }
    .`).concat(e,`.place-bottom::after {
        border-left: 8px solid transparent;
        border-right: 8px solid transparent;
        top: -6px;
        left: 50%;
        margin-left: -8px;
        border-bottom-color: `).concat(c,`;
        border-bottom-style: solid;
        border-bottom-width: 6px;
    }

    .`).concat(e,`.place-left {
        margin-left: -10px;
    }
    .`).concat(e,`.place-left::before {
        border-left: 8px solid `).concat(l,`;
    }
    .`).concat(e,`.place-left::after {
        border-top: 5px solid transparent;
        border-bottom: 5px solid transparent;
        right: -6px;
        top: 50%;
        margin-top: -4px;
        border-left-color: `).concat(c,`;
        border-left-style: solid;
        border-left-width: 6px;
    }

    .`).concat(e,`.place-right {
        margin-left: 10px;
    }
    .`).concat(e,`.place-right::before {
        border-right: 8px solid `).concat(l,`;
    }
    .`).concat(e,`.place-right::after {
        border-top: 5px solid transparent;
        border-bottom: 5px solid transparent;
        left: -6px;
        top: 50%;
        margin-top: -4px;
        border-right-color: `).concat(c,`;
        border-right-style: solid;
        border-right-width: 6px;
    }
  `)}function getPopupColors(e,r,n){var a=e.text,l=e.background,c=e.border,u=e.arrow?e.arrow:e.background,d=getDefaultPopupColors(r);return a&&(d.text=a),l&&(d.background=l),n&&(c?d.border=c:d.border=r==="light"?"black":"white"),u&&(d.arrow=u),d}var commonjsGlobal=typeof globalThis!="undefined"?globalThis:typeof window!="undefined"?window:typeof global!="undefined"?global:typeof self!="undefined"?self:{};function createCommonjsModule(e,r){return r={exports:{}},e(r,r.exports),r.exports}var check=function(e){return e&&e.Math==Math&&e},global_1=check(typeof globalThis=="object"&&globalThis)||check(typeof window=="object"&&window)||check(typeof self=="object"&&self)||check(typeof commonjsGlobal=="object"&&commonjsGlobal)||function(){return this}()||Function("return this")(),fails=function(e){try{return!!e()}catch{return!0}},descriptors=!fails(function(){return Object.defineProperty({},1,{get:function(){return 7}})[1]!=7}),$propertyIsEnumerable={}.propertyIsEnumerable,getOwnPropertyDescriptor=Object.getOwnPropertyDescriptor,NASHORN_BUG=getOwnPropertyDescriptor&&!$propertyIsEnumerable.call({1:2},1),f=NASHORN_BUG?function e(r){var n=getOwnPropertyDescriptor(this,r);return!!n&&n.enumerable}:$propertyIsEnumerable,objectPropertyIsEnumerable={f},createPropertyDescriptor=function(e,r){return{enumerable:!(e&1),configurable:!(e&2),writable:!(e&4),value:r}},toString={}.toString,classofRaw=function(e){return toString.call(e).slice(8,-1)},split="".split,indexedObject=fails(function(){return!Object("z").propertyIsEnumerable(0)})?function(e){return classofRaw(e)=="String"?split.call(e,""):Object(e)}:Object,requireObjectCoercible=function(e){if(e==null)throw TypeError("Can't call method on "+e);return e},toIndexedObject=function(e){return indexedObject(requireObjectCoercible(e))},isObject$1=function(e){return typeof e=="object"?e!==null:typeof e=="function"},toPrimitive=function(e,r){if(!isObject$1(e))return e;var n,a;if(r&&typeof(n=e.toString)=="function"&&!isObject$1(a=n.call(e))||typeof(n=e.valueOf)=="function"&&!isObject$1(a=n.call(e))||!r&&typeof(n=e.toString)=="function"&&!isObject$1(a=n.call(e)))return a;throw TypeError("Can't convert object to primitive value")},toObject=function(e){return Object(requireObjectCoercible(e))},hasOwnProperty={}.hasOwnProperty,has=function e(r,n){return hasOwnProperty.call(toObject(r),n)},document$1=global_1.document,EXISTS=isObject$1(document$1)&&isObject$1(document$1.createElement),documentCreateElement=function(e){return EXISTS?document$1.createElement(e):{}},ie8DomDefine=!descriptors&&!fails(function(){return Object.defineProperty(documentCreateElement("div"),"a",{get:function(){return 7}}).a!=7}),$getOwnPropertyDescriptor=Object.getOwnPropertyDescriptor,f$1=descriptors?$getOwnPropertyDescriptor:function e(r,n){if(r=toIndexedObject(r),n=toPrimitive(n,!0),ie8DomDefine)try{return $getOwnPropertyDescriptor(r,n)}catch{}if(has(r,n))return createPropertyDescriptor(!objectPropertyIsEnumerable.f.call(r,n),r[n])},objectGetOwnPropertyDescriptor={f:f$1},anObject=function(e){if(!isObject$1(e))throw TypeError(String(e)+" is not an object");return e},$defineProperty=Object.defineProperty,f$2=descriptors?$defineProperty:function e(r,n,a){if(anObject(r),n=toPrimitive(n,!0),anObject(a),ie8DomDefine)try{return $defineProperty(r,n,a)}catch{}if("get"in a||"set"in a)throw TypeError("Accessors not supported");return"value"in a&&(r[n]=a.value),r},objectDefineProperty={f:f$2},createNonEnumerableProperty=descriptors?function(e,r,n){return objectDefineProperty.f(e,r,createPropertyDescriptor(1,n))}:function(e,r,n){return e[r]=n,e},setGlobal=function(e,r){try{createNonEnumerableProperty(global_1,e,r)}catch{global_1[e]=r}return r},SHARED="__core-js_shared__",store=global_1[SHARED]||setGlobal(SHARED,{}),sharedStore=store,functionToString=Function.toString;typeof sharedStore.inspectSource!="function"&&(sharedStore.inspectSource=function(e){return functionToString.call(e)});var inspectSource=sharedStore.inspectSource,WeakMap=global_1.WeakMap,nativeWeakMap=typeof WeakMap=="function"&&/native code/.test(inspectSource(WeakMap)),shared=createCommonjsModule(function(e){(e.exports=function(r,n){return sharedStore[r]||(sharedStore[r]=n!==void 0?n:{})})("versions",[]).push({version:"3.12.1",mode:"global",copyright:"\xA9 2021 Denis Pushkarev (zloirock.ru)"})}),id=0,postfix=Math.random(),uid=function(e){return"Symbol("+String(e===void 0?"":e)+")_"+(++id+postfix).toString(36)},keys=shared("keys"),sharedKey=function(e){return keys[e]||(keys[e]=uid(e))},hiddenKeys={},OBJECT_ALREADY_INITIALIZED="Object already initialized",WeakMap$1=global_1.WeakMap,set,get,has$1,enforce=function(e){return has$1(e)?get(e):set(e,{})},getterFor=function(e){return function(r){var n;if(!isObject$1(r)||(n=get(r)).type!==e)throw TypeError("Incompatible receiver, "+e+" required");return n}};if(nativeWeakMap||sharedStore.state){var store$1=sharedStore.state||(sharedStore.state=new WeakMap$1),wmget=store$1.get,wmhas=store$1.has,wmset=store$1.set;set=function(e,r){if(wmhas.call(store$1,e))throw new TypeError(OBJECT_ALREADY_INITIALIZED);return r.facade=e,wmset.call(store$1,e,r),r},get=function(e){return wmget.call(store$1,e)||{}},has$1=function(e){return wmhas.call(store$1,e)}}else{var STATE=sharedKey("state");hiddenKeys[STATE]=!0,set=function(e,r){if(has(e,STATE))throw new TypeError(OBJECT_ALREADY_INITIALIZED);return r.facade=e,createNonEnumerableProperty(e,STATE,r),r},get=function(e){return has(e,STATE)?e[STATE]:{}},has$1=function(e){return has(e,STATE)}}var internalState={set,get,has:has$1,enforce,getterFor},redefine=createCommonjsModule(function(e){var r=internalState.get,n=internalState.enforce,a=String(String).split("String");(e.exports=function(l,c,u,d){var m=d?!!d.unsafe:!1,b=d?!!d.enumerable:!1,A=d?!!d.noTargetGet:!1,_;if(typeof u=="function"&&(typeof c=="string"&&!has(u,"name")&&createNonEnumerableProperty(u,"name",c),_=n(u),_.source||(_.source=a.join(typeof c=="string"?c:""))),l===global_1){b?l[c]=u:setGlobal(c,u);return}else m?!A&&l[c]&&(b=!0):delete l[c];b?l[c]=u:createNonEnumerableProperty(l,c,u)})(Function.prototype,"toString",function(){return typeof this=="function"&&r(this).source||inspectSource(this)})}),path=global_1,aFunction=function(e){return typeof e=="function"?e:void 0},getBuiltIn=function(e,r){return arguments.length<2?aFunction(path[e])||aFunction(global_1[e]):path[e]&&path[e][r]||global_1[e]&&global_1[e][r]},ceil=Math.ceil,floor=Math.floor,toInteger=function(e){return isNaN(e=+e)?0:(e>0?floor:ceil)(e)},min=Math.min,toLength=function(e){return e>0?min(toInteger(e),9007199254740991):0},max=Math.max,min$1=Math.min,toAbsoluteIndex=function(e,r){var n=toInteger(e);return n<0?max(n+r,0):min$1(n,r)},createMethod=function(e){return function(r,n,a){var l=toIndexedObject(r),c=toLength(l.length),u=toAbsoluteIndex(a,c),d;if(e&&n!=n){for(;c>u;)if(d=l[u++],d!=d)return!0}else for(;c>u;u++)if((e||u in l)&&l[u]===n)return e||u||0;return!e&&-1}},arrayIncludes={includes:createMethod(!0),indexOf:createMethod(!1)},indexOf=arrayIncludes.indexOf,objectKeysInternal=function(e,r){var n=toIndexedObject(e),a=0,l=[],c;for(c in n)!has(hiddenKeys,c)&&has(n,c)&&l.push(c);for(;r.length>a;)has(n,c=r[a++])&&(~indexOf(l,c)||l.push(c));return l},enumBugKeys=["constructor","hasOwnProperty","isPrototypeOf","propertyIsEnumerable","toLocaleString","toString","valueOf"],hiddenKeys$1=enumBugKeys.concat("length","prototype"),f$3=Object.getOwnPropertyNames||function e(r){return objectKeysInternal(r,hiddenKeys$1)},objectGetOwnPropertyNames={f:f$3},f$4=Object.getOwnPropertySymbols,objectGetOwnPropertySymbols={f:f$4},ownKeys$1$1=getBuiltIn("Reflect","ownKeys")||function e(r){var n=objectGetOwnPropertyNames.f(anObject(r)),a=objectGetOwnPropertySymbols.f;return a?n.concat(a(r)):n},copyConstructorProperties=function(e,r){for(var n=ownKeys$1$1(r),a=objectDefineProperty.f,l=objectGetOwnPropertyDescriptor.f,c=0;c<n.length;c++){var u=n[c];has(e,u)||a(e,u,l(r,u))}},replacement=/#|\.prototype\./,isForced=function(e,r){var n=data[normalize(e)];return n==POLYFILL?!0:n==NATIVE?!1:typeof r=="function"?fails(r):!!r},normalize=isForced.normalize=function(e){return String(e).replace(replacement,".").toLowerCase()},data=isForced.data={},NATIVE=isForced.NATIVE="N",POLYFILL=isForced.POLYFILL="P",isForced_1=isForced,getOwnPropertyDescriptor$1=objectGetOwnPropertyDescriptor.f,_export=function(e,r){var n=e.target,a=e.global,l=e.stat,c,u,d,m,b,A;if(a?u=global_1:l?u=global_1[n]||setGlobal(n,{}):u=(global_1[n]||{}).prototype,u)for(d in r){if(b=r[d],e.noTargetGet?(A=getOwnPropertyDescriptor$1(u,d),m=A&&A.value):m=u[d],c=isForced_1(a?d:n+(l?".":"#")+d,e.forced),!c&&m!==void 0){if(typeof b==typeof m)continue;copyConstructorProperties(b,m)}(e.sham||m&&m.sham)&&createNonEnumerableProperty(b,"sham",!0),redefine(u,d,b,e)}},aFunction$1=function(e){if(typeof e!="function")throw TypeError(String(e)+" is not a function");return e},functionBindContext=function(e,r,n){if(aFunction$1(e),r===void 0)return e;switch(n){case 0:return function(){return e.call(r)};case 1:return function(a){return e.call(r,a)};case 2:return function(a,l){return e.call(r,a,l)};case 3:return function(a,l,c){return e.call(r,a,l,c)}}return function(){return e.apply(r,arguments)}},isArray=Array.isArray||function e(r){return classofRaw(r)=="Array"},engineUserAgent=getBuiltIn("navigator","userAgent")||"",process=global_1.process,versions=process&&process.versions,v8=versions&&versions.v8,match,version;v8?(match=v8.split("."),version=match[0]<4?1:match[0]+match[1]):engineUserAgent&&(match=engineUserAgent.match(/Edge\/(\d+)/),(!match||match[1]>=74)&&(match=engineUserAgent.match(/Chrome\/(\d+)/),match&&(version=match[1])));var engineV8Version=version&&+version,nativeSymbol=!!Object.getOwnPropertySymbols&&!fails(function(){return!String(Symbol())||!Symbol.sham&&engineV8Version&&engineV8Version<41}),useSymbolAsUid=nativeSymbol&&!Symbol.sham&&typeof Symbol.iterator=="symbol",WellKnownSymbolsStore=shared("wks"),Symbol$1=global_1.Symbol,createWellKnownSymbol=useSymbolAsUid?Symbol$1:Symbol$1&&Symbol$1.withoutSetter||uid,wellKnownSymbol=function(e){return(!has(WellKnownSymbolsStore,e)||!(nativeSymbol||typeof WellKnownSymbolsStore[e]=="string"))&&(nativeSymbol&&has(Symbol$1,e)?WellKnownSymbolsStore[e]=Symbol$1[e]:WellKnownSymbolsStore[e]=createWellKnownSymbol("Symbol."+e)),WellKnownSymbolsStore[e]},SPECIES=wellKnownSymbol("species"),arraySpeciesCreate=function(e,r){var n;return isArray(e)&&(n=e.constructor,typeof n=="function"&&(n===Array||isArray(n.prototype))?n=void 0:isObject$1(n)&&(n=n[SPECIES],n===null&&(n=void 0))),new(n===void 0?Array:n)(r===0?0:r)},push=[].push,createMethod$1=function(e){var r=e==1,n=e==2,a=e==3,l=e==4,c=e==6,u=e==7,d=e==5||c;return function(m,b,A,_){for(var T=toObject(m),C=indexedObject(T),S=functionBindContext(b,A,3),P=toLength(C.length),k=0,x=_||arraySpeciesCreate,E=r?x(m,P):n||u?x(m,0):void 0,g,y;P>k;k++)if((d||k in C)&&(g=C[k],y=S(g,k,T),e))if(r)E[k]=y;else if(y)switch(e){case 3:return!0;case 5:return g;case 6:return k;case 2:push.call(E,g)}else switch(e){case 4:return!1;case 7:push.call(E,g)}return c?-1:a||l?l:E}},arrayIteration={forEach:createMethod$1(0),map:createMethod$1(1),filter:createMethod$1(2),some:createMethod$1(3),every:createMethod$1(4),find:createMethod$1(5),findIndex:createMethod$1(6),filterOut:createMethod$1(7)},objectKeys=Object.keys||function e(r){return objectKeysInternal(r,enumBugKeys)},objectDefineProperties=descriptors?Object.defineProperties:function e(r,n){anObject(r);for(var a=objectKeys(n),l=a.length,c=0,u;l>c;)objectDefineProperty.f(r,u=a[c++],n[u]);return r},html=getBuiltIn("document","documentElement"),GT=">",LT="<",PROTOTYPE="prototype",SCRIPT="script",IE_PROTO=sharedKey("IE_PROTO"),EmptyConstructor=function(){},scriptTag=function(e){return LT+SCRIPT+GT+e+LT+"/"+SCRIPT+GT},NullProtoObjectViaActiveX=function(e){e.write(scriptTag("")),e.close();var r=e.parentWindow.Object;return e=null,r},NullProtoObjectViaIFrame=function(){var e=documentCreateElement("iframe"),r="java"+SCRIPT+":",n;return e.style.display="none",html.appendChild(e),e.src=String(r),n=e.contentWindow.document,n.open(),n.write(scriptTag("document.F=Object")),n.close(),n.F},activeXDocument,NullProtoObject=function(){try{activeXDocument=document.domain&&new ActiveXObject("htmlfile")}catch{}NullProtoObject=activeXDocument?NullProtoObjectViaActiveX(activeXDocument):NullProtoObjectViaIFrame();for(var e=enumBugKeys.length;e--;)delete NullProtoObject[PROTOTYPE][enumBugKeys[e]];return NullProtoObject()};hiddenKeys[IE_PROTO]=!0;var objectCreate=Object.create||function e(r,n){var a;return r!==null?(EmptyConstructor[PROTOTYPE]=anObject(r),a=new EmptyConstructor,EmptyConstructor[PROTOTYPE]=null,a[IE_PROTO]=r):a=NullProtoObject(),n===void 0?a:objectDefineProperties(a,n)},UNSCOPABLES=wellKnownSymbol("unscopables"),ArrayPrototype=Array.prototype;ArrayPrototype[UNSCOPABLES]==null&&objectDefineProperty.f(ArrayPrototype,UNSCOPABLES,{configurable:!0,value:objectCreate(null)});var addToUnscopables=function(e){ArrayPrototype[UNSCOPABLES][e]=!0},$find=arrayIteration.find,FIND="find",SKIPS_HOLES=!0;FIND in[]&&Array(1)[FIND](function(){SKIPS_HOLES=!1});_export({target:"Array",proto:!0,forced:SKIPS_HOLES},{find:function e(r){return $find(this,r,arguments.length>1?arguments[1]:void 0)}});addToUnscopables(FIND);var _class,_class2,_temp,ReactTooltip=staticMethods(_class=windowListener(_class=customEvent(_class=isCapture(_class=getEffect(_class=bodyMode(_class=trackRemoval(_class=(_temp=_class2=function(e){_inherits(r,e),_createClass(r,null,[{key:"propTypes",get:function(){return{uuid:PropTypes.string,children:PropTypes.any,place:PropTypes.string,type:PropTypes.string,effect:PropTypes.string,offset:PropTypes.object,multiline:PropTypes.bool,border:PropTypes.bool,textColor:PropTypes.string,backgroundColor:PropTypes.string,borderColor:PropTypes.string,arrowColor:PropTypes.string,insecure:PropTypes.bool,class:PropTypes.string,className:PropTypes.string,id:PropTypes.string,html:PropTypes.bool,delayHide:PropTypes.number,delayUpdate:PropTypes.number,delayShow:PropTypes.number,event:PropTypes.string,eventOff:PropTypes.string,isCapture:PropTypes.bool,globalEventOff:PropTypes.string,getContent:PropTypes.any,afterShow:PropTypes.func,afterHide:PropTypes.func,overridePosition:PropTypes.func,disable:PropTypes.bool,scrollHide:PropTypes.bool,resizeHide:PropTypes.bool,wrapper:PropTypes.string,bodyMode:PropTypes.bool,possibleCustomEvents:PropTypes.string,possibleCustomEventsOff:PropTypes.string,clickable:PropTypes.bool}}}]);function r(n){var a;return _classCallCheck(this,r),a=_possibleConstructorReturn(this,_getPrototypeOf(r).call(this,n)),a.state={uuid:n.uuid||generateUUID(),place:n.place||"top",desiredPlace:n.place||"top",type:"dark",effect:"float",show:!1,border:!1,customColors:{},offset:{},extraClass:"",html:!1,delayHide:0,delayShow:0,event:n.event||null,eventOff:n.eventOff||null,currentEvent:null,currentTarget:null,ariaProps:parseAria(n),isEmptyTip:!1,disable:!1,possibleCustomEvents:n.possibleCustomEvents||"",possibleCustomEventsOff:n.possibleCustomEventsOff||"",originTooltip:null,isMultiline:!1},a.bind(["showTooltip","updateTooltip","hideTooltip","hideTooltipOnScroll","getTooltipContent","globalRebuild","globalShow","globalHide","onWindowResize","mouseOnToolTip"]),a.mount=!0,a.delayShowLoop=null,a.delayHideLoop=null,a.delayReshow=null,a.intervalUpdateContent=null,a}return _createClass(r,[{key:"bind",value:function(a){var l=this;a.forEach(function(c){l[c]=l[c].bind(l)})}},{key:"componentDidMount",value:function(){var a=this.props;a.insecure;var l=a.resizeHide;this.bindListener(),this.bindWindowEvents(l),this.injectStyles()}},{key:"componentWillUnmount",value:function(){this.mount=!1,this.clearTimer(),this.unbindListener(),this.removeScrollListener(this.state.currentTarget),this.unbindWindowEvents()}},{key:"injectStyles",value:function(){var a=this.tooltipRef;if(!!a){for(var l=a.parentNode;l.parentNode;)l=l.parentNode;var c;switch(l.constructor.name){case"Document":case"HTMLDocument":case void 0:c=l.head;break;case"ShadowRoot":default:c=l;break}if(!c.querySelector("style[data-react-tooltip]")){var u=document.createElement("style");u.textContent=baseCss,u.setAttribute("data-react-tooltip","true"),c.appendChild(u)}}}},{key:"mouseOnToolTip",value:function(){var a=this.state.show;return a&&this.tooltipRef?(this.tooltipRef.matches||(this.tooltipRef.msMatchesSelector?this.tooltipRef.matches=this.tooltipRef.msMatchesSelector:this.tooltipRef.matches=this.tooltipRef.mozMatchesSelector),this.tooltipRef.matches(":hover")):!1}},{key:"getTargetArray",value:function(a){var l=[],c;if(!a)c="[data-tip]:not([data-for])";else{var u=a.replace(/\\/g,"\\\\").replace(/"/g,'\\"');c='[data-tip][data-for="'.concat(u,'"]')}return nodeListToArray(document.getElementsByTagName("*")).filter(function(d){return d.shadowRoot}).forEach(function(d){l=l.concat(nodeListToArray(d.shadowRoot.querySelectorAll(c)))}),l.concat(nodeListToArray(document.querySelectorAll(c)))}},{key:"bindListener",value:function(){var a=this,l=this.props,c=l.id,u=l.globalEventOff,d=l.isCapture,m=this.getTargetArray(c);m.forEach(function(b){b.getAttribute("currentItem")===null&&b.setAttribute("currentItem","false"),a.unbindBasicListener(b),a.isCustomEvent(b)&&a.customUnbindListener(b)}),this.isBodyMode()?this.bindBodyListener(m):m.forEach(function(b){var A=a.isCapture(b),_=a.getEffect(b);if(a.isCustomEvent(b)){a.customBindListener(b);return}b.addEventListener("mouseenter",a.showTooltip,A),b.addEventListener("focus",a.showTooltip,A),_==="float"&&b.addEventListener("mousemove",a.updateTooltip,A),b.addEventListener("mouseleave",a.hideTooltip,A),b.addEventListener("blur",a.hideTooltip,A)}),u&&(window.removeEventListener(u,this.hideTooltip),window.addEventListener(u,this.hideTooltip,d)),this.bindRemovalTracker()}},{key:"unbindListener",value:function(){var a=this,l=this.props,c=l.id,u=l.globalEventOff;if(this.isBodyMode())this.unbindBodyListener();else{var d=this.getTargetArray(c);d.forEach(function(m){a.unbindBasicListener(m),a.isCustomEvent(m)&&a.customUnbindListener(m)})}u&&window.removeEventListener(u,this.hideTooltip),this.unbindRemovalTracker()}},{key:"unbindBasicListener",value:function(a){var l=this.isCapture(a);a.removeEventListener("mouseenter",this.showTooltip,l),a.removeEventListener("mousemove",this.updateTooltip,l),a.removeEventListener("mouseleave",this.hideTooltip,l)}},{key:"getTooltipContent",value:function(){var a=this.props,l=a.getContent,c=a.children,u;return l&&(Array.isArray(l)?u=l[0]&&l[0](this.state.originTooltip):u=l(this.state.originTooltip)),getTipContent(this.state.originTooltip,c,u,this.state.isMultiline)}},{key:"isEmptyTip",value:function(a){return typeof a=="string"&&a===""||a===null}},{key:"showTooltip",value:function(a,l){if(!!this.tooltipRef){if(l){var c=this.getTargetArray(this.props.id),u=c.some(function(j){return j===a.currentTarget});if(!u)return}var d=this.props,m=d.multiline,b=d.getContent,A=a.currentTarget.getAttribute("data-tip"),_=a.currentTarget.getAttribute("data-multiline")||m||!1,T=a instanceof window.FocusEvent||l,C=!0;a.currentTarget.getAttribute("data-scroll-hide")?C=a.currentTarget.getAttribute("data-scroll-hide")==="true":this.props.scrollHide!=null&&(C=this.props.scrollHide),a&&a.currentTarget&&a.currentTarget.setAttribute&&a.currentTarget.setAttribute("aria-describedby",this.state.uuid);var S=a.currentTarget.getAttribute("data-place")||this.props.place||"top",P=T&&"solid"||this.getEffect(a.currentTarget),k=a.currentTarget.getAttribute("data-offset")||this.props.offset||{},x=getPosition(a,a.currentTarget,this.tooltipRef,S,S,P,k);x.position&&this.props.overridePosition&&(x.position=this.props.overridePosition(x.position,a,a.currentTarget,this.tooltipRef,S,S,P,k));var E=x.isNewState?x.newState.place:S;this.clearTimer();var g=a.currentTarget,y=this.state.show?g.getAttribute("data-delay-update")||this.props.delayUpdate:0,I=this,M=function(){I.setState({originTooltip:A,isMultiline:_,desiredPlace:S,place:E,type:g.getAttribute("data-type")||I.props.type||"dark",customColors:{text:g.getAttribute("data-text-color")||I.props.textColor||null,background:g.getAttribute("data-background-color")||I.props.backgroundColor||null,border:g.getAttribute("data-border-color")||I.props.borderColor||null,arrow:g.getAttribute("data-arrow-color")||I.props.arrowColor||null},effect:P,offset:k,html:(g.getAttribute("data-html")?g.getAttribute("data-html")==="true":I.props.html)||!1,delayShow:g.getAttribute("data-delay-show")||I.props.delayShow||0,delayHide:g.getAttribute("data-delay-hide")||I.props.delayHide||0,delayUpdate:g.getAttribute("data-delay-update")||I.props.delayUpdate||0,border:(g.getAttribute("data-border")?g.getAttribute("data-border")==="true":I.props.border)||!1,extraClass:g.getAttribute("data-class")||I.props.class||I.props.className||"",disable:(g.getAttribute("data-tip-disable")?g.getAttribute("data-tip-disable")==="true":I.props.disable)||!1,currentTarget:g},function(){C&&I.addScrollListener(I.state.currentTarget),I.updateTooltip(a),b&&Array.isArray(b)&&(I.intervalUpdateContent=setInterval(function(){if(I.mount){var O=I.props.getContent,R=getTipContent(A,"",O[0](),_),B=I.isEmptyTip(R);I.setState({isEmptyTip:B}),I.updatePosition()}},b[1]))})};y?this.delayReshow=setTimeout(M,y):M()}}},{key:"updateTooltip",value:function(a){var l=this,c=this.state,u=c.delayShow,d=c.disable,m=this.props.afterShow,b=this.getTooltipContent(),A=a.currentTarget||a.target;if(!this.mouseOnToolTip()&&!(this.isEmptyTip(b)||d)){var _=this.state.show?0:parseInt(u,10),T=function(){if(Array.isArray(b)&&b.length>0||b){var S=!l.state.show;l.setState({currentEvent:a,currentTarget:A,show:!0},function(){l.updatePosition(),S&&m&&m(a)})}};clearTimeout(this.delayShowLoop),_?this.delayShowLoop=setTimeout(T,_):T()}}},{key:"listenForTooltipExit",value:function(){var a=this.state.show;a&&this.tooltipRef&&this.tooltipRef.addEventListener("mouseleave",this.hideTooltip)}},{key:"removeListenerForTooltipExit",value:function(){var a=this.state.show;a&&this.tooltipRef&&this.tooltipRef.removeEventListener("mouseleave",this.hideTooltip)}},{key:"hideTooltip",value:function(a,l){var c=this,u=arguments.length>2&&arguments[2]!==void 0?arguments[2]:{isScroll:!1},d=this.state.disable,m=u.isScroll,b=m?0:this.state.delayHide,A=this.props.afterHide,_=this.getTooltipContent();if(!!this.mount&&!(this.isEmptyTip(_)||d)){if(l){var T=this.getTargetArray(this.props.id),C=T.some(function(P){return P===a.currentTarget});if(!C||!this.state.show)return}a&&a.currentTarget&&a.currentTarget.removeAttribute&&a.currentTarget.removeAttribute("aria-describedby");var S=function(){var k=c.state.show;if(c.mouseOnToolTip()){c.listenForTooltipExit();return}c.removeListenerForTooltipExit(),c.setState({show:!1},function(){c.removeScrollListener(c.state.currentTarget),k&&A&&A(a)})};this.clearTimer(),b?this.delayHideLoop=setTimeout(S,parseInt(b,10)):S()}}},{key:"hideTooltipOnScroll",value:function(a,l){this.hideTooltip(a,l,{isScroll:!0})}},{key:"addScrollListener",value:function(a){var l=this.isCapture(a);window.addEventListener("scroll",this.hideTooltipOnScroll,l)}},{key:"removeScrollListener",value:function(a){var l=this.isCapture(a);window.removeEventListener("scroll",this.hideTooltipOnScroll,l)}},{key:"updatePosition",value:function(){var a=this,l=this.state,c=l.currentEvent,u=l.currentTarget,d=l.place,m=l.desiredPlace,b=l.effect,A=l.offset,_=this.tooltipRef,T=getPosition(c,u,_,d,m,b,A);if(T.position&&this.props.overridePosition&&(T.position=this.props.overridePosition(T.position,c,u,_,d,m,b,A)),T.isNewState)return this.setState(T.newState,function(){a.updatePosition()});_.style.left=T.position.left+"px",_.style.top=T.position.top+"px"}},{key:"clearTimer",value:function(){clearTimeout(this.delayShowLoop),clearTimeout(this.delayHideLoop),clearTimeout(this.delayReshow),clearInterval(this.intervalUpdateContent)}},{key:"hasCustomColors",value:function(){var a=this;return Boolean(Object.keys(this.state.customColors).find(function(l){return l!=="border"&&a.state.customColors[l]})||this.state.border&&this.state.customColors.border)}},{key:"render",value:function(){var a=this,l=this.state,c=l.extraClass,u=l.html,d=l.ariaProps,m=l.disable,b=l.uuid,A=this.getTooltipContent(),_=this.isEmptyTip(A),T=generateTooltipStyle(this.state.uuid,this.state.customColors,this.state.type,this.state.border),C="__react_component_tooltip"+" ".concat(this.state.uuid)+(this.state.show&&!m&&!_?" show":"")+(this.state.border?" border":"")+" place-".concat(this.state.place)+" type-".concat(this.hasCustomColors()?"custom":this.state.type)+(this.props.delayUpdate?" allow_hover":"")+(this.props.clickable?" allow_click":""),S=this.props.wrapper;r.supportedWrappers.indexOf(S)<0&&(S=r.defaultProps.wrapper);var P=[C,c].filter(Boolean).join(" ");if(u){var k="".concat(A,`
<style aria-hidden="true">`).concat(T,"</style>");return t.createElement(S,_extends$1({className:"".concat(P),id:this.props.id||b,ref:function(E){return a.tooltipRef=E}},d,{"data-id":"tooltip",dangerouslySetInnerHTML:{__html:k}}))}else return t.createElement(S,_extends$1({className:"".concat(P),id:this.props.id||b},d,{ref:function(E){return a.tooltipRef=E},"data-id":"tooltip"}),t.createElement("style",{dangerouslySetInnerHTML:{__html:T},"aria-hidden":"true"}),A)}}],[{key:"getDerivedStateFromProps",value:function(a,l){var c=l.ariaProps,u=parseAria(a),d=Object.keys(u).some(function(m){return u[m]!==c[m]});return d?_objectSpread2({},l,{ariaProps:u}):null}}]),r}(t.Component),_defineProperty$2(_class2,"defaultProps",{insecure:!0,resizeHide:!0,wrapper:"div",clickable:!1}),_defineProperty$2(_class2,"supportedWrappers",["div","span"]),_defineProperty$2(_class2,"displayName","ReactTooltip"),_temp))||_class)||_class)||_class)||_class)||_class)||_class)||_class;const UserCheckbox=e=>{const{isMultiple:r}=e,{getInputProps:n,getCheckboxProps:a}=r?useCheckbox(e):useRadio(e),l=n(),c=a();return jsxs(Box,{as:"label",rounded:"md",d:"block",bg:useColorModeValue("#1e293b","#1e293b50"),children:[jsx("input",H(N({},l),{style:{display:"none"}})),jsxs(Box,H(N({},c),{cursor:"pointer",opacity:.8,pos:"relative",children:[l.checked&&jsx(CheckCircleIcon,{color:"green.500",fontSize:"xl",pos:"absolute",top:2,right:2,zIndex:2}),e.children]}))]})},roleConfigs=[{key:"",value:"All"},{key:"zimians",value:"Zimians"},{key:"student",value:"Student"}];function useDebounce$1(e,r){const[n,a]=react.exports.useState(e);return react.exports.useEffect(()=>{const l=setTimeout(()=>{a(e)},r);return()=>{clearTimeout(l)}},[e,r]),n}const isReachBottom$2=e=>e.scrollHeight-e.scrollTop<=e.clientHeight+10,ListUser=({getCheckboxProps:e,listParticipants:r,usersInfo:n,isAddSupporter:a,value:l,setValue:c})=>{var O,R,B,G;const[u]=useUser(),[d,m]=react.exports.useState(""),[b,A]=react.exports.useState(!1),[_,T]=react.exports.useState(""),C=useDebounce$1(d,500),S=V=>{A(isReachBottom$2(V.target))},P=pickBy_1({roles:a?["EC"]:_==="all"?"":_==="zimians"?zimRoles:_==="student"?["HV"]:"",searchString:C,limit:20},V=>V),{data:k,loading:x,fetchMore:E}=useQuery(GET_LIST_ACCOUNT_CHAT,{variables:P}),g=(R=(O=k==null?void 0:k.getAccountChatPagination)==null?void 0:O.docs)==null?void 0:R.filter(V=>{var Q,W,q,L;return!(r==null?void 0:r.includes((W=(Q=V==null?void 0:V.user)==null?void 0:Q.id)==null?void 0:W.toString()))&&((L=(q=V==null?void 0:V.user)==null?void 0:q.id)==null?void 0:L.toString())!==(u==null?void 0:u.id)}),y=(B=k==null?void 0:k.getAccountChatPagination)==null?void 0:B.page,I=(G=k==null?void 0:k.getAccountChatPagination)==null?void 0:G.hasNextPage,{onLoadMore:M,isLoadingMore:j}=useLoadMore({variables:H(N({},P),{page:y+1}),fetchMore:E,hasNextPage:I,isNormalLoadMore:!1});return react.exports.useEffect(()=>{b&&I&&!j&&M()},[b,I,j]),jsxs(Stack,{spacing:3,children:[jsx(Text,{fontSize:14,children:"Danh s\xE1ch th\xE0nh vi\xEAn"}),jsx(HStack,{children:roleConfigs.map((V,Q)=>{const W=V.key===_;return jsx(Button$1,{onClick:()=>T(V.key),colorScheme:"teal",variant:W?"solid":"outline",disabled:a&&Q!==1,children:V.value},Q)})}),jsx(Wrap,{my:2,children:n==null?void 0:n.map((V,Q)=>jsxs(Tag,{size:"md",rounded:"md",children:[jsx(TagLabel,{children:V.full_name}),jsx(TagCloseButton,{onClick:()=>c(l==null?void 0:l.filter(W=>{var q;return W!==((q=V==null?void 0:V.id)==null?void 0:q.toString())}))})]},Q))}),jsx(TextInput,{type:"text",placeholder:"Nh\u1EADp t\xEAn ng\u01B0\u1EDDi d\xF9ng",size:"md",value:d,onChange:V=>m(V.target.value)}),jsxs(Box,{position:"relative",h:"250px",overflow:"auto",w:"full",flexGrow:1,onScroll:S,children:[jsxs(SimpleGrid,{columns:2,spacing:2,children:[(g==null?void 0:g.length)>0&&(g==null?void 0:g.map(({user:V})=>{const Q=e({value:V.id});return jsx(Box,{rounded:"lg",overflow:"hidden",children:jsx(UserCheckbox,H(N({isMultiple:!0},Q),{children:jsx(UserItemInfo,H(N({},V),{username:V==null?void 0:V.full_name,positionName:V==null?void 0:V.role_name,lastOnline:V==null?void 0:V.status_online,px:3,py:2}))}))},`${V.id}`)})),x&&jsxs(HStack,{rounded:"lg",bg:"blackAlpha.300",opacity:.5,pos:"absolute",top:0,bottom:0,right:0,left:0,justifyContent:"center",children:[jsx(Spinner,{color:"#319795",size:"lg"}),jsx(Text,{fontSize:"xl",color:"white",children:"\u0110ang t\u1EA3i ..."})]})]}),j&&jsxs(HStack,{w:"full",alignItems:"center",justifyContent:"center",py:2,children:[jsx(Spinner,{color:"#319795",size:"lg"}),jsx(Text,{fontSize:"xl",color:useColorModeValue("gray.700","white"),children:"\u0110ang t\u1EA3i ..."})]})]})]})},resetQueries=(e,r=null)=>{e.forEach(n=>{r==null||r.evict({id:"ROOT_QUERY",fieldName:n})})};function useResetQueries(){const e=useApolloClient();return r=>{resetQueries(r,e.cache)}}const useCreateOrUpdateConversation=({onClose:e,userId:r,isEdit:n,groupId:a,setUserId:l,isAddSupporter:c,isChannel:u})=>{const[d]=useUser(),m=useToast(),b=useResetQueries(),A=useStore(k=>k.setConversationId),[_,{loading:T}]=useMutation(CREATE_CONVERSATION,{onCompleted:k=>{const{createConversation:x}=k;x&&(m({title:"Th\xE0nh c\xF4ng!",description:`T\u1EA1o ${u?"channel":"nh\xF3m chat"} ${x==null?void 0:x.name} th\xE0nh c\xF4ng`,status:"success",duration:3e3,isClosable:!0}),e(),A(x==null?void 0:x.id)),l([]),b(["conversations","conversationDetail"])},onError:k=>{var x;m({title:`T\u1EA1o ${u?"channel":"nh\xF3m chat"} kh\xF4ng th\xE0nh c\xF4ng!`,description:`${(x=k==null?void 0:k.message)!=null?x:"L\u1ED7i"}`,status:"error",duration:3e3,isClosable:!0})}}),[C,{loading:S}]=useMutation(UPDATE_CONVERSATION,{onCompleted:k=>{const{updateConversation:x}=k;x&&m({title:"Th\xE0nh c\xF4ng!",description:c?"Th\xEAm EC h\u1ED7 tr\u1EE3 th\xE0nh c\xF4ng":`C\u1EADp nh\u1EADt ${u?"channel":"nh\xF3m chat"} ${x==null?void 0:x.name} th\xE0nh c\xF4ng`,status:"success",duration:3e3,isClosable:!0}),e(),l([]),b(["conversations","conversationDetail"])},onError:k=>{var x;m({title:`C\u1EADp nh\u1EADt ${u?"channel":"nh\xF3m chat"} kh\xF4ng th\xE0nh c\xF4ng!`,description:`${(x=k==null?void 0:k.message)!=null?x:"L\u1ED7i"}`,status:"error",duration:3e3,isClosable:!0})}});return{handleSubmitForm:react.exports.useCallback(async k=>{try{n?await C({variables:pickBy_1({name:k==null?void 0:k.name,id:a,userIds:r==null?void 0:r.filter(x=>x!==(d==null?void 0:d.id))})}):await _({variables:pickBy_1({isChannel:u,name:k==null?void 0:k.name,group:a,userIds:r})})}catch(x){console.log(x)}},[r,a,n,d,u]),loading:T||S}},getUserInfo=e=>typeof e=="object"?Promise.resolve(e):apolloClient.query({query:GRAPH_NET_GET_USER_INFO,variables:{userId:parseInt(e,10)}}).then(({data:r})=>r.getUserById),getUsersInfo=(e,r)=>lodash.exports.isEmpty(e||r)?[]:Promise.allSettled(e==null?void 0:e.map(getUserInfo)).then(n=>n.filter(a=>a.status==="fulfilled").map(a=>a==null?void 0:a.value)),CreateConversationModal=({onClose:e,isOpen:r,groupId:n="",listParticipants:a,name:l,isEdit:c,isAddSupporter:u,isChannel:d})=>{const[m,b]=react.exports.useState([]),A=react.exports.useRef(),[_,T]=react.exports.useState([]),[C,S]=react.exports.useState(n),{getCheckboxProps:P,value:k,setValue:x}=useCheckboxGroup({onChange:T}),{handleSubmitForm:E,loading:g}=useCreateOrUpdateConversation({onClose:e,userId:[...a!=null?a:[],..._!=null?_:[]],isEdit:c,groupId:C,setUserId:T,isAddSupporter:u,isChannel:d}),y=useForm({mode:"onBlur",reValidateMode:"onChange"}),{register:I,formState:{errors:M}}=y,j=react.exports.useRef(null),O=y.watch("name"),R=!lodash.exports.isEqual(O,j.current)||(_==null?void 0:_.length)>0;return useDeepCompareEffect$2(()=>{c&&!u&&(y.reset({name:l}),j.current=l)},[r,c,j,y,l]),react.exports.useEffect(()=>{(async function(){try{const B=await getUsersInfo(_,!r);b(B)}catch(B){console.log(B)}})()},[_]),react.exports.useEffect(()=>{S(n)},[n]),jsx(FormProvider,H(N({},y),{children:jsxs(Modal$1,{finalFocusRef:A,isOpen:r,onClose:()=>{e(),x([])},size:"2xl",children:[jsx(ModalOverlay,{}),jsxs(ModalContent,{bg:useColorModeValue(void 0,"slate.900"),children:[jsx(ModalHeader,{children:u?"Th\xEAm EC h\u1ED7 tr\u1EE3":c?"C\u1EADp nh\u1EADt nh\xF3m chat":d?"T\u1EA1o channel":"T\u1EA1o nh\xF3m chat"}),jsx(ModalCloseButton,{}),jsxs("form",{onSubmit:y.handleSubmit(E),children:[jsx(ModalBody,{children:jsxs(Stack,{spacing:6,children:[!u&&jsx(TextInput,H(N({label:d?"T\xEAn channel":"T\xEAn nh\xF3m",placeholder:"Nh\u1EADp t\xEAn nh\xF3m"},I("name",{required:!0})),{error:M==null?void 0:M.name})),jsx(ListUser,{value:k,setValue:x,isAddSupporter:u,usersInfo:m,listParticipants:a,getCheckboxProps:P})]})}),jsxs(ModalFooter,{children:[jsx(Button$1,{disabled:!c&&lodash.exports.isEmpty(_)||g||c&&!R,leftIcon:jsx(Icon,{as:AiOutlineSend}),bg:"#319795",mr:4,type:"submit",isLoading:!1,color:"white",children:c?"C\u1EADp nh\u1EADt":"T\u1EA1o nh\xF3m"}),jsx(Button$1,{disabled:g,variant:"outline",onClick:()=>{e(),x([]),T([])},children:"H\u1EE7y"})]})]})]})]})}))},tabConfig=[{key:"open",value:"Open"},{key:"following",value:"Following"},{key:"done",value:"Done"}],DataTabs=({data:e,isSelected:r,setParticipants:n,setUserId:a,loading:l,setLocalTab:c})=>{var x;const{isOpen:u,onOpen:d,onClose:m}=useDisclosure(),b=useStore(E=>E.setTab),A=useStore(E=>E.channel),_=useStore(E=>E.conversationId),T=useStore(E=>E.setConversationId),[C]=useUser(),[S,P]=useSearchParams({}),k=react.exports.useCallback(E=>{_===E.id&&b("message"),T(E.id),n(E==null?void 0:E.participants),a(""),b("message"),P({qsConversationId:E.id}),S.delete("qsUserId"),S.delete("qsGroup")},[_,S]);return jsxs(Tabs,{onChange:E=>{var g;return c((g=tabConfig[E])==null?void 0:g.key)},defaultIndex:tabConfig.findIndex(E=>E.key===r),children:[jsxs(Flex,{flexDirection:{base:"row",md:"column"},position:"sticky",top:0,zIndex:20,bg:useColorModeValue("white","#10172a"),alignItems:"center",justifyContent:"center",children:[jsxs(HStack,{w:{base:["my chats","guest"].includes(A)?"25%":"100%",md:"full"},alignItems:"center",justify:"space-between",spacing:4,px:2,borderBottomWidth:1,minH:"50px",children:[jsx(Text,{fontSize:13,fontWeight:"bold",textColor:useColorModeValue("#444","slate.300"),children:(x=A==null?void 0:A.toString())==null?void 0:x.toLocaleUpperCase()}),!["my chats","guest"].includes(A)&&zimRolesNumber.map(E=>E.id).includes(C==null?void 0:C.roleId)&&jsxs(Box,{onClick:d,"data-tip":"React-tooltip",display:"flex",alignItems:"center",justifyContent:"center",cursor:"pointer",children:[jsx(MdOutlineGroupAdd,{fontSize:20}),jsx(ReactTooltip,{children:jsx("p",{className:"text-base font-medium",children:"T\u1EA1o group chat"}),place:"bottom",backgroundColor:"green",effect:"float"})]})]}),A==="guest"&&jsx(HStack,{spacing:0,w:"full",minH:"50px",children:tabConfig.map((E,g)=>jsx(Tab,{minH:"50px",flex:1,_dark:{color:"slate.300"},w:"full",fontSize:{sm:14},px:0,borderBottomColor:useColorModeValue("gray.200","whiteAlpha.300"),mb:0,children:E.value},E.key))})]}),jsx(Stack,{spacing:0,children:(e==null?void 0:e.length)>0?e.map((E,g)=>{var V,Q,W,q,L,D,F,$,U,z,K,Z;const y=E==null?void 0:E.participants,I=!(y==null?void 0:y.find(Y=>Y.userId===(C==null?void 0:C.id)))&&(y==null?void 0:y.length)>=2||(y==null?void 0:y.find(Y=>Y.userId===(C==null?void 0:C.id)))&&(y==null?void 0:y.length)>=3,M=(E==null?void 0:E.name)||I?y:y==null?void 0:y.filter(Y=>Y.userId!==(C==null?void 0:C.id))[0],j=(M==null?void 0:M.isGuest)?M==null?void 0:M.fullName:(E==null?void 0:E.name)?E==null?void 0:E.name:I?y==null?void 0:y.map(Y=>Y.fullName).join(","):M==null?void 0:M.fullName,O=((Q=(V=E==null?void 0:E.usersTyping)==null?void 0:V.filter(Y=>Y!==(C==null?void 0:C.id)))==null?void 0:Q.length)>0,R=(E==null?void 0:E.lastMessage)?(q=(W=E==null?void 0:E.lastMessage)==null?void 0:W.seenBy)==null?void 0:q.includes(C.id):!0,B=timeDifferenceCalculator(dayjs(E==null?void 0:E.updatedAt).toDate()),G=((D=(L=E==null?void 0:E.lastMessage)==null?void 0:L.attachments)==null?void 0:D.length)>0?["[T\u1EC7p \u0111\xEDnh k\xE8m]"]:(F=E==null?void 0:E.lastMessage)==null?void 0:F.text;return l?jsx(ConversationLoading,{},g):react.exports.createElement(ConversationItem,H(N({},M),{isTyping:O,isActive:E.id===_,key:E.id,onClick:()=>{k(E)},username:j,lastMessage:G,updatedAt:B,seen:R,seenByCount:(z=(U=($=E==null?void 0:E.lastMessage)==null?void 0:$.seenBy)==null?void 0:U.length)!=null?z:0,avatarUrl:M==null?void 0:M.avatar,groupInfo:(E==null?void 0:E.name)||I?M:null,userTypingName:(Z=(K=E==null?void 0:E.usersTyping)==null?void 0:K.filter(Y=>Y!==C.id))==null?void 0:Z.map(Y=>{var le;return(le=y.find(re=>re.userId===Y))==null?void 0:le.fullName})}))}):jsx(Text,{textAlign:"center",mt:4,children:`Hi\u1EC7n t\u1EA1i kh\xF4ng c\xF3 ${A==="guest"?"y\xEAu c\u1EA7u h\u1ED7 tr\u1EE3":"cu\u1ED9c tr\xF2 chuy\u1EC7n"} n\xE0o !!!`})}),jsx(CreateConversationModal,{isChannel:!1,isAddSupporter:!1,isEdit:!1,name:"",listParticipants:[],isOpen:u,onClose:m,groupId:A})]})},useGetSupportConversations=({status:e="",search:r,channel:n})=>{var S,P,k;const a=H(N({},pickBy_1({status:e,search:r},x=>x)),{limit:20,offset:0}),[l,{data:c,loading:u,fetchMore:d,called:m}]=useLazyQuery(SUPPORT_CONVERSATIONS,{variables:a,fetchPolicy:"network-only",nextFetchPolicy:"cache-first"});react.exports.useEffect(()=>{n&&n==="guest"&&!m&&(async()=>await l())()},[n,m]);const b=((S=c==null?void 0:c.supportConversations)==null?void 0:S.docs)||[],A=(P=c==null?void 0:c.supportConversations)==null?void 0:P.hasNextPage,{onLoadMore:_,isLoadingMore:T}=useLoadMore({variables:H(N({},a),{offset:(b==null?void 0:b.length)+1}),fetchMore:d,hasNextPage:A});return{listSupportConversations:((k=c==null?void 0:c.supportConversations)==null?void 0:k.docs)||[],loading:u,onLoadMore:_,hasNextPage:A,isLoadingMore:T}},useGetConversations=({search:e,channel:r})=>{var T,C;const n=H(N({},pickBy_1({group:r==="my chats"?"":r,search:e},S=>S)),{limit:20,offset:0}),[a,{data:l,loading:c,fetchMore:u,called:d}]=useLazyQuery(CONVERSATIONS,{variables:n,fetchPolicy:"network-only",nextFetchPolicy:"cache-first"});react.exports.useEffect(()=>{r&&r!=="guest"&&!d&&(async()=>await a())()},[r,d,a]);const m=((T=l==null?void 0:l.conversations)==null?void 0:T.docs)||[],b=(C=l==null?void 0:l.conversations)==null?void 0:C.hasNextPage,{onLoadMore:A,isLoadingMore:_}=useLoadMore({variables:H(N({},n),{offset:(m==null?void 0:m.length)+1}),fetchMore:u,hasNextPage:b});return{listConversations:m,loading:c,onLoadMore:A,hasNextPage:b,isLoadingMore:_}},isReachBottom$1=e=>e.scrollHeight-e.scrollTop<=e.clientHeight+10,Loading=({text:e="\u0110ang l\u1EA5y d\u1EEF li\u1EC7u...",color:r="gray.900"})=>jsxs(HStack,{justifyContent:"center",pointerEvents:"none",userSelect:"none",children:[jsx(Spinner,{color:r}),jsx(Text,{textAlign:"center",color:r,children:e})]}),LoadingAnimated=()=>jsx("div",{className:"loading-container",children:jsxs("div",{className:"loading-circle",children:[jsx("div",{}),jsx("div",{}),jsx("div",{}),jsx("div",{}),jsx("div",{})]})}),MessageSearchResultItem=({onClick:e,username:r,lastMessage:n,isActive:a,createdAt:l,avatarUrl:c})=>{const u=useStore(d=>d.searchStringMessage);return jsxs(HStack,{_hover:{backgroundColor:"#e4e4e450"},w:"100%",align:"start",p:3,py:2,shadow:"md",cursor:"pointer",rounded:"lg",onClick:e,bg:useColorModeValue(a?"gray.100":"bg-white",a?"#1e293b":"#1e293b50"),children:[jsx(MyAvatar,{boxSize:"2em",src:c,name:r}),jsxs(Stack,{flex:1,spacing:1,children:[jsx(Text,{noOfLines:2,fontSize:15,lineHeight:"18px",color:useColorModeValue("gray.900","slate.300"),children:r}),jsx(Text,{noOfLines:2,fontSize:12,lineHeight:"24px",fontWeight:"400",color:useColorModeValue("gray.900","slate.300"),children:jsx(Highlighter,{searchWords:[u],autoEscape:!0,textToHighlight:n})})]}),jsx(VStack,{spacing:2,alignItems:"flex-end",minW:"50px",children:jsx(Text,{color:useColorModeValue("gray.900","slate.300"),fontSize:12,children:l})})]})},ListItemSearchMessage=()=>{const e=useStore(d=>d.searchStringMessage),r=useStore(d=>d.searchResult),n=useStore(d=>d.setMessageId),a=useStore(d=>d.messageId),l=useStore(d=>d.setBeforeId),c=useStore(d=>d.setAfterId),u=d=>{n(d),l(""),c("")};return jsxs(Stack,{w:"full",h:"full",p:2,spacing:2,children:[jsx(HStack,{position:"sticky",top:0,zIndex:50,bg:useColorModeValue("white","#10172a"),alignItems:"center",children:jsx(Text,{fontSize:16,fontWeight:"bold",textColor:useColorModeValue("#444","slate.300"),children:`C\xF3 ${r==null?void 0:r.length} k\u1EBFt qu\u1EA3 t\xECm ki\u1EBFm cho t\u1EEB kh\xF3a "${e}"`})}),r==null?void 0:r.map(d=>{var _,T;const m=(T=(_=d==null?void 0:d.conversation)==null?void 0:_.participants)==null?void 0:T.find(C=>C.userId===(d==null?void 0:d.from)),b=timeDifferenceCalculator(dayjs(d==null?void 0:d.createdAt).toDate()),A=d.id===a;return jsx(MessageSearchResultItem,{isActive:A,onClick:()=>u(d.id),lastMessage:d==null?void 0:d.text,avatarUrl:m==null?void 0:m.avatar,username:m==null?void 0:m.fullName,createdAt:b},d.id)})]})},MyListConversation=()=>{const e=useStore(y=>y.setParticipants),r=useStore(y=>y.searchStringMessage),n=useStore(y=>y.channel),a=useStore(y=>y.setUserId),[l,c]=react.exports.useState(!1),[u,d]=react.exports.useState("open"),{listSupportConversations:m,onLoadMore:b,isLoadingMore:A,hasNextPage:_,loading:T}=useGetSupportConversations({status:u,search:"",channel:n}),{listConversations:C,onLoadMore:S,hasNextPage:P,isLoadingMore:k,loading:x}=useGetConversations({channel:n,search:""}),E=n==="guest"?{onLoadMore:b,isLoadingMore:A,hasNextPage:_,loading:T}:{onLoadMore:S,hasNextPage:P,isLoadingMore:k,loading:x},g=react.exports.useCallback(y=>{c(isReachBottom$1(y.target))},[]);return useDeepCompareEffect$2(()=>{l&&E.hasNextPage&&!E.isLoadingMore&&E.onLoadMore()},[l,E]),jsxs(Flex,{direction:"column",w:{base:"full",md:"260px",lg:"350px"},h:"100%",bg:useColorModeValue("white","#10172a"),onScroll:g,overflow:"auto",position:"relative",children:[r?jsx(ListItemSearchMessage,{}):jsx(DataTabs,{setParticipants:e,setLocalTab:d,setUserId:a,loading:E.loading,data:E.loading?[1,2,3,4,5,6,7,8]:n==="guest"?m:C,isSelected:u}),E.isLoadingMore&&jsx(Loading,{text:"\u0110ang t\u1EA3i cu\u1ED9c tr\xF2 chuy\u1EC7n",color:useColorModeValue("","white")})]})};var sayHi="/assets/weCanDoItWomen.png";const usePushWindowNotification=()=>({onPush:react.exports.useCallback((r,n,a)=>{if(!Notification)return;Notification.permission!=="granted"&&Notification.requestPermission();let l=new Notification("B\u1EA1n c\xF3 tin nh\u1EAFn m\u1EDBi !!",{icon:sayHi,body:`${a}: ${n}`});l.onclick=function(){window.open(`${appConfigs.apiUrl}/Admin/Chat?qsConversationId=${r}`)}},[])}),useUpdateConversations=()=>{const[e]=useUser(),{onPush:r}=usePushWindowNotification(),n=useStore(l=>l.conversationId),a=useStore(l=>l.userId);useSubscription(CONVERSATION_UPDATED,{onSubscriptionData:({client:l,subscriptionData:c})=>{var W,q,L,D,F,$,U,z,K,Z,Y,le,re,ee,oe,me,ne,ce,fe,X,de,se,te,pe,ge,Ce,he,Ee,_e,J,ae,ve,ye,be,Ie,Pe,we,Me,je,ke,Fe,Oe,$e,De,Le,Be,Ne,Ve,Ge,qe,ze,He,Ue,We,Qe,Ke,Ye,Ze,Xe,Je,et,tt,rt;const u=l.cache.readQuery({query:MESSAGES,variables:H(N({},pickBy_1({conversationId:n,userId:n?"":a==null?void 0:a.toString(),limit:30},ie=>ie)),{offset:0})}),d=(q=(W=u==null?void 0:u.messages)==null?void 0:W.docs)!=null?q:[],{conversationUpdated:m}=c.data,{lastMessage:b,typing:A}=m,{id:_,from:T}=b;if(!(d==null?void 0:d.find(ie=>(ie==null?void 0:ie.id)===_))&&e.id!==T&&A===null){const ie=(D=(L=m==null?void 0:m.participants)==null?void 0:L.find(xe=>xe.userId===T))==null?void 0:D.fullName;r((F=c==null?void 0:c.data)==null?void 0:F.conversationUpdated.id,($=c==null?void 0:c.data)==null?void 0:$.conversationUpdated.lastMessage.text,ie)}const S=(z=(U=c==null?void 0:c.data)==null?void 0:U.conversationUpdated)==null?void 0:z.group,P=(Z=(K=c==null?void 0:c.data)==null?void 0:K.conversationUpdated)==null?void 0:Z.status,k=l.cache.readQuery({query:CONVERSATIONS,variables:{offset:0,limit:20}}),x=(le=(Y=k==null?void 0:k.conversations)==null?void 0:Y.docs)!=null?le:[];if((k==null?void 0:k.conversations)&&P!=="open"){let ie=[...x];const xe=ie==null?void 0:ie.find(ue=>{var Se,Ae;return(ue==null?void 0:ue.id)===((Ae=(Se=c==null?void 0:c.data)==null?void 0:Se.conversationUpdated)==null?void 0:Ae.id)});xe&&((re=c==null?void 0:c.data)==null?void 0:re.conversationUpdated.typing)===null?(ie=ie.filter(ue=>ue.id!==xe.id),l.writeQuery({query:CONVERSATIONS,variables:{offset:0,limit:20},data:H(N({},k),{conversations:H(N({},(ee=k.conversations)!=null?ee:[]),{docs:[(oe=c==null?void 0:c.data)==null?void 0:oe.conversationUpdated,...ie]})})})):xe?l.writeQuery({query:CONVERSATIONS,variables:{offset:0,limit:20},data:N({},k)}):l.writeQuery({query:CONVERSATIONS,variables:{offset:0,limit:20},data:H(N({},k),{conversations:H(N({},(me=k.conversations)!=null?me:[]),{docs:[(ne=c==null?void 0:c.data)==null?void 0:ne.conversationUpdated,...ie]})})})}const E=l.cache.readQuery({query:CONVERSATIONS,variables:{group:"zimians",offset:0,limit:20}}),g=(fe=(ce=E==null?void 0:E.conversations)==null?void 0:ce.docs)!=null?fe:[];if((E==null?void 0:E.conversations)&&S==="zimians"){let ie=[...g];const xe=ie==null?void 0:ie.find(ue=>{var Se,Ae;return(ue==null?void 0:ue.id)===((Ae=(Se=c==null?void 0:c.data)==null?void 0:Se.conversationUpdated)==null?void 0:Ae.id)});xe&&((X=c==null?void 0:c.data)==null?void 0:X.conversationUpdated.typing)===null?(ie=ie.filter(ue=>ue.id!==xe.id),l.writeQuery({query:CONVERSATIONS,variables:{group:"zimians",offset:0,limit:20},data:H(N({},E),{conversations:H(N({},(de=E.conversations)!=null?de:[]),{docs:[(se=c==null?void 0:c.data)==null?void 0:se.conversationUpdated,...ie]})})})):xe?l.writeQuery({query:CONVERSATIONS,variables:{group:"zimians",offset:0,limit:20},data:N({},E)}):l.writeQuery({query:CONVERSATIONS,variables:{group:"zimians",offset:0,limit:20},data:H(N({},E),{conversations:H(N({},(te=E.conversations)!=null?te:[]),{docs:[(pe=c==null?void 0:c.data)==null?void 0:pe.conversationUpdated,...ie]})})})}const y=l.cache.readQuery({query:CONVERSATIONS,variables:{group:"student",offset:0,limit:20}}),I=(Ce=(ge=y==null?void 0:y.conversations)==null?void 0:ge.docs)!=null?Ce:[];if((y==null?void 0:y.conversations)&&S==="student"){let ie=[...I];const xe=ie==null?void 0:ie.find(ue=>{var Se,Ae;return(ue==null?void 0:ue.id)===((Ae=(Se=c==null?void 0:c.data)==null?void 0:Se.conversationUpdated)==null?void 0:Ae.id)});xe&&((he=c==null?void 0:c.data)==null?void 0:he.conversationUpdated.typing)===null?(ie=ie.filter(ue=>ue.id!==xe.id),l.writeQuery({query:CONVERSATIONS,variables:{group:"student",offset:0,limit:20},data:H(N({},y),{conversations:H(N({},(Ee=y.conversations)!=null?Ee:[]),{docs:[(_e=c==null?void 0:c.data)==null?void 0:_e.conversationUpdated,...ie]})})})):xe?l.writeQuery({query:CONVERSATIONS,variables:{group:"student",offset:0,limit:20},data:N({},y)}):l.writeQuery({query:CONVERSATIONS,variables:{group:"student",offset:0,limit:20},data:H(N({},y),{conversations:H(N({},(J=y.conversations)!=null?J:[]),{docs:[(ae=c==null?void 0:c.data)==null?void 0:ae.conversationUpdated,...ie]})})})}const M=l.cache.readQuery({query:CONVERSATIONS,variables:{group:"customer",offset:0,limit:20}}),j=(ye=(ve=M==null?void 0:M.conversations)==null?void 0:ve.docs)!=null?ye:[];if((M==null?void 0:M.conversations)&&S==="customer"){let ie=[...j];const xe=ie==null?void 0:ie.find(ue=>{var Se,Ae;return(ue==null?void 0:ue.id)===((Ae=(Se=c==null?void 0:c.data)==null?void 0:Se.conversationUpdated)==null?void 0:Ae.id)});xe&&((be=c==null?void 0:c.data)==null?void 0:be.conversationUpdated.typing)===null?(ie=ie.filter(ue=>ue.id!==xe.id),l.writeQuery({query:CONVERSATIONS,variables:{group:"customer",offset:0,limit:20},data:H(N({},M),{conversations:H(N({},(Ie=M.conversations)!=null?Ie:[]),{docs:[(Pe=c==null?void 0:c.data)==null?void 0:Pe.conversationUpdated,...ie]})})})):xe?l.writeQuery({query:CONVERSATIONS,variables:{group:"customer",offset:0,limit:20},data:N({},M)}):l.writeQuery({query:CONVERSATIONS,variables:{group:"customer",offset:0,limit:20},data:H(N({},M),{conversations:H(N({},(we=M.conversations)!=null?we:[]),{docs:[(Me=c==null?void 0:c.data)==null?void 0:Me.conversationUpdated,...ie]})})})}const O=l.cache.readQuery({query:SUPPORT_CONVERSATIONS,variables:{status:"open",offset:0,limit:20}}),R=(ke=(je=O==null?void 0:O.supportConversations)==null?void 0:je.docs)!=null?ke:[];if((O==null?void 0:O.supportConversations)&&S==="support"){let ie=[...R];const xe=ie==null?void 0:ie.find(ue=>{var Se,Ae;return(ue==null?void 0:ue.id)===((Ae=(Se=c==null?void 0:c.data)==null?void 0:Se.conversationUpdated)==null?void 0:Ae.id)});!xe&&P==="open"?l.writeQuery({query:SUPPORT_CONVERSATIONS,variables:{status:"open",offset:0,limit:20},data:H(N({},O),{supportConversations:H(N({},(Fe=O.supportConversations)!=null?Fe:[]),{docs:[(Oe=c==null?void 0:c.data)==null?void 0:Oe.conversationUpdated,...ie]})})}):xe&&(($e=c==null?void 0:c.data)==null?void 0:$e.conversationUpdated.typing)===null&&((Be=(Le=(De=c==null?void 0:c.data)==null?void 0:De.conversationUpdated)==null?void 0:Le.lastMessage)==null?void 0:Be.from)===e.id?(ie=ie.filter(ue=>ue.id!==xe.id),l.writeQuery({query:SUPPORT_CONVERSATIONS,variables:{status:"open",offset:0,limit:20},data:H(N({},O),{supportConversations:H(N({},(Ne=O.supportConversations)!=null?Ne:[]),{docs:[...ie]})})})):l.writeQuery({query:SUPPORT_CONVERSATIONS,variables:{status:"open",offset:0,limit:20},data:H(N({},O),{supportConversations:N({},(Ve=O.supportConversations)!=null?Ve:[])})})}const B=l.cache.readQuery({query:SUPPORT_CONVERSATIONS,variables:{status:"following",offset:0,limit:20}}),G=(qe=(Ge=B==null?void 0:B.supportConversations)==null?void 0:Ge.docs)!=null?qe:[];if((B==null?void 0:B.supportConversations)&&S==="support"){let ie=[...G];const xe=ie==null?void 0:ie.find(ue=>{var Se,Ae;return(ue==null?void 0:ue.id)===((Ae=(Se=c==null?void 0:c.data)==null?void 0:Se.conversationUpdated)==null?void 0:Ae.id)});!xe&&P==="following"?l.writeQuery({query:SUPPORT_CONVERSATIONS,variables:{status:"following",offset:0,limit:20},data:H(N({},B),{supportConversations:H(N({},(ze=B.supportConversations)!=null?ze:[]),{docs:[(He=c==null?void 0:c.data)==null?void 0:He.conversationUpdated,...ie]})})}):xe&&((Ue=c==null?void 0:c.data)==null?void 0:Ue.conversationUpdated.typing)===null?(ie=ie.filter(ue=>ue.id!==xe.id),l.writeQuery({query:SUPPORT_CONVERSATIONS,variables:{status:"following",offset:0,limit:20},data:H(N({},B),{supportConversations:H(N({},(We=B.supportConversations)!=null?We:[]),{docs:P==="following"?[(Qe=c==null?void 0:c.data)==null?void 0:Qe.conversationUpdated,...ie]:[...ie]})})})):l.writeQuery({query:SUPPORT_CONVERSATIONS,variables:{status:"following",offset:0,limit:20},data:H(N({},B),{supportConversations:N({},(Ke=B.supportConversations)!=null?Ke:[])})})}const V=l.cache.readQuery({query:SUPPORT_CONVERSATIONS,variables:{status:"done",offset:0,limit:20}}),Q=(Ze=(Ye=V==null?void 0:V.supportConversations)==null?void 0:Ye.docs)!=null?Ze:[];if((V==null?void 0:V.supportConversations)&&S==="support"){let ie=[...Q];const xe=ie==null?void 0:ie.find(ue=>{var Se,Ae;return(ue==null?void 0:ue.id)===((Ae=(Se=c==null?void 0:c.data)==null?void 0:Se.conversationUpdated)==null?void 0:Ae.id)});xe&&((Xe=c==null?void 0:c.data)==null?void 0:Xe.conversationUpdated.typing)===null?(ie=ie.filter(ue=>ue.id!==xe.id),l.writeQuery({query:SUPPORT_CONVERSATIONS,variables:{status:"done",offset:0,limit:20},data:H(N({},Q),{supportConversations:H(N({},(Je=Q.supportConversations)!=null?Je:[]),{docs:[...ie]})})})):!xe&&P==="done"?l.writeQuery({query:SUPPORT_CONVERSATIONS,variables:{status:"done",offset:0,limit:20},data:H(N({},V),{supportConversations:H(N({},(et=V.supportConversations)!=null?et:[]),{docs:[(tt=c==null?void 0:c.data)==null?void 0:tt.conversationUpdated,...ie]})})}):l.writeQuery({query:SUPPORT_CONVERSATIONS,variables:{status:"done",offset:0,limit:20},data:H(N({},Q),{supportConversations:N({},(rt=Q.supportConversations)!=null?rt:[])})})}},shouldResubscribe:!!e})};function replaceTextWithEmoji(e,r){const n=e.split(" ");return n.map((a,l)=>{if(a.length>1&&a.includes(":")){if(!r&&l===n.length-1)return a;const c=emojiIndex.search(a);return c.length>0?c[0].native:a}return a}).join(" ")}const useSendMessage=({setContent:e,userId:r,group:n,setConversationId:a})=>{const[l]=useUser(),c=l==null?void 0:l.id,[u,{client:d,loading:m}]=useMutation(SEND_MESSAGE),b=react.exports.useCallback(async(C,S,P,k)=>{var g;const x=P==null?void 0:P.map(({type:y,fileId:I})=>({type:y,attachmentId:I})),{data:E}=await u({variables:{recipient:pickBy_1({recipientType:k?"conversation":"user",conversationId:k||null,userId:k?null:r==null?void 0:r.toString(),group:k||n==="my chats"?"":n},y=>y),message:pickBy_1({text:C.trim(),attachments:lodash.exports.isEmpty(x)?null:x},y=>y)},update:(y,I)=>{var j;const M=y.readQuery({query:MESSAGES,variables:H(N({},pickBy_1({conversationId:k,userId:k?"":r==null?void 0:r.toString(),limit:30},O=>O)),{offset:0})});if(M==null?void 0:M.messages){const O=((j=M==null?void 0:M.messages)==null?void 0:j.docs)||[],R=O==null?void 0:O.map(B=>B.id===S?H(N(N({},B),I==null?void 0:I.data.sendMessage.lastMessage),{loading:!1}):B);y.writeQuery({query:MESSAGES,variables:H(N({},pickBy_1({conversationId:k,userId:k?"":r==null?void 0:r.toString(),limit:30},B=>B)),{offset:0}),data:H(N({},M),{messages:H(N({},M==null?void 0:M.messages),{docs:R})})})}}});k||await a((g=E==null?void 0:E.sendMessage)==null?void 0:g.id)},[u,r,n,c,l,a]),A=react.exports.useCallback(async(C,S)=>{if(S)try{await u({variables:{recipient:{conversationId:S,recipientType:"conversation"},senderAction:C}})}catch(P){console.log(P)}},[u]),_=react.exports.useCallback(async C=>{try{await u({variables:{recipient:pickBy_1({recipientType:"conversation",conversationId:C||"",userId:C?"":r==null?void 0:r.toString()},S=>S),senderAction:"markSeen"}})}catch(S){console.log(S)}},[u,r]);return{onSendMessage:react.exports.useCallback(async({nativeEvent:C,conversationId:S,file:P})=>{var I,M;const k=H(N({},pickBy_1({conversationId:S,userId:S?"":r==null?void 0:r.toString(),limit:30},j=>j)),{offset:0}),x=replaceTextWithEmoji(C.text,!0),E=v4$1(),g=d.cache.readQuery({query:MESSAGES,variables:N({},k)}),y=(M=(I=g==null?void 0:g.messages)==null?void 0:I.docs)!=null?M:[];try{e(""),(g==null?void 0:g.messages)&&d.cache.writeQuery({query:MESSAGES,variables:N({},k),data:H(N({},g),{messages:H(N({},g==null?void 0:g.messages),{docs:[{id:E,text:x,from:c,to:null,type:"update",attachments:[],seenBy:[],createdAt:dayjs().format(),updatedAt:dayjs().format(),deletedAt:null,callPayload:null,loading:!0,error:!1,conversation:{id:S,participants:[{userId:c,avatar:l.avatar,fullName:l.fullName}]},emojis:{userId:l==null?void 0:l.id,emoji:""}},...y]})})}),await b(x,E,P,S);const j=document.getElementById("chat-box");j&&(j.scrollTop=j==null?void 0:j.scrollHeight)}catch{d.cache.writeQuery({query:MESSAGES,variables:N({},k),data:H(N({},g),{messages:H(N({},g==null?void 0:g.messages),{docs:[{id:E,text:x,from:c,to:null,type:"update",attachments:[],seenBy:[],createdAt:dayjs().format(),updatedAt:dayjs().format(),deletedAt:null,callPayload:null,loading:!1,error:!0,conversation:{id:S,participants:[{userId:c,avatar:l.avatar,fullName:l.fullName}]},emojis:{userId:l==null?void 0:l.id,emoji:""}},...y]})})})}},[e,b,r,d]),onTyping:A,onMarkSeen:_,loading:m}},SpaceTime=({time:e})=>jsx(Box,{py:6,display:"flex",alignItems:"center",justifyContent:"center",w:"full",children:jsx(Text,{fontSize:14,textAlign:"center",color:useColorModeValue("gray.400","slate.300"),children:moment(e).format("HH:mm - DD-MM-YYYY")})}),validURL=e=>{if(typeof e!="string"||!e)return!1;let r;try{r=new URL(e)}catch{return!1}return r.protocol==="http:"||r.protocol==="https:"},ApiURL="https://getopengraph.herokuapp.com/",REGEX=new RegExp("^(?!mailto:)(?:(?:http|https|ftp)://)(?:\\S+(?::\\S*)?@)?(?:(?:(?:[1-9]\\d?|1\\d\\d|2[01]\\d|22[0-3])(?:\\.(?:1?\\d{1,2}|2[0-4]\\d|25[0-5])){2}(?:\\.(?:[0-9]\\d?|1\\d\\d|2[0-4]\\d|25[0-4]))|(?:(?:[a-z\\u00a1-\\uffff0-9]+-?)*[a-z\\u00a1-\\uffff0-9]+)(?:\\.(?:[a-z\\u00a1-\\uffff0-9]+-?)*[a-z\\u00a1-\\uffff0-9]+)*(?:\\.(?:[a-z\\u00a1-\\uffff]{2,})))|localhost)(?::\\d{2,5})?(?:(/|\\?|#)[^\\s]*)?$"),PreviewLink=({url:e,isMyMessage:r})=>{const[n,a]=react.exports.useState(null),[l,c]=react.exports.useState(!1);react.exports.useEffect(()=>{e&&e.match(REGEX)!==null&&u(e)},[e]);const u=async m=>{c(!0);let b=await axios.get(`${ApiURL}?url=${m}`,{method:"GET"});a(b.data),c(!1)},d=react.exports.useMemo(()=>{var m;return jsxs(HStack,{flexDirection:"column",rounded:"8px",children:[jsx(Link$1,{_hover:{color:r?"white":void 0},fontSize:14,target:"_blank",href:e,cursor:"pointer",color:"black",textDecoration:"underline",onClick:()=>window.open(e),rel:"noreferrer",children:(n==null?void 0:n.url)||e}),((m=n==null?void 0:n.img)==null?void 0:m.length)>0&&jsx(Image$1,{w:"100%",h:"150px",mt:4,alt:"img",src:n==null?void 0:n.img,rounded:"8px"}),jsx(Link$1,{_hover:{color:r?"white":void 0},fontSize:14,target:"_blank",href:e,cursor:"pointer",color:"black",textDecoration:"underline",onClick:()=>window.open(e),rel:"noreferrer",children:(n==null?void 0:n.title)||"kh\xF4ng t\xECm th\u1EA5y"})]})},[n,e,r]);return l?jsx(Link$1,{_hover:{color:r?"white":void 0},fontSize:14,target:"_blank",href:e,cursor:"pointer",color:"black",textDecoration:"underline",onClick:()=>window.open(e),rel:"noreferrer",children:e}):(n==null?void 0:n.status)===200&&n&&d},getFilePathName=e=>{if(!e)return;const r=(e==null?void 0:e.search("-"))+1;return e==null?void 0:e.slice(r)},ImageMessage=({files:e,onPress:r,isMyMessage:n})=>(console.log(e,"?????"),jsx(SimpleGrid,{maxW:{base:"250px"},columns:(e==null?void 0:e.length)>3?3:e==null?void 0:e.length,spacing:"8px",children:e==null?void 0:e.map((a,l)=>{var c,u,d,m;return jsx(Box,{rounded:"lg",borderColor:useColorModeValue("gray.100","gray.300"),cursor:"pointer",overflow:"hidden",bg:n?"blue.50":"gray.100",display:"flex",alignItems:"center",justifyContent:"center",children:((c=a==null?void 0:a.attachment)==null?void 0:c.type)!=="file"?jsx(Image$1,{objectFit:"cover",w:"80px",h:"80px",onClick:()=>r(l),src:(u=a==null?void 0:a.attachment)==null?void 0:u.fullUrl},l):jsx(Link$1,{href:(d=a==null?void 0:a.attachment)==null?void 0:d.fullUrl,target:"_blank",children:jsxs(HStack,{m:2,children:[jsx(Box,{w:6,h:6,rounded:"full",display:"flex",alignItems:"center",justifyContent:"center",borderWidth:1,children:jsx(AiOutlineFileZip,{className:"w-6 h-6 text-gray-700"})}),jsx(Text,{flex:1,color:"gray.800",fontSize:14,children:getFilePathName((m=a==null?void 0:a.attachment)==null?void 0:m.path)})]})})},l)})})),TextMessage=({text:e,isMyMessage:r,deletedAt:n,createdAt:a,attachments:l,setAttachmentIndex:c,setShowImageViewer:u})=>{const d=useStore(_=>_.searchStringMessage),m=useColorModeValue("gray.100","slate.300"),b=useColorModeValue("gray.900","slate.900"),A=useColorModeValue("text-gray-100","text-slate-300");return react.exports.useMemo(()=>jsxs(Box,{pointerEvents:n?"none":"auto",py:{sm:1.5,base:3},px:3,rounded:"lg",maxW:{sm:"2/3",base:"210px",lg:"250px",xl:"280px","2xl":"400px"},bg:r?"blue.50":m,pos:"relative",children:[jsx(IoCaretForwardSharp,{className:`${r?"text-blue-50":A} absolute ${r?"top-4 -right-2.5":"bottom-1 -left-2.5"} transform ${r?void 0:"rotate-180"}`}),n?jsx(Text,{letterSpacing:.1,fontWeight:"500",fontSize:{base:"13px"},color:b,children:"Tin nh\u1EAFn \u0111\xE3 b\u1ECB x\xF3a"}):validURL(e)?jsx(PreviewLink,{url:e,isMyMessage:r}):jsxs(Box,{children:[!!(l==null?void 0:l.length)&&jsx(ImageMessage,{isMyMessage:r,files:l,onPress:_=>{c(_),u(!0)}}),!!e&&jsx(Text,{pt:(l==null?void 0:l.length)?2:0,letterSpacing:.1,fontSize:{base:"15px"},textAlign:"left",color:b,children:jsx(Highlighter,{searchWords:[d],autoEscape:!0,textToHighlight:e||"Tin nh\u1EAFn \u0111\xE3 b\u1ECB x\xF3a"})})]})]}),[validURL,r,e,a,n,d,b,l])},useRemoveMessage=()=>{const[e,{loading:r}]=useMutation(REMOVE_MESSAGE),n=useResetQueries();return{onRemoveMessage:react.exports.useCallback(async l=>{try{await e({variables:{id:l}}),await n(["messages"])}catch(c){console.log(c)}},[e,n]),loading:r}},MessageDeleted=({isMyMessage:e,isHoverMessage:r,setIsHoverMessage:n,id:a})=>{const{onRemoveMessage:l}=useRemoveMessage(),c=async()=>{await l(a),n(!1)};return e&&r&&jsxs(Menu,{isLazy:!0,boundary:document.getElementById("chat-box"),children:[jsx(MenuButton,{display:"flex",alignItems:"center",justifyContent:"center",rounded:"full",cursor:"pointer",w:4,h:4,borderWidth:1,borderColor:useColorModeValue("blackAlpha.800","white"),children:jsx(Box,{display:"flex",alignItems:"center",justifyContent:"center",children:jsx(FiMoreHorizontal,{})})}),jsx(Portal,{children:jsx(MenuList,{alignItems:"center",py:4,children:jsx(MenuItem,{onClick:c,icon:jsx(Icon,{as:AiOutlineDelete,w:4,h:4}),children:"X\xF3a tin nh\u1EAFn n\xE0y"})})})]})},MessageStatus=({lastSeenMessageId:e,id:r,userData:n,isMyMessage:a,notSeen:l,loading:c,error:u,isGroup:d})=>{const m=useColorModeValue("white","slate.300");return react.exports.useMemo(()=>e===r&&!d?jsx(Box,{w:4,h:4,rounded:"full",alignItems:"center",justifyContent:"center",display:"flex",flexDirection:"column",overflow:"hidden",bg:m,children:jsx(MyAvatar,{src:n==null?void 0:n.avatar,name:n==null?void 0:n.fullName,size:"xs"})}):a&&l||c||e===r&&d?jsx(Box,{w:4,h:4,rounded:"lg",alignItems:"center",justifyContent:"center",display:"flex",flexDirection:"column",bg:c||u?"transparent":"gray.400",border:c||u?"1px solid #838b8b":void 0,children:jsx(BiCheck,{fill:"#fff",className:"w-3 h-3"})}):null,[e,r,u,c,l,a,d,n])},ImageViewer=({attachments:e,attachmentIndex:r,setAttachmentIndex:n,showImageViewer:a,setShowImageViewer:l})=>jsx(Box,{w:"100%",h:"100%",position:"relative",children:jsx(ImgsViewer,{imgs:e.map(c=>{var u;return{src:(u=c==null?void 0:c.attachment)==null?void 0:u.fullUrl}}),currImg:r,isOpen:a,onClose:()=>l(!1),showImgCount:!1,backdropCloseable:!0,onClickPrev:()=>{r>1&&n(r-1)},onClickNext:()=>{r<(e==null?void 0:e.length)-1&&n(r+1)}})}),MarkAsQuestion=({isParticipant:e,onOpen:r})=>e&&jsxs(Menu,{isLazy:!0,boundary:document.getElementById("chat-box"),children:[jsx(MenuButton,{display:"flex",alignItems:"center",justifyContent:"center",rounded:"full",cursor:"pointer",borderWidth:1,borderColor:useColorModeValue("blackAlpha.800","white"),w:4,h:4,mt:"-20px",children:jsx(Box,{display:"flex",alignItems:"center",justifyContent:"center",children:jsx(FiMoreHorizontal,{})})}),jsx(Portal,{children:jsx(MenuList,{alignItems:"center",py:4,children:jsx(MenuItem,{onClick:()=>{r()},icon:jsx(Icon,{as:BsQuestionCircle,w:4,h:4}),children:"\u0110\xE1nh d\u1EA5u c\xE2u h\u1ECFi"})})})]});var ring="data:audio/mpeg;base64,SUQzBAAAAAACbFRFTkMAAAANAAADTG9naWMgUHJvIFgAVERSQwAAAAwAAAMyMDE0LTExLTA5AFRYWFgAAAARAAADY29kaW5nX2hpc3RvcnkAAFRYWFgAAAAaAAADdGltZV9yZWZlcmVuY2UAMTU4NzYwMDAwAFRYWFgAAAEJAAADdW1pZAAweDAwMDAwMDAwMTU2NDhEODQyMUZBMDBGQzcwQjY0MDU4RkY3RjAwMDAwMEZBODMwMDAwNjAwMDAwODA0RjEyMDAwMDAwMDAwMDM2QjI1NzA4MDEwMDAwMDA3MDcwRDI3OEZGN0YwMDAwQjBBRTQwNThGRjdGMDAwMEVBRTZEQzhFAFRTU0UAAAAPAAADTGF2ZjU4Ljc2LjEwMABDVE9DAAAACgAAdG9jAAMBY2gwAENIQVAAAAAsAABjaDAAAAAAAAAAAH3//////////1RJVDIAAAAOAAADVGVtcG86IDEyMC4wAAAAAAAAAAAAAAD/+5AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABJbmZvAAAADwAAAAYAAAtsAElJSUlJSUlJSUlJSUlJSUltbW1tbW1tbW1tbW1tbW1tbZKSkpKSkpKSkpKSkpKSkpK2tra2tra2tra2tra2tra2ttvb29vb29vb29vb29vb29vb/////////////////////wAAAABMYXZjNTguMTMAAAAAAAAAAAAAAAAkAzgAAAAAAAALbGkpEGgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD/+5BkAA/wAABpAAAACAAADSAAAAEAAAGkAAAAIAAANIAAAAQCAQFAIBgQMEAAAD/BAACCQB4xqZo+NkGUSf8wEYAgMBqAFDu03xU4g5WM/zAPQD8vkYrwEFmIthKuDAAhjAD9baEDbOW3iUxkyYAxiDgA5U0HA5jr79i4Yk+BiZFIBjpJOBgxO8BrYNx8ky5wMsouwMAYWgMOALAMT4GwMKwTv0G3hk4GBYIAGGAKQAwTA3AGAIAX/+KIAUBAqpgFAMIRL//9AskTWZm9Bv//////////5o1BaczN3V/////////sTZPuAgIAwGBAGGAAAAB/1Y8YLg3xjUCp/7iM0NXU5cz8CdP9/8jC+A5MJMEPPoAcVYoGbQ/03gYEBAGn1aBqMu/4ZGAJAINQgBigHf+AMIxahOAceJQ//WQwWQVFIf/7GmXGoFAoFAoFAoEAZKKAAAMIKGKjez/1FfxgTQqGYAGAHGALCRpsh0VoYNGG+mIkABBCATmJ5ADxCAgmH3AahiGRZsJAOBgAIHEJALhgCIDgcOyDxiTgHGHwAQ3/+5JkvoAAAABpBQAACAAADSCgAAEYfhEPmfsAAYCYorM9UAA0FEcwgwow4DCuVbNhMXqLiIEkZAI6DAITBqAAMMMCQwFwFjAIIoMg0feI0j9DwOTeeVQBRGAMYGQExgWgJmB2D2ZF4EZfSAK7OFbpbRyZYIaARvLNFASDARBJZKz8lAC76rs94aqrU5vxICyAaq52E/Ua/Yzqxr/bJP45Rn9VpN388P/+W//9//43h4z////////+z/+3AcDN9rI5Ta25/gAAAAwHAGCMflQT/gn5KZWweGGBTgusm+TeYHcAQGCABaHJJysBueN0AoN4kJagYFQSAYFimgZfQmV9gGAdCCwIA9AYcw299aguADbwDQFgND3IYqgl1HpVDWBbs1MyU3+hyQaF5/UAAC8GIwCEKjAUBLMJAL0wzgszEkLhO/oIgyAg+TDWCxMNUEYwOQkDAMA8QjSgfhfRUAAMD4J0DiM1A2ARQMEAMUIFzwpZEumJYDLgX6AwaXQNfCwGC4QVI4dykEiyKRDGoGAhOCygCw8R+NM6RNRFDYyI8G6Q//uSZOuAB347zu5/wARxZPttz9mAl12jLn3qgAC3CyfPttACKBYAoQBqgtGSS1IJGQyw0wuSLnK5gzv1FkWSOA4zJbrmQ5xiXzek/SMiaOvdm+kqm6Leup/1///////6kv9SkDb+vY1SAICgA12UspYiFBMbRAwJXSzZ9pjvP/TogOcL6jf9Q/hwHv9ZiWB/w6c+76P/sQAIAAAwFKU3CUBMwBAGAYE+YVgvBicpImFsEYYfgsY3DQdT1WbnmeZig0GEyGBOmYBgHKBIMYD1DJQDArq0UzZp4y4S5hRCa4kZOqSYzlqx4Unw1x05PjynnASMM+mAga9dpFKk9y+ihpaNAAkg1hY6iDX4fzz/Hf1Iaf5YZscMz0MRiUUljdqtGq9jPe/1reH9wrc1nb0xgKg24Waz///////9BAACAoAANAAsoR6UQAbayyXP4Z54kG31F1kzwbIRnsv500H8iku7f0i8cioqDjkfV8tBE7LoFXsGAKYfkcZZiwYTCeFAMMIQgMyH7NlsqMU0BMRCgMRgHSSdKhUFMCkNWWU3fiB52v/7kmSFioUjM0tL3dHAMcLZ7Q8tGBHg2Syu6S3QvgompBysYDxwn4FSOSSBAsAGjutAeROE8NUgMsCcl55RvsqpFMmncuYyhh0bcKHyzqAguOgwxRd665BK5+pdbkkpUsiaIR0VCIhNkCC/63/c9bLKWt0otwAjrv//////9ECAFAHdlaSazo1I0CGzQ6FQVVIFuNpKHU+r//hyJQDoaQHg6CZvw7nmAoFgG/+gcgIs63QPWQgOqgQ0vMcUGCIC+YPY7Bmlo4GWSbGYzYfZhXgemAAAQoG09fip4hhLpc/0klkttx6XwQ7agBn2RiABoTQxBPERD0xvnRoBhihgOENIg2Buy6tlczorfzUFuaj0XiROBgEOAl5ky09Gsqbv3Xhh/L3cOf3LeVWzKo1Go1PSqjltNet4WEGBOpCCwijf/////9B48AABwIYB9AIKYCcERkDZzDTtQMXXvUzun/e7/j9g3HCcRRKNQeCYaHI/QDQMgsLF/6gMYIF76tal+StyaykKXSMFAPMNyQMRzpNzGPOEU8NPDEMagoJUjYSDIGL/+5JkegLUxTFJCx7SIDJC6XkLS0QREMcgtdyAAMOMpIqycACWaGfpKszQV7FuWxrHe9xhXQMLDgDbqOtYYxCSzkUAKJpjlr2Jy+3bt51abHLHWd+l1nehqPsNWFZ1G12vQ+7oSl343Y3+H/zvOfv8t45VgVBUiEhdX///////8aKgI9Bn////6xxtRlI4RjnI6d6BoALUEwOBY1X1Tsd84UkABgDgNDYOBHEsmf/dgqeCQsXqDMZAAAiUKAuZAAAvVC2ixZp3oZLJGBwCYUAM25RgITmyKmDguYvei5R0AAFSUXKDioAEAwwMG/FgfJAwGgWGLw4MNlOitSJjHk4OoNsEfCCBNksTJMC5i8DQIE+buNgPSC94uMhxkbENMSdTTdM4kFlAhEI0EFw+oavI5iaJ4pG5RIsRBuXhS45AsAyhFRZZFTE1JpReWy/5dJ8uFsijE4WSfrRRYySSMf/mZsTheJxMqF83L5yjWpJTmK0f/+Thw8XzUvqK5gaFxEvmJ9VJ1LRWySkl1v////zQ2Ln/8aWAAEAAA/8Mfy/Knnfy//uSZHaABwJ2SeZyoABAw2kVzMgAi0Q43lyTAAiPhJnLhhAAZCa28XC8FWaBdURVEXKRtzc6fRo1so0SQ9KPz7GRXTYtHCADAfuwsML/6UnBr/+uhH//6YAGukSJEilcVURM8qCIImSIEgk4GARKnIkaimjQXEFNhDAoKbBWwU3I7//+BT8TYoL4VwK////i9BTcjoQV0F6C////hXAo6KbFBfCvO//4FeC4goqEMhBXgoiwBszMfqzGoCAkGAgkDQdKnUdYKuLHiwdiVYK/+s6JQ1BqHUxBTUUzLjEwMKqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqg==";const useSendReactMessage=e=>{const r=new Audio(ring);r.volume=1;const[n]=useMutation(SEND_REACT_MESSAGE);return{onReactMessage:react.exports.useCallback(async(l,c)=>{try{await n({variables:{messageId:c,emoji:l}}),await e(!1),await r.play()}catch(u){console.log(u)}},[])}},emojiConfig=["http://chat-media.zim.vn/628dd69bb37316154a08c0ee/like-removebg-preview.png","http://chat-media.zim.vn/628dd6c1b37316154a08c0f6/heart-removebg-preview.png","http://chat-media.zim.vn/628dd718b37316154a08c113/emoji__1_-removebg-preview.png","http://chat-media.zim.vn/628dd733b37316154a08c11c/emoji-removebg-preview.png","http://chat-media.zim.vn/628dd751b37316154a08c128/sad-removebg-preview.png","http://chat-media.zim.vn/628dd76bb37316154a08c12d/happy__1_-removebg-preview.png","http://chat-media.zim.vn/628dd6e4b37316154a08c107/angry-removebg-preview.png"],MessageReact=({setIsHoverMessage:e,id:r})=>{const{onReactMessage:n}=useSendReactMessage(e);return jsxs(Menu,{isLazy:!0,boundary:document.getElementById("chat-box"),children:[jsx(MenuButton,{display:"flex",alignItems:"center",justifyContent:"center",rounded:"full",cursor:"pointer",w:4,h:4,children:jsx(Box,{display:"flex",alignItems:"center",justifyContent:"center",children:jsx(BsEmojiSmile,{className:"w-full h-full"})})}),jsx(Portal,{children:jsx(MenuList,{display:"flex",alignItems:"center",justifyContent:"center",bg:"white",rounded:"md",shadow:"sm",zIndex:1e3,children:jsx(HStack,{alignItems:"center",children:emojiConfig.map((a,l)=>jsx(Box,{w:5,h:5,rounded:"full",bg:"white",display:"flex",alignItems:"center",justifyContent:"center",cursor:"pointer",onClick:()=>n(a,r),children:jsx(Image$1,{_hover:{transition:"all 0.3s ease-in-out",transform:"scale(1.3)"},flex:1,src:a})},l))})})})]})},RenderReactionMessage=react.exports.memo(({emojis:e})=>{var a,l;const[r,n]=react.exports.useState([]);return react.exports.useEffect(()=>{(async function(){try{const c=await getUsersInfo(e==null?void 0:e.map(u=>u.userId),!1);n(c)}catch(c){console.log(c)}})()},[e]),jsx(HStack,{alignItems:"center",spacing:2,children:(l=(a=uniqBy_1(e,"emoji"))==null?void 0:a.slice(0,3))==null?void 0:l.map(c=>jsx(Tooltip,{openDelay:300,bg:"green.400",p:2,hasArrow:!0,label:jsx(VStack,{maxW:"150px",children:jsxs(HStack,{children:[jsx(Stack,{children:e==null?void 0:e.map((u,d)=>jsx(Box,{display:"flex",alignItems:"center",justifyContent:"center",w:4,h:4,cursor:"pointer",fontSize:"16px",children:jsx(Image$1,{flex:1,src:u==null?void 0:u.emoji})},d))}),jsx(Stack,{children:r==null?void 0:r.map((u,d)=>jsx(Text,{color:"white",fontWeight:600,children:u==null?void 0:u.full_name},d))})]})}),children:jsx(Box,{display:"flex",alignItems:"center",justifyContent:"center",w:4,h:4,cursor:"pointer",fontSize:"16px",children:jsx(Image$1,{flex:1,src:c==null?void 0:c.emoji})})},c==null?void 0:c.emoji))})}),CustomTableCell=TableCell.extend({addAttributes(){var e,r,n,a,l;return H(N({},(e=this.parent)==null?void 0:e.call(this)),{backgroundColor:{default:(n=(r=this.options)==null?void 0:r.backgroundColor)!=null?n:null,parseHTML:c=>c.getAttribute("data-background-color"),renderHTML:c=>({"data-background-color":c.backgroundColor,style:`background-color: ${c.backgroundColor}`})},style:{default:(l=(a=this.options)==null?void 0:a.style)!=null?l:null,parseHTML:c=>c.getAttribute("colwidth"),renderHTML:c=>({style:`${c.style?`width: ${c.colwidth}px`:null}`,colspan:c.colspan,rowspan:c.rowspan,colwidth:c.colwidth})}})}}),CustomTable=Table$1.extend({renderHTML({HTMLAttributes:e}){return["div",{class:"table-tiptap"},["table",e,["tbody",0]]]}}),CustomBlockquote=Blockquote.extend({content:"paragraph*",addAttributes(){var e;return H(N({},(e=this.parent)==null?void 0:e.call(this)),{cite:{default:this.options.cite},class:{default:this.options.class},title:{default:this.options.title}})}}),CustomLink=Link$3.extend({content:"paragraph*",addAttributes(){var e,r,n,a,l,c,u,d;return H(N({},(e=this.parent)==null?void 0:e.call(this)),{rel:{default:(n=(r=this==null?void 0:this.options)==null?void 0:r.rel)!=null?n:"noopener nofollow noreferrer"},target:{default:(l=(a=this==null?void 0:this.options)==null?void 0:a.target)!=null?l:"_blank"},"data-contextual":{default:((c=this==null?void 0:this.options)==null?void 0:c["data-contextual"])||void 0},"data-contextual-tag":{default:((u=this==null?void 0:this.options)==null?void 0:u["data-contextual-tag"])||void 0},"data-contextual-tag-id":{default:((d=this==null?void 0:this.options)==null?void 0:d["data-contextual-tag-id"])||void 0}})}}),CustomImage=Image$2.extend({addAttributes(){var e,r,n,a,l;return H(N({},(e=this.parent)==null?void 0:e.call(this)),{alt:{default:(n=(r=this==null?void 0:this.options)==null?void 0:r.alt)!=null?n:"image-alt"},title:{default:(l=(a=this==null?void 0:this.options)==null?void 0:a.title)!=null?l:"image-title"}})}}),ButtonIcon=react.exports.forwardRef((d,u)=>{var m=d,{activeKey:e="",activeOptions:r={},isActive:n=!1,icon:a,label:l=""}=m,c=Te(m,["activeKey","activeOptions","isActive","icon","label"]);return a?jsx(Tooltip,{label:l,children:jsx(IconButton,N({ref:u,colorScheme:n?"brand":"gray",variant:"solid",fontSize:"24px",icon:a},c))}):jsx(Tooltip,{label:l,children:jsx(Button$1,H(N({ref:u,colorScheme:n?"brand":"gray",variant:"solid"},c),{children:c.children}))})});var ButtonIcon$1=react.exports.memo(ButtonIcon);const headings=[{name:"H1",value:1},{name:"H2",value:2},{name:"H3",value:3},{name:"H4",value:4},{name:"H5",value:5},{name:"H6",value:6}],MenuHeading=react.exports.memo(()=>{const{editor:e}=react.exports.useContext(EditorContext);return e?jsx(Fragment,{children:jsxs(Popover,{children:[jsx(PopoverTrigger,{children:jsx(ButtonIcon$1,{label:"H tag",icon:jsx(BiHeading,{})})}),jsx(PopoverContent,{zIndex:99,p:0,children:jsx(PopoverBody,{p:2,children:jsx(Flex,{gap:2,children:headings.map(r=>jsx(ButtonIcon$1,{isActive:e.isActive("heading",{level:r.value}),activeKey:"heading",activeOptions:{level:r.value},onClick:()=>e.chain().focus().toggleHeading({level:r.value}).run(),children:r.name},r.value))})})})]})}):null}),TableMenu=react.exports.memo(({editor:e})=>{if(!e)return null;const r=react.exports.useCallback(()=>{e.chain().focus().insertTable({rows:3,cols:3,withHeaderRow:!0}).run()},[e]),n=react.exports.useCallback(()=>{e.chain().focus().deleteTable().run()},[e]),a=react.exports.useCallback(()=>{e.chain().focus().addColumnAfter().run()},[e]),l=react.exports.useCallback(()=>{e.chain().focus().addColumnBefore().run()},[e]),c=react.exports.useCallback(()=>{e.chain().focus().deleteColumn().run()},[e]),u=react.exports.useCallback(()=>{e.chain().focus().addRowBefore().run()},[e]),d=react.exports.useCallback(()=>{e.chain().focus().addRowAfter().run()},[e]),m=react.exports.useCallback(()=>{e.chain().focus().deleteRow().run()},[e]),b=react.exports.useCallback(()=>{e.chain().focus().mergeCells().run()},[e]),A=react.exports.useCallback(()=>{e.chain().focus().splitCell().run()},[e]),_=react.exports.useCallback(()=>{e.chain().focus().toggleHeaderColumn().run()},[e]);return jsxs(SimpleGrid,{minChildWidth:38,gap:2,p:2,bg:"gray.50",w:"full",maxW:350,children:[jsx(ButtonIcon$1,{onClick:r,icon:jsx(GrTableAdd,{}),label:"Th\xEAm table"}),jsx(ButtonIcon$1,{onClick:n,icon:jsx(AiOutlineClear,{}),label:"X\xF3a table"}),jsx(ButtonIcon$1,{onClick:a,icon:jsx(RiInsertColumnRight,{}),label:"Th\xEAm c\u1ED9t b\xEAn ph\u1EA3i"}),jsx(ButtonIcon$1,{onClick:l,icon:jsx(RiInsertColumnLeft,{}),label:"Th\xEAm c\u1ED9t b\xEAn tr\xE1i"}),jsx(ButtonIcon$1,{onClick:c,icon:jsx(RiDeleteColumn,{}),label:"X\xF3a c\u1ED9t"}),jsx(ButtonIcon$1,{onClick:u,icon:jsx(RiInsertRowTop,{}),label:"Th\xEAm h\xE0ng ph\xEDa tr\xEAn"}),jsx(ButtonIcon$1,{onClick:d,icon:jsx(RiInsertRowBottom,{}),label:"Th\xEAm h\xE0ng b\xEAn d\u01B0\u1EDBi"}),jsx(ButtonIcon$1,{onClick:m,icon:jsx(RiDeleteRow,{}),label:"X\xF3a h\xE0ng"}),jsx(ButtonIcon$1,{onClick:b,icon:jsx(RiMergeCellsHorizontal,{}),label:"G\u1ED9p c\xE1c \xF4"}),jsx(ButtonIcon$1,{onClick:A,icon:jsx(RiSplitCellsHorizontal,{}),label:"T\xE1ch c\xE1c \xF4"}),jsx(ButtonIcon$1,{onClick:_,icon:jsx(AiOutlineInsertRowLeft,{}),label:"Toggle header c\u1ED9t"}),jsx(ButtonIcon$1,{onClick:()=>e.chain().focus().toggleHeaderRow().run(),icon:jsx(AiOutlineInsertRowAbove,{}),label:"Toggle header h\xE0ng"}),jsx(ButtonIcon$1,{onClick:()=>e.chain().focus().toggleHeaderCell().run(),icon:jsx(RiCellphoneFill,{}),label:"Toggle header cell"})]})}),MenuTable=react.exports.memo(({editor:e})=>e?jsxs(Popover,{children:[jsx(PopoverTrigger,{children:jsx(ButtonIcon$1,{label:"Table",icon:jsx(BiTable,{})})}),jsx(PopoverContent,{zIndex:99,p:0,children:jsx(PopoverBody,{p:0,children:jsx(TableMenu,{editor:e})})})]}):null),ImageCheckbox=e=>{const m=e,{isMultiple:r,children:n}=m,a=Te(m,["isMultiple","children"]),{getInputProps:l,getCheckboxProps:c}=r?useCheckbox(a):useRadio(a),u=l(),d=c();return jsxs(Box,{as:"label",rounded:"base",overflow:"hidden",d:"block",children:[jsx("input",H(N({},u),{style:{display:"none"}})),jsxs(Box,H(N({},d),{cursor:"pointer",opacity:.7,_checked:{borderColor:"red.500",opacity:1},_focus:{boxShadow:"none"},pos:"relative",children:[u.checked&&jsx(CheckCircleIcon,{color:"green.500",fontSize:"2xl",pos:"absolute",top:2,left:2,zIndex:2}),n]}))]})},fetchAndStoreAccessToken=async()=>{const e=appGetAuthToken(),r=o(e);if(!appGetAuthToken()||appGetAuthToken()&&Date.now()>=r*1e3){const{token:n}=await fetchAccessToken();n&&appSetAuthToken(n)}};async function uploadFilesPromise(e){const n=`${getEnviroment("ZIM_IMAGE_ENDPOINT")}/upload`;try{if(e)return await fetchAndStoreAccessToken(),await(await fetch(n,{method:"POST",credentials:"include",headers:{Authorization:`Bearer ${appGetAuthToken()}`,Accept:"*/*","x-no-compression":"true"},body:e})).json()}catch(a){console.log("uploadFiles : "+a)}}const MediaFragment=gql`
  fragment Media on Media {
    id
    type
    path
    variants {
      id
      width
      height
      path
      type
    }
    filename
    title
    visibility
    width
    height
  }
`,GET_MEDIAS=gql`
  query medias(
    $first: Int
    $after: String
    $type: [MediaType]
    $width: Int
    $height: Int
    $search: String
    $userId: Int
  ) @api(name: "zim") {
    medias(
      first: $first
      after: $after
      type: $type
      width: $width
      height: $height
      search: $search
      userId: $userId
    ) {
      edges {
        node {
          ...Media
        }
      }
      pageInfo {
        startCursor
        endCursor
        hasNextPage
      }
    }
  }
  ${MediaFragment}
`,isReachBottom=e=>e.scrollHeight-e.scrollTop<=e.clientHeight+10,useDebounce=(e,r)=>{const[n,a]=react.exports.useState(e);return react.exports.useEffect(()=>{const l=setTimeout(()=>a(e),r);return()=>clearTimeout(l)},[e,r]),n};var index$4="";function useRouter(){const e=useParams(),r=useLocation(),n=useNavigate();return react.exports.useMemo(()=>({navigate:n,pathname:r.pathname,query:N(N({},queryString.parse(r.search)),e),location:r}),[e,r,n])}const Select=react.exports.forwardRef((u,c)=>{var d=u,{onChange:e,paramKey:r,options:n,allOption:a}=d,l=Te(d,["onChange","paramKey","options","allOption"]);var T;useTheme();const m=useRouter();useSearchParams();const b=react.exports.useMemo(()=>({menuPortal:C=>H(N({},C),{zIndex:1400}),container:(C,{isDisabled:S,isHovered:P,isSelected:k})=>H(N({},C),{color:useColorModeValue("var(--chakra-colors-gray-900)","var(--chakra-colors-slate-400)")}),placeholder:(C,S)=>H(N({},C),{color:useColorModeValue("var(--chakra-colors-gray-400)","var(--chakra-colors-slate-500)")}),input:(C,S)=>H(N({},C),{minHeight:30,color:useColorModeValue("var(--chakra-colors-gray-900)","var(--chakra-colors-slate-400)")}),control:(C,S)=>H(N({},C),{backgroundColor:S.isDisabled?useColorModeValue("var(--chakra-colors-gray-50)","var(--chakra-colors-slate-500)"):useColorModeValue("var(--chakra-colors-white)","var(--chakra-colors-slate-700)"),borderRadius:"var(--chakra-radii-md)",borderColor:useColorModeValue("var(--chakra-colors-gray.300)","var(--chakra-colors-slate-600)")}),menu:(C,S)=>H(N({},C),{zIndex:3}),menuList:(C,S)=>H(N({},C),{backgroundColor:useColorModeValue("var(--chakra-colors-white)","var(--chakra-colors-slate-700)")}),option:(C,S)=>H(N({},C),{color:S.isSelected?useColorModeValue("white","currentColor"):S.isFocused?useColorModeValue("var(--chakra-colors-gray.900)","black"):"currentColor","&:hover":{color:S.isSelected?useColorModeValue("var(--chakra-colors-white)","var(--chakra-colors-white)"):S.isFocused?useColorModeValue("var(--chakra-colors-gray.900)","var(--chakra-colors-slate-900)"):useColorModeValue("var(--chakra-colors-gray.900)","var(--chakra-colors-slate-900)")},"&:focus":{color:"red"}}),singleValue:(C,S)=>H(N({},C),{color:useColorModeValue("var(--chakra-colors-gray.900)","var(--chakra-colors-slate-300)")})}),[]),A=react.exports.useCallback((C,S)=>{e(C,S)},[l,m.query,r]),_=react.exports.useMemo(()=>a?[a,...n]:n,[l,n,a]);return jsxs(FormControl,{isInvalid:!!(l==null?void 0:l.error),width:"100%",children:[(l==null?void 0:l.label)&&jsx(FormLabel,{htmlFor:l.id,children:l==null?void 0:l.label}),jsx(Select$2,H(N({ref:c,id:l.id},l),{styles:b,onChange:A,options:_,menuPortalTarget:document.body})),(l==null?void 0:l.error)&&jsx(FormErrorMessage,{children:(T=l==null?void 0:l.error)==null?void 0:T.message})]})});var Select$1=react.exports.memo(Select);const mediaTypeSelections=[{label:"T\u1EC7p tin",value:"file"},{label:"H\xECnh \u1EA3nh",value:"image"}],UploadModal=({selectedCallback:e,onClose:r,isOpen:n,isMultiple:a=!0,defaultTypes:l=["image"],enableTypeFilter:c,enableSizeFilter:u,width:d,height:m})=>{var K,Z,Y,le,re,ee,oe,me;const[b,A]=react.exports.useState(!1),[_,T]=react.exports.useState([]),[C,S]=react.exports.useState(!1),[P,k]=useToggle$1(!0),[x,E]=react.exports.useState(""),g=useDebounce(x,500),[y,I]=react.exports.useState([...l]),[M]=useUser(),j={first:20,type:sortBy_1(y),search:g,height:0,width:0,userId:parseInt(M.id,10)};P&&d&&m&&(j.width=d,j.height=m);const{data:O,fetchMore:R,refetch:B,loading:G}=useQuery(GET_MEDIAS,{variables:j,notifyOnNetworkStatusChange:!0,skip:!n}),V=async()=>{A(!0);try{await B()}catch(ne){console.log({e:ne})}A(!1)},Q=async()=>{const ne=document.createElement("input");ne.setAttribute("type","file"),y.includes("file")||ne.setAttribute("accept","image/*"),ne.click(),ne.onchange=async()=>{const ce=ne.files[0],fe=new FormData;fe.append("file",ce),fe.append("visibility","public"),await uploadFilesPromise(fe),await V()}},{getCheckboxProps:W,setValue:q}=useCheckboxGroup({onChange:T}),{getRootProps:L,getRadioProps:D}=useRadioGroup({name:"images",onChange:(...ne)=>T(ne)}),F=L(),$=()=>{var ne;e((ne=O==null?void 0:O.medias)==null?void 0:ne.edges.filter(ce=>_.includes(ce.node.id))),q([]),r()},U=()=>{r(),q([])},z=ne=>{S(isReachBottom(ne.target))};return react.exports.useEffect(()=>{var ne;C&&typeof R=="function"&&((ne=O.medias)==null?void 0:ne.pageInfo.hasNextPage)&&!G&&(async()=>{var ce;await R({variables:{after:(ce=O.medias)==null?void 0:ce.pageInfo.endCursor,first:20,type:sortBy_1(y)}})})()},[C]),jsx(Fragment,{children:jsxs(Modal$1,{closeOnOverlayClick:!1,isCentered:!0,isOpen:n,onClose:U,size:"6xl",children:[jsx(ModalOverlay,{}),jsxs(ModalContent,{children:[jsx(ModalHeader,{children:"Th\u01B0 vi\u1EC7n media"}),jsx(ModalCloseButton,{}),jsxs(ModalBody,{children:[jsxs(HStack,{mb:4,justifyContent:"space-between",children:[jsx(Button$1,{as:Link$1,variant:"outline",colorScheme:"black",onClick:Q,leftIcon:jsx(AiOutlineCloudUpload,{}),isLoading:b,children:"Select from computer"}),c&&jsx(Box,{width:300,children:jsx(Select$1,{placeholder:"Ch\u1ECDn lo\u1EA1i media",isMulti:!0,value:y.map(ne=>mediaTypeSelections.find(ce=>ce.value===ne)).filter(ne=>ne),onChange:ne=>{I(ne.map(ce=>ce.value))},options:mediaTypeSelections})}),u&&d&&m&&jsxs(HStack,{children:[jsxs(Text,{children:["Ch\u1EC9 filter nh\u1EEFng \u1EA3nh size ",d," x ",m]}),jsx(Switch,{isChecked:P,onChange:k})]})]}),jsxs(Box,{width:350,pos:"relative",role:"group",children:[jsx(Input$1,{_focus:{color:"gray.900",borderColor:"gray.900"},_groupHover:{color:"gray.900",borderColor:"gray.900"},type:"text",placeholder:"T\xECm ki\u1EBFm media",size:"lg",pr:12,mb:4,value:x,onChange:ne=>E(ne.target.value)}),jsx(SearchIcon$1,{_groupHover:{color:"gray.900"},pos:"absolute",right:4,top:"50%",color:"gray.400",transform:"translateY(-50%)"})]}),jsxs(Box,{position:"relative",h:"calc(65vh - 50px)",overflow:b?"hidden":"auto",w:"full",flexGrow:1,p:3,onScroll:z,children:[a?jsx(HStack,{alignItems:"stretch",flexWrap:"wrap",mx:"-0.75rem",spacing:0,children:((Z=(K=O==null?void 0:O.medias)==null?void 0:K.edges)==null?void 0:Z.length)>0&&((le=(Y=O==null?void 0:O.medias)==null?void 0:Y.edges)==null?void 0:le.map(({node:ne})=>{const ce=W({value:ne.id});return jsx(Box,{w:{lg:"25%",base:"50%"},p:3,children:jsx(ImageCheckbox,H(N({isMultiple:a},ce),{children:jsx(Media,N({},ne))}))},`${ne.id}`)}))}):jsx(HStack,H(N({alignItems:"stretch",flexWrap:"wrap",mx:"-0.75rem",spacing:0},F),{children:((ee=(re=O==null?void 0:O.medias)==null?void 0:re.edges)==null?void 0:ee.length)>0&&((me=(oe=O==null?void 0:O.medias)==null?void 0:oe.edges)==null?void 0:me.map(({node:ne})=>{const ce=D({value:ne.id});return jsx(Box,{w:{lg:"25%",base:"50%"},p:3,children:jsx(ImageCheckbox,H(N({isMultiple:!1},ce),{children:jsx(Media,N({},ne))}))},`${ne.id}`)}))})),b&&jsxs(HStack,{bgColor:"whiteAlpha.800",pos:"absolute",top:0,bottom:0,right:0,left:0,spacing:2,justifyContent:"center",children:[jsx(Spinner,{color:"black",size:"xl"}),jsx(Text,{fontSize:"2xl",color:"black",children:"\u0110ang ti\u1EBFn h\xE0nh upload"})]})]})]}),jsxs(ModalFooter,{children:[jsx(Button$1,{colorScheme:"blue",mr:3,disabled:!_.length,onClick:$,children:"X\xE1c nh\u1EADn"}),jsx(Button$1,{colorScheme:"gray",mr:0,onClick:U,children:"\u0110\xF3ng l\u1EA1i"})]})]})]})})};function Media({type:e,path:r,filename:n}){if(e==="file")return jsx(Box,{display:"flex",flexDirection:"column",justifyContent:"center",borderWidth:2,borderColor:"#ccc",borderRadius:10,paddingX:"12px",w:"full",h:"150",children:jsxs(VStack,{spacing:6,width:"full",overflow:"hidden",children:[jsx(AiOutlineFile,{fontSize:"40"}),jsx(Box,{w:"full",display:"table",style:{tableLayout:"fixed"},children:jsx(Text,{display:"table-cell",textAlign:"center",whiteSpace:"nowrap",textOverflow:"ellipsis",overflow:"hidden",children:n})})]})});const a=getEnviroment("ZIM_IMAGE_ENDPOINT");return jsx(Box,{borderColor:"#ccc",borderRadius:10,borderWidth:2,w:"full",h:"150",backgroundSize:"cover",backgroundRepeat:"no-repeat",backgroundImage:`url('${urlJoin(a,r)}')`})}const MenuImage=react.exports.memo(({editor:e})=>{const r=d=>{d&&e.chain().focus().setImage({src:d}).run()},[n,a]=react.exports.useState(!1);return jsxs(Fragment,{children:[jsx(ButtonIcon$1,{activeKey:"image",onClick:()=>{a(!0)},icon:jsx(BiImages,{}),label:"H\xECnh \u1EA3nh"}),jsx(UploadModal,{isOpen:n,onClose:()=>{a(!1)},selectedCallback:d=>{d.map(m=>{r(`${getEnviroment("ZIM_IMAGE_ENDPOINT")}/${m.node.path}`)})},isMultiple:!0})]})}),useActiveGroupStore=create$1(e=>({activeGroup:null,setActiveGroup:r=>e({activeGroup:r}),clearActiveGroup:()=>e({activeGroup:{}})})),MenuAddFillTextSpace=react.exports.memo(({editor:e})=>{const r=useActiveGroupStore(m=>m.activeGroup);if(lodash.exports.isEmpty(r))return null;const{onOpen:n,onClose:a,isOpen:l}=useDisclosure(),[c,u]=react.exports.useState({questionGroupId:0,type:"",question:"",questionIndex:0,questionCount:1,answers:[]});if(!e)return null;const d=({questionId:m,questionGroupId:b})=>{e.chain().focus().addSpace({"data-question":"","data-question-group":"","data-label":""}).run()};return react.exports.useEffect(()=>{l&&u(H(N({},c),{answers:[]}))},[]),jsx(ButtonIcon$1,{onClick:d,icon:jsx(MdSpaceBar,{}),label:"Th\xEAm space \u0111\xE1p \xE1n"})}),MenuBar=react.exports.memo(({editor:e,stickyMenuBar:r,isMockTestEditor:n,enableSpaceMenu:a})=>{const l=useColorModeValue("white","slate.700"),c=useColorModeValue("gray.200","slate.600");return jsx(Fragment,{children:jsxs(SimpleGrid,{minChildWidth:"38px",gap:2,w:"full",bg:l,zIndex:10,px:2,py:2,pos:r?"sticky":"static",top:0,borderBottom:"1px solid",borderBottomColor:c,children:[jsx(MenuHeading,{}),jsx(ButtonIcon$1,{onClick:()=>e.chain().focus().toggleBold().run(),activeKey:"bold",label:"In \u0111\u1EADm",icon:jsx(BiBold,{})}),jsx(ButtonIcon$1,{onClick:()=>e.chain().focus().toggleItalic().run(),activeKey:"italic",label:"In nghi\xEAng",icon:jsx(BiItalic,{})}),jsx(ButtonIcon$1,{onClick:()=>e.chain().focus().toggleUnderline().run(),activeKey:"underline",label:"G\u1EA1ch ch\xE2n",icon:jsx(AiOutlineUnderline,{})}),jsx(ButtonIcon$1,{onClick:()=>e.chain().focus().toggleStrike().run(),activeKey:"strike",label:"G\u1EA1ch ngang",icon:jsx(BiStrikethrough,{})}),jsx(ButtonIcon$1,{onClick:()=>e.chain().focus().setTextAlign("left").run(),label:"C\u0103n l\u1EC1 tr\xE1i",icon:jsx(BiAlignLeft,{})}),jsx(ButtonIcon$1,{onClick:()=>e.chain().focus().setTextAlign("center").run(),label:"C\u0103n gi\u1EEFa",icon:jsx(BiAlignMiddle,{})}),jsx(ButtonIcon$1,{onClick:()=>e.chain().focus().setTextAlign("right").run(),label:"C\u0103n l\u1EC1 ph\u1EA3i",icon:jsx(BiAlignRight,{})}),jsx(ButtonIcon$1,{onClick:()=>e.chain().focus().toggleBulletList().run(),label:"Danh s\xE1ch bullet",icon:jsx(BiListUl,{})}),jsx(ButtonIcon$1,{onClick:()=>e.chain().focus().toggleOrderedList().run(),label:"Danh s\xE1ch s\u1ED1 th\u1EE9 t\u1EF1",icon:jsx(BiListOl,{})}),jsx(MenuTable,{editor:e}),jsx(MenuImage,{editor:e}),n&&a&&jsx(MenuAddFillTextSpace,{editor:e})]})})});react.exports.memo(({editor:e})=>jsxs(SimpleGrid,{minChildWidth:38,gap:2,p:2,bg:"gray.50",w:{base:300,sm:300,md:450},children:[jsx(MenuHeading,{}),jsx(ButtonIcon$1,{onClick:()=>e.chain().focus().toggleBulletList().run(),label:"Danh s\xE1ch bullet",icon:jsx(BiListUl,{}),isActive:e.isActive("bulletList")}),jsx(ButtonIcon$1,{onClick:()=>e.chain().focus().toggleOrderedList().run(),label:"Danh s\xE1ch s\u1ED1 th\u1EE9 t\u1EF1",icon:jsx(BiListOl,{}),isActive:e.isActive("orderedList")}),jsx(MenuTable,{editor:e}),jsx(MenuImage,{editor:e})]}));const ImageMenu=react.exports.memo(({editor:e})=>{const{onOpen:r,onClose:n,isOpen:a}=useDisclosure(),l=react.exports.useRef(null),[c,u]=react.exports.useState(""),[d,m]=react.exports.useState(""),b=async()=>{const A=e.getAttributes("image").src;e.chain().focus().setImage({src:A,alt:d,title:c}).run(),n()};return react.exports.useEffect(()=>{e.isActive("image")?(m(e.getAttributes("image").alt),u(e.getAttributes("image").title)):(m(""),u(""))},[a]),jsxs(HStack,{bg:"white",shadow:"base",p:2,spacing:4,rounded:4,children:[jsxs(Box,{fontSize:"sm",children:[jsxs(Text,{as:"div",children:[jsx("strong",{children:"Title:"})," ",e.getAttributes("image").title]}),jsxs(Text,{as:"div",children:[jsx("strong",{children:"Alt:"})," ",e.getAttributes("image").alt]})]}),jsxs(Popover,{isOpen:a,initialFocusRef:l,onOpen:r,onClose:n,placement:"top",closeOnBlur:!0,children:[jsx(PopoverTrigger,{children:jsx(ButtonIcon$1,{activeKey:"image",label:"Ch\u1EC9nh s\u1EEDa",icon:jsx(BiEdit,{})})}),jsx(PopoverContent,{children:jsx(Box,{p:4,bg:"white",shadow:"base",children:jsxs(VStack,{spacing:4,alignItems:"flex-start",children:[jsx(TextInput,{ref:l,label:"Title",id:"title-url",value:c,onChange:A=>u(A.target.value),autoComplete:"off"}),jsx(TextInput,{label:"Alt",id:"alt-url",value:d,onChange:A=>m(A.target.value),autoComplete:"off"}),jsx(Button$1,{colorScheme:"teal",onClick:b,children:"C\u1EADp nh\u1EADt"})]})})})]})]})});var Iframe=Node.create({name:"iframe",group:"block",atom:!0,defaultOptions:{allowFullscreen:!0,HTMLAttributes:{class:"iframe-wrapper"}},addAttributes(){return{class:{default:null},src:{default:null},frameborder:{default:0},allowfullscreen:{default:this.options.allowFullscreen,parseHTML:()=>this.options.allowFullscreen},width:{default:this.options.width},height:{default:this.options.height}}},parseHTML(){return[{tag:"iframe"}]},renderHTML({HTMLAttributes:e}){return["div",this.options.HTMLAttributes,["iframe",e]]},addCommands(){return{setIframe:e=>({tr:r,dispatch:n})=>{const{selection:a}=r,l=this.type.create(e);return n&&r.replaceRangeWith(a.from,a.to,l),!0}}}});Node.create({name:"video",group:"block",atom:!0,defaultOptions:{HTMLAttributes:{class:"iframe-wrapper"}},addAttributes(){return{class:{default:this.options.class},src:{default:null},autoplay:{default:this.options.width},loop:{default:this.options.loop},muted:{default:this.options.muted},preload:{default:this.options.preload},width:{default:this.options.width},height:{default:this.options.height},controls:{default:this.options.controls}}},parseHTML(){return[{tag:"video"}]},renderHTML({HTMLAttributes:e}){return["div",this.options.HTMLAttributes,["video",e]]},addCommands(){return{Video:e=>({tr:r,dispatch:n})=>{const{selection:a}=r,l=this.type.create(e);return n&&r.replaceRangeWith(a.from,a.to,l),!0}}}});var TextSpace=Node.create({name:"spacing",content:"inline*",atom:!0,draggable:!0,defaultOptions:{HTMLAttributes:{class:"space-wrapper","data-question":"","data-question-group":"","data-label":"","data-type":""}},addOptions(){return{inline:!0,HTMLAttributes:{},class:"space-wrapper"}},inline(){return this.options.inline},group(){return this.options.inline?"inline":"block"},addAttributes(){var e,r,n,a,l,c,u;return{class:{default:((e=this==null?void 0:this.options)==null?void 0:e.class)||void 0},"data-question":{default:(n=(r=this==null?void 0:this.options)==null?void 0:r["data-question"])!=null?n:"",parseHTML:d=>d.getAttribute("data-question")},"data-question-group":{default:(l=(a=this==null?void 0:this.options)==null?void 0:a["data-question-group"])!=null?l:"",parseHTML:d=>d.getAttribute("data-question-group")},"data-type":{default:(u=(c=this==null?void 0:this.options)==null?void 0:c["data-type"])!=null?u:"",parseHTML:d=>d.getAttribute("data-type")},"data-label":{default:"",parseHTML:d=>d.getAttribute("data-label"),renderHTML:d=>d["data-label"]?{"data-label":d["data-label"]}:{"data-label":""}}}},parseHTML(){return[{tag:"span.space-wrapper"}]},renderHTML({HTMLAttributes:e}){return["span",mergeAttributes(this.options.HTMLAttributes,e)]},renderText({HTMLAttributes:e}){var r;return(r=e==null?void 0:e["data-label"])!=null?r:"r\u1ED7ng"},addCommands(){return{addSpace:e=>({commands:r})=>r.insertContent({type:this.name,attrs:e})}}}),animate="";const TicketFragment=gql`
  fragment Ticket on TicketType {
    id
    refId
    title
    description
    createDate
    status
    createBy {
      id
      user_name
      full_name
      status
      avatar
      supporter
      role_id
      phone
      address
      supporter
    }
    receiver {
      id
      user_name
      full_name
      avatar
      role_id
      phone
      address
      supporter
    }
    reactions {
      id
      ticketId
      reaction
      count
    }
  }
`,CREATE_TICKET=gql`
  mutation createTicket($input: TicketInputType) @api(name: "appZim") {
    createTicket(input: $input) {
      ...Ticket
    }
  }
  ${TicketFragment}
`,UPDATE_TICKET=gql`
  mutation updateTicket($input: TicketInputType) @api(name: "appZim") {
    updateTicket(input: $input) {
      ...Ticket
    }
  }
  ${TicketFragment}
`,UPDATE_TICKET_STATUS=gql`
  mutation updateTicketStatus($input: TicketUpdateStatusType)
  @api(name: "appZim") {
    updateTicketStatus(input: $input) {
      ...Ticket
    }
  }
  ${TicketFragment}
`,CREATE_TICKET_REACTION=gql`
  mutation createTicketReaction($input: TicketReactionInputType)
  @api(name: "appZim") {
    createTicketReaction(input: $input) {
      id
      ticketId
      reaction
      count
    }
  }
  ${TicketFragment}
`,statuses={waiting:{tooltip:"Ticket waiting",icon:MdOutlineQuickreply,color:"red"},longtime:{tooltip:"Longtime no see",icon:HiOutlineEmojiSad,color:"blue"}},BadgeTicketStatusIcon=({type:e="waiting"})=>{const r=react.exports.useMemo(()=>statuses==null?void 0:statuses[e],[e]);return jsx(Tooltip,{hasArrow:!0,bg:useColorModeValue("slate.700","slate.700"),label:r==null?void 0:r.tooltip,"aria-label":"status tool tip",isDisabled:!(r==null?void 0:r.tooltip),color:useColorModeValue("slate.300","slate.300"),children:jsx(HStack,{spacing:1,color:`${r==null?void 0:r.color}.500`,children:jsx(Box,{as:"span",children:(r==null?void 0:r.icon)&&jsx(Icon,{as:r==null?void 0:r.icon})})})})},BoxStyled=newStyled(HStack)`
  .flip-card {
    perspective: 1000px;
    background: transparent;
  }
  .flip-card-inner {
    position: relative;
    width: 100%;
    height: 100%;
    text-align: center;
    transition: transform 0.6s;
    transform-style: preserve-3d;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
  }

  .flip-card:hover .flip-card-inner {
    transform: rotateY(180deg);
  }

  .flip-card-front,
  .flip-card-back {
    position: absolute;
    width: 100%;
    height: 100%;
    -webkit-backface-visibility: hidden;
    backface-visibility: hidden;
  }

  .flip-card-front {
    background-color: #bbb;
    color: black;
  }

  .flip-card-back {
    background-color: #2980b9;
    color: white;
    transform: rotateY(180deg);
    display: flex;
    align-items: center;
    justify-content: center;
  }
`,UserCard=({name:e,avatar:r,meta:n,lastContact:a="",badgeTitle:l="",badgeProps:c,isFlipTicketAvatarCard:u=!1,onClickFlipCard:d=null,avatarSize:m=10,badgeStatusType:b=null})=>jsxs(BoxStyled,{flexGrow:1,alignItems:"flex-start",spacing:4,children:[jsx(Box,{className:"flip-card",cursor:d?"pointer":"normal",width:m,height:m,rounded:"full",onClick:d,flexShrink:0,overflow:"hidden",children:u?jsx(Tooltip,{label:"G\u1EEDi ticket ch\u0103m s\xF3c",children:jsxs(Box,{className:"flip-card-inner",children:[jsx(Box,{className:"flip-card-front",children:jsx(Avatar,{src:r,objectFit:"cover",boxSize:m,flexShrink:0,ignoreFallback:!0})}),jsx(Box,{className:"flip-card-back",children:jsx(Icon,{as:FiSend,width:6,height:6})})]})}):jsx(Avatar,{src:r,objectFit:"cover",boxSize:m,flexShrink:0,ignoreFallback:!0})}),jsxs(VStack,{justifyContent:"stretch",alignItems:"flex-start",spacing:0,children:[jsxs(Flex,{alignItems:"flex-start",gap:2,children:[jsx(NavLink,{to:"/",children:jsx(Text,{fontWeight:600,children:e||""})}),l&&jsx(Badge$1,H(N({size:"sm",colorScheme:"green",variant:"subtle",rounded:4},c),{children:l})),b&&jsx(BadgeTicketStatusIcon,{type:b})]}),jsxs(Flex,{gap:0,fontSize:"sm",direction:"column",color:"gray.500",children:[jsx(Text,{children:n||""}),a&&jsxs(Text,{fontSize:"xs",children:["Last contact ",a||""]})]})]})]}),ReplyCard=({data:e,onTicketUpdate:r,canOperationComment:n})=>{var R;const[a,l]=useToggle(),O=e,{name:c,avatar:u,time:d,status:m,meta:b,question:A}=O,_=Te(O,["name","avatar","time","status","meta","question"]),[T,C]=react.exports.useState(e.question.content),S=react.exports.useRef(),P=useToast(),[k,{loading:x}]=useMutation(UPDATE_TICKET,{onCompleted:B=>{const{updateTicket:G}=B;r(H(N({},e),{question:{title:G==null?void 0:G.title,content:G==null?void 0:G.description}})),P({title:"Th\xE0nh c\xF4ng!",description:"C\u1EADp nh\u1EADt n\u1ED9i dung th\xE0nh c\xF4ng.",status:"success",duration:3e3,isClosable:!0}),l()},onError:B=>{var G;P({title:"Th\u1EA5t b\u1EA1i!",description:`${(G=B==null?void 0:B.message)!=null?G:"L\u1ED7i c\u1EADp nh\u1EADt"}`,status:"error",duration:3e3,isClosable:!0})}}),[E]=useUser(),g=parseInt(E.id,10)===((R=e==null?void 0:e.createBy)==null?void 0:R.id),[y,{loading:I}]=useMutation(UPDATE_TICKET_STATUS,{onCompleted:B=>{const{updateTicketStatus:G}=B;G&&(r(H(N({},e),{status:G.status})),P({title:"Th\xE0nh c\xF4ng!",description:G.status===ETicketStatus.DELETED?"\u0110\xE3 x\xF3a ticket":"C\u1EADp nh\u1EADt tr\u1EA1ng th\xE1i th\xE0nh c\xF4ng.",status:"success",duration:3e3,isClosable:!0}))}}),M=async()=>{var B,G,V;if(S.current){const Q=(G=(B=S==null?void 0:S.current)==null?void 0:B.props)==null?void 0:G.editor;if(!Q||!(Q==null?void 0:Q.getText()))return;const q=Q==null?void 0:Q.getHTML();q&&C(q),await k({variables:{input:{refId:(V=e==null?void 0:e.id)!=null?V:EMPTY_GUID,title:"",description:q}}})}},j=react.exports.useCallback(async()=>{await y({variables:{input:{refId:e.id,status:ETicketStatus.DELETED}}})},[e]);return jsxs(Box,{w:"full",children:[jsx(Flex,{gap:4,children:jsxs(HStack,{justifyContent:"space-between",flexGrow:1,alignItems:"flex-start",children:[jsx(UserCard,{avatar:u,name:c,meta:b,avatarSize:10}),g&&n&&jsxs(Menu,{children:[jsx(MenuButton,{as:IconButton,variant:"ghost",icon:jsx(AiOutlineMore,{fontSize:24})}),jsxs(MenuList,{children:[jsx(MenuItem,{icon:jsx(Icon,{as:AiOutlineEdit,w:4,h:4}),onClick:()=>l(),children:"Ch\u1EC9nh s\u1EEDa"}),jsx(MenuItem,{icon:jsx(AiOutlineDelete,{}),onClick:j,children:"X\xF3a b\xECnh lu\u1EADn"})]})]})]})}),jsx(Box,{mt:2,w:"full",children:a?jsxs(Box,{mt:4,w:"full",children:[jsx(Box,{w:"full",className:"reply-editor",mt:2,children:jsx(TipTap$1,{ref:S,showWordCount:!1,placeholder:"N\u1ED9i dung ph\u1EA3n h\u1ED3i ticket...",defaultValue:T})}),jsxs(HStack,{mt:2,children:[jsx(Button$1,{colorScheme:"orange",size:"sm",onClick:M,isLoading:x,children:"C\u1EADp nh\u1EADt"}),jsx(Button$1,{colorScheme:"gray",onClick:()=>l(),size:"sm",children:"H\u1EE7y"})]})]}):jsx(Box,{mt:2,overflow:"auto",w:"full",children:jsx(StyledTextContent,{borderColor:useColorModeValue("black","white"),dangerouslySetInnerHTML:{__html:A==null?void 0:A.content}})})})]})};var ETicketReaction;(function(e){e.Ok="Ok",e.NotOk="NotOk",e.Love="Love",e.Report="Report"})(ETicketReaction||(ETicketReaction={}));const reactionOptions=[{reaction:ETicketReaction.Love,count:0},{reaction:ETicketReaction.Ok,count:0},{reaction:ETicketReaction.NotOk,count:0},{reaction:ETicketReaction.Report,count:0}],EmoIcon=({bg:e,icon:r,text:n,count:a,allowVote:l})=>{const[c,u]=react.exports.useState(a);return jsxs(HStack,{className:"emoji","aria-label":"vote emoji",spacing:{base:1,md:2},onClick:b=>{const{target:A}=b,_=A.closest(".emoji");_&&l&&(_.classList.add("animate-emoji"),u(c+1))},onAnimationEnd:b=>{const{target:A}=b,_=A.closest(".emoji");_&&_.classList.remove("animate-emoji")},transition:"all .2s ease",_hover:{color:e},children:[jsx(Flex,{bg:e,rounded:"full",width:6,height:6,d:"inline-flex",alignItems:"center",justifyContent:"center",color:"white",p:1,gap:2,className:"icon",children:jsx(Icon,{as:r,w:4,h:4})}),jsxs(HStack,{as:"span",spacing:1,children:[jsx(Box,{as:"span",d:{base:"none",md:"inline"},children:n}),jsxs(Box,{as:"span",children:["(",c,")"]})]})]})},getAttribute=e=>{switch(e){case ETicketReaction.Ok:return{icon:AiOutlineLike,colorScheme:"green"};case ETicketReaction.NotOk:return{icon:AiOutlineDislike,colorScheme:"orange"};case ETicketReaction.Report:return{icon:MdOutlineReport,colorScheme:"slate"};case ETicketReaction.Love:return{icon:AiOutlineHeart,colorScheme:"red"};default:return{icon:AiOutlineHeart,colorScheme:"blue"}}},EmojiVote=({data:e,allowVote:r=!1,ticketId:n})=>{const[a,{loading:l}]=useMutation(CREATE_TICKET_REACTION),c=useToast(),u=react.exports.useCallback(async b=>{var A;try{await a({variables:{input:{refId:n,reaction:b}}})}catch(_){c({title:"Th\u1EA5t b\u1EA1i!",description:`${(A=_==null?void 0:_.message)!=null?A:"L\u1ED7i reaction"}`,status:"error",duration:3e3,isClosable:!0})}},[n]),d=reactionOptions.map(b=>{const A=e==null?void 0:e.find(_=>_.reaction===b.reaction);return A?N({},A):N({},b)}),m=r?d:d.filter(b=>b.count>0);return m?jsx(HStack,{spacing:4,fontSize:"sm",children:m.map(b=>{const A=getAttribute(b.reaction);return jsx(Box,{pointerEvents:l?"none":"inherit",as:"span",role:r?"button":"text",onClick:r?()=>u(b.reaction):null,children:jsx(EmoIcon,{icon:A.icon,bg:`${A.colorScheme}.500`,text:b.reaction,count:b.count,allowVote:r})},b.reaction)})}):null};var ETicketStatus;(function(e){e.PROCESSING="Processing",e.CLOSED="Closed",e.WAITING="Waiting",e.DELETED="Deleted"})(ETicketStatus||(ETicketStatus={}));const LIMIT_PREVIEW_SUB=1,StyledTextContent=newStyled(Text)`
  table {
    display: table;
    margin-left: auto;
    margin-right: auto;
    width: 100%;
    border: 1px solid #666666;
    border-spacing: 5px;
    border-collapse: collapse;
    th,
    td {
      border: ${e=>`1px solid ${e.borderColor||"black"}`};
      min-width: 100px;
      padding: 2px;
    }
    p {
      margin: 0;
    }
  }
  .custom-image {
    max-width: ${e=>`${e.maxWidthImage||"100%"}`};
    height: ${e=>`${e.heightImage||"auto"}`};
  }
`,TicketCard=({data:e,defaultShowEditor:r=!1,onTicketUpdate:n})=>{var me,ne,ce,fe,X,de;const[a,l]=useToggle(),[c,u]=react.exports.useState(e),[d,m]=react.exports.useState(((me=c.question)==null?void 0:me.title)||""),[b,A]=react.exports.useState(((ne=c.question)==null?void 0:ne.content)||""),[_,T]=react.exports.useState([]),[C,S]=useToggle(!1),[P,k]=useToggle(r),[x,E]=useToggle(!1),g=react.exports.useRef(null),y=useToast(),{name:I,avatar:M,time:j,status:O,meta:R,question:B,createDate:G}=c;useResetQueries();const[V]=useUser(),Q=parseInt(V.id,10)===((ce=e==null?void 0:e.createBy)==null?void 0:ce.id),W=(V==null?void 0:V.roleId)===5,[q,{loading:L}]=useMutation(CREATE_TICKET,{onCompleted:se=>{var pe,ge;const{createTicket:te}=se;c.status===ETicketStatus.WAITING&&u(H(N({},c),{status:ETicketStatus.PROCESSING})),T([H(N({},te),{id:te.id,name:(pe=te==null?void 0:te.createBy)==null?void 0:pe.full_name,avatar:(ge=te==null?void 0:te.createBy)==null?void 0:ge.avatar,status:te==null?void 0:te.status,time:(te==null?void 0:te.createDate)?dayjs(te.createDate).format("DD/MM/YYYY HH:mm"):"",meta:" [ZIM - 308 Tr\u1EA7n Ph\xFA] - Online - Advanced (10 h\u1ECDc vi\xEAn), 16/11, 19:30-21:30",question:{title:te==null?void 0:te.title,content:te==null?void 0:te.description},comments:[]}),..._]),y({title:"Th\xE0nh c\xF4ng!",description:"B\xECnh lu\u1EADn c\u1EE7a b\u1EA1n \u0111\xE3 \u0111\u01B0\u1EE3c g\u1EEDi.",status:"success",duration:3e3,isClosable:!0})},onError:se=>{var te;y({title:"T\u1EA1o kh\xF4ng th\xE0nh c\xF4ng!",description:`${(te=se==null?void 0:se.message)!=null?te:"L\u1ED7i"}`,status:"error",duration:3e3,isClosable:!0})}}),[D,{loading:F}]=useMutation(UPDATE_TICKET,{onCompleted:se=>{const{updateTicket:te}=se;u(H(N({},e),{question:{title:te==null?void 0:te.title,content:te==null?void 0:te.description}})),y({title:"Th\xE0nh c\xF4ng!",description:"C\u1EADp nh\u1EADt n\u1ED9i dung th\xE0nh c\xF4ng.",status:"success",duration:3e3,isClosable:!0})},onError:se=>{var te;y({title:"Th\u1EA5t b\u1EA1i!",description:`${(te=se==null?void 0:se.message)!=null?te:"L\u1ED7i c\u1EADp nh\u1EADt"}`,status:"error",duration:3e3,isClosable:!0})}}),[$,{loading:U}]=useMutation(UPDATE_TICKET_STATUS,{onCompleted:se=>{const{updateTicketStatus:te}=se;te&&(u(H(N({},c),{status:te.status})),n(H(N({},c),{status:te.status})),y({title:"Th\xE0nh c\xF4ng!",description:te.status===ETicketStatus.DELETED?"\u0110\xE3 x\xF3a ticket":"C\u1EADp nh\u1EADt tr\u1EA1ng th\xE1i th\xE0nh c\xF4ng.",status:"success",duration:3e3,isClosable:!0}))}}),z=react.exports.useCallback(se=>{se.preventDefault(),k()},[]),K=react.exports.useCallback(se=>{se.preventDefault(),S()},[]);react.exports.useCallback(se=>{se.preventDefault(),E()},[]);const Z=react.exports.useCallback(async()=>{const se=c.status;await $({variables:{input:{refId:c.id,status:se===TICKET_STATUS.Closed.value?ETicketStatus.PROCESSING:ETicketStatus.CLOSED}}})},[c]),Y=react.exports.useCallback(async()=>{await $({variables:{input:{refId:c.id,status:ETicketStatus.DELETED}}})},[c]),le=react.exports.useCallback(()=>{l()},[]),re=react.exports.useCallback(se=>{const te=se.status===ETicketStatus.DELETED?_.filter(pe=>pe.id!==se.id):_.map(pe=>pe.id===se.id?se:pe);T(te)},[_]),ee=async()=>{var se,te,pe;if(g.current){const ge=(te=(se=g==null?void 0:g.current)==null?void 0:se.props)==null?void 0:te.editor;if(!ge||!(ge==null?void 0:ge.getText()))return;const he=ge==null?void 0:ge.getHTML();he&&A(he),await D({variables:{input:{refId:(pe=c==null?void 0:c.id)!=null?pe:EMPTY_GUID,title:d,description:he}}})}l()},oe=async()=>{var se,te,pe,ge,Ce;try{if(g.current){const he=(te=(se=g==null?void 0:g.current)==null?void 0:se.props)==null?void 0:te.editor;if(he){const Ee=he==null?void 0:he.getHTML();if(!(he==null?void 0:he.getText())&&!Ee)return;await q({variables:{input:{refId:(pe=e==null?void 0:e.id)!=null?pe:EMPTY_GUID,title:"",description:Ee,receiverId:(ge=e==null?void 0:e.createBy)==null?void 0:ge.id}}}),(Ce=he==null?void 0:he.commands)==null||Ce.clearContent()}}}catch(he){console.log({e:he})}};return react.exports.useEffect(()=>{T(e.comments||[])},[e]),e?jsxs(Card$1,{className:"animate__animated animate__fadeIn",children:[jsx(Flex,{gap:4,children:jsxs(HStack,{justifyContent:"space-between",flexGrow:1,alignItems:"flex-start",children:[jsx(UserCard,{avatar:M,name:I,meta:dayjs(G).format("DD/MM/YYYY HH:mm"),badgeTitle:(fe=TICKET_STATUS==null?void 0:TICKET_STATUS[c==null?void 0:c.status])==null?void 0:fe.label,badgeProps:{colorScheme:(X=TICKET_STATUS==null?void 0:TICKET_STATUS[c==null?void 0:c.status])==null?void 0:X.colorScheme}}),!W&&jsxs(Menu,{children:[jsx(MenuButton,{as:IconButton,variant:"ghost",icon:jsx(AiOutlineMore,{fontSize:24})}),jsxs(MenuList,{children:[jsx(MenuItem,{icon:jsx(Icon,{as:AiOutlineEdit,w:4,h:4}),onClick:le,children:"Ch\u1EC9nh s\u1EEDa"}),jsx(MenuItem,{icon:jsx(Icon,{as:AiOutlineLock,w:4,h:4}),onClick:Z,children:jsx("span",{children:c.status!==ETicketStatus.CLOSED?"\u0110\xF3ng ticket":"M\u1EDF ticket"})}),!W&&jsx(MenuItem,{icon:jsx(Icon,{as:AiOutlineDelete,w:4,h:4}),onClick:Y,children:"X\xF3a ticket"})]})]})]})}),jsxs(Box,{mt:4,children:[a?jsxs(Box,{mt:4,w:"full",children:[jsxs(Box,{w:"full",className:"reply-editor",mt:2,children:[jsx(TextInput,{label:"Ti\xEAu \u0111\u1EC1",value:d,onChange:se=>m(se.target.value)}),jsx(Box,{mt:2,children:jsx(TipTap$1,{ref:g,showWordCount:!1,placeholder:"N\u1ED9i dung ticket...",defaultValue:b})})]}),jsxs(HStack,{mt:2,children:[jsx(Button$1,{colorScheme:"orange",size:"sm",onClick:ee,isLoading:F,children:"C\u1EADp nh\u1EADt"}),jsx(Button$1,{colorScheme:"gray",onClick:()=>l(),size:"sm",children:"H\u1EE7y"})]})]}):jsxs(Box,{children:[jsx(Text,{fontWeight:600,children:(de=B==null?void 0:B.title)!=null?de:""}),jsx(Box,{mt:2,overflow:"auto",w:"full",children:jsx(StyledTextContent,{borderColor:useColorModeValue("black","white"),dangerouslySetInnerHTML:{__html:B==null?void 0:B.content}})})]}),jsx(VStack,{spacing:4,mx:-4,children:_&&_.map((se,te)=>C?jsx(Box,{borderTop:"1px solid",borderColor:useColorModeValue("gray.300","slate.700"),mt:4,pt:4,px:4,w:"full",children:jsx(ReplyCard,{data:H(N({},se),{meta:dayjs(se.createDate).format("DD/MM/YYYY HH:mm")}),onTicketUpdate:re,canOperationComment:!W})},se.id):te<LIMIT_PREVIEW_SUB&&jsx(Box,{borderTop:"1px solid",borderColor:useColorModeValue("gray.300","slate.700"),mt:4,pt:4,px:4,w:"full",children:jsx(ReplyCard,{onTicketUpdate:re,data:H(N({},se),{meta:dayjs(se.createDate).format("DD/MM/YYYY HH:mm")}),canOperationComment:!W})},se.id))}),!C&&(_==null?void 0:_.length)>LIMIT_PREVIEW_SUB&&jsxs(Link$1,{href:"#",color:"blue.500",d:"flex",gap:2,mt:2,alignItems:"center",onClick:K,children:[jsx(Box,{as:"span",className:"animate__animated animate__pulse animate__infinite",children:jsx(AiOutlineArrowDown,{})}),jsx(Text,{role:"button","aria-label":"load more replies",children:"Xem c\xE1c ph\u1EA3n h\u1ED3i tr\u01B0\u1EDBc..."})]}),P&&jsxs(Box,{mt:4,w:"full",className:"animate__animated animate__fadeIn",children:[jsx(Text,{fontWeight:600,children:"Ph\u1EA3n h\u1ED3i"}),jsx(Box,{width:"full",className:"reply-editor",mt:2,children:jsx(TipTap$1,{ref:g,showWordCount:!1,placeholder:"N\u1ED9i dung ph\u1EA3n h\u1ED3i ticket..."})}),jsx(Button$1,{colorScheme:"blue",mt:4,mb:0,size:"sm",onClick:oe,isLoading:L,children:"G\u1EEDi ph\u1EA3n h\u1ED3i"})]})]}),jsx(Box,{my:4,children:jsx(EmojiVote,{data:e.reactions,allowVote:Q,ticketId:e.id})}),jsxs(Flex,{justifyContent:"space-between",alignItems:"center",gap:2,mt:4,flexWrap:"wrap",children:[jsx(HStack,{spacing:4,children:jsx(Button$1,{leftIcon:jsx(MdReply,{}),variant:P?"solid":"outline",size:"sm",colorScheme:"gray",onClick:z,isDisabled:e.status===ETicketStatus.CLOSED||e.status===ETicketStatus.DELETED,children:"Ph\u1EA3n h\u1ED3i"})}),jsx(Text,{color:"gray.500",fontSize:"sm",children:j||""})]})]}):null},Card$1=e=>{const c=e,{variant:r,children:n}=c,a=Te(c,["variant","children"]),l=useStyleConfig("Card",{variant:r});return jsx(Box,H(N({__css:l},a),{children:n}))},questionTypeTypingId=4,questionTypeMultipleChoiceId=2,questionTypeSingleChoice=1,questionTypeEssayId=6,questionTypeSpeakingId=7,questionTypeNoAnswerQuestion=[questionTypeEssayId,questionTypeSpeakingId],blankStateCreateExercise={title:"",category:"",exerciseType:null,module:null,skill:null,lessonTopic:null,levelIn:null,levelOut:null,subject:null},QuestionAnswers=({questionTypeId:e,answers:r,handleAnswersChange:n,isDisabled:a=!1})=>{const[l,c]=react.exports.useState([]),u=e===questionTypeSingleChoice;useDeepCompareEffect$2(()=>{if(r==null?void 0:r.length){const C=r.map(S=>{var P;return H(N({},S),{alternatives:(P=S.alternatives)==null?void 0:P.join(", ")})});c(C)}else return},[r]);const d=useDebounce$1(l,500),m=react.exports.useCallback(C=>{const S=[...l].filter(P=>P.id!==C);c(S)},[l]),b=react.exports.useCallback((C,S)=>{if(u){const P=l.map(k=>k.id===C?H(N({},k),{correct:S}):H(N({},k),{correct:!1}));c(P)}else{const P=l.map(k=>k.id===C?H(N({},k),{correct:S}):k);c(P)}},[l,u]),A=react.exports.useCallback((C,S)=>{const P=[...l].map(k=>k.id===C?H(N({},k),{answer:S}):k);c(P)},[l]),_=react.exports.useCallback((C,S)=>{const P=[...l].map(k=>k.id===C?H(N({},k),{alternatives:S}):k);c([...P])},[l]),T=react.exports.useCallback(()=>{c([...l,{id:v4$1(),answer:"",correct:!1,alternatives:[]}])},[l]);return react.exports.useEffect(()=>{const C=d.map(S=>{var P;if((P=S.alternatives)==null?void 0:P.length){const x=S.alternatives.split(", ").map(E=>typeof E=="string"&&E.length>0?E.trim():"");return H(N({},S),{alternatives:x})}return H(N({},S),{alternatives:[]})});n(C)},[d]),jsxs(Fragment,{children:[jsx(HStack,{mx:-4,flexWrap:"wrap",spacing:0,alignItems:"start",children:l&&l.length>0&&l.map((C,S)=>jsxs(VStack,{px:4,w:{base:"100%",md:"50%"},pb:4,spacing:0,children:[jsxs(HStack,{pb:2,flexGrow:1,width:"full",children:[jsx(FormControl,{flexGrow:1,isInvalid:!C.correct,children:jsxs(InputGroup,{size:"lg",children:[jsx(Input$1,{placeholder:"\u0110\xE1p \xE1n",value:C.answer,onChange:P=>A(C.id,P.target.value),isReadOnly:a,fontSize:"16px"}),jsx(InputRightElement,{width:"4.5rem",children:jsx(HStack,{spacing:2,children:jsx(Switch,{isChecked:C==null?void 0:C.correct,onChange:P=>b(C.id,P.target.checked),isReadOnly:a})})})]})}),!a&&jsx(Fragment,{children:jsx(Tooltip,{label:"Delete answer",children:jsx(IconButton,{"aria-label":"Delete answer",icon:jsx(DeleteIcon,{}),colorScheme:"red",size:"lg",onClick:()=>m(C==null?void 0:C.id)})})})]}),C.correct&&e===questionTypeTypingId&&jsxs(HStack,{pl:0,pb:4,flexGrow:1,width:"full",children:[jsx(MdSubdirectoryArrowRight,{fontSize:24}),jsx(FormControl,{flexGrow:1,isInvalid:!C.correct,children:jsx(InputGroup,{size:"lg",children:jsx(Input$1,{placeholder:"KQ \u0111\xFAng kh\xE1c, c\xE1ch nhau b\u1EDFi d\u1EA5u ph\u1EA9y. VD: money, Money",value:C.alternatives,onChange:P=>_(C.id,P.target.value),fontSize:"16px",isReadOnly:a})})})]})]},`${C==null?void 0:C.id}-${S}`))}),!a&&jsx(Button$1,{colorScheme:"orange",onClick:T,mt:2,leftIcon:jsx(PlusSquareIcon,{}),children:"Th\xEAm \u0111\xE1p \xE1n"})]})},CreateQuestionForm=({questionTypeId:e})=>{const{control:r,setValue:n,watch:a}=useFormContext(),l=a("answers");return jsx(VStack,{align:"stretch",children:!questionTypeNoAnswerQuestion.includes(e)&&jsxs(Fragment,{children:[e===questionTypeMultipleChoiceId&&jsx(Fragment,{children:jsxs(HStack,{align:"center",spacing:5,children:[jsx(Controller,{control:r,render:({field:c})=>jsxs(HStack,{children:[jsx(Text,{whiteSpace:"nowrap",fontSize:"sm",children:"S\u1ED1 l\u01B0\u1EE3ng c\xE2u h\u1ECFi"}),jsx(TextInput,N({},c))]}),name:"questionCount"}),jsx(Controller,{control:r,render:({field:{value:c,onChange:u}})=>jsxs(HStack,{children:[jsx(Text,{whiteSpace:"nowrap",fontSize:"sm",children:"Chia \u0111i\u1EC3m"}),jsx(Switch,{isChecked:c,onChange:u})]}),name:"requireOrder"}),jsx(Controller,{control:r,render:({field:{value:c,onChange:u}})=>jsxs(HStack,{children:[jsx(Text,{whiteSpace:"nowrap",fontSize:"sm",children:"Check th\u1EE9 t\u1EF1 \u0111\xE1p \xE1n"}),jsx(Switch,{isChecked:c,onChange:u})]}),name:"allowToBreakPoint"})]})}),e!==questionTypeTypingId&&jsxs(Fragment,{children:[jsx(Text,{fontWeight:"600",children:"C\xE2u h\u1ECFi"}),jsx(Controller,{control:r,render:({field:c})=>jsx(Box,{className:"question-editor",children:jsx(TipTap$1,H(N({showWordCount:!1,enableSpaceMenu:!0,isMockTestEditor:!0},c),{defaultValue:c.value}))}),name:"question"})]}),jsxs(FormControl,{children:[jsxs(HStack,{spacing:4,mb:4,children:[jsx(Text,{children:"\u0110\xE1p \xE1n:"}),jsxs(HStack,{children:[jsx(Switch,{isReadOnly:!0,defaultChecked:!0}),jsx(Text,{children:"B\u1EADt n\u1EBFu \u0111\xF3 l\xE0 \u0111\xE1p \xE1n \u0111\xFAng"})]})]}),jsx(QuestionAnswers,{handleAnswersChange:c=>n("answers",c),answers:l,questionTypeId:e})]})]})})},QUESTION_TYPE=gql`
  fragment QuestionTypeField on QuestionType {
    id
    contentQuestion
    questionIndex
    questionCount
    allowToBreakPoint
    requiredOrder
    answer {
      id
      isCorrect
      contentAnswer
      answerRelated {
        id
        contentAnswerRelated
      }
    }
  }
`,CREATE_EXERCISE=gql`
  mutation createExercise($input: ExerciseInputType) @api(name: "appZim") {
    createExercise(input: $input) {
      id
    }
  }
`,UPDATE_STATUS_EXERCISE=gql`
  mutation updateStatusExercise($id: Int, $status: StatusExerciseGrapNet)
  @api(name: "appZim") {
    updateStatusExercise(id: $id, status: $status) {
      id
    }
  }
`,UPDATE_EXERCISE=gql`
  mutation updateExercise($id: Int, $input: ExerciseInputType)
  @api(name: "appZim") {
    updateExercise(id: $id, input: $input) {
      id
      title
      exerciseType {
        id
        name
      }
      cat {
        id
        name
      }
      subcat {
        id
        name
      }
      subject {
        id
        name
      }
      levelIn {
        id
        name
      }
      levelOut {
        id
        name
      }
      lessonTopic {
        id
        title
      }
    }
  }
`,CREATE_QUESTION_GROUP=gql`
  mutation createQuestionGroup($input: CreateQuestionGroupInputType)
  @api(name: "appZim") {
    createQuestionGroup(input: $input) {
      id
      title
      groupIndex
      questionType {
        id
        name
      }
      question {
        ...QuestionTypeField
      }
    }
  }
  ${QUESTION_TYPE}
`,CREATE_QUESTION=gql`
  mutation createQuestion($input: CreateQuestionInputType)
  @api(name: "appZim") {
    createQuestion(input: $input) {
      id
      contentQuestion
    }
  }
`,UPDATE_QUESTION_GROUP=gql`
  mutation updateQuestionGroup($input: updateQuestionGroupInputType)
  @api(name: "appZim") {
    updateQuestionGroup(input: $input) {
      id
      title
    }
  }
`,REMOVE_QUESTION_GROUP=gql`
  mutation removeQuestionGroup($id: Int!) @api(name: "appZim") {
    removeQuestionGroup(id: $id) {
      success
    }
  }
`,UPDATE_QUESTION=gql`
  mutation updateQuestion($input: UpdateQuestionInputType)
  @api(name: "appZim") {
    updateQuestion(input: $input) {
      id
      contentQuestion
    }
  }
`,REMOVE_QUESTION=gql`
  mutation removeQuestion($id: Int!) @api(name: "appZim") {
    removeQuestion(id: $id) {
      success
    }
  }
`,COPY_QUESTION=gql`
  mutation copyQuestion($currentId: Int, $currentQuestionIndex: Int)
  @api(name: "appZim") {
    copyQuestion(
      currentId: $currentId
      currentQuestionIndex: $currentQuestionIndex
    ) {
      id
    }
  }
`,GET_EXERCISE_PAGINATION=gql`
  query getExercisePagination($input: GetListExerciseInputType)
  @api(name: "appZim") {
    getExercisePagination(input: $input) {
      hasNextPage
      hasPrevPage
      limit
      page
      pageNext
      pagePrev
      pagingCounter
      totalPages
      totalDocs
      docs {
        id
        title
        status
        cat {
          id
          name
        }
        subcat {
          id
          name
        }
        exerciseType {
          id
          name
        }
        levelOut {
          id
          name
          graduation
        }
        levelIn {
          id
          name
          graduation
        }
        lessonTopic {
          id
          title
        }
        subject {
          id
          name
        }
      }
    }
  }
`,GET_EXERCISE_DETAIL=gql`
  query getExerciseDetail($id: Int) @api(name: "appZim") {
    getExerciseDetail(id: $id) {
      id
      title
      status
      exerciseType {
        id
        name
      }
      cat {
        id
        name
      }
      subcat {
        id
        name
      }
      questionGroup {
        id
        title
        description
        groupIndex
        questionType {
          id
          name
        }
      }
      part {
        id
        contentPart
        questionGroup {
          id
          title
          description
          groupIndex
        }
      }
      levelIn {
        id
        name
        graduation
      }
      levelOut {
        id
        name
        graduation
      }
      lessonTopic {
        id
        title
      }
      subcat {
        id
        name
      }
      subject {
        id
        name
      }
    }
  }
`,GET_QUESTION_GROUP_ID=gql`
  query getQuestionGroupId($id: Int!) @api(name: "appZim") {
    getQuestionGroupId(id: $id) {
      id
      title
      description
      groupIndex
      question {
        ...QuestionTypeField
      }
      questionType {
        id
        name
      }
      answerFile {
        id
        url
      }
      questionFile {
        id
        url
      }
      explanation
    }
  }
  ${QUESTION_TYPE}
`,GET_QUESTION_BY_ID=gql`
  query getQuestionById($id: Int!) @api(name: "appZim") {
    getQuestionById(id: $id) {
      ...QuestionTypeField
    }
  }
  ${QUESTION_TYPE}
`,QuestionModal=({isOpen:e,onClose:r,questionGroupId:n,questionId:a,questionTypeId:l})=>{const c=useForm({defaultValues:{question:"",questionIndex:0,questionCount:1,explanation:"",requireOrder:!1,allowToBreakPoint:!1,answers:[]}}),{handleSubmit:u,reset:d}=c,[m,b]=react.exports.useState([]),A=a>0,_=useResetQueries(),T=useToast(),{data:C}=useQuery(GET_QUESTION_BY_ID,{variables:{id:a},skip:!a});react.exports.useEffect(()=>{var I;if(!C)return;const y=C==null?void 0:C.getQuestionById;d({question:y.contentQuestion,questionCount:y.questionCount,allowToBreakPoint:y.allowToBreakPoint,requireOrder:y.requiredOrder,answers:y.answer.map(M=>H(N({},M),{correct:M.isCorrect,answer:M.contentAnswer,alternatives:M.answerRelated.map(j=>j.contentAnswerRelated)}))}),b((I=y.answer)==null?void 0:I.map(M=>M.id))},[C]);const[S,{loading:P}]=useMutation(CREATE_QUESTION,{onCompleted:()=>{T({title:"Th\xE0nh c\xF4ng!",description:"T\u1EA1o c\xE2u h\u1ECFi th\xE0nh c\xF4ng",status:"success",duration:3e3,isClosable:!0}),_(["getQuestionGroupId"]),r()},onError:y=>{T({title:"L\u1ED7i!",description:`Create question error: ${y.message}`,status:"error",duration:3e3,isClosable:!0})}}),[k,{loading:x}]=useMutation(UPDATE_QUESTION,{onCompleted:()=>{T({title:"Th\xE0nh c\xF4ng!",description:"C\u1EADp nh\u1EADt c\xE2u h\u1ECFi th\xE0nh c\xF4ng",status:"success",duration:3e3,isClosable:!0}),_(["getQuestionGroupId","getQuestionById"]),r()},onError:y=>{T({title:"L\u1ED7i!",description:`Update question error: ${y.message}`,status:"error",duration:3e3,isClosable:!0})}}),E=react.exports.useCallback(async y=>{var I,M;try{await S({variables:{input:{questionGroupId:n,contentQuestion:y.question,requiredOrder:y.requiredOrder,questionCount:Number(y.questionCount),allowToBreakPoint:y.allowToBreakPoint,answer:(M=(I=y.answers)==null?void 0:I.filter(j=>j.answer.length>0))==null?void 0:M.map(j=>({contentAnswer:j.answer,isCorrect:j.correct,answerRelated:j.alternatives}))}}})}catch(j){console.log(j)}},[n]),g=react.exports.useCallback(async y=>{var I;try{await k({variables:{input:{id:a,contentQuestion:y.question,requiredOrder:y.requiredOrder,questionCount:Number(y.questionCount),allowToBreakPoint:y.allowToBreakPoint,answer:(I=y.answers)==null?void 0:I.filter(M=>{var j;return((j=M.answer)==null?void 0:j.length)>0}).map(M=>{const j={id:M.id,contentAnswer:M.answer,isCorrect:M.correct,answerRelated:M.alternatives.map(O=>({contentAnswerRelated:O}))};return m.includes(j.id)||delete j.id,j})}}})}catch(M){console.log(M)}},[a,m]);return jsxs(Modal$1,{isOpen:e,onClose:r,size:"6xl",closeOnOverlayClick:!1,isCentered:!0,scrollBehavior:"inside",closeOnEsc:!1,children:[jsx(ModalOverlay,{}),jsxs(ModalContent,{children:[jsx(ModalCloseButton,{}),jsx(ModalHeader,{children:questionTypeNoAnswerQuestion.includes(l)?"Gi\u1EA3i th\xEDch \u0111\xE1p \xE1n":"T\u1EA1o c\xE2u h\u1ECFi"}),jsx(ModalBody,{children:jsx(FormProvider,H(N({},c),{children:jsx(CreateQuestionForm,{questionTypeId:l})}))}),jsxs(ModalFooter,{children:[jsx(Button$1,{mr:3,onClick:r,children:"\u0110\xF3ng"}),jsx(Button$1,{colorScheme:"green",isLoading:P||x,onClick:u(A?g:E),children:A?"C\u1EADp nh\u1EADt c\xE2u h\u1ECFi":"T\u1EA1o c\xE2u h\u1ECFi"})]})]})]})},IndeterminateCheckbox=react.exports.forwardRef((a,n)=>{var l=a,{indeterminate:e}=l,r=Te(l,["indeterminate"]);const c=react.exports.useRef(),u=useCombinedRefs(n,c);return react.exports.useEffect(()=>{u.current&&(u.current.indeterminate=e)},[u,e]),jsx(Fragment,{children:jsx(Checkbox,H(N({ref:u},r),{isChecked:r.checked}))})}),DataTable=({columns:e,data:r,loading:n=!1,dragScrollProps:a={},renderRowSubComponent:l,tableProps:c,isSelection:u=!1,onSelectedRows:d})=>{var P;const{getTableProps:m,getTableBodyProps:b,headerGroups:A,prepareRow:_,rows:T,visibleColumns:C,selectedFlatRows:S}=reactTable.exports.useTable({data:r,columns:e,autoResetPage:!0,manualSortBy:!0},reactTable.exports.useSortBy,reactTable.exports.useExpanded,u&&reactTable.exports.useRowSelect,k=>{u&&k.visibleColumns.push(x=>[{id:"selection",Header:({getToggleAllRowsSelectedProps:E})=>jsx("div",{children:jsx(IndeterminateCheckbox,N({},E()))}),Cell:({row:E})=>jsx("div",{children:jsx(IndeterminateCheckbox,N({},E.getToggleRowSelectedProps()))})},...x])});return react.exports.useEffect(()=>{typeof d=="function"&&d(S.map(k=>k.original))},[S]),jsxs(Box,{pos:"relative",children:[jsx(VStack,{pos:"absolute",bg:"blackAlpha.700",top:0,left:0,right:0,bottom:0,zIndex:10,d:n?"flex":"none",justifyContent:"center",alignItems:"center",transition:"all .2s ease",opacity:1,children:jsx(Loading,{color:"white"})}),jsx(Box,H(N({as:p$1,pos:"relative",overflow:"auto",className:"scroll-container",hideScrollbars:!1,vertical:!0,horizontal:!0,ignoreElements:(P=a==null?void 0:a.ignoreElements)!=null?P:"[class^=chakra-text]"},a),{children:jsxs(Table$2,H(N(N({},m()),c),{children:[jsx(Thead,{children:A.map(k=>jsx(Tr,H(N({},k.getHeaderGroupProps()),{children:k.headers.map(x=>jsx(Th,H(N({},x.getHeaderProps(x.getSortByToggleProps())),{pos:(x==null?void 0:x.isSticky)?"sticky":null,left:(x==null?void 0:x.stickyLeftOffset)?x==null?void 0:x.stickyLeftOffset:0,right:(x==null?void 0:x.stickyRightOffset)?x==null?void 0:x.stickyRightOffset:0,width:x==null?void 0:x.width,minW:x==null?void 0:x.minWidth,maxW:x==null?void 0:x.maxWidth,zIndex:(x==null?void 0:x.isSticky)&&2,whiteSpace:(x==null?void 0:x.nowrap)&&"nowrap",children:jsxs(HStack,{children:[jsx(Text,{children:x.render("Header")}),jsx(chakra.span,{children:x.isSorted?x.isSortedDesc?jsx(TriangleDownIcon,{"aria-label":"sorted descending"}):jsx(TriangleUpIcon,{"aria-label":"sorted ascending"}):null})]})})))})))}),jsx(Tbody,H(N({},b()),{children:n&&(!r.length||r.length===0)?jsx(Tr,{children:jsx(Td,{colSpan:e.length,children:jsx(Box,{py:24})})}):r.length>0?T.map(k=>{_(k);const x=k.getRowProps();return jsxs(react.exports.Fragment,{children:[jsx(Tr,H(N({},k.getRowProps()),{children:k.cells.map(E=>{const g=E.column,y=(g==null?void 0:g.stickyLeftOffset)!=="undefined",I=(g==null?void 0:g.stickyRightOffset)!=="undefined";return jsx(Td,H(N({},E.getCellProps()),{isNumeric:g==null?void 0:g.isNumeric,pos:(g==null?void 0:g.isSticky)?"sticky":null,left:y?g==null?void 0:g.stickyLeftOffset:0,right:I?g==null?void 0:g.stickyRightOffset:0,zIndex:(g==null?void 0:g.isSticky)&&2,whiteSpace:(g==null?void 0:g.nowrap)&&"nowrap",bgColor:(g==null?void 0:g.isSticky)?"gray.200":"white",overflow:"visible",minW:g==null?void 0:g.minWidth,className:y?"shadow-left":"",_after:{content:'""',position:"absolute",left:(g==null?void 0:g.isSticky)?g==null?void 0:g.stickyRightOffset:0,right:(g==null?void 0:g.isSticky)?g==null?void 0:g.stickyLeftOffset:0,top:0,height:"100%",width:1,boxShadow:(g==null?void 0:g.isSticky)?`${I?"-1px":y?"1px":0} 0px 3px 0px ${useColorModeValue("var(--chakra-colors-gray-300)","var(--chakra-colors-slate-700)")}`:"none"},children:E.render("Cell")}))})})),k.isExpanded&&l({row:k,rowProps:x,visibleColumns:C})]},x.key)}):jsx(Tr,{children:jsx(Td,{colSpan:e.length,children:jsx(Text,{textAlign:"center",color:"error",children:"Kh\xF4ng c\xF3 d\u1EEF li\u1EC7u ph\xF9 h\u1EE3p."})})})}))]}))}))]})},CloneQuestionButton=({id:e,questionIndex:r})=>{const n=useToast(),a=useResetQueries(),[l,{loading:c}]=useMutation(COPY_QUESTION,{onCompleted:()=>{n({title:"Th\xE0nh c\xF4ng",description:"T\u1EA1o b\u1EA3n sao th\xE0nh c\xF4ng",status:"success",duration:3e3,isClosable:!1}),a(["getQuestionGroupId"])},onError:d=>{n({title:"Th\u1EA5t b\u1EA1i",description:`COPY QUESTION error: ${d.message}`,status:"error",duration:3e3,isClosable:!1})}}),u=react.exports.useCallback(async()=>{await l({variables:{currentId:e,currentQuestionIndex:r}})},[e,r]);return jsx(IconButton,{colorScheme:"blue",onClick:u,"aria-label":"duplicate",icon:jsx(CopyIcon,{}),isLoading:c})},Answers=react.exports.memo(({data:e})=>e?jsx(HStack,{spacing:0,flexWrap:"wrap",mx:-2,children:e.map(r=>jsx(Box,{p:1,children:jsx(Badge$1,{colorScheme:(r==null?void 0:r.isCorrect)?"facebook":"red",size:"md",fontWeight:"normal",children:r==null?void 0:r.contentAnswer})},r==null?void 0:r.contentAnswer))}):null),TableQuestion=({questionGroupId:e,isEditable:r,questions:n,loading:a,questionType:l})=>{const{isOpen:c,onClose:u,onOpen:d}=useDisclosure(),m=react.exports.useRef(),[b,A]=react.exports.useState(null),_=useToast(),T=useResetQueries(),C=react.exports.useMemo(()=>[{Header:"STT",accessor:"questionIndex",maxWidth:"10px",nowrap:!0},{Header:"C\xE2u h\u1ECFi",accessor:"contentQuestion",minWidth:"150px",Cell:({value:R})=>jsx(StyledTextContent,{dangerouslySetInnerHTML:{__html:R},style:{fontWeight:"bold"}})},{Header:"\u0110\xE1p \xE1n",accessor:"answer",Cell:({value:R})=>jsx(Answers,{data:R}),minWidth:"150px"},{Header:"SL c\xE2u",accessor:"questionCount",minWidth:"150px"},{Header:"",accessor:"actions",Cell:({row:{original:R}})=>jsxs(HStack,{spacing:2,justifyContent:"flex-end",children:[r&&jsx(CloneQuestionButton,{id:R.id,questionIndex:R.questionIndex}),jsx(IconButton,{colorScheme:"yellow",onClick:()=>S(R.id),"aria-label":"edit",icon:jsx(EditIcon,{})}),r&&jsx(IconButton,{colorScheme:"red",onClick:()=>k(R.id),"aria-label":"delete",icon:jsx(BiTrashAlt,{fontSize:16})})]})}],[r]),S=react.exports.useCallback(R=>{A(R),g()},[]),P=react.exports.useCallback(()=>{y(),A(null)},[]),k=react.exports.useCallback(R=>{A(R),d()},[]),x=react.exports.useCallback(()=>{u(),A(null)},[]),{isOpen:E,onOpen:g,onClose:y}=useDisclosure(),[I,{loading:M}]=useMutation(REMOVE_QUESTION,{onCompleted:()=>{_({title:"Th\xE0nh c\xF4ng!",description:"X\xF3a c\xE2u h\u1ECFi th\xE0nh c\xF4ng",status:"success",duration:3e3,isClosable:!0}),T(["getQuestionGroupId"]),x()},onError:R=>{_({title:"L\u1ED7i!",description:`Delete question error: ${R.message}`,status:"error",duration:3e3,isClosable:!0})}}),j=react.exports.useCallback(async()=>{try{await I({variables:{id:b}})}catch(R){console.log(R)}},[b]),O=sortBy_1(n,["questionIndex"]);return jsxs(VStack,{align:"stretch",flexGrow:0,spacing:4,children:[jsxs(Text,{as:"span",children:["Lo\u1EA1i nh\xF3m c\xE2u:"," ",jsx(Text,{as:"span",fontWeight:"600",children:l==null?void 0:l.name})]}),jsxs(HStack,{justify:"space-between",children:[jsx(Heading,{size:"md",children:"Danh s\xE1ch c\xE2u h\u1ECFi"}),r&&jsx(Button$1,{colorScheme:"blue",onClick:g,children:"T\u1EA1o c\xE2u h\u1ECFi"})]}),jsx(DataTable,{columns:C,data:O,loading:a}),E&&jsx(QuestionModal,{isOpen:E,onClose:P,questionGroupId:e,questionId:b,questionTypeId:l==null?void 0:l.id}),jsx(AlertDialog,{isOpen:c,leastDestructiveRef:m,onClose:y,children:jsx(ModalOverlay,{children:jsxs(AlertDialogContent,{children:[jsx(ModalHeader,{fontSize:"lg",fontWeight:"bold",children:"X\xF3a c\xE2u h\u1ECFi"}),jsx(ModalBody,{children:"B\u1EA1n c\xF3 ch\u1EAFc ch\u1EAFn mu\u1ED1n x\xF3a c\xE2u h\u1ECFi n\xE0y kh\xF4ng ?"}),jsxs(ModalFooter,{children:[jsx(Button$1,{ref:m,onClick:x,children:"Cancel"}),jsx(Button$1,{isLoading:M,colorScheme:"red",onClick:j,ml:3,children:"Delete"})]})]})})})]})};function QuestionMenu({editor:e,defaultValues:r}){var m;const[n,a]=react.exports.useState(null),l=useToast(),c=useActiveGroupStore(b=>b.activeGroup),u=react.exports.useMemo(()=>(c==null?void 0:c.questions)?c.questions.map(b=>H(N({},b),{label:`${b==null?void 0:b.questionIndex} - ${b.answer.filter(A=>A.isCorrect).map(A=>A.contentAnswer).join(" | ")}`,value:b.id})):[],[c]),d=()=>{var b;try{n?e.chain().updateAttributes("spacing",{"data-type":n==null?void 0:n.type,"data-question":n==null?void 0:n.id,"data-question-group":c==null?void 0:c.id,"data-label":`${n==null?void 0:n.questionIndex}`,class:"space-wrapper"}).run():l({title:"Th\u1EA5t b\u1EA1i",description:"Vui l\xF2ng ch\u1ECDn c\xE2u h\u1ECFi",status:"error",duration:3e3,isClosable:!0})}catch(A){l({title:"Th\u1EA5t b\u1EA1i",description:(b=A==null?void 0:A.message)!=null?b:"Li\xEAn k\u1EBFt kh\xF4ng th\xE0nh c\xF4ng",status:"error",duration:3e3,isClosable:!0})}};return react.exports.useEffect(()=>{if(e&&e.isActive("spacing")){const b=u.find(A=>(A==null?void 0:A.id)===Number(r==null?void 0:r.questionId));a(b)}else a(null)},[r==null?void 0:r.questionId,u]),jsxs(Card$1,{px:4,py:4,shadow:"outline",w:400,children:[jsxs(VStack,{w:"full",spacing:4,children:[jsxs(FormControl,{children:[jsx(FormLabel,{children:"Ch\u1ECDn c\xE2u h\u1ECFi:"}),jsx(Select$2,{options:u,onChange:a,value:n})]}),(n==null?void 0:n.answer)&&jsx(Box,{children:jsx(Answers,{data:(m=n==null?void 0:n.answer)!=null?m:[]})})]}),jsxs(HStack,{justifyContent:"flex-end",w:"full",mt:6,children:[jsx(Button$1,{colorScheme:"teal",onClick:d,children:"Li\xEAn k\u1EBFt c\xE2u"}),jsx(Button$1,{isDisabled:!e.isActive("spacing"),variant:"solid",colorScheme:"red",onClick:()=>{e.chain().focus().deleteSelection().run()},children:"X\xF3a"})]})]})}const EditorContext=react.exports.createContext({editor:null}),TipTap=react.exports.forwardRef(({onChange:e,placeholder:r="Nh\u1EADp n\u1ED9i dung",defaultValue:n="",extensions:a=[],enableSpaceMenu:l=!1,showWordCount:c=!0,isDisabled:u=!1,stickyMenuBar:d=!1,isMockTestEditor:m=!1},b)=>{const[A,_]=react.exports.useState(0),T=useActiveGroupStore(E=>E.activeGroup),C=react.exports.useCallback(({editor:E})=>{try{if(m&&!lodash.exports.isEmpty(T)){const g=(T==null?void 0:T.questions)||[],y=E.getHTML(),I=cheerio.load(y);I(".space-wrapper").each((M,j)=>{var B,G;const O=Number(I(j).attr("data-question")),R=g.find(V=>V.id===O);I(j).attr("data-question",(B=R==null?void 0:R.id)!=null?B:""),I(j).attr("data-question-group",(G=R==null?void 0:R.questionGroupId)!=null?G:""),I(j).attr("data-label",R?`${R==null?void 0:R.questionIndex}`:""),I(j).attr("data-type",R==null?void 0:R.type)}),E.commands.setContent(I.html(),!0)}}catch(g){console.log({e:g})}},[m,n]),S=useEditor({extensions:[StarterKit,CustomTable.configure({resizable:!0}),TableRow,TableHeader,CustomTableCell,CustomBlockquote,Typography,Underline,CustomImage.configure({inline:!0,HTMLAttributes:{class:"custom-image"}}),CustomLink.configure({openOnClick:!1}),Placeholder.configure({placeholder:r||"Nh\u1EADp n\u1ED9i dung",emptyEditorClass:"is-editor-empty"}),Iframe.configure({inline:!0}),TextAlign.configure({types:["heading","paragraph"],alignments:["left","right","center"]}),TextSpace.configure({inline:!0}),...a],content:n,onCreate:async({editor:E})=>{const g=E.state.doc.textContent.split(" ").length;_(g),C({editor:E})},onUpdate:({editor:E})=>{const g=E.state.doc.textContent.split(" ").length;_(g);const y=E.getHTML();typeof e=="function"&&e(y)},editable:!u}),P=useColorModeValue("white","slate.700"),k=useColorModeValue("gray.200","slate.700"),x=useDebounce$1(S,1e3);return react.exports.useEffect(()=>{S&&n&&((S==null?void 0:S.getText())||S.commands.setContent(n))},[S,n]),react.exports.useEffect(()=>{S&&T&&m&&C({editor:S})},[T,m,S]),jsxs(EditorContext.Provider,{value:{editor:x},children:[c&&jsxs(Box,{mb:4,children:["Word count: ",jsx("strong",{children:A})]}),jsx(Box,{p:2,pt:0,borderColor:k,borderRadius:4,borderWidth:1,h:"full",bg:P,children:S&&jsxs(Fragment,{children:[!u&&jsxs(Fragment,{children:[jsx(MenuBar,{editor:S,enableSpaceMenu:l,stickyMenuBar:d,isMockTestEditor:m}),jsx(BubbleMenu,{editor:S,pluginKey:"bubbleImageMenu",shouldShow:({editor:E})=>E.isActive("image"),children:jsx(ImageMenu,{editor:S})}),jsx(BubbleMenu,{editor:S,tippyOptions:{placement:"bottom"},pluginKey:"bubbleTextSelection",shouldShow:({editor:E,view:g,state:y})=>E.isActive("link",{"data-contextual":!0})}),m&&jsx(BubbleMenu,{editor:S,tippyOptions:{placement:"auto"},pluginKey:"bubbleTextSelection",shouldShow:({editor:E,view:g,state:y})=>E.isActive("spacing"),children:jsx(QuestionMenu,{editor:S,defaultValues:{questionId:S.isActive("spacing")?S.getAttributes("spacing")["data-question"]:null}})})]}),jsx(EditorContent,{editor:S,ref:b})]})})]})});var TipTap$1=react.exports.memo(TipTap);const CreateQAToForum=({questionText:e})=>{var c;const{data:r}=useQuery(QA_POST_CATEGORIES),{register:n,formState:{errors:a}}=useFormContext(),l=(c=r==null?void 0:r.QAPostCategoriesTree)!=null?c:[];return jsxs(Flex,{direction:"column",w:"full",justifyContent:"stretch",alignItems:"stretch",className:"reply-editor",gap:4,children:[jsx(TextInput,H(N({label:"Ti\xEAu \u0111\u1EC1",placeholder:"Nh\u1EADp ti\xEAu \u0111\u1EC1..."},n("title",{required:!0})),{error:a==null?void 0:a.name})),jsxs(Stack,{spacing:1,children:[jsx(Text,{fontSize:14,children:"C\xE2u h\u1ECFi"}),jsx(Box,{fontWeight:"400",bg:"gray.100",borderWidth:1,w:"100%",p:4,color:"black",rounded:"md",children:e})]}),jsx(Controller,{rules:{required:"Vui l\xF2ng ch\u1ECDn ch\u1EE7 \u0111\u1EC1"},name:"categories",render:({field:u,fieldState:{error:d}})=>jsx(Select$1,N({error:d,label:"Ch\u1EE7 \u0111\u1EC1",options:l,getOptionLabel:m=>m.title,getOptionValue:m=>m.id+"",formatOptionLabel:m=>jsx(Text,{children:m.title})},u))}),jsxs(Stack,{spacing:1,children:[jsx(Text,{fontWeight:"400",fontSize:14,children:"C\xE2u tr\u1EA3 l\u1EDDi"}),jsx(Controller,{name:"answer",render:({field:u})=>jsx(TipTap$1,N({showWordCount:!1,placeholder:"Nh\u1EADp c\xE2u tr\u1EA3 l\u1EDDi..."},u))})]})]})},CreateQAToForumFormModal=({onClose:e,isOpen:r,questionText:n,messageQAOwnerId:a,setIsHoverMessage:l})=>{const c=react.exports.useRef(),u=useForm({mode:"onBlur",reValidateMode:"onChange"}),d=useToast(),[m]=useMutation(CREATE_QA_POST),[b,{loading:A}]=useMutation(POST_COMMENT),_=react.exports.useCallback(async({title:T,categories:C,answer:S})=>{var P;try{const{data:k}=await m({variables:{input:pickBy_1({title:T,content:n,type:"QUESTION",categoryIds:(C==null?void 0:C.id)?[C==null?void 0:C.id]:""},x=>x),userId:a}});await b({variables:{ref:(P=k==null?void 0:k.createQAPost)==null?void 0:P.id,content:S,type:"QAPost"}}),e(),l(!1),await d({title:"Success.",description:"\u0110\u1EA9y c\xE2u h\u1ECFi l\xEAn forum th\xE0nh c\xF4ng",status:"success",duration:3e3,isClosable:!0})}catch(k){console.log(k)}},[n,a,e,l]);return jsx(FormProvider,H(N({},u),{children:jsxs(Modal$1,{finalFocusRef:c,isOpen:r,onClose:e,size:"3xl",children:[jsx(ModalOverlay,{}),jsxs(ModalContent,{children:[jsx(ModalHeader,{children:"T\u1EA1o QA "}),jsx(ModalCloseButton,{}),jsxs("form",{onSubmit:u.handleSubmit(_),children:[jsx(ModalBody,{children:jsx(CreateQAToForum,{questionText:n})}),jsxs(ModalFooter,{children:[jsx(Button$1,{leftIcon:jsx(Icon,{as:AiOutlineSend}),colorScheme:"green",mr:4,type:"submit",isLoading:A,children:"X\xE1c nh\u1EADn"}),jsx(Button$1,{variant:"ghost",onClick:e,children:"H\u1EE7y"})]})]})]})]})}))};function Message({isMyMessage:e,text:r,loading:n,notSeen:a,error:l,createdAt:c,lastSeenMessageId:u,id:d,userData:m,attachments:b,deletedAt:A,from:_,isGroup:T,conversation:C,emojis:S}){var Q,W,q;const[P]=useUser(),{isOpen:k,onOpen:x,onClose:E}=useDisclosure(),[g,y]=react.exports.useState(!1),I=useDebounce$1(g,300),[M,j]=react.exports.useState(null),[O,R]=react.exports.useState(!1),B=useColorModeValue("white","slate.300"),G=(Q=C==null?void 0:C.participants)==null?void 0:Q.find(L=>(L==null?void 0:L.userId)===_),V=react.exports.useMemo(()=>{var L;return jsx(Box,{as:"div",w:6,h:6,rounded:"full",alignItems:"center",justifyContent:"center",display:"flex",flexDirection:"column",position:"absolute",overflow:"hidden",bg:B,className:`${e?"-right-8 top-3":"-left-[30px] bottom-6"}`,children:jsx(MyAvatar,{size:"xs",src:G==null?void 0:G.avatar,name:(L=G==null?void 0:G.fullName)!=null?L:"\u1EA8n danh"})})},[e,B,G==null?void 0:G.avatar,G==null?void 0:G.fullName]);return jsxs(Fragment,{children:[jsx(HStack,{onMouseEnter:()=>!A&&y(!0),onMouseLeave:()=>!A&&y(!1),w:"full",position:"relative",alignItems:"center",justifyContent:e?"flex-end":"flex-start",children:jsx(HStack,{w:"full",alignItems:"center",children:jsx(Box,{w:"full",position:"relative",display:"flex",justifyContent:e?"flex-end":"flex-start ",children:jsxs(Box,{w:"full",display:"flex",flexDirection:"column",alignItems:e?"flex-end":"flex-start",children:[V,!e&&jsx(Text,{color:"gray.400",fontSize:12,children:((q=(W=C==null?void 0:C.participants)==null?void 0:W.find(L=>(L==null?void 0:L.userId)===_))==null?void 0:q.fullName)||"\u1EA8n danh"}),jsxs(Popover,{isOpen:I,placement:e?"left":"right",children:[jsx(PopoverTrigger,{children:jsx(HStack,{align:"center",children:jsx(TextMessage,{isMyMessage:e,text:r,deletedAt:A,createdAt:c,attachments:b,setAttachmentIndex:j,setShowImageViewer:R})})}),jsx(PopoverContent,{w:"65px",bg:"transparent",border:0,boxShadow:"none",_focus:{outline:"none"},children:I&&jsxs(HStack,{alignItems:"center",flexGrow:1,justifyContent:e?"flex-end":"flex-start",children:[jsx(MessageDeleted,{isMyMessage:e,isHoverMessage:!0,setIsHoverMessage:y,id:d}),jsx(MessageReact,{id:d,setIsHoverMessage:y}),zimRolesNumber.map(L=>L.id).includes(P.roleId)&&r&&jsx(MarkAsQuestion,{isParticipant:!e,onOpen:x})]})})]}),jsxs(HStack,{w:"full",justify:e?"flex-end":"flex-start",mt:1,alignItems:"center",spacing:2,children:[jsx(Text,{letterSpacing:.1,fontSize:12,color:"gray.500",children:dayjs(c).format("HH:mm")}),jsx(MessageStatus,{isGroup:T,isMyMessage:e,lastSeenMessageId:u,notSeen:a,id:d,error:l,loading:n,userData:m}),!A&&(S==null?void 0:S.length)>0&&jsx(RenderReactionMessage,{emojis:S})]}),jsx(ImageViewer,{setAttachmentIndex:j,attachments:b,attachmentIndex:M,showImageViewer:O,setShowImageViewer:R})]})})})}),jsx(CreateQAToForumFormModal,{isOpen:k,onClose:E,questionText:r,messageQAOwnerId:_,setIsHoverMessage:y})]})}var Message$1=react.exports.memo(Message);const ListMessage=({conversationId:e,setConversationId:r,ownerId:n,messages:a,participants:l,userId:c,isGroup:u,loading:d,channel:m,isLoadMore:b,onLoadBeforeMessage:A,hasBeforeLoadMoreButton:_,searchLoading:T,onLoadAfterMessage:C,hasAfterLoadMoreButton:S,chatBoxRef:P,totalDocs:k})=>{var Q;const[x,E]=react.exports.useState(null),g=useStore(W=>W.searchStringMessage),[y,I]=react.exports.useState(""),{onMarkSeen:M}=useSendMessage({setContent:()=>{},userId:c,group:m,setConversationId:r}),j=react.exports.useMemo(()=>lodash.exports.uniqBy(lodash.exports.reverse([].concat(a)),W=>W.id),[a]),O=(Q=a.find(W=>{var q;if(W.from===n&&((q=W.seenBy)==null?void 0:q.length)>=2)return!0}))==null?void 0:Q.id,R=l,B=R==null?void 0:R.filter(W=>W.userId!==n)[0],G=j[j.length-1],V=_default$2();return useDeepCompareEffect$2(()=>{var W;e&&(l==null?void 0:l.find(q=>q.userId===n))&&!d&&!((W=G==null?void 0:G.seenBy)==null?void 0:W.includes(n==null?void 0:n.toString()))&&async function(){await V(M(e))}()},[V,n,G,l,d,e]),react.exports.useLayoutEffect(()=>{E(null)},[e]),react.exports.useEffect(()=>{let W;return(P==null?void 0:P.current)&&!g&&x!==e&&(W=setTimeout(()=>{var q;P.current.scrollTop=(q=P.current.scrollHeight)!=null?q:5e3,E(e)},50)),()=>{clearTimeout(W)}},[e,x,g]),jsxs(VStack,{alignItems:"stretch",w:"full",px:10,py:4,spacing:4,children:[_&&k>=(j==null?void 0:j.length)&&jsx(VStack,{children:jsx(Button$1,{w:"100px",h:"30px",onClick:A,isLoading:T,loadingText:"Loading",colorScheme:"teal",variant:"outline",spinnerPlacement:"end",children:"Load more"})}),b&&jsx(Collapse,{in:b,animateOpacity:!0,children:jsx(Box,{pt:2,children:jsx(Loading,{text:"\u0110ang t\u1EA3i tin nh\u1EAFn..."})})}),j==null?void 0:j.map((W,q)=>{var K,Z,Y,le,re;const L=W.from===n,D=moment((K=j[q])==null?void 0:K.createdAt),F=moment((Z=j[q-1])==null?void 0:Z.createdAt),$=moment.duration(D.diff(F)),U=$.asMinutes(),z=U>15&&jsx(SpaceTime,{time:W==null?void 0:W.createdAt});return jsxs(t.Fragment,{children:[z,jsx(Message$1,H(N({},W),{QAId:y,setQAId:I,isGroup:u,attachments:(Y=W==null?void 0:W.attachments)!=null?Y:[],loading:W==null?void 0:W.loading,error:W==null?void 0:W.error,isMyMessage:L,text:W==null?void 0:W.text,id:W==null?void 0:W.id,userData:B,lastSeenMessageId:O,createdAt:W==null?void 0:W.createdAt,notSeen:((le=W==null?void 0:W.seenBy)==null?void 0:le.length)===1&&((re=W==null?void 0:W.seenBy)==null?void 0:re.includes(n))}))]},q)}),S&&(j==null?void 0:j.length)<k&&jsx(VStack,{pb:2,children:jsx(Button$1,{w:"100px",h:"30px",onClick:C,isLoading:T,loadingText:"Loading",colorScheme:"teal",variant:"outline",spinnerPlacement:"end",children:"Load more"})})]})},useGetMessages=({userId:e})=>{var k,x,E;const[r]=useUser(),n=useResetQueries(),[a,l]=react.exports.useState(!1),c=useStore(g=>g.conversationId),u=useStore(g=>g.setConversationId),d=H(N({},pickBy_1({conversationId:c,userId:c?"":e==null?void 0:e.toString(),limit:30},g=>g)),{offset:0}),{data:m,loading:b,fetchMore:A,error:_}=useQuery(MESSAGES,{variables:N({},d),notifyOnNetworkStatusChange:!0,skip:!c&&!e});useSubscription(CONVERSATION_UPDATED,{onSubscriptionData:({client:g,subscriptionData:y})=>{var M,j;const I=g.cache.readQuery({query:MESSAGES,variables:d});if(I==null?void 0:I.messages){let R=[...(j=(M=I==null?void 0:I.messages)==null?void 0:M.docs)!=null?j:[]];const{conversationUpdated:B}=y.data,{lastMessage:G,isDeleteMessage:V,id:Q}=B,{id:W,from:q}=G,L=R==null?void 0:R.find(D=>(D==null?void 0:D.id)===W);V&&!a&&c===Q&&l(!0),!L&&r.id!==q&&c===Q&&(g.writeQuery({query:MESSAGES,variables:d,data:H(N({},I),{messages:H(N({},I.messages),{docs:[G,...R]})})}),setTimeout(()=>{const D=document.getElementById("chat-box");D&&(D.scrollTop=D.scrollHeight)},50))}},shouldResubscribe:!!c});const T=((k=m==null?void 0:m.messages)==null?void 0:k.docs)||[],C=(x=m==null?void 0:m.messages)==null?void 0:x.hasNextPage,{onLoadMore:S,isLoadingMore:P}=useLoadMore({hasNextPage:C,variables:H(N({},d),{offset:T.length+1,limit:10}),fetchMore:A});return react.exports.useEffect(()=>{a&&(n(["messages","getAttachmentsInConversation"]),l(!1))},[a,n,l]),react.exports.useEffect(()=>{var g,y;(T==null?void 0:T.length)>0&&u((y=(g=T==null?void 0:T[0])==null?void 0:g.conversation)==null?void 0:y.id)},[T]),{data:m,loading:b,isLoadMore:P,loadMore:S,hasNextPage:C,messages:T,error:_,totalDocs:(E=m==null?void 0:m.messages)==null?void 0:E.totalDocs}},useGetSearchMessage=({userId:e,options:r})=>{var T,C,S;const n=useStore(P=>P.conversationId),a=useStore(P=>P.beforeId),l=useStore(P=>P.afterId),c=N({},pickBy_1({conversationId:n,userId:n?"":e==null?void 0:e.toString(),limit:10,before:a,after:l},P=>P)),{data:u,loading:d,error:m}=useQuery(MESSAGES,H(N({variables:N({},c)},r),{skip:!a&&!l})),b=((T=u==null?void 0:u.messages)==null?void 0:T.docs)||[],A=(C=u==null?void 0:u.messages)==null?void 0:C.hasNextPage,_=(S=u==null?void 0:u.messages)==null?void 0:S.hasPrevPage;return{data:u,loading:d,hasNextPage:A,hasPrevPage:_,messages:b,error:m}},useSearchMessage=({conversationId:e="",search:r})=>{var P,k;const n=useStore(x=>x.setSearchResult),a=useStore(x=>x.messageId),l=useStore(x=>x.beforeId),[c,u]=react.exports.useState(!1),{refetch:d,called:m,loading:b}=useQuery(SEARCH_MESSAGE,{variables:{conversationId:e,search:r},onCompleted:x=>{var E;n((E=x==null?void 0:x.searchMessage)==null?void 0:E.docs),u(!0)},onError:()=>{n([])},skip:e.length===0||!!a||!!l||!r}),{data:A,refetch:_,loading:T}=useQuery(NEAR_MESSAGE,{variables:{messageId:a,limit:10},skip:!a}),C=(P=A==null?void 0:A.nearbyMessages)==null?void 0:P.hasPrevPage,S=(k=A==null?void 0:A.nearbyMessages)==null?void 0:k.hasNextPage;return react.exports.useEffect(()=>{(m&&r.length||m&&c&&(r==null?void 0:r.length)===0)&&d()},[r,c,m]),react.exports.useEffect(()=>{m&&a&&_()},[a,m]),{loading:T,hasPrevPageNearbyMessages:C,hasNextPageNearbyMessages:S,getSearchResultLoading:b}},ChatContent=({conversationId:e,ownerId:r,userId:n,onSendMessage:a,participants:l,isGroup:c,setConversationId:u,channel:d,sendMessageLoading:m})=>{var Y,le,re;const b=useApolloClient(),A=react.exports.useRef(0),_=react.exports.useRef(0),T=react.exports.useRef(null),C=react.exports.useCallback(async()=>{m||await a({nativeEvent:{text:"Hi !!"},conversationId:e})},[a,e,m]),[S,P]=react.exports.useState(!1),k=useStore(ee=>ee.searchStringMessage),x=useStore(ee=>ee.setBeforeId),E=useStore(ee=>ee.setAfterId),g=useStore(ee=>ee.messageId),y=useStore(ee=>ee.beforeId),I=useStore(ee=>ee.afterId),{messages:M,loading:j,error:O,loadMore:R,isLoadMore:B,hasNextPage:G,totalDocs:V}=useGetMessages({userId:n}),{hasPrevPageNearbyMessages:Q,hasNextPageNearbyMessages:W,getSearchResultLoading:q}=useSearchMessage({conversationId:e,search:k}),{hasNextPage:L,hasPrevPage:D}=useGetSearchMessage({userId:n,options:{onCompleted:ee=>{var X,de,se,te,pe;P(!0);const oe=(X=b.cache.readQuery({query:NEAR_MESSAGE,variables:{messageId:g,limit:10}}))!=null?X:{},me=(se=(de=oe==null?void 0:oe.nearbyMessages)==null?void 0:de.docs)!=null?se:[],ne=(pe=(te=ee==null?void 0:ee.messages)==null?void 0:te.docs)!=null?pe:[],ce=y?[...me,...ne]:[...ne,...me],fe=uniqBy_1(ce,"id");_.current=T.current.scrollHeight,b.cache.writeQuery({query:NEAR_MESSAGE,variables:{messageId:g,limit:10},data:H(N({},oe),{nearbyMessages:H(N({},oe==null?void 0:oe.nearbyMessages),{docs:fe})})}),P(!1),y&&(T.current.scrollTop=T.current.scrollHeight-_.current)}}}),F=(Y=b.cache.readQuery({query:NEAR_MESSAGE,variables:{messageId:g,limit:10}}))!=null?Y:{},$=(re=(le=F==null?void 0:F.nearbyMessages)==null?void 0:le.docs)!=null?re:[],U=(k==null?void 0:k.length)>0&&(g==null?void 0:g.length)>0?$:M,z=react.exports.useCallback(()=>{var ee;E(""),x((ee=$==null?void 0:$[$.length-1])==null?void 0:ee.id)},[$]),K=react.exports.useCallback(()=>{var ee;x(""),E((ee=$==null?void 0:$[0])==null?void 0:ee.id)},[$]),Z=react.exports.useCallback(async ee=>{const{scrollTop:oe}=ee.nativeEvent.target;oe<=20&&oe<A.current&&G&&!B&&!k&&(await R(),B||(T.current.scrollTop=oe+60)),A.current=oe},[G,B,k]);return O?jsx(HStack,{bg:"blackAlpha.400",h:"100%",justifyContent:"center",children:jsx(Loading,{color:"white"})}):jsx(Box,{ref:T,bg:useColorModeValue("white","#10172a"),h:"full",w:"full",id:"chat-box",overflow:"auto",position:"relative",onScroll:Z,children:(U==null?void 0:U.length)===0&&!q?jsxs(VStack,{w:"100%",h:"100%",display:"flex",flexDirection:"column",alignItems:"center",justifyContent:"center",spacing:8,children:[jsx(VStack,{w:"40%",spacing:4,maxW:"360px",maxH:"400px",children:jsx(Image$1,{src:sayHi,objectFit:"contain"})}),(e||n)&&(U==null?void 0:U.length)===0&&!j&&!m&&jsx(Box,{h:10,px:2,bg:"red.400",rounded:"lg",as:"button",onClick:C,fontSize:14,color:"white",fontWeight:500,children:"G\u1EEDi l\u1EDDi ch\xE0o"})]}):q?jsx(Loading,{}):jsx(Box,{h:"full",w:"full",children:jsx(ListMessage,{totalDocs:V,chatBoxRef:T,hasBeforeLoadMoreButton:(!y&&Q||D)&&(k==null?void 0:k.length)>0,hasAfterLoadMoreButton:(!I&&W||L)&&(k==null?void 0:k.length)>0,onLoadBeforeMessage:z,onLoadAfterMessage:K,isLoadMore:B,searchLoading:S,loading:j,isGroup:c,ownerId:r,messages:U,conversationId:e,participants:l,userId:n,setConversationId:u,channel:d})})})};var COMMON_MIME_TYPES=new Map([["aac","audio/aac"],["abw","application/x-abiword"],["arc","application/x-freearc"],["avif","image/avif"],["avi","video/x-msvideo"],["azw","application/vnd.amazon.ebook"],["bin","application/octet-stream"],["bmp","image/bmp"],["bz","application/x-bzip"],["bz2","application/x-bzip2"],["cda","application/x-cdf"],["csh","application/x-csh"],["css","text/css"],["csv","text/csv"],["doc","application/msword"],["docx","application/vnd.openxmlformats-officedocument.wordprocessingml.document"],["eot","application/vnd.ms-fontobject"],["epub","application/epub+zip"],["gz","application/gzip"],["gif","image/gif"],["htm","text/html"],["html","text/html"],["ico","image/vnd.microsoft.icon"],["ics","text/calendar"],["jar","application/java-archive"],["jpeg","image/jpeg"],["jpg","image/jpeg"],["js","text/javascript"],["json","application/json"],["jsonld","application/ld+json"],["mid","audio/midi"],["midi","audio/midi"],["mjs","text/javascript"],["mp3","audio/mpeg"],["mp4","video/mp4"],["mpeg","video/mpeg"],["mpkg","application/vnd.apple.installer+xml"],["odp","application/vnd.oasis.opendocument.presentation"],["ods","application/vnd.oasis.opendocument.spreadsheet"],["odt","application/vnd.oasis.opendocument.text"],["oga","audio/ogg"],["ogv","video/ogg"],["ogx","application/ogg"],["opus","audio/opus"],["otf","font/otf"],["png","image/png"],["pdf","application/pdf"],["php","application/x-httpd-php"],["ppt","application/vnd.ms-powerpoint"],["pptx","application/vnd.openxmlformats-officedocument.presentationml.presentation"],["rar","application/vnd.rar"],["rtf","application/rtf"],["sh","application/x-sh"],["svg","image/svg+xml"],["swf","application/x-shockwave-flash"],["tar","application/x-tar"],["tif","image/tiff"],["tiff","image/tiff"],["ts","video/mp2t"],["ttf","font/ttf"],["txt","text/plain"],["vsd","application/vnd.visio"],["wav","audio/wav"],["weba","audio/webm"],["webm","video/webm"],["webp","image/webp"],["woff","font/woff"],["woff2","font/woff2"],["xhtml","application/xhtml+xml"],["xls","application/vnd.ms-excel"],["xlsx","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"],["xml","application/xml"],["xul","application/vnd.mozilla.xul+xml"],["zip","application/zip"],["7z","application/x-7z-compressed"],["mkv","video/x-matroska"],["mov","video/quicktime"],["msg","application/vnd.ms-outlook"]]);function toFileWithPath(e,r){var n=withMimeType(e);if(typeof n.path!="string"){var a=e.webkitRelativePath;Object.defineProperty(n,"path",{value:typeof r=="string"?r:typeof a=="string"&&a.length>0?a:e.name,writable:!1,configurable:!1,enumerable:!0})}return n}function withMimeType(e){var r=e.name,n=r&&r.lastIndexOf(".")!==-1;if(n&&!e.type){var a=r.split(".").pop().toLowerCase(),l=COMMON_MIME_TYPES.get(a);l&&Object.defineProperty(e,"type",{value:l,writable:!1,configurable:!1,enumerable:!0})}return e}var FILES_TO_IGNORE=[".DS_Store","Thumbs.db"];function fromEvent(e){return __awaiter(this,void 0,void 0,function(){return __generator(this,function(r){return isObject(e)&&isDataTransfer(e)?[2,getDataTransferFiles(e.dataTransfer,e.type)]:isChangeEvt(e)?[2,getInputFiles(e)]:Array.isArray(e)&&e.every(function(n){return"getFile"in n&&typeof n.getFile=="function"})?[2,getFsHandleFiles(e)]:[2,[]]})})}function isDataTransfer(e){return isObject(e.dataTransfer)}function isChangeEvt(e){return isObject(e)&&isObject(e.target)}function isObject(e){return typeof e=="object"&&e!==null}function getInputFiles(e){return fromList(e.target.files).map(function(r){return toFileWithPath(r)})}function getFsHandleFiles(e){return __awaiter(this,void 0,void 0,function(){var r;return __generator(this,function(n){switch(n.label){case 0:return[4,Promise.all(e.map(function(a){return a.getFile()}))];case 1:return r=n.sent(),[2,r.map(function(a){return toFileWithPath(a)})]}})})}function getDataTransferFiles(e,r){return __awaiter(this,void 0,void 0,function(){var n,a;return __generator(this,function(l){switch(l.label){case 0:return e===null?[2,[]]:e.items?(n=fromList(e.items).filter(function(c){return c.kind==="file"}),r!=="drop"?[2,n]:[4,Promise.all(n.map(toFilePromises))]):[3,2];case 1:return a=l.sent(),[2,noIgnoredFiles(flatten(a))];case 2:return[2,noIgnoredFiles(fromList(e.files).map(function(c){return toFileWithPath(c)}))]}})})}function noIgnoredFiles(e){return e.filter(function(r){return FILES_TO_IGNORE.indexOf(r.name)===-1})}function fromList(e){if(e===null)return[];for(var r=[],n=0;n<e.length;n++){var a=e[n];r.push(a)}return r}function toFilePromises(e){if(typeof e.webkitGetAsEntry!="function")return fromDataTransferItem(e);var r=e.webkitGetAsEntry();return r&&r.isDirectory?fromDirEntry(r):fromDataTransferItem(e)}function flatten(e){return e.reduce(function(r,n){return __spread(r,Array.isArray(n)?flatten(n):[n])},[])}function fromDataTransferItem(e){var r=e.getAsFile();if(!r)return Promise.reject(e+" is not a File");var n=toFileWithPath(r);return Promise.resolve(n)}function fromEntry(e){return __awaiter(this,void 0,void 0,function(){return __generator(this,function(r){return[2,e.isDirectory?fromDirEntry(e):fromFileEntry(e)]})})}function fromDirEntry(e){var r=e.createReader();return new Promise(function(n,a){var l=[];function c(){var u=this;r.readEntries(function(d){return __awaiter(u,void 0,void 0,function(){var m,b,A;return __generator(this,function(_){switch(_.label){case 0:if(d.length)return[3,5];_.label=1;case 1:return _.trys.push([1,3,,4]),[4,Promise.all(l)];case 2:return m=_.sent(),n(m),[3,4];case 3:return b=_.sent(),a(b),[3,4];case 4:return[3,6];case 5:A=Promise.all(d.map(fromEntry)),l.push(A),c(),_.label=6;case 6:return[2]}})})},function(d){a(d)})}c()})}function fromFileEntry(e){return __awaiter(this,void 0,void 0,function(){return __generator(this,function(r){return[2,new Promise(function(n,a){e.file(function(l){var c=toFileWithPath(l,e.fullPath);n(c)},function(l){a(l)})})]})})}var _default$1=function(e,r){if(e&&r){var n=Array.isArray(r)?r:r.split(","),a=e.name||"",l=(e.type||"").toLowerCase(),c=l.replace(/\/.*$/,"");return n.some(function(u){var d=u.trim().toLowerCase();return d.charAt(0)==="."?a.toLowerCase().endsWith(d):d.endsWith("/*")?c===d.replace(/\/.*$/,""):l===d})}return!0};function ownKeys$1(e,r){var n=Object.keys(e);if(Object.getOwnPropertySymbols){var a=Object.getOwnPropertySymbols(e);r&&(a=a.filter(function(l){return Object.getOwnPropertyDescriptor(e,l).enumerable})),n.push.apply(n,a)}return n}function _objectSpread$1(e){for(var r=1;r<arguments.length;r++){var n=arguments[r]!=null?arguments[r]:{};r%2?ownKeys$1(Object(n),!0).forEach(function(a){_defineProperty$1(e,a,n[a])}):Object.getOwnPropertyDescriptors?Object.defineProperties(e,Object.getOwnPropertyDescriptors(n)):ownKeys$1(Object(n)).forEach(function(a){Object.defineProperty(e,a,Object.getOwnPropertyDescriptor(n,a))})}return e}function _defineProperty$1(e,r,n){return r in e?Object.defineProperty(e,r,{value:n,enumerable:!0,configurable:!0,writable:!0}):e[r]=n,e}function _slicedToArray$1(e,r){return _arrayWithHoles$1(e)||_iterableToArrayLimit$1(e,r)||_unsupportedIterableToArray$1(e,r)||_nonIterableRest$1()}function _nonIterableRest$1(){throw new TypeError(`Invalid attempt to destructure non-iterable instance.
In order to be iterable, non-array objects must have a [Symbol.iterator]() method.`)}function _unsupportedIterableToArray$1(e,r){if(!!e){if(typeof e=="string")return _arrayLikeToArray$1(e,r);var n=Object.prototype.toString.call(e).slice(8,-1);if(n==="Object"&&e.constructor&&(n=e.constructor.name),n==="Map"||n==="Set")return Array.from(e);if(n==="Arguments"||/^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n))return _arrayLikeToArray$1(e,r)}}function _arrayLikeToArray$1(e,r){(r==null||r>e.length)&&(r=e.length);for(var n=0,a=new Array(r);n<r;n++)a[n]=e[n];return a}function _iterableToArrayLimit$1(e,r){var n=e==null?null:typeof Symbol!="undefined"&&e[Symbol.iterator]||e["@@iterator"];if(n!=null){var a=[],l=!0,c=!1,u,d;try{for(n=n.call(e);!(l=(u=n.next()).done)&&(a.push(u.value),!(r&&a.length===r));l=!0);}catch(m){c=!0,d=m}finally{try{!l&&n.return!=null&&n.return()}finally{if(c)throw d}}return a}}function _arrayWithHoles$1(e){if(Array.isArray(e))return e}var FILE_INVALID_TYPE="file-invalid-type",FILE_TOO_LARGE="file-too-large",FILE_TOO_SMALL="file-too-small",TOO_MANY_FILES="too-many-files",getInvalidTypeRejectionErr=function e(r){r=Array.isArray(r)&&r.length===1?r[0]:r;var n=Array.isArray(r)?"one of ".concat(r.join(", ")):r;return{code:FILE_INVALID_TYPE,message:"File type must be ".concat(n)}},getTooLargeRejectionErr=function e(r){return{code:FILE_TOO_LARGE,message:"File is larger than ".concat(r," ").concat(r===1?"byte":"bytes")}},getTooSmallRejectionErr=function e(r){return{code:FILE_TOO_SMALL,message:"File is smaller than ".concat(r," ").concat(r===1?"byte":"bytes")}},TOO_MANY_FILES_REJECTION={code:TOO_MANY_FILES,message:"Too many files"};function fileAccepted(e,r){var n=e.type==="application/x-moz-file"||_default$1(e,r);return[n,n?null:getInvalidTypeRejectionErr(r)]}function fileMatchSize(e,r,n){if(isDefined(e.size))if(isDefined(r)&&isDefined(n)){if(e.size>n)return[!1,getTooLargeRejectionErr(n)];if(e.size<r)return[!1,getTooSmallRejectionErr(r)]}else{if(isDefined(r)&&e.size<r)return[!1,getTooSmallRejectionErr(r)];if(isDefined(n)&&e.size>n)return[!1,getTooLargeRejectionErr(n)]}return[!0,null]}function isDefined(e){return e!=null}function allFilesAccepted(e){var r=e.files,n=e.accept,a=e.minSize,l=e.maxSize,c=e.multiple,u=e.maxFiles;return!c&&r.length>1||c&&u>=1&&r.length>u?!1:r.every(function(d){var m=fileAccepted(d,n),b=_slicedToArray$1(m,1),A=b[0],_=fileMatchSize(d,a,l),T=_slicedToArray$1(_,1),C=T[0];return A&&C})}function isPropagationStopped(e){return typeof e.isPropagationStopped=="function"?e.isPropagationStopped():typeof e.cancelBubble!="undefined"?e.cancelBubble:!1}function isEvtWithFiles(e){return e.dataTransfer?Array.prototype.some.call(e.dataTransfer.types,function(r){return r==="Files"||r==="application/x-moz-file"}):!!e.target&&!!e.target.files}function onDocumentDragOver(e){e.preventDefault()}function isIe(e){return e.indexOf("MSIE")!==-1||e.indexOf("Trident/")!==-1}function isEdge(e){return e.indexOf("Edge/")!==-1}function isIeOrEdge(){var e=arguments.length>0&&arguments[0]!==void 0?arguments[0]:window.navigator.userAgent;return isIe(e)||isEdge(e)}function composeEventHandlers(){for(var e=arguments.length,r=new Array(e),n=0;n<e;n++)r[n]=arguments[n];return function(a){for(var l=arguments.length,c=new Array(l>1?l-1:0),u=1;u<l;u++)c[u-1]=arguments[u];return r.some(function(d){return!isPropagationStopped(a)&&d&&d.apply(void 0,[a].concat(c)),isPropagationStopped(a)})}}function canUseFileSystemAccessAPI(){return"showOpenFilePicker"in window}function filePickerOptionsTypes(e){return e=typeof e=="string"?e.split(","):e,[{description:"everything",accept:Array.isArray(e)?e.filter(function(r){return r==="audio/*"||r==="video/*"||r==="image/*"||r==="text/*"||/\w+\/[-+.\w]+/g.test(r)}).reduce(function(r,n){return _objectSpread$1(_objectSpread$1({},r),{},_defineProperty$1({},n,[]))},{}):{}}]}function isAbort(e){return e instanceof DOMException&&(e.name==="AbortError"||e.code===e.ABORT_ERR)}function isSecurityError(e){return e instanceof DOMException&&(e.name==="SecurityError"||e.code===e.SECURITY_ERR)}var _excluded=["children"],_excluded2=["open"],_excluded3=["refKey","role","onKeyDown","onFocus","onBlur","onClick","onDragEnter","onDragOver","onDragLeave","onDrop"],_excluded4=["refKey","onChange","onClick"];function _toConsumableArray(e){return _arrayWithoutHoles(e)||_iterableToArray(e)||_unsupportedIterableToArray(e)||_nonIterableSpread()}function _nonIterableSpread(){throw new TypeError(`Invalid attempt to spread non-iterable instance.
In order to be iterable, non-array objects must have a [Symbol.iterator]() method.`)}function _iterableToArray(e){if(typeof Symbol!="undefined"&&e[Symbol.iterator]!=null||e["@@iterator"]!=null)return Array.from(e)}function _arrayWithoutHoles(e){if(Array.isArray(e))return _arrayLikeToArray(e)}function _slicedToArray(e,r){return _arrayWithHoles(e)||_iterableToArrayLimit(e,r)||_unsupportedIterableToArray(e,r)||_nonIterableRest()}function _nonIterableRest(){throw new TypeError(`Invalid attempt to destructure non-iterable instance.
In order to be iterable, non-array objects must have a [Symbol.iterator]() method.`)}function _unsupportedIterableToArray(e,r){if(!!e){if(typeof e=="string")return _arrayLikeToArray(e,r);var n=Object.prototype.toString.call(e).slice(8,-1);if(n==="Object"&&e.constructor&&(n=e.constructor.name),n==="Map"||n==="Set")return Array.from(e);if(n==="Arguments"||/^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n))return _arrayLikeToArray(e,r)}}function _arrayLikeToArray(e,r){(r==null||r>e.length)&&(r=e.length);for(var n=0,a=new Array(r);n<r;n++)a[n]=e[n];return a}function _iterableToArrayLimit(e,r){var n=e==null?null:typeof Symbol!="undefined"&&e[Symbol.iterator]||e["@@iterator"];if(n!=null){var a=[],l=!0,c=!1,u,d;try{for(n=n.call(e);!(l=(u=n.next()).done)&&(a.push(u.value),!(r&&a.length===r));l=!0);}catch(m){c=!0,d=m}finally{try{!l&&n.return!=null&&n.return()}finally{if(c)throw d}}return a}}function _arrayWithHoles(e){if(Array.isArray(e))return e}function ownKeys(e,r){var n=Object.keys(e);if(Object.getOwnPropertySymbols){var a=Object.getOwnPropertySymbols(e);r&&(a=a.filter(function(l){return Object.getOwnPropertyDescriptor(e,l).enumerable})),n.push.apply(n,a)}return n}function _objectSpread(e){for(var r=1;r<arguments.length;r++){var n=arguments[r]!=null?arguments[r]:{};r%2?ownKeys(Object(n),!0).forEach(function(a){_defineProperty(e,a,n[a])}):Object.getOwnPropertyDescriptors?Object.defineProperties(e,Object.getOwnPropertyDescriptors(n)):ownKeys(Object(n)).forEach(function(a){Object.defineProperty(e,a,Object.getOwnPropertyDescriptor(n,a))})}return e}function _defineProperty(e,r,n){return r in e?Object.defineProperty(e,r,{value:n,enumerable:!0,configurable:!0,writable:!0}):e[r]=n,e}function _objectWithoutProperties(e,r){if(e==null)return{};var n=_objectWithoutPropertiesLoose$1(e,r),a,l;if(Object.getOwnPropertySymbols){var c=Object.getOwnPropertySymbols(e);for(l=0;l<c.length;l++)a=c[l],!(r.indexOf(a)>=0)&&(!Object.prototype.propertyIsEnumerable.call(e,a)||(n[a]=e[a]))}return n}function _objectWithoutPropertiesLoose$1(e,r){if(e==null)return{};var n={},a=Object.keys(e),l,c;for(c=0;c<a.length;c++)l=a[c],!(r.indexOf(l)>=0)&&(n[l]=e[l]);return n}var Dropzone=react.exports.forwardRef(function(e,r){var n=e.children,a=_objectWithoutProperties(e,_excluded),l=useDropzone(a),c=l.open,u=_objectWithoutProperties(l,_excluded2);return react.exports.useImperativeHandle(r,function(){return{open:c}},[c]),jsx(Fragment,{children:n(_objectSpread(_objectSpread({},u),{},{open:c}))})});Dropzone.displayName="Dropzone";var defaultProps={disabled:!1,getFilesFromEvent:fromEvent,maxSize:1/0,minSize:0,multiple:!0,maxFiles:0,preventDropOnDocument:!0,noClick:!1,noKeyboard:!1,noDrag:!1,noDragEventsBubbling:!1,validator:null,useFsAccessApi:!0};Dropzone.defaultProps=defaultProps;Dropzone.propTypes={children:PropTypes.func,accept:PropTypes.oneOfType([PropTypes.string,PropTypes.arrayOf(PropTypes.string)]),multiple:PropTypes.bool,preventDropOnDocument:PropTypes.bool,noClick:PropTypes.bool,noKeyboard:PropTypes.bool,noDrag:PropTypes.bool,noDragEventsBubbling:PropTypes.bool,minSize:PropTypes.number,maxSize:PropTypes.number,maxFiles:PropTypes.number,disabled:PropTypes.bool,getFilesFromEvent:PropTypes.func,onFileDialogCancel:PropTypes.func,onFileDialogOpen:PropTypes.func,useFsAccessApi:PropTypes.bool,onDragEnter:PropTypes.func,onDragLeave:PropTypes.func,onDragOver:PropTypes.func,onDrop:PropTypes.func,onDropAccepted:PropTypes.func,onDropRejected:PropTypes.func,validator:PropTypes.func};var initialState={isFocused:!1,isFileDialogActive:!1,isDragActive:!1,isDragAccept:!1,isDragReject:!1,draggedFiles:[],acceptedFiles:[],fileRejections:[]};function useDropzone(){var e=arguments.length>0&&arguments[0]!==void 0?arguments[0]:{},r=_objectSpread(_objectSpread({},defaultProps),e),n=r.accept,a=r.disabled,l=r.getFilesFromEvent,c=r.maxSize,u=r.minSize,d=r.multiple,m=r.maxFiles,b=r.onDragEnter,A=r.onDragLeave,_=r.onDragOver,T=r.onDrop,C=r.onDropAccepted,S=r.onDropRejected,P=r.onFileDialogCancel,k=r.onFileDialogOpen,x=r.useFsAccessApi,E=r.preventDropOnDocument,g=r.noClick,y=r.noKeyboard,I=r.noDrag,M=r.noDragEventsBubbling,j=r.validator,O=react.exports.useMemo(function(){return typeof k=="function"?k:noop$1},[k]),R=react.exports.useMemo(function(){return typeof P=="function"?P:noop$1},[P]),B=react.exports.useRef(null),G=react.exports.useRef(null),V=react.exports.useReducer(reducer,initialState),Q=_slicedToArray(V,2),W=Q[0],q=Q[1],L=W.isFocused,D=W.isFileDialogActive,F=W.draggedFiles,$=react.exports.useRef(typeof window!="undefined"&&window.isSecureContext&&x&&canUseFileSystemAccessAPI()),U=function(){!$.current&&D&&setTimeout(function(){if(G.current){var ae=G.current.files;ae.length||(q({type:"closeDialog"}),R())}},300)};react.exports.useEffect(function(){return window.addEventListener("focus",U,!1),function(){window.removeEventListener("focus",U,!1)}},[G,D,R,$]);var z=react.exports.useRef([]),K=function(ae){B.current&&B.current.contains(ae.target)||(ae.preventDefault(),z.current=[])};react.exports.useEffect(function(){return E&&(document.addEventListener("dragover",onDocumentDragOver,!1),document.addEventListener("drop",K,!1)),function(){E&&(document.removeEventListener("dragover",onDocumentDragOver),document.removeEventListener("drop",K))}},[B,E]);var Z=react.exports.useCallback(function(J){J.preventDefault(),J.persist(),te(J),z.current=[].concat(_toConsumableArray(z.current),[J.target]),isEvtWithFiles(J)&&Promise.resolve(l(J)).then(function(ae){isPropagationStopped(J)&&!M||(q({draggedFiles:ae,isDragActive:!0,type:"setDraggedFiles"}),b&&b(J))})},[l,b,M]),Y=react.exports.useCallback(function(J){J.preventDefault(),J.persist(),te(J);var ae=isEvtWithFiles(J);if(ae&&J.dataTransfer)try{J.dataTransfer.dropEffect="copy"}catch{}return ae&&_&&_(J),!1},[_,M]),le=react.exports.useCallback(function(J){J.preventDefault(),J.persist(),te(J);var ae=z.current.filter(function(ye){return B.current&&B.current.contains(ye)}),ve=ae.indexOf(J.target);ve!==-1&&ae.splice(ve,1),z.current=ae,!(ae.length>0)&&(q({isDragActive:!1,type:"setDraggedFiles",draggedFiles:[]}),isEvtWithFiles(J)&&A&&A(J))},[B,A,M]),re=react.exports.useCallback(function(J,ae){var ve=[],ye=[];J.forEach(function(be){var Ie=fileAccepted(be,n),Pe=_slicedToArray(Ie,2),we=Pe[0],Me=Pe[1],je=fileMatchSize(be,u,c),ke=_slicedToArray(je,2),Fe=ke[0],Oe=ke[1],$e=j?j(be):null;if(we&&Fe&&!$e)ve.push(be);else{var De=[Me,Oe];$e&&(De=De.concat($e)),ye.push({file:be,errors:De.filter(function(Le){return Le})})}}),(!d&&ve.length>1||d&&m>=1&&ve.length>m)&&(ve.forEach(function(be){ye.push({file:be,errors:[TOO_MANY_FILES_REJECTION]})}),ve.splice(0)),q({acceptedFiles:ve,fileRejections:ye,type:"setFiles"}),T&&T(ve,ye,ae),ye.length>0&&S&&S(ye,ae),ve.length>0&&C&&C(ve,ae)},[q,d,n,u,c,m,T,C,S,j]),ee=react.exports.useCallback(function(J){J.preventDefault(),J.persist(),te(J),z.current=[],isEvtWithFiles(J)&&Promise.resolve(l(J)).then(function(ae){isPropagationStopped(J)&&!M||re(ae,J)}),q({type:"reset"})},[l,re,M]),oe=react.exports.useCallback(function(){if($.current){q({type:"openDialog"}),O();var J={multiple:d,types:filePickerOptionsTypes(n)};window.showOpenFilePicker(J).then(function(ae){return l(ae)}).then(function(ae){re(ae,null),q({type:"closeDialog"})}).catch(function(ae){isAbort(ae)?(R(ae),q({type:"closeDialog"})):isSecurityError(ae)&&($.current=!1,G.current&&(G.current.value=null,G.current.click()))});return}G.current&&(q({type:"openDialog"}),O(),G.current.value=null,G.current.click())},[q,O,R,x,re,n,d]),me=react.exports.useCallback(function(J){!B.current||!B.current.isEqualNode(J.target)||(J.keyCode===32||J.keyCode===13)&&(J.preventDefault(),oe())},[B,oe]),ne=react.exports.useCallback(function(){q({type:"focus"})},[]),ce=react.exports.useCallback(function(){q({type:"blur"})},[]),fe=react.exports.useCallback(function(){g||(isIeOrEdge()?setTimeout(oe,0):oe())},[g,oe]),X=function(ae){return a?null:ae},de=function(ae){return y?null:X(ae)},se=function(ae){return I?null:X(ae)},te=function(ae){M&&ae.stopPropagation()},pe=react.exports.useMemo(function(){return function(){var J=arguments.length>0&&arguments[0]!==void 0?arguments[0]:{},ae=J.refKey,ve=ae===void 0?"ref":ae,ye=J.role,be=J.onKeyDown,Ie=J.onFocus,Pe=J.onBlur,we=J.onClick,Me=J.onDragEnter,je=J.onDragOver,ke=J.onDragLeave,Fe=J.onDrop,Oe=_objectWithoutProperties(J,_excluded3);return _objectSpread(_objectSpread(_defineProperty({onKeyDown:de(composeEventHandlers(be,me)),onFocus:de(composeEventHandlers(Ie,ne)),onBlur:de(composeEventHandlers(Pe,ce)),onClick:X(composeEventHandlers(we,fe)),onDragEnter:se(composeEventHandlers(Me,Z)),onDragOver:se(composeEventHandlers(je,Y)),onDragLeave:se(composeEventHandlers(ke,le)),onDrop:se(composeEventHandlers(Fe,ee)),role:typeof ye=="string"&&ye!==""?ye:"button"},ve,B),!a&&!y?{tabIndex:0}:{}),Oe)}},[B,me,ne,ce,fe,Z,Y,le,ee,y,I,a]),ge=react.exports.useCallback(function(J){J.stopPropagation()},[]),Ce=react.exports.useMemo(function(){return function(){var J=arguments.length>0&&arguments[0]!==void 0?arguments[0]:{},ae=J.refKey,ve=ae===void 0?"ref":ae,ye=J.onChange,be=J.onClick,Ie=_objectWithoutProperties(J,_excluded4),Pe=_defineProperty({accept:n,multiple:d,type:"file",style:{display:"none"},onChange:X(composeEventHandlers(ye,ee)),onClick:X(composeEventHandlers(be,ge)),autoComplete:"off",tabIndex:-1},ve,G);return _objectSpread(_objectSpread({},Pe),Ie)}},[G,n,d,ee,a]),he=F.length,Ee=he>0&&allFilesAccepted({files:F,accept:n,minSize:u,maxSize:c,multiple:d,maxFiles:m}),_e=he>0&&!Ee;return _objectSpread(_objectSpread({},W),{},{isDragAccept:Ee,isDragReject:_e,isFocused:L&&!a,getRootProps:pe,getInputProps:Ce,rootRef:B,inputRef:G,open:X(oe)})}function reducer(e,r){switch(r.type){case"focus":return _objectSpread(_objectSpread({},e),{},{isFocused:!0});case"blur":return _objectSpread(_objectSpread({},e),{},{isFocused:!1});case"openDialog":return _objectSpread(_objectSpread({},initialState),{},{isFileDialogActive:!0});case"closeDialog":return _objectSpread(_objectSpread({},e),{},{isFileDialogActive:!1});case"setDraggedFiles":var n=r.isDragActive,a=r.draggedFiles;return _objectSpread(_objectSpread({},e),{},{draggedFiles:a,isDragActive:n});case"setFiles":return _objectSpread(_objectSpread({},e),{},{acceptedFiles:r.acceptedFiles,fileRejections:r.fileRejections});case"reset":return _objectSpread({},initialState);default:return e}}function noop$1(){}function _extends(){return _extends=Object.assign||function(e){for(var r=1;r<arguments.length;r++){var n=arguments[r];for(var a in n)Object.prototype.hasOwnProperty.call(n,a)&&(e[a]=n[a])}return e},_extends.apply(this,arguments)}function _objectWithoutPropertiesLoose(e,r){if(e==null)return{};var n={},a=Object.keys(e),l,c;for(c=0;c<a.length;c++)l=a[c],!(r.indexOf(l)>=0)&&(n[l]=e[l]);return n}var index$3=react.exports.useLayoutEffect,useLatest=function e(r){var n=react.exports.useRef(r);return index$3(function(){n.current=r}),n},updateRef=function e(r,n){if(typeof r=="function"){r(n);return}r.current=n},useComposedRef=function e(r,n){var a=react.exports.useRef();return react.exports.useCallback(function(l){r.current=l,a.current&&updateRef(a.current,null),a.current=n,!!n&&updateRef(n,l)},[n])},HIDDEN_TEXTAREA_STYLE={"min-height":"0","max-height":"none",height:"0",visibility:"hidden",overflow:"hidden",position:"absolute","z-index":"-1000",top:"0",right:"0"},forceHiddenStyles=function e(r){Object.keys(HIDDEN_TEXTAREA_STYLE).forEach(function(n){r.style.setProperty(n,HIDDEN_TEXTAREA_STYLE[n],"important")})},hiddenTextarea=null,getHeight=function e(r,n){var a=r.scrollHeight;return n.sizingStyle.boxSizing==="border-box"?a+n.borderSize:a-n.paddingSize};function calculateNodeHeight(e,r,n,a){n===void 0&&(n=1),a===void 0&&(a=1/0),hiddenTextarea||(hiddenTextarea=document.createElement("textarea"),hiddenTextarea.setAttribute("tabindex","-1"),hiddenTextarea.setAttribute("aria-hidden","true"),forceHiddenStyles(hiddenTextarea)),hiddenTextarea.parentNode===null&&document.body.appendChild(hiddenTextarea);var l=e.paddingSize,c=e.borderSize,u=e.sizingStyle,d=u.boxSizing;Object.keys(u).forEach(function(T){var C=T;hiddenTextarea.style[C]=u[C]}),forceHiddenStyles(hiddenTextarea),hiddenTextarea.value=r;var m=getHeight(hiddenTextarea,e);hiddenTextarea.value="x";var b=hiddenTextarea.scrollHeight-l,A=b*n;d==="border-box"&&(A=A+l+c),m=Math.max(A,m);var _=b*a;return d==="border-box"&&(_=_+l+c),m=Math.min(_,m),[m,b]}var noop=function e(){},pick=function e(r,n){return r.reduce(function(a,l){return a[l]=n[l],a},{})},SIZING_STYLE=["borderBottomWidth","borderLeftWidth","borderRightWidth","borderTopWidth","boxSizing","fontFamily","fontSize","fontStyle","fontWeight","letterSpacing","lineHeight","paddingBottom","paddingLeft","paddingRight","paddingTop","tabSize","textIndent","textRendering","textTransform","width","wordBreak"],isIE=!!document.documentElement.currentStyle,getSizingData=function e(r){var n=window.getComputedStyle(r);if(n===null)return null;var a=pick(SIZING_STYLE,n),l=a.boxSizing;if(l==="")return null;isIE&&l==="border-box"&&(a.width=parseFloat(a.width)+parseFloat(a.borderRightWidth)+parseFloat(a.borderLeftWidth)+parseFloat(a.paddingRight)+parseFloat(a.paddingLeft)+"px");var c=parseFloat(a.paddingBottom)+parseFloat(a.paddingTop),u=parseFloat(a.borderBottomWidth)+parseFloat(a.borderTopWidth);return{sizingStyle:a,paddingSize:c,borderSize:u}},useWindowResizeListener=function e(r){var n=useLatest(r);react.exports.useLayoutEffect(function(){var a=function(c){n.current(c)};return window.addEventListener("resize",a),function(){window.removeEventListener("resize",a)}},[])},TextareaAutosize=function e(r,n){var a=r.cacheMeasurements,l=r.maxRows,c=r.minRows,u=r.onChange,d=u===void 0?noop:u,m=r.onHeightChange,b=m===void 0?noop:m,A=_objectWithoutPropertiesLoose(r,["cacheMeasurements","maxRows","minRows","onChange","onHeightChange"]),_=A.value!==void 0,T=react.exports.useRef(null),C=useComposedRef(T,n),S=react.exports.useRef(0),P=react.exports.useRef(),k=function(){var g=T.current,y=a&&P.current?P.current:getSizingData(g);if(!!y){P.current=y;var I=calculateNodeHeight(y,g.value||g.placeholder||"x",c,l),M=I[0],j=I[1];S.current!==M&&(S.current=M,g.style.setProperty("height",M+"px","important"),b(M,{rowHeight:j}))}},x=function(g){_||k(),d(g)};return react.exports.useLayoutEffect(k),useWindowResizeListener(k),react.exports.createElement("textarea",_extends({},A,{onChange:x,ref:C}))},index$2=react.exports.forwardRef(TextareaAutosize),ResizeTextarea=index$2;gql`
  mutation updateMedia($id: ObjectID!, $input: UpdateMediaInput!)
  @api(name: "zim") {
    updateMedia(id: $id, input: $input) {
      id
      title
    }
  }
`;gql`
  mutation deleteMedia($id: ObjectID!) @api(name: "zim") {
    deleteMedia(id: $id)
  }
`;gql`
  mutation uploadImages($file: [Upload]) @api(name: "appZim") {
    uploadImages(file: $file)
  }
`;const SINGLE_UPLOAD=gql`
  mutation singleUpload($file: Upload!) @api(name: "chat") {
    singleUpload(file: $file) {
      id
      path
      fullUrl
      type
    }
  }
`,UPLOAD_EXAM_DOCUMENT=gql`
  mutation uploadExamDocument($file: Upload!, $folderName: String)
  @api(name: "chat") {
    uploadExamDocument(file: $file, folderName: $folderName) {
      id
      path
      fullUrl
      type
    }
  }
`,UPLOAD_S3=gql`
  mutation ($file: [Upload]!, $subfolder: SubfolderS3Enum!)
  @api(name: "appZim") {
    uploadS3(file: $file, subfolder: $subfolder)
  }
`,useUploadImage=({setMediaUrls:e})=>{const[r,{loading:n}]=useMutation(SINGLE_UPLOAD),a=react.exports.useCallback(async c=>{var d,m,b;const u=v4$1();try{await e.push({fileId:u,url:URL.createObjectURL(c),name:c.name,loading:!0,progress:0,type:((d=c==null?void 0:c.type)==null?void 0:d.includes("video"))?"video":((m=c==null?void 0:c.type)==null?void 0:m.includes("image"))?"image":"file"});const{data:A}=await r({variables:{file:c},context:{fetchOptions:{onUploadProgress:_=>{var T,C;e.update(S=>S.fileId===u,{fileId:u,url:URL.createObjectURL(c),name:c.name,loading:!0,progress:_.loaded/_.total,type:((T=c==null?void 0:c.type)==null?void 0:T.includes("video"))?"video":((C=c==null?void 0:c.type)==null?void 0:C.includes("image"))?"image":"file"})}}}});await e.update(_=>_.fileId===u,{fileId:A.singleUpload.id,name:A.singleUpload.path,url:URL.createObjectURL(c),type:c.type.includes("video")?"video":((b=c==null?void 0:c.type)==null?void 0:b.includes("image"))?"image":"file"})}catch(A){console.log(A)}},[r]);return{doUploadImage:react.exports.useCallback(async c=>{await Promise.all(c.map(a))},[a]),loading:n}};var useDeepCompareEffect$1={},useCustomCompareEffect$1={};Object.defineProperty(useCustomCompareEffect$1,"__esModule",{value:!0});var react_1$1=react.exports,useCustomCompareEffect=function(e,r,n){var a=react_1$1.useRef(void 0);(!a.current||!n(r,a.current))&&(a.current=r),react_1$1.useEffect(e,a.current)};useCustomCompareEffect$1.default=useCustomCompareEffect;var isDeepEqual={};Object.defineProperty(isDeepEqual,"__esModule",{value:!0});var tslib_1$1=require$$0,react_1=tslib_1$1.__importDefault(react$1);isDeepEqual.default=react_1.default;Object.defineProperty(useDeepCompareEffect$1,"__esModule",{value:!0});var tslib_1=require$$0,useCustomCompareEffect_1=tslib_1.__importDefault(useCustomCompareEffect$1),isDeepEqual_1=tslib_1.__importDefault(isDeepEqual),useDeepCompareEffect=function(e,r){useCustomCompareEffect_1.default(e,r,isDeepEqual_1.default)},_default=useDeepCompareEffect$1.default=useDeepCompareEffect;const EmojiPicker=loadable$2(()=>__vitePreload(()=>import("./index28.js").then(function(e){return e.i}),["assets/index28.js","assets/vendor.js"])),maxSize=1048576*2,EnterMessage=({conversationId:e,setConversationId:r,userId:n,group:a,textAreaRef:l,file:c,setFile:u,conversationDetail:d,isMyTyping:m})=>{var b,A,_,T;{const C=useToast(),[S]=useUser(),P=react.exports.useRef(null),k=useColorModeValue("gray.900","slate.300"),[x,E]=react.exports.useState(!1),[g,y]=react.exports.useState(!1),[I,M]=react.exports.useState(""),[j,O]=react.exports.useState(!1),{onSendMessage:R,onTyping:B}=useSendMessage({setContent:M,userId:n,group:a,setConversationId:r}),G=((A=(b=d==null?void 0:d.usersTyping)==null?void 0:b.filter(F=>F!==(S==null?void 0:S.id)))==null?void 0:A.length)>0,V=(T=(_=d==null?void 0:d.usersTyping)==null?void 0:_.filter(F=>F!==S.id))==null?void 0:T.map(F=>{var $;return($=d==null?void 0:d.participants.find(U=>U.userId===F))==null?void 0:$.fullName}),{doUploadImage:Q}=useUploadImage({setMediaUrls:u}),{getRootProps:W,getInputProps:q}=useDropzone({accept:["image/*","application/vnd.ms-excel","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet","application/vnd.openxmlformats-officedocument.presentationml.presentation","application/pdf","application/xhtml+xml","application/xml","application/msword","application/vnd.openxmlformats-officedocument.wordprocessingml.document","application/zip"],multiple:!1,onDrop:(F,$)=>{var K;const U=($==null?void 0:$.length)>0,z=$.length>0&&((K=$[0].errors)==null?void 0:K.find(Z=>Z.code==="file-too-large"));U?C({title:"Th\u1EA5t b\u1EA1i!",description:z?ERROR_FILE_SIZE:ERROR_FILE_NOT_SUPPORT,status:"error",duration:3e3,isClosable:!0}):Q(F).then()},minSize:0,maxSize}),L=react.exports.useCallback(async F=>{const $=F.target.value.trim();if(F.key==="Enter"&&!F.shiftKey)($.length!==0||(c==null?void 0:c.length)>0)&&(j&&O(!1),await R({nativeEvent:{text:$},conversationId:e}),O(!1),M(""));else return!1},[e,I,j,c,l]),D=()=>g&&jsx(Textarea,{_focus:{outline:"none"},onFocus:()=>{E(!0)},onBlur:()=>{E(!1)},as:ResizeTextarea,style:{resize:"none"},ref:l,minRows:1,maxRows:5,rows:1,value:I,onChange:async F=>{F.target.value!==`
`&&await M(F.target.value)},border:"none",placeholder:"Nh\u1EADp n\u1ED9i dung ...",onKeyPress:L,color:k,bg:"transparent",mr:4});return _default(()=>{g&&async function(){x&&await B("typingOn",e),x||await B("typingOff",e)}()},[x,e,B,g,l]),react.exports.useEffect(()=>{let F;return m&&(F=setTimeout(()=>{E(!1)},15e3)),()=>{clearTimeout(F)}},[m]),react.exports.useEffect(()=>{y(!0)},[]),react.exports.useEffect(()=>{const F=$=>{P.current&&!P.current.contains($.target)&&O(!1)};return document.addEventListener("click",F,!0),()=>{document.removeEventListener("click",F,!0)}},[O]),jsxs(Box,{bg:useColorModeValue("white",""),position:"sticky",bottom:0,w:"100%",display:"flex",flexDirection:"column",borderTopWidth:1,borderColor:"var(--chakra-colors-whiteAlpha-300)",p:2,children:[G&&jsxs(HStack,{w:"50%",px:2,rounded:4,mb:2,children:[jsx(Text,{fontSize:13,color:useColorModeValue("gray.700","white"),children:V==null?void 0:V.map(F=>F||"Someone is Typing").join(",")}),jsx(Box,{display:"flex",alignItems:"center",justifyContent:"center",w:10,h:5,children:jsx(Typing,{})})]}),jsxs(HStack,{w:"full",children:[jsxs(Box,H(N({as:"div"},W({className:"dropzone"})),{children:[jsx("input",H(N({},q()),{style:{display:"none"},type:"file"})),jsx(BsImages,{color:useColorModeValue("#DA0037","white"),className:"w-5 h-5"})]})),jsxs(Box,{w:"100%",display:"flex",flexDirection:"row",borderWidth:1,rounded:"lg",borderColor:useColorModeValue("gray.300","var(--chakra-colors-whiteAlpha-300)"),p:2,children:[D(),jsxs(HStack,{display:"flex",alignItems:"center",children:[jsx(Box,{as:"button",onClick:()=>O(!j),children:jsx(BsEmojiSmile,{color:useColorModeValue("#DA0037","white"),className:"w-5 h-5 "})}),jsx(Box,{cursor:"pointer",pointerEvents:!I&&(c==null?void 0:c.length)===0?"none":"auto",onClick:async()=>{I.trim().length!==0||(c==null?void 0:c.length)>0?(j&&O(!1),await R({nativeEvent:{text:I},conversationId:e})):M("")},children:jsx(MdSend,{fill:useColorModeValue((I==null?void 0:I.length)>0||(c==null?void 0:c.length)>0?"#DA0037":"#686868","white"),className:"w-5 h-5"})})]})]}),j&&jsx(Box,{ref:P,color:"slate.900",children:jsx(EmojiPicker,{disableSkinTonePicker:!0,pickerStyle:{position:"absolute",bottom:"70px",right:"20px",overflow:"hidden",backgroundColor:"white",zIndex:"1000"},onEmojiClick:(F,$)=>{M(`${I} ${$.emoji}`)}})})]})]})}},useMarkDoneConversation=e=>{const r=useToast(),[n,{loading:a}]=useMutation(MARK_DONE_CONVERSATION);return{onMarkDone:react.exports.useCallback(async()=>{try{await n({variables:{conversationId:e}}),await r({title:"Th\xE0nh c\xF4ng.",description:"\u0110\xE3 chuy\u1EC3n user sang tr\u1EA1ng th\xE1i done",status:"success",duration:2e3,isClosable:!0})}catch(c){r({title:"L\u1ED7i !!!",description:c.message,status:"success",duration:2e3,isClosable:!0})}},[e,n,r]),loading:a}},InputSearch=n=>{var a=n,{onChange:e}=a,r=Te(a,["onChange"]);const[l,c]=react.exports.useState(""),u=useDebounce$1(l,1e3);return react.exports.useEffect(()=>{e(u)},[u]),jsx(Input$1,H(N({},r),{value:l,onChange:d=>c(d.target.value)}))},ActionButton=e=>{const c=e,{Icon:r,onClick:n,disabled:a}=c,l=Te(c,["Icon","onClick","disabled"]);return jsx(Box,H(N({_hover:{backgroundColor:"#0085E4"},as:"button",display:"flex",alignItems:"center",justifyContent:"center",width:10,rounded:"4px",h:"100%",onClick:n,disabled:a},l),{children:jsx(r,{})}))},BoxMessageHeader=({userInfo:e,groupInfo:r,loading:n,participants:a})=>{var M;const[l]=useUser(),[c,u]=react.exports.useState(!1),d=useStore(j=>j.searchStringMessage),m=useStore(j=>j.setSearchStringMessage),b=useStore(j=>j.visibleUserProfile),A=useStore(j=>j.conversationId),_=useStore(j=>j.setSearchResult),T=useStore(j=>j.setNearSearchResult),C=useStore(j=>j.setVisibleUserProfile),S=useStore(j=>j.setBeforeId),P=useStore(j=>j.setAfterId),k=useStore(j=>j.setMessageId),x=useBreakpointValue({base:"",xl:"visible"}),E=useBreakpointValue({base:"",md:"visible"}),{onMarkDone:g,loading:y}=useMarkDoneConversation(A);useSearchMessage({conversationId:A,search:d});const I=react.exports.useMemo(()=>n?"\u0110ang t\u1EA3i ...":(r==null?void 0:r.name)?r==null?void 0:r.name:(e==null?void 0:e.full_name)||(e==null?void 0:e.fullName)||"\u1EA8n danh",[n,r==null?void 0:r.name,e==null?void 0:e.full_name,e==null?void 0:e.fullName]);return jsxs(Box,{w:"100%",minH:"35px",maxH:"50px",display:"flex",flexDirection:"row",alignItems:"center",borderBottomWidth:1,pl:2,py:3,zIndex:1,children:[!c&&jsxs(HStack,{id:"js-toggle-profile",flex:1,alignItems:"center",cursor:"pointer",onClick:()=>!x&&C(!b),children:[e&&lodash.exports.isEmpty(r)?jsx(MyAvatar,{boxSize:"2em",src:e==null?void 0:e.avatar,name:I}):jsx(GroupAvatar,{groupInfo:r==null?void 0:r.participants}),jsx(Box,{flex:1,maxW:"300px",children:jsx(Text,{noOfLines:1,fontSize:14,lineHeight:"24px",fontWeight:"medium",color:useColorModeValue("gray.800","white"),textTransform:"capitalize",children:I})})]}),jsxs(HStack,{w:c?"full":"",h:"100%",pr:c?2:4,spacing:0,minH:10,children:[[6].includes(parseInt(l==null?void 0:l.roleId,10))&&(a==null?void 0:a.find(j=>j.userId===l.id.toString()))&&!c&&((e==null?void 0:e.isGuest)||((M=r==null?void 0:r.participants)==null?void 0:M.find(j=>j.isGuest)))&&jsx(ActionButton,{onClick:g,disabled:y,Icon:MdDoneOutline}),!c&&E&&jsx(ActionButton,{onClick:()=>{u(!0)},Icon:FiSearch}),c&&jsxs(InputGroup,{children:[jsx(InputSearch,{type:"text",placeholder:"Nh\u1EADp g\xEC \u0111\xF3 ...",onChange:m,pr:"4.5rem"}),jsx(InputRightElement,{width:"4.5rem",children:jsx(Button$1,{h:"1.75rem",size:"sm",onClick:()=>{u(!1),m(""),_([]),T([]),k(""),S(""),P("")},children:c?"Close":"Open"})})]})]})]})},useGetConversationDetail=(e,r)=>{var u;const{data:n,loading:a,error:l}=useQuery(CONVERSATION_DETAIL,{variables:pickBy_1({id:e,userId:e?"":r?r==null?void 0:r.toString():""},d=>d),skip:!e&&!r});return{conversationDetail:(u=n==null?void 0:n.conversationDetail)!=null?u:{},loading:a,error:l}},useGetUserInfoGraphNet=(e="",r)=>{var c;const{data:n,loading:a}=useQuery(GRAPH_NET_GET_USER_INFO,{variables:{userId:Number(e)},skip:r||!e||isNaN(Number(e))});return{userData:(c=n==null?void 0:n.getUserById)!=null?c:{},loading:a}},usePermissionForGroup=({conversationDetail:e})=>{var n,a,l,c;const[r]=useUser();return!((n=e==null?void 0:e.participants)==null?void 0:n.find(u=>u.userId===(r==null?void 0:r.id)))&&((a=e==null?void 0:e.participants)==null?void 0:a.length)>=2||((l=e==null?void 0:e.participants)==null?void 0:l.find(u=>u.userId===(r==null?void 0:r.id)))&&((c=e==null?void 0:e.participants)==null?void 0:c.length)>=3},sm="_sm_183st_1",base="_base_183st_6",md="_md_183st_10";var s={sm,base,md};const buttonSizes={sm:s.sm,base:s.base,md:s.md},buttonTypes={primary:"bg-red-700 border-red-700 dark:bg-red-700 hover:border-red-900 hover:bg-red-900 dark:hover:bg-red-900 dark:border-red-500  hover:text-white ring-red-400 text-white ",info:"bg-blue-500 border-blue-500 hover:border-blue-700 hover:bg-blue-700 hover:text-white ring-blue-400 text-white",white:"bg-white border-transparent hover:border-red-700 hover:bg-red-700 hover:text-white ring-red-400 text-black",warning:"bg-yellow-500 hover:bg-yellow-700 border-yellow-500 hover:border-yellow-700 ring-yellow-400 text-black",success:"bg-green-500 hover:bg-green-700 ring-green-400 text-white border-green-500 hover:border-green-700","primary-outline":"border border-red-700 hover:bg-red-700 hover:text-white ring-red-400 text-red-700 dark:text-red-300 dark:border-red-300 dark:hover:bg-red-300 dark:hover:text-slate-300","info-outline":"border border-blue-700 hover:bg-blue-700 hover:text-white ring-blue-400 text-blue-700","success-outline":"border border-green-700 hover:bg-green-700 hover:text-white ring-green-400 text-green-700","warning-outline":"border border-yellow-700 text-yellow-700 hover:bg-yellow-700 hover:text-black ring-yellow-400","black-outline":"border border-black hover:bg-gray-50 text-black hover:text-black ring-gray-700 dark:border-slate-500 dark:text-slate-300 dark:hover:text-slate-900",gray:"bg-gray-200 hover:bg-gray-600 ring-gray-400 text-gray-900 hover:text-white dark:bg-slate-600 dark:text-slate-300 dark:hover:bg-slate-700 dark:border-slate-700",ghost:"hover:bg-gray-100 ring-gray-400 border-transparent hover:border-transparent dark:bg-slate-600 dark:text-slate-300 dark:hover:bg-slate-700"},isOverrideClasses=(e,r)=>e.match(r),Button=react.exports.forwardRef((m,d)=>{var b=m,{className:e="",buttonType:r=buttonTypes.primary,buttonSize:n=buttonSizes.base,isLoading:a=!1,isDisabled:l=!1,children:c}=b,u=Te(b,["className","buttonType","buttonSize","isLoading","isDisabled","children"]);const A=react.exports.useMemo(()=>buttonTypes[r],[r]),_=react.exports.useMemo(()=>buttonSizes[n],[n]),T=react.exports.useMemo(()=>a||l,[a,l]);return jsxs("button",H(N({disabled:l||a,ref:d,className:`whitespace-nowrap border inline-flex items-center justify-center transition-colors transition-background duration-300 ${isOverrideClasses(e,"p-")?"":"py-2 px-4"} rounded focus:outline-none ring-opacity-75  focus:ring ${isOverrideClasses(e,"text-")?"":"text-base"} font-medium ${T&&"opacity-60 cursor-not-allowed"} ${A} ${e} ${_}`},u),{children:[a&&jsxs("svg",{className:"animate-spin -ml-1 mr-3 h-5 w-5 text-current",xmlns:"http://www.w3.org/2000/svg",fill:"none",viewBox:"0 0 24 24",children:[jsx("circle",{className:"opacity-25",cx:"12",cy:"12",r:"10",stroke:"currentColor",strokeWidth:"4"}),jsx("path",{className:"opacity-100",fill:"currentColor",d:"M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"})]}),c]}))}),UploadFileContent=react.exports.memo(({showUploadFileDialog:e,file:r,setFile:n,setShowUploadFileDialog:a,onSendMessage:l})=>{const c=useResetQueries(),u=useStore(m=>m.conversationId),d=react.exports.useCallback(async()=>{await l({nativeEvent:{text:""},conversationId:u,file:r}),await c(["getAttachmentsInConversation"]),await n.clear()},[u,r]);return jsx(mt,{appear:!0,show:e,as:"div",enter:"transition-opacity duration-75",enterFrom:"opacity-0",enterTo:"opacity-100",leave:"transition-opacity duration-150",leaveFrom:"opacity-100",leaveTo:"opacity-0",children:jsx(Box,{p:2,zIndex:1e3,position:"absolute",bg:useColorModeValue("white","slate.600"),rounded:"lg",display:"flex",flexDirection:"column",justifyContent:"space-between",w:"50%",h:"40%",maxH:"300px",shadow:"md",className:"translate-y-[90%] translate-x-[50%] inset-0 ",children:r==null?void 0:r.map((m,b)=>jsxs(Flex,{flexDirection:"column",display:"flex",justifyContent:"space-between",w:"full",h:"100%",overflow:"hidden",rounded:"lg",children:[(m==null?void 0:m.type)!=="file"?jsx(Box,{maxH:"75%",children:jsx(Image$1,{w:"full",h:"full",src:m==null?void 0:m.url,alt:"file",objectFit:"contain"})}):jsx(Stack,{display:"flex",maxH:"75%",h:"full",alignItems:"center",justifyContent:"center",children:jsxs(VStack,{children:[jsx(Box,{w:14,h:14,rounded:"full",display:"flex",alignItems:"center",children:jsx(AiOutlineFileZip,{className:"w-[40px] h-[40px]"})}),jsx(Text,{textAlign:"center",flex:1,color:useColorModeValue("gray.800","white"),fontSize:14,children:getFilePathName(m==null?void 0:m.name)})]})}),jsx(Box,{onClick:()=>{n.removeAt(b),a(!1)},top:-2,right:-1,rounded:"full",cursor:"pointer",position:"absolute",zIndex:20,bg:useColorModeValue("white","#10172a"),children:jsx(XCircleIcon,{onClick:()=>{n.removeAt(b),a(!1)},width:32,className:"text-gray-400 hover:text-red-400"})}),jsxs(Stack,{display:"flex",flexDirection:"column",height:"25%",justifyContent:"flex-end",children:[jsx(Box,{h:"18%",w:"100%",children:jsx(Progress,{colorScheme:(m==null?void 0:m.progress)<1?"":"green",hasStripe:(m==null?void 0:m.progress)<1,mt:2,value:(m==null?void 0:m.progress)*100})}),jsx(Spacer,{}),jsx(Button,{disabled:(m==null?void 0:m.progress)<1,onClick:d,buttonType:"info",className:"w-full text-center py-2 rounded-lg text-white mt-2 max-h-[40px]",children:"G\u1EEDi"})]})]},b))})})}),getVisibleValue=(e=[],r)=>{if((e==null?void 0:e.length)<2)return!0;if((e==null?void 0:e.length)>=2)return!!(e==null?void 0:e.find(n=>n.userId===r))},BoxMessage=()=>{var g,y,I,M;const e=useStore(j=>j.channel),r=useStore(j=>j.userId),n=useStore(j=>j.conversationId),a=useStore(j=>j.setConversationId),l=useStore(j=>j.searchStringMessage),c=react.exports.useRef(null),[u]=useUser(),[d,m]=useList([]),[b,A]=react.exports.useState(!1),{userData:_,loading:T}=useGetUserInfoGraphNet(r,!r),{conversationDetail:C,loading:S}=useGetConversationDetail(n,r),P=((g=C==null?void 0:C.participants)==null?void 0:g.filter(j=>(j==null?void 0:j.userId)!==(u==null?void 0:u.id.toString()))[0])||_,k=usePermissionForGroup({conversationDetail:C}),{onSendMessage:x,loading:E}=useSendMessage({setContent:()=>{},userId:r,group:e,setConversationId:a});return react.exports.useEffect(()=>{(d==null?void 0:d.length)>0?A(!0):A(!1)},[d]),jsxs(Box,{bg:useColorModeValue("white","#10172a"),display:"flex",flexDirection:"column",flex:1,w:"full",position:"relative",h:"full",children:[(n||r)&&jsx(BoxMessageHeader,{participants:C==null?void 0:C.participants,userInfo:P,groupInfo:k&&n?{name:(C==null?void 0:C.name)?C==null?void 0:C.name:k&&n?(I=(y=C==null?void 0:C.participants)==null?void 0:y.map(j=>j.fullName))==null?void 0:I.join(","):"",participants:C==null?void 0:C.participants}:null,loading:T||S}),jsx(ChatContent,{isGroup:C==null?void 0:C.name,ownerId:u==null?void 0:u.id,conversationId:n,userId:r,onSendMessage:x,sendMessageLoading:E,participants:C==null?void 0:C.participants,setConversationId:a,channel:e}),jsx(UploadFileContent,{onSendMessage:x,file:d,setFile:m,showUploadFileDialog:b,setShowUploadFileDialog:A}),(getVisibleValue(C==null?void 0:C.participants,u==null?void 0:u.id)&&(n||r)||k&&ALLOW_EC_ROLE.includes(u==null?void 0:u.roleId))&&!l&&!S&&jsx(EnterMessage,{conversationDetail:C,file:d,setFile:m,textAreaRef:c,conversationId:n,setConversationId:a,userId:r,group:e,isMyTyping:(M=C==null?void 0:C.usersTyping)==null?void 0:M.includes(u==null?void 0:u.id)}),(d==null?void 0:d.length)>0&&jsx(Box,{opacity:.4,zIndex:10,position:"absolute",bg:useColorModeValue("black","slate.300"),className:"inset-0"})]})},AvatarComponent=({participantAvatarUrl:e,fullName:r="",onClose:n,loading:a})=>jsxs(Box,{flex:1,display:"flex",alignItems:"center",cursor:"pointer",onClick:()=>n?n():()=>{},children:[jsx(MyAvatar,{boxSize:"2em",src:e,name:r}),jsx(Text,{ml:2,flex:1,noOfLines:1,fontSize:14,lineHeight:"24px",fontWeight:"medium",color:useColorModeValue("gray.800","slate.300"),children:a?"\u0110ang t\u1EA3i ...":r||"\u1EA8n danh"})]}),REGEX_PASSWORD=/^(?=.*\d)(?=.*[a-zA-Z])[\da-zA-Z_.\-@]{8,}$/,REGEX_ONLY_NUMBER=/^\d+$/,REGEX_PHONE=/(84|03|05|07|08|09|01[2|6|8|9])+([0-9]{8})\b/,REGEX_LINK=/^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w.-]+)+[\w\-._~:/?#[\]@!$&'()*+,;=.]+$/gm,getFileExtension$1=e=>{if(typeof e!="string")return"";const r=e.split(".");return r.length>0?r[r.length-1]:""};addMethod(create,"password",function(e){return this.matches(REGEX_PASSWORD,{message:e,excludeEmptyString:!0})});addMethod(create,"onlyNumber",function(e){return this.matches(REGEX_ONLY_NUMBER,{message:e,excludeEmptyString:!0})});addMethod(create,"phone",function(e){return this.matches(REGEX_PHONE,{message:e,excludeEmptyString:!0})});addMethod(create$2,"fileSize",function(e,r){return this.test("fileSize",r,n=>Array.isArray(n)&&n.length>0&&n[0]?!n.map(l=>l.size>e):!0)});addMethod(create$2,"fileType",function(e=[],r){return this.test("fileType",r,n=>e.length===0?!0:Array.isArray(n)&&n.length>0&&n[0]?!n.map(l=>e.includes(l.type)):!0)});addMethod(create$2,"file",function({size:e,type:r},n){return this.test({name:"file",test:function(a){var u,d,m,b,A,_;const l=(u=r==null?void 0:r.value)!=null?u:[],c=(d=e==null?void 0:e.value)!=null?d:5*1024*1e3;if(Array.isArray(a)&&a.length>0&&a[0]){const T=a.find(S=>!l.includes(S.type)),C=a.find(S=>S.size>c);return T?this.createError({message:`${(m=r==null?void 0:r.message)!=null?m:"Kh\xF4ng h\u1ED7 tr\u1EE3 \u0111\u1ECBnh d\u1EA1ng"} ${getFileExtension$1((b=T==null?void 0:T.name)!=null?b:"")}`,path:this.path}):C?this.createError({message:(_=(A=e==null?void 0:e.message)!=null?A:`Dung l\u01B0\u1EE3ng v\u01B0\u1EE3t qu\xE1 ${c/1024e3}`+(C==null?void 0:C.name))!=null?_:"",path:this.path}):!0}else return!0}})});const schema$2=yup.object().shape({title:yup.string().required("Vui l\xF2ng nh\u1EADp ti\xEAu \u0111\u1EC1"),description:yup.string().min(10,"T\u1ED1i thi\u1EC3u 10 k\xFD t\u1EF1").required("Vui l\xF2ng nh\u1EADp m\xF4 t\u1EA3"),receiver:yup.mixed()}),TicketForm=({enableReceiver:e,users:r,onInputChange:n})=>{var u;const{register:a,formState:{errors:l},resetField:c}=useFormContext();return jsxs(Flex,{direction:"column",w:"full",justifyContent:"stretch",alignItems:"stretch",className:"reply-editor",gap:4,children:[jsx(TextInput,H(N({label:"Ti\xEAu \u0111\u1EC1",placeholder:"Nh\u1EADp..."},a("title")),{error:l==null?void 0:l.title})),e&&jsx(Controller,{name:"receiver",render:({field:d})=>jsx(Select$1,N({label:"Ng\u01B0\u1EDDi nh\u1EADn",options:r||[],getOptionLabel:m=>`${m.full_name} - ${m.phone}`,getOptionValue:m=>m.id+"",formatOptionLabel:m=>jsxs(HStack,{children:[jsx(Avatar,{src:m.avatar,objectFit:"cover",boxSize:6,flexShrink:0}),jsx(Text,{children:m.full_name})]}),error:l==null?void 0:l.receiver,onInputChange:n},d))}),jsx(Box,{children:jsxs(FormControl,{isInvalid:l==null?void 0:l.description,children:[jsx(Controller,{name:"description",render:({field:d})=>jsx(TipTap$1,N({showWordCount:!1,placeholder:"N\u1ED9i dung ticket..."},d))}),jsx(FormErrorMessage,{children:(u=l==null?void 0:l.description)==null?void 0:u.message})]})})]})},TicketFormModal=({onClose:e,isOpen:r,submitCallback:n=null})=>{const a=react.exports.useRef(),l=useForm({resolver:o$1(schema$2),mode:"onBlur",reValidateMode:"onChange"}),c=useToast();useRouter();const[u,d]=useSearchParams(),m=useResetQueries(),[b,A]=react.exports.useState(""),[_,T]=react.exports.useState(b);useDebounce$2(()=>{A(_)},500,[_]);const[C]=useUser(),S=C.roleId===6,[P,{loading:k}]=useMutation(CREATE_TICKET,{onCompleted:y=>{const{createTicket:I}=y;I&&c({title:"Th\xE0nh c\xF4ng!",description:"Ticket \u0111\xE3 \u0111\u01B0\u1EE3c g\u1EEDi.",status:"success",duration:3e3,isClosable:!0}),e(),n&&typeof n=="function"&&n(),m(["getTicketsPagination"])},onError:y=>{var I;c({title:"G\u1EEDi kh\xF4ng th\xE0nh c\xF4ng!",description:`${(I=y==null?void 0:y.message)!=null?I:"L\u1ED7i"}`,status:"error",duration:3e3,isClosable:!0})},notifyOnNetworkStatusChange:!0}),{data:x}=useQuery(GET_LIST_STUDENT_BY_EC_BY_FIELD(["avatar","full_name","phone"]),{variables:{input:{limit:100,q:b}},skip:!S}),E=x==null?void 0:x.getListStudentByEc.docs,g=react.exports.useCallback(async y=>{var M;if(S&&!(y==null?void 0:y.receiver)){l.setError("receiver",{type:"manual",message:"Vui l\xF2ng ch\u1ECDn ng\u01B0\u1EDDi nh\u1EADn"});return}const I={refId:EMPTY_GUID,title:y==null?void 0:y.title,description:y==null?void 0:y.description,receiverId:S?parseInt((M=y==null?void 0:y.receiver)==null?void 0:M.id):null};S||delete I.receiverId,await P({variables:{input:I}})},[S]);return react.exports.useEffect(()=>{if(r){const y=u.get("receiverId");if(!y||!(E==null?void 0:E.length))return;const I=E.find(M=>M.id===Number(y));l.reset({title:"",description:"",receiver:I||null})}else u.delete("receiverId"),d(u),l.reset({title:"",description:"",receiver:null})},[r,u,E]),jsx(FormProvider,H(N({},l),{children:jsxs(Modal$1,{finalFocusRef:a,isOpen:r,onClose:e,size:"3xl",children:[jsx(ModalOverlay,{}),jsxs(ModalContent,{children:[jsx(ModalHeader,{children:"T\u1EA1o ticket"}),jsx(ModalCloseButton,{}),jsxs("form",{onSubmit:l.handleSubmit(g),children:[jsx(ModalBody,{children:jsx(TicketForm,{enableReceiver:S,users:E,onInputChange:T})}),jsxs(ModalFooter,{children:[jsx(Button$1,{leftIcon:jsx(Icon,{as:AiOutlineSend}),colorScheme:"green",mr:4,type:"submit",isLoading:k,children:"G\u1EEDi ticket"}),jsx(Button$1,{variant:"ghost",onClick:e,children:"H\u1EE7y"})]})]})]})]})}))};function TagColor({color:e,onClick:r,isActive:n}){return jsx(Box,{borderWidth:n?3:1,borderColor:n?"white":"",as:"button",onClick:r,rounded:"sm",w:4,h:4,bg:`${e}`})}function TagForm({firstFieldRef:e,onClose:r}){var x,E;const n=useToast(),[a,l]=react.exports.useState("1"),[c,u]=react.exports.useState(null),d=useStore(g=>g.conversationId),[m,b]=react.exports.useState("#00DAA3"),[A,_]=react.exports.useState(""),{data:T}=useQuery(GET_TAGS,{skip:a==="2"}),[C,{loading:S}]=useMutation(CREATE_TAG_FOR_CONVERSATION),P=react.exports.useCallback(()=>{b("#00DAA3"),_(""),u(null)},[]),k=react.exports.useCallback(async()=>{var I;const g={conversationId:d,tagId:c==null?void 0:c.id},y={conversationId:d,name:A,color:m};try{await C({variables:(c==null?void 0:c.id)?g:y}),r(),P(),n({title:"Th\xE0nh c\xF4ng!",description:"G\u1EAFn tag th\xE0nh c\xF4ng",status:"success",duration:3e3,isClosable:!0})}catch(M){n({title:"G\u1EAFn tag kh\xF4ng th\xE0nh c\xF4ng!",description:`${(I=M==null?void 0:M.message)!=null?I:"L\u1ED7i"}`,status:"error",duration:3e3,isClosable:!0})}},[A,m,d,c,P]);return jsxs(Stack,{spacing:4,children:[jsx(RadioGroup,{defaultValue:"1",children:jsxs(HStack,{spacing:5,align:"center",children:[jsx(Radio,{isChecked:a==="1",colorScheme:"red",value:"1",onChange:()=>{l("1"),_(""),b("#00DAA3")},children:"Tag c\xF3 s\u1EB5n"}),jsx(Radio,{isChecked:a==="2",colorScheme:"green",value:"2",onChange:()=>{l("2"),u(null)},children:"Th\xEAm tag m\u1EDBi"})]})}),a==="1"&&jsx(Select$1,{value:c,label:"Tags",onChange:u,options:(E=(x=T==null?void 0:T.getListTag)==null?void 0:x.docs)!=null?E:[],getOptionLabel:g=>g.name,getOptionValue:g=>g.id+"",formatOptionLabel:g=>jsxs(HStack,{children:[jsx(Box,{bg:g.color,w:"5px",h:"5px",rounded:"full",mr:1}),jsx(Text,{children:g.name})]})}),a==="2"&&jsxs(Stack,{spacing:4,children:[jsx(RadioGroup,{onChange:b,value:m,children:jsx(HStack,{spacing:3,children:tagColor.map((g,y)=>{const I=g.color===m;return jsx(TagColor,{color:g.color,onClick:()=>b(g.color),isActive:I},g.color)})})}),jsx(TextInput,{label:"Tag name",id:"tag-name",ref:e,placeholder:"Nh\u1EADp t\xEAn tag",value:A,onChange:g=>_(g.target.value)})]}),jsxs(ButtonGroup,{d:"flex",justifyContent:"flex-end",children:[jsx(Button$1,{onClick:()=>{r(),P()},variant:"outline",children:"Cancel"}),jsx(Button$1,{onClick:k,disabled:!A&&!c||S,colorScheme:"teal",children:"Save"})]})]})}const RowInfo=({item:e})=>jsxs(HStack,{children:[jsx(Box,{w:6,h:6,p:1,rounded:"full",bg:useColorModeValue("gray.100","#1e293b"),overflow:"hidden",display:"flex",alignItems:"center",justifyContent:"center",children:e.icon}),typeof e.value=="function"?e==null?void 0:e.value():jsx(Text,{flex:1,fontSize:14,color:useColorModeValue("gray.800","slate.300"),noOfLines:2,children:e.value||"Kh\xF4ng c\xF3 th\xF4ng tin"})]}),ParticipantProfile=react.exports.memo(({participantInfo:e,participantAvatarUrl:r,fullName:n,loading:a,isStudent:l,tags:c,participants:u})=>{const{isOpen:d,onClose:m,onOpen:b}=useDisclosure(),A=t.useRef(null),[_]=useUser(),T=zimRolesNumber.map(E=>E.id),C=useStore(E=>E.setVisibleUserProfile),S=useStore(E=>E.conversationId),P=useParticipantDetail(e,c),k=useBreakpointValue({sm:"visible",xl:""}),x=react.exports.useCallback(()=>{C(!1)},[]);return!l&&jsxs(VStack,{alignItems:"stretch",spacing:2.5,px:4,py:2,borderBottomWidth:1,children:[jsxs(Stack,{spacing:2.5,children:[jsxs(HStack,{justifyContent:"space-between",children:[jsx(AvatarComponent,{onClose:x,participantAvatarUrl:r,fullName:n,loading:a}),k&&jsx(Box,{onClick:x,display:"flex",cursor:"pointer",w:6,h:6,alignItems:"center",justifyContent:"center",children:jsx(CgCloseR,{className:"w-full h-full"})})]}),P==null?void 0:P.filter(E=>E.visible).map((E,g)=>jsx(RowInfo,{item:E},`${E.value}-${g}`))]}),!ALLOW_STUDENT_ROLE.includes(_.roleId)&&S&&(u==null?void 0:u.find(E=>E.userId===(_==null?void 0:_.id.toString())))&&jsxs(HStack,{spacing:10,children:[jsx(Popover,{children:({onClose:E})=>jsxs(Fragment,{children:[jsx(PopoverTrigger,{children:jsx(Button$1,{bg:"transparent",_hover:{backgroundColor:"transparent"},_focus:{backgroundColor:"transparent"},_active:{backgroundColor:"transparent"},children:T.includes(_==null?void 0:_.roleId)&&jsx(AddButton,{iconContainerClassName:"bg-blue-500",label:"G\u1EAFn tag"})})}),jsx(Portal,{children:jsxs(PopoverContent,{zIndex:100,children:[jsx(PopoverArrow,{}),jsx(PopoverHeader,{children:"G\u1EAFn tag"}),jsxs(PopoverBody,{children:[jsx(PopoverArrow,{}),jsx(TagForm,{firstFieldRef:A,onClose:E})]})]})})]})}),(e==null?void 0:e.role_name)==="H\u1ECDc vi\xEAn"&&T.includes(_==null?void 0:_.roleId)&&S&&jsx(AddButton,{onClick:b,label:"T\u1EA1o ticket",iconContainerClassName:"bg-red-500"})]}),jsx(TicketFormModal,{onClose:m,isOpen:d})]})}),AddButton=({label:e="T\u1EA1o ticket",onClick:r,iconContainerClassName:n})=>jsxs(HStack,{onClick:r,display:"flex",alignItems:"center",cursor:"pointer",children:[jsx(Box,{className:n,w:5,h:5,rounded:"full",display:"flex",alignItems:"center",justifyContent:"center",children:jsx(IoMdAddCircleOutline,{color:"white"})}),jsx(Text,{fontWeight:"400",color:useColorModeValue("gray.800","slate.300"),children:e})]}),NoteItem=({createdAt:e,content:r,owner:n})=>{var a;return jsxs(HStack,{display:"flex",alignItems:"start",children:[jsx(MyAvatar,{size:"sm",src:n==null?void 0:n.avatar,name:n==null?void 0:n.fullName}),jsxs(VStack,{flex:1,alignItems:"stretch",spacing:1,children:[jsxs(HStack,{spacing:3,justifyContent:{base:"start","2xl":"space-between"},children:[jsx(Text,{noOfLines:1,fontSize:14,lineHeight:"24px",fontWeight:"medium",color:useColorModeValue("gray.800","slate.400"),maxW:{base:"90px","2xl":"120px"},children:(a=n==null?void 0:n.fullName)!=null?a:"Kh\xF4ng c\xF3"}),jsx(Text,{fontSize:12,lineHeight:"24px",className:"text-gray-400",children:dayjs(e).format("DD-MM-YYYY, HH:mm")})]}),jsx(Text,{fontSize:12,lineHeight:"16px",color:useColorModeValue("gray.800","slate.200"),children:r})]})]})},NoteInput=react.exports.memo(()=>{const e=react.exports.useRef(null),r=useStore(_=>_.conversationId),n=useResetQueries(),[a,l]=react.exports.useState(!1),[c,u]=react.exports.useState(""),[d]=useMutation(CREATE_NOTE),m=react.exports.useCallback(async()=>{try{await d({variables:{conversationId:r,content:c}}),await n(["notes"])}catch(_){console.log(_)}},[r,c]),b=react.exports.useCallback(async()=>{c.trim().length!==0&&(await m(),u(""))},[c]),A=react.exports.useCallback(async _=>{const T=_.target.value.trim();if(_.key==="Enter"&&!_.shiftKey)T.length!==0&&(m().then(),u(""));else return!1},[m]);return react.exports.useEffect(()=>{l(!0)},[]),jsxs(HStack,{w:"100%",borderWidth:1,rounded:"lg",borderColor:useColorModeValue("gray.300","whiteAlpha.300"),p:1,children:[a&&jsx(Textarea,{_focus:{outline:"none"},as:ResizeTextarea,style:{resize:"none"},ref:e,minRows:1,maxRows:5,rows:1,value:c,onChange:_=>{_.target.value!==`
`&&u(_.target.value)},border:"none",placeholder:"Nh\u1EADp n\u1ED9i dung ghi ch\xFA ...",onKeyPress:A,color:useColorModeValue("gray.900","slate.300"),bg:"transparent"}),jsx(Box,{cursor:"pointer",w:5,h:5,pointerEvents:c?"auto":"none",onClick:b,children:jsx(MdSend,{fill:useColorModeValue((c==null?void 0:c.length)>0?"#DA0037":"#686868","white")})})]})}),useGetNotes=({conversationId:e,userId:r})=>{var A,_,T;const{data:n,loading:a,fetchMore:l}=useQuery(GET_NOTES,{variables:pickBy_1({conversationId:e||"",userId:e?"":r?r==null?void 0:r.toString():"",limit:5},C=>C),skip:!e}),c=(A=n==null?void 0:n.notes)==null?void 0:A.docs,u=(_=n==null?void 0:n.notes)==null?void 0:_.page,d=(T=n==null?void 0:n.notes)==null?void 0:T.hasNextPage,{onLoadMore:m,isLoadingMore:b}=useLoadMore({variables:{conversationId:e,limit:5,page:u+1},fetchMore:l,hasNextPage:d});return react.exports.useMemo(()=>({notes:c,loading:a,onLoadMore:m,isLoadingMore:b,hasNextPage:d}),[c,a,m,b,d])};var perfectScrollbar="";const Notes=react.exports.memo(({isSuperAdmin:e,allowForSupperAdmin:r})=>{const n=useStore(_=>_.conversationId),a=useStore(_=>_.userId),l=react.exports.useRef(null),{notes:c,onLoadMore:u,hasNextPage:d,isLoadingMore:m}=useGetNotes({conversationId:n,userId:a}),b=react.exports.useCallback(async()=>{await u()},[u]),A=react.exports.useMemo(()=>c==null?void 0:c.map(_=>jsx(NoteItem,N({},_),_.id)),[c]);return react.exports.useEffect(()=>{let _;return(l==null?void 0:l.current)&&(_=new PerfectScrollbar(l==null?void 0:l.current,{wheelSpeed:1,minScrollbarLength:10})),()=>{_&&_.destroy()}},[]),jsxs(VStack,{alignItems:"stretch",spacing:2.5,px:4,py:2,children:[jsxs(Text,{fontSize:14,color:useColorModeValue("gray.800","slate.300"),children:["Notes ",c==null?void 0:c.length]}),jsx(VStack,{alignItems:"stretch",spacing:2.5,maxH:"200px",ref:l,className:"scroll-container",overflow:"hidden",position:"relative",minH:0,children:A}),d&&jsx(Button$1,{h:"32px",isLoading:m,loadingText:"\u0110ang t\u1EA3i th\xEAm",onClick:b,w:"100%",rounded:"none",borderWidth:"1px 0px 0px 0px",bg:useColorModeValue("white","slate.600"),fontSize:14,fontWeight:400,justifyContent:"center",children:"Xem th\xEAm"}),(!e||r)&&jsx(NoteInput,{})]})}),useGetAttachments=({conversationId:e,userId:r})=>{var T,C,S;const[n,a]=react.exports.useState(!1),l=H(N({},pickBy_1({conversationId:e||"",userId:e?"":r?r==null?void 0:r.toString():""},P=>P)),{offset:0}),{data:c,loading:u,fetchMore:d}=useQuery(GET_ATTACHMENTS,{variables:l}),m=(C=(T=c==null?void 0:c.getAttachmentsInConversation)==null?void 0:T.docs)==null?void 0:C.map(P=>P.attachments),b=[].concat.apply([],m),A=(S=c==null?void 0:c.getAttachmentsInConversation)==null?void 0:S.hasNextPage,_=react.exports.useCallback(async()=>{var P;!A||n||(await a(!0),await d({variables:H(N({},l),{offset:((P=c==null?void 0:c.getAttachmentsInConversation)==null?void 0:P.docs.length)+1}),updateQuery:(k,{fetchMoreResult:x})=>x?mergeDeep(k,x):k}),await a(!1))},[c,d,n,e]);return{attachments:b,loading:u,onLoadMore:_,isLoadingMore:n,hasNextPage:A}};function getFileExtension(e){let r=/^.+\.([^.]+)$/.exec(e);return r==null?"":r[1]}const MediaItem=react.exports.memo(({item:e,setAttachmentIndex:r,setShowImageViewer:n,index:a})=>{var d,m,b,A;const l=react.exports.useCallback(()=>{r(a),n(!0)},[a]),c=getFileExtension((d=e==null?void 0:e.attachment)==null?void 0:d.fullUrl),u=react.exports.useMemo(()=>{switch(c){case"xls":return RiFileExcel2Line;case"doc":return FaRegFileWord;default:return AiOutlineFileText}},[c]);return jsxs(Box,{minH:"80px",children:[(e==null?void 0:e.type)==="image"&&jsx(Image$1,{rounded:"md",onClick:l,cursor:"pointer",flex:1,objectFit:"contain",src:(m=e==null?void 0:e.attachment)==null?void 0:m.fullUrl,alt:"",h:"full"}),(e==null?void 0:e.type)==="file"&&jsx(Link$1,{target:"_blank",href:(b=e==null?void 0:e.attachment)==null?void 0:b.fullUrl,children:jsxs(Box,{"data-tip":"file-tooltip",flex:1,display:"flex",alignItems:"center",justifyContent:"center",bg:useColorModeValue("blue.50","slate.400"),rounded:"md",h:"full",children:[jsx(u,{className:"w-5 h-5 "}),jsx(ReactTooltip,{children:jsx("p",{children:getFilePathName((A=e==null?void 0:e.attachment)==null?void 0:A.path)}),place:"top",type:useColorModeValue("info","success"),effect:"float"})]})})]})}),Attachments=react.exports.memo(()=>{const e=useStore(_=>_.conversationId),r=useStore(_=>_.userId),[n,a]=react.exports.useState(null),[l,c]=react.exports.useState(!1),{attachments:u,isLoadingMore:d,onLoadMore:m,hasNextPage:b}=useGetAttachments({conversationId:e,userId:r}),A=react.exports.useCallback(async()=>{await m()},[m]);return jsxs(VStack,{alignItems:"stretch",spacing:2.5,py:2,children:[jsx(Accordion,{allowMultiple:!0,children:jsxs(AccordionItem,{children:[jsxs(HStack,{position:"relative",spacing:0,pl:4,_hover:{backgroundColor:useColorModeValue("#f0f0f0","slate.600")},children:[jsx(Box,{display:"flex",justifyContent:"start",flex:1,children:jsx(Text,{color:useColorModeValue("#444","white"),fontSize:16,fontWeight:"medium",children:"T\xE0i li\u1EC7u"})}),jsx(AccordionButton,{display:"flex",alignItems:"center",justifyContent:"center",w:10,children:jsx(AccordionIcon,{})})]}),jsxs(AccordionPanel,{pb:4,children:[(u==null?void 0:u.length)===0&&jsx(Text,{textAlign:"center",children:"Cu\u1ED9c tr\xF2 chuy\u1EC7n ch\u01B0a c\xF3 t\xE0i li\u1EC7u !!!"}),jsx(SimpleGrid,{columns:3,spacing:2,children:u==null?void 0:u.map((_,T)=>jsx(MediaItem,{item:_,setAttachmentIndex:a,setShowImageViewer:c,index:T}))}),b&&jsx(Button$1,{h:"32px",isLoading:d,loadingText:"\u0110ang t\u1EA3i th\xEAm",onClick:A,w:"100%",rounded:"none",borderWidth:"1px 0px 0px 0px",bg:useColorModeValue("white","slate.600"),fontSize:14,fontWeight:400,justifyContent:"center",mt:2,children:"Xem th\xEAm"})]})]})}),jsx(ImageViewer,{attachments:u,attachmentIndex:n,setAttachmentIndex:a,showImageViewer:l,setShowImageViewer:c})]})}),ParticipantInfoModal=({isOpen:e,onClose:r,userInfo:n,isOwner:a,groupId:l,isSupperAdmin:c,visibleRemoveMemberButton:u})=>{var P;const d=useResetQueries(),{isOpen:m,onOpen:b,onClose:A}=useDisclosure(),_=t.useRef(),[T,{loading:C}]=useMutation(REMOVE_MEMBER),S=react.exports.useCallback(async()=>{var k;try{await T({variables:{userId:(k=n==null?void 0:n.userId)==null?void 0:k.toString(),id:l}}),d(["conversationDetail"]),A(),r()}catch(x){console.log(x)}},[n,l]);return jsxs(Fragment,{children:[jsxs(Modal$1,{isOpen:e,onClose:r,children:[jsx(ModalOverlay,{}),jsxs(ModalContent,{children:[jsx(ModalHeader,{textAlign:"center",children:"Th\xF4ng tin th\xE0nh vi\xEAn"}),jsx(ModalCloseButton,{}),jsxs(ModalBody,{pb:4,children:[jsx(VStack,{p:4,pt:0,children:jsx(MyAvatar,{size:"2xl",src:n.avatar,name:n.fullName})}),jsx(VStack,{alignItems:"stretch",children:jsxs(HStack,{justifyContent:a||c?"space-between":"center",alignItems:"center",children:[jsxs(Text,{textAlign:a||c?"left":"center",fontSize:"xl",children:[n.fullName," ",`(${(P=ROLES.find(k=>k.id===Number(n==null?void 0:n.roleId)))==null?void 0:P.name})`]}),u&&jsx(Button$1,{onClick:b,size:"sm",colorScheme:"red",variant:"solid",fontSize:13,textColor:"white",children:"M\u1EDDi r\u1EDDi nh\xF3m"})]})})]})]})]}),jsx(AlertDialog,{isOpen:m,leastDestructiveRef:_,onClose:A,children:jsx(ModalOverlay,{children:jsxs(AlertDialogContent,{children:[jsx(ModalHeader,{fontSize:"lg",fontWeight:"bold",children:"C\u1EA3nh b\xE1o !!!"}),jsx(ModalBody,{children:"B\u1EA1n c\xF3 ch\u1EAFc kh\xF4ng? B\u1EA1n kh\xF4ng th\u1EC3 ho\xE0n t\xE1c h\xE0nh \u0111\u1ED9ng n\xE0y sau khi x\xF3a."}),jsxs(ModalFooter,{children:[jsx(Button$1,{disabled:C,ref:_,onClick:A,children:"Tr\u1EDF l\u1EA1i"}),jsx(Button$1,{disabled:C,colorScheme:"red",onClick:S,ml:3,color:"white",loadingText:"\u0110ang x\u1EED l\xFD",children:"X\xE1c nh\u1EADn"})]})]})})})]})},GroupInfo=react.exports.memo(({conversationDetail:e})=>{var C,S,P,k;const r=react.exports.useRef(null),n=useStore(x=>x.conversationId),[a]=useUser(),{isOpen:l,onClose:c,onOpen:u}=useDisclosure(),{isOpen:d,onClose:m,onOpen:b}=useDisclosure(),[A,_]=react.exports.useState(null),T=react.exports.useMemo(()=>{var x;return(x=e==null?void 0:e.participants)==null?void 0:x.map(E=>{var y,I;const g=((y=e==null?void 0:e.owner)==null?void 0:y.userId)===(E==null?void 0:E.userId);return jsx(UserItemInfo,H(N({},E),{isOwner:g,lastOnline:E==null?void 0:E.online,avatarUrl:E==null?void 0:E.avatar,username:E==null?void 0:E.fullName,positionName:(I=ROLES.find(M=>M.id===Number(E==null?void 0:E.roleId)))==null?void 0:I.name,onClick:async()=>{await _(E),await b()},px:0}),E.userId)})},[e]);return react.exports.useEffect(()=>{let x;return(r==null?void 0:r.current)&&(x=new PerfectScrollbar(r==null?void 0:r.current,{wheelSpeed:1,minScrollbarLength:10})),()=>{x&&x.destroy()}},[]),jsxs(VStack,{alignItems:"stretch",spacing:2.5,px:4,py:2,borderBottomWidth:1,children:[jsxs(HStack,{justifyContent:"space-between",alignItems:"center",children:[jsx(Text,{fontSize:14,color:useColorModeValue("gray.800","slate.300"),children:"Th\xE0nh vi\xEAn"}),((C=e==null?void 0:e.owner)==null?void 0:C.userId)===(a==null?void 0:a.id)&&jsx(Box,{onClick:u,as:"button",w:6,h:6,display:"flex",alignItems:"center",justifyContent:"center",children:jsx(MdGroupAdd,{className:"w-full h-full"})})]}),jsx(VStack,{alignItems:"stretch",spacing:0,maxH:"400px",ref:r,className:"scroll-container",overflow:"hidden",position:"relative",minH:0,children:T}),jsx(CreateConversationModal,{isChannel:!1,isAddSupporter:!1,isEdit:!0,isOpen:l,onClose:c,listParticipants:(S=e==null?void 0:e.participants)==null?void 0:S.map(x=>x==null?void 0:x.userId),name:e==null?void 0:e.name,groupId:n}),jsx(ParticipantInfoModal,{visibleRemoveMemberButton:a.id===((P=e==null?void 0:e.owner)==null?void 0:P.userId),isSupperAdmin:(a==null?void 0:a.roleId)===1,groupId:e==null?void 0:e.id,isOwner:((k=e==null?void 0:e.owner)==null?void 0:k.userId)===(A==null?void 0:A.userId),userInfo:A!=null?A:{},isOpen:d&&A,onClose:m})]})}),statusColorConfig={ongoing:{backgroundColor:"green.500"},incoming:{backgroundColor:"orange.400"},finished:{backgroundColor:"gray.300"}},CourseItem=({status:e,title:r})=>jsxs(HStack,{children:[jsx(Box,{w:2.5,h:2.5,rounded:"full",bg:statusColorConfig==null?void 0:statusColorConfig[e].backgroundColor}),jsx(Text,{flex:1,lineHeight:"20px",fontSize:14,color:useColorModeValue("#444","slate.300"),children:r})]}),Courses=react.exports.memo(({data:e})=>{const r=react.exports.useRef(null);react.exports.useEffect(()=>{let a;return(r==null?void 0:r.current)&&(a=new PerfectScrollbar(r==null?void 0:r.current,{wheelSpeed:1,minScrollbarLength:10})),()=>{a&&a.destroy()}},[]);const n=react.exports.useMemo(()=>e.map(a=>{var u;const{status:l,name:c}=(u=a==null?void 0:a.course)!=null?u:{};return react.exports.createElement(CourseItem,H(N({},a),{key:a==null?void 0:a.id,status:l,title:c}))}),[e]);return jsxs(VStack,{alignItems:"stretch",spacing:2.5,px:4,py:2,borderBottomWidth:1,maxH:"200px",ref:r,className:"scroll-container",overflow:"hidden",position:"relative",minH:0,children:[jsxs(Text,{fontSize:14,color:useColorModeValue("gray.800","slate.300"),children:["Kh\xF3a h\u1ECDc ",e.length]}),n]})}),LEAD_PERMISSION={create:[6],changeEC:[12,1,2],makeApointment:[6]},MOCK_TEST_PERMISSION={additionalTeacherOrCancelForStudent:[1,2],viewTableMockTestDetail:[1,2,6],canCreateOrEditMockTest:[1,2],assignTeacherAndEC:[1,2]},AutoResizeTextarea=t.forwardRef((e,r)=>{var n;return jsxs(FormControl,{isInvalid:!!(e==null?void 0:e.error),width:"100%",children:[(e==null?void 0:e.label)&&jsx(FormLabel,{htmlFor:e.id,children:e==null?void 0:e.label}),jsx(Textarea,N({minH:"unset",overflow:"hidden",w:"100%",resize:"none",ref:r,minRows:1,as:ResizeTextarea,transition:"height none",bg:useColorModeValue("white","slate.700")},e)),(e==null?void 0:e.error)&&jsx(FormErrorMessage,{children:(n=e==null?void 0:e.error)==null?void 0:n.message})]})}),useListSources=(e={},r={})=>{const n=useToast();return useQuery(GET_SOURCES,N({variables:e,onError:a=>{n({title:"L\u1EA5y d\u1EEF li\u1EC7u th\u1EA5t b\u1EA1i.",description:`GET list source status error: ${a==null?void 0:a.message}`,status:"error",duration:3e3,isClosable:!0})},fetchPolicy:"cache-first",errorPolicy:"all"},r))},CREATE_LEAD_NOTE=gql`
  mutation saveNote($rawId: Int, $note: String) @api(name: "appZim") {
    saveNote(rawId: $rawId, note: $note) {
      note
      createBy
      createDate
    }
  }
`,UPDATE_LEAD_STATUS=gql`
  mutation updateStatusLeads($input: updateStatusLeadsInputType)
  @api(name: "appZim") {
    updateStatusLeads(input: $input) {
      success
      error
      message
      dataString
    }
  }
`,CREATE_APOINTMENT=gql`
  mutation makeApointment($input: MakeApointmentInputType)
  @api(name: "appZim") {
    makeApointment(input: $input) {
      success
      error
      message
      dataString
    }
  }
`,UPDATE_LEAD_EC=gql`
  mutation changeEcLeads($input: ChangeEcLeadsInputType) @api(name: "appZim") {
    changeEcLeads(input: $input) {
      success
      error
      message
      dataString
    }
  }
`,CREATE_LEAD=gql`
  mutation addCustomerLeads($input: AddCustomerLeadInputType)
  @api(name: "appZim") {
    addCustomerLeads(input: $input) {
      success
      error
      message
      dataString
    }
  }
`,GET_LEAD_REPORT_DATA=gql`
  query getReportLeads($startDate: String, $endDate: String)
  @api(name: "appZim") {
    getReportLeads(startDate: $startDate, endDate: $endDate) {
      date
      totalView
      dataLeads {
        date
        sourseName
        data
      }
    }
  }
`,GET_LEADS=gql`
  query getRawDataPagination(
    $q: String
    $status: RawDataStatusEnum
    $fromDate: DateTime
    $toDate: DateTime
    $orderBy: String
    $order: String
    $page: Int
    $limit: Int
    $ecId: Int
    $schoolId: Int
    $tags: [String]
    $date: DateTime
    $sourceName: String
    $isOnlyDataFacebook: Boolean
  ) @api(name: "appZim") {
    getRawDataPagination(
      q: $q
      status: $status
      fromDate: $fromDate
      toDate: $toDate
      orderBy: $orderBy
      order: $order
      page: $page
      limit: $limit
      ecId: $ecId
      schoolId: $schoolId
      tags: $tags
      date: $date
      sourceName: $sourceName
      isOnlyDataFacebook: $isOnlyDataFacebook
    ) {
      hasNextPage
      hasPrevPage
      limit
      page
      pageNext
      pagePrev
      pagingCounter
      totalPages
      totalDocs
      docs {
        id
        student {
          id
          user_name
          full_name
          phone
          address
          email
          role_id
          role_name
          supporter
          student_more_info {
            identity_card_city_id
            note_home
            birthday
            city_id
            city_name
            district_id
            district_name
            ward_id
            ward_name
            street_id
            street_name
            home_number
            source_id
            source_name
            job_id
            job_name
            identity_card
            identity_card_city_name
            identity_card_date
            learning_status_id
            work_place
            learning_status_name
          }
        }
        fb_user_id
        source_id
        ec_name
        ec_id
        source_name
        form_name
        link
        note
        created_date
        appoinment_test_id
        status
        tags {
          level_name
          name
        }
      }
    }
  }
`,GET_LEAD_NOTE=gql`
  query getRawDataNote($rawId: Int) @api(name: "appZim") {
    getRawDataNote(rawId: $rawId) {
      note
      createBy
      createDate
    }
  }
`,GET_FB_COMMENT=gql`
  query facebookCommentsPagination(
    $page: Int
    $limit: Int
    $fbProfileId: String
    $postId: ObjectID
    $search: String
  ) @api(name: "crawler") {
    facebookCommentsPagination(
      page: $page
      limit: $limit
      search: $search
      postId: $postId
      fbProfileId: $fbProfileId
    ) {
      totalDocs
      totalPages
      docs {
        id
        post {
          id
          link
        }
        content
        facebookId
        createdAt
        updatedAt
        owner
        linkProfile
        inDemand
        fbProfileId
      }
    }
  }
`,GET_FB_POST=gql`
  query facebookPostsPagination(
    $page: Int
    $limit: Int
    $fbProfileId: String
    $search: String
  ) @api(name: "crawler") {
    facebookPostsPagination(
      page: $page
      limit: $limit
      search: $search
      fbProfileId: $fbProfileId
    ) {
      totalDocs
      totalPages
      docs {
        id
        content
        facebookId
        numOfComments
        numOfLikes
        numOfShares
        numOfCmtInDemand
        link
        featured
        createdAt
        updatedAt
        inDemand
        postOwnerId
        postOwnerName
      }
    }
  }
`,validateExist=async(e,r)=>{var n,a,l,c,u;if(e==="")return!1;try{const d=await apolloClient.query({query:GET_LEADS,variables:{limit:1,q:e,page:1}});return!(!((a=(n=d.data)==null?void 0:n.getRawDataPagination)==null?void 0:a.docs)||((u=(c=(l=d.data)==null?void 0:l.getRawDataPagination)==null?void 0:c.docs)==null?void 0:u.length)>0)}catch{return!1}},schema$1=yup.object().shape({phone:yup.string().matches(REGEX_PHONE,"S\u1ED1 \u0111i\u1EC7n tho\u1EA1i kh\xF4ng h\u1EE3p l\u1EC7").required("S\u1ED1 \u0111i\u1EC7n tho\u1EA1i l\xE0 b\u1EAFt bu\u1ED9c").test("phone","\u0110\xE3 c\xF3 EC ch\u0103m s\xF3c S\u0110T n\xE0y. Vui l\xF2ng ki\u1EC3m tra l\u1EA1i trong lead",validateExist),name:yup.string().required("Vui l\xF2ng nh\u1EADp h\u1ECD t\xEAn"),email:yup.string().email("\u0110\u1ECBa ch\u1EC9 email kh\xF4ng h\u1EE3p l\u1EC7").required("Vui l\xF2ng nh\u1EADp email"),source:yup.mixed().nullable(!0).required("M\u1EE5c n\xE0y l\xE0 b\u1EAFt bu\u1ED9c"),note:yup.string().required("Vui l\xF2ng nh\u1EADp ghi ch\xFA ")}),StyledWrapItem=e=>jsx(WrapItem,N({w:{base:"100%",md:"50%"},p:2},e)),CreateLead=({onClose:e,isOpen:r})=>{const{handleSubmit:n,register:a,control:l,formState:{errors:c},reset:u}=useForm({mode:"onSubmit",reValidateMode:"onBlur",resolver:o$1(schema$1)}),{data:d,loading:m}=useListSources(),b=useToast(),[A,{loading:_}]=useMutation(CREATE_LEAD,{refetchQueries:[GET_LEADS],onCompleted:({addCustomerLeads:C})=>{var S;C.success?(b({title:"Th\xE0nh c\xF4ng !",description:"T\u1EA1o lead th\xE0nh c\xF4ng !",status:"success",duration:3e3,isClosable:!0}),u(),e()):b({title:"T\u1EA1o lead th\u1EA5t b\u1EA1i !",description:(S=C==null?void 0:C.message)!=null?S:"Vui l\xF2ng ki\u1EC3m tra l\u1EA1i th\xF4ng tin \u0111\xE3 nh\u1EADp",status:"error",duration:3e3,isClosable:!0})},onError:C=>{var S;b({title:"T\u1EA1o lead th\u1EA5t b\u1EA1i !",description:(S=C==null?void 0:C.message)!=null?S:"Vui l\xF2ng ki\u1EC3m tra l\u1EA1i th\xF4ng tin",status:"error",duration:3e3,isClosable:!0})}});return jsxs(Modal$1,{isOpen:r,onClose:()=>{},size:"3xl",children:[jsx(ModalOverlay,{}),jsx(ModalContent,{children:jsxs("form",{onSubmit:n(async C=>{try{await A({variables:{input:{phoneNumber:C.phone,fullName:C.name,email:C.email,sourceId:C.source.id,note:C.note}}})}catch(S){console.log({e:S})}}),children:[jsx(ModalHeader,{children:"T\u1EA1o Lead m\u1EDBi"}),jsx(ModalCloseButton,{onClick:e}),jsxs(Box,{px:4,pb:8,children:[jsxs(Wrap,{spacing:0,children:[jsx(StyledWrapItem,{children:jsx(TextInput,H(N({label:"S\u1ED1 \u0111i\u1EC7n tho\u1EA1i (*)",placeholder:"Nh\u1EADp...",variant:"outline",pr:10},a("phone")),{error:c==null?void 0:c.phone}))}),jsx(StyledWrapItem,{children:jsx(TextInput,H(N({},a("name")),{label:"H\u1ECD t\xEAn (*)",placeholder:"Nh\u1EADp...",error:c==null?void 0:c.name}))}),jsx(StyledWrapItem,{children:jsx(TextInput,H(N({label:"Email (*)",placeholder:"Nh\u1EADp..."},a("email")),{error:c==null?void 0:c.email}))}),jsx(StyledWrapItem,{children:jsx(Controller,{name:"source",control:l,render:({field:C})=>jsx(Select$1,N({label:"Ngu\u1ED3n (*)",error:c==null?void 0:c.source,getOptionLabel:S=>S.sourceOfCustomer,getOptionValue:S=>S.id,options:d?d.getSourceOfCustomer:[],isLoading:m},C))})}),jsx(WrapItem,{w:"full",p:2,children:jsx(AutoResizeTextarea,H(N({label:"Ghi ch\xFA (*)",bg:"white",minRows:2,placeholder:"N\u1ED9i dung..."},a("note")),{error:c==null?void 0:c.note}))})]}),jsxs(HStack,{px:2,mt:4,spacing:4,children:[jsx(Button$1,{type:"submit",colorScheme:"green",isLoading:_,children:"T\u1EA1o m\u1EDBi"}),jsx(Button$1,{variant:"outline",onClick:e,children:"H\u1EE7y b\u1ECF"})]})]})]})})]})},ListUserInfoForAdmin=react.exports.memo(({conversationDetail:e})=>{const r=react.exports.useMemo(()=>{var n;return(n=e==null?void 0:e.participants)==null?void 0:n.map(a=>{var l;return jsx(UserItemInfo,{avatarUrl:a==null?void 0:a.avatar,username:a==null?void 0:a.fullName,positionName:(l=ROLES.find(c=>{var u;return c.id===Number((u=a.roleId)!=null?u:14)}))==null?void 0:l.name},a==null?void 0:a.userId)})},[e]);return jsx(Box,{borderBottomWidth:"1px",borderColor:"whiteAlpha.300",children:r})}),ManageParticipantDetail=()=>{var y,I,M,j,O,R,B,G,V,Q,W,q,L,D;const{onOpen:e,isOpen:r,onClose:n}=useDisclosure(),{isOpen:a,onOpen:l,onClose:c}=useDisclosure(),u=useStore(F=>F.userId),[d]=useUser(),m=useStore(F=>F.visibleUserProfile),b=useStore(F=>F.setVisibleUserProfile),A=useStore(F=>F.conversationId),{conversationDetail:_}=useGetConversationDetail(A,u),T=usePermissionForGroup({conversationDetail:_}),C=(y=_==null?void 0:_.participants)==null?void 0:y.find(F=>(F==null?void 0:F.roleId)==="5"||(F==null?void 0:F.roleId)===null),S=T?C:(I=_==null?void 0:_.participants)==null?void 0:I.filter(F=>(F==null?void 0:F.userId)!==(d==null?void 0:d.id.toString()))[0],{userData:P,loading:k}=useGetUserInfoGraphNet(u||(S==null?void 0:S.userId),_.name),x=T?(S==null?void 0:S.fullName)||((M=_==null?void 0:_.owner)==null?void 0:M.fullName):(P==null?void 0:P.full_name)?P==null?void 0:P.full_name:(S==null?void 0:S.isGuest)?`${S==null?void 0:S.fullName}`:S==null?void 0:S.fullName;useEventListener("click",F=>{const $=F.target;!$.closest(".profile")&&!$.closest("#js-toggle-profile")&&b(!1)});const g=useBreakpointValue({base:"",xl:"visible"});return!u&&!A?null:jsxs(Fragment,{children:[jsxs(Stack,{pos:g?void 0:"fixed",top:g?0:"56px",right:0,bottom:0,zIndex:51,h:"full",overflowY:"auto",bg:useColorModeValue("white","slate.900"),w:{base:"320px","2xl":"380px"},transform:g||m?"none":"translateX(100%)",transition:"all .3s ease",className:"profile",id:"profile-portal",spacing:0,children:[!(_==null?void 0:_.name)&&(((j=_==null?void 0:_.participants)==null?void 0:j.length)===2&&((O=_==null?void 0:_.participants)==null?void 0:O.find(F=>F.userId===d.id.toString()))||u||T&&ALLOW_EC_ROLE.includes(d==null?void 0:d.roleId))&&jsxs(Box,{children:[jsx(ParticipantProfile,{tags:(R=_==null?void 0:_.tags)!=null?R:[],isStudent:ALLOW_STUDENT_ROLE.includes(d==null?void 0:d.roleId),participantAvatarUrl:S==null?void 0:S.avatar,fullName:x,loading:k,participants:_==null?void 0:_.participants,participantInfo:P}),LEAD_PERMISSION.create.includes(d==null?void 0:d.roleId)&&(S==null?void 0:S.isGuest)&&jsxs(Box,{p:2,borderBottomWidth:1,borderColor:"whiteAlpha.300",children:[jsx(Button$1,{w:"full",colorScheme:"green",onClick:l,children:"Th\xEAm lead m\u1EDBi"}),LEAD_PERMISSION.create.includes(d==null?void 0:d.roleId)&&jsx(CreateLead,{isOpen:a,onClose:c})]}),(P==null?void 0:P.supporter)&&!ALLOW_STUDENT_ROLE.includes(d==null?void 0:d.roleId)&&((G=(B=_==null?void 0:_.participants)==null?void 0:B.filter(F=>F.roleId!=="5"))==null?void 0:G.map(F=>{var U;const $=(F==null?void 0:F.userId)===((U=P==null?void 0:P.supporter)==null?void 0:U.id.toString());return jsx(UserItemInfo,{username:F==null?void 0:F.fullName,positionName:`${$?"EC ch\xEDnh":"EC h\u1ED7 tr\u1EE3"}`},F==null?void 0:F.id)})),((V=P==null?void 0:P.supporter)==null?void 0:V.id)===Number(d==null?void 0:d.id)&&((Q=_==null?void 0:_.participants)==null?void 0:Q.length)&&jsx(Box,{w:"full",px:2,children:jsx(Button$1,{onClick:e,w:"full",rightIcon:jsx(TiUserAdd,{}),colorScheme:"blue",variant:"outline",children:"Th\xEAm EC h\u1ED7 tr\u1EE3"})})]}),T&&!(_==null?void 0:_.name)&&jsx(ListUserInfoForAdmin,{conversationDetail:_}),(_==null?void 0:_.name)&&jsx(GroupInfo,{conversationDetail:_}),A&&jsx(Notes,{isSuperAdmin:ALLOW_SUPER_ADMIN_ROLE.includes(d==null?void 0:d.roleId),allowForSupperAdmin:ALLOW_SUPER_ADMIN_ROLE.includes(d==null?void 0:d.roleId)&&((W=_==null?void 0:_.participants)==null?void 0:W.some(F=>(F==null?void 0:F.userId)===(d==null?void 0:d.id.toString())))}),((q=P==null?void 0:P.courseStudents)==null?void 0:q.length)>0&&!ALLOW_STUDENT_ROLE.includes(d==null?void 0:d.roleId)&&jsx(Courses,{data:(L=P==null?void 0:P.courseStudents)!=null?L:[]}),jsx(Attachments,{})]}),m&&jsx(Fragment,{children:jsx(Box,{bg:"blackAlpha.800",shadow:"md",pos:"fixed",top:14,bottom:0,zIndex:50,left:0,right:0,h:"screen"})}),jsx(CreateConversationModal,{isChannel:!1,isEdit:!0,isAddSupporter:!0,isOpen:r,onClose:n,listParticipants:(D=_==null?void 0:_.participants)==null?void 0:D.map(F=>F==null?void 0:F.userId),name:_==null?void 0:_.name,groupId:A})]})},useStore=create$1(e=>({conversationId:"",setConversationId:r=>e({conversationId:r}),userId:"",setUserId:r=>e({userId:r}),tab:"conversation",setTab:r=>e({tab:r}),channel:"",setChannel:r=>e({channel:r}),participants:[],setParticipants:r=>e({participants:r}),visibleUserProfile:!1,setVisibleUserProfile:r=>e({visibleUserProfile:r}),searchStringMessage:"",setSearchStringMessage:r=>e({searchStringMessage:r}),searchResult:[],setSearchResult:r=>e({searchResult:r}),nearSearchResult:[],setNearSearchResult:r=>e({nearSearchResult:r}),messageId:"",setMessageId:r=>e({messageId:r}),beforeId:"",setBeforeId:r=>e({beforeId:r}),afterId:"",setAfterId:r=>e({afterId:r})})),ChatPage=()=>{const e=useStore(S=>S.setChannel),r=useStore(S=>S.userId),n=useStore(S=>S.setUserId),a=useStore(S=>S.tab),l=useStore(S=>S.conversationId),c=useStore(S=>S.setConversationId),[u]=useUser(),[d,m]=useSearchParams({}),{height:b}=useGetWindowDimensions(),A=useBreakpointValue({base:"",md:"visible"});getEnviroment("ZIM_BUILD_MODE")==="module"&&useUpdateConversations();const _=d.get("qsConversationId"),T=d.get("qsUserId"),C=d.get("qsGroup");return react.exports.useEffect(()=>{(u==null?void 0:u.roleId)===6?e("guest"):e("my chats")},[u]),react.exports.useEffect(()=>{_||T?(_&&c(_),T&&n(T),C&&e(C)):(d.delete("qsConversationId"),d.delete("qsUserId"),d.delete("qsGroup"))},[_,T,C]),jsxs(VStack,{h:b-113,spacing:0,w:"full",children:[!A&&jsx(HeaderMobileChat,{}),jsxs(Box,{w:"100%",h:"100%",display:"flex",className:"divide-x",overflow:"hidden",children:[(A||!A&&a==="channel")&&jsx(ManageChannels,{}),(A||!A&&a==="conversation")&&jsx(MyListConversation,{}),(A||!A&&a==="message")&&jsx(BoxMessage,{}),(r||l)&&jsx(ManageParticipantDetail,{})]})]})};var index$1=Object.freeze({__proto__:null,[Symbol.toStringTag]:"Module",useStore,default:ChatPage});const useDeleteTagInConversation=()=>{const e=useToast(),r=useResetQueries(),n=useStore(u=>u.conversationId),[a,{loading:l}]=useMutation(DELETE_TAG_IN_CONVERSATION);return{onDeleteTag:react.exports.useCallback(async u=>{try{await a({variables:{tagId:u,conversationId:n}}),await r(["conversationDetail"])}catch(d){e({title:"L\u1ED7i !!!",description:d.message,status:"success",duration:2e3,isClosable:!0})}},[n,a,e,r]),loading:l}};function TagItem({id:e,name:r,color:n}){const{onDeleteTag:a,loading:l}=useDeleteTagInConversation(),c=useStore(u=>u.conversationId);return jsxs(Tag,{size:"md",rounded:"full",children:[jsx(Box,{bg:n,w:"5px",h:"5px",rounded:"full",mr:1}),jsx(TagLabel,{children:r}),l?jsx(Spinner,{color:"red.500",size:"xs",ml:2.5}):c&&jsx(TagCloseButton,{onClick:()=>a(e)})]})}const useParticipantDetail=(e,r)=>{const[n]=useUser();return react.exports.useMemo(()=>{var a,l;return[{icon:jsx(GoLocation,{}),value:(a=e==null?void 0:e.address)!=null?a:"Kh\xF4ng c\xF3 th\xF4ng tin",visible:ALLOW_VISIBLE_PROFILE_ROLE.includes(n.roleId)},{icon:jsx(BsTelephone,{}),value:(e==null?void 0:e.phone)||"Kh\xF4ng c\xF3 th\xF4ng tin",visible:ALLOW_VISIBLE_PROFILE_ROLE.includes(n.roleId)},{icon:jsx(FiFacebook,{}),value:((l=e==null?void 0:e.more_info)==null?void 0:l.facebook)||"Kh\xF4ng c\xF3 th\xF4ng tin",visible:ALLOW_VISIBLE_PROFILE_ROLE.includes(n.roleId)},{icon:jsx(FiMail,{}),value:(e==null?void 0:e.email)||"Kh\xF4ng c\xF3 th\xF4ng tin",visible:ALLOW_VISIBLE_PROFILE_ROLE.includes(n.roleId)},{icon:jsx(BsTags,{}),value:(r==null?void 0:r.length)?()=>jsx(Wrap,{flex:1,children:r==null?void 0:r.map((c,u)=>jsx(TagItem,N({},c),u))}):"Kh\xF4ng c\xF3 tag",visible:!0}]},[e,r,n])},zimRoles=["Admin","CC","CS","GV","EC","AM","KT","KTH","MK","BM","BP"],zimRolesNumber=[{id:1,name:"Super Admin"},{id:2,name:"CC"},{id:4,name:"Gi\xE1o vi\xEAn"},{id:6,name:"EC"},{id:8,name:"AM"},{id:11,name:"Marketer"},{id:12,name:"BM"}],ALLOW_SUPER_ADMIN_ROLE=[1],ALLOW_STUDENT_ROLE=[5],ALLOW_EC_ROLE=[5],ALLOW_VISIBLE_PROFILE_ROLE=[1,2,6],ERROR_FILE_SIZE="File c\u1EE7a b\u1EA1n v\u01B0\u1EE3t qu\xE1 10MB.",ERROR_FILE_NOT_SUPPORT="File c\u1EE7a b\u1EA1n kh\xF4ng \u0111\u01B0\u1EE3c h\u1ED7 tr\u1EE3.",tagColor=[{color:"#00DAA3"},{color:"#21A3F3"},{color:"#F51C50"},{color:"#006E7F"},{color:"#F8CB2E"}],ZimiansChannel=react.exports.memo(({searchString:e,overallMessageUnRead:r})=>{const[n]=useUser(),a=useStore(y=>y.setChannel),l=useStore(y=>y.setParticipants),c=useStore(y=>y.setConversationId),u=useStore(y=>y.setTab),d=useStore(y=>y.setUserId),[m,b]=useSearchParams({}),{listAccount:A,onLoadMore:_,hasNextPage:T,isLoadingMore:C,loading:S}=useGetListAccountChat({roles:zimRoles,searchString:e}),P=react.exports.useCallback(async()=>{await _()},[_]),k=react.exports.useMemo(()=>S?[1,2,3,4,5,6,7,8]:(A==null?void 0:A.filter(y=>{var I;return((I=y.user)==null?void 0:I.id)!==parseInt(n==null?void 0:n.id,10)}))||[],[n,S,A]),x=react.exports.useCallback(()=>{a("zimians"),u("conversation")},[]),E=react.exports.useCallback((y,I)=>{d(y),b({qsUserId:y,qsGroup:I}),m.delete("qsConversationId"),c(""),u("message"),a(I),l([])},[]),g=react.exports.useMemo(()=>jsx(ChannelItem,{canLoadMore:T,isLoading:C,onLoadMore:P,onClick:E,loading:S,conversations:k}),[k,S,T,C,E,P]);return jsx(MyAccordion,{onClick:x,title:"Zimians",overallMessageUnRead:r,children:g})}),useGetSupporterInfo=e=>{var a;const{data:r}=useQuery(GET_SUPPORTER_USER_INFO,{variables:{userId:e}}),n=r==null?void 0:r.getUserById;return{supporterInfo:n,id:(a=n==null?void 0:n.supporter)==null?void 0:a.id}},useGetListChannel=({search:e})=>{var b,A;const r=H(N({},pickBy_1({isChannel:!0,search:e},_=>_)),{limit:100,offset:0}),[n,{data:a,loading:l,fetchMore:c,called:u}]=useLazyQuery(CONVERSATIONS,{variables:r,fetchPolicy:"network-only",nextFetchPolicy:"cache-first"});react.exports.useEffect(()=>{u||(async()=>await n())()},[u,n]);const d=((b=a==null?void 0:a.conversations)==null?void 0:b.docs)||[],m=(A=a==null?void 0:a.conversations)==null?void 0:A.hasNextPage;return useLoadMore({variables:H(N({},r),{offset:(d==null?void 0:d.length)+1}),fetchMore:c,hasNextPage:m}),{listChannel:d,loading:l}},StudentChannel=react.exports.memo(({searchString:e,overallMessageUnRead:r})=>{const n=useStore(E=>E.setChannel),a=useStore(E=>E.setParticipants),l=useStore(E=>E.setConversationId),c=useStore(E=>E.setTab),u=useStore(E=>E.setUserId),[d,m]=useSearchParams({}),{listAccount:b,hasNextPage:A,isLoadingMore:_,onLoadMore:T,loading:C}=useGetListAccountChat({roles:["HV"],searchString:e}),S=react.exports.useMemo(()=>C?[1,2,3,4,5,6,7,8]:b,[C,b]),P=react.exports.useCallback(async()=>{await T()},[T]),k=react.exports.useCallback(()=>{n("student"),c("conversation")},[]),x=react.exports.useCallback((E,g)=>{u(E),m({qsUserId:E,qsGroup:g}),d.delete("qsConversationId"),l(""),c("message"),n(g),a([])},[]);return jsx(MyAccordion,{overallMessageUnRead:r,onClick:k,title:"Student",children:jsx(ChannelItem,{isLoading:_,canLoadMore:A,onLoadMore:P,onClick:x,loading:C,conversations:S})})}),NotiCount=react.exports.memo(({count:e})=>jsx(Badge$1,{w:6,h:6,variant:"solid",rounded:"full",bg:"blue.500",position:"absolute",right:8,alignItems:"center",justifyContent:"center",display:"flex",fontSize:11,children:jsx(Box,{alignItems:"center",justifyContent:"center",display:"flex",children:e>99?"99+":e})})),ManageChannels=react.exports.memo(()=>{var g,y;const[e]=useUser(),[r,n]=useSearchParams({}),a=useStore(I=>I.setParticipants),l=useStore(I=>I.setChannel),c=useStore(I=>I.setUserId),u=useStore(I=>I.setConversationId),d=useStore(I=>I.setTab),[m,b]=react.exports.useState(""),A=useDebounce$1(m,500),{overall:_,overallMyChats:T}=useGetOverallUnreadMessage(),{id:C}=useGetSupporterInfo(Number(e==null?void 0:e.id)),{listChannel:S}=useGetListChannel(m);S==null||S.filter(I=>I.name&&!["zimians","student"].includes(I.group));const P=react.exports.useMemo(()=>r,[]),k=react.exports.useCallback(async()=>{n({qsUserId:C}),P.delete("qsConversationId"),P.delete("qsGroup"),l("student"),c(C),d("message"),u(""),a([])},[C]),x=react.exports.useRef(null),E=react.exports.useCallback(()=>{l("guest"),d("conversation")},[]);return react.exports.useEffect(()=>{let I;return(x==null?void 0:x.current)&&(I=new PerfectScrollbar(x==null?void 0:x.current,{wheelSpeed:1,minScrollbarLength:10})),()=>{I&&I.destroy()}},[]),jsxs(Flex,{direction:"column",w:{base:"full",md:"200px",lg:"250px"},bg:useColorModeValue("white","#10172a"),h:"100%",children:[jsx(MyInfoSection,{searchString:m,setSearchString:b}),jsxs(Box,{flexGrow:1,ref:x,className:"scroll-container",overflow:"hidden",position:"relative",minH:0,children:[jsxs(Stack,{zIndex:50,spacing:0,position:"sticky",top:0,children:[(e==null?void 0:e.roleId)===5&&C&&jsx(Box,{display:"flex",alignItems:"center",px:4,h:10,cursor:"pointer",_hover:{backgroundColor:useColorModeValue("white","slate.500")},onClick:k,w:"100%",rounded:"none",borderWidth:"1px 0px 0px 0px",bg:useColorModeValue("white","#10172a"),fontSize:16,fontWeight:500,justifyContent:"start",children:jsx(Text,{children:"Chat v\u1EDBi supporter"})}),jsxs(Box,{display:"flex",alignItems:"center",px:4,h:10,cursor:"pointer",_hover:{backgroundColor:useColorModeValue("white","slate.500")},onClick:()=>{l("my chats"),d("conversation")},w:"100%",rounded:"none",borderWidth:"1px 0px 0px 0px",bg:useColorModeValue("white","#10172a"),fontSize:16,fontWeight:500,justifyContent:"start",children:["My Chats",T>0&&jsx(NotiCount,{count:T})]}),(e==null?void 0:e.roleId)!==4&&(e==null?void 0:e.roleId)!==5&&jsxs(Button$1,{display:"flex",alignItems:"center",px:4,h:10,cursor:"pointer",_hover:{backgroundColor:useColorModeValue("white","slate.500")},onClick:E,w:"100%",rounded:"none",borderWidth:"1px 0px 0px 0px",bg:useColorModeValue("white","#10172a"),fontSize:16,fontWeight:500,justifyContent:"start",children:["Guest",(_==null?void 0:_[3])+(_==null?void 0:_[4])>0&&jsx(NotiCount,{count:(_==null?void 0:_[3])+(_==null?void 0:_[4])})]})]}),jsxs(Stack,{display:{base:"block"},spacing:0,children:[(e==null?void 0:e.roleId)!==5&&jsx(ZimiansChannel,{overallMessageUnRead:(g=_==null?void 0:_[0])!=null?g:null,searchString:A}),jsx(StudentChannel,{overallMessageUnRead:(y=_==null?void 0:_[1])!=null?y:null,searchString:A})]})]})]})});function Header(){const{appState:{user:e},appSetLogout:r}=useAppContext(),n=useNavigate(),{colorMode:a,toggleColorMode:l}=useColorMode(),[c,u]=useToggle();getEnviroment("ZIM_BUILD_MODE")!=="module"&&useUpdateConversations();const{overallMyChats:d}=useGetOverallUnreadMessage(),m=_=>{_.preventDefault(),u()},b=_=>{const T=_.target;!T.closest(".main-menu")&&!T.closest("#js-toggle-menu")&&u(!1)},A=useBreakpointValue({base:"",md:"visible"});return useEventListener("click",b),jsxs(Fragment,{children:[jsx(Box,{zIndex:100,bg:useColorModeValue("white","slate.900"),px:4,borderBottom:"1px solid",borderBottomColor:useColorModeValue("gray.200","slate.800"),position:"sticky",top:0,children:jsxs(Flex,{h:14,alignItems:"center",justifyContent:"space-between",children:[jsxs(HStack,{cursor:"pointer",flexShrink:0,onClick:()=>n("/"),children:[jsx(Image$1,{src:logoSrc,objectFit:"contain",flexShrink:0,boxSize:"38px"}),jsx(Text,{fontSize:{lg:"xl",base:"md"},fontWeight:600,d:{sm:"none",md:"block"},children:"ZIM Academy"})]}),jsxs(Container,{maxW:"7xl",position:{base:"static",lg:"relative"},flexGrow:1,children:[jsxs(HStack,{spacing:4,flexGrow:1,children:[jsx(Button$1,{variant:"ghost",p:0,_hover:{backgroundColor:"transparent"},onClick:m,id:"js-toggle-menu",children:jsx(HamburgerIcon,{fontSize:32})}),jsx(Box,{flexGrow:1,children:jsx(SearchInput,{variant:"solid",bg:"gray.100"})})]}),jsx(Box,{children:jsx(Box,{position:"absolute",top:"100%",left:0,px:4,rounded:4,w:"full",id:"nav-menu",className:`${c?A:""} main-menu`,mt:2,children:jsx(Box,{bg:useColorModeValue("white","slate.800"),p:4,shadow:"sm",border:"1px solid",borderColor:useColorModeValue("gray.200","slate.700"),children:jsx(MegaMenu,{data:menuData,onClose:()=>u(!1)})})})})]}),jsx(Flex,{alignItems:"center",children:jsxs(Stack,{direction:"row",spacing:4,children:[jsxs(HStack,{spacing:1,children:[jsx(Button$1,{onClick:l,variant:"ghost",children:a==="light"?jsx(RiMoonClearLine,{}):jsx(SunIcon,{})}),jsxs(Button$1,{onClick:()=>n("/chat"),variant:"ghost",position:"relative",children:[jsx(Box,{position:"absolute",top:"-4px",right:"-32px",bg:"red",children:d>0&&jsx(NotiCount,{count:d})}),jsx(ChatIcon,{})]})]}),jsx(Box,{flexShrink:0,children:jsxs(Menu,{children:[jsx(MenuButton,{as:Button$1,rounded:"full",variant:"link",cursor:"pointer",minW:0,children:jsx(Avatar,{size:"sm",src:e==null?void 0:e.avatar})}),jsxs(MenuList,{alignItems:"center",py:4,children:[jsx(Center,{children:jsx(Avatar,{size:"xl",src:e==null?void 0:e.avatar})}),jsx(Center,{my:4,fontWeight:600,children:jsx("p",{children:e==null?void 0:e.fullName})}),jsx(MenuDivider,{}),jsx(MenuItem,{icon:jsx(Icon,{as:AiOutlineUser,w:4,h:4}),children:"T\xE0i kho\u1EA3n"}),jsx(MenuItem,{onClick:()=>r(),icon:jsx(Icon,{as:AiOutlineLogout,w:4,h:4}),children:"\u0110\u0103ng xu\u1EA5t"})]})]})})]})})]})}),jsx(Box,{w:"300px",shadow:"md",pos:"fixed",top:14,bottom:0,zIndex:101,h:"screen",overflowY:"auto",bg:useColorModeValue("white","slate.900"),className:"main-menu",display:{base:"block",md:"none"},transform:c?"none":"translateX(-100%)",transition:"all .3s ease",children:jsx(SideMenu,{data:menuData,onClose:()=>u(!1)})}),c&&jsx(Fragment,{children:jsx(Box,{bg:"blackAlpha.800",shadow:"md",pos:"fixed",top:14,bottom:0,zIndex:99,left:0,right:0,h:"screen"})})]})}const Breadcrumbs=({data:e=[]})=>jsx(Breadcrumb,{children:e&&e.map((r,n)=>jsx(BreadcrumbItem,{isCurrentPage:r.isActive,children:jsx(BreadcrumbLink,{as:Link$2,to:r.path,color:useColorModeValue("blue.500","blue.300"),children:r.breadcrumbText})},r.path+n))}),Home=loadable$2(()=>__vitePreload(()=>import("./index4.js"),["assets/index4.js","assets/vendor.js"])),ListCourse=loadable$2(()=>__vitePreload(()=>import("./index5.js"),["assets/index5.js","assets/vendor.js","assets/dateToStringFormat.js"])),CourseDetail=loadable$2(()=>__vitePreload(()=>import("./index6.js"),["assets/index6.js","assets/vendor.js","assets/useCreateOrUpdateCoursePlan.js","assets/useListSchools.js","assets/useListLevels.js","assets/useCourse.js","assets/index7.js","assets/dateToStringFormat.js","assets/main.js","assets/test-utils.js","assets/index8.js"])),TicketDetailPage=loadable$2(()=>__vitePreload(()=>import("./index9.js"),["assets/index9.js","assets/vendor.js","assets/index7.js","assets/tickets.js"])),TicketListPage=loadable$2(()=>__vitePreload(()=>import("./index10.js"),["assets/index10.js","assets/tickets.js","assets/vendor.js"])),Chat=loadable$2(()=>__vitePreload(()=>Promise.resolve().then(function(){return index$1}),void 0)),LeadReportPage=loadable$2(()=>__vitePreload(()=>import("./index11.js"),["assets/index11.js","assets/index7.js","assets/vendor.js","assets/dateToStringFormat.js"])),AllSupportedUsersPage=loadable$2(()=>__vitePreload(()=>import("./index12.js"),["assets/index12.js","assets/vendor.js"])),ScheduleRegistration=loadable$2(()=>__vitePreload(()=>import("./index13.js"),["assets/index13.js","assets/index7.js","assets/vendor.js","assets/main.js","assets/configs.js","assets/utils.js","assets/isSameOrAfter.js","assets/dateToStringFormat.js","assets/index14.js"])),MockTestOnlinePage$1=loadable$2(()=>__vitePreload(()=>Promise.resolve().then(function(){return index}),void 0)),MockTestOfflinePage=loadable$2(()=>__vitePreload(()=>import("./index15.js"),["assets/index15.js","assets/vendor.js"])),MockTestDetailPage=loadable$2(()=>__vitePreload(()=>import("./index16.js").then(function(e){return e.i}),["assets/index16.js","assets/vendor.js","assets/index14.js","assets/useGetIdOnRoute.js"])),ListSchedulesPage=loadable$2(()=>__vitePreload(()=>import("./index17.js").then(function(e){return e.i}),["assets/index17.js","assets/main.js","assets/vendor.js","assets/dateToStringFormat.js","assets/configs.js","assets/index18.js"])),RegisterTestSchedulePage=loadable$2(()=>__vitePreload(()=>import("./index19.js"),["assets/index19.js","assets/index7.js","assets/vendor.js"])),LeadPage=loadable$2(()=>__vitePreload(()=>import("./index20.js"),["assets/index20.js","assets/vendor.js","assets/index22.js","assets/useListSchools.js","assets/index21.js","assets/test-utils.js"])),CoursePlannerPage=loadable$2(()=>__vitePreload(()=>import("./index23.js"),["assets/index23.js","assets/vendor.js","assets/useCreateOrUpdateCoursePlan.js","assets/useListSchools.js","assets/useListLevels.js","assets/generateCourseName.js","assets/useCourse.js","assets/index21.js","assets/index22.js"])),CoursePlanDetailPage=loadable$2(()=>__vitePreload(()=>import("./index8.js"),["assets/index8.js","assets/vendor.js","assets/useCreateOrUpdateCoursePlan.js","assets/useListSchools.js","assets/useListLevels.js","assets/index7.js"])),PaperBasedTestBank=loadable$2(()=>__vitePreload(()=>import("./index24.js"),["assets/index24.js","assets/vendor.js"])),ExercisesPage=loadable$2(()=>__vitePreload(()=>import("./index25.js"),["assets/index25.js","assets/vendor.js","assets/CreateExerciseForm.js","assets/useListLevels.js"])),UpdateExercisePage=loadable$2(()=>__vitePreload(()=>import("./index26.js"),["assets/index26.js","assets/vendor.js","assets/useGetIdOnRoute.js","assets/index18.js","assets/CreateExerciseForm.js","assets/useListLevels.js"])),ListCoursePlan=loadable$2(()=>__vitePreload(()=>import("./index27.js"),["assets/index27.js","assets/vendor.js","assets/useCreateOrUpdateCoursePlan.js","assets/useListSchools.js","assets/useListLevels.js","assets/generateCourseName.js","assets/useCourse.js","assets/index21.js"])),routes=[{path:routePaths.home,exact:!0,component:Home,allowRoles:[],breadcrumbText:"Trang ch\u1EE7"},{path:`${routePaths.courseDetail}/:id`,exact:!0,component:CourseDetail,allowRoles:[],breadcrumbText:"Kh\xF3a h\u1ECDc"},{path:routePaths.listCourses,exact:!0,component:ListCourse,allowRoles:[],breadcrumbText:"Danh s\xE1ch kh\xF3a h\u1ECDc"},{path:`${routePaths.ticket}`,exact:!0,component:TicketListPage,allowRoles:[],breadcrumbText:"Danh s\xE1ch ticket"},{path:`${routePaths.ticket}/:id`,exact:!0,component:TicketDetailPage,allowRoles:[],breadcrumbText:"Chi ti\u1EBFt ticket"},{path:`${routePaths.leadReport}`,exact:!1,component:LeadReportPage,allowRoles:[],breadcrumbText:"Lead Report"},{path:`${routePaths.registerTestSchedule}`,exact:!1,component:RegisterTestSchedulePage,allowRoles:[],breadcrumbText:"\u0110\u0103ng k\xFD l\u1ECBch tr\u1EF1c test"},{path:`${routePaths.lead}`,exact:!1,component:LeadPage,allowRoles:[],breadcrumbText:"Ngu\u1ED3n kh\xE1ch h\xE0ng"},{path:`${routePaths.studentSupportByEc}`,exact:!1,component:AllSupportedUsersPage,allowRoles:[],breadcrumbText:"Danh s\xE1ch h\u1ECDc vi\xEAn"},{path:routePaths.chat,exact:!0,component:Chat,allowRoles:[],breadcrumbText:"Chat"},{path:routePaths.scheduleRegistration,exact:!0,component:ScheduleRegistration,allowRoles:[],breadcrumbText:"\u0110\u0103ng k\xFD l\u1ECBch l\xE0m vi\u1EC7c"},{path:routePaths.listSchedules,exact:!0,component:ListSchedulesPage,allowRoles:[],breadcrumbText:"Danh s\xE1ch l\u1ECBch l\xE0m vi\u1EC7c"},{path:routePaths.coursePlanner,exact:!0,component:CoursePlannerPage,allowRoles:[],breadcrumbText:"Course planner"},{path:`${routePaths.coursePlanDetail}/:id`,exact:!0,component:CoursePlanDetailPage,allowRoles:[],breadcrumbText:"Course plan detail"},{path:routePaths.paperBasedTestBank,exact:!0,component:PaperBasedTestBank,allowRoles:[],breadcrumbText:"Danh s\xE1ch \u0111\u1EC1 thi"},{path:routePaths.mockTestOnline,exact:!0,component:MockTestOnlinePage$1,allowRoles:[],breadcrumbText:"Mock test online"},{path:routePaths.mockTestOffline,exact:!0,component:MockTestOfflinePage,allowRoles:[],breadcrumbText:"Mock test offline"},{path:`${routePaths.mockTestDetail}/:id`,exact:!0,component:MockTestDetailPage,allowRoles:[],breadcrumbText:"Mock test detail"},{path:`${routePaths.listCoursesPlan}`,exact:!0,component:ListCoursePlan,allowRoles:[],breadcrumbText:"Kh\xF3a h\u1ECDc d\u1EF1 ki\u1EBFn"},{path:`${routePaths.exercises}`,exact:!0,component:ExercisesPage,allowRoles:[],breadcrumbText:"Danh s\xE1ch b\xE0i t\u1EADp"},{path:`${routePaths.updateExercise}/:id`,exact:!0,component:UpdateExercisePage,allowRoles:[],breadcrumbText:"C\u1EADp nh\u1EADt b\xE0i t\u1EADp"}];function Footer(){return jsx(Box,{bg:useColorModeValue("gray.50","gray.800"),color:useColorModeValue("gray.700","gray.200"),children:jsx(Container,{as:Stack,maxW:"full",py:2,direction:{base:"column",md:"row"},spacing:4,justify:{base:"center",md:"space-between"},align:{base:"center",md:"center"},children:jsx(Text,{children:"\xA9 2022 ZIM Academy"})})})}const Layout=({children:e,contentOnly:r=!1})=>{const{pathname:n}=useRouter(),a=useParams(),l=routes.filter(({path:c})=>n.includes(c)||matchPath(c,n)).map(d=>{var m=d,{path:c}=m,u=Te(m,["path"]);return H(N({path:Object.keys(a).length?Object.keys(a).reduce((b,A)=>b.replace(`:${A}`,a==null?void 0:a[A]),c):c},u),{isActive:c===n})});return r?jsx(VStack,{w:"full",justifyContent:"stretch",alignItems:"stretch",spacing:0,minH:"full",bg:useColorModeValue("#f2f3f5","slate.900"),children:jsx(Box,{flexShrink:0,flexGrow:1,px:4,py:8,maxW:2560,className:"mx-auto",w:"full",children:e})}):jsxs(VStack,{w:"full",justifyContent:"stretch",alignItems:"stretch",spacing:0,minH:"full",bg:useColorModeValue("#f2f3f5","slate.900"),children:[jsx(Header,{}),jsx(Box,{bg:useColorModeValue("gray.50","slate.800"),px:4,py:2,children:(l==null?void 0:l.find(c=>c.path!=="/chat"&&c.path!=="/"))&&jsx(Breadcrumbs,{data:l})}),jsx(Box,{flexShrink:0,flexGrow:1,maxW:2560,className:"mx-auto",w:"full",children:e}),jsx(Footer,{})]})},ProtectedRoute=a=>{var l=a,{children:e,allowRoles:r=[]}=l,n=Te(l,["children","allowRoles"]);const{appState:{isAuthenticated:c,user:u}}=useAppContext(),d=useLocation();if(c&&r.length>0&&u&&!r.includes(u==null?void 0:u.roleId))return jsx(Navigate,{to:"/error/403"});const m=d.search.replaceAll("?","");return c?e:jsx(Navigate,{to:`/auth/login?redirectUrl=${encodeURIComponent(`${d.pathname}?${m}`)}`,state:{from:d}})},useSignIn=()=>{const{appSetLogin:e}=useAppContext(),r=useResetQueries();return useMutation(SIGN_IN,{onCompleted:async n=>{n.signInV2&&(await e(n.signInV2.token),await r(["conversations"]))}})},schema=yup.object().shape({username:yup.string().required("Vui l\xF2ng nh\u1EADp t\xE0i kho\u1EA3n"),password:yup.string().required("Vui l\xF2ng nh\u1EADp m\u1EADt kh\u1EA9u")}),Login=()=>{var m,b;const{appState:e}=useAppContext(),{register:r,handleSubmit:n,formState:{errors:a}}=useForm({resolver:o$1(schema),mode:"onBlur",reValidateMode:"onChange"}),l=useRouter(),[c,{loading:u}]=useSignIn(),d=react.exports.useCallback(async A=>{const{username:_,password:T}=A;await c({variables:{username:_,password:T}})},[]);return e.isAuthenticated&&e.user?jsx(Navigate,{to:(b=decodeURIComponent((m=l.query)==null?void 0:m.redirectUrl))!=null?b:"/"}):jsx(Fragment,{children:jsxs(Stack,{minH:"100vh",direction:{base:"column",md:"row"},children:[jsx(Flex,{flex:1,children:jsx(Image$1,{alt:"Login Image",objectFit:"cover",src:"https://www.nikaiacours.fr/wp-content/uploads/2019/12/login-background.jpg"})}),jsx(Flex,{p:8,flex:1,align:"center",justify:"center",as:"form",onSubmit:n(d),children:jsxs(Stack,{spacing:4,w:"full",maxW:"md",children:[jsx(Heading,{fontSize:"2xl",children:"Sign in to your account"}),jsxs(FormControl,{id:"email",children:[jsx(FormLabel,{children:"Email address"}),jsx(Input$1,N({type:"text"},r("username")))]}),jsxs(FormControl,{id:"password",children:[jsx(FormLabel,{children:"Password"}),jsx(Input$1,N({type:"password"},r("password")))]}),jsxs(Stack,{spacing:6,children:[jsxs(Stack,{direction:{base:"column",sm:"row"},align:"start",justify:"space-between",children:[jsx(Checkbox,{children:"Remember me"}),jsx(Link$1,{color:"blue.500",as:Link$2,to:"/auth/forgot",children:"Forgot password?"})]}),jsx(Button$1,{isLoading:u,type:"submit",colorScheme:"blue",variant:"solid",children:"Sign in"})]})]})})]})})};function ErrorFallback({error:e,componentStack:r,resetErrorBoundary:n}){return jsxs(Alert,{role:"alert",status:"error",as:VStack,my:4,children:[jsxs(Code,{p:4,d:"block",children:[jsx("pre",{children:e.message}),jsx("pre",{children:r})]}),jsx(Button$1,{onClick:n,colorScheme:"facebook",children:"Try again"})]})}const TestDate=({date:e,shifts:r})=>jsxs(VStack,{align:"stretch",spacing:.5,children:[jsx(Text,{fontWeight:600,children:e}),r==null?void 0:r.map(n=>{var a,l;return jsxs(VStack,{align:"stretch",spacing:.5,overflow:"hidden",children:[jsxs(HStack,{spacing:1,children:[jsxs(Text,{fontWeight:600,children:[n.name,":"]}),jsxs(Text,{children:[n.start_time," - ",n.end_time]})]}),((a=n.ecs)==null?void 0:a.length)>0&&jsxs(Text,{children:["EC: ",n.ecs.map(c=>c.full_name).join(", ")]}),((l=n.teachers)==null?void 0:l.length)>0&&jsxs(Text,{children:["GV:"," ",n.teachers.map(c=>c.full_name).join(", ")]})]},` ${e}-${n.id}`)})]}),useBaseMockTestColumnDefs=({onEditMockTest:e=()=>{}}={})=>{const[r]=useUser();return[{Header:"Trung t\xE2m",accessor:"center",nowrap:!0,width:"10px"},{Header:"M\xF4n h\u1ECDc",accessor:"subject",nowrap:!0,width:"10px"},{Header:"Ng\xE0y thi 1",accessor:"testDate1",Cell:({value:n})=>jsx(TestDate,N({},n)),maxWidth:400,minWidth:200},{Header:"Ng\xE0y thi 2",accessor:"testDate2",Cell:({value:n})=>jsx(TestDate,N({},n)),maxWidth:400,minWidth:200},{Header:"S\u1ED1 l\u01B0\u1EE3ng",accessor:"slot",nowrap:!0},{Header:"Tr\u1EA1ng th\xE1i",accessor:"status",nowrap:!0,Cell:({value:n})=>{var a;return jsx(Badge$1,{colorScheme:(a=MOCK_TEST_STATUS[n])==null?void 0:a.colorScheme,children:n})}},{Header:"Thao t\xE1c",accessor:"actions",Cell:({row:{original:n}})=>jsxs(HStack,{children:[GROUP_ROLES.canCreateOrEditMockTest.includes(r.roleId)&&jsx(IconButton,{onClick:()=>e(n.id),"aria-label":"edit",icon:jsx(Icon,{as:AiFillEdit})}),jsx(NavLink,{to:useBuildModeValue(`${routePaths.mockTestDetail}/${n.id}`,`${appZimPaths.mockTestDetail}/${n.id}`),isExternal:!0,children:jsx(IconButton,{"aria-label":"view-detail",icon:jsx(Icon,{as:ViewIcon})})})]}),nowrap:!0,isSticky:!0,stickyRightOffset:0}]},Title=e=>jsx(Heading,N({as:"h1",fontSize:"lg"},e));var reactDatepicker="";newStyled(Box)`
  .react-datepicker-wrapper {
    display: block;
  }

  .react-datepicker {
    display: flex;
    border: 0 !important;
    box-shadow: 4px 0px 8px 0px rgba(0, 0, 0, 0.15);
    .react-datepicker__time-container {
      border-color: var(--chakra-colors-gray-200);
    }
  }

  .react-datepicker__header {
    border-bottom: 0;
    background: var(--chakra-colors-gray-100);
  }

  .react-datepicker__day--today {
    background: var(--chakra-colors-yellow-200);
    color: var(--chakra-colors-gray-900);
    border-radius: 0.3rem;
  }

  .react-datepicker__day--selected {
    background: var(--chakra-colors-primary);
    color: #fff;
  }
  react-datepicker__tab-loop {
    .react-datepicker-popper {
      z-index: 11;
    }
  }

  .react-datepicker-popper[data-placement^='bottom']
    .react-datepicker__triangle,
  .react-datepicker-popper[data-placement^='bottom']
    .react-datepicker__triangle:before {
    border-bottom-color: var(--chakra-colors-gray-100);
  }
  .react-datepicker__input-container {
    border-color: var(--chakra-colors-gray-100);
  }
`;const DateTimePicker=react.exports.forwardRef((l,a)=>{var c=l,{error:e,label:r}=c,n=Te(c,["error","label"]);return jsxs(FormControl,{isInvalid:!!e,children:[r&&jsx(FormLabel,{htmlFor:n.id,children:r}),jsx(Wt,H(N({},n),{customInput:jsx(Input$1,{autoComplete:"off",background:useColorModeValue("white","slate.700")}),ref:a})),e&&jsx(FormErrorMessage,{children:e&&(e==null?void 0:e.message)})]})});var DateTimePicker$1=react.exports.memo(DateTimePicker);const Wrapper=({children:e,split:r=!1})=>jsx(Box,{w:{base:r?"50%":"100%",md:r?"25%":"50%",lg:"33.33%",xl:"25%","2xl":"20%"},p:2,children:e}),useQueryOption=({query:e,options:r={}})=>{const{data:n,loading:a,refetch:l}=useQuery(e,N({fetchPolicy:"cache-and-network"},r));return[n,a,l]},allOption={label:"T\u1EA5t c\u1EA3",value:"all"},initialFilterState={school:allOption,subject:allOption,openStatuses:[],openDateFrom:null,openDateTo:null,finishDateFrom:null,finishDateTo:null,level:allOption,shifts:[],ec:allOption,dayOfWeeks:[],skills:[],preTeachers:[]},CourseFilter=({onSubmit:e,loading:r})=>{var O,R;const[n,a]=useToggle(),[l,c]=react.exports.useState(initialFilterState),[u,d]=react.exports.useState([]),[m,b]=react.exports.useState([]),[A,_]=react.exports.useState([]),[T,C]=useQueryOption({query:GET_SCHOOLS}),[S,P]=useQueryOption({query:GET_SUBJECTS$1,options:{variables:{status:"active"}}}),[k,x,E]=useQueryOption({query:GET_LEVELS$1,options:{variables:{subject_id:((O=l==null?void 0:l.subject)==null?void 0:O.value)==="all"?0:(R=l==null?void 0:l.subject)==null?void 0:R.value}}}),g=react.exports.useCallback(B=>{c(H(N({},l),{openDateFrom:B}))},[l]),y=react.exports.useCallback(B=>{c(H(N({},l),{finishDateTo:B}))},[l]),I=react.exports.useCallback((B,G)=>{(G==null?void 0:G.name)&&c(H(N({},l),{[G.name]:B}))},[l]),M=()=>{e(l)},j=()=>{c(initialFilterState),e(initialFilterState)};return react.exports.useEffect(()=>{(T==null?void 0:T.schools)&&!C&&d(T.schools.map(B=>H(N({},B),{label:B.name,value:B.id})))},[T,C]),react.exports.useEffect(()=>{(S==null?void 0:S.subjects)&&!P&&b(S.subjects.map(B=>H(N({},B),{label:B.name,value:B.id})))},[S,P]),react.exports.useEffect(()=>{(k==null?void 0:k.levels)&&!x&&_(k.levels.map(B=>H(N({},B),{label:B.graduation,value:B.id})))},[k,x]),react.exports.useEffect(()=>{var B,G;!((B=l.subject)==null?void 0:B.value)||((G=l.subject)==null?void 0:G.value)==="all"?(c(H(N({},l),{level:allOption})),_([])):(async()=>{var Q;return await E({subject_id:(Q=l.subject)==null?void 0:Q.value})})()},[l.subject.value]),react.exports.useEffect(()=>{JSON.stringify(l)===JSON.stringify(initialFilterState)?a(!1):a(!0)},[l]),jsxs(Box,{children:[jsxs(Flex,{mx:-2,flexWrap:"wrap",children:[jsx(Wrapper,{children:jsx(Select$1,{id:"f-school",name:"school",label:"Trung t\xE2m",value:l.school,options:u,isLoading:C,onChange:I,paramKey:"school",allOption:{label:"T\u1EA5t c\u1EA3",value:"all"}})}),jsx(Wrapper,{children:jsx(Select$1,{id:"f-subject",name:"subject",label:"M\xF4n h\u1ECDc",value:l.subject,options:m,isLoading:P,onChange:I,paramKey:"subject",allOption:{label:"T\u1EA5t c\u1EA3 ",value:"all"}})}),jsx(Wrapper,{children:jsx(Select$1,{name:"level",isLoading:x,label:"Tr\xECnh \u0111\u1ED9",value:l.level,options:A,onChange:I,allOption:{label:"T\u1EA5t c\u1EA3 ",value:"all"}})}),jsxs(Wrapper,{children:[jsx(FormControl,{children:jsx(FormLabel,{htmlFor:"f-startDate",children:"Khai gi\u1EA3ng"})}),jsx(DateTimePicker$1,{id:"f-startDate",selected:l.openDateFrom,onChange:g,dateFormat:"dd/MM/yyyy",placeholderText:"Ch\u1ECDn ng\xE0y"})]}),jsxs(Wrapper,{children:[jsx(FormControl,{children:jsx(FormLabel,{htmlFor:"f-endDate",children:"K\u1EBFt th\xFAc"})}),jsx(DateTimePicker$1,{id:"f-endDate",selected:l.finishDateTo,onChange:y,dateFormat:"dd/MM/yyyy",placeholderText:"Ch\u1ECDn ng\xE0y",monthsShown:1})]}),jsx(Wrapper,{children:jsx(Select$1,{label:"Tr\u1EA1ng th\xE1i",name:"openStatuses",isMulti:!0,options:courseStatusOptions,value:l.openStatuses,onChange:I})})]}),jsxs(HStack,{mt:4,spacing:4,children:[jsx(Button$1,{colorScheme:"purple",width:{base:"100%",md:"auto"},onClick:M,isLoading:r,children:"L\u1ECDc d\u1EEF li\u1EC7u"}),n&&jsx(Button$1,{leftIcon:jsx(MdClear,{}),variant:"ghost",colorScheme:"red",onClick:j,flexShrink:0,isDisabled:r,children:"X\xF3a b\u1ED9 l\u1ECDc"})]})]})},blankStateMockTestFilter={province:null,center:{label:"T\u1EA5t c\u1EA3",value:0},status:{label:"T\u1EA5t c\u1EA3",value:0},startDate:null,endDate:null,subject:{name:"T\u1EA5t c\u1EA3",id:0}},subjectSpeakingId=2,subjectLwrId=1,mockTestOnlineId=1,mockTestOfflineId=6,GET_SUBJECT_FOR_MOCK_TEST=gql`
  query @api(name: "appZim") {
    getSubjectForMockTest {
      id
      name
      status
      catMockTest {
        id
        name
        subject {
          id
          name
        }
        mockTestSubCat {
          id
          name
          cat {
            id
            name
          }
        }
      }
    }
  }
`,GET_SUBJECT_OF_MOCK_TEST_FOR_FILER=gql`
  query @api(name: "appZim") {
    getSubjectForMockTest {
      id
      name
    }
  }
`,GET_LIST_MOCK_TEST_BANK=gql`
  query getListMockTestBank($subjectId: Int, $offset: Int, $limit: Int)
  @api(name: "appZim") {
    getListMockTestBank(limit: $limit, offset: $offset, subjectId: $subjectId) {
      edges {
        node {
          id
          subject {
            id
            name
          }
          mockTestSubCat {
            id
            name
          }
          cat {
            id
            name
          }
          answerLink
          questionLink
          referenceLink
          isPublish
          usedNumber
        }
      }
      totalCount
      pageInfo {
        hasNextPage
        hasPreviousPage
      }
    }
  }
`,GET_MOCK_TEST_BANK=gql`
  query getMockTestBank($id: Int) @api(name: "appZim") {
    getMockTestBank(id: $id) {
      id
      subject {
        id
        name
      }
      mockTestSubCat {
        id
        name
      }
      cat {
        id
        name
      }
      answerLink
      questionLink
      referenceLink
      isPublish
    }
  }
`,GET_SUB_CAT_MOCK_TEST=gql`
  query ($catIds: [Int]!) @api(name: "appZim") {
    getSubCatMockTest(catIds: $catIds) {
      id
      name
    }
  }
`,useListSubjectsOfMockTest=e=>{const r=useToast();return useQuery(GET_SUBJECT_FOR_MOCK_TEST,N({onError:n=>{r({title:"L\u1EA5y d\u1EEF li\u1EC7u th\u1EA5t b\u1EA1i.",description:`GET  subject of mock test error: ${n==null?void 0:n.message}`,status:"error",duration:3e3,isClosable:!0})},fetchPolicy:"cache-first",errorPolicy:"all"},e))};dayjs.extend(customParseFormat);const stringToDate=(e,r="DD/MM/YYYY HH:mm")=>dayjs(e,r).toDate();dayjs.extend(customParseFormat);const dateAddTime=(e,r,n="DD/MM/YYYY "+r)=>stringToDate(dayjs(e).format(n));function arrayToSelectOption(e,r,n,a=!0){return e.map(l=>a?H(N({},l),{label:l[r],id:l[n]}):{label:l[r],id:l[n]})}const MockTestFilter=({onFilter:e,onResetState:r,listCenters:n=[]})=>{var C;const{control:a,watch:l,handleSubmit:c}=useFormContext(),T=l(),{startDate:u,endDate:d}=T,m=Te(T,["startDate","endDate"]),{data:b}=useListSubjectsOfMockTest(),A=arrayToSelectOption((C=b==null?void 0:b.getSubjectForMockTest)!=null?C:[],"name","id"),_=!lodash.exports.isEqual(blankStateMockTestFilter,H(N({},m),{startDate:u,endDate:d}));return jsxs(Box,{children:[jsxs(Flex,{flexWrap:"wrap",children:[jsx(Wrapper,{children:jsx(Controller,{control:a,render:({field:S})=>jsx(Select$1,H(N({},S),{options:[{label:"T\u1EA5t c\u1EA3",value:0},...n],label:"Trung t\xE2m"})),name:"center"})}),jsxs(Wrapper,{children:[jsx(FormControl,{children:jsx(FormLabel,{htmlFor:"startDate",children:"T\u1EEB ng\xE0y"})}),jsx(Controller,{render:({field:{value:S,onChange:P}})=>jsx(DateTimePicker$1,{selected:S,onChange:P,placeholderText:"Ch\u1ECDn...",id:"startDate",maxDate:d,dateFormat:"dd/MM/yyyy",autoComplete:"off"}),name:"startDate"})]}),jsxs(Wrapper,{children:[jsx(FormControl,{children:jsx(FormLabel,{htmlFor:"endDate",children:"\u0110\u1EBFn ng\xE0y"})}),jsx(Controller,{render:({field:{value:S,onChange:P}})=>jsx(DateTimePicker$1,{selected:S,onChange:P,placeholderText:"Ch\u1ECDn...",id:"endDate",minDate:u,dateFormat:"dd/MM/yyyy",autoComplete:"off"}),name:"endDate"})]}),jsx(Wrapper,{children:jsx(Controller,{control:a,render:({field:S})=>jsx(Select$1,H(N({label:"Tr\u1EA1ng th\xE1i"},S),{options:[{label:"T\u1EA5t c\u1EA3",value:0},...MockTestStatusOptions]})),name:"status"})}),jsx(Wrapper,{children:jsx(Controller,{control:a,render:({field:S})=>jsx(Select$1,H(N({label:"M\xF4n h\u1ECDc"},S),{options:[{name:"T\u1EA5t c\u1EA3",id:0},...A],getOptionLabel:P=>P.name,getOptionValue:P=>P.id.toString()})),name:"subject"})})]}),jsxs(HStack,{spacing:4,mt:4,children:[jsx(Button$1,{mx:2,colorScheme:"green",onClick:c(e),children:"L\u1ECDc d\u1EEF li\u1EC7u"}),_&&jsx(Button$1,{leftIcon:jsx(MdClear,{}),variant:"ghost",colorScheme:"red",onClick:r,flexShrink:0,children:"X\xF3a b\u1ED9 l\u1ECDc"})]})]})},QuestionFragment=gql`
  fragment QuestionField on IeltsPracticeQuestion {
    id
    testId
    questionGroupId
    question
    questionIndex
    questionCount
    sampleAnswers
    allowToBreakPoint

    answers {
      answer
      correct
      alternatives
    }
    explanation
    type
  }
`,QuestionGroupFragment=gql`
  fragment QuestionGroupField on IeltsPracticeQuestionGroup {
    id
    title
    testId
    question
    groupIndex
    audioFiles {
      id
      url
    }
    questions {
      ...QuestionField
    }
  }
  ${QuestionFragment}
`,ModuleFragment=gql`
  fragment ModuleField on IeltsPracticeTest {
    id
    title
    type
    level
    status
    format
    parts {
      question
      questionGroupId
    }
    audio {
      id
      url
    }
    questionGroups {
      ...QuestionGroupField
    }
  }
  ${QuestionGroupFragment}
`,PracticeTestFragment=gql`
  fragment PracticeTestField on IeltsPracticeTestGroup {
    id
    title
    slug
    level
    status
    practiceTests {
      id
      title
      type
      level
      status
    }
    active
  }
`;gql`
  mutation createIELTSPracticeTest(
    $type: IeltsPracticeTestType!
    $level: IeltsPracticeTestLevel!
    $title: String
    $audio: Upload
    $parts: [IeltsPracticeTestPartInput]
    $format: IeltsPracticeTestFormat
  ) @api(name: "zim") {
    createIELTSPracticeTest(
      type: $type
      level: $level
      title: $title
      audio: $audio
      parts: $parts
      format: $format
    ) {
      id
      title
      type
      level
      status
      parts {
        question
        questionGroupId
      }
    }
  }
`;gql`
  mutation updateIELTSPracticeTest(
    $id: ObjectID!
    $type: IeltsPracticeTestType!
    $level: IeltsPracticeTestLevel!
    $title: String
    $audio: Upload
    $parts: [IeltsPracticeTestPartInput]
    $format: IeltsPracticeTestFormat
  ) @api(name: "zim") {
    updateIELTSPracticeTest(
      id: $id
      type: $type
      level: $level
      title: $title
      audio: $audio
      parts: $parts
      format: $format
    ) {
      id
      type
      level
      status
      parts {
        question
        questionGroupId
      }
    }
  }
`;gql`
  mutation publishIELTSPracticeTest($id: ObjectID!) @api(name: "zim") {
    publishIELTSPracticeTest(id: $id) {
      id
      type
      level
      status
      questionGroups {
        ...QuestionGroupField
      }
    }
  }
  ${QuestionGroupFragment}
`;gql`
  mutation createIELTSPracticeTestQuestionGroup(
    $testId: ObjectID!
    $question: String!
    $groupIndex: Int
    $title: String
  ) @api(name: "zim") {
    createIELTSPracticeTestQuestionGroup(
      testId: $testId
      question: $question
      groupIndex: $groupIndex
      title: $title
    ) {
      ...QuestionGroupField
    }
  }
  ${QuestionGroupFragment}
`;gql`
  mutation updateIELTSPracticeTestQuestionGroup(
    $id: ObjectID!
    $question: String!
    $title: String
    $groupIndex: Int
  ) @api(name: "zim") {
    updateIELTSPracticeTestQuestionGroup(
      id: $id
      question: $question
      title: $title
      groupIndex: $groupIndex
    ) {
      ...QuestionGroupField
    }
  }
  ${QuestionGroupFragment}
`;gql`
  mutation removeIELTSPracticeTestQuestionGroup($id: ObjectID!)
  @api(name: "zim") {
    removeIELTSPracticeTestQuestionGroup(id: $id)
  }
`;gql`
  mutation removeIELTSPracticeTestQuestion($id: ObjectID!) @api(name: "zim") {
    removeIELTSPracticeTestQuestion(id: $id)
  }
`;gql`
  mutation createIELTSPracticeTestQuestion(
    $questionGroupId: ObjectID!
    $input: IELTSPracticeTestQuestionInput!
    $questionIndex: Int
  ) @api(name: "zim") {
    createIELTSPracticeTestQuestion(
      questionGroupId: $questionGroupId
      input: $input
      questionIndex: $questionIndex
    ) {
      ...QuestionField
    }
  }
  ${QuestionFragment}
`;gql`
  mutation updateIELTSPracticeTestQuestion(
    $id: ObjectID!
    $input: IELTSPracticeTestQuestionInput!
  ) @api(name: "zim") {
    updateIELTSPracticeTestQuestion(id: $id, input: $input) {
      ...QuestionField
    }
  }
  ${QuestionFragment}
`;gql`
  mutation swapIELTSPracticeTestQuestionIndex(
    $firstId: ObjectID!
    $secondId: ObjectID!
  ) @api(name: "zim") {
    swapIELTSPracticeTestQuestionIndex(firstId: $firstId, secondId: $secondId) {
      id
      testId
    }
  }
`;gql`
  mutation swapIELTSPracticeTestQuestionGroupIndex(
    $firstId: ObjectID!
    $secondId: ObjectID!
  ) @api(name: "zim") {
    swapIELTSPracticeTestQuestionGroupIndex(
      firstId: $firstId
      secondId: $secondId
    ) {
      id
      testId
    }
  }
`;gql`
  mutation createIELTSPracticeTestGroup(
    $title: String!
    $slug: String!
    $level: IeltsPracticeTestLevel!
    $practiceTests: [ObjectID]!
    $featureMediaIds: [ObjectID]!
    $description: String
  ) @api(name: "zim") {
    createIELTSPracticeTestGroup(
      title: $title
      slug: $slug
      level: $level
      practiceTests: $practiceTests
      featureMediaIds: $featureMediaIds
      description: $description
    ) {
      ...PracticeTestField
    }
  }
  ${PracticeTestFragment}
`;gql`
  mutation updateIELTSPracticeTestGroup(
    $id: ObjectID!
    $title: String!
    $slug: String!
    $level: IeltsPracticeTestLevel!
    $practiceTests: [ObjectID]!
    $featureMediaIds: [ObjectID]!
    $description: String
    $categoryIds: [ObjectID]
  ) @api(name: "zim") {
    updateIELTSPracticeTestGroup(
      id: $id
      title: $title
      slug: $slug
      level: $level
      practiceTests: $practiceTests
      featureMediaIds: $featureMediaIds
      description: $description
      categoryIds: $categoryIds
    ) {
      ...PracticeTestField
    }
  }
  ${PracticeTestFragment}
`;gql`
  mutation publishIELTSPracticeTestGroup($id: ObjectID!) @api(name: "zim") {
    publishIELTSPracticeTestGroup(id: $id) {
      ...PracticeTestField
    }
  }
  ${PracticeTestFragment}
`;gql`
  mutation unpublishIELTSPracticeTestGroup($id: ObjectID!) @api(name: "zim") {
    unpublishIELTSPracticeTestGroup(id: $id) {
      ...PracticeTestField
    }
  }
  ${PracticeTestFragment}
`;gql`
  mutation setActiveIELTSPracticeTestGroup($id: ObjectID!) @api(name: "zim") {
    setActiveIELTSPracticeTestGroup(id: $id) {
      ...PracticeTestField
    }
  }
  ${PracticeTestFragment}
`;gql`
  mutation updateIELTSPracticeTestGroupSeoConfig(
    $IELTSPracticeTestGroupId: ObjectID!
    $seo: CommonSeoInput!
  ) @api(name: "zim") {
    updateIELTSPracticeTestGroupSeoConfig(
      IELTSPracticeTestGroupId: $IELTSPracticeTestGroupId
      seo: $seo
    ) {
      seo {
        title
        description
        ogDescription
        ogImage {
          id
          type
          path
          filename
          title
          visibility
          width
          height
          usage
          createdAt
          updatedAt
        }
        ogTitle
        publisher
        noIndex
        noFollow
        canonicalUrl
        customHeadHtml
      }
    }
  }
`;const CREATE_MOCK_TEST=gql`
  mutation createMockTest(
    $dates: [String]
    $school_id: Int!
    $type: EnumMockTestType
    $subjectId: Int!
  ) @api(name: "appZim") {
    createMockTest(
      dates: $dates
      school_id: $school_id
      type: $type
      subjectId: $subjectId
    ) {
      id
      slug
      type
      status
      fee
      total_slot
    }
  }
`,UPDATE_MOCK_TEST=gql`
  mutation updateMockTest(
    $id: Int
    $status: EnumBaseStatus
    $dates: [String]
    $school_id: Int
  ) @api(name: "appZim") {
    updateMockTest(
      id: $id
      status: $status
      dates: $dates
      school_id: $school_id
    ) {
      id
    }
  }
`,UPDATE_MOCK_TEST_DETAIL_V2=gql`
  mutation updateMockTestDetailV2(
    $mockTestId: Int!
    $mockTestDetail: [MockTestDetailInputV2Type]!
  ) @api(name: "appZim") {
    updateMockTestDetailV2(
      mockTestDetail: $mockTestDetail
      mockTestId: $mockTestId
    )
  }
`,UPDATE_MOCK_TEST_ATTENDANCE_V2=gql`
  mutation updateMockTestAttendanceV2(
    $mock_test_detail_id: Int
    $student_id: Int
    $is_missing: Boolean
  ) @api(name: "appZim") {
    updateMockTestAttendanceV2(
      mock_test_detail_id: $mock_test_detail_id
      student_id: $student_id
      is_missing: $is_missing
    ) {
      id
    }
  }
`,UPLOAD_RESULT_STUDENT=gql`
  mutation uploadResultStudent($input: UploadResultMockTestOrderInputType)
  @api(name: "appZim") {
    uploadResultStudent(input: $input) {
      success
    }
  }
`,UPDATE_RESULT_MOCK_TEST=gql`
  mutation updateResultMockTest(
    $orderId: Int
    $input: UpdateOrderDetailInputType
  ) @api(name: "appZim") {
    updateResultMockTest(orderId: $orderId, input: $input) {
      message
    }
  }
`,SENT_EMAIL_RESULT_FOR_STUDENT=gql`
  mutation sentEmailResultForStudent($orderId: Int) @api(name: "appZim") {
    sentEmailResultForStudent(orderId: $orderId) {
      message
    }
  }
`,CREATE_MOCK_TEST_OF_STUDENT_SCHEDULE=gql`
  mutation createMockTetOfStudentSchedule(
    $input: CreateMockTestOfStudentScheduleInputType
  ) @api(name: "appZim") {
    createMockTestOfStudentSchedule(input: $input) {
      message
    }
  }
`,UPDATE_MOCK_TEST_ORDER_CORRECTING_SECTION=gql`
  mutation updateMockTestOrderCorrectingSession(
    $input: MockTestOrderCorrectingSessionInputType
  ) @api(name: "appZim") {
    updateMockTestOrderCorrectingSession(input: $input) {
      sessionIdListening
      sessionIdReading
      sessionIdWriting
    }
  }
`,SEO_FRAGMENT=gql`
  fragment SEOField on CommonSeoConfig {
    title
    description
    ogDescription
    ogImage {
      id
      type
      path
      variants {
        id
        width
        height
        path
        type
      }
      filename
      title
      visibility
      width
      height
      usage
      createdAt
      updatedAt
    }
    ogTitle
    publisher
    noIndex
    noFollow
    canonicalUrl
    customHeadHtml
  }
`,MOCK_TEST_FIELD=gql`
  fragment MockTestField on MockTestV2Type {
    id
    slug
    status
    fee
    total_slot
    available_slot
    school {
      id
      name
    }
    mockTestDetails {
      id
      date
      shift {
        id
        name
        start_time
        end_time
      }
      subject {
        id
        name
      }
      mock_test_shift_id
      mock_test_subject_id
      ecs {
        id
        full_name
      }
      teachers {
        id
        full_name
      }
    }
    subject {
      id
      name
    }
  }
`,MOCK_TEST_FIELD_FOR_DETAIL=gql`
  fragment MockTestFieldForDetail on MockTestV2Type {
    id
    slug
    status
    fee
    type
    total_slot
    available_slot
    sent_email_status
    school {
      id
      name
    }
    mockTestDetails {
      id
      date
      shift {
        id
        name
        start_time
        end_time
      }
      subject {
        id
        name
      }
      mock_test_shift_id
      mock_test_subject_id
      available_slot
      total_slot
      currentSlot
      ecs {
        id
        full_name
      }
      teachers {
        id
        full_name
      }
    }
    examQuestionMockTest {
      id
      cat {
        id
        name
      }
      configMockTestBank {
        id
        questionLink
        answerLink
        referenceLink
        mockTestSubCat {
          id
          name
        }
      }
    }
    practiceTests {
      id
      module
      listeningId
      readingId
      writingId
    }
  }
`;gql`
  query IELTSPracticeTestGroupPagination(
    $page: Int
    $limit: Int
    $status: [IeltsPracticeTestGroupStatus]
    $level: [IeltsPracticeTestLevel]
  ) @api(name: "zim") {
    IELTSPracticeTestGroupPagination(
      page: $page
      limit: $limit
      status: $status
      level: $level
    ) {
      hasNextPage
      hasPrevPage
      limit
      nextPage
      page
      pagingCounter
      prevPage
      totalDocs
      totalPages
      docs {
        id
        slug
        title
        level
        status
        active
        categories {
          id
          title
          description
        }
        practiceTests {
          id
          title
        }
      }
    }
  }
`;gql`
  query IELTSPracticeTestPagination(
    $page: Int
    $limit: Int
    $type: [IeltsPracticeTestType]
    $status: [IeltsPracticeTestStatus]
    $level: [IeltsPracticeTestLevel]
    $format: [IeltsPracticeTestFormat]
  ) @api(name: "zim") {
    IELTSPracticeTestPagination(
      page: $page
      limit: $limit
      type: $type
      status: $status
      level: $level
      format: $format
    ) {
      hasNextPage
      hasPrevPage
      limit
      nextPage
      page
      pagingCounter
      prevPage
      totalDocs
      totalPages
      docs {
        ...ModuleField
      }
    }
  }
  ${ModuleFragment}
`;gql`
  query IELTSPracticeTest($id: ObjectID!) @api(name: "zim") {
    IELTSPracticeTest(id: $id) {
      ...ModuleField
    }
  }
  ${ModuleFragment}
`;gql`
  query IELTSPracticeTestGroup($id: ObjectID!) @api(name: "zim") {
    IELTSPracticeTestGroup(id: $id) {
      id
      title
      slug
      description
      level
      status
      active
      practiceTests {
        id
        title
        type
        status
      }
      categories {
        id
        title
        description
        slug
      }
      featureMedias {
        id
        type
        path
        variants {
          id
          width
          height
          path
          type
        }
        filename
        title
        visibility
        width
        height
        usage
        createdAt
        updatedAt
      }
      seo {
        ...SEOField
      }
    }
  }
  ${SEO_FRAGMENT}
`;const GET_MOCK_TEST_DETAIL_FOR_EDIT=gql`
  query getMockTestDetailV2($idOrSlug: String) @api(name: "appZim") {
    getMockTestDetailV2(idOrSlug: $idOrSlug) {
      id
      status
      details {
        date
      }
      fee
      school {
        id
        name
      }
      subject {
        id
        name
      }
    }
  }
`,GET_MOCK_TEST_DETAIL=gql`
  query getMockTestDetailV2($idOrSlug: String) @api(name: "appZim") {
    getMockTestDetailV2(idOrSlug: $idOrSlug) {
      ...MockTestFieldForDetail
    }
  }
  ${MOCK_TEST_FIELD_FOR_DETAIL}
`,GET_MOCK_TEST_DETAIL_BY_FIELD=(e=[])=>gql`
  query getMockTestDetailByField($idOrSlug: String) @api(name: "appZim") {
    getMockTestDetailV2(idOrSlug: $idOrSlug) {
      id
      ${e.join()}
    }
  }
`,GET_MOCK_TESTS_BY_ROLE_V2=gql`
  query getListMockTestByRoleV2(
    $q: String
    $type: EnumMockTestType
    $status: EnumBaseStatus
    $schoolId: Int
    $fromDate: String
    $toDate: String
    $offset: Int
    $limit: Int
    $subjectId: Int
  ) @api(name: "appZim") {
    getListMockTestByRoleV2(
      q: $q
      type: $type
      status: $status
      schoolId: $schoolId
      fromDate: $fromDate
      toDate: $toDate
      offset: $offset
      limit: $limit
      subjectId: $subjectId
    ) {
      edges {
        node {
          ...MockTestField
        }
      }
      totalCount
      pageInfo {
        hasNextPage
        hasPreviousPage
      }
    }
  }
  ${MOCK_TEST_FIELD}
`,GET_LIST_TEACHER_REGISTER_FOR_FUNCTION=gql`
  query getListTeacherRegisterForFunction(
    $type: Int!
    $date: String!
    $schoolId: Int
    $shift: Int!
  ) @api(name: "appZim") {
    getListTeacherRegisterForFunction(
      type: $type
      date: $date
      schoolId: $schoolId
      shift: $shift
    ) {
      id
      full_name
    }
  }
`,GET_LIST_EC_FOR_CENTER=gql`
  query getListEcForCenter($schoolId: Int) @api(name: "appZim") {
    getListEcForCenter(schoolId: $schoolId) {
      id
      full_name
    }
  }
`,MOCK_TEST_ORDERS_BY_ROLES=gql`
  query getListMockTestOrderByRoles(
    $mocktestId: String
    $status: MockTestStatusEnum
  ) @api(name: "appZim") {
    getListMockTestOrderByRoles(mocktestId: $mocktestId, status: $status) {
      id
      id_number
      status
      order_id
      student {
        id
        full_name
        phone
        email
      }
      owner {
        name
        email
        phone
      }
      moduleV2 {
        id
        name
        create_date
        status
      }
      mockTestDetailV2 {
        date
        id
        shift {
          id
          start_time
          end_time
        }
        subject {
          id
          name
        }
      }
      schedulesV2 {
        is_missing
        subject {
          id
        }
        timePreview
        platform_id
      }
      feedback_writing1
      feedback_writing2
      teacherV2 {
        id
        full_name
      }
      catV2 {
        id
        name
      }
      mockTestResultv2 {
        id
        reading
        writing1
        writing2
        writing_note
        overall
        listening
        speaking
        send_email_status
        speakingNote
        sessions {
          sessionIdReading
          sessionIdWriting
          sessionIdSpeaking
          sessionIdListening
          sessionIdWritingTask1
          sessionIdWritingTask2
        }
        mock_test_writings {
          id
          essay_correction {
            id
            title
          }
          score
          status
        }
      }
      imageMockTestOrder {
        id
        mockTestOrderId
        mockTestSubCatId
        mockTestSubCat {
          id
          name
        }
        url
      }
    }
  }
`,GET_SUB_CAT_MOCK_TEST_FOR_STUDENT=gql`
  query ($catId: Int!) @api(name: "appZim") {
    getSubCatMockTestForStudent(catId: $catId) {
      id
      name
    }
  }
`,GET_IMAGE_MOCK_TEST_ORDER_BY_SUB_CAT=gql`
  query getImageMockTestOrderBySubCat($orderId: Int!, $mockTestSubCatId: Int)
  @api(name: "appZim") {
    getImageMockTestOrderBySubCat(
      mockTestSubCatId: $mockTestSubCatId
      orderId: $orderId
    ) {
      id
      mockTestOrderId
      mockTestSubCatId
      mockTestSubCat {
        id
        name
      }
      url
    }
  }
`;gql`
  query calcWritingAverage($writingTask1: Float, $writingTask2: Float)
  @api(name: "appZim") {
    calcWritingAverage(writingTask1: $writingTask1, writingTask2: $writingTask2)
  }
`;const CALC_OVERALL=gql`
  query calcOverall(
    $listening: Float!
    $reading: Float!
    $writing: Float
    $writingTask1: Float
    $writingTask2: Float
    $speaking: Float!
  ) @api(name: "appZim") {
    calcOverall(
      listening: $listening
      reading: $reading
      writing: $writing
      speaking: $speaking
      writingTask1: $writingTask1
      writingTask2: $writingTask2
    )
  }
`,GET_RESULT_MOCK_TEST_STUDENT=gql`
  query getResultMockTestStudent($mocktestId: String) @api(name: "appZim") {
    getResultMockTestStudent(mocktestId: $mocktestId) {
      id
      listening
      reading
      writing1
      writing2
      writing_note
      send_email_status
      speaking
      speakingNote
    }
  }
`,GET_LIST_TEACHER_ASSIGNED_MOCK_TEST=gql`
  query getListTeacherAssignedMockTest($mockTestId: Int!) @api(name: "appZim") {
    getListTeacherAssignedMockTest(mockTestId: $mockTestId) {
      id
      full_name
    }
  }
`,GET_LIST_TEACHER_OF_MOCK_TEST_WITH_SCHEDULE=gql`
  query getListTeacherOfMockTestWithSchedule($mockTestId: Int, $orderId: Int)
  @api(name: "appZim") {
    getListTeacherOfMockTestWithSchedule(
      mockTestId: $mockTestId
      orderId: $orderId
    ) {
      teacher_id
      teacher_name
      schedule {
        mock_test_detail_id
        isChange
        id_number
        lRW
        speaking
      }
    }
  }
`,blankState={center:null,date1:null,date2:null,fee:"",status:null,subject:null},AddMockTestModal=({isOpen:e,onClose:r,isMockTestOnline:n,isEditMockTest:a,mockTestId:l,listCenters:c=[]})=>{var O;const{control:u,handleSubmit:d,clearErrors:m,reset:b,watch:A,setValue:_}=useForm({defaultValues:N({},blankState)}),T=useToast(),C=useResetQueries(),{data:S}=useListSubjectsOfMockTest(),P=arrayToSelectOption((O=S==null?void 0:S.getSubjectForMockTest)!=null?O:[],"name","id"),{loading:k}=useQuery(GET_MOCK_TEST_DETAIL_FOR_EDIT,{variables:{idOrSlug:String(l)},onCompleted:R=>{const B=R.getMockTestDetailV2,G=MockTestStatusOptions.find(V=>V.value===B.status);_("fee",B.fee),_("center",{label:B.school.name,value:B.school.id}),_("date1",new Date(B.details[0].date)),_("date2",new Date(B.details[1].date)),_("status",G),_("subject",B.subject)},onError:R=>{T({title:"L\u1ED7i!",description:R.message,status:"error",duration:15e3,isClosable:!0})},skip:!l}),x=a?UPDATE_MOCK_TEST:CREATE_MOCK_TEST,[E,{loading:g}]=useMutation(x,{onCompleted:()=>{T({title:"Th\xE0nh c\xF4ng!",description:`${a?"C\u1EADp nh\u1EADt":"T\u1EA1o"} \u0111\u1EE3t thi th\u1EED th\xE0nh c\xF4ng`,status:"success",duration:3e3,isClosable:!0}),r()},onError:R=>{var B;T({title:"Th\u1EA5t b\u1EA1i!",description:(B=R.message)!=null?B:`${a?"C\u1EADp nh\u1EADt":"T\u1EA1o"} \u0111\u1EE3t thi th\u1EED th\u1EA5t b\u1EA1i`,status:"error",duration:15e3,isClosable:!0})}}),y=A("date1"),I=A("date2"),M=react.exports.useCallback(()=>{m(),b(N({},blankState)),r()},[]),j=react.exports.useCallback(async R=>{var G;const B={dates:[dayjs(R.date1).format("YYYY/MM/DD 00:00"),dayjs(R.date2).format("YYYY/MM/DD 00:00")],school_id:R.center.value,type:n?EnumMockTestType.Online:EnumMockTestType.Offline,id:l,status:(G=R.status)==null?void 0:G.value,subjectId:R.subject.id};a?delete B.subjectId:(delete B.status,delete B.id);try{await E({variables:B}),C(["getMockTestDetailV2","getListMockTestByRoleV2"])}catch(V){console.log(V)}},[n,a,l]);return e?jsxs(Modal$1,{isOpen:e,onClose:M,isCentered:!0,children:[jsx(ModalOverlay,{}),jsxs(ModalContent,{pos:"relative",children:[jsx(ModalCloseButton,{}),jsx(ModalHeader,{children:jsxs(Title,{children:[a?"C\u1EADp nh\u1EADt":"T\u1EA1o"," \u0111\u1EE3t thi th\u1EED"]})}),jsxs(ModalBody,{experimental_spaceY:2,children:[jsx(Controller,{rules:{required:"Vui l\xF2ng ch\u1ECDn m\xF4n h\u1ECDc"},control:u,render:({field:R,fieldState:{error:B}})=>jsx(Select$1,H(N({},R),{label:"M\xF4n h\u1ECDc (*)",options:P,error:B,getOptionValue:G=>G.id.toString(),getOptionLabel:G=>G.name,isDisabled:a})),name:"subject"}),jsx(Controller,{rules:{required:"Vui l\xF2ng ch\u1ECDn trung t\xE2m"},control:u,render:({field:R,fieldState:{error:B}})=>jsx(Select$1,H(N({},R),{label:"Trung t\xE2m (*)",options:c,error:B})),name:"center"}),jsxs(HStack,{spacing:4,children:[jsxs(VStack,{spacing:1,align:"stretch",children:[jsx(FormControl,{children:jsx(FormLabel,{htmlFor:"date1",children:"Ng\xE0y thi 1 (*)"})}),jsx(Controller,{rules:{required:"Ch\u1ECDn ng\xE0y thi 1"},control:u,render:({field:{value:R,onChange:B},fieldState:{error:G}})=>jsx(DateTimePicker$1,{id:"date1",selected:R,onChange:B,dateFormat:"dd/MM/yyyy",placeholderText:"Ch\u1ECDn...",error:G,maxDate:I?dayjs(I).subtract(1,"day").$d:null,minDate:new Date,autoComplete:"off"}),name:"date1"})]}),jsxs(VStack,{spacing:1,children:[jsx(FormControl,{children:jsx(FormLabel,{htmlFor:"date1",children:"Ng\xE0y thi 2 (*)"})}),jsx(Controller,{rules:{required:"Ch\u1ECDn ng\xE0y thi 2"},control:u,render:({field:{value:R,onChange:B},fieldState:{error:G}})=>jsx(DateTimePicker$1,{minDate:y?dayjs(y).add(1,"day").$d:new Date,id:"date2",selected:R,onChange:B,dateFormat:"dd/MM/yyyy",placeholderText:"Ch\u1ECDn...",error:G,autoComplete:"off"}),name:"date2"})]})]}),a&&jsx(Controller,{control:u,render:({field:R})=>jsx(Select$1,H(N({label:"Tr\u1EA1ng th\xE1i (*)"},R),{options:MockTestStatusOptions})),name:"status"})]}),jsx(ModalFooter,{children:jsx(Button$1,{colorScheme:"blue",w:"full",onClick:d(j),isLoading:g,children:a?"C\u1EADp nh\u1EADt":"T\u1EA1o"})}),k&&jsx(VStack,{bg:useColorModeValue("gray.50","slate.800"),justify:"center",alignItems:"center",w:"full",h:"full",pos:"absolute",top:0,rounded:8,children:jsx(Loading,{})})]})]}):null};gql`
  query schools($city_id: Int, $status: String) @api(name: "appZim") {
    schools(city_id: $city_id, status: $status) {
      id
      city {
        id
      }
      district {
        id
      }
      name
      address
      phone
      color_code
      lat
      lon
      status
    }
  }
`;const GET_GRADES=gql`
  query cats @api(name: "appZim") {
    cats {
      id
      name
    }
  }
`,GET_SUBJECTS=gql`
  query subjects($name: String, $status: StatusEnum) @api(name: "appZim") {
    subjects(name: $name, status: $status) {
      id
      name
      status
      cats {
        id
        name
        status
        subject {
          id
          name
          status
        }
      }
    }
  }
`;gql`
  query subcatsBySubjectId($subject_id: Int!) @api(name: "appZim") {
    subcatsBySubjectId(subject_id: $subject_id) {
      id
      name
      status
      cat_id
    }
  }
`;const GET_CURRICULUMS=gql`
  query curriculums($name: String, $status: StatusEnum, $program_id: Int)
  @api(name: "appZim") {
    curriculums(name: $name, status: $status, program_id: $program_id) {
      id
      name
      status
      shift_minute
      total_lesson
    }
  }
`,GET_SHIFTS=gql`
  query shifts($q: String, $status: String, $shift_minute: Int)
  @api(name: "appZim") {
    shifts(q: $q, status: $status, shift_minute: $shift_minute) {
      id
      status
      shift_minute
      start_time
      end_time
    }
  }
`,GET_SIZES=gql`
  query sizes($q: String, $status: String) @api(name: "appZim") {
    sizes(q: $q, status: $status) {
      id
      status
      name
      size
    }
  }
`,GET_LEVELS=gql`
  query levels(
    $subject_id: Int!
    $graduation: Float
    $status: EnumLevelStatus
    $name: String
    $type: String
  ) @api(name: "appZim") {
    levels(
      subject_id: $subject_id
      graduation: $graduation
      status: $status
      name: $name
      type: $type
    ) {
      id
      name
      status
      graduation
    }
  }
`,GET_EXERCISE_TYPES=gql`
  query @api(name: "appZim") {
    getExerciseTypes {
      id
      name
    }
  }
`,GET_QUESTION_TYPE=gql`
  query @api(name: "appZim") {
    getQuestionType {
      id
      name
    }
  }
`,GET_SUB_CATS=gql`
  query subcatsBySubjectId($subject_id: Int!) @api(name: "appZim") {
    subcatsBySubjectId(subject_id: $subject_id) {
      id
      name
    }
  }
`;gql`
  query levels(
    $name: String
    $status: EnumLevelStatus
    $type: String
    $subject_id: Int
    $graduation: Float
  ) @api(name: "appZim") {
    levels(
      name: $name
      status: $status
      type: $type
      subject_id: $subject_id
      graduation: $graduation
    ) {
      id
      name
    }
  }
`;const GET_LESSON_BY_CONFIG=gql`
  query getLessonByConfig(
    $subjectId: Int!
    $catId: Int!
    $subcatId: Int!
    $levelIn: Int!
    $levelOut: Int!
  ) @api(name: "appZim") {
    getLessonByConfig(
      subjectId: $subjectId
      catId: $catId
      subcatId: $subcatId
      levelIn: $levelIn
      levelOut: $levelOut
    ) {
      id
      title
    }
  }
`,GET_SUB_CAT_BY_CAT_ID=gql`
  query subcats($cat_id: Int, $status: String) @api(name: "appZim") {
    subcats(cat_id: $cat_id, status: $status) {
      id
      name
    }
  }
`,GET_LIST_SCHOOL_ASSIGN_FOR_TEACHER=gql`
  query getListSchoolAssignForTeacher(
    $teacherId: CustomUserInputType
    $functionId: Int
  ) @api(name: "appZim") {
    getListSchoolAssignForTeacher(
      teacherId: $teacherId
      functionId: $functionId
    ) {
      id
      name
    }
  }
`,GET_ACCOUNTS_BY_ROLE=gql`
  query getAccountsByRole(
    $userId: CustomUserInputType
    $schoolId: Int
    $roleId: Int!
  ) @api(name: "appZim") {
    getAccountsByRole(userId: $userId, schoolId: $schoolId, roleId: $roleId) {
      id
      full_name
    }
  }
`,useGetCenterByUser=({onCompleted:e=()=>{},variables:r}={})=>{const n=useToast(),[a]=useUser();return useQuery(GET_LIST_SCHOOL_ASSIGN_FOR_TEACHER,{variables:N({teacherId:Number(a.id)},r),onCompleted:l=>{var u;const c=(u=l.getListSchoolAssignForTeacher)==null?void 0:u.map(d=>({label:d.name,value:d.id}));e(c)},onError:l=>{var c;n({title:"Failed",description:(c=l.message)!=null?c:"L\u1EA5y danh s\xE1ch trung t\xE2m kh\xF4ng th\xE0nh c\xF4ng !!",status:"error",duration:3e3,isClosable:!0})},skip:!a})},Pagination=a=>{var l=a,{onPageChange:e,paramKey:r}=l,n=Te(l,["onPageChange","paramKey"]);const{colorMode:c}=useColorMode(),u=useRouter();let[d,m]=useSearchParams();const b=react.exports.useCallback(({selected:A})=>{r&&m(H(N({},u.query),{[r]:A+1})),e({selected:A})},[n]);return jsx(StyledBox,{colorMode:c,children:jsx(ReactPaginate,H(N({className:"pagination",pageRangeDisplayed:2,previousLinkClassName:"nav prev",nextLinkClassName:"nav next",breakLinkClassName:"link",pageLinkClassName:"link",activeLinkClassName:"active",containerClassName:"pagination",nextLabel:jsx(Icon,{as:ChevronRightIcon,width:8,height:8}),previousLabel:jsx(Icon,{as:ChevronLeftIcon,width:8,height:8})},n),{onPageChange:b}))})},StyledBox=newStyled(Box)`
  .pagination {
    list-style: none;
    display: flex;
    gap: 0.25rem;
    align-items: stretch;
    flex-wrap: wrap;
    justify-content: center;
  }
  .link {
    padding: 0.5rem 0.25rem;
    background: ${({theme:e,colorMode:r})=>{var n,a;return r==="light"?(n=e.colors.gray)==null?void 0:n["50"]:(a=e.colors.slate)==null?void 0:a["700"]}};
    border-radius: 4px;
    transition: all;
    border: 1px solid
      ${({theme:e,colorMode:r})=>{var n,a;return r==="light"?(n=e.colors.gray)==null?void 0:n["200"]:(a=e.colors.slate)==null?void 0:a["800"]}};
    min-width: 40px;
    display: inline-block;
    text-align: center;
    color: ${({theme:e,colorMode:r})=>{var n,a;return r==="light"?(n=e.colors.blue)==null?void 0:n["500"]:(a=e.colors.blue)==null?void 0:a["200"]}};
    &:hover {
      background: ${({theme:e})=>{var r;return(r=e.colors.brand)==null?void 0:r["100"]}};
      color: ${({theme:e})=>{var r;return(r=e.colors.brand)==null?void 0:r["700"]}};
    }
    &.active {
      background: ${({theme:e})=>{var r;return(r=e.colors.brand)==null?void 0:r["500"]}};
      color: ${({theme:e})=>e.colors.white};
    }
  }
  a.nav {
    display: inline-flex;
    justify-content: center;
    align-items: center;
    height: 100%;
  }
`,groupByShift=(e=[])=>uniqBy_1(e.map(n=>n.shift),"id").map(n=>{const a=e.filter(d=>d.shift.id===n.id)||[],l=a.reduce((d,m)=>(d.push(H(N({},m.subject||{}),{mockTestDetailId:m.id,ecs:m.ecs,total_slot:m.total_slot,available_slot:m.available_slot,currentSlot:m.currentSlot})),d),[]).sort((d,m)=>d.id-m.id),c=a.reduce((d,m)=>d.concat(m.ecs),[]),u=a.reduce((d,m)=>d.concat(m.teachers),[]);return H(N({},n),{subjects:uniqBy_1(l,"id"),ecs:c,teachers:u})});function groupByDate(e=[]){var n;const r=e.reduce(function(a,l){return a[l.date]=a[l.date]||[],a[l.date].push(l),a},Object.create(null));return(n=Object.entries(r))==null?void 0:n.map(([a,l])=>({date:a,shifts:groupByShift(l)}))}const convertDataTestDate=e=>{var r;return{date:dayjs(e==null?void 0:e.date).format("DD/MM/YYYY"),shifts:(r=e==null?void 0:e.shifts)==null?void 0:r.map(n=>H(N({},n),{ecs:uniqBy_1(n.ecs,"id"),teachers:n.teachers}))}},convertMockTestData=(e=[])=>e.map(r=>{var a;const n=groupByDate(r.mockTestDetails);return H(N({},r),{center:r.school.name,testDate1:convertDataTestDate(n==null?void 0:n[0]),testDate2:convertDataTestDate(n==null?void 0:n[1]),slot:`${r.available_slot}/${r.total_slot}`,status:r.status,subject:(a=r==null?void 0:r.subject)==null?void 0:a.name})}),ListMockTests=({isMockTestOnline:e})=>{const{isOpen:r,onOpen:n,onClose:a}=useDisclosure(),[l,c]=react.exports.useState(0),[u,d]=react.exports.useState(""),[m,b]=react.exports.useState(N({},blankStateMockTestFilter)),[A,_]=react.exports.useState([]),[T,C]=react.exports.useState(0),[S,P]=react.exports.useState(!1),[k,x]=react.exports.useState([]),[E,g]=react.exports.useState(1),[y,I]=react.exports.useState({label:"10 rows",value:10}),[M]=useUser();useGetCenterByUser({onCompleted:_,variables:{functionId:e?1:0}});const j=react.exports.useMemo(()=>({type:e?EnumMockTestType.Online:EnumMockTestType.Offline,offset:l*y.value,limit:y.value,schoolId:m.center.value===0?null:m.center.value,fromDate:m.startDate?dayjs(m.startDate).format("YYYY/MM/DD 00:00"):null,toDate:m.endDate?dayjs(m.endDate).format("YYYY/MM/DD 00:00"):null,status:m.status.value===0?null:m.status.value,subjectId:m.subject.id===0?null:m.subject.id,q:u}),[e,u,m,l,y]),{loading:O,refetch:R}=useQuery(GET_MOCK_TESTS_BY_ROLE_V2,{variables:j,onCompleted:$=>{x(convertMockTestData($.getListMockTestByRoleV2.edges.map(U=>N({},U.node)))),g($.getListMockTestByRoleV2.totalCount)}}),B=react.exports.useCallback(({target:$})=>{const{value:U}=$;U===""&&(d(""),c(0),R(H(N({},j),{q:"",offset:0,limit:y.value})))},[j,y]),G=useBaseMockTestColumnDefs({onEditMockTest:$=>{C($),n(),P(!0)}}),{isOpen:V,onToggle:Q}=useDisclosure({defaultIsOpen:!0}),W=react.exports.useCallback(()=>{a(),C(0),P(!1)},[]),q=useForm({defaultValues:N({},m)}),L=react.exports.useCallback(async $=>{d($),c(0)},[]),D=react.exports.useCallback(()=>{q.reset(N({},blankStateMockTestFilter)),b(N({},blankStateMockTestFilter)),c(0)},[]),F=$=>{c(0),I($)};return jsxs(VStack,{align:"stretch",p:4,spacing:4,children:[jsxs(Title,{children:["Mock test ",e?"online":"offline"]}),jsxs(HStack,{justifyContent:"space-between",children:[GROUP_ROLES.canCreateOrEditMockTest.includes(M.roleId)?jsx(Button$1,{colorScheme:"blue",onClick:n,children:"T\u1EA1o \u0111\u1EE3t thi th\u1EED"}):jsx(Box,{}),jsxs(HStack,{alignItems:"center",width:{base:"100%",sm:"auto"},children:[jsx(SearchInput,{variant:"outline",onSubmitSearch:L,placeholder:"T\xECm ki\u1EBFm...",w:"100%",onKeyUp:B}),jsx(IconButton,{colorScheme:V?"blue":"gray","aria-label":"toggle filter",icon:jsx(Icon,{as:MdFilterListAlt,w:6,h:6}),onClick:Q})]})]}),jsx(Collapse,{in:V,animateOpacity:!0,children:jsx(Card$1,{bg:useColorModeValue("gray.50","slate.800"),mt:4,children:jsx(FormProvider,H(N({},q),{children:jsx(MockTestFilter,{onFilter:$=>{b($),c(0)},onResetState:D,listCenters:A})}))})}),jsx(DataTable,{columns:G,data:k,loading:O}),E>0&&jsxs(Wrap,{children:[jsx(WrapItem,{children:E>y.value&&jsx(Pagination,{pageCount:Math.ceil(E/y.value),forcePage:l,onPageChange:({selected:$})=>c($)})}),jsx(WrapItem,{flexGrow:1,justifyContent:{sm:"flex-start",md:"flex-end"},children:jsxs(HStack,{children:[jsx(Text,{whiteSpace:"nowrap",children:"Hi\u1EC3n th\u1ECB"}),jsx(Box,{width:200,children:jsx(Select$1,{menuPlacement:"top",value:y,options:tableDisplayRowsOptions,onChange:F})})]})})]}),r&&jsx(AddMockTestModal,{isOpen:r,onClose:W,mockTestId:T,isEditMockTest:S,listCenters:A,isMockTestOnline:e})]})},MockTestOnlinePage=()=>jsx(ListMockTests,{isMockTestOnline:!0});var index=Object.freeze({__proto__:null,[Symbol.toStringTag]:"Module",default:MockTestOnlinePage});const isModuleMode=getEnviroment("ZIM_BUILD_MODE")==="module",Forbidden403Page=loadable$2(()=>__vitePreload(()=>import("./index2.js"),["assets/index2.js","assets/vendor.js"])),NotFound404Page=loadable$2(()=>__vitePreload(()=>import("./index3.js"),["assets/index3.js","assets/vendor.js"]));function App(){const[e,r]=react.exports.useState(!0),{appSetLogin:n,appSetLogout:a,appState:l}=useAppContext(),c=useIsMounted(),{loaded:u}=l,d=()=>{};return react.exports.useEffect(()=>{if(!!c()){if(isModuleMode){n(getCookie("token")),Object.values(global.wsClients).forEach(m=>{m.connect()}),r(!1);return}fetchAccessToken().then(m=>{m===void 0||(m==null?void 0:m.token)===void 0?(a(),r(!1),Object.values(global.wsClients).forEach(A=>{A.close(!0,!0)})):(n(m==null?void 0:m.token),Object.values(global.wsClients).forEach(A=>{A.connect()}))}).catch(m=>{console.log({e:m}),a(),r(!1)}).finally(()=>{r(!1)})}},[c()]),jsx(ErrorBoundary,{FallbackComponent:ErrorFallback,onError:d,children:jsx(BrowserRouter,{children:e||!u?jsx(HStack,{w:"100%",h:"100vh",justifyContent:"center",alignItems:"center",children:jsx(Loading,{text:"\u0110ang l\u1EA5y d\u1EEF li\u1EC7u..."})}):isModuleMode?jsx(Routes,{children:jsx(Route,{path:"*",element:jsx(MockTestOnlinePage,{})})}):jsxs(Routes,{children:[routes.map((m,b)=>jsx(Route,{path:m.path,element:jsx(ProtectedRoute,{allowRoles:m.allowRoles,children:jsx(Layout,{children:jsx(m.component,{})})})},`${m.path}-${b}`)),jsx(Route,{path:"/auth/login",element:jsx(Login,{})}),jsx(Route,{path:"/error/403",element:jsx(Forbidden403Page,{})}),jsx(Route,{path:"/error/404",element:jsx(NotFound404Page,{})})]})})})}const colors={secondary:"#c89934",primary:"#3463c8",gray:{50:"#F8F8F8",100:"#F0F0F0",200:"#E6E6E6",300:"#D5D5D5",400:"#B0B0B0",500:"#909090",600:"#686868",700:"#555555",800:"#373737",900:"#171717"},secondBrand:{50:"#f8fae8",100:"#eff2c5",200:"#e5e9a0",300:"#dce07d",400:"#d6da66",500:"#d1d551",600:"#cfc449",700:"#ccae3f",800:"#c89934",900:"#c07722"},brand:{50:"#e4f2ff",100:"#bedeff",200:"#94caff",300:"#6bb5ff",400:"#4fa4ff",500:"#3463c8",600:"#2e44a8",700:"#243b9c",800:"#1b3190",900:"#081f7c"},pink:{50:"#f9e4ec",100:"#f2bcd0",200:"#eb92b1",300:"#e36993",400:"#dd4d7c",500:"#d93767",600:"#c83463",700:"#b2305d",800:"#9d2c58",900:"#77264c"},blue:{50:"#E1F2FC",100:"#B7DDFA",200:"#88C8F7",300:"#55B3F4",400:"#21A3F3",500:"#0093F1",600:"#0085E4",700:"#0074D1",800:"#0063BF",900:"#0045A1"},green:{50:"#E8F5E9",100:"#C8E6C9",200:"#A5D6A7",300:"#81C784",400:"#66BB6A",500:"#4CAF50",600:"#43A047",700:"#388E3C",800:"#2E7D32",900:"#1B5E20"},red:{50:"#FFEAED",100:"#FFCBD5",200:"#F1959E",300:"#E9686B",400:"#F54258",500:"#FC243E",600:"#EC163D",700:"#DA0037",800:"#CD002F",900:"#BF0022"},slate:{50:"#f8fafc",100:"#f1f5f9",200:"#e2e8f0",300:"#cbd5e1",400:"#94a3b8",500:"#64748b",600:"#475569",700:"#334155",800:"#1e293b",900:"#0f172a"},orange:{50:"#FFF4E2",100:"#FFE1B6",200:"#FFCE88",300:"#FFBA5A",400:"#FFAB3A",500:"#FF9C27",600:"#FF9125",700:"#FA8123",800:"#F37121",900:"#E9581E"}},Card={baseStyle:({colorMode:e})=>{const r=e==="light";return{display:"block",background:r?"white":"slate.800",gap:6,border:"1px solid",borderColor:r?"gray.200":"slate.700"}},variants:{rounded:{padding:8,borderRadius:"xl"},smooth:{padding:4,borderRadius:"base"}},defaultProps:{variant:"smooth"}},Table={baseStyle:({colorMode:e})=>{const r=e==="dark";return{table:{th:{background:r?"slate.700":"gray.50",lineHeight:1.5},td:{background:r?"slate.800":"white",lineHeight:1.5}}}},variants:{simple:({colorMode:e})=>{const n=e==="dark"?"slate.700":"gray.300";return{td:{borderBottomColor:n,paddingTop:2,paddingBottom:2,paddingInlineStart:4,paddingInlineEnd:4},th:{borderBottomColor:n,paddingInlineStart:4,paddingInlineEnd:4,paddingTop:2,paddingBottom:2}}}},sizes:{xs:({colorMode:e})=>({td:{paddingTop:"var(--chakra-space-1)",paddingBottom:"var(--chakra-space-1)",fontSize:"var(--chakra-fontSizes-xs)"},th:{paddingTop:"var(--chakra-space-1)",paddingBottom:"var(--chakra-space-1)",fontSize:"var(--chakra-fontSizes-xs)"}})},defaultProps:{variant:"simple",size:"sm"}},Badge={baseStyle:({colorMode:e})=>({textTransform:"capitalize",padding:"0.25rem 0.5rem ",borderRadius:4}),variants:{},sizes:{xs:{padding:"0.125rem 0.5rem",fontSize:"var(--chakra-fontSizes-xs)"}},defaultProps:{variant:"subtle",size:"sm"}},Input={baseStyle:{_invalid:{boxShadow:0}},variants:{outline:{display:"none"}},defaultProps:{variant:"outline"}},Link={baseStyle:({colorMode:e})=>{const r=e==="light";return{color:r?"blue.500":"blue.300",_hover:{textDecoration:"none",color:r?"blue.700":"blue.500"}}}},Modal={baseStyle:({colorMode:e})=>({dialog:{background:e==="light"?"white":"slate.800"}})},Drawer={baseStyle:({colorMode:e})=>({dialog:{background:e==="light"?"white":"slate.800"}})};var components={Card,Table,Badge,Input,Link,Modal,Drawer};const config={initialColorMode:"light",useSystemColorMode:!1},breakpoints={sm:"320px",md:"768px",lg:"960px",xl:"1280px","2xl":"1536px"},theme=extendTheme({config,colors,breakpoints,styles:{global:e=>({"html, body":{color:e.colorMode==="dark"?"gray.100":"gray.900",lineHeight:1.5},a:{color:e.colorMode==="dark"?"blue.300":"blue.500"},ul:{paddingRight:1}})},semanticTokens:{text:{default:"gray.900",_dark:"slate.400"},colors:{error:{default:"red.500",_dark:"red.200"},"gray.500":{default:"gray.500",_dark:"slate.400"}}},fonts:{body:"'Roboto', sans-serif",heading:"'Arial', sans-serif",mono:"monospace"},components:H(N({},components),{FormLabel:{baseStyle:{fontSize:"sm",marginBottom:1}},Avatar:{defaultProps:{colorScheme:"gray"}}})},withDefaultColorScheme({colorScheme:"gray",components:["Avatar"]}));ReactDOM.render(jsx(t.StrictMode,{children:jsx(ProvideAuth,{children:jsxs(ChakraProvider,{theme,children:[jsx(ColorModeScript,{initialColorMode:theme.config.initialColorMode}),jsx(App,{})]})})}),document.getElementById("root"));export{useUser as $,EMPTY_ERROR as A,EnumConfigSystemNames as B,Card$1 as C,DATE_OF_WEEKS as D,EnumCoursePlanerTypeEnum as E,TextInput as F,GET_COURSES as G,ACCOUNT_FRAGMENT as H,GET_COURSE_PLAN as I,GET_PERSONAL_INFO as J,GET_STUDENT_INVOICE as K,Loading as L,EnumInvoiceStatus as M,NavLink as N,OPEN_STATUSES as O,Pagination as P,AutoResizeTextarea as Q,RequestStudentToCourseTypeEnumeration as R,SKILLS as S,Title as T,GET_COURSE as U,GET_SCHOOLS as V,GET_SUBJECTS as W,GET_LEVELS as X,TicketCard as Y,TicketFragment as Z,TICKET_STATUS_LOWERCASE as _,useRouter as a,UPDATE_LEAD_EC as a$,ticketStatusOptions as a0,useResetQueries as a1,GROUP_ROLES as a2,ticketFilterTypeOptions as a3,TicketFormModal as a4,GET_LEAD_REPORT_DATA as a5,UserStatusEnum as a6,GET_STUDENT_SUPPORTED_BY_EC_FOR_DATA_TABLE as a7,userStatusOptions as a8,StyledTextContent as a9,__vitePreload as aA,GET_MOCK_TEST_DETAIL_BY_FIELD as aB,GET_MOCK_TEST_DETAIL as aC,EnumMockTestType as aD,groupByDate as aE,EnumStatusScheduleType as aF,GET_ACCOUNTS_BY_ROLE as aG,Wrapper as aH,EnumShiftsType as aI,useGetWindowDimensions as aJ,GET_FB_COMMENT as aK,GET_FB_POST as aL,LEAD_STATUS as aM,GET_LEAD_NOTE as aN,GET_LEADS as aO,UPDATE_LEAD_STATUS as aP,CREATE_LEAD_NOTE as aQ,GET_WARDS_BY_DISTRICT as aR,GET_DISTRICTS as aS,GET_CITIES as aT,GET_STREETS_BY_DISTRICT as aU,GET_JOBS as aV,GET_ECS as aW,REGEX_PHONE as aX,useListSources as aY,GET_USER_BY_PHONE as aZ,CREATE_APOINTMENT as a_,arrayToSelectOption as aa,useGetCenterByUser as ab,ListMockTests as ac,UPDATE_MOCK_TEST_ATTENDANCE_V2 as ad,GET_SUB_CAT_MOCK_TEST_FOR_STUDENT as ae,UPLOAD_EXAM_DOCUMENT as af,GET_IMAGE_MOCK_TEST_ORDER_BY_SUB_CAT as ag,UPLOAD_RESULT_STUDENT as ah,useDropzone as ai,ZimVnPaths as aj,ERROR_FILE_SIZE as ak,ERROR_FILE_NOT_SUPPORT as al,SENT_EMAIL_RESULT_FOR_STUDENT as am,GET_RESULT_MOCK_TEST_STUDENT as an,CALC_OVERALL as ao,UPDATE_RESULT_MOCK_TEST as ap,colors as aq,GET_LIST_TEACHER_OF_MOCK_TEST_WITH_SCHEDULE as ar,CREATE_MOCK_TEST_OF_STUDENT_SCHEDULE as as,OrderActionInput as at,MockTestStatusEnum as au,MOCK_TEST_PERMISSION as av,UPDATE_MOCK_TEST_ORDER_CORRECTING_SECTION as aw,MOCK_TEST_ORDERS_BY_ROLES as ax,subjectSpeakingId as ay,subjectLwrId as az,useBuildModeValue as b,useToggle as b0,LEAD_PERMISSION as b1,CreateLead as b2,useCombinedRefs as b3,EnumOrder as b4,GET_SUB_CAT_MOCK_TEST as b5,GET_MOCK_TEST_BANK as b6,useListSubjectsOfMockTest as b7,GET_SUBJECT_OF_MOCK_TEST_FOR_FILER as b8,tableDisplayRowsOptions as b9,GET_LIST_TEACHER_REGISTER_FOR_FUNCTION as bA,GET_LIST_EC_FOR_CENTER as bB,mockTestOnlineId as bC,mockTestOfflineId as bD,GET_LIST_TEACHER_ASSIGNED_MOCK_TEST as bE,MockTestSubjectsType as bF,UPDATE_MOCK_TEST_DETAIL_V2 as bG,GET_LIST_STUDENT_BY_EC_BY_FIELD as bH,REGEX_LINK as bI,ROOMS as bJ,GET_LIST_MOCK_TEST_BANK as ba,blankStateCreateExercise as bb,CREATE_EXERCISE as bc,ExerciseOptions as bd,ExerciseStatus as be,GET_EXERCISE_PAGINATION as bf,GET_EXERCISE_TYPES as bg,GET_GRADES as bh,GET_LESSON_BY_CONFIG as bi,GET_SUB_CAT_BY_CAT_ID as bj,GET_QUESTION_TYPE as bk,GET_QUESTION_GROUP_ID as bl,UPLOAD_S3 as bm,SubfolderS3Enum as bn,useActiveGroupStore as bo,CREATE_QUESTION_GROUP as bp,UPDATE_QUESTION_GROUP as bq,TipTap$1 as br,questionTypeNoAnswerQuestion as bs,TableQuestion as bt,REMOVE_QUESTION_GROUP as bu,useDebounce as bv,StatusExerciseGrapNet as bw,GET_EXERCISE_DETAIL as bx,UPDATE_STATUS_EXERCISE as by,UPDATE_EXERCISE as bz,appZimPaths as c,routePaths as d,SearchInput as e,CourseFilter as f,DataTable as g,Select$1 as h,dateAddTime as i,LoadingAnimated as j,DateTimePicker$1 as k,ErrorFallback as l,EnumCourseTypeEnum as m,COURSE_STATUSES as n,GET_ROOMS as o,GET_ECS_BY_SCHOOL_ID as p,GET_CURRICULUMS as q,routes as r,GET_SHIFTS as s,GET_SIZES as t,useIsMounted as u,GET_TEACHERS as v,GET_SUB_CATS as w,StatusEnum as x,EnumLevelStatus as y,GET_COURSE_FEE_SUGGESTION as z};
