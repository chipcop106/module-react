var dt=Object.defineProperty,Et=Object.defineProperties;var ht=Object.getOwnPropertyDescriptors;var Ze=Object.getOwnPropertySymbols;var Yn=Object.prototype.hasOwnProperty,Kn=Object.prototype.propertyIsEnumerable;var Jn=(s,e,u)=>e in s?dt(s,e,{enumerable:!0,configurable:!0,writable:!0,value:u}):s[e]=u,r=(s,e)=>{for(var u in e||(e={}))Yn.call(e,u)&&Jn(s,u,e[u]);if(Ze)for(var u of Ze(e))Kn.call(e,u)&&Jn(s,u,e[u]);return s},y=(s,e)=>Et(s,ht(e));var Ge=(s,e)=>{var u={};for(var i in s)Yn.call(s,i)&&e.indexOf(i)<0&&(u[i]=s[i]);if(s!=null&&Ze)for(var i of Ze(s))e.indexOf(i)<0&&Kn.call(s,i)&&(u[i]=s[i]);return u};import{a2 as R,av as T,h as p,g as C,m as E,U as tn,j as t,X as sn,_ as pt,Y as _t,u as X,e8 as mt,Z as gt,e9 as bt,e2 as x,e4 as Ee,e5 as Ct,d1 as yt,ay as Xn,d3 as Qn,F as he,B as v,V as Wn,w as pe,ce as A,af as un,b4 as fe,H as _e,L as Ue,b$ as xt,aw as Vn,d0 as S,q as g,ap as on,dh as Fe,d5 as vt,d6 as At,bE as St,cu as ft,a0 as an,aK as Ft,aB as $t,aC as Tt,aD as Lt,aE as It,aF as Ot,aG as Dt,ea as wt,eb as Ye}from"./vendor.js";import{E as oe,m as ln,n as rn,o as kt,p as Pt,q as Nt,s as Rt,t as Bt,v as Mt,w as Gt,x as Ke,y as et,z as Ut,A as L,D as qt,B as Ht,h as w,k as zt,F as cn,l as jt,H as Je,I as Zt,J as Yt,K as Kt,b as $e,c as Te,d as ae,g as nt,R as le,M as Jt,N as Xt,Q as Qt,L as Wt}from"./index.js";import{u as tt}from"./useListSchools.js";import{u as Vt,a as it}from"./useListLevels.js";const qe=(s,e=",")=>{let u=/\D+/g,i=(s+"").replace(u,"");return(Number(i)+"").replace(/(\d)(?=(\d{3})+(?!\d))/g,`$1${e}`)},dn=s=>Number(s.replaceAll(",",".").replaceAll(".","")),zi=[{label:"Ch\xEDnh th\u1EE9c",value:oe.Course},{label:"D\u1EF1 ki\u1EBFn",value:oe.CoursePlan}],He=[{label:"Online",value:ln.Online},{label:"Classroom",value:ln.ClassRoom}],ji=[{label:"In coming",value:rn.INCOMING},{label:"On coming",value:rn.ONGOING},{label:"Closed",value:rn.CLOSED}],ei=[{label:"Th\u1EE9 2",value:1},{label:"Th\u1EE9 3",value:2},{label:"Th\u1EE9 4",value:3},{label:"Th\u1EE9 5",value:4},{label:"Th\u1EE9 6",value:5},{label:"Th\u1EE9 7",value:6},{label:"Ch\u1EE7 nh\u1EADt",value:0}],ni=[{id:1,label:"3 bu\u1ED5i/tu\u1EA7n",value:1},{id:2,label:"4-6 bu\u1ED5i/tu\u1EA7n",value:2},{id:3,label:"Tr\xEAn 6 bu\u1ED5i/tu\u1EA7n",value:3}],Zi=[{label:"5 rows",value:5},{label:"10 rows",value:10},{label:"15 rows",value:15},{label:"20 rows",value:20},{label:"25 rows",value:25},{label:"30 rows",value:30},{label:"50 rows",value:50},{label:"100 rows",value:100}],ti=(s,e={})=>{const u=T();return R(kt,r({variables:r({status:"active"},s),onError:i=>{u({title:"L\u1EA5y d\u1EEF li\u1EC7u th\u1EA5t b\u1EA1i.",description:`GET list rooms status error: ${i==null?void 0:i.message}`,status:"error",duration:3e3,isClosable:!0})},fetchPolicy:"cache-first",errorPolicy:"all"},e))},ii=(s={},e={})=>{const u=T();return R(Pt,r({variables:s,onError:i=>{u({title:"L\u1EA5y d\u1EEF li\u1EC7u th\u1EA5t b\u1EA1i.",description:`GET list EC status error: ${i==null?void 0:i.message}`,status:"error",duration:3e3,isClosable:!0})},fetchPolicy:"cache-first",errorPolicy:"all"},e))},si=(s,e={})=>{const u=T();return R(Nt,r({variables:s,onError:i=>{u({title:"L\u1EA5y d\u1EEF li\u1EC7u th\u1EA5t b\u1EA1i.",description:`GET list curriculums status error: ${i==null?void 0:i.message}`,status:"error",duration:3e3,isClosable:!0})},fetchPolicy:"cache-first",errorPolicy:"all"},e))},ui=(s,e={})=>{const u=T();return R(Rt,r({variables:s,onError:i=>{u({title:"L\u1EA5y d\u1EEF li\u1EC7u th\u1EA5t b\u1EA1i.",description:`GET list shifts status error: ${i==null?void 0:i.message}`,status:"error",duration:3e3,isClosable:!0})},fetchPolicy:"cache-first",errorPolicy:"all"},e))},oi=(s,e={})=>{const u=T();return R(Bt,r({variables:s,onError:i=>{u({title:"L\u1EA5y d\u1EEF li\u1EC7u th\u1EA5t b\u1EA1i.",description:`GET list sizes status error: ${i==null?void 0:i.message}`,status:"error",duration:3e3,isClosable:!0})},fetchPolicy:"cache-first",errorPolicy:"all"},e))},re=s=>({hour:parseInt(s.split(":")[0]),minute:parseInt(s.split(":")[1])}),ai=(s,e)=>{let u=!1;if(!e||!s)return u;const i=new Date().setHours(re(s.start_time).hour,re(s.start_time).minute),a=new Date().setHours(re(s.end_time).hour,re(s.end_time).minute);return e&&e.map(d=>{const f=new Date().setHours(re(d.start_time).hour,re(d.start_time).minute),b=new Date().setHours(re(d.end_time).hour,re(d.end_time).minute);(f<=i&&i<=b||f<=a&&a<=b)&&(u=!0)}),u},li=(s,e={})=>{const u=T();return R(Mt,r({variables:r({},s),onError:i=>{u({title:"L\u1EA5y d\u1EEF li\u1EC7u th\u1EA5t b\u1EA1i.",description:`GET list teachers status error: ${i==null?void 0:i.message}`,status:"error",duration:3e3,isClosable:!0})},fetchPolicy:"cache-first",errorPolicy:"all"},e))},ri=(s={},e={})=>{const u=T();return R(Gt,r({variables:s,onError:i=>{u({title:"L\u1EA5y d\u1EEF li\u1EC7u th\u1EA5t b\u1EA1i.",description:`GET sub cats error: ${i==null?void 0:i.message}`,status:"error",duration:3e3,isClosable:!0})},fetchPolicy:"cache-first",errorPolicy:"all"},e))},ci=({schoolWatch:s,subjectWatch:e,shiftsWatch:u,levelInWatch:i,levelOutWatch:a,skillsWatch:d,typeWatch:f,configFeeWatch:b},m,F)=>{var c,D,H,z;const{data:Q,loading:ee}=tt(),{data:O,loading:j}=Vt({status:Ke.Active}),{data:o,loading:U}=ti({status:"active",school_id:s==null?void 0:s.value},{skip:!(s==null?void 0:s.value)}),{data:Z,loading:Ie}=ii({schoolId:s==null?void 0:s.value},{skip:!(s==null?void 0:s.value)}),{data:k,loading:W}=li({input:{type:(c=f==null?void 0:f.value)!=null?c:ln.All,page:1,limit:99999}}),{data:ne,loading:Oe}=si({status:Ke.Active}),{data:_,loading:ce}=ui({status:Ke.Active}),{data:Y,loading:be}=ri({subject_id:(D=e==null?void 0:e.value)!=null?D:0},{onCompleted:({subcatsBySubjectId:l})=>{if(l)if(d){const ie=l.filter(K=>d.find(V=>V.value===K.id));if(ie.length>0){const K=b.filter(V=>ie.find(Re=>Re.id===V.detail_id));F("skills",d),F("config_fee",K)}else F("skills",[]),F("config_fee",[])}else F("levelIn",[])},skip:!(e==null?void 0:e.value)}),{data:q,loading:de}=oi({status:Ke.Active}),{data:M,loading:te}=it({subject_id:(H=e==null?void 0:e.value)!=null?H:0,graduation:0,type:"in",status:et.Active},{onCompleted:({levels:l})=>{l&&(i&&l.find(K=>K.id===i.value)?F("levelIn",i):F("levelIn",null))},skip:!(e==null?void 0:e.value)}),{data:G,loading:De}=it({subject_id:(z=e==null?void 0:e.value)!=null?z:0,graduation:0,type:"out",status:et.Active},{onCompleted:({levels:l})=>{l&&(a&&l.find(K=>K.id===a.value)?F("levelOut",a):F("levelOut",null))},skip:!(e==null?void 0:e.value)}),we=p.exports.useMemo(()=>O&&O.subjects?O.subjects.map(l=>({label:l.name,value:l.id})):[],[O]),Ce=p.exports.useMemo(()=>o&&o.rooms?o.rooms.map(l=>({label:l.name,value:l.id})):[],[o]),ke=p.exports.useMemo(()=>Z&&Z.getListEcForCenter?Z.getListEcForCenter.map(l=>({label:l.full_name,value:l.id})):[],[Z]),P=p.exports.useMemo(()=>k&&k.getListTeacherPagination?k.getListTeacherPagination.docs.map(l=>({label:l.full_name,value:l.id})):[],[k]),Pe=p.exports.useMemo(()=>ne&&ne.curriculums?ne.curriculums.map(l=>({label:l.name,value:l.id})):[],[ne]),ye=p.exports.useMemo(()=>q&&q.sizes?q.sizes.map(l=>({label:l.size,value:l.id})):[],[q]),B=p.exports.useMemo(()=>_&&_.shifts?_.shifts.map(l=>((_==null?void 0:_.shifts)&&ai(l,u),y(r({},l),{label:`${l==null?void 0:l.start_time} - ${l==null?void 0:l.end_time}`,value:l.id,isDisabled:!1}))):[],[_,u]),Ne=p.exports.useMemo(()=>M&&M.levels?M.levels.map(l=>({label:l.graduation,value:l.id})):[],[M]),xe=p.exports.useMemo(()=>G&&G.levels?G.levels.map(l=>({label:l.graduation,value:l.id})):[],[G]),ve=p.exports.useMemo(()=>Q&&Q.schools?Q.schools.map(l=>({label:l.name,value:l.id})):[],[Q]);return{options:{subcatOptions:p.exports.useMemo(()=>Y&&Y.subcatsBySubjectId?Y.subcatsBySubjectId.map(l=>({label:l.name,value:l.id})):[],[Y]),schoolOptions:ve,levelOutOptions:xe,levelInOptions:Ne,shiftOptions:B,sizeOptions:ye,curriculumOptions:Pe,ecOptions:ke,roomOptions:Ce,subjectOptions:we,teacherOptions:P},loadings:{ecLoading:Ie,levelOutLoading:De,levelInLoading:te,subcatsLoading:be,subjectLoading:j,roomLoading:U,sizeLoading:de,curriculumLoading:Oe,shiftLoading:ce,schoolLoading:ee,teacherLoading:W}}},di=C`
  query getConfigSystemByName($name: EnumConfigSystemNames)
  @api(name: "appZim") {
    getConfigSystemByName(name: $name) {
      name
      value
      description
    }
  }
`,Ei=(s,e={})=>{const u=T();return R(di,r({variables:s,onError:i=>{var a;u({title:"L\u1EA5y d\u1EEF li\u1EC7u th\u1EA5t b\u1EA1i.",description:`GET config ${(a=s==null?void 0:s.name)!=null?a:""} status error: ${i==null?void 0:i.message}`,status:"error",duration:3e3,isClosable:!0})},fetchPolicy:"network-only",errorPolicy:"all"},e))},hi=p.exports.forwardRef((i,u)=>{var a=i,{addonValue:s}=a,e=Ge(a,["addonValue"]);var d;return E(tn,{isInvalid:e.isInvalid,children:[t(sn,{htmlFor:e.id,fontSize:"sm",color:"gray.500",children:e.label}),E(pt,{children:[t(_t,y(r({bg:X("white","slate.700"),borderColor:X("gray.300","slate.600"),_hover:{borderColor:X("gray.400","slate.700")},color:X("gray.900","slate.300"),_placeholder:{color:X("gray.400","slate.500")}},e),{ref:u})),t(mt,{flexGrow:1,children:s})]}),t(gt,{children:(d=e==null?void 0:e.error)==null?void 0:d.message})]})}),pi=(s={},e={})=>{const u=T();return R(Ut,r({variables:s,onError:i=>{var a;u({title:"L\u1EA5y d\u1EEF li\u1EC7u th\u1EA5t b\u1EA1i.",description:(a=i==null?void 0:i.message)!=null?a:"L\u1EA5y d\u1EEF li\u1EC7u kh\xF4ng th\xE0nh c\xF4ng",status:"error",duration:3e3,isClosable:!0})},notifyOnNetworkStatusChange:!0},e))},me=(s,e)=>s?s.toString().replace(/\B(?=(\d{3})+(?!\d))/g,e||"."):"0",ge=7,I=u=>{var i=u,{children:s}=i,e=Ge(i,["children"]);return t(v,y(r({p:2,width:{base:"100%",md:"33%",sm:"50%"}},e),{children:s}))},Le=i=>{var a=i,{children:s,title:e}=a,u=Ge(a,["children","title"]);return t(v,y(r({w:"full",borderBottom:"1px solid",borderBottomColor:X("gray.500","slate.500"),pos:"relative"},u),{children:t(v,{mx:-2,pb:4,children:s})}))},_i=({onSubmit:s,defaultValues:e,isCreateMode:u=!0,courseType:i=oe.Course,formRef:a})=>{var hn,pn,_n,mn,gn,bn,Cn,yn,xn,vn,An,Sn,fn,Fn,$n,Tn,Ln,In,On,Dn,wn,kn,Pn,Nn,Rn,Bn;const d=i===oe.Course,f=p.exports.useMemo(()=>x().shape({school:x().nullable(!0).required(L("School")),rooms:d?Ee().of(x()).required(L("Rooms")).min(1,L("Rooms")).nullable(!0):Ee().nullable(!0),levelIn:x().nullable(!0).when("subject",(n,$)=>n&&(n==null?void 0:n.value)===ge?x().nullable(!0):x().nullable(!0).required(L("Level out"))),levelOut:x().nullable(!0).when("subject",(n,$)=>n&&(n==null?void 0:n.value)===ge?x().nullable(!0):x().nullable(!0).required(L("Level out"))),shifts:Ee().of(x()).required(L("Shifts")).min(1,L("Shifts")).nullable(!0),skills:x().nullable(!0).when("subject",(n,$)=>n&&(n==null?void 0:n.value)===ge?Ee().of(x()).nullable(!0):Ee().of(x()).required(L("Skills")).min(1,L("Skills")).nullable(!0)),dayOfWeeks:Ee().of(x()).required(L("Day Study")).min(1,L("Day Study")).nullable(!0),subject:x().nullable(!0).required(L("Subject")),curriculum:x().nullable(!0).required(L("Curriculum")),dateOpen:Ct().nullable(!0).required(L("Date open")),size:x().nullable(!0).required(L("Size")),fee:yt().required(),type:x().nullable(!0).required(L("Course type")),ec:d?x().nullable(!0).required(L("EC")):x().nullable(!0),teachers:Ee().of(x()).required(L("Teachers")).min(1,L("Teachers")).nullable(!0),coefficientRange:x().nullable(!0).required(L("Coefficient"))}),[i]),{handleSubmit:b,control:m,setValue:F,getValues:Q,trigger:ee,register:O,resetField:j,formState:{errors:o},watch:U,setError:Z}=Xn({resolver:Qn(f),defaultValues:{subject:(e==null?void 0:e.subject)&&{label:(hn=e.subject)==null?void 0:hn.name,value:(pn=e.subject)==null?void 0:pn.id},program:(e==null?void 0:e.program)&&{label:(_n=e.program)==null?void 0:_n.name,value:(mn=e.program)==null?void 0:mn.id},rooms:(e==null?void 0:e.rooms)&&e.rooms.map(n=>({label:n==null?void 0:n.name,value:n==null?void 0:n.id})),size:(e==null?void 0:e.size)&&{label:(gn=e.size)==null?void 0:gn.name,value:(bn=e.size)==null?void 0:bn.id},shifts:(e==null?void 0:e.shifts)&&(e==null?void 0:e.shifts.map(n=>y(r({},n),{label:`${n==null?void 0:n.start_time} - ${n==null?void 0:n.end_time}`,value:n==null?void 0:n.id}))),skills:(e==null?void 0:e.subcats)&&e.subcats.map(n=>({label:n==null?void 0:n.name,value:n==null?void 0:n.id})),curriculum:(e==null?void 0:e.curriculum)&&{label:(Cn=e.curriculum)==null?void 0:Cn.name,value:(yn=e.curriculum)==null?void 0:yn.id},school:(e==null?void 0:e.school)&&{label:(xn=e.school)==null?void 0:xn.name,value:(vn=e.school)==null?void 0:vn.id},dayOfWeeks:(e==null?void 0:e.day_of_weeks)&&e.day_of_weeks.map(n=>({label:qt[n.id].full,value:n==null?void 0:n.id})),ec:(e==null?void 0:e.ec)&&{label:(An=e.ec)==null?void 0:An.full_name,value:(Sn=e.ec)==null?void 0:Sn.id},levelIn:(e==null?void 0:e.level_in)&&{label:(fn=e.level_in)==null?void 0:fn.graduation,value:(Fn=e.level_in)==null?void 0:Fn.id},levelOut:(e==null?void 0:e.level_out)&&{label:($n=e.level_out)==null?void 0:$n.graduation,value:(Tn=e.level_out)==null?void 0:Tn.id},teachers:(e==null?void 0:e.teachers)&&e.teachers.map(n=>({label:n==null?void 0:n.full_name,value:n==null?void 0:n.id})),dateOpen:(e==null?void 0:e.start_date)?new Date(e==null?void 0:e.start_date):null,fee:(Ln=e==null?void 0:e.fee)!=null?Ln:"",config_fee:(e==null?void 0:e.config_fee)&&e.config_fee.map(n=>y(r({},n),{label:n==null?void 0:n.detail_name,value:n==null?void 0:n.detail_id})),type:(e==null?void 0:e.type)?He.find(n=>n.value.toLowerCase()===e.type.toLowerCase()):(In=He==null?void 0:He[1])!=null?In:null,coefficientRange:(e==null?void 0:e.coefficient_range)&&{label:(On=e.coefficient_range)==null?void 0:On.name,value:(Dn=e.coefficient_range)==null?void 0:Dn.id},config_fee_month:(e==null?void 0:e.config_fee)&&(kn=(wn=e==null?void 0:e.config_fee.find(n=>(n==null?void 0:n.type)==="forMonth"))==null?void 0:wn.detail_id)!=null?kn:1}}),Ie=T(),[k,W,ne,Oe,_,ce,Y,be,q,de,M,te,G,De,we]=U(["school","curriculum","program","skills","subject","shifts","fee","config_fee_month","teachers","size","coefficientRange","type","config_fee","levelIn","levelOut"]),{options:{subcatOptions:Ce,schoolOptions:ke,levelOutOptions:P,levelInOptions:Pe,shiftOptions:ye,sizeOptions:B,curriculumOptions:Ne,ecOptions:xe,roomOptions:ve,subjectOptions:h,teacherOptions:c},loadings:{ecLoading:D,levelOutLoading:H,levelInLoading:z,subcatsLoading:l,subjectLoading:ie,roomLoading:K,sizeLoading:V,curriculumLoading:Re,shiftLoading:Xe,schoolLoading:J,teacherLoading:Qe}}=ci({schoolWatch:k,subjectWatch:_,shiftsWatch:ce,levelInWatch:De,levelOutWatch:we,skillsWatch:Oe,typeWatch:te,configFeeWatch:G},j,F),{data:Ae,loading:Bi}=Ei({name:Ht.PercentageForFeeByMonth}),{data:We,loading:ot}=pi({input:{coefficient_range_id:(Pn=M==null?void 0:M.value)!=null?Pn:0,curriculum_id:(Nn=W==null?void 0:W.value)!=null?Nn:0,subject_id:(Rn=_==null?void 0:_.value)!=null?Rn:0,size_id:(Bn=de==null?void 0:de.value)!=null?Bn:0,shift_ids:ce?ce.map(n=>n.value):[],teacher_ids:q?q.map(n=>n.value):[],type:te==null?void 0:te.value}},{fetchPolicy:"network-only",skip:!M||!W||!_||!de||!ce||!q||!te}),Be=Ae==null?void 0:Ae.getConfigSystemByName,je=We==null?void 0:We.getCourseFeeSuggestion,at=p.exports.useCallback(n=>n?n.isDisabled:!1,[]),lt=p.exports.useCallback(n=>{let $=dn(qe(Y)+""),se=isNaN(Number(n))?0:Number(n);se<=0&&(se=1);let Me=Be?parseInt(Be==null?void 0:Be.value):0,ue=$;return se>1&&(ue=$*(100+Me)/100),Math.round(ue/se/1e3)*1e3},[be,Y,Be]),rt=async n=>{var $,se,Me,ue,Mn,Gn,Un;try{let Se=!1,Ve=[];const qn=n.config_fee?n.config_fee.filter(N=>N.type=="forSubcat"):[],en=je/qn.length,Hn=dn(n.fee);if(qn.map(N=>{var jn,Zn;const zn=dn((jn=n["config_fee_subcat_"+N.detail_id])!=null?jn:0);((Zn=n.subject)==null?void 0:Zn.value)!==ge&&zn<en&&(Z(`config_fee_subcat_${N.detail_id}`,{type:"custom",message:`Ph\xED t\u1ED1i thi\u1EC3u l\xE0: ${en?me(en):0}`}),Se=!0),Ve.push({detail_id:N.value,type:"forSubcat",detail_name:N.label,fee:zn})}),Ve.push({type:"forMonth",detail_id:Number(n.config_fee_month),fee:0,detail_name:"Fee For Month"}),je>Hn){Z("fee",{type:"custom",message:"H\u1ECDc ph\xED kh\xF4ng \u0111\u01B0\u1EE3c th\u1EA5p h\u01A1n h\u1ECDc ph\xED \u0111\u1EC1 xu\u1EA5t"});return}if(Se)return;const nn={fee:Hn,config_fee:Ve,school_id:n==null?void 0:n.school.value,teacher_ids:(n==null?void 0:n.teachers)?n.teachers.map(N=>N.value):[],courseType:n.type.value,subcat_ids:(n==null?void 0:n.skills)?n.skills.map(N=>N.value):[],ec_id:(se=($=n==null?void 0:n.ec)==null?void 0:$.value)!=null?se:null,day_of_week_ids:(n==null?void 0:n.dayOfWeeks)?n.dayOfWeeks.map(N=>N.value):[],level_in_id:(ue=(Me=n==null?void 0:n.levelIn)==null?void 0:Me.value)!=null?ue:0,level_out_id:(Gn=(Mn=n==null?void 0:n.levelOut)==null?void 0:Mn.value)!=null?Gn:0,room_ids:(n==null?void 0:n.rooms)?(Un=n==null?void 0:n.rooms)==null?void 0:Un.map(N=>N.value):[],shift_ids:(n==null?void 0:n.shifts)?n.shifts.map(N=>N.value):[],start_date:un(n.dateOpen).toISOString(),size_id:n.size.value,coefficient_range_id:n.coefficientRange.value,curriculum_id:n.curriculum.value,subject_id:n.subject.value};i===oe.CoursePlan&&(delete nn.ec_id,delete nn.room_ids),s(nn)}catch(Se){Ie({title:"Failed",description:Se==null?void 0:Se.message,status:"error",duration:3e3,isClosable:!0})}},ct=p.exports.useCallback(n=>{F("skills",n),F("config_fee",n.map($=>y(r({},$),{detail_id:$.value,type:"forSubcat",fee:0,detail_name:$.label})))},[]);return t(he,{children:t(v,{children:t("form",{onSubmit:b(rt),ref:a,children:E(Wn,{spacing:4,children:[t(Le,{title:"\u0110\u1ECBa \u0111i\u1EC3m",children:E(pe,{flexWrap:"wrap",w:"full",pb:{base:4,md:0},children:[t(I,{width:{base:"100%",md:"50%"},children:t(A,{name:"school",control:m,render:({field:n})=>t(w,r({label:"Trung t\xE2m",options:ke,isSearchable:!0,isLoading:J,name:"school",placeholder:"Ch\u1ECDn trung t\xE2m",id:"school",error:o==null?void 0:o.school,isDisabled:!u},n))})}),d&&t(I,{width:{base:"100%",md:"50%"},children:t(A,{name:"rooms",control:m,render:({field:n})=>t(w,y(r({label:"Ph\xF2ng h\u1ECDc",options:ve,isSearchable:!0,isLoading:K,name:"rooms",placeholder:"Ch\u1ECDn ph\xF2ng",id:"rooms",error:o==null?void 0:o.rooms},n),{isMulti:!0}))})})]})}),t(Le,{title:"Th\xF4ng tin kh\xF3a",children:E(pe,{flexWrap:"wrap",w:"full",pb:{base:4,md:0},children:[t(I,{width:{base:"100%",md:"50%",lg:"25%"},children:t(A,{name:"type",control:m,render:({field:n})=>t(w,r({label:"Lo\u1EA1i kh\xF3a h\u1ECDc",options:He,isSearchable:!0,isLoading:!1,name:"type",placeholder:"Ch\u1ECDn lo\u1EA1i kh\xF3a h\u1ECDc",id:"type",error:o==null?void 0:o.type,isDisabled:!u},n))})}),t(I,{width:{base:"100%",md:"50%",lg:"25%"},children:t(A,{name:"size",control:m,render:({field:n})=>t(w,r({label:"S\u0129 s\u1ED1",options:B,isSearchable:!0,isLoading:V,name:"size",placeholder:"Ch\u1ECDn s\u1ED1 l\u01B0\u1EE3ng",id:"size",error:o==null?void 0:o.size,noOptionsMessage:()=>"Kh\xF4ng c\xF3 s\u1ED1 l\u01B0\u1EE3ng ph\xF9 h\u1EE3p",isDisabled:!u},n))})}),t(I,{width:{base:"100%",md:"50%"},children:t(A,{name:"subject",control:m,render:({field:n})=>t(w,y(r({label:"M\xF4n h\u1ECDc",options:h,isSearchable:!0,isLoading:ie,name:"subject",placeholder:"Ch\u1ECDn m\xF4n h\u1ECDc",id:"program",error:o==null?void 0:o.subject},n),{isDisabled:!u}))})}),t(I,{width:{base:"100%",md:"50%"},children:t(A,{name:"curriculum",control:m,render:({field:n})=>t(w,y(r({label:"L\u1ED9 tr\xECnh",options:Ne,isSearchable:!0,isLoading:Re,name:"curriculum",placeholder:"Ch\u1ECDn l\u1ED9 tr\xECnh",id:"curriculum",error:o==null?void 0:o.curriculum},n),{isDisabled:!u}))})}),(_==null?void 0:_.value)!==ge&&t(I,{width:{base:"100%",md:"50%"},children:t(A,{name:"skills",control:m,render:({field:n})=>t(w,y(r({label:"K\u0129 n\u0103ng",options:Ce,isSearchable:!0,isLoading:l,name:"skills",placeholder:"Ch\u1ECDn k\u0129 n\u0103ng",id:"skills",error:o==null?void 0:o.skills},n),{isMulti:!0,isDisabled:!u,onChange:ct}))})})]})}),(_==null?void 0:_.value)!==ge&&t(Le,{title:"Tr\xECnh \u0111\u1ED9",children:E(pe,{flexWrap:"wrap",w:"full",pb:{base:4,md:0},children:[t(I,{width:{base:"100%",md:"50%"},children:t(A,{name:"levelIn",control:m,render:({field:n})=>t(w,r({label:"\u0110\u1EA7u v\xE0o",options:Pe,isSearchable:!0,isLoading:z,name:"levelIn",placeholder:"Ch\u1ECDn \u0111\u1EA7u v\xE0o",id:"levelIn",error:o==null?void 0:o.levelIn},n))})}),t(I,{width:{base:"100%",md:"50%"},children:t(A,{name:"levelOut",control:m,render:({field:n})=>t(w,r({label:"\u0110\u1EA7u ra",options:P,isSearchable:!0,isLoading:H,name:"levelOut",placeholder:"Ch\u1ECDn \u0111\u1EA7u ra",id:"levelOut",error:o==null?void 0:o.levelOut},n))})})]})}),t(Le,{title:"Th\u1EDDi gian",children:E(pe,{flexWrap:"wrap",w:"full",pb:{base:4,md:0},children:[t(I,{width:{base:"100%",md:"50%"},children:t(A,{name:"dateOpen",control:m,render:Me=>{var ue=Me,{field:{onChange:n,value:$}}=ue,se=Ge(ue,["field"]);return t(zt,{label:"Ng\xE0y m\u1EDF",selected:$,onChange:n,"data-value":$,autoComplete:"off",dateFormat:"dd/MM/yyyy",placeholderText:"Ng\xE0y b\u1EAFt \u0111\u1EA7u",disabled:!u,minDate:new Date,error:o==null?void 0:o.dateOpen})}})}),t(I,{width:{base:"100%",md:"50%"},children:t(A,{name:"shifts",control:m,render:({field:n})=>t(w,r({label:"Ca h\u1ECDc",options:ye,isSearchable:!0,isLoading:Xe,name:"shifts",placeholder:"Ch\u1ECDn ca h\u1ECDc",id:"shifts",error:o==null?void 0:o.shifts,isMulti:!0,isOptionDisabled:at},n))})}),t(I,{width:{base:"100%",md:"50%"},children:t(A,{name:"dayOfWeeks",control:m,render:({field:n})=>t(w,y(r({label:"Ng\xE0y h\u1ECDc",options:ei,isSearchable:!0,isLoading:!1,placeholder:"Ch\u1ECDn ng\xE0y h\u1ECDc",id:"day_of_week",error:o==null?void 0:o.dayOfWeeks},n),{isMulti:!0}))})}),t(I,{width:{base:"100%",md:"50%"},children:t(A,{name:"coefficientRange",control:m,render:({field:n})=>t(w,r({label:"C\u01B0\u1EDDng \u0111\u1ED9",options:ni,isSearchable:!0,isLoading:!1,name:"coefficientRange",placeholder:"Ch\u1ECDn",id:"coefficientRange",error:o==null?void 0:o.coefficientRange},n))})})]})}),t(Le,{title:"Nh\xE2n s\u1EF1",children:E(pe,{flexWrap:"wrap",w:"full",pb:{base:4,md:0},children:[d&&t(I,{width:{base:"100%",md:"50%"},children:t(A,{name:"ec",control:m,render:({field:n})=>t(w,r({label:"EC ch\u1EE7 nhi\u1EC7m",options:xe,isSearchable:!0,isLoading:D,name:"ec",placeholder:"Ch\u1ECDn EC",id:"ec",error:o==null?void 0:o.ec,noOptionsMessage:()=>k?"Vui l\xF2ng ch\u1ECDn trung t\xE2m tr\u01B0\u1EDBc":"Kh\xF4ng c\xF3 EC ph\xF9 h\u1EE3p"},n))})}),t(I,{width:{base:"100%",md:"50%"},children:t(A,{name:"teachers",control:m,render:({field:n})=>t(w,y(r({label:"Gi\u1EA3ng vi\xEAn d\u1EF1 ki\u1EBFn",options:c,isSearchable:!0,isLoading:Qe,placeholder:"Ch\u1ECDn gi\u1EA3ng vi\xEAn",id:"teachers",error:o==null?void 0:o.teachers,isMulti:!0},n),{isDisabled:!u&&d}))})})]})}),t(Le,{title:"H\u1ECDc ph\xED",borderBottom:0,children:E(pe,{flexWrap:"wrap",w:"full",children:[t(I,{width:{base:"100%",md:"50%",xl:"25%"},children:t(A,{control:m,render:({field:n})=>{var $;return t(cn,y(r({label:"H\u1ECDc ph\xED",placeholder:"Nh\u1EADp h\u1ECDc ph\xED",id:"fee"},n),{style:{boxShadow:"0 0 0 0px #f44336"},error:o==null?void 0:o.fee,isInvalid:!!(o==null?void 0:o.fee),value:qe(($=n==null?void 0:n.value)!=null?$:0)}))},name:"fee"})}),t(I,{width:{base:"100%",md:"50%",xl:"25%"},children:t(cn,{label:"H\u1ECDc ph\xED \u0111\u1EC1 xu\u1EA5t",placeholder:"H\u1ECDc ph\xED \u0111\u1EC1 xu\u1EA5t",id:"suggestion-fee",style:{boxShadow:"0 0 0 0px #f44336"},error:o==null?void 0:o.fee_suggestion,isInvalid:!!(o==null?void 0:o.fee_suggestion),value:ot?"\u0110ang \u0111\u1EC1 xu\u1EA5t...":je?`${qe(je)}`:"0",isReadOnly:!0})}),t(I,{flex:1,minW:{base:"50%",md:"50%"},maxW:{base:"100%",md:"50%"},children:t(hi,y(r({label:"H\u1ECDc ph\xED theo th\xE1ng",placeholder:"Nh\u1EADp th\xE1ng",id:"config_fee_month",style:{boxShadow:"0 0 0 0px #f44336"},error:o==null?void 0:o.config_fee_month,isInvalid:!!(o==null?void 0:o.config_fee_month)},O("config_fee_month",{required:!0})),{addonValue:qe(lt(be))+" \u0111/th\xE1ng",width:125}))}),(_==null?void 0:_.value)!==ge&&t(pe,{px:{base:0,md:4},flexGrow:1,mx:-4,flexWrap:"wrap",w:"full",children:G&&(G==null?void 0:G.filter(n=>n.type=="forSubcat").map(n=>{const $=O(`config_fee_subcat_${n.detail_id}`,{required:!0});return t(I,{flex:1,minW:{base:"100%",sm:"50%",md:"33%",lg:"25%"},maxW:{base:"100%",sm:"50%",md:"33%",lg:"25%"},children:t(cn,y(r({label:`H\u1ECDc ph\xED ${n.detail_name}`,placeholder:"Nh\u1EADp h\u1ECDc ph\xED",id:`config_fee_subcat_${n.detail_id}`,style:{boxShadow:"0 0 0 0px #f44336"},error:o==null?void 0:o[`config_fee_subcat_${n.detail_id}`],isInvalid:!!(o==null?void 0:o[`config_fee_subcat_${n.detail_id}`])},$),{value:qe(U(`config_fee_subcat_${n.detail_id}`)||n.fee)}))},`subcat_${n.detail_id}`)}))})]})})]})})})})},mi=bt(p.exports.memo(_i),{FallbackComponent:jt});var Yi=p.exports.forwardRef((s,e)=>t(mi,y(r({},s),{formRef:e})));const Ki=C`
  mutation upsertCoursePlanerNote(
    $coursePlanerId: Int!
    $id: Int
    $note: String!
    $coursePlanerType: EnumCoursePlanerTypeEnum!
  ) @api(name: "appZim") {
    upsertCoursePlanerNote(
      coursePlanerId: $coursePlanerId
      note: $note
      id: $id
      coursePlanerType: $coursePlanerType
    ) {
      note
      createBy {
        id
        user_name
        full_name
      }
      refId
      refTable
      createDate
    }
  }
`,Ji=C`
  mutation upsertCourse($input: UpsertCourseInput) @api(name: "appZim") {
    upsertCourse(input: $input) {
      id
      name
    }
  }
`,gi=C`
  mutation upsertCoursePlan($input: UpsertCoursePlanInput)
  @api(name: "appZim") {
    upsertCoursePlan(input: $input) {
      message
      success
      error
      coursePlan {
        id
        name
      }
    }
  }
`,bi=C`
  mutation createRequestStudentToCourse(
    $student_id: Int
    $course_id: Int
    $type: RequestStudentToCourseTypeEnumeration
    $note: String
    $new_invoice_ids: [Int]
    $old_invoice_ids: [Int]
    $refTable: EnumCoursePlanerTypeEnum
  ) @api(name: "appZim") {
    createRequestStudentToCourse(
      student_id: $student_id
      course_id: $course_id
      type: $type
      note: $note
      new_invoice_ids: $new_invoice_ids
      old_invoice_ids: $old_invoice_ids
      refTable: $refTable
    ) {
      id
      type
      status
      course {
        name
      }
      student {
        ...AccountField
      }
      note
      update_by
      update_date
      create_by
      create_date
    }
  }

  ${Je}
`,Xi=C`
  mutation updateCourseSchedule(
    $event_id: String
    $date: String
    $lesson_id: Int
    $subject_ids: [Int]
    $course_id: Int
    $room_id: Int
    $shift_id: Int
    $teacher_id: Int
    $subcat_type_id: Int
  ) @api(name: "appZim") {
    updateCourseSchedule(
      event_id: $event_id
      date: $date
      lesson_id: $lesson_id
      subject_ids: $subject_ids
      course_id: $course_id
      room_id: $room_id
      shift_id: $shift_id
      teacher_id: $teacher_id
      subcat_type_id: $subcat_type_id
    ) {
      is_additional
      event_id
      course {
        id
        slug
      }
      lesson
      teacher_id
      teacher {
        id
        user_name
        full_name
      }
      alt_teacher_id
      date
      room {
        id
        name
      }
      shift {
        id
        start_time
        end_time
      }
      subcats {
        id
        name
      }
      status
      subcat_type_id
      subcat_type {
        id
        name
      }
      document
    }
  }
`,Qi=C`
  mutation createCourseSchedule(
    $type: String
    $date: String
    $lesson_id: Int
    $subject_ids: [Int]
    $course_id: Int
    $room_id: Int
    $shift_id: Int
    $teacher_id: Int
    $subcat_type_id: Int
  ) @api(name: "appZim") {
    createCourseSchedule(
      type: $type
      date: $date
      lesson_id: $lesson_id
      subject_ids: $subject_ids
      course_id: $course_id
      room_id: $room_id
      shift_id: $shift_id
      teacher_id: $teacher_id
      subcat_type_id: $subcat_type_id
    ) {
      is_additional
      event_id
      course {
        id
        slug
      }
      lesson
      teacher_id
      teacher {
        id
        user_name
        full_name
      }
      alt_teacher_id
      date
      room {
        id
        name
      }
      shift {
        id
        start_time
        end_time
      }
      subcats {
        id
        name
      }
      status
      subcat_type_id
      subcat_type {
        id
        name
      }
      document
    }
  }
`,Wi=C`
  mutation deleteCourseSchedule($event_id: String, $id: Int)
  @api(name: "appZim") {
    deleteCourseSchedule(event_id: $event_id, id: $id) {
      success
      error
      message
    }
  }
`,st=C`
  fragment LevelField on LevelType {
    subject_id
    id
    name
    graduation
    status
    type
  }
`,Ci=C`
  fragment SubjectField on SubjectType {
    id
    name
    status
    cats {
      id
      name
      create_date
      status
    }
    levels {
      ...LevelField
    }
  }
  ${st}
`,yi=C`
  fragment ShiftField on ShiftType {
    id
    start_time
    end_time
    status
    shift_minute
  }
`,xi=C`
  fragment SchoolField on SchoolType {
    id
    city {
      id
      name
      status
    }
    district {
      id
      name
      status
    }
    name
    address
    phone
    status
  }
`,vi=C`
  fragment CurriculumField on CurriculumType {
    id
    name
    shift_minute
    total_lesson
    status
    curriculum_details {
      id
      lesson
    }
    update_by
    update_date
  }
`,Ai=C`
  query coursePlanerPagination($input: CoursePlanerPaginationInputType)
  @api(name: "appZim") {
    coursePlanerPagination(input: $input) {
      hasNextPage
      hasPrevPage
      limit
      page
      pageNext
      pagePrev
      pagingCounter
      totalPages
      totalDocs
      docs {
        id
        code
        ref_table
        start_date
        type
        fee
        total_registed
        total_new
        total_enroll
        total_paid
        percent_occupancy
        day_left
        subject {
          ...SubjectField
        }
        school {
          ...SchoolField
        }
        status
        size {
          id
          name
          size
          status
        }
        level_in {
          ...LevelField
        }
        level_out {
          ...LevelField
        }
        day_of_weeks {
          id
          name
        }
        subcats {
          id
          cat_id
          cat {
            id
            name
            create_date
            status
          }
          name
          status
          parent_id
        }
        shifts {
          ...ShiftField
        }
        curriculum {
          ...CurriculumField
        }
        ec {
          id
          full_name
        }
        teachers {
          ...AccountField
        }
        note
      }
    }
  }
  ${Je}
  ${st}
  ${yi}
  ${Ci}
  ${xi}
  ${vi}
`,Si=C`
  query getCoursePlanerStudents(
    $refId: Int!
    $type: EnumCoursePlanerTypeEnum!
    $studentType: String
  ) @api(name: "appZim") {
    getCoursePlanerStudents(
      refId: $refId
      type: $type
      studentType: $studentType
    ) {
      student_id
      student_name
      student_type
      ec_source_name
      ec_support_name
      invoices
      listInvoice {
        id
        student {
          id
          user_name
          full_name
          phone
          email
        }
        code
        number_invoice
        paid
        status
        payment
        qr_code
        ec_id
        ec_name
        type
        create_date
        create_by
        update_by
        update_date
        note
        appointment_date
        discount_price
        invoice_detail {
          id
          detail_id
          detail_type
          detail_name
          detail_fee
          subcat_name
        }
      }
      invoiceStudent {
        id
        student {
          id
          user_name
          full_name
          phone
          email
        }
        code
        number_invoice
        paid
        status
        payment
        qr_code
        ec_id
        ec_name
        type
        create_date
        create_by
        update_by
        update_date
        note
        appointment_date
        discount_price
        invoice_detail {
          id
          detail_id
          detail_type
          detail_name
          detail_fee
          subcat_name
        }
      }
    }
  }
`,fi=C`
  query getAllCourseBySTudentById($studentId: Int!) @api(name: "appZim") {
    getAllCourseBySTudentById(studentId: $studentId) {
      courseNew {
        tags {
          level_name
          name
        }
        id
        slug
        name
        start_date
        school {
          id
          name
          address
          phone
          color_code
          lat
          lon
          status
        }
        curriculum {
          id
          name
          shift_minute
          total_lesson
          status
          update_by
          update_date
        }
        size {
          id
          name
          size
          status
        }
        fee
        status
        code
        rooms {
          id
          name
          status
          description
        }
        shifts {
          id
          start_time
          end_time
          status
          shift_minute
        }
        day_of_weeks {
          id
          name
        }
        ec_id
        ec {
          id
          user_name
          full_name
          phone
          address
          email
          role_id
        }
        teacher_lead_id
        teacher_lead {
          id
          user_name
          full_name
          phone
          address
          email
          role_id
        }
        description
        featured_image
        total_register
        total_paid
        config_fee {
          type
          fee
          detail_id
          detail_name
        }
        end_date
        subcats {
          id
          name
          status
          parent_id
        }
        type
        level_in {
          subject_id
          id
          name
          graduation
          status
          type
        }
        level_out {
          subject_id
          id
          name
          graduation
          status
          type
        }
        subject {
          id
          name
          status
        }
        coefficient_range {
          id
          min
          max
          operator
          name
        }
        teachers {
          id
          user_name
          full_name
          phone
          address
          email
          role_id
          role_name
        }
      }
      courseOld {
        id
        name
        grade_id
        grade_name
        class_id
        class_name
        open_date
        fee
        curriculum_id
        curriculum_name
        total_schedule
        school {
          name
        }
      }
    }
  }
`,Vi=C`
  query teacherAvailable($course_id: Int!, $date: String, $shift_id: Int)
  @api(name: "appZim") {
    teacherAvailable(course_id: $course_id, date: $date, shift_id: $shift_id) {
      ...AccountField
    }
  }
  ${Je}
`,es=C`
  query courseLesson($course_id: Int!) @api(name: "appZim") {
    courseLesson(course_id: $course_id) {
      id
      name
      subcat_type {
        id
        name
      }
      subcats {
        id
        name
      }
    }
  }
`,ns=C`
  query roomsAvalable(
    $course_id: Int!
    $date: String
    $shift_id: Int
    $room_ids: [Int]
  ) @api(name: "appZim") {
    roomsAvalable(
      course_id: $course_id
      date: $date
      shift_id: $shift_id
      room_ids: $room_ids
    ) {
      id
      name
      status
      description
    }
  }
  ${Je}
`,Fi=C`
  query coursePlanbyStudentId($studentId: Int!) @api(name: "appZim") {
    coursePlanbyStudentId(studentId: $studentId) {
      id
      name
      school {
        name
        id
      }
    }
  }
`,ts=C`
  query getCoursePlanerNotes(
    $coursePlanerId: Int!
    $coursePlanerType: EnumCoursePlanerTypeEnum!
  ) @api(name: "appZim") {
    getCoursePlanerNotes(
      coursePlanerId: $coursePlanerId
      coursePlanerType: $coursePlanerType
    ) {
      id
      refId
      refTable
      note
      createBy {
        id
        user_name
        full_name
      }
      createDate
    }
  }
`,is=C`
  query courseSchedule(
    $course_id: Int
    $subcat_type_id: Int
    $month: Int
    $year: Int
  ) @api(name: "appZim") {
    courseSchedule(
      course_id: $course_id
      subcat_type_id: $subcat_type_id
      month: $month
      year: $year
    ) {
      is_additional
      event_id
      course {
        id
        slug
      }
      lesson
      teacher_id
      teacher {
        id
        user_name
        full_name
      }
      alt_teacher_id
      date
      room {
        id
        name
      }
      shift {
        id
        start_time
        end_time
      }
      subcats {
        id
        name
      }
      status
      subcat_type_id
      subcat_type {
        id
        name
      }
      document
    }
  }
`,ss=C`
  query getCourseSummary($course_id: Int) @api(name: "appZim") {
    getCourseSummary(course_id: $course_id) {
      total

      total_registered

      total_additional
    }
  }
`,us=(s={},e={})=>{const u=T();return R(Zt,r({variables:s,onError:i=>{var a;u({title:"L\u1EA5y d\u1EEF li\u1EC7u th\u1EA5t b\u1EA1i.",description:(a=i==null?void 0:i.message)!=null?a:"L\u1EA5y d\u1EEF li\u1EC7u kh\xF4ng th\xE0nh c\xF4ng",status:"error",duration:3e3,isClosable:!0})},notifyOnNetworkStatusChange:!0},e))},os=(s,e={})=>{const u=T();return R(Si,r({variables:s,onError:i=>{u({title:"L\u1EA5y d\u1EEF li\u1EC7u th\u1EA5t b\u1EA1i.",description:`GET course's student error: ${i==null?void 0:i.message}`,status:"error",duration:3e3,isClosable:!0})}},e))},$i=(s,e={})=>{const u=T();return R(Yt,r({variables:s,onError:i=>{var a;u({title:"L\u1EA5y d\u1EEF li\u1EC7u th\u1EA5t b\u1EA1i.",description:(a=i==null?void 0:i.message)!=null?a:"L\u1EA5y d\u1EEF li\u1EC7u kh\xF4ng th\xE0nh c\xF4ng",status:"error",duration:3e3,isClosable:!0})},notifyOnNetworkStatusChange:!0,skip:!(s==null?void 0:s.id)},e))},Ti=(s,e={})=>{const u=T();return R(Kt,r({variables:s,onError:i=>{var a;u({title:"L\u1EA5y d\u1EEF li\u1EC7u th\u1EA5t b\u1EA1i.",description:(a=i==null?void 0:i.message)!=null?a:"L\u1EA5y d\u1EEF li\u1EC7u kh\xF4ng th\xE0nh c\xF4ng",status:"error",duration:3e3,isClosable:!0})},notifyOnNetworkStatusChange:!0},e))},Li=({children:s,limit:e,onReadMore:u=null})=>{var ee;const i=p.exports.useRef(null),[a,d]=p.exports.useState(!1),[f,b]=p.exports.useState({trim:null,full:null}),[m,F]=p.exports.useState(!0),Q=()=>{u&&typeof u=="function"?u():F(!m)};return p.exports.useEffect(()=>{if(i.current){const O=i.current.textContent;O!==null&&b({full:O,trim:O.slice(0,e)})}},[e,s,a]),p.exports.useEffect(()=>{f.full&&!a&&d(!0)},[f.full,s]),p.exports.useEffect(()=>{d(!1)},[s]),E(he,{children:[t("div",{className:"text hidden",ref:i,children:s}),E("p",{children:[m?f.trim:f.full,((ee=f==null?void 0:f.full)==null?void 0:ee.length)>e&&t("span",{onClick:Q,className:"whitespace-nowrap cursor-pointer read-or-hide text-blue-700 hover:text-blue-900 dark:text-blue-500 dark:hover:text-blue-700 duration-200 transition-colors",children:m?"... Xem th\xEAm":" \u1EA8n b\u1EDBt"})]})]})};var ze;(function(s){s.OLD="invoiceold",s.NEW="invoicenew"})(ze||(ze={}));const Ii=({data:s,onSelectedRows:e})=>{const u=p.exports.useMemo(()=>[{Header:"M\xE3 phi\u1EBFu",accessor:"number_invoice",nowrap:!0,Cell:({value:i,row:{original:a}})=>t(fe,{label:"B\u1EA5m \u0111\u1EC3 xem phi\u1EBFu thu",children:E(_e,{alignItems:"center",children:[t(Ue,{href:$e(`${ae.invoice}/${a.code}`,`${Te.invoice}${a.type==="invoicenew"?`/${a.code}`:`?code=${a.code}`}`),isExternal:!0,children:i}),t(xt,{colorScheme:"gray",size:"xs",fontSize:"xs",p:1,children:a.type===ze.NEW?"M\u1EDBi":"C\u0169"})]})}),disableSortBy:!0},{Header:"\u0110\xE3 thanh to\xE1n",accessor:"paid",Cell:({value:i,row:{original:a}})=>i!==0?`${me(i)}`:"0",disableSortBy:!0,nowrap:!0},{Header:"Ghi ch\xFA",accessor:"note",Cell:({value:i})=>t(Li,{limit:50,children:i}),disableSortBy:!0},{Header:"Ng\u01B0\u1EDDi t\u1EA1o",accessor:"create_by",disableSortBy:!0,minWidth:100},{Header:"Ng\xE0y t\u1EA1o",accessor:"create_date",Cell:({value:i,row:{original:a}})=>t("span",{children:un(i).format("DD/MM/YYYY HH:mm")}),disableSortBy:!0,minWidth:100}],[]);return t(nt,{columns:u,data:s,isSelection:!0,onSelectedRows:e})},Oi=(s,e={})=>{const u=T();return R(Ai,r({variables:s,onError:i=>{var a;u({title:"L\u1EA5y d\u1EEF li\u1EC7u th\u1EA5t b\u1EA1i.",description:(a=i==null?void 0:i.message)!=null?a:"L\u1EA5y d\u1EEF li\u1EC7u kh\xF4ng th\xE0nh c\xF4ng",status:"error",duration:3e3,isClosable:!0})},notifyOnNetworkStatusChange:!0},e))},Di=(s,e={})=>{const u=T();return Vn(bi,r({variables:s,onError:i=>{var a;u({title:"T\u1EA1o y\xEAu c\u1EA7u th\u1EA5t b\u1EA1i",description:(a=i==null?void 0:i.message)!=null?a:"Vui l\xF2ng ki\u1EC3m tra l\u1EA1i d\u1EEF li\u1EC7u nh\u1EADp...",status:"error",duration:3e3,isClosable:!0})}},e))},wi=(s,e={})=>{const u=T();return R(Fi,r({variables:s,onError:i=>{var a;u({title:"L\u1EA5y d\u1EEF li\u1EC7u th\u1EA5t b\u1EA1i.",description:(a=i==null?void 0:i.message)!=null?a:"L\u1EA5y d\u1EEF li\u1EC7u kh\xF4ng th\xE0nh c\xF4ng",status:"error",duration:3e3,isClosable:!0})},notifyOnNetworkStatusChange:!0},e))},ki=(s,e={})=>{const u=T();return R(fi,r({variables:s,onError:i=>{var a;u({title:"L\u1EA5y d\u1EEF li\u1EC7u th\u1EA5t b\u1EA1i.",description:(a=i==null?void 0:i.message)!=null?a:"L\u1EA5y d\u1EEF li\u1EC7u kh\xF4ng th\xE0nh c\xF4ng",status:"error",duration:3e3,isClosable:!0})},notifyOnNetworkStatusChange:!0},e))},En=[{label:"Chuy\u1EC3n v\xE0o kh\xF3a",value:le.JoinCourse},{label:"X\xF3a h\u1ECDc vi\xEAn kh\u1ECFi kh\xF3a",value:le.LeftCourse},{label:"H\u1ECDc l\u1EA1i",value:le.ReEnroll}],ut=[{label:"Ch\xEDnh th\u1EE9c",value:oe.Course},{label:"D\u1EF1 ki\u1EBFn",value:oe.CoursePlan}],Pi=S.object().shape({requestType:S.string().required("M\u1EE5c n\xE0y l\xE0 b\u1EAFt bu\u1ED9c"),center:S.mixed().nullable(!0).required("M\u1EE5c n\xE0y l\xE0 b\u1EAFt bu\u1ED9c"),note:S.string().required("M\u1EE5c n\xE0y l\xE0 b\u1EAFt bu\u1ED9c"),isAddonCourse:S.boolean(),invoices:S.array().of(S.object().shape({number_invoice:S.string(),note:S.string(),paid:S.number(),create_by:S.string(),create_date:S.string()})).when("requestType",{is:s=>s!==le.LeftCourse,then:S.array().of(S.object().shape({number_invoice:S.string(),note:S.string(),paid:S.number(),create_by:S.string(),create_date:S.string()})).min(1,"Vui l\xF2ng ch\u1ECDn \xEDt nh\u1EA5t m\u1ED9t phi\u1EBFu thu").required("M\u1EE5c n\xE0y l\xE0 b\u1EAFt bu\u1ED9c")}),course:S.mixed().nullable(!0).required("M\u1EE5c n\xE0y l\xE0 b\u1EAFt bu\u1ED9c"),courseType:S.mixed().nullable(!0).required("M\u1EE5c n\xE0y l\xE0 b\u1EAFt bu\u1ED9c")}),Ni=({studentId:s,onClose:e})=>{var xe,ve;const{handleSubmit:u,register:i,control:a,formState:{errors:d},resetField:f,setValue:b,watch:m,getValues:F,setError:Q,reset:ee}=Xn({reValidateMode:"onBlur",defaultValues:{requestType:le.JoinCourse,invoices:[],course:null,note:"",center:null,courseType:ut[0],isAddonCourse:!1},resolver:Qn(Pi)}),[O,j,o,U,Z,Ie]=m(["courseType","center","invoices","course","requestType","isAddonCourse"]),k=(O==null?void 0:O.value)===oe.Course,W=T(),{data:ne,loading:Oe}=tt(),{data:_,loading:ce}=$i({id:s}),{data:Y,loading:be}=Oi({input:{refTypes:O?[O.value]:[],schoolIds:(j==null?void 0:j.id)?[j==null?void 0:j.id]:[],limit:9999,orderBy:"start_date"}},{onCompleted:({coursePlanerPagination:h})=>{h&&b("course",null)},skip:!j||!O}),{data:q,loading:de}=wi({studentId:s}),{data:M,loading:te}=ki({studentId:s}),{data:G,loading:De}=Ti({student_id:s,status:Jt.Paid},{skip:!s}),[we,{loading:Ce}]=Di({},{onCompleted:({createRequestStudentToCourse:h})=>{var c,D,H;h?(W({title:"Th\xE0nh c\xF4ng !",description:`T\u1EA1o y\xEAu c\u1EA7u: ${(c=En.find(z=>z.value===Z))==null?void 0:c.label} th\xE0nh c\xF4ng !!`,status:"success",duration:3e3,isClosable:!0}),e(),ee()):W({title:`T\u1EA1o y\xEAu c\u1EA7u: ${(D=En.find(z=>z.value===Z))==null?void 0:D.label} th\u1EA5t b\u1EA1i !!`,description:(H=h==null?void 0:h.message)!=null?H:"Kh\xF4ng x\xE1c \u0111\u1ECBnh",status:"error",duration:3e3,isClosable:!0})}}),ke=async h=>{try{const{requestType:c,invoices:D,course:H,note:z,center:l,courseType:ie,isAddonCourse:K}=h;let V={student_id:s,course_id:H.id,type:c,note:z,refTable:ie.value,new_invoice_ids:[],old_invoice_ids:[],is_free:K};const Re=D.filter(J=>J.type===ze.OLD),Xe=D.filter(J=>J.type===ze.NEW);if(c===le.JoinCourse&&D.reduce((Qe,Ae)=>Qe+Ae.paid+Ae.discount_price,0)<H.fee){Q("invoices",{type:"custom",message:"T\u1ED5ng phi\u1EBFu thu kh\xF4ng \u0111\u01B0\u1EE3c th\u1EA5p h\u01A1n s\u1ED1 ti\u1EC1n kh\xF3a chuy\u1EC3n v\xE0o"});return}V=y(r({},V),{new_invoice_ids:Xe.map(J=>J.id),old_invoice_ids:Re.map(J=>J.id)}),await we({variables:V})}catch(c){console.log("Error submit form",c)}},P=_==null?void 0:_.getPersonalInfo,Pe=(xe=Y==null?void 0:Y.coursePlanerPagination)==null?void 0:xe.docs,ye=G==null?void 0:G.getStudentInvoices,B=p.exports.useMemo(()=>k?M==null?void 0:M.getAllCourseBySTudentById:q==null?void 0:q.coursePlanbyStudentId,[M,q,k]),Ne=p.exports.useMemo(()=>{if(!B)return null;let h=[];return k?Array.isArray(B==null?void 0:B.courseNew)&&Array.isArray(B==null?void 0:B.courseOld)?(h=[...B.courseNew,...B.courseOld],console.log({courses:h},"1")):h=[]:h=B,h.length>0?h.map(c=>{var D;return t(g,{children:E(Ue,{isExternal:!0,href:$e(`${k?ae.courseDetail:ae.coursePlanDetail}/${c.id}`,`${k?Te.courseDetail:ae.coursePlanDetail}/${c.id}`),children:[`${k?"":`${(D=c==null?void 0:c.school)==null?void 0:D.name} | `}`,c.name]})},c.id)}):t(g,{color:"red.500",children:"Kh\xF4ng c\xF3 kh\xF3a h\u1ECDc n\xE0o"})},[k,B]);return p.exports.useEffect(()=>{ee()},[]),t(he,{children:E(v,{as:"form",onSubmit:u(ke),fontSize:"sm",children:[P&&t(v,{bg:"blue.50",rounded:4,p:2,children:E(on,{children:[t(Fe,{pr:4,children:E(g,{children:["H\u1ECD v\xE0 t\xEAn:"," ",t(fe,{label:"Xem th\xF4ng tin h\u1ECDc vi\xEAn",children:t(v,{as:"span",children:t(Xt,{to:`${$e(`${ae.account}/${P.id}`,`${Te.customerInfo}/${P.id}`)}`,children:t("strong",{children:P==null?void 0:P.fullName})})})})]})}),t(Fe,{pr:4,children:E(g,{children:["S\u0110T: ",t("strong",{children:P==null?void 0:P.phone})]})}),t(Fe,{pr:4,children:E(g,{children:["Email: ",E("strong",{children:[" ",P==null?void 0:P.email]})]})})]})}),t(v,{w:"full",mt:4,children:E(_e,{justifyContent:"stretch",children:[t(v,{w:200,children:t(A,{name:"courseType",control:a,render:({field:h})=>t(w,r({label:"Lo\u1EA1i kho\xE1",error:d==null?void 0:d.course,options:ut||[]},h))})}),t(v,{flexGrow:1,children:t(A,{name:"center",control:a,render:({field:h})=>t(w,r({label:"Trung t\xE2m (*)",error:d==null?void 0:d.center,getOptionLabel:c=>c.name,getOptionValue:c=>c.id,options:ne?ne.schools:[],isLoading:Oe,loadingMessage:()=>"\u0110ang l\u1EA5y th\xF4ng tin center"},h))})})]})}),E(Wn,{spacing:4,mt:4,children:[t(A,{name:"requestType",control:a,render:({field:h})=>E(tn,{children:[t(sn,{children:"Lo\u1EA1i y\xEAu c\u1EA7u:"}),t(vt,y(r({},h),{children:t(on,{spacing:4,children:En.map(c=>t(Fe,{pr:4,children:t(At,{value:c.value,children:c.label})},c.value))})}))]})}),E(v,{w:"full",children:[t(A,{name:"course",control:a,render:({field:h})=>t(w,r({label:"Kho\xE1 h\u1ECDc",placeholder:"Ch\u1ECDn kh\xF3a h\u1ECDc",error:d==null?void 0:d.course,getOptionLabel:c=>{var D,H,z,l;return`${c.type} ${c.size.size<4?"Solo":c.subject.name} | ${(H=(D=c.level_in)==null?void 0:D.graduation.toFixed(1))!=null?H:"unset"} \u2192
 ${(l=(z=c.level_out)==null?void 0:z.graduation.toFixed(1))!=null?l:"unset"} | ${un(c.start_date).format("DD/MM/YYYY")} | ${c.size.size} h\u1ECDc vi\xEAn | ${me(c.fee)}`},getOptionValue:c=>c.id,options:Pe||[],isLoading:be,loadingMessage:()=>"\u0110ang l\u1EA5y d\u1EEF li\u1EC7u kh\xF3a h\u1ECDc..."},h))}),!j&&t(g,{color:"red.500",mt:2,children:"Vui l\xF2ng ch\u1ECDn trung t\xE2m tr\u01B0\u1EDBc"})]}),E(v,{w:"full",children:[E(g,{children:["C\xE1c kho\xE1 ",k?"ch\xEDnh th\u1EE9c":"d\u1EF1 ki\u1EBFn"," hi\u1EC7n t\u1EA1i c\u1EE7a h\u1ECDc vi\xEAn:"]}),Ne]}),t(Qt,y(r({label:"Ghi ch\xFA",bg:"white",minRows:2,placeholder:"N\u1ED9i dung..."},i("note")),{error:d==null?void 0:d.note}))]}),E(v,{mt:4,children:[Z!==le.LeftCourse&&t(v,{mb:4,children:t(fe,{label:"N\u1EBFu enable th\xEC c\xE1c phi\u1EBFu thu \u0111\xE3 ch\u1ECDn s\u1EBD kh\xF4ng \u0111\u01B0\u1EE3c t\xEDnh v\xE0o t\u1ED5ng doanh thu c\u1EE7a kh\xF3a n\xE0y",children:E(tn,{display:"flex",alignItems:"center",children:[t(sn,{htmlFor:"is-addon-course",mb:"0",d:"flex",children:t(g,{as:"span",children:" Kh\xF3a h\u1ECDc t\u1EB7ng k\xE8m ?"})}),t(St,y(r({},i("isAddonCourse")),{isChecked:Ie}))]})})}),t(ft,{size:"base",mb:2,children:"Ch\u1ECDn phi\u1EBFu thu:"}),De?t(Wt,{text:"\u0110ang l\u1EA5y th\xF4ng tin h\xF3a \u0111\u01A1n"}):ye&&t(A,{control:a,render:({field:h})=>t(Ii,{data:ye,onSelectedRows:h.onChange}),name:"invoices"}),Z!==le.LeftCourse&&t(he,{children:t(g,{color:"slate.500",my:2,children:"Note: T\u1ED5ng ti\u1EC1n phi\u1EBFu thu ph\u1EA3i l\u1EDBn h\u01A1n ho\u1EB7c b\u1EB1ng s\u1ED1 ti\u1EC1n kh\xF3a h\u1ECDc \u0111\u01B0\u1EE3c chuy\u1EC3n v\xE0o"})}),d.invoices&&t(g,{color:X("red.500","red.300"),mt:2,children:(ve=d==null?void 0:d.invoices)==null?void 0:ve.message}),E(v,{mt:4,children:[U&&E(g,{children:["H\u1ECDc ph\xED kh\xF3a \u0111\xE3 ch\u1ECDn:"," ",t("strong",{children:(U==null?void 0:U.fee)?me(U.fee):0})," "]}),(o==null?void 0:o.length)>0&&E(g,{children:["T\u1ED5ng ti\u1EC1n \u0111\xE3 thanh to\xE1n:"," ",t("strong",{children:me(o.reduce((h,c)=>h+c.paid+c.discount_price,0))})]}),(o==null?void 0:o.length)>0&&U&&U&&E(g,{children:["S\u1ED1 ti\u1EC1n ch\xEAnh l\u1EC7ch:"," ",t("strong",{children:me(o.reduce((h,c)=>h+c.paid+c.discount_price,0)-U.fee)})]})]})]}),E(_e,{mt:8,spacing:4,children:[t(an,{type:"submit",colorScheme:"green",isLoading:Ce,children:"G\u1EEDi y\xEAu c\u1EA7u"}),t(an,{variant:"outline",onClick:e,isDisabled:Ce,children:"H\u1EE7y b\u1ECF"})]})]})})},Ri=({children:s,studentId:e})=>{const{onOpen:u,onClose:i,isOpen:a}=Ft();return E(he,{children:[t(v,{onClick:u,role:"button","aria-label":"Show modal dialog",children:s}),E($t,{isOpen:a,onClose:()=>{},size:"3xl",children:[t(Tt,{}),E(Lt,{children:[t(It,{children:"G\u1EEDi y\xEAu c\u1EA7u"}),t(Ot,{onClick:i}),t(Dt,{children:t(v,{pb:4,children:t(Ni,{studentId:e,onClose:i})})})]})]})]})},as=({data:s})=>{const e=p.exports.useMemo(()=>[{Header:"H\u1ECDc vi\xEAn",accessor:"student_name",nowrap:!0,Cell:({value:u,row:{original:i}})=>t(fe,{label:"Xem th\xF4ng tin h\u1ECDc vi\xEAn",children:t(v,{children:t(Ue,{href:`${$e(`${ae.account}/${i.student_id}`,`${Te.customerInfo}/${i.student_id}`)}`,isExternal:!0,children:u})})})},{Header:"Tr\u1EA1ng th\xE1i",accessor:"student_type",nowrap:!0,Cell:({value:u})=>u==="new"?"\u0110\u0103ng k\xFD m\u1EDBi":"H\u1ECDc l\u1EA1i"},{Header:()=>t(g,{align:"right",children:"\u0110\xE3 thanh to\xE1n"}),accessor:"paid",nowrap:!0,Cell:({value:u,row:{original:i}})=>{const{listInvoice:a,invoiceStudent:d}=i,b=((a==null?void 0:a.length)>0?a:d).reduce((m,F)=>F.paid+m,0);return t(g,{children:`${b?me(b):"0"}`})}},{Header:()=>t(g,{align:"right",children:"EC support"}),accessor:"ec_support_name",nowrap:!0},{Header:()=>t(g,{align:"right",children:"EC ngu\u1ED3n"}),accessor:"ec_source_name",nowrap:!0},{Header:"Thao t\xE1c",accessor:"",nowrap:!0,Cell:({value:u,row:{original:i}})=>t(Ri,{studentId:i.student_id,children:t(an,{size:"sm",leftIcon:t(wt,{}),children:"T\u1EA1o y\xEAu c\u1EA7u"})})},{Header:"Phi\u1EBFu thu",accessor:"invoices",disableSortBy:!0,isSticky:!0,stickyRightOffset:0,Cell:({value:u,row:{original:i}})=>{const{listInvoice:a,invoiceStudent:d}=i,f=(a==null?void 0:a.length)===0;return E(he,{children:[t(_e,{children:a.map(b=>t(Ue,{href:$e(`${ae.invoice}/${b.code}`,`${Te.invoice}/${b.code}`),isExternal:!0,children:t(v,{as:"span",children:t(fe,{label:b.note,children:t(g,{fontSize:24,color:X("blue.500","blue.300"),children:t(Ye,{})})})})},b.code))}),f&&E(he,{children:[t(_e,{children:d.map(b=>t(Ue,{href:$e(`${ae.invoice}/${b.code}`,`${Te.invoice}?code=${b.code}`),isExternal:!0,children:t(v,{as:"span",children:t(fe,{label:b.note,children:t(g,{color:"yellow.500",fontSize:24,children:t(Ye,{})})})})},b.code))}),t(g,{fontSize:12,mt:1,color:X("gray.500","slate.500"),children:"H\u1ECDc vi\xEAn kh\xF4ng c\xF3 phi\u1EBFu thu trong kh\xF3a n\xE0y, ki\u1EC3m tra l\u1EA1i trong c\xE1c phi\u1EBFu thu tr\xEAn."})]})]})}}],[]);return E(v,{children:[t(nt,{columns:e,data:s}),E(on,{mt:4,gap:4,children:[t(Fe,{children:E(_e,{children:[t(g,{fontSize:24,color:X("blue.500","blue.300"),children:t(Ye,{})}),t(g,{children:"Phi\u1EBFu thu kh\xF3a hi\u1EC7n t\u1EA1i"})]})}),t(Fe,{children:E(_e,{children:[t(g,{fontSize:24,color:"yellow.500",children:t(Ye,{})}),t(g,{children:"Phi\u1EBFu thu kh\xE1c c\u1EE7a h\u1ECDc vi\xEAn kh\xF4ng thu\u1ED9c kh\xF3a hi\u1EC7n t\u1EA1i"})]})})]})]})},ls=(s={},e={})=>{const u=T();return Vn(gi,r({variables:s,onError:i=>{var a;u({title:"Th\u1EA5t b\u1EA1i !!!",description:(a=i==null?void 0:i.message)!=null?a:"T\u1EA1o ho\u1EB7c c\u1EADp nh\u1EADt kh\xF4ng th\xE0nh c\xF4ng !",status:"error",duration:5e3,isClosable:!0})},context:{headers:{"x-app-token":"0aAUfvzhIZVSDIF0yBk53U8Vokij6xv7"}},notifyOnNetworkStatusChange:!0},e))};export{Yi as C,Wi as D,is as G,as as S,Xi as U,ss as a,Vi as b,ns as c,es as d,Qi as e,us as f,os as g,ls as h,Ji as i,ts as j,Ki as k,Oi as l,Zi as m,si as n,ui as o,oi as p,zi as q,He as r,ji as s,me as t,ri as u};
