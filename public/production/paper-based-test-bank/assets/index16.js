import{g as e,j as i,a0 as s,k as c,H as u,dZ as r,C as p}from"./vendor.js";const l=e`
  query ($type: EnumShiftsType) @api(name: "appZim") {
    getShiftRegisterSchedule(type: $type) {
      id
      start_time
      end_time
      name
    }
  }
`;e`
  query shifts($q: String, $status: String, $shift_minute: Int)
  @api(name: "appZim") {
    shifts(q: $q, status: $status, shift_minute: $shift_minute) {
      id
      start_time
      end_time
    }
  }
`;const m=e`
  query getScheduleDetail(
    $registerScheduleId: Int!
    $teacherId: CustomUserInputType
  ) @api(name: "appZim") {
    getScheduleDetail(
      registerScheduleId: $registerScheduleId
      teacherId: $teacherId
    ) {
      id
      type
      date
      schoolId
      startTime
      endTime
      students {
        id
        full_name
      }
      teacher {
        id
        full_name
      }
      eC {
        id
        full_name
      }
      linkMeeting
      description
    }
  }
`,h=e`
  mutation createRegisterSchedule($input: RegisterScheduleInputCreateType)
  @api(name: "appZim") {
    createRegisterSchedule(input: $input) {
      success
      error
      message
      dataString
    }
  }
`,S=e`
  mutation cancelRegisterSchedule($input: CancelRegisterInputType)
  @api(name: "appZim") {
    cancelRegisterSchedule(input: $input) {
      success
      error
      message
    }
  }
`,E=e`
  mutation createOrUpdateRegisterScheduleDetail(
    $input: CreateRegisterScheduleDetailInputType
  ) @api(name: "appZim") {
    createOrUpdateRegisterScheduleDetail(input: $input) {
      success
      error
      message
    }
  }
`,_=e`
  mutation approveOrRejectRequestCancel(
    $input: RejectRequestCancelScheduleInputType
  ) @api(name: "appZim") {
    approveOrRejectRequestCancel(input: $input) {
      success
      message
      dataString
    }
  }
`,T=e`
  mutation changeScheduleForTeacher($input: ChangeScheduleForTeacherInputType)
  @api(name: "appZim") {
    changeScheduleForTeacher(input: $input) {
      success
      error
      message
    }
  }
`,$=e`
  mutation cancelScheduleDetail($input: ScheduleDetailInputType)
  @api(name: "appZim") {
    cancelScheduleDetail(input: $input) {
      message
      success
      dataString
      error
    }
  }
`;e`
  mutation copyRegisterSchedule($input: CopyRegisterScheduleInputType)
  @api(name: "appZim") {
    copyRegisterSchedule(input: $input) {
      success
      message
    }
  }
`;const g=e`
  mutation reopenScheduleDetail($input: ReopenScheduleDetailInputType)
  @api(name: "appZim") {
    reopenScheduleDetail(input: $input) {
      success
      error
    }
  }
`,y=2,I=3,C=4;var n;(function(t){t.register="register",t.function="function"})(n||(n={}));const o=e`
  fragment AllScheduleCopyType on ScheduleCopyType {
    type
    id
    configFunction {
      isAutoSchedule
    }
    date
    shift {
      id
      start_time
      end_time
    }
    teachers {
      id
      full_name
    }
    school {
      id
      name
    }
  }
`;e`
  query getTodoScheduleByEc($idEc: Int, $date: String) @api(name: "appZim") {
    getTodoScheduleByEc(idEc: $idEc, date: $date) {
      id
      type
      content
      customer {
        id
        role_id
        user_name
        full_name
        avatar
      }
      ec {
        id
        user_name
        full_name
        avatar
      }
      instructor {
        id
        role_id
        user_name
        full_name
        avatar
      }
      file_attached
      link
      start_time
      end_time
      date
      is_delete
      create_by
      create_date
      update_by
      update_date
    }
  }
`;const D=e`
  query getConfigFunctionByType($type: EnumConfigFunctionType!)
  @api(name: "appZim") {
    getConfigFunctionByType(type: $type) {
      id
      name
      type
      description
      timePerShift
      color
      isAutoSchedule
    }
  }
`,R=e`
  query getListRegisterSchedule($fromDate: String, $toDate: String)
  @api(name: "appZim") {
    getListRegisterSchedule(fromDate: $fromDate, toDate: $toDate) {
      id
      type
      date
      shift {
        id
        start_time
        end_time
      }
      isFull
      requestsCancel {
        teacherId
        id
        reason
      }
      school {
        id
        name
      }
    }
  }
`,f=e`
  query ($teacherId: CustomUserInputType!) @api(name: "appZim") {
    getFunctionAssignedTeacher(teacherId: $teacherId) {
      id
      name
      color
      isAutoSchedule
      shiftType
      needToCenter
    }
  }
`,L=e`
  query getListRegisterSchedule(
    $fromDate: String
    $toDate: String
    $type: Int
    $schoolId: Int
  ) @api(name: "appZim") {
    getListRegisterSchedule(
      fromDate: $fromDate
      toDate: $toDate
      type: $type
      schoolId: $schoolId
    ) {
      id
      type
      date
      shift {
        id
        start_time
        end_time
        name
      }
      school {
        id
        name
      }
      teachers {
        id
        full_name
      }
      requestsCancel {
        teacherId
        id
        reason
      }
      configFunction {
        isAutoSchedule
        timePerShift
        id
        color
        name
      }
      isFull
    }
  }
`;e`
  query checkListCopySchedule($input: CopyRegisterScheduleInputType)
  @api(name: "appZim") {
    checkListCopySchedule(input: $input) {
      scheduleConflict {
        ...AllScheduleCopyType
      }
      scheduleApprove {
        ...AllScheduleCopyType
      }
    }
  }
  ${o}
`;const A=e`
  query getListScheduleByStatus(
    $fromDate: String
    $toDate: String
    $type: Int
    $teacherId: CustomUserInputType
    $schoolId: Int
    $status: EnumBaseStatus
  ) @api(name: "appZim") {
    getListScheduleByStatus(
      fromDate: $fromDate
      toDate: $toDate
      type: $type
      teacherId: $teacherId
      schoolId: $schoolId
      status: $status
    ) {
      id
      type
      date
      configFunction {
        id
        isAutoSchedule
      }
      teacher {
        id
        full_name
      }
      students {
        id
        full_name
      }
      startTime
      endTime
      description
      roomId
      linkMeeting
    }
  }
`,U=e`
  query getRequestCancelByStatus(
    $fromDate: String
    $toDate: String
    $type: Int
    $teacherId: CustomUserInputType
    $schoolId: Int
    $status: EnumRequestCancelStatus
  ) @api(name: "appZim") {
    getRequestCancelByStatus(
      fromDate: $fromDate
      toDate: $toDate
      type: $type
      teacherId: $teacherId
      schoolId: $schoolId
      status: $status
    ) {
      id
      type
      date
    }
  }
`,q=({typeRegister:t})=>(t==null?void 0:t.length)?i(s,{columns:{xl:4,base:1},maxW:"xl",rowGap:2,columnGap:{xl:6,base:0},children:t.map(a=>c(u,{spacing:2,children:[i(r,{size:3,bg:`${a.color}.500`}),i(p,{whiteSpace:"nowrap",children:a.name})]},a.id))}):null;export{_ as A,h as C,n as E,l as G,C as O,y as P,g as R,I as S,S as a,m as b,D as c,f as d,R as e,q as f,T as g,$ as h,L as i,A as j,U as k,E as l};
