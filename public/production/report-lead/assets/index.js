var Wt=Object.defineProperty,Vt=Object.defineProperties;var Zt=Object.getOwnPropertyDescriptors;var de=Object.getOwnPropertySymbols;var Ge=Object.prototype.hasOwnProperty,We=Object.prototype.propertyIsEnumerable;var Ve=(e,a,n)=>a in e?Wt(e,a,{enumerable:!0,configurable:!0,writable:!0,value:n}):e[a]=n,y=(e,a)=>{for(var n in a||(a={}))Ge.call(a,n)&&Ve(e,n,a[n]);if(de)for(var n of de(a))We.call(a,n)&&Ve(e,n,a[n]);return e},C=(e,a)=>Vt(e,Zt(a));var H=(e,a)=>{var n={};for(var r in e)Ge.call(e,r)&&a.indexOf(r)<0&&(n[r]=e[r]);if(e!=null&&de)for(var r of de(e))a.indexOf(r)<0&&We.call(e,r)&&(n[r]=e[r]);return n};import{g as s,o as Kt,A as Ce,O as Xt,I as Qt,r as Ie,M as Jt,c as ea,W as Ze,T as ta,a as aa,s as ia,b as na,u as ra,d as oa,e as l,j as t,f as sa,L as ce,h as la,i as p,F as Ke,k as Xe,l as Qe,m as _,n as Je,B as b,p as da,q as ca,t as N,V as ua,v as q,w as Q,y as Ee,x as De,z as Se,H as A,S as pa,C as T,D as ma,E as ga,G as ha,J as et,K as fa,N as ue,P as ba,Q as $a,R as pe,U as ya,X as Ca,Y as Ia,Z as Ea,_ as Da,$ as xe,a0 as R,a1 as Sa,a2 as tt,a3 as xa,a4 as Ta,a5 as ka,a6 as Pa,a7 as Aa,a8 as me,a9 as Te,aa as ae,ab as ke,ac as Pe,ad as va,ae as Ae,af as at,ag as W,ah as ve,ai as _a,aj as wa,ak as Oa,al as La,am as Fa,an as Ra,ao as Ba,ap as Ma,aq as Na,ar as ja,as as za,at as Ha,au as qa,av as Ua,aw as Ya,ax as Ga,ay as Wa,az as Va,aA as Za,aB as Ka,aC as Xa,aD as Qa,aE as it,aF as nt,aG as Ja,aH as ei,aI as _e,aJ as ti,aK as ai,aL as ii,aM as ni,aN as ri,aO as oi,aP as si,aQ as li,aR as di,aS as ci,aT as rt,aU as ui,aV as V,aW as pi,aX as mi,aY as ot,aZ as gi,a_ as hi,a$ as st,b0 as ie,b1 as lt,b2 as dt,b3 as ct,b4 as ut,b5 as ne,b6 as pt,b7 as mt,b8 as gt,b9 as fi,ba as bi,bb as $i,bc as yi,bd as Ci,be as Ii,bf as Ei,bg as Di,bh as Si,bi as xi,bj as Ti,bk as ki,bl as Pi,bm as Ai,bn as vi,bo as _i,bp as wi,bq as Oi,br as Li,bs as Fi,bt as Ri,bu as Bi,bv as Mi,bw as Ni,bx as ji}from"./vendor.js";const zi=function(){const a=document.createElement("link").relList;if(a&&a.supports&&a.supports("modulepreload"))return;for(const o of document.querySelectorAll('link[rel="modulepreload"]'))r(o);new MutationObserver(o=>{for(const i of o)if(i.type==="childList")for(const u of i.addedNodes)u.tagName==="LINK"&&u.rel==="modulepreload"&&r(u)}).observe(document,{childList:!0,subtree:!0});function n(o){const i={};return o.integrity&&(i.integrity=o.integrity),o.referrerpolicy&&(i.referrerPolicy=o.referrerpolicy),o.crossorigin==="use-credentials"?i.credentials="include":o.crossorigin==="anonymous"?i.credentials="omit":i.credentials="same-origin",i}function r(o){if(o.ep)return;o.ep=!0;const i=n(o);fetch(o.href,i)}};zi();const Hi="modulepreload",ht={},qi="/",U=function(a,n){return!n||n.length===0?a():Promise.all(n.map(r=>{if(r=`${qi}${r}`,r in ht)return;ht[r]=!0;const o=r.endsWith(".css"),i=o?'[rel="stylesheet"]':"";if(document.querySelector(`link[href="${r}"]${i}`))return;const u=document.createElement("link");if(u.rel=o?"stylesheet":Hi,o||(u.as="script",u.crossOrigin=""),u.href=r,document.head.appendChild(u),o)return new Promise((g,m)=>{u.addEventListener("load",g),u.addEventListener("error",m)})})).then(()=>a())};const re=s`
  fragment Post on Post {
    id
    title
    slug
    excerpt
    content
    status
    numOfComments
    categories {
      id
      title
      slug
    }
    tags {
      id
      name
      slug
    }
    postGroups {
      id
      title
      slug
    }
    publishedAt
    featureMedias {
      id
      type
      path
      variants {
        id
        type
        width
        height
        path
      }
    }
    user {
      id
      role
      username
      fullName
      phone
      gender
      avatar
    }
    censor {
      id
      fullName
    }
    createdAt
    updatedAt
    numOfContextualLink
  }
`;s`
  query posts(
    $after: String
    $before: String
    $first: Int
    $last: Int
    $categorySlugs: [String]
    $relatedPost: ObjectID
  ) @api(name: "zim") {
    posts(
      after: $after
      before: $before
      first: $first
      last: $last
      categorySlugs: $categorySlugs
      relatedPost: $relatedPost
    ) {
      totalCount
      edges {
        cursor
        node {
          ...Post
        }
      }
      pageInfo {
        startCursor
        endCursor
        hasNextPage
        hasPreviousPage
      }
    }
  }
  ${re}
`;s`
  query ($idOrSlug: String!) @api(name: "zim") {
    post(idOrSlug: $idOrSlug) {
      ...Post
      seo {
        title
        description
        ogDescription
        ogImage {
          id
          path
        }
        ogTitle
        publisher
        noIndex
        noFollow
        canonicalUrl
        customHeadHtml
      }
      rejectReason
    }
  }
  ${re}
`;s`
  query postPagination(
    $searchString: String
    $page: Int
    $limit: Int
    $categoryIds: [ObjectID]
    $tagIds: [ObjectID]
    $postGroupIds: [ObjectID]
    $userIds: [Int]
    $statuses: [PostStatus]
    $censorUserIds: [Int]
    $orderBy: PostOrderBy
    $order: OrderDirection
  ) @api(name: "zim") {
    postPagination(
      searchString: $searchString
      page: $page
      limit: $limit
      categoryIds: $categoryIds
      tagIds: $tagIds
      postGroupIds: $postGroupIds
      userIds: $userIds
      statuses: $statuses
      censorUserIds: $censorUserIds
      orderBy: $orderBy
      order: $order
    ) {
      hasNextPage
      hasPrevPage
      limit
      nextPage
      prevPage
      totalDocs
      totalPages
      docs {
        ...Post
      }
    }
  }
  ${re}
`;s`
  mutation updatePost($postId: ObjectID!, $input: UpdatePostData!)
  @api(name: "zim") {
    updatePost(input: $input, postId: $postId) {
      ...Post
    }
  }
  ${re}
`;s`
  mutation createPost($input: CreatePostData!) @api(name: "zim") {
    createPost(input: $input) {
      ...Post
    }
  }
  ${re}
`;s`
  mutation createTag($name: String!, $description: String) @api(name: "zim") {
    createTag(description: $description, name: $name) {
      id
      name
      slug
      description
    }
  }
`;s`
  mutation publishPost($postId: ObjectID!) @api(name: "zim") {
    publishPost(postId: $postId) {
      id
      title
      status
    }
  }
`;s`
  mutation unpublishPost($postId: ObjectID!) @api(name: "zim") {
    unpublishPost(postId: $postId) {
      id
      title
      status
    }
  }
`;s`
  mutation requestPostForApproval($postId: ObjectID!) @api(name: "zim") {
    requestPostForApproval(postId: $postId) {
      id
      title
      status
    }
  }
`;s`
  mutation approveOrRejectPost(
    $postId: ObjectID!
    $status: ApproveOrRejectPostStatus
    $rejectReason: String
    $updatedContent: String
  ) @api(name: "zim") {
    approveOrRejectPost(
      postId: $postId
      status: $status
      rejectReason: $rejectReason
      updatedContent: $updatedContent
    ) {
      id
      title
      status
      rejectReason
      content
    }
  }
`;s`
  mutation reassignPost($postId: ObjectID!, $userId: Int!) @api(name: "zim") {
    reassignPost(postId: $postId, userId: $userId) {
      id
      user {
        id
        birthday
        email
        fullName
        avatar
        roleId
        role
      }
    }
  }
`;s`
  mutation signInV2($username: String!, $password: String!) @api(name: "zim") {
    signInV2(username: $username, password: $password) {
      token
      expired_at
      refreshToken
    }
  }
`;const Ui=s`
  query @api(name: "zim") {
    me {
      id
      role
      roleId
      username
      fullName
      phone
      birthday
      gender
      avatar
      email
      canCensor
    }
  }
`,Yi=s`
  mutation signOut @api(name: "zim") {
    signOut
  }
`;s`
  mutation createPostCategory($input: PostCategoryInput!) @api(name: "zim") {
    createPostCategory(input: $input) {
      id
      title
      slug
      description
    }
  }
`;const ft=s`
  fragment CategoryFields on PostCategory {
    id
    title
    slug
    parentCategory {
      id
      title
      slug
    }
    description
  }
`;s`
  query postCategoriesTree @api(name: "zim") {
    postCategoriesTree {
      ...CategoryFields
      children {
        ...CategoryFields
        children {
          ...CategoryFields
          children {
            ...CategoryFields
          }
        }
      }
    }
  }
  ${ft}
`;s`
  query postCategories($flatten: Boolean, $search: String, $parent: ObjectID)
  @api(name: "zim") {
    postCategories(search: $search, parent: $parent, flatten: $flatten) {
      ...CategoryFields
      children {
        ...CategoryFields
      }
    }
  }
  ${ft}
`;s`
  mutation updatePostCategory($id: ObjectID!, $input: UpdatePostCategoryInput!)
  @api(name: "zim") {
    updatePostCategory(id: $id, input: $input) {
      id
      title
      slug
      description
    }
  }
`;s`
  mutation deletePostCategory($id: ObjectID!) @api(name: "zim") {
    deletePostCategory(id: $id)
  }
`;s`
  query comments(
    $type: CommentType!
    $after: String
    $before: String
    $first: Int
    $last: Int
    $refId: String!
    $parent: ObjectID
  ) @api(name: "zim") {
    comments(
      type: $type
      after: $after
      before: $before
      first: $first
      last: $last
      refId: $refId
      parent: $parent
    ) {
      totalCount
      edges {
        cursor
        node {
          ... on PostComment {
            id
            user {
              id
              role
              username
              fullName
              phone
              birthday
              gender
              avatar
            }
            content
            parent {
              id
              content
              user {
                id
                role
                username
                fullName
                phone
                birthday
                gender
                avatar
              }
            }
            liked
            numOfLikes
            createdAt
            updatedAt
          }
          __typename
        }
      }
      pageInfo {
        startCursor
        endCursor
        hasNextPage
        hasPreviousPage
      }
    }
  }
`;s`
  mutation postComment(
    $content: String!
    $type: CommentType
    $ref: ObjectID!
    $parent: ObjectID
  ) @api(name: "zim") {
    postComment(content: $content, type: $type, ref: $ref, parent: $parent) {
      ... on PostComment {
        id
        content
        user {
          id
          role
          username
          fullName
          phone
          birthday
          gender
          avatar
        }
        parent {
          id
          content
          user {
            id
            role
            username
            fullName
            phone
            birthday
            gender
            avatar
          }
          liked
          numOfLikes
          createdAt
          updatedAt
        }
        liked
        numOfLikes
        createdAt
        updatedAt
      }
      __typename
    }
  }
`;s`
  mutation likeOrUnlikeComment($commentId: ObjectID!, $liked: Boolean!)
  @api(name: "zim") {
    likeOrUnlikeComment(commentId: $commentId, liked: $liked) {
      ... on PostComment {
        id
        liked
        numOfLikes
      }
    }
  }
`;s`
  query schools($city_id: Int, $status: String) @api(name: "appZim") {
    schools(city_id: $city_id, status: $status) {
      id
      city {
        id
      }
      district {
        id
      }
      name
      address
      phone
      color_code
      lat
      lon
      status
    }
  }
`;s`
  query cities @api(name: "appZim") {
    cities {
      id
      name
      status
    }
  }
`;s`
  query cats @api(name: "appZim") {
    cats {
      id
      name
    }
  }
`;s`
  query subjects @api(name: "appZim") {
    subjects {
      id
      name
      status
      cats {
        id
        name
        status
        subject {
          id
          name
          status
        }
      }
    }
  }
`;const ir=s`
  query levels($subject_id: Int!) @api(name: "appZim") {
    levels(subject_id: $subject_id) {
      id
      name
      status
      graduation
    }
  }
`,bt=s`
  fragment Course on CoursePlanType {
    id
    slug
    start_date
    program {
      id
      name
      cat {
        id
        name
      }
      subject {
        id
        name
      }
      program_type {
        id
        name
      }
      level {
        subject_id
        id
        name
        graduation
        status
      }
      fee
      status
      sub_cats {
        id
        cat_id
        name
        status
        parent_id
      }
    }
    subject {
      id
      name
      status
    }
    level_out {
      subject_id
      id
      name
      graduation
    }
    type
    size {
      id
      name
      size
      status
    }
    school {
      id
      city {
        id
      }
      district {
        id
      }
      name
      address
      phone
      color_code
      lat
      lon
      status
    }
    curriculum {
      id
      name
      program {
        id
      }
      shift_minute
      total_lesson
      status
    }
    shifts {
      id
      start_time
      end_time
      status
      shift_minute
    }
    day_of_weeks {
      id
      name
    }
    config_fee {
      detail_name
      detail_id
      type
      fee
    }
    fee
    status
    ec_id
    teacher_lead_id
    description
    featured_image
  }
`,nr=s`
  query getListCoursePagination(
    $order_by: String
    $order: String
    $q: String
    $school_id: Int
    $city_id: Int
    $program_id: Int
    $program_type_id: Int
    $limit: Int
    $cat_id: Int
    $level_id: Int
    $subject_id: Int
    $status: [CourseStatusEnum]
    $type: EnumCourseTypeEnum
    $fromdate: String
    $todate: String
    $page: Int
    $bussiness_partner_id: Int
  ) @api(name: "appZim") {
    getListCoursePagination(
      q: $q
      status: $status
      subject_id: $subject_id
      level_id: $level_id
      cat_id: $cat_id
      school_id: $school_id
      city_id: $city_id
      program_id: $program_id
      program_type_id: $program_type_id
      type: $type
      fromdate: $fromdate
      todate: $todate
      order_by: $order_by
      order: $order
      page: $page
      limit: $limit
      bussiness_partner_id: $bussiness_partner_id
    ) {
      hasNextPage
      hasPrevPage
      limit
      page
      pageNext
      pagePrev
      pagingCounter
      totalPages
      totalDocs
      docs {
        id
        slug
        name
        start_date
        subcats {
          id
          cat_id
          name
          status
          parent_id
        }
        subject {
          id
          name
          status
        }
        school {
          id
          name
        }

        shifts {
          id
          start_time
          end_time
        }
        level_out {
          subject_id
          id
          name
          graduation
        }
        day_of_weeks {
          id
          name
        }
        size {
          id
          name
          size
          status
        }
        fee
        status
        total_register
        total_paid
        config_fee {
          type
          fee
          detail_id
          detail_name
        }
        end_date
        type
        ec {
          id
          full_name
        }
        teachers {
          id
          user_name
          full_name
        }
      }
    }
  }
`;s`
  query coursesPlan($offset: Int, $limit: Int, $school_id: Int, $city_id: Int)
  @api(name: "appZim") {
    coursesPlan(
      school_id: $school_id
      city_id: $city_id
      offset: $offset
      limit: $limit
    ) {
      totalCount
      edges {
        node {
          ...Course
        }
      }
      pageInfo {
        hasNextPage
        hasPreviousPage
      }
    }
  }
  ${bt}
`;s`
  query coursesPlan($limit: Int) @api(name: "appZim") {
    coursesPlan(status: incoming, limit: $limit) {
      edges {
        node {
          id
          slug
        }
      }
    }
  }
`;s`
  query coursePlan($idOrSlug: String!) @api(name: "appZim") {
    coursePlan(idOrSlug: $idOrSlug) {
      ...Course
    }
  }
  ${bt}
`;s`
  mutation createAdvisory(
    $name: String
    $phone: String!
    $email: String!
    $city_id: Int!
    $gclid: String
    $fbclid: String
    $link: String
    $note: String
  ) @api(name: "appZim") {
    createAdvisory(
      name: $name
      phone: $phone
      email: $email
      city_id: $city_id
      gclid: $gclid
      fbclid: $fbclid
      link: $link
      note: $note
    ) {
      success
      error
      message
    }
  }
`;s`
  mutation checkout($input: CheckoutInput!) @api(name: "zim") {
    checkout(input: $input) {
      paymentUrl
      order {
        id
        status
        paymentMethod
        paymentStatus
        shippingMethod
        orderId
        email
        shippingAddress {
          recipientName
          recipientPhone
          email
          street1
          street2
          city
          state
          zip
          country
        }
        total
      }
    }
  }
`;const $t=s`
  fragment OrderBaseFields on Order {
    id
    status
    paymentMethod
    paymentStatus
    shippingMethod
    orderId
    status
    email
    shippingAddress {
      recipientName
      recipientPhone
      email
      street1
      street2
      city
      state
      zip
      country
    }
    total
    createdAt
    updatedAt
  }
`,Gi=s`
  fragment OrderDetailFields on Order {
    ...OrderBaseFields
    user {
      id
      fullName
    }
    billingAddress {
      recipientName
      recipientPhone
      email
      street1
      street2
      city
      state
      zip
      country
    }
    attachments {
      id
      filename
      filepath
    }
    total
    totalPreDiscount
    discount
    coupons
    items {
      quantity
      total
      preTaxTotal
      price
      salePrice
      tax
      type
      item {
        ... on SimpleProduct {
          id
          name
          slug
          images {
            id
            path
          }
          sku
          attributes {
            isVariantAttribute
            attribute {
              id
              name
            }
            value
          }
        }
        ... on Post {
          id
          excerpt
          featureMedias {
            id
            path
          }
          title
          slug
        }
        ... on MockTestItem {
          data
        }
      }
    }
  }
  ${$t}
`;s`
  query order($idOrCode: String!) @api(name: "zim") {
    order(idOrCode: $idOrCode) {
      ...OrderBaseFields
      ...OrderDetailFields
    }
  }
  ${Gi}
`;s`
  query orderPagination(
    $searchString: String
    $statuses: [OrderStatus]
    $userIds: [Int]
    $orderBy: OrderOrderBy
    $order: OrderDirection
    $page: Int
    $limit: Int
    $itemType: [CheckoutProduct]
  ) @api(name: "zim") {
    orderPagination(
      searchString: $searchString
      statuses: $statuses
      userIds: $userIds
      order: $order
      orderBy: $orderBy
      page: $page
      limit: $limit
      itemType: $itemType
    ) {
      totalPages
      totalDocs
      docs {
        ...OrderBaseFields
      }
    }
  }
  ${$t}
`;s`
  query tags(
    $after: String
    $before: String
    $first: Int
    $last: Int
    $search: String
  ) @api(name: "zim") {
    tags(
      after: $after
      before: $before
      first: $first
      last: $last
      search: $search
    ) {
      edges {
        node {
          id
          name
          description
          slug
        }
      }
      pageInfo {
        hasNextPage
        hasPreviousPage
        startCursor
        endCursor
      }
    }
  }
`;s`
  query postCategories($search: String, $parent: ObjectID) @api(name: "zim") {
    postCategories(search: $search, parent: $parent) {
      id
      title
      slug
      parentCategory {
        id
        title
        slug
      }
      children {
        id
        title
        slug
        parentCategory {
          id
          title
          slug
        }
      }
    }
  }
`;const Z=s`
  fragment ProductBaseField on BaseProduct {
    ... on SimpleProduct {
      id
      sku
      name
      slug
      regularPrice
      salePrice
      salePriceFrom
      salePriceTo
      description
      shortDescription
      numOfContextualLink
      attributes {
        id
        attribute {
          id
          name
          type
          options
        }
        value
        isVariantAttribute
      }
      parentProduct {
        id
      }
      price
      categories {
        id
        title
        slug
      }
      seo {
        title
        description
        ogDescription
        ogImage {
          id
          path
        }
        ogTitle
        publisher
        noIndex
        noFollow
        canonicalUrl
        customHeadHtml
      }
      images {
        id
        path
      }
      status
      productType
      tags {
        id
        name
        description
        slug
      }
      downloadable
      attributes {
        attribute {
          id
          type
          name
          options
        }
        value
      }
      inStock
      inventories {
        id
        inStock
        manageStock
        reserved
        storeId
        inventory
        store {
          id
          name
        }
      }
      createdAt
      updatedAt
    }
    ... on VariableProduct {
      id
      sku
      name
      slug
      categories {
        id
        title
        slug
      }
      images {
        id
        filename
        path
      }
      description
      shortDescription
      status
      minPrice
      maxPrice
      minRegularPrice
      maxRegularPrice
      productType
      inStock
      variationAttributes {
        id
        name
        type
        options
      }
      createdAt
      updatedAt
      seo {
        title
        description
        ogDescription
        ogImage {
          id
          path
        }
        ogTitle
        publisher
        noIndex
        noFollow
        canonicalUrl
        customHeadHtml
      }
    }
  }
`,oe=s`
  fragment ProductDetailFields on BaseProduct {
    ...ProductBaseField
    ... on SimpleProduct {
      tags {
        id
        name
      }
      downloadable
      downloads {
        id
        filename
        path
      }

      attributes {
        id
        attribute {
          id
          name
          type
          options
        }
        value
        isVariantAttribute
        media {
          id
          type
          filename
          path
        }
      }
      parentProduct {
        id
        slug
        name
        status
        variationAttributes {
          id
          name
          type
          options
        }
      }
    }
    ... on VariableProduct {
      childCount
      childPublishedCount
      attributes {
        id
        attribute {
          id
          name
          type
          options
        }
        value
        media {
          id
          type
          filename
          path
        }
      }
      tags {
        id
        name
      }
    }
  }
  ${Z}
`;s`
  query product($id: ObjectID!) @api(name: "zim") {
    productAdmin(id: $id) {
      ...ProductBaseField
      ...ProductDetailFields
    }
  }
  ${oe}
`;s`
  query productPagination(
    $searchString: String
    $statuses: [ProductStatus]
    $orderBy: ProductOrderBy
    $order: OrderDirection
    $page: Int
    $limit: Int
    $parentProduct: ObjectID
    $tagSlugs: [String]
    $flatten: Boolean
    $simpleOnly: Boolean
    $tagIds: [ObjectID]
  ) @api(name: "zim") {
    productPagination(
      searchString: $searchString
      statuses: $statuses
      parentProduct: $parentProduct
      order: $order
      orderBy: $orderBy
      page: $page
      limit: $limit
      tagSlugs: $tagSlugs
      flatten: $flatten
      simpleOnly: $simpleOnly
      tagIds: $tagIds
    ) {
      totalPages
      totalDocs
      docs {
        ...ProductBaseField
      }
    }
  }
  ${Z}
`;s`
  query publishedProducts(
    $search: String
    $after: String
    $before: String
    $first: Int
    $last: Int
  ) @api(name: "zim") {
    publishedProducts(
      after: $after
      before: $before
      first: $first
      last: $last
      search: $search
    ) {
      totalCount
      pageInfo {
        hasNextPage
        hasPreviousPage
        startCursor
        endCursor
      }
      edges {
        cursor
        node {
          ...ProductBaseField
        }
      }
    }
  }
  ${Z}
`;s`
  query productVariants($parentProductId: ObjectID!) @api(name: "zim") {
    productVariants(parentProductId: $parentProductId) {
      ...ProductBaseField
    }
  }
  ${Z}
`;s`
  query (
    $searchString: String
    $excludeIds: [ObjectID]
    $page: Int
    $limit: Int
  ) @api(name: "zim") {
    productAttributePagination(
      page: $page
      limit: $limit
      excludeIds: $excludeIds
      searchString: $searchString
    ) {
      totalDocs
      docs {
        id
        name
        options
        type
      }
    }
  }
`;s`
  query ($id: ObjectID!) @api(name: "zim") {
    productAttribute(id: $id) {
      id
      name
      options
      type
    }
  }
`;s`
  query @api(name: "zim") {
    productCategoriesTree {
      id
      title
      slug
      parentCategory {
        id
        title
        slug
      }
      children {
        id
        title
        slug
        parentCategory {
          id
          title
          slug
        }
      }
    }
  }
`;s`
  query productCategories($search: String, $parent: ObjectID)
  @api(name: "zim") {
    productCategories(search: $search, parent: $parent) {
      id
      title
      slug
      parentCategory {
        id
        title
        slug
      }
      children {
        id
        title
        slug
        parentCategory {
          id
          title
          slug
        }
      }
    }
  }
`;s`
  query slugExist($slug: String!, $type: SlugType, $refId: ObjectID)
  @api(name: "zim") {
    slugExists(type: $type, slug: $slug, refId: $refId)
  }
`;s`
  query titleExists($title: String!, $type: SlugType, $refId: ObjectID)
  @api(name: "zim") {
    titleExists(type: $type, title: $title, refId: $refId)
  }
`;s`
  query postViewReport(
    $startTime: DateTime
    $endTime: DateTime
    $page: Int
    $limit: Int
    $userId: Int
  ) @api(name: "zim") {
    postViewReport(
      startTime: $startTime
      endTime: $endTime
      page: $page
      limit: $limit
      userId: $userId
    ) {
      totalDocs
      docs {
        post {
          id
          title
          slug
          user {
            id
            fullName
          }
        }
        viewCount
      }
    }
  }
`;s`
  query generalPostViewByDate(
    $range: FromToDateInput
    $dateViewMode: DateViewMode
  ) @api(name: "zim") {
    generalPostViewByDate(range: $range, dateViewMode: $dateViewMode) {
      label
      viewCount
      postCount
    }
  }
`;s`
  query postViewReportByUser(
    $startTime: DateTime
    $endTime: DateTime
    $page: Int
    $limit: Int
  ) @api(name: "zim") {
    postViewReportByUser(
      startTime: $startTime
      endTime: $endTime
      page: $page
      limit: $limit
    ) {
      totalDocs
      docs {
        user {
          id
          fullName
        }
        postCount
        viewCount
      }
    }
  }
`;s`
  query postReportBySpecificUser(
    $userId: Int!
    $startTime: DateTime
    $endTime: DateTime
  ) @api(name: "zim") {
    postReportBySpecificUser(
      startTime: $startTime
      endTime: $endTime
      userId: $userId
    ) {
      totalLifetimePosts
      totalLifetimePublishedPosts
      totalLifetimeViews
      totalPosts
      totalPublishedPosts
    }
  }
`;s`
  query postReportByStatus($userId: Int) @api(name: "zim") {
    postReportByStatus(userId: $userId) {
      status
      count
    }
  }
`;s`
  query postByUser(
    $page: Int
    $limit: Int
    $order: OrderDirection
    $orderBy: PostByUserSort
    $userFilter: UserWithPostUserFilter
  ) @api(name: "zim") {
    postByUser(
      page: $page
      limit: $limit
      order: $order
      orderBy: $orderBy
      userFilter: $userFilter
    ) {
      limit
      page
      totalPages
      totalDocs
      docs {
        user {
          id
          email
          fullName
          avatar
          role
        }
        reviewsAverage
        postCount
        viewCount
        employeeInfo {
          id
          showInFrontPage
          position
          priority
          userId
          user {
            id
            avatar
            role
            username
            fullName
          }
        }
      }
    }
  }
`;s`
  query commentsPagination(
    $type: CommentType!
    $refId: String!
    $parent: ObjectID
    $page: Int
  ) @api(name: "zim") {
    commentsPagination(
      type: $type
      refId: $refId
      parent: $parent
      page: $page
    ) {
      totalDocs
      totalPages
      limit
      page
      docs {
        ... on PostComment {
          id
          content
          numOfLikes
          createdAt
          numOfChildComments
          user {
            id
            email
            avatar
            fullName
          }
        }
      }
    }
  }
`;s`
  query comments(
    $first: Int
    $type: CommentType!
    $refId: String!
    $parent: ObjectID
    $after: String
  ) @api(name: "zim") {
    comments(
      type: $type
      refId: $refId
      parent: $parent
      first: $first
      after: $after
    ) {
      pageInfo {
        hasNextPage
        hasPreviousPage
        endCursor
        startCursor
      }
      totalCount
      edges {
        node {
          ... on PostComment {
            id
            content
            numOfLikes
            createdAt
            numOfChildComments
            user {
              id
              email
              avatar
              fullName
            }
          }
        }
        cursor
      }
    }
  }
`;s`
  query getUserList($roleIds: [Int], $search: String) @api(name: "zim") {
    users(role_ids: $roleIds, search: $search) {
      totalDocs
      docs {
        id
        fullName
        role
      }
    }
  }
`;s`
  query user($userId: Int!) @api(name: "zim") {
    user(id: $userId) {
      id
      fullName
      avatar
      role
      roleId
      username
      birthday
      gender
    }
  }
`;const rr=s`
  query getListStudentsPagination($input: TicketGetListStudentsInputType)
  @api(name: "appZim") {
    getListStudentsPagination(input: $input) {
      hasNextPage
      hasPrevPage
      limit
      page
      pageNext
      pagePrev
      pagingCounter
      totalPages
      totalDocs
      docs {
        supportUId
        userId
        schoolAddress
        course {
          name
        }
        status
        user {
          id
          user_name
          full_name
          phone
          address
          email
          role_id
          role_name
          status
          avatar
          supporter
        }
      }
    }
  }
`,yt=s`
  fragment AdvertisementBaseFields on Advertisement {
    id
    type
    status
    title
    description
    image {
      id
      filename
      path
      width
      height
    }
    url
    clicks
    showIn
    startDate
    endDate
    createdAt
    updatedAt
  }
`,Wi=s`
  fragment AdvertisementDetailFields on Advertisement {
    ...AdvertisementBaseFields
    postCategories {
      id
      title
    }
    productCategories {
      id
      title
    }
    posts {
      id
      title
    }
    tags {
      id
      name
    }
    products {
      ... on SimpleProduct {
        id
        name
      }
      ... on VariableProduct {
        id
        name
      }
    }
    excludeUrls
    position
    tagHtml
    tagPosition
  }
  ${yt}
`;s`
  query advertisementPagination(
    $page: Int
    $limit: Int
    $order: OrderDirection
    $orderBy: AdvertisementSortBy
    $statuses: [AdvertisementStatus]
  ) @api(name: "zim") {
    advertisementPagination(
      page: $page
      limit: $limit
      order: $order
      orderBy: $orderBy
      statuses: $statuses
    ) {
      totalDocs
      totalPages
      docs {
        ...AdvertisementBaseFields
      }
    }
  }
  ${yt}
`;s`
  query advertisement($id: ObjectID!) @api(name: "zim") {
    advertisement(id: $id) {
      ...AdvertisementDetailFields
    }
  }
  ${Wi}
`;s`
  mutation createProductCategory($input: ProductCategoryInput!)
  @api(name: "zim") {
    createProductCategory(input: $input) {
      id
      title
      slug
    }
  }
`;s`
  mutation updateProductCategory(
    $id: ObjectID!
    $input: UpdateProductCategoryInput!
  ) @api(name: "zim") {
    updateProductCategory(id: $id, input: $input) {
      id
      title
    }
  }
`;s`
  mutation deleteProductCategory($id: ObjectID!) @api(name: "zim") {
    deleteProductCategory(id: $id)
  }
`;s`
  mutation ($name: String!, $type: ProductAttributeType!, $options: [String])
  @api(name: "zim") {
    createProductAttribute(name: $name, type: $type, options: $options) {
      id
      type
      name
      options
    }
  }
`;s`
  mutation (
    $id: ObjectID!
    $name: String
    $type: ProductAttributeType
    $options: [String]
  ) @api(name: "zim") {
    updateProductAttribute(
      id: $id
      name: $name
      type: $type
      options: $options
    ) {
      id
      type
      name
      options
    }
  }
`;s`
  mutation (
    $type: ProductType!
    $sku: String!
    $description: String!
    $shortDescription: String!
    $name: String!
    $featured: Boolean
    $categories: [ObjectID]
    $images: [ObjectID]
    $attributes: [ProductAttributeInput]
    $tags: [ObjectID]
    $variationAttributes: [ObjectID]
    $regularPrice: Int
    $salePrice: Int
    $salePriceFrom: DateTime
    $salePriceTo: DateTime
    $downloads: [ObjectID]
    $downloadable: Boolean
  ) @api(name: "zim") {
    createProduct(
      input: {
        type: $type
        sku: $sku
        description: $description
        shortDescription: $shortDescription
        name: $name
        featured: $featured
        categories: $categories
        images: $images
        attributes: $attributes
        tags: $tags
        variationAttributes: $variationAttributes
        regularPrice: $regularPrice
        salePrice: $salePrice
        salePriceFrom: $salePriceFrom
        salePriceTo: $salePriceTo
        downloads: $downloads
        downloadable: $downloadable
      }
    ) {
      ...ProductBaseField
      ...ProductDetailFields
    }
  }
  ${oe}
`;s`
  mutation (
    $id: ObjectID!
    $description: String!
    $shortDescription: String!
    $name: String!
    $featured: Boolean
    $categories: [ObjectID]
    $images: [ObjectID]
    $attributes: [ProductAttributeInput]
    $tags: [ObjectID]
    $regularPrice: Int
    $salePrice: Int
    $salePriceFrom: DateTime
    $salePriceTo: DateTime
    $downloads: [ObjectID]
    $downloadable: Boolean
    $variationAttributes: [ObjectID]
    $slug: String
    $sku: String
  ) @api(name: "zim") {
    updateProduct(
      id: $id
      input: {
        description: $description
        shortDescription: $shortDescription
        name: $name
        featured: $featured
        categories: $categories
        images: $images
        attributes: $attributes
        tags: $tags
        regularPrice: $regularPrice
        salePrice: $salePrice
        salePriceFrom: $salePriceFrom
        salePriceTo: $salePriceTo
        downloads: $downloads
        downloadable: $downloadable
        variationAttributes: $variationAttributes
        slug: $slug
        sku: $sku
      }
    ) {
      ...ProductBaseField
      ...ProductDetailFields
    }
  }
  ${oe}
`;s`
  mutation (
    $id: ObjectID!
    $description: String
    $shortDescription: String
    $name: String
    $categories: [ObjectID]
    $images: [ObjectID]
    $attributes: [ProductAttributeInput]
    $tags: [ObjectID]
    $regularPrice: Int
    $salePrice: Int
    $salePriceFrom: DateTime
    $salePriceTo: DateTime
    $downloads: [ObjectID]
    $downloadable: Boolean
    $slug: String
    $sku: String
  ) @api(name: "zim") {
    updateProductVariants(
      id: $id
      input: {
        description: $description
        shortDescription: $shortDescription
        name: $name
        categories: $categories
        images: $images
        attributes: $attributes
        tags: $tags
        regularPrice: $regularPrice
        salePrice: $salePrice
        salePriceFrom: $salePriceFrom
        salePriceTo: $salePriceTo
        downloads: $downloads
        downloadable: $downloadable
        slug: $slug
        sku: $sku
      }
    ) {
      ...ProductBaseField
      ...ProductDetailFields
    }
  }
  ${oe}
`;s`
  mutation ($parentProduct: ObjectID!, $input: AddVariantProductInput!)
  @api(name: "zim") {
    createProductVariants(parentProduct: $parentProduct, input: $input) {
      ...ProductBaseField
      ...ProductDetailFields
    }
  }
  ${oe}
`;s`
  mutation ($id: ObjectID!) @api(name: "zim") {
    publishProduct(id: $id) {
      ...ProductBaseField
    }
  }
  ${Z}
`;s`
  mutation ($id: ObjectID!) @api(name: "zim") {
    unPublishProduct(id: $id) {
      ...ProductBaseField
    }
  }
  ${Z}
`;s`
  mutation ($productId: ObjectID!, $inventories: [InventoryInput]!)
  @api(name: "zim") {
    bulkUpdateProductInventory(
      productId: $productId
      inventories: $inventories
    ) {
      id
      inStock
      inventories {
        id
        inStock
        inventory
        reserved
        manageStock
        store {
          id
          name
        }
      }
    }
  }
`;s`
  mutation ($id: ObjectID!) @api(name: "zim") {
    removeProductVariants(id: $id) {
      ...ProductBaseField
    }
  }
  ${Z}
`;s`
  fragment RecruitmentPostBaseField on RecruitmentPost {
    id
    title
    area
    status
    position
    currentNumOfReplies
    maxCandidates
    startDate
    endDate
    createdAt
    updatedAt
    slug
  }
`;const Vi=s`
  fragment RecruitmentPostDetailsField on RecruitmentPost {
    id
    title
    area
    position
    numOfCandidates
    maxCandidates
    jobDescription
    currentNumOfReplies
    salaryFrom
    slug
    salaryType
    salaryTo
    status
    endDate
    createdAt
    updatedAt
    startDate
    seo {
      title
      description
      ogDescription
      ogImage {
        id
        path
      }
      ogTitle
      publisher
      noIndex
      noFollow
      canonicalUrl
      customHeadHtml
    }
  }
`;s`
  mutation ($postId: ObjectID!, $seo: PostSeoInput!) @api(name: "zim") {
    updatePostSeoConfig(postId: $postId, seo: $seo) {
      id
      title
      slug
      seo {
        title
        description
        ogDescription
        ogImage {
          id
          path
        }
        ogTitle
        publisher
        noIndex
        noFollow
        canonicalUrl
        customHeadHtml
      }
      createdAt
      updatedAt
    }
  }
`;s`
  mutation ($productId: ObjectID!, $seo: PostSeoInput!) @api(name: "zim") {
    updateProductSeoConfig(productId: $productId, seo: $seo) {
      ... on SimpleProduct {
        id
        createdAt
        updatedAt
        seo {
          title
          description
          ogDescription
          ogImage {
            id
            path
          }
          ogTitle
          publisher
          noIndex
          noFollow
          canonicalUrl
          customHeadHtml
        }
      }
      ... on VariableProduct {
        id
        createdAt
        updatedAt
        seo {
          title
          description
          ogDescription
          ogImage {
            id
            path
          }
          ogTitle
          publisher
          noIndex
          noFollow
          canonicalUrl
          customHeadHtml
        }
      }
    }
  }
`;s`
  mutation ($recruitmentPostId: ObjectID!, $seo: CommonSeoInput!)
  @api(name: "zim") {
    updateRecruitmentPostSeoConfig(
      recruitmentPostId: $recruitmentPostId
      seo: $seo
    ) {
      ...RecruitmentPostDetailsField
    }
  }
  ${Vi}
`;s`
  query redirectUrlPagination($page: Int, $limit: Int, $search: String)
  @api(name: "zim") {
    redirectUrlPagination(page: $page, limit: $limit, search: $search) {
      docs {
        id
        prevUrl
        newUrl
        redirectCode
        createdAt
        updatedAt
      }
      totalPages
      totalDocs
    }
  }
`;s`
  mutation updateRedirectUrl($id: ObjectID!, $input: UpdateRedirectUrlInput!)
  @api(name: "zim") {
    updateRedirectUrl(id: $id, input: $input) {
      id
      prevUrl
      newUrl
      redirectCode
      createdAt
      updatedAt
    }
  }
`;s`
  mutation deleteRedirectUrl($id: ObjectID!) @api(name: "zim") {
    deleteRedirectUrl(id: $id)
  }
`;s`
  mutation createRedirectUrl($input: CreateRedirectUrlInput!)
  @api(name: "zim") {
    createRedirectUrl(input: $input) {
      id
      prevUrl
      newUrl
      redirectCode
      createdAt
      updatedAt
    }
  }
`;function Zi(e){const a=e+"=",r=decodeURIComponent(document.cookie).split(";");for(let o=0;o<r.length;o++){let i=r[o];for(;i.charAt(0)===" ";)i=i.substring(1);if(i.indexOf(a)===0)return i.substring(a.length,i.length)}return""}const Ki=!0,Xi="";console.log({token:Xi});const Qi=`
    mutation {
        refreshToken {
            token
            refreshToken
        }
    }
`,Ji=e=>{},en=()=>{},ge=()=>(console.log({isModuleMode:Ki}),Zi("token")),tn=Kt(({graphQLErrors:e,networkError:a})=>{e&&e.map(({message:n,locations:r,path:o})=>console.log(`[GraphQL error]: Message: ${n}, Location: ${r}, Path: ${o}`)),a&&console.log(`[Network error]: ${a}`)}),an=new Ce((e,a)=>new Xt(n=>{let r;return Promise.resolve(e).then(o=>{let i="";typeof window!="undefined"&&(i=localStorage.getItem("guest_id")),o.setContext({headers:{authorization:`Bearer ${ge()}`,"Access-Control-Allow-Credentials":!0,"x-guest-id":i}})}).then(()=>{r=a(e).subscribe({next:n.next.bind(n),error:n.error.bind(n),complete:n.complete.bind(n)})}).catch(n.error.bind(n)),()=>{r&&r.unsubscribe()}})),nn=new Qt({possibleTypes:{BaseProduct:["VariableProduct","SimpleProduct"]},typePolicies:{Query:{fields:{medias:Ie(["type","width","height","search"]),tags:Ie(["search"]),comments:Ie(["type","refId","parent"])}}}}),rn=new Jt({endpoints:{zim:"https://graph-api.zim.vn",appZim:"https://graphnet-api.zim.vn",analytics:{}.REACT_APP_ANALYTICS_API_ENDPOINT},createHttpLink:()=>ea({credentials:"include"}),wsSuffix:"/graphql",createWsLink:e=>(console.log({endpoint:e}),new Ze({uri:e,options:{reconnect:!0,connectionParams:()=>({authToken:ge()})}}))}),on=async()=>{const e={operationName:null,variables:{},query:Qi};return fetch("https://graph-api.zim.vn/graphql",{method:"POST",credentials:"include",body:JSON.stringify(e),headers:{"Access-Control-Request-Headers":"content-type","Content-Type":"application/json; charset=utf-8","x-no-compression":"true"}}).then(async a=>{const n=await a.json();return n==null?void 0:n.data.refreshToken})},Ct=new ta({accessTokenField:"token",isTokenValidOrUndefined:()=>{const e=ge();if(typeof e=="string"&&e.length===0)return!0;try{const{exp:a}=aa(e);return Date.now()<a*1e3}catch{return!1}},fetchAccessToken:on,handleFetch:e=>{e?localStorage.setItem("isAuthenticated","true"):localStorage.removeItem("isAuthenticated")},handleResponse:()=>{},handleError:e=>{console.log(`handleError: ${e}`)}}),sn=ia(({query:e})=>{const a=na(e);return a.kind==="OperationDefinition"&&a.operation==="subscription"},Ce.from([Ct,new Ze({uri:ra("https://graph-api.zim.vn","graphql").replace("http","ws"),options:{reconnect:!0,connectionParams:()=>({authToken:ge()})}})]),Ce.from([Ct,an,tn,rn])),we=new oa({ssrMode:typeof window=="undefined",link:sn,credentials:"include",cache:nn,connectToDevTools:!0}),It=l.exports.createContext({appState:{isAuthenticated:!1,user:null,loaded:!1}}),ln=({children:e})=>{const[a,n]=l.exports.useState({isAuthenticated:!1,user:null,loaded:!1,buildMode:"module"}),r=async()=>{var u,g;try{const m=await we.query({query:Ui,fetchPolicy:"network-only"});((u=m==null?void 0:m.data)==null?void 0:u.me)?(n(C(y({},a),{user:(g=m==null?void 0:m.data)==null?void 0:g.me,isAuthenticated:!0,loaded:!0})),localStorage.setItem("isAuthenticated","true")):(localStorage.removeItem("isAuthenticated"),n(C(y({},a),{user:null,isAuthenticated:!1,loaded:!0})))}catch{n(C(y({},a),{user:null,isAuthenticated:!1,loaded:!0})),localStorage.removeItem("isAuthenticated")}},o=async u=>{await r()},i=async()=>{await we.mutate({mutation:Yi}),n(C(y({},a),{user:null,isAuthenticated:!1,loaded:!0})),localStorage.removeItem("isAuthenticated")};return t(It.Provider,{value:{getUser:r,appState:a,setAppState:n,appSetLogin:o,appSetLogout:i,appSetAuthToken:Ji,appClearAuthToken:en},children:t(sa,{client:we,children:e})})};function dn(){return l.exports.useContext(It)}const Oe=e=>t(ce,y({rounded:"base",_hover:{textDecoration:"none"},as:la},e)),or={home:"/",listCourses:"/courses",courseDetail:"/courses/:id",forbiddenError:"/error/403",ticket:"/ticket",report:"/report",leadReport:"/report/lead"},he={lead:"/Admin/ContactCustomer/ContactList"},se=(e=!1)=>{const[a,n]=l.exports.useState(e),r=l.exports.useCallback(o=>n(i=>o!==void 0?o:!i),[]);return[a,r]},cn=(...e)=>{const a=l.exports.useRef();return l.exports.useEffect(()=>{e.forEach(n=>{!n||(typeof n=="function"?n(a.current):n.current=a.current)})},[e]),a},fe=l.exports.forwardRef((r,n)=>{var o=r,{error:e}=o,a=H(o,["error"]);return p(Ke,{isInvalid:!!e,children:[(a==null?void 0:a.label)&&t(Xe,{htmlFor:a.id,children:a.label}),t(Qe,C(y({bg:_("white","slate.700"),borderColor:_("gray.300","slate.600"),_hover:{borderColor:_("gray.400","slate.700")},color:_("gray.900","slate.300"),_placeholder:{color:_("gray.400","slate.500")}},a),{ref:n})),e&&t(Je,{children:e&&(e==null?void 0:e.message)})]})}),sr=l.exports.forwardRef((o,r)=>{var i=o,{placeholder:e,onSubmitSearch:a}=i,n=H(i,["placeholder","onSubmitSearch"]);const u=l.exports.useRef(null),g=cn(r,u),m=l.exports.useCallback(c=>{c.preventDefault();const{value:d}=g.current;typeof a=="function"&&a(d)},[]);return t(b,{as:"form",onSubmit:m,id:"form-header-search",w:"full",children:p(da,{flexGrow:1,w:"100%",children:[t(ca,{children:t(N,{p:0,variant:"ghost",_hover:{backgroundColor:"transparent"},type:"submit",children:t(ua,{fontSize:18})})}),t(fe,C(y({placeholder:"T\xECm ki\u1EBFm...",variant:"filled"},n),{ref:g,pr:10}))]})})});q(()=>U(()=>import("./index4.js"),["assets/index4.js","assets/vendor.js","assets/useUser.js","assets/index5.js","assets/index6.js","assets/index.css"]));q(()=>U(()=>import("./index7.js"),["assets/index7.js","assets/vendor.js","assets/index6.js","assets/index.css","assets/index5.js"]));q(()=>U(()=>import("./index8.js"),["assets/index8.js","assets/index2.css","assets/vendor.js","assets/index6.js","assets/index.css","assets/useUser.js"]));q(()=>U(()=>import("./index9.js"),["assets/index9.js","assets/vendor.js","assets/index4.js","assets/useUser.js","assets/index5.js","assets/index6.js","assets/index.css"]));q(()=>U(()=>import("./index10.js"),["assets/index10.js","assets/vendor.js"]));q(()=>U(()=>Promise.resolve().then(function(){return Qn}),void 0));const un=/^(?=.*\d)(?=.*[a-zA-Z])[\da-zA-Z_.\-@]{8,}$/,pn=/^\d+$/,mn=/(84|03|05|07|08|09|01[2|6|8|9])+([0-9]{8})\b/,gn=e=>{if(typeof e!="string")return"";const a=e.split(".");return a.length>0?a[a.length-1]:""};Q(De,"password",function(e){return this.matches(un,{message:e,excludeEmptyString:!0})});Q(De,"onlyNumber",function(e){return this.matches(pn,{message:e,excludeEmptyString:!0})});Q(De,"phone",function(e){return this.matches(mn,{message:e,excludeEmptyString:!0})});Q(Se,"fileSize",function(e,a){return this.test("fileSize",a,n=>Array.isArray(n)&&n.length>0&&n[0]?!n.map(o=>o.size>e):!0)});Q(Se,"fileType",function(e=[],a){return this.test("fileType",a,n=>e.length===0?!0:Array.isArray(n)&&n.length>0&&n[0]?!n.map(o=>e.includes(o.type)):!0)});Q(Se,"file",function({size:e,type:a},n){return this.test({name:"file",test:function(r){var u,g,m,c,d,$;const o=(u=a==null?void 0:a.value)!=null?u:[],i=(g=e==null?void 0:e.value)!=null?g:5*1024*1e3;if(Array.isArray(r)&&r.length>0&&r[0]){const k=r.find(E=>!o.includes(E.type)),x=r.find(E=>E.size>i);return k?this.createError({message:`${(m=a==null?void 0:a.message)!=null?m:"Kh\xF4ng h\u1ED7 tr\u1EE3 \u0111\u1ECBnh d\u1EA1ng"} ${gn((c=k==null?void 0:k.name)!=null?c:"")}`,path:this.path}):x?this.createError({message:($=(d=e==null?void 0:e.message)!=null?d:`Dung l\u01B0\u1EE3ng v\u01B0\u1EE3t qu\xE1 ${i/1024e3}`+(x==null?void 0:x.name))!=null?$:"",path:this.path}):!0}else return!0}})});Ee.object().shape({username:Ee.string().required("Vui l\xF2ng nh\u1EADp t\xE0i kho\u1EA3n"),password:Ee.string().required("Vui l\xF2ng nh\u1EADp m\u1EADt kh\u1EA9u")});const Et=({text:e="\u0110ang l\u1EA5y d\u1EEF li\u1EC7u...",color:a="gray.900"})=>p(A,{justifyContent:"center",pointerEvents:"none",userSelect:"none",children:[t(pa,{color:a}),t(T,{textAlign:"center",color:a,children:e})]});q(()=>U(()=>import("./index2.js"),["assets/index2.js","assets/useIsMounted.js","assets/vendor.js"]));q(()=>U(()=>import("./index3.js"),["assets/index3.js","assets/useIsMounted.js","assets/vendor.js"]));const hn={secondary:"#c89934",primary:"#3463c8",gray:{50:"#F8F8F8",100:"#F0F0F0",200:"#E6E6E6",300:"#D5D5D5",400:"#B0B0B0",500:"#909090",600:"#686868",700:"#555555",800:"#373737",900:"#171717"},secondBrand:{50:"#f8fae8",100:"#eff2c5",200:"#e5e9a0",300:"#dce07d",400:"#d6da66",500:"#d1d551",600:"#cfc449",700:"#ccae3f",800:"#c89934",900:"#c07722"},brand:{50:"#e4f2ff",100:"#bedeff",200:"#94caff",300:"#6bb5ff",400:"#4fa4ff",500:"#3463c8",600:"#2e44a8",700:"#243b9c",800:"#1b3190",900:"#081f7c"},pink:{50:"#f9e4ec",100:"#f2bcd0",200:"#eb92b1",300:"#e36993",400:"#dd4d7c",500:"#d93767",600:"#c83463",700:"#b2305d",800:"#9d2c58",900:"#77264c"},blue:{50:"#E1F2FC",100:"#B7DDFA",200:"#88C8F7",300:"#55B3F4",400:"#21A3F3",500:"#0093F1",600:"#0085E4",700:"#0074D1",800:"#0063BF",900:"#0045A1"},green:{50:"#E8F5E9",100:"#C8E6C9",200:"#A5D6A7",300:"#81C784",400:"#66BB6A",500:"#4CAF50",600:"#43A047",700:"#388E3C",800:"#2E7D32",900:"#1B5E20"},red:{50:"#FFEAED",100:"#FFCBD5",200:"#F1959E",300:"#E9686B",400:"#F54258",500:"#FC243E",600:"#EC163D",700:"#DA0037",800:"#CD002F",900:"#BF0022"},slate:{50:"#f8fafc",100:"#f1f5f9",200:"#e2e8f0",300:"#cbd5e1",400:"#94a3b8",500:"#64748b",600:"#475569",700:"#334155",800:"#1e293b",900:"#0f172a"}},fn={baseStyle:({colorMode:e})=>{const a=e==="light";return{display:"block",background:a?"white":"slate.800",gap:6,border:"1px solid",borderColor:a?"gray.200":"slate.700"}},variants:{rounded:{padding:8,borderRadius:"xl"},smooth:{padding:4,borderRadius:"base"}},defaultProps:{variant:"smooth"}},bn={baseStyle:({colorMode:e})=>{const a=e==="dark";return{table:{th:{background:a?"slate.700":"gray.50",lineHeight:1.5},td:{background:a?"slate.800":"white",lineHeight:1.5}}}},variants:{simple:({colorMode:e})=>{const n=e==="dark"?"slate.700":"gray.300";return{td:{borderBottomColor:n,paddingTop:2,paddingBottom:2,paddingInlineStart:4,paddingInlineEnd:4},th:{borderBottomColor:n,paddingInlineStart:4,paddingInlineEnd:4,paddingTop:2,paddingBottom:2}}}},defaultProps:{variant:"simple",size:"sm"}},$n={baseStyle:({colorMode:e})=>({textTransform:"capitalize",padding:"0.25rem 0.5rem ",borderRadius:4}),variants:{},defaultProps:{variant:"subtle",size:"sm"}},yn={baseStyle:{_invalid:{boxShadow:0}},variants:{outline:{display:"none"}},defaultProps:{variant:"outline"}},Cn={baseStyle:{_hover:{textDecoration:"none",color:"brand.500"}}};var In={Card:fn,Table:bn,Badge:$n,Input:yn,Link:Cn};const En={initialColorMode:"light",useSystemColorMode:!1},Dt=ma({config:En,colors:hn,styles:{global:e=>({"html, body":{color:e.colorMode==="dark"?"gray.100":"gray.700",lineHeight:1.5},a:{color:e.colorMode==="dark"?"blue.300":"blue.500"},ul:{paddingRight:1}})},semanticTokens:{text:{default:"gray.900",_dark:"slate.400"},colors:{error:{default:"red.500",_dark:"red.200"},"gray.500":{default:"gray.500",_dark:"slate.400"}}},fonts:{body:"'Roboto', sans-serif",heading:"'Arial', sans-serif",mono:"monospace"},components:C(y({},In),{FormLabel:{baseStyle:{fontSize:"sm",marginBottom:1}}})}),Dn=e=>t(ga,y({px:0,maxW:"7xl"},e)),Sn=({data:e})=>t(ha,{height:400,data:e,options:{responsive:!0,maintainAspectRatio:!1,interaction:{intersect:!1,mode:"index"},plugins:{legend:{reverse:!0,labels:{usePointStyle:!0,pointStyle:"circle",boxWidth:8,padding:15},onClick(a,n,r){const o=n.datasetIndex,i=this.chart,u=i.getDatasetMeta(o).hidden===null?!1:i.getDatasetMeta(o).hidden;i.data.datasets.forEach(function(g,m){const c=i.getDatasetMeta(m);m!==o?u?c.hidden===null&&(c.hidden=!0):c.hidden=c.hidden===null?!c.hidden:null:m===o&&(c.hidden=null)}),i.update()},position:"top"},title:{display:!0,text:"Lead Data Report"},tooltip:{enabled:!0,position:"average"}}}}),xn=({columns:e,data:a,loading:n=!1})=>{const{getTableProps:r,getTableBodyProps:o,headerGroups:i,prepareRow:u,rows:g}=et.exports.useTable({data:a,columns:e,autoResetPage:!0},et.exports.useSortBy);return t(b,{my:4,children:p(b,{as:fa,pos:"relative",overflow:"auto",className:"scroll-container",hideScrollbars:!1,children:[t(ue,{pos:"absolute",bg:"blackAlpha.700",top:0,left:0,right:0,bottom:0,zIndex:n?10:-1,justifyContent:"center",alignItems:"center",transition:"all .2s ease",opacity:n?1:0,children:t(Et,{color:"white"})}),p(ba,C(y({},r()),{children:[t($a,{children:i.map(m=>t(pe,C(y({},m.getHeaderGroupProps()),{children:m.headers.map(c=>t(ya,C(y({},c.getHeaderProps(c.getSortByToggleProps())),{pos:(c==null?void 0:c.isSticky)?"sticky":null,left:(c==null?void 0:c.stickyLeft)&&0,right:(c==null?void 0:c.stickyRight)&&0,zIndex:(c==null?void 0:c.isSticky)&&2,whiteSpace:(c==null?void 0:c.nowrap)&&"nowrap",children:p(A,{children:[t(T,{children:c.render("Header")}),t(Ca.span,{children:c.isSorted?c.isSortedDesc?t(Ia,{"aria-label":"sorted descending"}):t(Ea,{"aria-label":"sorted ascending"}):null})]})})))})))}),t(Da,C(y({},o()),{children:n&&(!a.length||a.length===0)?t(pe,{children:t(xe,{colSpan:e.length,children:t(b,{py:24})})}):a.length>0?g.map(m=>(u(m),t(pe,C(y({},m.getRowProps()),{children:m.cells.map(c=>{const d=c.column;return t(xe,C(y({},c.getCellProps()),{isNumeric:d==null?void 0:d.isNumeric,pos:(d==null?void 0:d.isSticky)?"sticky":null,left:(d==null?void 0:d.stickyLeft)&&0,right:(d==null?void 0:d.stickyRight)&&0,zIndex:(d==null?void 0:d.isSticky)&&2,whiteSpace:(d==null?void 0:d.nowrap)&&"nowrap",bgColor:(d==null?void 0:d.isSticky)?"gray.200":"white",overflow:"visible",minW:d==null?void 0:d.minWidth,className:(d==null?void 0:d.stickyRight)?"shadow-left":"",_after:{content:'""',position:"absolute",left:(d==null?void 0:d.stickyRight)?0:null,right:(d==null?void 0:d.stickyLeft)?0:null,top:0,height:"100%",width:1,boxShadow:`${(d==null?void 0:d.isSticky)&&(d==null?void 0:d.stickyRight)?"-1px":(d==null?void 0:d.stickyLeft)?"1px":0} 0px 0px 0px ${_("var(--chakra-colors-gray-300)","var(--chakra-colors-slate-700)")}`},children:c.render("Cell")}))})})))):t(pe,{children:t(xe,{colSpan:e.length,children:t(T,{textAlign:"center",color:"error",children:"Kh\xF4ng c\xF3 d\u1EEF li\u1EC7u ph\xF9 h\u1EE3p."})})})}))]}))]})})},St=(e,a)=>{const{appState:{buildMode:n}}=dn();return n==="module"?a:e};R.extend(Sa);const Tn=({data:e,labels:a})=>{const n=l.exports.useMemo(()=>[{Header:"Ng\xE0y",accessor:"",Cell:({data:r,row:o})=>{const{index:i}=o;return p(A,{justifyContent:"space-between",spacing:4,children:[t(T,{fontWeight:i===0?600:"normal",children:e[i].label}),t(T,{fontWeight:600,children:e[i].data.reduce((u,g)=>u+g,0)})]})},nowrap:!0,isSticky:!0,stickyLeft:!0},...a.map((r,o)=>({id:r+o,Header:()=>t(T,{children:r}),accessor:(i,u)=>{var g,m,c,d;return u===0?t(b,{fontWeight:600,children:St(t(Oe,{to:`${he.lead}?date=${encodeURIComponent(R(r,"DD/MM/YY").format("DD/MM/YYYY"))}`,children:(g=i.data)==null?void 0:g[o]}),t(ce,{href:`${he.lead}?date=${encodeURIComponent(R(r,"DD/MM/YY").format("DD/MM/YYYY").trim())}
                `,target:"_blank",children:(m=i.data)==null?void 0:m[o]}))}):St(t(Oe,{to:`${he.lead}?date=${encodeURIComponent(R(r,"DD/MM/YY").format("DD/MM/YYYY").trim())}&sourceName=${encodeURIComponent(i.label)}
                `,children:(c=i==null?void 0:i.data)==null?void 0:c[o]}),t(ce,{href:`${he.lead}?date=${encodeURIComponent(R(r,"DD/MM/YY").format("DD/MM/YYYY").trim())}&sourceName=${encodeURIComponent(i.label)}`,target:"_blank",children:(d=i==null?void 0:i.data)==null?void 0:d[o]}))},nowrap:!0,disableSortBy:!0,Cell:({value:i})=>t(T,{children:i})}))],[a]);return t(b,{children:t(xn,{columns:n,data:e})})},kn=s`
  query getReportLeads($startDate: String, $endDate: String)
  @api(name: "appZim") {
    getReportLeads(startDate: $startDate, endDate: $endDate) {
      date
      totalView
      dataLeads {
        date
        sourseName
        data
      }
    }
  }
`;tt(b)`
  .react-datepicker-wrapper {
    display: block;
  }

  .react-datepicker {
    display: flex;
    border: 0 !important;
    box-shadow: 4px 0px 8px 0px rgba(0, 0, 0, 0.15);
    .react-datepicker__time-container {
      border-color: var(--chakra-colors-gray-200);
    }
  }

  .react-datepicker__header {
    border-bottom: 0;
    background: var(--chakra-colors-gray-100);
  }

  .react-datepicker__day--today {
    background: var(--chakra-colors-yellow-200);
    color: var(--chakra-colors-gray-900);
    border-radius: 0.3rem;
  }

  .react-datepicker__day--selected {
    background: var(--chakra-colors-primary);
    color: #fff;
  }
  react-datepicker__tab-loop {
    .react-datepicker-popper {
      z-index: 11;
    }
  }

  .react-datepicker-popper[data-placement^='bottom']
    .react-datepicker__triangle,
  .react-datepicker-popper[data-placement^='bottom']
    .react-datepicker__triangle:before {
    border-bottom-color: var(--chakra-colors-gray-100);
  }
  .react-datepicker__input-container {
    border-color: var(--chakra-colors-gray-100);
  }
`;const Pn=l.exports.forwardRef((o,r)=>{var i=o,{error:e,label:a}=i,n=H(i,["error","label"]);return p(Ke,{isInvalid:!!e,children:[a&&t(Xe,{htmlFor:n.id,children:a}),t(xa,C(y({},n),{customInput:t(Qe,{autoComplete:"off"}),ref:r})),e&&t(Je,{children:e&&(e==null?void 0:e.message)})]})});var xt=l.exports.memo(Pn);const be=(e,a="DD/MM/YYYY")=>R(e).isValid()?R(e).format(a):"Ng\xE0y kh\xF4ng h\u1EE3p l\u1EC7",An=Ta.extend({addAttributes(){var e,a,n,r,o;return C(y({},(e=this.parent)==null?void 0:e.call(this)),{backgroundColor:{default:(n=(a=this.options)==null?void 0:a.backgroundColor)!=null?n:null,parseHTML:i=>i.getAttribute("data-background-color"),renderHTML:i=>({"data-background-color":i.backgroundColor,style:`background-color: ${i.backgroundColor}`})},style:{default:(o=(r=this.options)==null?void 0:r.style)!=null?o:null,parseHTML:i=>i.getAttribute("colwidth"),renderHTML:i=>({style:`${i.style?`width: ${i.colwidth}px`:null}`,colspan:i.colspan,rowspan:i.rowspan,colwidth:i.colwidth})}})}}),vn=ka.extend({content:"paragraph*",addAttributes(){var e;return C(y({},(e=this.parent)==null?void 0:e.call(this)),{cite:{default:this.options.cite},class:{default:this.options.class},title:{default:this.options.title}})}}),_n=Pa.extend({content:"paragraph*",addAttributes(){var e,a,n,r,o,i,u,g;return C(y({},(e=this.parent)==null?void 0:e.call(this)),{rel:{default:(n=(a=this==null?void 0:this.options)==null?void 0:a.rel)!=null?n:"noopener nofollow noreferrer"},target:{default:(o=(r=this==null?void 0:this.options)==null?void 0:r.target)!=null?o:"_blank"},"data-contextual":{default:((i=this==null?void 0:this.options)==null?void 0:i["data-contextual"])||void 0},"data-contextual-tag":{default:((u=this==null?void 0:this.options)==null?void 0:u["data-contextual-tag"])||void 0},"data-contextual-tag-id":{default:((g=this==null?void 0:this.options)==null?void 0:g["data-contextual-tag-id"])||void 0}})}}),wn=Aa.extend({addAttributes(){var e,a,n,r,o;return C(y({},(e=this.parent)==null?void 0:e.call(this)),{alt:{default:(n=(a=this==null?void 0:this.options)==null?void 0:a.alt)!=null?n:"image-alt"},title:{default:(o=(r=this==null?void 0:this.options)==null?void 0:r.title)!=null?o:"image-title"}})}}),On=l.exports.forwardRef((g,u)=>{var m=g,{activeKey:e="",activeOptions:a={},isActive:n=!1,icon:r,label:o=""}=m,i=H(m,["activeKey","activeOptions","isActive","icon","label"]);return r?t(me,{label:o,children:t(Te,y({ref:u,colorScheme:n?"brand":"gray",variant:"solid",fontSize:"24px",icon:r},i))}):t(me,{label:o,children:t(N,C(y({ref:u,colorScheme:n?"brand":"gray",variant:"solid"},i),{children:i.children}))})});var I=l.exports.memo(On);const Ln=[{name:"H1",value:1},{name:"H2",value:2},{name:"H3",value:3},{name:"H4",value:4},{name:"H5",value:5},{name:"H6",value:6}],Tt=l.exports.memo(()=>{const{editor:e}=l.exports.useContext(At);return e?t(ae,{children:p(ke,{children:[t(Pe,{children:t(I,{label:"H tag",icon:t(va,{})})}),t(Ae,{zIndex:99,p:0,children:t(at,{p:2,children:t(W,{gap:2,children:Ln.map(a=>t(I,{isActive:e.isActive("heading",{level:a.value}),activeKey:"heading",activeOptions:{level:a.value},onClick:()=>e.chain().focus().toggleHeading({level:a.value}).run(),children:a.name},a.value))})})})]})}):null}),Fn=l.exports.memo(({editor:e})=>{if(!e)return null;const a=l.exports.useCallback(()=>{e.chain().focus().insertTable({rows:3,cols:3,withHeaderRow:!0}).run()},[e]),n=l.exports.useCallback(()=>{e.chain().focus().deleteTable().run()},[e]),r=l.exports.useCallback(()=>{e.chain().focus().addColumnAfter().run()},[e]),o=l.exports.useCallback(()=>{e.chain().focus().addColumnBefore().run()},[e]),i=l.exports.useCallback(()=>{e.chain().focus().deleteColumn().run()},[e]),u=l.exports.useCallback(()=>{e.chain().focus().addRowBefore().run()},[e]),g=l.exports.useCallback(()=>{e.chain().focus().addRowAfter().run()},[e]),m=l.exports.useCallback(()=>{e.chain().focus().deleteRow().run()},[e]),c=l.exports.useCallback(()=>{e.chain().focus().mergeCells().run()},[e]),d=l.exports.useCallback(()=>{e.chain().focus().splitCell().run()},[e]),$=l.exports.useCallback(()=>{e.chain().focus().toggleHeaderColumn().run()},[e]);return p(ve,{minChildWidth:38,gap:2,p:2,bg:"gray.50",w:"full",maxW:350,children:[t(I,{onClick:a,icon:t(_a,{}),label:"Th\xEAm table"}),t(I,{onClick:n,icon:t(wa,{}),label:"X\xF3a table"}),t(I,{onClick:r,icon:t(Oa,{}),label:"Th\xEAm c\u1ED9t b\xEAn ph\u1EA3i"}),t(I,{onClick:o,icon:t(La,{}),label:"Th\xEAm c\u1ED9t b\xEAn tr\xE1i"}),t(I,{onClick:i,icon:t(Fa,{}),label:"X\xF3a c\u1ED9t"}),t(I,{onClick:u,icon:t(Ra,{}),label:"Th\xEAm h\xE0ng ph\xEDa tr\xEAn"}),t(I,{onClick:g,icon:t(Ba,{}),label:"Th\xEAm h\xE0ng b\xEAn d\u01B0\u1EDBi"}),t(I,{onClick:m,icon:t(Ma,{}),label:"X\xF3a h\xE0ng"}),t(I,{onClick:c,icon:t(Na,{}),label:"G\u1ED9p c\xE1c \xF4"}),t(I,{onClick:d,icon:t(ja,{}),label:"T\xE1ch c\xE1c \xF4"}),t(I,{onClick:$,icon:t(za,{}),label:"Toggle header c\u1ED9t"}),t(I,{onClick:()=>e.chain().focus().toggleHeaderRow().run(),icon:t(Ha,{}),label:"Toggle header h\xE0ng"}),t(I,{onClick:()=>e.chain().focus().toggleHeaderCell().run(),icon:t(qa,{}),label:"Toggle header cell"})]})}),kt=l.exports.memo(({editor:e})=>e?p(ke,{children:[t(Pe,{children:t(I,{label:"Table",icon:t(Ua,{})})}),t(Ae,{zIndex:99,p:0,children:t(at,{p:0,children:t(Fn,{editor:e})})})]}):null),Pt=l.exports.memo(({editor:e})=>{const[a,n]=l.exports.useState(!1);return t(ae,{children:t(I,{activeKey:"image",onClick:()=>{n(!0)},icon:t(Ya,{}),label:"H\xECnh \u1EA3nh"})})}),Rn=l.exports.memo(({editor:e,stickyMenuBar:a})=>{const n=_("white","slate.700"),r=_("gray.200","slate.600");return t(ae,{children:p(ve,{minChildWidth:"38px",gap:2,w:"full",bg:n,zIndex:10,px:2,py:2,pos:a?"sticky":"static",top:0,borderBottom:"1px solid",borderBottomColor:r,children:[t(Tt,{}),t(I,{onClick:()=>e.chain().focus().toggleBold().run(),activeKey:"bold",label:"In \u0111\u1EADm",icon:t(Ga,{})}),t(I,{onClick:()=>e.chain().focus().toggleItalic().run(),activeKey:"italic",label:"In nghi\xEAng",icon:t(Wa,{})}),t(I,{onClick:()=>e.chain().focus().toggleUnderline().run(),activeKey:"underline",label:"G\u1EA1ch ch\xE2n",icon:t(Va,{})}),t(I,{onClick:()=>e.chain().focus().toggleStrike().run(),activeKey:"strike",label:"G\u1EA1ch ngang",icon:t(Za,{})}),t(I,{onClick:()=>e.chain().focus().setTextAlign("left").run(),label:"C\u0103n l\u1EC1 tr\xE1i",icon:t(Ka,{})}),t(I,{onClick:()=>e.chain().focus().setTextAlign("center").run(),label:"C\u0103n gi\u1EEFa",icon:t(Xa,{})}),t(I,{onClick:()=>e.chain().focus().setTextAlign("right").run(),label:"C\u0103n l\u1EC1 ph\u1EA3i",icon:t(Qa,{})}),t(I,{onClick:()=>e.chain().focus().toggleBulletList().run(),label:"Danh s\xE1ch bullet",icon:t(it,{})}),t(I,{onClick:()=>e.chain().focus().toggleOrderedList().run(),label:"Danh s\xE1ch s\u1ED1 th\u1EE9 t\u1EF1",icon:t(nt,{})}),t(kt,{editor:e}),t(Pt,{editor:e})]})})});l.exports.memo(({editor:e})=>p(ve,{minChildWidth:38,gap:2,p:2,bg:"gray.50",w:{base:300,sm:300,md:450},children:[t(Tt,{}),t(I,{onClick:()=>e.chain().focus().toggleBulletList().run(),label:"Danh s\xE1ch bullet",icon:t(it,{}),isActive:e.isActive("bulletList")}),t(I,{onClick:()=>e.chain().focus().toggleOrderedList().run(),label:"Danh s\xE1ch s\u1ED1 th\u1EE9 t\u1EF1",icon:t(nt,{}),isActive:e.isActive("orderedList")}),t(kt,{editor:e}),t(Pt,{editor:e})]}));const Bn=l.exports.memo(({editor:e})=>{const{onOpen:a,onClose:n,isOpen:r}=Ja(),o=l.exports.useRef(null),[i,u]=l.exports.useState(""),[g,m]=l.exports.useState(""),c=async()=>{const d=e.getAttributes("image").src;e.chain().focus().setImage({src:d,alt:g,title:i}).run(),n()};return l.exports.useEffect(()=>{e.isActive("image")?(m(e.getAttributes("image").alt),u(e.getAttributes("image").title)):(m(""),u(""))},[r]),p(A,{bg:"white",shadow:"base",p:2,spacing:4,rounded:4,children:[p(b,{fontSize:"sm",children:[p(T,{as:"div",children:[t("strong",{children:"Title:"})," ",e.getAttributes("image").title]}),p(T,{as:"div",children:[t("strong",{children:"Alt:"})," ",e.getAttributes("image").alt]})]}),p(ke,{isOpen:r,initialFocusRef:o,onOpen:a,onClose:n,placement:"top",closeOnBlur:!0,children:[t(Pe,{children:t(I,{activeKey:"image",label:"Ch\u1EC9nh s\u1EEDa",icon:t(ei,{})})}),t(Ae,{children:t(b,{p:4,bg:"white",shadow:"base",children:p(ue,{spacing:4,alignItems:"flex-start",children:[t(fe,{ref:o,label:"Title",id:"title-url",value:i,onChange:d=>u(d.target.value),autoComplete:"off"}),t(fe,{label:"Alt",id:"alt-url",value:g,onChange:d=>m(d.target.value),autoComplete:"off"}),t(N,{colorScheme:"teal",onClick:c,children:"C\u1EADp nh\u1EADt"})]})})})]})]})});var Mn=_e.create({name:"iframe",group:"block",atom:!0,defaultOptions:{allowFullscreen:!0,HTMLAttributes:{class:"iframe-wrapper"}},addAttributes(){return{class:{default:null},src:{default:null},frameborder:{default:0},allowfullscreen:{default:this.options.allowFullscreen,parseHTML:()=>this.options.allowFullscreen},width:{default:this.options.width},height:{default:this.options.height}}},parseHTML(){return[{tag:"iframe"}]},renderHTML({HTMLAttributes:e}){return["div",this.options.HTMLAttributes,["iframe",e]]},addCommands(){return{setIframe:e=>({tr:a,dispatch:n})=>{const{selection:r}=a,o=this.type.create(e);return n&&a.replaceRangeWith(r.from,r.to,o),!0}}}});_e.create({name:"video",group:"block",atom:!0,defaultOptions:{HTMLAttributes:{class:"iframe-wrapper"}},addAttributes(){return{class:{default:this.options.class},src:{default:null},autoplay:{default:this.options.width},loop:{default:this.options.loop},muted:{default:this.options.muted},preload:{default:this.options.preload},width:{default:this.options.width},height:{default:this.options.height},controls:{default:this.options.controls}}},parseHTML(){return[{tag:"video"}]},renderHTML({HTMLAttributes:e}){return["div",this.options.HTMLAttributes,["video",e]]},addCommands(){return{Video:e=>({tr:a,dispatch:n})=>{const{selection:r}=a,o=this.type.create(e);return n&&a.replaceRangeWith(r.from,r.to,o),!0}}}});var Nn=_e.create({name:"spacing",content:"inline*",atom:!0,draggable:!0,defaultOptions:{HTMLAttributes:{class:"space-wrapper","data-question":"","data-question-group":"","data-label":"","data-type":""}},addOptions(){return{inline:!0,HTMLAttributes:{}}},inline(){return this.options.inline},group(){return this.options.inline?"inline":"block"},addAttributes(){var e,a,n,r,o,i,u;return{class:{default:((e=this==null?void 0:this.options)==null?void 0:e.class)||void 0},"data-question":{default:(n=(a=this==null?void 0:this.options)==null?void 0:a["data-question"])!=null?n:"",parseHTML:g=>g.getAttribute("data-question")},"data-question-group":{default:(o=(r=this==null?void 0:this.options)==null?void 0:r["data-question-group"])!=null?o:"",parseHTML:g=>g.getAttribute("data-question-group")},"data-type":{default:(u=(i=this==null?void 0:this.options)==null?void 0:i["data-type"])!=null?u:"",parseHTML:g=>g.getAttribute("data-type")},"data-label":{default:"",parseHTML:g=>g.getAttribute("data-label"),renderHTML:g=>g["data-label"]?{"data-label":g["data-label"]}:{"data-label":""}}}},parseHTML(){return[{tag:"span.space-wrapper"}]},renderHTML({HTMLAttributes:e}){return["span",ti(this.options.HTMLAttributes,e)]},renderText({HTMLAttributes:e}){var a;return(a=e==null?void 0:e["data-label"])!=null?a:"r\u1ED7ng"},addCommands(){return{addSpace:e=>({commands:a})=>a.insertContent({type:this.name,attrs:e})}}});function jn(e,a){const[n,r]=l.exports.useState(e);return l.exports.useEffect(()=>{const o=setTimeout(()=>{r(e)},a);return()=>{clearTimeout(o)}},[e,a]),n}const At=l.exports.createContext({editor:null}),zn=l.exports.forwardRef(({onChange:e,placeholder:a="Nh\u1EADp n\u1ED9i dung",defaultValue:n="",extensions:r=[],enableSpaceMenu:o=!1,showWordCount:i=!0,isDisabled:u=!1,stickyMenuBar:g=!1},m)=>{const[c,d]=l.exports.useState(0),$=ai({extensions:[ii,ni.configure({resizable:!0}),ri,oi,An,vn,si,li,wn.configure({inline:!0,HTMLAttributes:{class:"custom-image"}}),_n.configure({openOnClick:!1}),di.configure({placeholder:a||"Nh\u1EADp n\u1ED9i dung",emptyEditorClass:"is-editor-empty"}),Mn.configure({inline:!0}),ci.configure({types:["heading","paragraph"],alignments:["left","right","center"]}),Nn.configure({inline:!0}),...r],content:n,onCreate:async({editor:D})=>{const B=D.state.doc.textContent.split(" ").length;d(B)},onUpdate:({editor:D})=>{const B=D.state.doc.textContent.split(" ").length;d(B);const Y=D.getHTML();typeof e=="function"&&e(Y)},editable:!u}),k=_("white","slate.700"),x=_("gray.200","slate.700"),E=jn($,1e3);return p(At.Provider,{value:{editor:E},children:[i&&p(b,{mb:4,children:["Word count: ",t("strong",{children:c})]}),t(b,{p:2,pt:0,borderColor:x,borderRadius:4,borderWidth:1,h:"full",bg:k,children:$&&p(ae,{children:[!u&&p(ae,{children:[t(Rn,{editor:$,enableSpaceMenu:o,stickyMenuBar:g}),t(rt,{editor:$,pluginKey:"bubbleImageMenu",shouldShow:({editor:D})=>D.isActive("image"),children:t(Bn,{editor:$})}),t(rt,{editor:$,tippyOptions:{placement:"bottom"},pluginKey:"bubbleTextSelection",shouldShow:({editor:D,view:B,state:Y})=>D.isActive("link",{"data-contextual":!0})})]}),t(ui,{editor:$,ref:m})]})})]})});var Le=l.exports.memo(zn);const $e=s`
  fragment Ticket on TicketType {
    id
    refId
    title
    description
    createDate
    status
    createBy {
      id
      user_name
      full_name
      status
      avatar
      supporter
    }
    receiver {
      id
      user_name
      full_name
      avatar
    }
    reactions {
      id
      ticketId
      reaction
      count
    }
  }
`,Hn=s`
  mutation createTicket($input: TicketInputType) @api(name: "appZim") {
    createTicket(input: $input) {
      ...Ticket
    }
  }
  ${$e}
`,vt=s`
  mutation updateTicket($input: TicketInputType) @api(name: "appZim") {
    updateTicket(input: $input) {
      ...Ticket
    }
  }
  ${$e}
`,_t=s`
  mutation updateTicketStatus($input: TicketUpdateStatusType)
  @api(name: "appZim") {
    updateTicketStatus(input: $input) {
      ...Ticket
    }
  }
  ${$e}
`;s`
  mutation createTicketReaction($input: TicketReactionInputType)
  @api(name: "appZim") {
    createTicketReaction(input: $input) {
      id
      ticketId
      reaction
      count
    }
  }
  ${$e}
`;const lr=e=>`${e} is required.`,Fe="00000000-0000-0000-0000-000000000000",dr={0:{short:"CN",long:"CN"},1:{short:"2",long:"T2"},2:{short:"3",long:"T3"},3:{short:"4",long:"T4"},4:{short:"5",long:"T5"},5:{short:"6",long:"T6"},6:{short:"7",long:"T7"}},cr={LISTENING:{short:"L",long:"Listening"},READING:{short:"R",long:"Reading"},SPEAKING:{short:"S",long:"Speaking"},WRITING:{short:"W",long:"Writing"},"WRITING TASK 1":{short:"WT1",long:"WT1"},"WRITING TASK 2":{short:"WT2",long:"WT2"}},Re={ALL:"all",INCOMING:"incoming",ONGOING:"ongoing",CLOSED:"closed"},ur={ALL:{label:"All",colorScheme:"gray"},INCOMING:{label:"Incoming",colorScheme:"green"},ONGOING:{label:"Ongoing",colorScheme:"orange"},FINISHED:{label:"Finished",colorScheme:"red"}},pr=[{label:"Incoming",value:Re.INCOMING},{label:"Ongoing",value:Re.ONGOING},{label:"Closed",value:Re.CLOSED}],mr=[{id:1,label:"Th\u1EE9 Hai",value:1},{id:2,label:"Th\u1EE9 Ba",value:1},{id:3,label:"Th\u1EE9 T\u01B0",value:3},{id:4,label:"Th\u1EE9 N\u0103m",value:4},{id:5,label:"Th\u1EE9 S\xE1u",value:5},{id:6,label:"Th\u1EE9 B\u1EA3y",value:6},{id:0,label:"Ch\u1EE7 nh\u1EADt",value:7}];var J;(function(e){e.PROCESSING="Processing",e.CLOSED="Closed",e.WAITING="Waiting",e.DELETED="Deleted"})(J||(J={}));const K={All:{label:"All",value:"All",colorScheme:"gray"},Processing:{label:"Processing",value:"Processing",colorScheme:"green"},Waiting:{label:"Waiting",value:"Waiting",colorScheme:"orange"},Closed:{label:"Closed",value:"Closed",colorScheme:"red"},Deleted:{label:"Deleted",value:"Deleted",colorScheme:"black"}},gr={apointment:{label:"L\u1ECBch h\u1EB9n",value:"Apointment",colorScheme:"red"},finalTest:{label:"Test \u0111\u1EA7u v\xE0o",value:"FinalTest",colorScheme:"blue"},toDo:{label:"C\xF4ng vi\u1EC7c",value:"ToDo",colorScheme:"orange"}};J.PROCESSING,J.WAITING,J.CLOSED,J.DELETED;const hr=[{id:1,label:"3 bu\u1ED5i/tu\u1EA7n"},{id:2,label:"4-6 bu\u1ED5i/tu\u1EA7n"},{id:3,label:"Tr\xEAn 6 bu\u1ED5i/tu\u1EA7n"}],Be={waiting:{tooltip:"Ticket waiting",icon:pi,color:"red"},longtime:{tooltip:"Longtime no see",icon:mi,color:"blue"}},qn=({type:e="waiting"})=>{const a=l.exports.useMemo(()=>Be==null?void 0:Be[e],[e]);return t(me,{hasArrow:!0,bg:_("slate.700","slate.700"),label:a.tooltip,"aria-label":"status tool tip",isDisabled:!(a==null?void 0:a.tooltip),color:_("slate.300","slate.300"),children:t(A,{spacing:1,color:`${a==null?void 0:a.color}.500`,children:t(b,{as:"span",children:a.icon&&t(V,{as:a.icon})})})})},Un=(e,a="log")=>(console==null?void 0:console[a])?console[a](e):null,Yn=tt(A)`
  .flip-card {
    perspective: 1000px;
    background: transparent;
  }
  .flip-card-inner {
    position: relative;
    width: 100%;
    height: 100%;
    text-align: center;
    transition: transform 0.6s;
    transform-style: preserve-3d;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
  }

  .flip-card:hover .flip-card-inner {
    transform: rotateY(180deg);
  }

  .flip-card-front,
  .flip-card-back {
    position: absolute;
    width: 100%;
    height: 100%;
    -webkit-backface-visibility: hidden;
    backface-visibility: hidden;
  }

  .flip-card-front {
    background-color: #bbb;
    color: black;
  }

  .flip-card-back {
    background-color: #2980b9;
    color: white;
    transform: rotateY(180deg);
    display: flex;
    align-items: center;
    justify-content: center;
  }
`,wt=({name:e,avatar:a,meta:n,lastContact:r="",badgeTitle:o="",badgeProps:i,isFlipTicketAvatarCard:u=!1,onClickFlipCard:g=null,avatarSize:m=10,badgeStatusType:c=null})=>(Un(c),p(Yn,{flexGrow:1,alignItems:"flex-start",spacing:4,children:[t(b,{className:"flip-card",cursor:g?"pointer":"normal",width:m,height:m,rounded:"full",onClick:g,flexShrink:0,overflow:"hidden",children:u?t(me,{label:"G\u1EEDi ticket ch\u0103m s\xF3c",children:p(b,{className:"flip-card-inner",children:[t(b,{className:"flip-card-front",children:t(ot,{src:a,objectFit:"cover",boxSize:m,flexShrink:0})}),t(b,{className:"flip-card-back",children:t(V,{as:gi,width:6,height:6})})]})}):t(ot,{src:a,objectFit:"cover",boxSize:m,flexShrink:0})}),p(ue,{justifyContent:"stretch",alignItems:"flex-start",spacing:0,children:[p(W,{alignItems:"flex-start",gap:2,children:[t(Oe,{to:"/",children:t(T,{fontWeight:600,children:e||""})}),o&&t(hi,C(y({size:"sm",colorScheme:"green",variant:"subtle",rounded:4},i),{children:o})),c&&t(qn,{type:c})]}),p(W,{gap:0,fontSize:"sm",direction:"column",color:"gray.500",children:[t(T,{children:n||""}),r&&p(T,{fontSize:"xs",children:["Last contact ",r||""]})]})]})]})),Ot=({data:e,onTicketUpdate:a})=>{const[n,r]=se(),X=e,{name:o,avatar:i,time:u,status:g,meta:m,question:c}=X,d=H(X,["name","avatar","time","status","meta","question"]),[$,k]=l.exports.useState(e.question.content),x=l.exports.useRef(),E=st(),[D,{loading:B}]=ie(vt,{onCompleted:v=>{const{updateTicket:S}=v;a(C(y({},e),{question:{title:S==null?void 0:S.title,content:S==null?void 0:S.description}})),E({title:"Th\xE0nh c\xF4ng!",description:"C\u1EADp nh\u1EADt n\u1ED9i dung th\xE0nh c\xF4ng.",status:"success",duration:3e3,isClosable:!0}),r()},onError:v=>{var S;E({title:"Th\u1EA5t b\u1EA1i!",description:`${(S=v==null?void 0:v.message)!=null?S:"L\u1ED7i c\u1EADp nh\u1EADt"}`,status:"error",duration:3e3,isClosable:!0})}}),[Y,{loading:j}]=ie(_t,{onCompleted:v=>{const{updateTicketStatus:S}=v;S&&(a(C(y({},e),{status:S.status})),E({title:"Th\xE0nh c\xF4ng!",description:S.status===w.DELETED?"\u0110\xE3 x\xF3a ticket":"C\u1EADp nh\u1EADt tr\u1EA1ng th\xE1i th\xE0nh c\xF4ng.",status:"success",duration:3e3,isClosable:!0}))}}),P=async()=>{var v,S,ye;if(x.current){const z=(S=(v=x==null?void 0:x.current)==null?void 0:v.props)==null?void 0:S.editor;if(!z||!(z==null?void 0:z.getText()))return;const le=z==null?void 0:z.getHTML();le&&k(le),await D({variables:{input:{refId:(ye=e==null?void 0:e.id)!=null?ye:Fe,title:"",description:le}}})}},M=l.exports.useCallback(async()=>{await Y({variables:{input:{refId:e.id,status:w.DELETED}}})},[e]);return p(b,{w:"full",children:[t(W,{gap:4,children:p(A,{justifyContent:"space-between",flexGrow:1,alignItems:"flex-start",children:[t(wt,{avatar:i,name:o,meta:m,avatarSize:10}),p(lt,{children:[t(dt,{as:Te,variant:"ghost",icon:t(ct,{fontSize:24})}),p(ut,{children:[t(ne,{icon:t(V,{as:pt,w:4,h:4}),onClick:()=>r(),children:"Ch\u1EC9nh s\u1EEDa"}),t(ne,{icon:t(mt,{}),onClick:M,children:"X\xF3a b\xECnh lu\u1EADn"})]})]})]})}),t(b,{mt:2,w:"full",children:n?p(b,{mt:4,w:"full",children:[t(b,{w:"full",className:"reply-editor",mt:2,children:t(Le,{ref:x,showWordCount:!1,placeholder:"N\u1ED9i dung ph\u1EA3n h\u1ED3i ticket...",defaultValue:$})}),p(A,{mt:2,children:[t(N,{colorScheme:"orange",size:"sm",onClick:P,isLoading:B,children:"C\u1EADp nh\u1EADt"}),t(N,{colorScheme:"gray",onClick:()=>r(),size:"sm",children:"H\u1EE7y"})]})]}):t(b,{mt:2,children:t(T,{dangerouslySetInnerHTML:{__html:c==null?void 0:c.content}})})})]})};var ee;(function(e){e.Ok="Ok",e.NotOk="NotOk",e.Love="Love",e.Report="Report"})(ee||(ee={}));const Gn=({bg:e,icon:a,text:n,count:r,allowVote:o})=>{const[i,u]=l.exports.useState(r);return p(A,{className:"emoji","aria-label":"vote emoji",spacing:{base:1,md:2},onClick:c=>{const{target:d}=c,$=d.closest(".emoji");$&&o&&($.classList.add("animate-emoji"),u(i+1))},onAnimationEnd:c=>{const{target:d}=c,$=d.closest(".emoji");$&&$.classList.remove("animate-emoji")},transition:"all .2s ease",_hover:{color:e},children:[t(W,{bg:e,rounded:"full",width:6,height:6,d:"inline-flex",alignItems:"center",justifyContent:"center",color:"white",p:1,gap:2,className:"icon",children:t(V,{as:a,w:4,h:4})}),p(A,{as:"span",spacing:1,children:[t(b,{as:"span",d:{base:"none",md:"inline"},children:n}),p(b,{as:"span",children:["(",i,")"]})]})]})},Wn=e=>{switch(e){case ee.Ok:return{icon:$i,colorScheme:"green"};case ee.NotOk:return{icon:bi,colorScheme:"orange"};case ee.Report:return{icon:fi,colorScheme:"slate"};case ee.Love:return{icon:gt,colorScheme:"red"};default:return{icon:gt,colorScheme:"blue"}}},Vn=({data:e,allowVote:a=!1})=>{const n=r=>{};return t(A,{spacing:4,fontSize:"sm",children:e.map(r=>{const o=Wn(r.reaction);return t(b,{as:"span",role:a?"button":"text",onClick:a?()=>n(r.reaction):null,children:t(Gn,{icon:o.icon,bg:`${o.colorScheme}.500`,text:r.reaction,count:r.count,allowVote:a})},r.reaction)})})},Zn=(e,a=null)=>{e.forEach(n=>{a==null||a.evict({id:"ROOT_QUERY",fieldName:n})})};function Kn(){const e=yi();return a=>{Zn(a,e.cache)}}var w;(function(e){e.PROCESSING="Processing",e.CLOSED="Closed",e.WAITING="Waiting",e.DELETED="Deleted"})(w||(w={}));const Lt=1,fr=({data:e,defaultShowEditor:a=!1,onTicketUpdate:n})=>{var qe,Ue,Ye;const[r,o]=se(),[i,u]=l.exports.useState(e),[g,m]=l.exports.useState(i.question.title),[c,d]=l.exports.useState(i.question.content),[$,k]=l.exports.useState([]),[x,E]=se(!1),[D,B]=se(a),[Y,j]=se(!1),P=l.exports.useRef(null),M=st(),He=i,{name:X,avatar:v,time:S,status:ye,meta:z,question:G}=He,le=H(He,["name","avatar","time","status","meta","question"]);Kn();const[Rt,{loading:Bt}]=ie(Hn,{onCompleted:f=>{var O,L;const{createTicket:h}=f;i.status===w.WAITING&&u(C(y({},i),{status:w.PROCESSING})),k([C(y({},h),{id:h.id,name:(O=h==null?void 0:h.createBy)==null?void 0:O.full_name,avatar:(L=h==null?void 0:h.createBy)==null?void 0:L.avatar,status:h==null?void 0:h.status,time:(h==null?void 0:h.createDate)?R(h.createDate).format("DD/MM/YYYY HH:mm"):"",meta:" [ZIM - 308 Tr\u1EA7n Ph\xFA] - Online - Advanced (10 h\u1ECDc vi\xEAn), 16/11, 19:30-21:30",question:{title:h==null?void 0:h.title,content:h==null?void 0:h.description},comments:[]}),...$]),M({title:"Th\xE0nh c\xF4ng!",description:"B\xECnh lu\u1EADn c\u1EE7a b\u1EA1n \u0111\xE3 \u0111\u01B0\u1EE3c g\u1EEDi.",status:"success",duration:3e3,isClosable:!0})},onError:f=>{var h;M({title:"T\u1EA1o kh\xF4ng th\xE0nh c\xF4ng!",description:`${(h=f==null?void 0:f.message)!=null?h:"L\u1ED7i"}`,status:"error",duration:3e3,isClosable:!0})}}),[Mt,{loading:Nt}]=ie(vt,{onCompleted:f=>{const{updateTicket:h}=f;u(C(y({},e),{question:{title:h==null?void 0:h.title,content:h==null?void 0:h.description}})),M({title:"Th\xE0nh c\xF4ng!",description:"C\u1EADp nh\u1EADt n\u1ED9i dung th\xE0nh c\xF4ng.",status:"success",duration:3e3,isClosable:!0})},onError:f=>{var h;M({title:"Th\u1EA5t b\u1EA1i!",description:`${(h=f==null?void 0:f.message)!=null?h:"L\u1ED7i c\u1EADp nh\u1EADt"}`,status:"error",duration:3e3,isClosable:!0})}}),[je,{loading:Jn}]=ie(_t,{onCompleted:f=>{const{updateTicketStatus:h}=f;h&&(u(C(y({},i),{status:h.status})),n(C(y({},i),{status:h.status})),M({title:"Th\xE0nh c\xF4ng!",description:h.status===w.DELETED?"\u0110\xE3 x\xF3a ticket":"C\u1EADp nh\u1EADt tr\u1EA1ng th\xE1i th\xE0nh c\xF4ng.",status:"success",duration:3e3,isClosable:!0}))}}),jt=l.exports.useCallback(f=>{f.preventDefault(),B()},[]),zt=l.exports.useCallback(f=>{f.preventDefault(),E()},[]);l.exports.useCallback(f=>{f.preventDefault(),j()},[]);const Ht=l.exports.useCallback(async()=>{const f=i.status;await je({variables:{input:{refId:i.id,status:f===K.Closed.value?w.PROCESSING:w.CLOSED}}})},[i]),qt=l.exports.useCallback(async()=>{await je({variables:{input:{refId:i.id,status:w.DELETED}}})},[i]),Ut=l.exports.useCallback(()=>{o()},[]),ze=l.exports.useCallback(f=>{const h=f.status===w.DELETED?$.filter(O=>O.id!==f.id):$.map(O=>O.id===f.id?f:O);k(h)},[$]),Yt=async()=>{var f,h,O;if(P.current){const L=(h=(f=P==null?void 0:P.current)==null?void 0:f.props)==null?void 0:h.editor;if(!L||!(L==null?void 0:L.getText()))return;const te=L==null?void 0:L.getHTML();te&&d(te),await Mt({variables:{input:{refId:(O=i==null?void 0:i.id)!=null?O:Fe,title:g,description:te}}})}o()},Gt=async()=>{var f,h,O,L;try{if(P.current){const F=(h=(f=P==null?void 0:P.current)==null?void 0:f.props)==null?void 0:h.editor;if(F){const te=F==null?void 0:F.getHTML();if(!(F==null?void 0:F.getText()))return;await Rt({variables:{input:{refId:(O=e==null?void 0:e.id)!=null?O:Fe,title:"",description:te}}}),(L=F==null?void 0:F.commands)==null||L.clearContent()}}}catch(F){console.log({e:F})}};return l.exports.useEffect(()=>{k(e.comments)},[e]),p(Me,{className:"animate__animated animate__fadeIn",children:[t(W,{gap:4,children:p(A,{justifyContent:"space-between",flexGrow:1,alignItems:"flex-start",children:[t(wt,{avatar:v,name:X,meta:z,badgeTitle:(qe=K==null?void 0:K[i==null?void 0:i.status])==null?void 0:qe.label,badgeProps:{colorScheme:(Ue=K==null?void 0:K[i==null?void 0:i.status])==null?void 0:Ue.colorScheme}}),p(lt,{children:[t(dt,{as:Te,variant:"ghost",icon:t(ct,{fontSize:24})}),p(ut,{children:[t(ne,{icon:t(V,{as:pt,w:4,h:4}),onClick:Ut,children:"Ch\u1EC9nh s\u1EEDa"}),t(ne,{icon:t(V,{as:Ci,w:4,h:4}),onClick:Ht,children:t("span",{children:i.status!==w.CLOSED?"\u0110\xF3ng ticket":"M\u1EDF ticket"})}),t(ne,{icon:t(V,{as:mt,w:4,h:4}),onClick:qt,children:"X\xF3a ticket"})]})]})]})}),p(b,{mt:4,children:[r?p(b,{mt:4,w:"full",children:[p(b,{w:"full",className:"reply-editor",mt:2,children:[t(fe,{label:"Ti\xEAu \u0111\u1EC1",value:g,onChange:f=>m(f.target.value)}),t(b,{mt:2,children:t(Le,{ref:P,showWordCount:!1,placeholder:"N\u1ED9i dung ticket...",defaultValue:c})})]}),p(A,{mt:2,children:[t(N,{colorScheme:"orange",size:"sm",onClick:Yt,isLoading:Nt,children:"C\u1EADp nh\u1EADt"}),t(N,{colorScheme:"gray",onClick:()=>o(),size:"sm",children:"H\u1EE7y"})]})]}):p(b,{children:[t(T,{fontWeight:600,children:(Ye=G==null?void 0:G.title)!=null?Ye:""}),t(b,{mt:2,children:t(T,{dangerouslySetInnerHTML:{__html:G==null?void 0:G.content}})})]}),t(ue,{spacing:4,mx:-4,children:$.map((f,h)=>x?t(b,{borderTop:"1px solid",borderColor:_("gray.300","slate.700"),mt:4,pt:4,px:4,w:"full",children:t(Ot,{data:C(y({},f),{meta:R(f.createDate).format("DD/MM/YYYY HH:mm")}),onTicketUpdate:ze})},f.id):h<Lt&&t(b,{borderTop:"1px solid",borderColor:_("gray.300","slate.700"),mt:4,pt:4,px:4,w:"full",children:t(Ot,{onTicketUpdate:ze,data:C(y({},f),{meta:"20/02/2022 10:30"})})},f.id))}),!x&&$.length>Lt+1&&p(ce,{href:"#",color:"blue.500",d:"flex",gap:2,mt:2,alignItems:"center",onClick:zt,children:[t(b,{as:"span",className:"animate__animated animate__pulse animate__infinite",children:t(Ii,{})}),t(T,{role:"button","aria-label":"load more replies",children:"Xem c\xE1c ph\u1EA3n h\u1ED3i tr\u01B0\u1EDBc..."})]}),D&&p(b,{mt:4,w:"full",className:"animate__animated animate__fadeIn",children:[t(T,{fontWeight:600,children:"Ph\u1EA3n h\u1ED3i"}),t(b,{width:"full",className:"reply-editor",mt:2,children:t(Le,{ref:P,showWordCount:!1,placeholder:"N\u1ED9i dung ph\u1EA3n h\u1ED3i ticket..."})}),t(N,{colorScheme:"blue",mt:4,mb:0,size:"sm",onClick:Gt,isLoading:Bt,children:"G\u1EEDi ph\u1EA3n h\u1ED3i"})]})]}),t(b,{my:4,children:t(Vn,{data:e.reactions})}),p(W,{justifyContent:"space-between",alignItems:"center",gap:2,mt:4,flexWrap:"wrap",children:[t(A,{spacing:4,children:t(N,{leftIcon:t(Ei,{}),variant:D?"solid":"outline",size:"sm",colorScheme:"gray",onClick:jt,isDisabled:e.status===w.CLOSED||e.status===w.DELETED,children:"Ph\u1EA3n h\u1ED3i"})}),t(T,{color:"gray.500",fontSize:"sm",children:S||""})]})]})},Me=e=>{const i=e,{variant:a,children:n}=i,r=H(i,["variant","children"]),o=Di("Card",{variant:a});return t(b,C(y({__css:o},r),{children:n}))},Ne=()=>Math.floor(Math.random()*(235-52+1)+52),Xn=()=>`rgb(${Ne()}, ${Ne()}, ${Ne()})`;Si.register(xi,Ti,ki,Pi,Ai,vi,_i);const Ft=()=>{const[e,a]=l.exports.useState(R().subtract(3,"month").toDate()),[n,r]=l.exports.useState(R().toDate()),[o,{data:i,loading:u,refetch:g,called:m}]=wi(kn,{fetchPolicy:"cache-and-network"}),[c,d]=l.exports.useState([]),$=l.exports.useMemo(()=>{if(!(i==null?void 0:i.getReportLeads)||c.length===0)return[];const{getReportLeads:E}=i;return E?E.map(D=>R(D.date).format("DD/MM/YY")):[]},[c,i]),k=l.exports.useMemo(()=>{if(!(i==null?void 0:i.getReportLeads)||c.length===0)return[];const{getReportLeads:E}=i;return[{label:"T\u1ED5ng",data:E.map(D=>D.totalView)},...c]},[i,c]),x=async()=>{m?await g({startDate:be(e,"YYYY/MM/DD HH:mm"),endDate:be(n,"YYYY/MM/DD HH:mm")}):await o({variables:{startDate:be(e,"YYYY/MM/DD HH:mm"),endDate:be(n,"YYYY/MM/DD HH:mm")}})};return l.exports.useEffect(()=>{var D;if(!i)return;const E=i==null?void 0:i.getReportLeads;try{if(E&&(E==null?void 0:E.length)){const Y=((D=E==null?void 0:E[0])==null?void 0:D.dataLeads.map(j=>j.sourseName)).map(j=>{const P=[];E.map(X=>{const v=X.dataLeads.find(S=>S.sourseName===j);P.push(v.data)});const M=Xn();return{label:j,data:P,backgroundColor:M,borderColor:M,borderRadius:4,pointRadius:1,tension:.4,borderWidth:1}});d([{label:"T\u1ED5ng",data:E.map(j=>j.totalView),backgroundColor:"#0093f1",borderColor:"#0093f1",borderRadius:4,pointRadius:1,tension:.5,borderWidth:3},...Y])}}catch(B){console.log({e:B})}},[i]),l.exports.useEffect(()=>{x()},[]),p(Dn,{px:4,py:8,children:[t(Me,{p:4,children:p(Oi,{bg:"white",direction:["column","column","row"],alignItems:{md:"flex-end"},spacing:4,mb:4,children:[p(A,{spacing:4,flexWrap:"nowrap",children:[t(xt,{name:"fromDate",label:"T\u1EEB ng\xE0y",selected:e,onChange:a,autoComplete:"off",dateFormat:"dd/MM/yyyy",placeholderText:"Ng\xE0y b\u1EAFt \u0111\u1EA7u"}),t(xt,{label:"\u0110\u1EBFn ng\xE0y",selected:n,onChange:r,autoComplete:"off",dateFormat:"dd/MM/yyyy",placeholderText:"Ng\xE0y k\u1EBFt th\xFAc"})]}),t(b,{children:t(N,{colorScheme:"green",onClick:x,isLoading:u,children:"L\u1ECDc d\u1EEF li\u1EC7u"})})]})}),c.length>0&&$.length>0&&t(b,{my:8,children:u?t(Et,{}):t(Me,{children:t(Sn,{data:{labels:$,datasets:c}})})}),(k==null?void 0:k.length)>0&&($==null?void 0:$.length)>0&&!u&&t(Tn,{data:k,labels:$})]})};var Qn=Object.freeze({__proto__:null,[Symbol.toStringTag]:"Module",default:Ft});Li.render(t(Fi.StrictMode,{children:t(ln,{children:p(Ri,{theme:Dt,children:[t(Bi,{initialColorMode:Dt.config.initialColorMode}),t(Mi,{children:t(Ni,{children:t(ji,{path:"*",element:t(Ft,{})})})})]})})}),document.getElementById("root"));export{Me as C,xt as D,Fe as E,rr as G,Et as L,Oe as N,ur as O,gr as S,$e as T,wt as U,Kn as a,Hn as b,fe as c,Le as d,J as e,Dn as f,fr as g,It as h,nr as i,se as j,ir as k,pr as l,be as m,cr as n,dr as o,sr as p,xn as q,or as r,Un as s,Zi as t,dn as u,lr as v,mr as w,hr as x};
