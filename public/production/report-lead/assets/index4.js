var me=Object.defineProperty,pe=Object.defineProperties;var he=Object.getOwnPropertyDescriptors;var W=Object.getOwnPropertySymbols;var Ee=Object.prototype.hasOwnProperty,fe=Object.prototype.propertyIsEnumerable;var Z=(n,s,a)=>s in n?me(n,s,{enumerable:!0,configurable:!0,writable:!0,value:a}):n[s]=a,x=(n,s)=>{for(var a in s||(s={}))Ee.call(s,a)&&Z(n,a,s[a]);if(W)for(var a of W(s))fe.call(s,a)&&Z(n,a,s[a]);return n},A=(n,s)=>pe(n,he(s));import{u as Te,s as xe,C as K,L as Q,S as J,N as ve,U as Ce,T as X,a as be,b as Se,E as Ae,c as Pe,d as ye,G as Ie,e as N,f as Fe,g as De}from"./index.js";import{i as l,H as G,m as M,B as v,j as e,C as P,b1 as _e,b2 as Ne,a9 as Me,by as Be,b4 as Oe,b5 as ee,bz as Le,aV as H,bA as $e,ag as S,bB as qe,g as j,a2 as we,bC as B,a0 as O,bD as te,y as L,e as m,bE as Ge,bF as He,a$ as ne,bG as ae,b0 as je,bH as Re,bI as Ue,bJ as Ye,bK as Ve,bL as ze,bM as ke,bN as We,bO as Ze,t as R,bP as ie,bQ as Ke,bR as se,aY as Qe,F as Je,n as Xe,bS as et,aG as tt,N as nt}from"./vendor.js";import{v as f,u as re}from"./useUser.js";import{P as at}from"./index5.js";import{u as oe,S as it}from"./index6.js";const st=({time:n="",description:s="",colorScheme:a="gray"})=>l(G,{px:2,py:1,bg:M(`${a}.50`,`${a}.100`),borderLeft:"5px solid",borderLeftColor:M(`${a}.500`,`${a}.600`),rounded:4,justifyContent:"space-between",fontSize:"sm",children:[l(v,{color:M("gray.900","gray.900"),children:[e(P,{children:n}),e(P,{isTruncated:!0,children:s})]}),l(_e,{children:[e(Ne,{as:Me,variant:"ghost",icon:e(Be,{fontSize:24}),_hover:{bg:"transparent"},_focus:{bg:"transparent"},_active:{bg:"transparent"},color:M("gray.900","gray.900")}),l(Oe,{children:[e(ee,{icon:e(Le,{}),children:"Action 1"}),e(ee,{icon:e(H,{as:$e,w:4,h:4}),children:"Action 2"})]})]})]}),rt=()=>{const{appState:{user:n}}=Te();return l(S,{alignItems:"flex-start",direction:"column",gap:2,children:[e(qe,{src:n==null?void 0:n.avatar,objectFit:"cover",boxSize:100,rounded:4}),l(v,{children:[e(P,{fontWeight:600,children:n==null?void 0:n.fullName}),e(P,{fontSize:"sm",children:n==null?void 0:n.role})]})]})},ot=j`
  query getTodoScheduleByEc($idEc: Int, $date: String) @api(name: "appZim") {
    getTodoScheduleByEc(idEc: $idEc, date: $date) {
      id
      type
      content
      customer {
        id
        role_id
        user_name
        full_name
        avatar
      }
      ec {
        id
        user_name
        full_name
        avatar
      }
      instructor {
        id
        role_id
        user_name
        full_name
        avatar
      }
      file_attached
      link
      start_time
      end_time
      date
      is_delete
      create_by
      create_date
      update_by
      update_date
    }
  }
`;""+f(),""+f(),""+f();const ct=we(v)`
  &:hover .flip-card-inner {
    transform: rotateY(180deg);
  }
`,lt=n=>{var s,a;return console.log(n),((s=J)==null?void 0:s[n])?(a=J)==null?void 0:a[n].colorScheme:"gray"},ut=({onOpenTicketForm:n,supportedUsers:s})=>{const[a]=re(),{data:i,loading:h}=B(ot,{variables:{date:O().format("YYYY/MM/DD 00:00"),idEc:(a==null?void 0:a.id)?parseInt(a.id):null},skip:!a||a.roleId!==6});return xe(i),l(S,{flexDirection:"column",gap:8,children:[l(v,{children:[e(P,{fontSize:"sm",children:"\u201CHow wonderful it is that nobody need wait a single moment before starting to improve the world.\u201D \u2013 Anne Frank"}),l(K,{mt:4,mx:{base:-4,md:0},children:[e(te,{size:"sm",children:"L\u1ECBch h\xF4m nay"}),h?e(Q,{}):e(S,{direction:"column",gap:4,mt:4,children:(i==null?void 0:i.getTodoScheduleByEc)&&(i==null?void 0:i.getTodoScheduleByEc.map(o=>e(st,{time:`${O(o.start_time).format("HH:mm")} - ${O(o.end_time).format("HH:mm")}`,description:o.content,colorScheme:lt(o.type)},o.id)))})]})]}),l(v,{children:[l(G,{justifyContent:"space-between",children:[e(te,{size:"sm",children:"Danh s\xE1ch h\u1ECDc vi\xEAn"}),e(ve,{to:"/",children:"Xem t\u1EA5t c\u1EA3"})]}),e(S,{direction:"column",mt:4,gap:4,children:s.map((o,p)=>{var g;return e(ct,{children:e(Ce,{name:o.fullName,avatar:o.avatar,meta:(g=o==null?void 0:o.meta)!=null?g:"Ch\u01B0a c\xF3 th\xF4ng tin",isFlipTicketAvatarCard:!0,onClickFlipCard:()=>n(o==null?void 0:o.userId),avatarSize:12,lastContact:o.lastContact,badgeStatusType:o==null?void 0:o.status})},o.id)})})]})]})},dt=j`
  query getTicketsPagination($input: TicketPaginationInputType)
  @api(name: "appZim") {
    getTicketsPagination(input: $input) {
      hasNextPage
      hasPrevPage
      limit
      page
      pageNext
      pagePrev
      pagingCounter
      totalPages
      totalDocs
      docs {
        ...Ticket
        comments {
          ...Ticket
        }
      }
    }
  }
  ${X}
`,gt=j`
  query getTicketsPagination($input: TicketPaginationInputType)
  @api(name: "appZim") {
    getTicketsPagination(input: $input) {
      hasNextPage
      hasPrevPage
      limit
      page
      pageNext
      pagePrev
      pagingCounter
      totalPages
      totalDocs
      docs {
        status
      }
    }
  }
  ${X}
`,mt=L.object().shape({title:L.string().required("Vui l\xF2ng nh\u1EADp ti\xEAu \u0111\u1EC1"),description:L.string().min(10,"T\u1ED1i thi\u1EC3u 10 k\xFD t\u1EF1").required("Vui l\xF2ng nh\u1EADp m\xF4 t\u1EA3"),receiver:L.mixed()}),pt=({enableReceiver:n,users:s})=>{var o;const{register:a,formState:{errors:i},resetField:h}=Ke();return l(S,{direction:"column",w:"full",justifyContent:"stretch",alignItems:"stretch",className:"reply-editor",gap:4,children:[e(Pe,A(x({label:"Ti\xEAu \u0111\u1EC1",placeholder:"Nh\u1EADp..."},a("title")),{error:i==null?void 0:i.title})),n&&e(se,{name:"receiver",render:({field:p})=>e(it,x({label:"Ng\u01B0\u1EDDi nh\u1EADn",options:s,getOptionLabel:g=>g.fullName,getOptionValue:g=>g.id+"",formatOptionLabel:g=>l(G,{children:[e(Qe,{src:g.avatar,objectFit:"cover",boxSize:6,flexShrink:0}),e(P,{children:g.fullName})]}),errors:i==null?void 0:i.receiver},p))}),e(v,{children:l(Je,{isInvalid:i==null?void 0:i.description,children:[e(se,{name:"description",render:({field:p})=>e(ye,x({showWordCount:!1,placeholder:"N\u1ED9i dung ticket..."},p))}),e(Xe,{children:(o=i==null?void 0:i.description)==null?void 0:o.message})]})})]})},ht=({onClose:n,isOpen:s,enableReceiver:a=!1,submitCallback:i=null,supportedUsers:h=[]})=>{const o=m.exports.useRef(),p=Ge({resolver:He(mt),mode:"onBlur",reValidateMode:"onChange"}),g=ne();oe();const[y,F]=ae(),$=be(),[q,{loading:w}]=je(Se,{onCompleted:c=>{const{createTicket:T}=c;T&&g({title:"Th\xE0nh c\xF4ng!",description:"Ticket \u0111\xE3 \u0111\u01B0\u1EE3c g\u1EEDi.",status:"success",duration:3e3,isClosable:!0}),n(),i&&typeof i=="function"&&i(),$(["getTicketsPagination"])},onError:c=>{var T;g({title:"G\u1EEDi kh\xF4ng th\xE0nh c\xF4ng!",description:`${(T=c==null?void 0:c.message)!=null?T:"L\u1ED7i"}`,status:"error",duration:3e3,isClosable:!0})},notifyOnNetworkStatusChange:!0}),D=m.exports.useCallback(async c=>{var T;if(a&&!(c==null?void 0:c.receiver)){p.setError("receiver",{type:"manual",message:"Vui l\xF2ng ch\u1ECDn ng\u01B0\u1EDDi nh\u1EADn"});return}await q({variables:{input:{refId:Ae,title:c==null?void 0:c.title,description:c==null?void 0:c.description,receiverId:a?parseInt((T=c==null?void 0:c.receiver)==null?void 0:T.id):null}}})},[a]);return m.exports.useEffect(()=>{if(s){const c=y.get("receiverId");if(!c)return;const T=h.find(d=>d.userId===Number(c));p.reset({title:"",description:"",receiver:T||null})}else y.delete("receiverId"),F(y),p.reset({title:"",description:"",receiver:null})},[s,y]),e(Re,A(x({},p),{children:l(Ue,{finalFocusRef:o,isOpen:s,onClose:n,size:"3xl",children:[e(Ye,{}),l(Ve,{children:[e(ze,{children:"T\u1EA1o ticket"}),e(ke,{}),l("form",{onSubmit:p.handleSubmit(D),children:[e(We,{children:e(pt,{enableReceiver:a,users:h})}),l(Ze,{children:[e(R,{leftIcon:e(H,{as:ie}),colorScheme:"green",mr:4,type:"submit",isLoading:w,children:"G\u1EEDi ticket"}),e(R,{variant:"ghost",onClick:n,children:"H\u1EE7y"})]})]})]})]})}))},Et=(n={input:{}},s={})=>{const a=ne();return B(Ie,x({variables:n,onError:i=>{a({title:"L\u1EA5y d\u1EEF li\u1EC7u th\u1EA5t b\u1EA1i.",description:`GET supported student error: ${i==null?void 0:i.message}`,status:"error",duration:3e3,isClosable:!0})},fetchPolicy:"cache-and-network"},s))},U=10,ft=()=>{var Y,V;et();const[n,s]=m.exports.useState(0),[a,i]=m.exports.useState([]),[h,o]=m.exports.useState("all"),[p,g]=m.exports.useState([]),{isOpen:y,onOpen:F,onClose:$}=tt(),q=oe();let[w,D]=ae();const{data:c,loading:T}=Et({input:{limit:100,page:1}}),{data:d,loading:ce}=B(dt,{variables:{input:{page:n+1,limit:U,status:h==="all"?[]:[h],filter:"received"}}}),{data:_,loading:Tt}=B(gt,{variables:{input:{page:1,limit:1e3,filter:"received"}}}),le=m.exports.useCallback(t=>{D(A(x({},q.query),{receiverId:t})),F()},[]),ue=m.exports.useCallback(t=>{t.status===N.DELETED&&i(a.filter(u=>u.id!==t.id))},[a]),de=t=>{o(t)},ge=m.exports.useMemo(()=>{var b;const t=(b=_==null?void 0:_.getTicketsPagination)==null?void 0:b.docs,u=t==null?void 0:t.filter(r=>r.status===N.PROCESSING),E=t==null?void 0:t.filter(r=>r.status===N.CLOSED),C=t==null?void 0:t.filter(r=>r.status===N.WAITING);return[{label:"All",value:"all",count:t?t==null?void 0:t.length:0},{label:"Processing",value:"Processing",count:(u==null?void 0:u.length)||0},{label:"Waiting",value:"Waiting",count:(C==null?void 0:C.length)||0},{label:"Closed",value:"Closed",count:(E==null?void 0:E.length)||0}]},[_]);return m.exports.useEffect(()=>{var t,u;if((d==null?void 0:d.getTicketsPagination)&&((t=d==null?void 0:d.getTicketsPagination)==null?void 0:t.docs)){const E=b=>!b||!(b==null?void 0:b.length)?[]:b.map(r=>{var z,k;return A(x({},r),{id:r.id,name:(z=r==null?void 0:r.createBy)==null?void 0:z.full_name,avatar:(k=r==null?void 0:r.createBy)==null?void 0:k.avatar,status:r==null?void 0:r.status,time:(r==null?void 0:r.createDate)?O(r.createDate).format("DD/MM/YYYY HH:mm"):"",meta:" [ZIM - 308 Tr\u1EA7n Ph\xFA] - Online - Advanced (10 h\u1ECDc vi\xEAn), 16/11, 19:30-21:30",question:{title:r==null?void 0:r.title,content:r==null?void 0:r.description},comments:E(r==null?void 0:r.comments)})}),C=E((u=d==null?void 0:d.getTicketsPagination)==null?void 0:u.docs);i(C)}},[d]),m.exports.useEffect(()=>{s(0),D({status:h})},[h]),m.exports.useEffect(()=>{const t=w.get("status");t&&o(t)},[]),m.exports.useEffect(()=>{var t;c&&((t=c==null?void 0:c.getListStudentsPagination)==null?void 0:t.docs)&&g(c.getListStudentsPagination.docs.map(u=>{var E,C;return A(x({},u),{fullName:(E=u==null?void 0:u.user)==null?void 0:E.full_name,avatar:(C=u==null?void 0:u.user)==null?void 0:C.avatar})}))},[c]),l(Fe,{px:4,children:[l(S,{gap:8,justifyContent:"stretch",alignItems:"flex-start",flexWrap:{base:"wrap",lg:"nowrap"},children:[l(S,{gap:8,flexDirection:"column",w:{base:"full",lg:"200px"},flexShrink:0,children:[e(rt,{}),e(R,{leftIcon:e(H,{as:ie}),colorScheme:"blue",onClick:F,children:"G\u1EEDi ticket"}),e(v,{flexGrow:1,children:e(S,{alignItems:"stretch",as:"ul",listStyleType:"none",direction:{base:"row",lg:"column"},gap:{base:4,lg:4},w:"full",flexWrap:"wrap",lineHeight:1,children:ge.map(t=>l(v,{role:"button","aria-label":`Filter ticket ${t.value}`,whiteSpace:"nowrap",as:"li",onClick:()=>de(t.value),color:h===t.value?"primary":"",fontWeight:h===t.value?600:"",children:[t.label," (",t.count,")"]},t.value))})})]}),l(nt,{spacing:4,flexGrow:1,alignItems:"stretch",zIndex:2,w:"full",mx:{base:-4,sm:0},children:[(a==null?void 0:a.length)>0?a.map(t=>{var u,E;return e(De,{data:A(x({},t),{avatar:(u=t==null?void 0:t.createBy)==null?void 0:u.avatar,name:(E=t==null?void 0:t.createBy)==null?void 0:E.full_name}),onTicketUpdate:ue},t.id)}):ce?e(Q,{}):e(K,{children:e(P,{color:"red.500",textAlign:"center",children:"Kh\xF4ng c\xF3 ticket n\xE0o"})}),((Y=d==null?void 0:d.getTicketsPagination)==null?void 0:Y.totalDocs)>U&&e(at,{forcePage:n,pageCount:Math.ceil(((V=d==null?void 0:d.getTicketsPagination)==null?void 0:V.totalDocs)/U),pageRangeDisplayed:5,onPageChange:({selected:t})=>s(t)})]}),e(v,{w:{base:"full",lg:"325px"},flexShrink:0,children:e(ut,{onOpenTicketForm:le,supportedUsers:p})})]}),e(ht,{enableReceiver:!0,onClose:$,isOpen:y,submitCallback:()=>o("Waiting"),supportedUsers:p})]})};var I;(function(n){n.PROCESSING="processing",n.CLOSED="closed"})(I||(I={}));const Pt=[{id:f(),name:"Tr\u01B0\u01A1ng V\u0103n Nam",avatar:`https://i.pravatar.cc/300?q=${f()}`,status:I.PROCESSING,time:"20/01/2022 10:30",meta:" [ZIM - 308 Tr\u1EA7n Ph\xFA] - Online - Advanced (10 h\u1ECDc vi\xEAn), 16/11, 19:30-21:30",question:{title:"Demo question title",content:"Demo question content"}},{id:f(),name:"Tr\u01B0\u01A1ng V\u0103n Nam",avatar:`https://i.pravatar.cc/300?q=${f()}`,status:I.PROCESSING,time:"20/01/2022 10:30",meta:" [ZIM - 308 Tr\u1EA7n Ph\xFA] - Online - Advanced (10 h\u1ECDc vi\xEAn), 16/11, 19:30-21:30",question:{title:"Demo question title",content:"Demo question content"}},{id:f(),name:"Tr\u01B0\u01A1ng V\u0103n Nam",avatar:`https://i.pravatar.cc/300?q=${f()}`,status:I.PROCESSING,time:"20/01/2022 10:30",meta:" [ZIM - 308 Tr\u1EA7n Ph\xFA] - Online - Advanced (10 h\u1ECDc vi\xEAn), 16/11, 19:30-21:30",question:{title:"Demo question title",content:"Demo question content"}},{id:f(),name:"Tr\u01B0\u01A1ng V\u0103n Nam",avatar:`https://i.pravatar.cc/300?q=${f()}`,status:I.PROCESSING,time:"20/01/2022 10:30",meta:" [ZIM - 308 Tr\u1EA7n Ph\xFA] - Online - Advanced (10 h\u1ECDc vi\xEAn), 16/11, 19:30-21:30",question:{title:"Demo question title",content:"Demo question content"}}],yt=()=>(re(),e(ft,{}));export{I as ETicketStatus,yt as default,Pt as fakeTicketData};
