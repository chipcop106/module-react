var pe=Object.defineProperty,ge=Object.defineProperties;var be=Object.getOwnPropertyDescriptors;var te=Object.getOwnPropertySymbols;var Ee=Object.prototype.hasOwnProperty,Ce=Object.prototype.propertyIsEnumerable;var le=(l,t,r)=>t in l?pe(l,t,{enumerable:!0,configurable:!0,writable:!0,value:r}):l[t]=r,p=(l,t)=>{for(var r in t||(t={}))Ee.call(t,r)&&le(l,r,t[r]);if(te)for(var r of te(t))Ce.call(t,r)&&le(l,r,t[r]);return l},y=(l,t)=>ge(l,be(t));import{j as a,bD as fe,bC as oe,a$ as Se,g as _,e as o,i,B as H,ag as K,F as re,k as ue,H as G,t as ce,c5 as xe,aG as ye,a_ as ve,aa as V,C as F,a9 as ie,aV as he,c6 as De,c7 as we,c8 as Fe,m as je}from"./vendor.js";import{i as _e,j as Te,k as me,D as de,l as ke,O as Oe,r as Le,m as Z,n as Ae,o as $e,p as Be,C as He,q as qe}from"./index.js";import{S as f,u as Ie}from"./index6.js";import{P as Pe}from"./index5.js";const We=l=>a(fe,p({as:"h1",fontSize:"lg"},l)),Me=(l={},t={})=>{const r=Se();return oe(_e,p({variables:l,onError:n=>{var h;r({title:"L\u1EA5y d\u1EEF li\u1EC7u th\u1EA5t b\u1EA1i.",description:(h=n==null?void 0:n.message)!=null?h:"L\u1EA5y d\u1EEF li\u1EC7u kh\xF4ng th\xE0nh c\xF4ng",status:"error",duration:3e3,isClosable:!0})},notifyOnNetworkStatusChange:!0},t))},Re=_`
  query schools($city_id: Int, $status: String) @api(name: "appZim") {
    schools(city_id: $city_id, status: $status) {
      id
      city {
        id
        name
      }
      district {
        id
        name
      }
      name
      address
      phone
      status
    }
  }
`;_`
  query @api(name: "appZim") {
    cities {
      id
      name
      status
    }
  }
`;_`
  query @api(name: "appZim") {
    cats {
      id
      name
    }
  }
`;const Ke=_`
  query subject($name: String, $status: StatusEnum) @api(name: "appZim") {
    subjects(name: $name, status: $status) {
      id
      name
      status
      cats {
        id
        name
        status
        subject {
          id
          name
          status
        }
      }
    }
  }
`;_`
  query levels($subject_id: Int!) @api(name: "appZim") {
    levels(subject_id: $subject_id, status: active) {
      id
      name
      status
      graduation
      subject_id
    }
  }
`;_`
  query shifts($q: String) @api(name: "appZim") {
    shifts(q: $q, status: "active", shift_minute: 0) {
      id
      start_time
      end_time
      status
      shift_minute
    }
  }
`;const E=({children:l,split:t=!1})=>a(H,{w:{base:t?"50%":"100%",md:t?"25%":"50%",lg:"33.33%",xl:"25%","2xl":"20%"},p:2,children:l}),U=({query:l,options:t={}})=>{const{data:r,loading:n,refetch:h}=oe(l,p({fetchPolicy:"cache-and-network"},t));return[r,n,h]},O={label:"T\u1EA5t c\u1EA3",value:"all"},q={school:O,subject:O,openStatuses:[],openDateFrom:null,openDateTo:null,finishDateFrom:null,finishDateTo:null,level:O,shifts:[],ec:O,dayOfWeeks:[],skills:[],preTeachers:[]},Ge=({onSubmit:l})=>{var d,S;const[t,r]=Te(),[n,h]=o.exports.useState(q),[L,I]=o.exports.useState([]),[A,T]=o.exports.useState([]),[P,$]=o.exports.useState([]),[v,m]=U({query:Re}),[D,g]=U({query:Ke,options:{variables:{status:"active"}}}),[w,k,W]=U({query:me,options:{variables:{subject_id:((d=n==null?void 0:n.subject)==null?void 0:d.value)==="all"?0:(S=n==null?void 0:n.subject)==null?void 0:S.value}}}),M=o.exports.useCallback(e=>{const[c,b]=e;h(y(p({},n),{openDateFrom:c,openDateTo:b}))},[n]),R=o.exports.useCallback(e=>{const[c,b]=e;h(y(p({},n),{finishDateFrom:c,finishDateTo:b}))},[n]),j=o.exports.useCallback((e,c)=>{(c==null?void 0:c.name)&&h(y(p({},n),{[c.name]:e}))},[n]),s=()=>{l(n)},C=()=>{h(q),l(q)};return o.exports.useEffect(()=>{(v==null?void 0:v.schools)&&!m&&I(v.schools.map(e=>y(p({},e),{label:e.name,value:e.id})))},[v,m]),o.exports.useEffect(()=>{(D==null?void 0:D.subjects)&&!g&&T(D.subjects.map(e=>y(p({},e),{label:e.name,value:e.id})))},[D,g]),o.exports.useEffect(()=>{(w==null?void 0:w.levels)&&!k&&$(w.levels.map(e=>y(p({},e),{label:e.name,value:e.id})))},[w,k]),o.exports.useEffect(()=>{var e,c;!((e=n.subject)==null?void 0:e.value)||((c=n.subject)==null?void 0:c.value)==="all"?(h(y(p({},n),{level:O})),$([])):(async()=>{var x;return await W({subject_id:(x=n.subject)==null?void 0:x.value})})()},[n.subject.value]),o.exports.useEffect(()=>{JSON.stringify(n)===JSON.stringify(q)?r(!1):r(!0)},[n]),i(H,{children:[i(K,{mx:-2,flexWrap:"wrap",children:[a(E,{children:a(f,{id:"f-school",name:"school",label:"Trung t\xE2m",value:n.school,options:L,isLoading:m,onChange:j,paramKey:"school",allOption:{label:"T\u1EA5t c\u1EA3",value:"all"}})}),a(E,{children:a(f,{id:"f-subject",name:"subject",label:"M\xF4n h\u1ECDc",value:n.subject,options:A,isLoading:g,onChange:j,paramKey:"subject",allOption:{label:"T\u1EA5t c\u1EA3 ",value:"all"}})}),a(E,{children:a(f,{name:"level",isLoading:k,label:"Tr\xECnh \u0111\u1ED9",value:n.level,options:P,onChange:j})}),a(E,{children:a(f,{label:"K\u1EF9 n\u0103ng",isMulti:!0,value:[{label:"Listening",value:5.5},{label:"Writing",value:5.5}]})}),i(E,{children:[a(re,{children:a(ue,{htmlFor:"f-startDate",children:"Khai gi\u1EA3ng"})}),a(de,{id:"f-startDate",selected:new Date,onChange:M,startDate:n.openDateFrom,endDate:n.openDateTo,selectsRange:!0,dateFormat:"dd/MM/yyyy",placeholderText:"Ch\u1ECDn ng\xE0y",monthsShown:2})]}),i(E,{children:[a(re,{children:a(ue,{htmlFor:"f-endDate",children:"K\u1EBFt th\xFAc"})}),a(de,{id:"f-endDate",selected:new Date,onChange:R,startDate:n.finishDateFrom,endDate:n.finishDateTo,selectsRange:!0,dateFormat:"dd/MM/yyyy",placeholderText:"Ch\u1ECDn ng\xE0y",monthsShown:2})]}),a(E,{children:a(f,{label:"Ca h\u1ECDc",isMulti:!0,value:[{label:"09:00 - 11:00",value:5.5},{label:"10:00 - 12:00",value:5.5}]})}),a(E,{children:a(f,{label:"EC ch\u1EE7 nhi\u1EC7m",value:{label:"Tr\u01B0\u01A1ng V\u0103n Lam",value:5.5}})}),a(E,{children:a(f,{label:"Gi\u1EA3ng vi\xEAn d\u1EF1 ki\u1EBFn",value:{label:"Tr\u01B0\u01A1ng Anh Lam",value:5.5}})}),a(E,{children:a(f,{label:"Tr\u1EA1ng th\xE1i",name:"openStatuses",isMulti:!0,options:ke,value:n.openStatuses,onChange:j})})]}),i(G,{mt:4,spacing:4,children:[a(ce,{colorScheme:"green",width:{base:"100%",md:"auto"},onClick:s,children:"L\u1ECDc d\u1EEF li\u1EC7u"}),t&&a(ce,{leftIcon:a(xe,{}),variant:"ghost",colorScheme:"red",onClick:C,flexShrink:0,children:"X\xF3a b\u1ED9 l\u1ECDc"})]})]})},Ve=[{label:"5 rows",value:5},{label:"10 rows",value:10},{label:"15 rows",value:15},{label:"20 rows",value:20},{label:"25 rows",value:25},{label:"30 rows",value:30}],Qe=()=>{const l=Ie(),[t,r]=o.exports.useState(""),{isOpen:n,onToggle:h}=ye(),[L,I]=o.exports.useState({label:"10 rows",value:10}),[A,T]=o.exports.useState(0),[P,$]=o.exports.useState(1),[v,m]=o.exports.useState([]),D=o.exports.useRef(null),{data:g,loading:w,refetch:k}=Me({limit:L.value,page:A+1,q:t}),W=o.exports.useMemo(()=>[{Header:"Tr\u1EA1ng th\xE1i",accessor:"status",Cell:({value:s})=>{var C,d;return a(ve,{colorScheme:(d=(C=Oe)==null?void 0:C[s.toUpperCase()])==null?void 0:d.colorScheme,children:s})}},{Header:"Trung t\xE2m",accessor:"school",nowrap:!0},{Header:"H\xECnh th\u1EE9c",accessor:"method",nowrap:!0},{Header:"Kh\xF3a h\u1ECDc",accessor:"class",disableSortBy:!0,Cell:({value:s})=>i(V,{children:[a(F,{whiteSpace:"nowrap",fontWeight:600,children:s==null?void 0:s.method}),i(F,{whiteSpace:"nowrap",children:[s==null?void 0:s.subject," ",s==null?void 0:s.level," ",s==null?void 0:s.level_out]})]})},{accessor:"startDate",Cell:({value:s})=>a(F,{whiteSpace:"nowrap",children:s}),Header:({value:s})=>a(F,{whiteSpace:"nowrap",children:"Khai gi\u1EA3ng - K\u1EBFt th\xFAc"})},{Header:"L\u1ECBch h\u1ECDc",accessor:"schedule",disableSortBy:!0,Cell:({value:s})=>i(V,{children:[i(F,{whiteSpace:"nowrap",children:["L\u1ECBch h\u1ECDc: ",a("strong",{children:s.dayOfWeeks})]}),i(F,{whiteSpace:"nowrap",children:["Ca h\u1ECDc: ",a("strong",{children:s.shifts})]})]})},{Header:"K\u1EF9 n\u0103ng",accessor:"skills",minWidth:150},{Header:"S\u1EC9 s\u1ED1",accessor:"numberOfRegistered",minWidth:75},{Header:"T\u1ED1i \u0111a",accessor:"maximumRegister",minWidth:75},{Header:"EC",accessor:"ec",minWidth:150},{Header:"GV d\u1EF1 ki\u1EBFn",accessor:"preTeachers",minWidth:200,disableSortBy:!0},{Header:"",accessor:"id",Cell:({value:s})=>a(ie,{"aria-label":"view-detail",size:"sm",onClick:()=>l.navigate(`${Le.listCourses}/${s}`),icon:a(he,{as:De})}),isSticky:!0,stickyRight:!0}],[]),M=s=>{T(0),I(s)},R=o.exports.useCallback(async s=>{var C,d,S,e,c,b,x;await k({school_id:((C=s==null?void 0:s.school)==null?void 0:C.value)==="all"?0:parseInt((d=s==null?void 0:s.school)==null?void 0:d.value),status:((S=s==null?void 0:s.openStatuses)==null?void 0:S.length)?s.openStatuses.map(B=>B.value):[],subject_id:((e=s==null?void 0:s.subject)==null?void 0:e.value)==="all"?0:parseInt((c=s==null?void 0:s.subject)==null?void 0:c.value),level_id:((b=s==null?void 0:s.level)==null?void 0:b.value)==="all"?0:parseInt((x=s==null?void 0:s.level)==null?void 0:x.value),q:t})},[t]),j=o.exports.useCallback(async s=>{r(s),T(0)},[]);return o.exports.useEffect(()=>{var d,S;if(!g)return;const{docs:s}=g.getListCoursePagination,C=s.map(e=>{var c,b,x,B,z,N,J,Q,X,Y;return{id:e.id,school:`${(c=e==null?void 0:e.school)==null?void 0:c.name}`,startDate:`${(e==null?void 0:e.start_date)?Z(e==null?void 0:e.start_date):""} - ${(e==null?void 0:e.end_date)?Z(e==null?void 0:e.end_date):""}`,endDate:(e==null?void 0:e.end_date)?Z(e==null?void 0:e.end_date):"",method:e==null?void 0:e.type,class:{subject:`${(b=e==null?void 0:e.subject)==null?void 0:b.name}`,level:`${(x=e==null?void 0:e.level_out)==null?void 0:x.name} ${(B=e==null?void 0:e.level_out)==null?void 0:B.graduation}`},skills:((z=e==null?void 0:e.subcats)==null?void 0:z.length)?e.subcats.map(u=>{var ee,se,ae,ne;return(ne=(ae=(se=Ae)==null?void 0:se[(ee=u==null?void 0:u.name)==null?void 0:ee.toUpperCase()])==null?void 0:ae.short)!=null?ne:u==null?void 0:u.name}).join(", "):"",numberOfRegistered:e==null?void 0:e.total_register,maximumRegister:(N=e==null?void 0:e.size)==null?void 0:N.size,schedule:{dayOfWeeks:((J=e==null?void 0:e.day_of_weeks)==null?void 0:J.length)?e.day_of_weeks.map(u=>$e[u.id].short).join(", "):"",shifts:((Q=e==null?void 0:e.shifts)==null?void 0:Q.length)?e.shifts.map(u=>`${u==null?void 0:u.start_time} - ${u==null?void 0:u.end_time}`).join(", "):""},status:e==null?void 0:e.status,ec:(X=e==null?void 0:e.ec)==null?void 0:X.full_name,preTeachers:((Y=e==null?void 0:e.teachers)==null?void 0:Y.length)?e.teachers.map(u=>u==null?void 0:u.full_name).join(", "):"Ch\u01B0a c\xF3"}});m(C),$((S=(d=g==null?void 0:g.getListCoursePagination)==null?void 0:d.totalPages)!=null?S:1)},[g]),i(V,{children:[i(K,{gap:4,justifyContent:"space-between",alignItems:"center",flexWrap:"wrap",children:[a(We,{children:"Danh s\xE1ch kh\xF3a h\u1ECDc"}),i(G,{alignItems:"center",width:{base:"100%",sm:"auto"},children:[a(Be,{ref:D,variant:"outline",onSubmitSearch:j,placeholder:"T\xECm ki\u1EBFm...",w:"100%"}),a(ie,{colorScheme:n?"blue":"gray","aria-label":"toggle filter",icon:a(he,{as:we,w:6,h:6}),onClick:h})]})]}),a(Fe,{in:n,animateOpacity:!0,children:a(He,{bg:je("gray.50","slate.800"),mt:4,children:a(Ge,{onSubmit:R})})}),a(H,{mt:8,children:a(qe,{columns:W,data:v,loading:w})}),i(K,{justifyContent:"space-between",alignItems:"center",children:[a(Pe,{pageCount:P,forcePage:A,onPageChange:({selected:s})=>T(s)}),i(G,{children:[a(F,{whiteSpace:"nowrap",children:"Hi\u1EC3n th\u1ECB"}),a(H,{width:200,children:a(f,{menuPlacement:"top",value:L,options:Ve,onChange:M})})]})]})]})};export{Qe as default};
