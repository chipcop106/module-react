var at=Object.defineProperty,ot=Object.defineProperties;var lt=Object.getOwnPropertyDescriptors;var Le=Object.getOwnPropertySymbols;var nt=Object.prototype.hasOwnProperty,it=Object.prototype.propertyIsEnumerable;var st=(e,r,n)=>r in e?at(e,r,{enumerable:!0,configurable:!0,writable:!0,value:n}):e[r]=n,B=(e,r)=>{for(var n in r||(r={}))nt.call(r,n)&&st(e,n,r[n]);if(Le)for(var n of Le(r))it.call(r,n)&&st(e,n,r[n]);return e},H=(e,r)=>ot(e,lt(r));var Ae=(e,r)=>{var n={};for(var a in e)nt.call(e,a)&&r.indexOf(a)<0&&(n[a]=e[a]);if(e!=null&&Le)for(var a of Le(e))r.indexOf(a)<0&&it.call(e,a)&&(n[a]=e[a]);return n};import{g as gql,o as onError,A as ApolloLink,O as Observable,I as InMemoryCache,r as relayStylePagination,M as MultiAPILink,c as createUploadLink,b as buildAxiosFetch_1,a as axios,W as WebSocketLink,T as TokenRefreshLink,d as o,s as split$1,e as getMainDefinition,f as ApolloClient,S as SubscriptionClient_1,h as react,j as jsx,i as ApolloProvider,L as Link$1,k as Link$2,u as useColorModeValue,F as Fragment,l as Accordion,m as jsxs,n as AccordionItem,p as AccordionButton,H as HStack,q as Text,t as AccordionIcon,v as AccordionPanel,B as Box,w as Flex,x as Tabs,y as TabList,V as VStack,z as Tab,C as TabPanels,D as TabPanel,E as ImNewspaper,G as SiGoogleclassroom,J as IoLibraryOutline,K as HiUserGroup,N as FaUserFriends,P as BiMoney,Q as MdOutlineMiscellaneousServices,R as IoSettingsOutline,U as FormControl,X as FormLabel,Y as Input$1,Z as FormErrorMessage,_ as InputGroup,$ as InputRightElement,a0 as Button$1,a1 as VscSearch,a2 as useQuery,a3 as useSubscription,a4 as SearchIcon,a5 as Avatar,a6 as t,a7 as AvatarBadge,a8 as FcKey,a9 as SkeletonCircle,aa as SkeletonText,ab as uniqBy_1,ac as pickBy_1,ad as FiGrid,ae as RiChat3Line,af as dayjs,ag as commonjsGlobal$1,ah as SimpleGrid,ai as lodash,aj as Stack,ak as BsCheck2,al as PropTypes,am as useCheckbox,an as useRadio,ao as CheckCircleIcon,ap as Wrap,aq as Tag,ar as TagLabel,as as TagCloseButton,at as Spinner,au as useApolloClient,av as useToast,aw as useMutation,ax as useCheckboxGroup,ay as useForm,az as useDeepCompareEffect$2,aA as FormProvider,aB as Modal$1,aC as ModalOverlay,aD as ModalContent,aE as ModalHeader,aF as ModalCloseButton,aG as ModalBody,aH as ModalFooter,aI as Icon,aJ as AiOutlineSend,aK as useDisclosure,aL as useSearchParams,aM as MdOutlineGroupAdd,aN as useLazyQuery,aO as Highlighter,aP as emojiIndex,aQ as v4$1,aR as moment,aS as Image$1,aT as AiOutlineFileZip,aU as IoCaretForwardSharp,aV as Menu$1,aW as MenuButton,aX as FiMoreHorizontal,aY as Portal,aZ as MenuList,a_ as MenuItem,a$ as AiOutlineDelete,b0 as BiCheck,b1 as ImgsViewer,b2 as BsQuestionCircle,b3 as BsEmojiSmile,b4 as Tooltip,b5 as TableCell,b6 as Table$1,b7 as Blockquote,b8 as Link$3,b9 as Image$2,ba as IconButton,bb as Popover,bc as PopoverTrigger,bd as BiHeading,be as PopoverContent,bf as PopoverBody,bg as GrTableAdd,bh as AiOutlineClear,bi as RiInsertColumnRight,bj as RiInsertColumnLeft,bk as RiDeleteColumn,bl as RiInsertRowTop,bm as RiInsertRowBottom,bn as RiDeleteRow,bo as RiMergeCellsHorizontal,bp as RiSplitCellsHorizontal,bq as AiOutlineInsertRowLeft,br as AiOutlineInsertRowAbove,bs as RiCellphoneFill,bt as BiTable,bu as queryString,bv as useParams,bw as useLocation,bx as useNavigate,by as useTheme,bz as Select$2,bA as useToggle$1,bB as sortBy_1,bC as useRadioGroup,bD as AiOutlineCloudUpload,bE as Switch,bF as SearchIcon$1,bG as AiOutlineFile,bH as urlJoin,bI as BiImages,bJ as create$1,bK as MdSpaceBar,bL as BiBold,bM as BiItalic,bN as AiOutlineUnderline,bO as BiStrikethrough,bP as BiAlignLeft,bQ as BiAlignMiddle,bR as BiAlignRight,bS as BiListUl,bT as BiListOl,bU as BiEdit,bV as Node,bW as mergeAttributes,bX as MdOutlineQuickreply,bY as HiOutlineEmojiSad,bZ as newStyled,b_ as FiSend,b$ as Badge$1,c0 as AiOutlineMore,c1 as AiOutlineEdit,c2 as AiOutlineHeart,c3 as MdOutlineReport,c4 as AiOutlineDislike,c5 as AiOutlineLike,c6 as AiOutlineLock,c7 as AiOutlineArrowDown,c8 as MdReply,c9 as useStyleConfig,ca as DeleteIcon,cb as MdSubdirectoryArrowRight,cc as PlusSquareIcon,cd as useFormContext,ce as Controller,cf as Checkbox,cg as reactTable,ch as p$1,ci as Table$2,cj as Thead,ck as Tr,cl as Th,cm as chakra,cn as TriangleDownIcon,co as TriangleUpIcon,cp as Tbody,cq as Td,cr as CopyIcon,cs as EditIcon,ct as BiTrashAlt,cu as Heading,cv as AlertDialog,cw as AlertDialogContent,cx as cheerio,cy as useEditor,cz as StarterKit,cA as TableRow,cB as TableHeader,cC as Typography,cD as Underline,cE as Placeholder,cF as TextAlign,cG as BubbleMenu,cH as EditorContent,cI as _default$2,cJ as Collapse,cK as __awaiter,cL as __generator,cM as __spread,cN as react$1,cO as require$$0,cP as loadable$2,cQ as BsImages,cR as Textarea,cS as MdSend,cT as useBreakpointValue,cU as MdDoneOutline,cV as FiSearch,cW as mt,cX as XCircleIcon,cY as Progress,cZ as Spacer,c_ as useList,c$ as addMethod,d0 as yup,d1 as create,d2 as create$2,d3 as o$1,d4 as useDebounce$2,d5 as RadioGroup,d6 as Radio,d7 as ButtonGroup,d8 as CgCloseR,d9 as PopoverArrow,da as PopoverHeader,db as IoMdAddCircleOutline,dc as PerfectScrollbar,dd as AiOutlineFileText,de as FaRegFileWord,df as RiFileExcel2Line,dg as MdGroupAdd,dh as WrapItem,di as TiUserAdd,dj as GoLocation,dk as BsTelephone,dl as FiFacebook,dm as FiMail,dn as BsTags,dp as useColorMode,dq as Container,dr as HamburgerIcon,ds as RiMoonClearLine,dt as SunIcon,du as ChatIcon,dv as Center,dw as MenuDivider,dx as AiOutlineUser,dy as AiOutlineLogout,dz as Breadcrumb,dA as BreadcrumbItem,dB as BreadcrumbLink,dC as matchPath,dD as Navigate,dE as Alert,dF as Code,dG as MdRefresh,dH as customParseFormat,dI as Divider,dJ as components$1,dK as Wt,dL as MdOutlineCancel,dM as Circle,dN as MdClear,dO as FullCalendar,dP as main,dQ as main$1,dR as Drawer$1,dS as DrawerContent,dT as ErrorBoundary,dU as BrowserRouter,dV as Routes,dW as Route,dX as extendTheme,dY as withDefaultColorScheme,dZ as ReactDOM,d_ as ChakraProvider,d$ as ColorModeScript}from"./vendor.js";const p=function(){const r=document.createElement("link").relList;if(r&&r.supports&&r.supports("modulepreload"))return;for(const l of document.querySelectorAll('link[rel="modulepreload"]'))a(l);new MutationObserver(l=>{for(const c of l)if(c.type==="childList")for(const u of c.addedNodes)u.tagName==="LINK"&&u.rel==="modulepreload"&&a(u)}).observe(document,{childList:!0,subtree:!0});function n(l){const c={};return l.integrity&&(c.integrity=l.integrity),l.referrerpolicy&&(c.referrerPolicy=l.referrerpolicy),l.crossorigin==="use-credentials"?c.credentials="include":l.crossorigin==="anonymous"?c.credentials="omit":c.credentials="same-origin",c}function a(l){if(l.ep)return;l.ep=!0;const c=n(l);fetch(l.href,c)}};p();var index$5="";const scriptRel="modulepreload",seen={},base$1="/",__vitePreload=function(r,n){return!n||n.length===0?r():Promise.all(n.map(a=>{if(a=`${base$1}${a}`,a in seen)return;seen[a]=!0;const l=a.endsWith(".css"),c=l?'[rel="stylesheet"]':"";if(document.querySelector(`link[href="${a}"]${c}`))return;const u=document.createElement("link");if(u.rel=l?"stylesheet":scriptRel,l||(u.as="script",u.crossOrigin=""),u.href=a,document.head.appendChild(u),l)return new Promise((d,m)=>{u.addEventListener("load",d),u.addEventListener("error",m)})})).then(()=>r())};var global$1="";const PostFragment=gql`
  fragment Post on Post {
    id
    title
    slug
    excerpt
    content
    status
    numOfComments
    categories {
      id
      title
      slug
    }
    tags {
      id
      name
      slug
    }
    postGroups {
      id
      title
      slug
    }
    publishedAt
    featureMedias {
      id
      type
      path
      variants {
        id
        type
        width
        height
        path
      }
    }
    user {
      id
      role
      username
      fullName
      phone
      gender
      avatar
    }
    censor {
      id
      fullName
    }
    createdAt
    updatedAt
    numOfContextualLink
  }
`;gql`
  query posts(
    $after: String
    $before: String
    $first: Int
    $last: Int
    $categorySlugs: [String]
    $relatedPost: ObjectID
  ) @api(name: "zim") {
    posts(
      after: $after
      before: $before
      first: $first
      last: $last
      categorySlugs: $categorySlugs
      relatedPost: $relatedPost
    ) {
      totalCount
      edges {
        cursor
        node {
          ...Post
        }
      }
      pageInfo {
        startCursor
        endCursor
        hasNextPage
        hasPreviousPage
      }
    }
  }
  ${PostFragment}
`;gql`
  query ($idOrSlug: String!) @api(name: "zim") {
    post(idOrSlug: $idOrSlug) {
      ...Post
      seo {
        title
        description
        ogDescription
        ogImage {
          id
          path
        }
        ogTitle
        publisher
        noIndex
        noFollow
        canonicalUrl
        customHeadHtml
      }
      rejectReason
    }
  }
  ${PostFragment}
`;gql`
  query postPagination(
    $searchString: String
    $page: Int
    $limit: Int
    $categoryIds: [ObjectID]
    $tagIds: [ObjectID]
    $postGroupIds: [ObjectID]
    $userIds: [Int]
    $statuses: [PostStatus]
    $censorUserIds: [Int]
    $orderBy: PostOrderBy
    $order: OrderDirection
  ) @api(name: "zim") {
    postPagination(
      searchString: $searchString
      page: $page
      limit: $limit
      categoryIds: $categoryIds
      tagIds: $tagIds
      postGroupIds: $postGroupIds
      userIds: $userIds
      statuses: $statuses
      censorUserIds: $censorUserIds
      orderBy: $orderBy
      order: $order
    ) {
      hasNextPage
      hasPrevPage
      limit
      nextPage
      prevPage
      totalDocs
      totalPages
      docs {
        ...Post
      }
    }
  }
  ${PostFragment}
`;gql`
  mutation updatePost($postId: ObjectID!, $input: UpdatePostData!)
  @api(name: "zim") {
    updatePost(input: $input, postId: $postId) {
      ...Post
    }
  }
  ${PostFragment}
`;gql`
  mutation createPost($input: CreatePostData!) @api(name: "zim") {
    createPost(input: $input) {
      ...Post
    }
  }
  ${PostFragment}
`;gql`
  mutation createTag($name: String!, $description: String) @api(name: "zim") {
    createTag(description: $description, name: $name) {
      id
      name
      slug
      description
    }
  }
`;gql`
  mutation publishPost($postId: ObjectID!) @api(name: "zim") {
    publishPost(postId: $postId) {
      id
      title
      status
    }
  }
`;gql`
  mutation unpublishPost($postId: ObjectID!) @api(name: "zim") {
    unpublishPost(postId: $postId) {
      id
      title
      status
    }
  }
`;gql`
  mutation requestPostForApproval($postId: ObjectID!) @api(name: "zim") {
    requestPostForApproval(postId: $postId) {
      id
      title
      status
    }
  }
`;gql`
  mutation approveOrRejectPost(
    $postId: ObjectID!
    $status: ApproveOrRejectPostStatus
    $rejectReason: String
    $updatedContent: String
  ) @api(name: "zim") {
    approveOrRejectPost(
      postId: $postId
      status: $status
      rejectReason: $rejectReason
      updatedContent: $updatedContent
    ) {
      id
      title
      status
      rejectReason
      content
    }
  }
`;gql`
  mutation reassignPost($postId: ObjectID!, $userId: Int!) @api(name: "zim") {
    reassignPost(postId: $postId, userId: $userId) {
      id
      user {
        id
        birthday
        email
        fullName
        avatar
        roleId
        role
      }
    }
  }
`;const SIGN_IN=gql`
  mutation signInV2($username: String!, $password: String!) @api(name: "zim") {
    signInV2(username: $username, password: $password) {
      token
      expired_at
      refreshToken
    }
  }
`,GET_USER=gql`
  query @api(name: "zim") {
    me {
      id
      role
      roleId
      username
      fullName
      phone
      birthday
      gender
      avatar
      email
      canCensor
    }
  }
`,SIGN_OUT=gql`
  mutation signOut @api(name: "zim") {
    signOut
  }
`;gql`
  mutation createPostCategory($input: PostCategoryInput!) @api(name: "zim") {
    createPostCategory(input: $input) {
      id
      title
      slug
      description
    }
  }
`;const POST_CATEGORY_FRAGMENT=gql`
  fragment CategoryFields on PostCategory {
    id
    title
    slug
    parentCategory {
      id
      title
      slug
    }
    description
  }
`;gql`
  query postCategoriesTree @api(name: "zim") {
    postCategoriesTree {
      ...CategoryFields
      children {
        ...CategoryFields
        children {
          ...CategoryFields
          children {
            ...CategoryFields
          }
        }
      }
    }
  }
  ${POST_CATEGORY_FRAGMENT}
`;gql`
  query postCategories($flatten: Boolean, $search: String, $parent: ObjectID)
  @api(name: "zim") {
    postCategories(search: $search, parent: $parent, flatten: $flatten) {
      ...CategoryFields
      children {
        ...CategoryFields
      }
    }
  }
  ${POST_CATEGORY_FRAGMENT}
`;gql`
  mutation updatePostCategory($id: ObjectID!, $input: UpdatePostCategoryInput!)
  @api(name: "zim") {
    updatePostCategory(id: $id, input: $input) {
      id
      title
      slug
      description
    }
  }
`;gql`
  mutation deletePostCategory($id: ObjectID!) @api(name: "zim") {
    deletePostCategory(id: $id)
  }
`;gql`
  query comments(
    $type: CommentType!
    $after: String
    $before: String
    $first: Int
    $last: Int
    $refId: String!
    $parent: ObjectID
  ) @api(name: "zim") {
    comments(
      type: $type
      after: $after
      before: $before
      first: $first
      last: $last
      refId: $refId
      parent: $parent
    ) {
      totalCount
      edges {
        cursor
        node {
          ... on PostComment {
            id
            user {
              id
              role
              username
              fullName
              phone
              birthday
              gender
              avatar
            }
            content
            parent {
              id
              content
              user {
                id
                role
                username
                fullName
                phone
                birthday
                gender
                avatar
              }
            }
            liked
            numOfLikes
            createdAt
            updatedAt
          }
          __typename
        }
      }
      pageInfo {
        startCursor
        endCursor
        hasNextPage
        hasPreviousPage
      }
    }
  }
`;gql`
  mutation postComment(
    $content: String!
    $type: CommentType
    $ref: ObjectID!
    $parent: ObjectID
  ) @api(name: "zim") {
    postComment(content: $content, type: $type, ref: $ref, parent: $parent) {
      ... on PostComment {
        id
        content
        user {
          id
          role
          username
          fullName
          phone
          birthday
          gender
          avatar
        }
        parent {
          id
          content
          user {
            id
            role
            username
            fullName
            phone
            birthday
            gender
            avatar
          }
          liked
          numOfLikes
          createdAt
          updatedAt
        }
        liked
        numOfLikes
        createdAt
        updatedAt
      }
      __typename
    }
  }
`;gql`
  mutation likeOrUnlikeComment($commentId: ObjectID!, $liked: Boolean!)
  @api(name: "zim") {
    likeOrUnlikeComment(commentId: $commentId, liked: $liked) {
      ... on PostComment {
        id
        liked
        numOfLikes
      }
    }
  }
`;const CourseFragment=gql`
  fragment Course on CoursePlanType {
    id
    slug
    start_date
    program {
      id
      name
      cat {
        id
        name
      }
      subject {
        id
        name
      }
      program_type {
        id
        name
      }
      level {
        subject_id
        id
        name
        graduation
        status
      }
      fee
      status
      sub_cats {
        id
        cat_id
        name
        status
        parent_id
      }
    }
    subject {
      id
      name
      status
    }
    level_out {
      subject_id
      id
      name
      graduation
    }
    type
    size {
      id
      name
      size
      status
    }
    school {
      id
      city {
        id
      }
      district {
        id
      }
      name
      address
      phone
      color_code
      lat
      lon
      status
    }
    curriculum {
      id
      name
      program {
        id
      }
      shift_minute
      total_lesson
      status
    }
    shifts {
      id
      start_time
      end_time
      status
      shift_minute
    }
    day_of_weeks {
      id
      name
    }
    config_fee {
      detail_name
      detail_id
      type
      fee
    }
    fee
    status
    ec_id
    teacher_lead_id
    description
    featured_image
  }
`,GET_COURSES=gql`
  query getListCoursePagination(
    $order_by: String
    $order: String
    $q: String
    $school_id: Int
    $city_id: Int
    $program_id: Int
    $program_type_id: Int
    $limit: Int
    $cat_id: Int
    $level_id: Int
    $subject_id: Int
    $status: [CourseStatusEnum]
    $type: EnumCourseTypeEnum
    $fromdate: String
    $todate: String
    $page: Int
    $bussiness_partner_id: Int
  ) @api(name: "appZim") {
    getListCoursePagination(
      q: $q
      status: $status
      subject_id: $subject_id
      level_id: $level_id
      cat_id: $cat_id
      school_id: $school_id
      city_id: $city_id
      program_id: $program_id
      program_type_id: $program_type_id
      type: $type
      fromdate: $fromdate
      todate: $todate
      order_by: $order_by
      order: $order
      page: $page
      limit: $limit
      bussiness_partner_id: $bussiness_partner_id
    ) {
      hasNextPage
      hasPrevPage
      limit
      page
      pageNext
      pagePrev
      pagingCounter
      totalPages
      totalDocs
      docs {
        id
        slug
        name
        start_date
        subcats {
          id
          cat_id
          name
          status
          parent_id
        }
        subject {
          id
          name
          status
        }
        school {
          id
          name
        }

        shifts {
          id
          start_time
          end_time
        }
        level_out {
          subject_id
          id
          name
          graduation
        }
        day_of_weeks {
          id
          name
        }
        size {
          id
          name
          size
          status
        }
        fee
        status
        total_register
        total_paid
        config_fee {
          type
          fee
          detail_id
          detail_name
        }
        end_date
        type
        ec {
          id
          full_name
        }
        teachers {
          id
          user_name
          full_name
        }
      }
    }
  }
`,GET_COURSE=gql`
  query course($idOrSlug: String) @api(name: "appZim") {
    course(idOrSlug: $idOrSlug) {
      id
      slug
      name
      start_date
      subcats {
        id
        cat_id
        name
        status
        parent_id
      }
      subject {
        id
        name
        status
      }
      school {
        id
        name
      }
      coefficient_range {
        id
        name
      }
      shifts {
        id
        start_time
        end_time
      }
      level_in {
        subject_id
        id
        name
        graduation
      }
      level_out {
        subject_id
        id
        name
        graduation
      }
      day_of_weeks {
        id
        name
      }
      size {
        id
        name
        size
        status
      }

      fee
      status
      curriculum {
        id
        name
      }
      rooms {
        id
        name
      }
      total_register
      total_paid
      config_fee {
        type
        fee
        detail_id
        detail_name
      }
      end_date
      type
      ec {
        id
        full_name
      }
      teachers {
        id
        user_name
        full_name
      }
    }
  }
  ${CourseFragment}
`,GET_COURSE_PLAN=gql`
  query coursePlan($idOrSlug: String) @api(name: "appZim") {
    coursePlan(idOrSlug: $idOrSlug) {
      id
      slug
      name
      start_date
      subcats {
        id
        cat_id
        name
        status
        parent_id
      }
      subject {
        id
        name
        status
      }
      school {
        id
        name
      }
      coefficient_range {
        id
        name
      }
      shifts {
        id
        start_time
        end_time
      }
      level_in {
        subject_id
        id
        name
        graduation
      }
      level_out {
        subject_id
        id
        name
        graduation
      }
      day_of_weeks {
        id
        name
      }
      size {
        id
        name
        size
        status
      }

      fee
      status
      curriculum {
        id
        name
      }
      rooms {
        id
        name
      }
      config_fee {
        type
        fee
        detail_id
        detail_name
      }
      type
      ec {
        id
        full_name
      }
      teachers {
        id
        user_name
        full_name
      }
    }
  }
  ${CourseFragment}
`,GET_COURSE_FEE_SUGGESTION=gql`
  query getCourseFeeSuggestion($input: CourseFeeSuggestionInput)
  @api(name: "appZim") {
    getCourseFeeSuggestion(input: $input)
  }
`;gql`
  query coursesPlan($offset: Int, $limit: Int, $school_id: Int, $city_id: Int)
  @api(name: "appZim") {
    coursesPlan(
      school_id: $school_id
      city_id: $city_id
      offset: $offset
      limit: $limit
    ) {
      totalCount
      edges {
        node {
          ...Course
        }
      }
      pageInfo {
        hasNextPage
        hasPreviousPage
      }
    }
  }
  ${CourseFragment}
`;gql`
  query coursesPlan($limit: Int) @api(name: "appZim") {
    coursesPlan(status: incoming, limit: $limit) {
      edges {
        node {
          id
          slug
        }
      }
    }
  }
`;gql`
  query coursePlan($idOrSlug: String!) @api(name: "appZim") {
    coursePlan(idOrSlug: $idOrSlug) {
      ...Course
    }
  }
  ${CourseFragment}
`;gql`
  mutation createAdvisory(
    $name: String
    $phone: String!
    $email: String!
    $city_id: Int!
    $gclid: String
    $fbclid: String
    $link: String
    $note: String
  ) @api(name: "appZim") {
    createAdvisory(
      name: $name
      phone: $phone
      email: $email
      city_id: $city_id
      gclid: $gclid
      fbclid: $fbclid
      link: $link
      note: $note
    ) {
      success
      error
      message
    }
  }
`;gql`
  mutation checkout($input: CheckoutInput!) @api(name: "zim") {
    checkout(input: $input) {
      paymentUrl
      order {
        id
        status
        paymentMethod
        paymentStatus
        shippingMethod
        orderId
        email
        shippingAddress {
          recipientName
          recipientPhone
          email
          street1
          street2
          city
          state
          zip
          country
        }
        total
      }
    }
  }
`;const OrderBaseFieldsFragment=gql`
  fragment OrderBaseFields on Order {
    id
    status
    paymentMethod
    paymentStatus
    shippingMethod
    orderId
    status
    email
    shippingAddress {
      recipientName
      recipientPhone
      email
      street1
      street2
      city
      state
      zip
      country
    }
    total
    createdAt
    updatedAt
  }
`,OrderDetailsFields=gql`
  fragment OrderDetailFields on Order {
    ...OrderBaseFields
    user {
      id
      fullName
    }
    billingAddress {
      recipientName
      recipientPhone
      email
      street1
      street2
      city
      state
      zip
      country
    }
    attachments {
      id
      filename
      filepath
    }
    total
    totalPreDiscount
    discount
    coupons
    items {
      quantity
      total
      preTaxTotal
      price
      salePrice
      tax
      type
      item {
        ... on SimpleProduct {
          id
          name
          slug
          images {
            id
            path
          }
          sku
          attributes {
            isVariantAttribute
            attribute {
              id
              name
            }
            value
          }
        }
        ... on Post {
          id
          excerpt
          featureMedias {
            id
            path
          }
          title
          slug
        }
        ... on MockTestItem {
          data
        }
      }
    }
  }
  ${OrderBaseFieldsFragment}
`;gql`
  query order($idOrCode: String!) @api(name: "zim") {
    order(idOrCode: $idOrCode) {
      ...OrderBaseFields
      ...OrderDetailFields
    }
  }
  ${OrderDetailsFields}
`;gql`
  query orderPagination(
    $searchString: String
    $statuses: [OrderStatus]
    $userIds: [Int]
    $orderBy: OrderOrderBy
    $order: OrderDirection
    $page: Int
    $limit: Int
    $itemType: [CheckoutProduct]
  ) @api(name: "zim") {
    orderPagination(
      searchString: $searchString
      statuses: $statuses
      userIds: $userIds
      order: $order
      orderBy: $orderBy
      page: $page
      limit: $limit
      itemType: $itemType
    ) {
      totalPages
      totalDocs
      docs {
        ...OrderBaseFields
      }
    }
  }
  ${OrderBaseFieldsFragment}
`;gql`
  query tags(
    $after: String
    $before: String
    $first: Int
    $last: Int
    $search: String
  ) @api(name: "zim") {
    tags(
      after: $after
      before: $before
      first: $first
      last: $last
      search: $search
    ) {
      edges {
        node {
          id
          name
          description
          slug
        }
      }
      pageInfo {
        hasNextPage
        hasPreviousPage
        startCursor
        endCursor
      }
    }
  }
`;gql`
  query postCategories($search: String, $parent: ObjectID) @api(name: "zim") {
    postCategories(search: $search, parent: $parent) {
      id
      title
      slug
      parentCategory {
        id
        title
        slug
      }
      children {
        id
        title
        slug
        parentCategory {
          id
          title
          slug
        }
      }
    }
  }
`;const ProductBaseFieldsFragment=gql`
  fragment ProductBaseField on BaseProduct {
    ... on SimpleProduct {
      id
      sku
      name
      slug
      regularPrice
      salePrice
      salePriceFrom
      salePriceTo
      description
      shortDescription
      numOfContextualLink
      attributes {
        id
        attribute {
          id
          name
          type
          options
        }
        value
        isVariantAttribute
      }
      parentProduct {
        id
      }
      price
      categories {
        id
        title
        slug
      }
      seo {
        title
        description
        ogDescription
        ogImage {
          id
          path
        }
        ogTitle
        publisher
        noIndex
        noFollow
        canonicalUrl
        customHeadHtml
      }
      images {
        id
        path
      }
      status
      productType
      tags {
        id
        name
        description
        slug
      }
      downloadable
      attributes {
        attribute {
          id
          type
          name
          options
        }
        value
      }
      inStock
      inventories {
        id
        inStock
        manageStock
        reserved
        storeId
        inventory
        store {
          id
          name
        }
      }
      createdAt
      updatedAt
    }
    ... on VariableProduct {
      id
      sku
      name
      slug
      categories {
        id
        title
        slug
      }
      images {
        id
        filename
        path
      }
      description
      shortDescription
      status
      minPrice
      maxPrice
      minRegularPrice
      maxRegularPrice
      productType
      inStock
      variationAttributes {
        id
        name
        type
        options
      }
      createdAt
      updatedAt
      seo {
        title
        description
        ogDescription
        ogImage {
          id
          path
        }
        ogTitle
        publisher
        noIndex
        noFollow
        canonicalUrl
        customHeadHtml
      }
    }
  }
`,ProductDetailsFields=gql`
  fragment ProductDetailFields on BaseProduct {
    ...ProductBaseField
    ... on SimpleProduct {
      tags {
        id
        name
      }
      downloadable
      downloads {
        id
        filename
        path
      }

      attributes {
        id
        attribute {
          id
          name
          type
          options
        }
        value
        isVariantAttribute
        media {
          id
          type
          filename
          path
        }
      }
      parentProduct {
        id
        slug
        name
        status
        variationAttributes {
          id
          name
          type
          options
        }
      }
    }
    ... on VariableProduct {
      childCount
      childPublishedCount
      attributes {
        id
        attribute {
          id
          name
          type
          options
        }
        value
        media {
          id
          type
          filename
          path
        }
      }
      tags {
        id
        name
      }
    }
  }
  ${ProductBaseFieldsFragment}
`;gql`
  query product($id: ObjectID!) @api(name: "zim") {
    productAdmin(id: $id) {
      ...ProductBaseField
      ...ProductDetailFields
    }
  }
  ${ProductDetailsFields}
`;gql`
  query productPagination(
    $searchString: String
    $statuses: [ProductStatus]
    $orderBy: ProductOrderBy
    $order: OrderDirection
    $page: Int
    $limit: Int
    $parentProduct: ObjectID
    $tagSlugs: [String]
    $flatten: Boolean
    $simpleOnly: Boolean
    $tagIds: [ObjectID]
  ) @api(name: "zim") {
    productPagination(
      searchString: $searchString
      statuses: $statuses
      parentProduct: $parentProduct
      order: $order
      orderBy: $orderBy
      page: $page
      limit: $limit
      tagSlugs: $tagSlugs
      flatten: $flatten
      simpleOnly: $simpleOnly
      tagIds: $tagIds
    ) {
      totalPages
      totalDocs
      docs {
        ...ProductBaseField
      }
    }
  }
  ${ProductBaseFieldsFragment}
`;gql`
  query publishedProducts(
    $search: String
    $after: String
    $before: String
    $first: Int
    $last: Int
  ) @api(name: "zim") {
    publishedProducts(
      after: $after
      before: $before
      first: $first
      last: $last
      search: $search
    ) {
      totalCount
      pageInfo {
        hasNextPage
        hasPreviousPage
        startCursor
        endCursor
      }
      edges {
        cursor
        node {
          ...ProductBaseField
        }
      }
    }
  }
  ${ProductBaseFieldsFragment}
`;gql`
  query productVariants($parentProductId: ObjectID!) @api(name: "zim") {
    productVariants(parentProductId: $parentProductId) {
      ...ProductBaseField
    }
  }
  ${ProductBaseFieldsFragment}
`;gql`
  query (
    $searchString: String
    $excludeIds: [ObjectID]
    $page: Int
    $limit: Int
  ) @api(name: "zim") {
    productAttributePagination(
      page: $page
      limit: $limit
      excludeIds: $excludeIds
      searchString: $searchString
    ) {
      totalDocs
      docs {
        id
        name
        options
        type
      }
    }
  }
`;gql`
  query ($id: ObjectID!) @api(name: "zim") {
    productAttribute(id: $id) {
      id
      name
      options
      type
    }
  }
`;gql`
  query @api(name: "zim") {
    productCategoriesTree {
      id
      title
      slug
      parentCategory {
        id
        title
        slug
      }
      children {
        id
        title
        slug
        parentCategory {
          id
          title
          slug
        }
      }
    }
  }
`;gql`
  query productCategories($search: String, $parent: ObjectID)
  @api(name: "zim") {
    productCategories(search: $search, parent: $parent) {
      id
      title
      slug
      parentCategory {
        id
        title
        slug
      }
      children {
        id
        title
        slug
        parentCategory {
          id
          title
          slug
        }
      }
    }
  }
`;gql`
  query slugExist($slug: String!, $type: SlugType, $refId: ObjectID)
  @api(name: "zim") {
    slugExists(type: $type, slug: $slug, refId: $refId)
  }
`;gql`
  query titleExists($title: String!, $type: SlugType, $refId: ObjectID)
  @api(name: "zim") {
    titleExists(type: $type, title: $title, refId: $refId)
  }
`;gql`
  query postViewReport(
    $startTime: DateTime
    $endTime: DateTime
    $page: Int
    $limit: Int
    $userId: Int
  ) @api(name: "zim") {
    postViewReport(
      startTime: $startTime
      endTime: $endTime
      page: $page
      limit: $limit
      userId: $userId
    ) {
      totalDocs
      docs {
        post {
          id
          title
          slug
          user {
            id
            fullName
          }
        }
        viewCount
      }
    }
  }
`;gql`
  query generalPostViewByDate(
    $range: FromToDateInput
    $dateViewMode: DateViewMode
  ) @api(name: "zim") {
    generalPostViewByDate(range: $range, dateViewMode: $dateViewMode) {
      label
      viewCount
      postCount
    }
  }
`;gql`
  query postViewReportByUser(
    $startTime: DateTime
    $endTime: DateTime
    $page: Int
    $limit: Int
  ) @api(name: "zim") {
    postViewReportByUser(
      startTime: $startTime
      endTime: $endTime
      page: $page
      limit: $limit
    ) {
      totalDocs
      docs {
        user {
          id
          fullName
        }
        postCount
        viewCount
      }
    }
  }
`;gql`
  query postReportBySpecificUser(
    $userId: Int!
    $startTime: DateTime
    $endTime: DateTime
  ) @api(name: "zim") {
    postReportBySpecificUser(
      startTime: $startTime
      endTime: $endTime
      userId: $userId
    ) {
      totalLifetimePosts
      totalLifetimePublishedPosts
      totalLifetimeViews
      totalPosts
      totalPublishedPosts
    }
  }
`;gql`
  query postReportByStatus($userId: Int) @api(name: "zim") {
    postReportByStatus(userId: $userId) {
      status
      count
    }
  }
`;gql`
  query postByUser(
    $page: Int
    $limit: Int
    $order: OrderDirection
    $orderBy: PostByUserSort
    $userFilter: UserWithPostUserFilter
  ) @api(name: "zim") {
    postByUser(
      page: $page
      limit: $limit
      order: $order
      orderBy: $orderBy
      userFilter: $userFilter
    ) {
      limit
      page
      totalPages
      totalDocs
      docs {
        user {
          id
          email
          fullName
          avatar
          role
        }
        reviewsAverage
        postCount
        viewCount
        employeeInfo {
          id
          showInFrontPage
          position
          priority
          userId
          user {
            id
            avatar
            role
            username
            fullName
          }
        }
      }
    }
  }
`;gql`
  query commentsPagination(
    $type: CommentType!
    $refId: String!
    $parent: ObjectID
    $page: Int
  ) @api(name: "zim") {
    commentsPagination(
      type: $type
      refId: $refId
      parent: $parent
      page: $page
    ) {
      totalDocs
      totalPages
      limit
      page
      docs {
        ... on PostComment {
          id
          content
          numOfLikes
          createdAt
          numOfChildComments
          user {
            id
            email
            avatar
            fullName
          }
        }
      }
    }
  }
`;gql`
  query comments(
    $first: Int
    $type: CommentType!
    $refId: String!
    $parent: ObjectID
    $after: String
  ) @api(name: "zim") {
    comments(
      type: $type
      refId: $refId
      parent: $parent
      first: $first
      after: $after
    ) {
      pageInfo {
        hasNextPage
        hasPreviousPage
        endCursor
        startCursor
      }
      totalCount
      edges {
        node {
          ... on PostComment {
            id
            content
            numOfLikes
            createdAt
            numOfChildComments
            user {
              id
              email
              avatar
              fullName
            }
          }
        }
        cursor
      }
    }
  }
`;const ACCOUNT_FRAGMENT=gql`
  fragment AccountField on AccountType {
    id
    user_name
    full_name
    phone
    address
    email
    role_id
    role_name
    status
    status_online
    avatar
    supporter
    student_more_info {
      city_id
      city_name
      district_id
      district_name
      ward_id
      ward_name
      street_id
      street_name
      home_number
      source_id
      source_name
      job_id
      job_name
      identity_card
      identity_card_city_id
      identity_card_city_name
      identity_card_date
      learning_status_id
      work_place
      birthday
      note_home
      learning_status_name
    }
  }
`;gql`
  query getUserList($roleIds: [Int], $search: String) @api(name: "zim") {
    users(role_ids: $roleIds, search: $search) {
      totalDocs
      docs {
        id
        fullName
        role
      }
    }
  }
`;gql`
  query user($userId: Int!) @api(name: "zim") {
    user(id: $userId) {
      id
      fullName
      avatar
      role
      roleId
      username
      birthday
      gender
    }
  }
`;gql`
  query user($userId: Int!) @api(name: "zim") {
    user(id: $userId) {
      id
      fullName
      avatar
      role
      roleId
      username
      birthday
      gender
      email
      phone
      moreInfo
    }
  }
`;const GRAPH_NET_GET_USER_INFO=gql`
  query getUserById($userId: Int!) @api(name: "appZim") {
    getUserById(id: $userId) {
      id
      full_name
      avatar
      role_name
      address
      email
      phone
      status_online
      supporter {
        id
        full_name
        avatar
        role_name
      }
      more_info {
        facebook
      }
      courseStudents {
        id
        course {
          status
          id
          slug
          name
        }
      }
    }
  }
`,GET_SUPPORTER_USER_INFO=gql`
  query getUserById($userId: Int!) @api(name: "appZim") {
    getUserById(id: $userId) {
      id
      supporter {
        id
      }
    }
  }
`,GET_LIST_ACCOUNT_CHAT=gql`
  query getAccountChatPagination(
    $searchString: String
    $roles: [EnumRoleType]
    $page: Int
    $limit: Int
  ) @api(name: "appZim") {
    getAccountChatPagination(
      input: { q: $searchString, roles: $roles, page: $page, limit: $limit }
    ) {
      hasNextPage
      hasPrevPage
      limit
      page
      pageNext
      pagePrev
      pagingCounter
      totalPages
      totalDocs
      docs {
        user {
          id
          full_name
          role_id
          role_name
          status
          avatar
          status_online
        }
      }
    }
  }
`;gql`
  query getListStudentsPagination($input: TicketGetListStudentsInputType)
  @api(name: "appZim") {
    getListStudentsPagination(input: $input) {
      hasNextPage
      hasPrevPage
      limit
      page
      pageNext
      pagePrev
      pagingCounter
      totalPages
      totalDocs
      docs {
        supportUId
        userId
        schoolAddress
        course {
          name
        }
        status
        user {
          id
          user_name
          full_name
          phone
          address
          email
          role_id
          role_name
          status
          avatar
        }
      }
    }
  }
`;const GET_STUDENT_SUPPORTED_BY_EC_FOR_DATA_TABLE=gql`
  query getListStudentsPagination($input: TicketGetListStudentsInputType)
  @api(name: "appZim") {
    getListStudentsPagination(input: $input) {
      hasNextPage
      hasPrevPage
      limit
      page
      pageNext
      pagePrev
      pagingCounter
      totalPages
      totalDocs
      docs {
        supportUId
        userId
        schoolAddress
        course {
          name
          code
          #          day_of_weeks {
          #            id
          #            name
          #          }
          #          shifts {
          #            id
          #            start_time
          #            end_time
          #          }
          ec {
            id
            full_name
          }
        }
        status
        user {
          id
          user_name
          full_name
          phone
          address
          email
          role_id
          role_name
        }
      }
    }
  }
`;gql`
  query getListStudentByEc($input: AccountByEcInputType) @api(name: "appZim") {
    getListStudentByEc(input: $input) {
      page
      hasNextPage
      totalPages
      totalDocs
      docs {
        id
        full_name
        avatar
        phone
        address
        email
        status
        supporter
      }
    }
  }
`;const GET_LIST_STUDENT_BY_EC_BY_FIELD=(e=[])=>gql`
  query getListStudentByEc($input: AccountByEcInputType)
  @api(name: "appZim") {
    getListStudentByEc(input: $input) {
      page
      hasNextPage
      docs {
        id
        ${e.join()}
      }
    }
  }
`,GET_PERSONAL_INFO=gql`
  query getPersonalInfo($id: Int!) @api(name: "appZim") {
    getPersonalInfo(id: $id) {
      id
      userName
      sourceOfCustomer {
        id
        sourceOfCustomer
      }
      supporter {
        id
        user_name
        full_name
        phone
        address
        email
      }
      city {
        id
        name
        status
      }
      district {
        id
        name
        status
      }
      ward {
        id
        wardName
      }
      street {
        id
        streetName
      }
      homeNumber
      fullName
      phone
      email
      birthday
      address
      identityCard
      placeOfIssue {
        id
        name
        status
      }
      identityCardDate
      job {
        id
        jobName
      }
      workPlace
      academicPurposes {
        id
        academicPurposesName
      }
      noteHome
      typeOfEducation
      scoreIn
      scoreOut
      status
      statusId
    }
  }
`,GET_USER_BY_PHONE=gql`
  query searchPhone($phone: String!) @api(name: "appZim") {
    searchPhone(phone: $phone) {
      id
      user_name
      full_name
      phone
      address
      email
      role_id
      role_name
      status
      status_online
      avatar
      supporter
      student_more_info {
        identity_card_city_id
        note_home
        birthday
        city_id
        city_name
        district_id
        district_name
        ward_id
        ward_name
        street_id
        street_name
        home_number
        source_id
        source_name
        job_id
        job_name
        identity_card
        identity_card_city_name
        identity_card_date
        learning_status_id
        work_place
        learning_status_name
      }
    }
  }
`;gql`
  query getAccountChatPagination($input: AccountInputGraphNetType)
  @api(name: "appZim") {
    getAccountChatPagination(input: $input) {
      hasNextPage
      hasPrevPage
      limit
      page
      pageNext
      pagePrev
      pagingCounter
      totalPages
      totalDocs
      docs {
        ...AccountField
      }
    }
  }
  ${ACCOUNT_FRAGMENT}
`;const GET_TEACHERS=gql`
  query getListTeacherPagination($input: GetListTeacherInputType)
  @api(name: "appZim") {
    getListTeacherPagination(input: $input) {
      hasNextPage
      hasPrevPage
      limit
      page
      pageNext
      pagePrev
      pagingCounter
      totalPages
      totalDocs
      docs {
        ...AccountField
      }
    }
  }
  ${ACCOUNT_FRAGMENT}
`;gql`
  query isUserExist($field: ExistFieldCheck!, $value: String!)
  @api(name: "zim") {
    isUserExist(field: $field, value: $value)
  }
`;const GET_STUDENT_INVOICE=gql`
  query getStudentInvoices($student_id: Int, $status: EnumInvoiceStatus)
  @api(name: "appZim") {
    getStudentInvoices(student_id: $student_id, status: $status) {
      id
      student {
        id
        user_name
        full_name
        phone
        email
      }
      code
      number_invoice
      paid
      status
      payment
      qr_code
      ec_id
      ec_name
      type
      create_date
      create_by
      update_by
      update_date
      note
      appointment_date
      discount_price
      invoice_detail {
        id
        detail_id
        detail_type
        detail_name
        detail_fee
        subcat_name
      }
    }
  }
`,ADVERTISEMENT_BASE_FIELDS=gql`
  fragment AdvertisementBaseFields on Advertisement {
    id
    type
    status
    title
    description
    image {
      id
      filename
      path
      width
      height
    }
    url
    clicks
    showIn
    startDate
    endDate
    createdAt
    updatedAt
  }
`,ADVERTISEMENT_DETAIL_FIELDS=gql`
  fragment AdvertisementDetailFields on Advertisement {
    ...AdvertisementBaseFields
    postCategories {
      id
      title
    }
    productCategories {
      id
      title
    }
    posts {
      id
      title
    }
    tags {
      id
      name
    }
    products {
      ... on SimpleProduct {
        id
        name
      }
      ... on VariableProduct {
        id
        name
      }
    }
    excludeUrls
    position
    tagHtml
    tagPosition
  }
  ${ADVERTISEMENT_BASE_FIELDS}
`;gql`
  query advertisementPagination(
    $page: Int
    $limit: Int
    $order: OrderDirection
    $orderBy: AdvertisementSortBy
    $statuses: [AdvertisementStatus]
  ) @api(name: "zim") {
    advertisementPagination(
      page: $page
      limit: $limit
      order: $order
      orderBy: $orderBy
      statuses: $statuses
    ) {
      totalDocs
      totalPages
      docs {
        ...AdvertisementBaseFields
      }
    }
  }
  ${ADVERTISEMENT_BASE_FIELDS}
`;gql`
  query advertisement($id: ObjectID!) @api(name: "zim") {
    advertisement(id: $id) {
      ...AdvertisementDetailFields
    }
  }
  ${ADVERTISEMENT_DETAIL_FIELDS}
`;const SUPPORT_CONVERSATIONS=gql`
  query supportConversations(
    $status: ConversationStatus
    $search: String
    $offset: Int
    $limit: Int
  ) @api(name: "chat") {
    supportConversations(
      status: $status
      search: $search
      offset: $offset
      limit: $limit
    ) {
      hasNextPage
      hasPrevPage
      limit
      nextPage
      page
      prevPage
      docs {
        id
        name
        group
        participants {
          userId
          fullName
          nickname
          avatar
          isGuest
        }
        createdAt
        updatedAt
        typing
        usersTyping
        pinnedAt
        blockedBy
        isUnread
        lastMessage {
          id
          text
          from
          to
          type
          attachments {
            type
            attachmentId
            attachment {
              id
              path
              fullUrl
              type
            }
          }
          loading @client
          error @client
          seenBy
          createdAt
          updatedAt
          deletedAt
          callPayload
        }
      }
    }
  }
`,CONVERSATIONS=gql`
  query conversations(
    $status: ConversationStatus
    $search: String
    $offset: Int
    $limit: Int
    $group: String
  ) @api(name: "chat") {
    conversations(
      status: $status
      search: $search
      offset: $offset
      limit: $limit
      group: $group
    ) {
      hasNextPage
      hasPrevPage
      limit
      nextPage
      page
      prevPage
      docs {
        id
        name
        group
        participants {
          userId
          fullName
          nickname
          avatar
          isGuest
          online
        }
        createdAt
        updatedAt
        typing
        usersTyping
        pinnedAt
        blockedBy
        isUnread
        lastMessage {
          id
          text
          from
          to
          type
          attachments {
            type
            attachmentId
            attachment {
              id
              path
              fullUrl
              type
            }
          }
          loading @client
          error @client
          seenBy
          createdAt
          updatedAt
          deletedAt
          callPayload
        }
      }
    }
  }
`,CONVERSATION_DETAIL=gql`
  query conversationDetail($id: ObjectID, $userId: String) @api(name: "chat") {
    conversationDetail(id: $id, userId: $userId) {
      id
      name
      group
      typing
      usersTyping
      isUnread
      participants {
        userId
        fullName
        nickname
        avatar
        isGuest
        online
        roleId
      }
      owner {
        userId
        fullName
      }
      tags {
        id
        name
        color
      }
    }
  }
`,MESSAGES=gql`
  query messages(
    $conversationId: ObjectID
    $before: ObjectID
    $after: ObjectID
    $userId: String
    $offset: Int
    $limit: Int
  ) @api(name: "chat") {
    messages(
      conversationId: $conversationId
      before: $before
      after: $after
      userId: $userId
      offset: $offset
      limit: $limit
    ) {
      hasNextPage
      hasPrevPage
      limit
      nextPage
      page
      prevPage
      totalDocs
      docs {
        id
        text
        from
        to
        type
        attachments {
          type
          attachmentId
          attachment {
            id
            path
            fullUrl
            type
          }
        }
        conversation {
          id
          participants {
            userId
            avatar
            fullName
          }
        }
        loading @client
        error @client
        seenBy
        createdAt
        updatedAt
        deletedAt
        callPayload
        emojis {
          userId
          emoji
        }
      }
    }
  }
`,GET_NOTES=gql`
  query notes(
    $conversationId: ObjectID
    $userId: String
    $page: Int
    $limit: Int
  ) @api(name: "chat") {
    notes(
      conversationId: $conversationId
      userId: $userId
      page: $page
      limit: $limit
    ) {
      hasNextPage
      hasPrevPage
      limit
      nextPage
      page
      pagingCounter
      prevPage
      totalDocs
      totalPages
      docs {
        id
        content
        createdAt
        createdBy
        owner {
          userId
          fullName
          avatar
        }
      }
    }
  }
`,GET_ATTACHMENTS=gql`
  query getAttachmentsInConversation(
    $conversationId: ObjectID
    $userId: String
    $offset: Int
    $limit: Int
  ) @api(name: "chat") {
    getAttachmentsInConversation(
      conversationId: $conversationId
      userId: $userId
      offset: $offset
      limit: $limit
    ) {
      hasNextPage
      hasPrevPage
      limit
      nextPage
      page
      pagingCounter
      prevPage
      totalDocs
      totalPages
      docs {
        id
        attachments {
          type
          attachmentId
          attachment {
            id
            path
            fullUrl
            type
          }
        }
      }
    }
  }
`,GET_MESSAGES_UNREAD=gql`
  query @api(name: "chat") {
    statisticMessageUnread {
      zimians
      student
      customer
      open
      following
      another
      isMarkSeen
    }
  }
`,GET_TAGS=gql`
  query getListTag($page: Int, $limit: Int) @api(name: "chat") {
    getListTag(page: $page, limit: $limit) {
      docs {
        id
        name
        color
      }
    }
  }
`,SEARCH_MESSAGE=gql`
  query searchMessage($conversationId: ObjectID!, $search: String!)
  @api(name: "chat") {
    searchMessage(conversationId: $conversationId, search: $search) {
      hasNextPage
      hasPrevPage
      limit
      nextPage
      page
      pagingCounter
      prevPage
      totalDocs
      totalPages
      docs {
        id
        text
        from
        to
        type
        conversation {
          id
          participants {
            userId
            fullName
            avatar
          }
        }
        attachments {
          type
          attachmentId
          attachment {
            id
            path
            fullUrl
            type
          }
        }
        loading @client
        error @client
        seenBy
        createdAt
        updatedAt
        deletedAt
        callPayload
      }
    }
  }
`,NEAR_MESSAGE=gql`
  query nearbyMessages($messageId: ObjectID!, $limit: Int) @api(name: "chat") {
    nearbyMessages(messageId: $messageId, limit: $limit) {
      hasNextPage
      hasPrevPage
      limit
      nextPage
      page
      prevPage
      docs {
        id
        text
        from
        to
        type
        attachments {
          type
          attachmentId
          attachment {
            id
            path
            fullUrl
            type
          }
        }
        conversation {
          id
          participants {
            userId
            avatar
            fullName
          }
        }
        loading @client
        error @client
        seenBy
        createdAt
        updatedAt
        deletedAt
        callPayload
      }
    }
  }
`,QA_POST_CATEGORIES=gql`
  query QAPostCategoriesTree @api(name: "zim") {
    QAPostCategoriesTree {
      id
      title
    }
  }
`,ROOMS=gql`
  query (
    $school_id: Int
    $q: String
    $name: String
    $description: String
    $status: String
  ) @api(name: "appZim") {
    rooms(
      school_id: $school_id
      q: $q
      name: $name
      description: $description
      status: $status
    ) {
      id
      name
    }
  }
`,GET_SCHOOLS=gql`
  query schools($city_id: Int, $status: String) @api(name: "appZim") {
    schools(city_id: $city_id, status: $status) {
      id
      city {
        id
        name
      }
      district {
        id
        name
      }
      name
      address
      phone
      status
    }
  }
`,GET_CITIES=gql`
  query @api(name: "appZim") {
    cities {
      id
      name
      status
    }
  }
`;gql`
  query @api(name: "appZim") {
    cats {
      id
      name
    }
  }
`;const GET_SUBJECTS$1=gql`
  query subject($name: String, $status: StatusEnum) @api(name: "appZim") {
    subjects(name: $name, status: $status) {
      id
      name
      status
      cats {
        id
        name
        status
        subject {
          id
          name
          status
        }
      }
    }
  }
`,GET_LEVELS$1=gql`
  query levels($subject_id: Int!) @api(name: "appZim") {
    levels(subject_id: $subject_id, status: active) {
      id
      name
      status
      graduation
      subject_id
    }
  }
`,GET_ROOMS=gql`
  query rooms(
    $school_id: Int!
    $q: String
    $name: String
    $description: String
    $status: String
  ) @api(name: "appZim") {
    rooms(
      school_id: $school_id
      status: $status
      name: $name
      description: $description
      q: $q
    ) {
      id
      name
      status
      description
    }
  }
`;gql`
  query shifts($q: String) @api(name: "appZim") {
    shifts(q: $q, status: "active", shift_minute: 0) {
      id
      start_time
      end_time
      status
      shift_minute
    }
  }
`;gql`
  query getStatusAccount @api(name: "appZim") {
    getStatusAccount {
      key
      value
    }
  }
`;gql`
  query getTypeOfEducation @api(name: "appZim") {
    getTypeOfEducation {
      key
      value
    }
  }
`;gql`
  query getAcademicPurposesAll @api(name: "appZim") {
    getAcademicPurposesAll {
      id
      academicPurposesName
    }
  }
`;const GET_ECS=gql`
  query getEcs @api(name: "appZim") {
    getEcs {
      id
      user_name
      full_name
      phone
      address
      email
      role_id
      role_name
      status
      status_online
      avatar
      supporter
    }
  }
`,GET_ECS_BY_SCHOOL_ID=gql`
  query getListEcForCenter($schoolId: Int) @api(name: "appZim") {
    getListEcForCenter(schoolId: $schoolId) {
      id
      user_name
      full_name
      phone
      address
      email
      role_id
      role_name
      status
      status_online
      avatar
      supporter
    }
  }
`,GET_JOBS=gql`
  query getJobs @api(name: "appZim") {
    getJobs {
      id
      jobName
    }
  }
`,GET_SOURCES=gql`
  query getSourceOfCustomer @api(name: "appZim") {
    getSourceOfCustomer {
      id
      sourceOfCustomer
    }
  }
`,GET_DISTRICTS=gql`
  query districts($city_id: Int!, $streetName: String, $status: StatusEnum)
  @api(name: "appZim") {
    districts(city_id: $city_id, name: $streetName, status: $status) {
      id
      name
      status
    }
  }
`,GET_STREETS_BY_DISTRICT=gql`
  query getStreets($districtid: Int!, $streetName: String, $isHide: Boolean)
  @api(name: "appZim") {
    getStreets(
      districtid: $districtid
      streetName: $streetName
      isHide: $isHide
    ) {
      id
      streetName
    }
  }
`,GET_WARDS_BY_DISTRICT=gql`
  query getWards($districtid: Int!, $wardName: String, $isHide: Boolean)
  @api(name: "appZim") {
    getWards(districtid: $districtid, wardName: $wardName, isHide: $isHide) {
      id
      wardName
    }
  }
`;gql`
  mutation createProductCategory($input: ProductCategoryInput!)
  @api(name: "zim") {
    createProductCategory(input: $input) {
      id
      title
      slug
    }
  }
`;gql`
  mutation updateProductCategory(
    $id: ObjectID!
    $input: UpdateProductCategoryInput!
  ) @api(name: "zim") {
    updateProductCategory(id: $id, input: $input) {
      id
      title
    }
  }
`;gql`
  mutation deleteProductCategory($id: ObjectID!) @api(name: "zim") {
    deleteProductCategory(id: $id)
  }
`;gql`
  mutation ($name: String!, $type: ProductAttributeType!, $options: [String])
  @api(name: "zim") {
    createProductAttribute(name: $name, type: $type, options: $options) {
      id
      type
      name
      options
    }
  }
`;gql`
  mutation (
    $id: ObjectID!
    $name: String
    $type: ProductAttributeType
    $options: [String]
  ) @api(name: "zim") {
    updateProductAttribute(
      id: $id
      name: $name
      type: $type
      options: $options
    ) {
      id
      type
      name
      options
    }
  }
`;gql`
  mutation (
    $type: ProductType!
    $sku: String!
    $description: String!
    $shortDescription: String!
    $name: String!
    $featured: Boolean
    $categories: [ObjectID]
    $images: [ObjectID]
    $attributes: [ProductAttributeInput]
    $tags: [ObjectID]
    $variationAttributes: [ObjectID]
    $regularPrice: Int
    $salePrice: Int
    $salePriceFrom: DateTime
    $salePriceTo: DateTime
    $downloads: [ObjectID]
    $downloadable: Boolean
  ) @api(name: "zim") {
    createProduct(
      input: {
        type: $type
        sku: $sku
        description: $description
        shortDescription: $shortDescription
        name: $name
        featured: $featured
        categories: $categories
        images: $images
        attributes: $attributes
        tags: $tags
        variationAttributes: $variationAttributes
        regularPrice: $regularPrice
        salePrice: $salePrice
        salePriceFrom: $salePriceFrom
        salePriceTo: $salePriceTo
        downloads: $downloads
        downloadable: $downloadable
      }
    ) {
      ...ProductBaseField
      ...ProductDetailFields
    }
  }
  ${ProductDetailsFields}
`;gql`
  mutation (
    $id: ObjectID!
    $description: String!
    $shortDescription: String!
    $name: String!
    $featured: Boolean
    $categories: [ObjectID]
    $images: [ObjectID]
    $attributes: [ProductAttributeInput]
    $tags: [ObjectID]
    $regularPrice: Int
    $salePrice: Int
    $salePriceFrom: DateTime
    $salePriceTo: DateTime
    $downloads: [ObjectID]
    $downloadable: Boolean
    $variationAttributes: [ObjectID]
    $slug: String
    $sku: String
  ) @api(name: "zim") {
    updateProduct(
      id: $id
      input: {
        description: $description
        shortDescription: $shortDescription
        name: $name
        featured: $featured
        categories: $categories
        images: $images
        attributes: $attributes
        tags: $tags
        regularPrice: $regularPrice
        salePrice: $salePrice
        salePriceFrom: $salePriceFrom
        salePriceTo: $salePriceTo
        downloads: $downloads
        downloadable: $downloadable
        variationAttributes: $variationAttributes
        slug: $slug
        sku: $sku
      }
    ) {
      ...ProductBaseField
      ...ProductDetailFields
    }
  }
  ${ProductDetailsFields}
`;gql`
  mutation (
    $id: ObjectID!
    $description: String
    $shortDescription: String
    $name: String
    $categories: [ObjectID]
    $images: [ObjectID]
    $attributes: [ProductAttributeInput]
    $tags: [ObjectID]
    $regularPrice: Int
    $salePrice: Int
    $salePriceFrom: DateTime
    $salePriceTo: DateTime
    $downloads: [ObjectID]
    $downloadable: Boolean
    $slug: String
    $sku: String
  ) @api(name: "zim") {
    updateProductVariants(
      id: $id
      input: {
        description: $description
        shortDescription: $shortDescription
        name: $name
        categories: $categories
        images: $images
        attributes: $attributes
        tags: $tags
        regularPrice: $regularPrice
        salePrice: $salePrice
        salePriceFrom: $salePriceFrom
        salePriceTo: $salePriceTo
        downloads: $downloads
        downloadable: $downloadable
        slug: $slug
        sku: $sku
      }
    ) {
      ...ProductBaseField
      ...ProductDetailFields
    }
  }
  ${ProductDetailsFields}
`;gql`
  mutation ($parentProduct: ObjectID!, $input: AddVariantProductInput!)
  @api(name: "zim") {
    createProductVariants(parentProduct: $parentProduct, input: $input) {
      ...ProductBaseField
      ...ProductDetailFields
    }
  }
  ${ProductDetailsFields}
`;gql`
  mutation ($id: ObjectID!) @api(name: "zim") {
    publishProduct(id: $id) {
      ...ProductBaseField
    }
  }
  ${ProductBaseFieldsFragment}
`;gql`
  mutation ($id: ObjectID!) @api(name: "zim") {
    unPublishProduct(id: $id) {
      ...ProductBaseField
    }
  }
  ${ProductBaseFieldsFragment}
`;gql`
  mutation ($productId: ObjectID!, $inventories: [InventoryInput]!)
  @api(name: "zim") {
    bulkUpdateProductInventory(
      productId: $productId
      inventories: $inventories
    ) {
      id
      inStock
      inventories {
        id
        inStock
        inventory
        reserved
        manageStock
        store {
          id
          name
        }
      }
    }
  }
`;gql`
  mutation ($id: ObjectID!) @api(name: "zim") {
    removeProductVariants(id: $id) {
      ...ProductBaseField
    }
  }
  ${ProductBaseFieldsFragment}
`;gql`
  fragment RecruitmentPostBaseField on RecruitmentPost {
    id
    title
    area
    status
    position
    currentNumOfReplies
    maxCandidates
    startDate
    endDate
    createdAt
    updatedAt
    slug
  }
`;const RECRUITMENT_POST_DETAILS_FIELDS_FRAGMENTS=gql`
  fragment RecruitmentPostDetailsField on RecruitmentPost {
    id
    title
    area
    position
    numOfCandidates
    maxCandidates
    jobDescription
    currentNumOfReplies
    salaryFrom
    slug
    salaryType
    salaryTo
    status
    endDate
    createdAt
    updatedAt
    startDate
    seo {
      title
      description
      ogDescription
      ogImage {
        id
        path
      }
      ogTitle
      publisher
      noIndex
      noFollow
      canonicalUrl
      customHeadHtml
    }
  }
`;gql`
  mutation ($postId: ObjectID!, $seo: PostSeoInput!) @api(name: "zim") {
    updatePostSeoConfig(postId: $postId, seo: $seo) {
      id
      title
      slug
      seo {
        title
        description
        ogDescription
        ogImage {
          id
          path
        }
        ogTitle
        publisher
        noIndex
        noFollow
        canonicalUrl
        customHeadHtml
      }
      createdAt
      updatedAt
    }
  }
`;gql`
  mutation ($productId: ObjectID!, $seo: PostSeoInput!) @api(name: "zim") {
    updateProductSeoConfig(productId: $productId, seo: $seo) {
      ... on SimpleProduct {
        id
        createdAt
        updatedAt
        seo {
          title
          description
          ogDescription
          ogImage {
            id
            path
          }
          ogTitle
          publisher
          noIndex
          noFollow
          canonicalUrl
          customHeadHtml
        }
      }
      ... on VariableProduct {
        id
        createdAt
        updatedAt
        seo {
          title
          description
          ogDescription
          ogImage {
            id
            path
          }
          ogTitle
          publisher
          noIndex
          noFollow
          canonicalUrl
          customHeadHtml
        }
      }
    }
  }
`;gql`
  mutation ($recruitmentPostId: ObjectID!, $seo: CommonSeoInput!)
  @api(name: "zim") {
    updateRecruitmentPostSeoConfig(
      recruitmentPostId: $recruitmentPostId
      seo: $seo
    ) {
      ...RecruitmentPostDetailsField
    }
  }
  ${RECRUITMENT_POST_DETAILS_FIELDS_FRAGMENTS}
`;gql`
  query redirectUrlPagination($page: Int, $limit: Int, $search: String)
  @api(name: "zim") {
    redirectUrlPagination(page: $page, limit: $limit, search: $search) {
      docs {
        id
        prevUrl
        newUrl
        redirectCode
        createdAt
        updatedAt
      }
      totalPages
      totalDocs
    }
  }
`;gql`
  mutation updateRedirectUrl($id: ObjectID!, $input: UpdateRedirectUrlInput!)
  @api(name: "zim") {
    updateRedirectUrl(id: $id, input: $input) {
      id
      prevUrl
      newUrl
      redirectCode
      createdAt
      updatedAt
    }
  }
`;gql`
  mutation deleteRedirectUrl($id: ObjectID!) @api(name: "zim") {
    deleteRedirectUrl(id: $id)
  }
`;gql`
  mutation createRedirectUrl($input: CreateRedirectUrlInput!)
  @api(name: "zim") {
    createRedirectUrl(input: $input) {
      id
      prevUrl
      newUrl
      redirectCode
      createdAt
      updatedAt
    }
  }
`;function getCookie(e){const r=e+"=",a=decodeURIComponent(document.cookie).split(";");for(let l=0;l<a.length;l++){let c=a[l];for(;c.charAt(0)===" ";)c=c.substring(1);if(c.indexOf(r)===0)return c.substring(r.length,c.length)}return""}const getEnviroment=e=>{var n;let r=(n={ZIM_BUILD_ENV:"production",ZIM_BUILD_MODE:"module",ZIM_BASE_URL:"https://app.zim.vn",ZIM_IMAGE_ENDPOINT:"https://graph-api.zim.vn",ZIM_CHAT_ENDPOINT:"https://chat.zim.vn",ZIM_GRAPH_API_ENDPOINT:"https://graph-api.zim.vn",ZIM_GRAPH_NET_API_ENDPOINT:"https://graphnet-api.zim.vn",ZIM_CRAWLER_ENDPOINT:"https://crawler.zim.vn",ZIM_PORT:"3000",ZIM_FRONT_PAGE_URL:"https://zim.vn",ZIM_RECAPTCHA_KEYS:"6LcB3-AcAAAAALD_an7FPdBHvKBYLAA3N8gp3Djk",ZIM_TOKEN:"",ZIM_CHAT_MEDIA_URL:"https://chat-media.zim.vn",ZIM_ADMIN_PAGE_URL:"https://admin.zim.vn",ZIM_MEDIA_APP_ZIM:"https://media-app.zim.vn",BASE_URL:"/",MODE:"production",DEV:!1,PROD:!0}[e])!=null?n:"";if(typeof window!="undefined"&&localStorage){const a=localStorage.getItem(e);return a||r}return r},isModuleMode$1=getEnviroment("ZIM_BUILD_MODE")==="module";getEnviroment("ZIM_TOKEN");const ZIM_TOKEN=getEnviroment("ZIM_TOKEN"),queryRefresh=`
    mutation {
        refreshToken {
            token
            refreshToken
        }
    }
`;let authToken="";const appSetAuthToken=e=>{authToken=e},appClearAuthToken=()=>{authToken=""},appGetAuthToken=()=>ZIM_TOKEN||(isModuleMode$1?getCookie("token"):authToken),errorLink=onError(({graphQLErrors:e,networkError:r})=>{e&&e.map(({message:n,locations:a,path:l})=>console.log(`[GraphQL error]: Message: ${n}, Location: ${a}, Path: ${l}`)),r&&console.log(`[Network error]: ${r}`)}),requestLink=new ApolloLink((e,r)=>new Observable(n=>{let a;const l=e.getContext().hasOwnProperty("headers")?e.getContext().headers:{};return Promise.resolve(e).then(c=>{c.setContext({headers:B({authorization:`Bearer ${appGetAuthToken()}`,"Access-Control-Allow-Credentials":!0,"x-app-token":"0aAUfvzhIZVSDIF0yBk53U8Vokij6xv7"},l)})}).then(()=>{a=r(e).subscribe({next:n.next.bind(n),error:n.error.bind(n),complete:n.complete.bind(n)})}).catch(n.error.bind(n)),()=>{a&&a.unsubscribe()}})),cache=new InMemoryCache({possibleTypes:{BaseProduct:["VariableProduct","SimpleProduct"]},typePolicies:{Participant:{keyFields:["userId"]},AccountStudentInfoType:{keyFields:(e,r)=>`${e.source_name}-${e.district_name}-${e.job_name}-${e.ward_name}-${e.city_name}-${e.street_name}-${e.birthday}`},AccountType:{keyFields:(e,r)=>`${e.id}-${e.full_name}-${e.phone}`},Message:{fields:{loading:{read:()=>!1},error:{read:()=>!1}}},Query:{fields:{getAccountChatPagination:{keyArgs:["input",["q","limit","roles"]]},messages:{keyArgs:["conversationId","userId","offset","before","after"]},supportConversations:{keyArgs:["status"]},conversations:{keyArgs:["group"]},medias:relayStylePagination(["type","width","height","search"]),tags:relayStylePagination(["search"]),comments:relayStylePagination(["type","refId","parent"])}}}});global.wsClients={};const wsClient=e=>{const r=new SubscriptionClient_1(e,{reconnect:!0,connectionParams:()=>({authToken:appGetAuthToken()})});return global.wsClients[e]=r,r},wsLink=e=>new WebSocketLink(wsClient(e)),multiApiLink=new MultiAPILink({endpoints:{zim:getEnviroment("ZIM_GRAPH_API_ENDPOINT"),appZim:getEnviroment("ZIM_GRAPH_NET_API_ENDPOINT"),analytics:getEnviroment("REACT_APP_ANALYTICS_API_ENDPOINT"),chat:getEnviroment("ZIM_CHAT_ENDPOINT"),crawler:getEnviroment("ZIM_CRAWLER_ENDPOINT")},createHttpLink:()=>createUploadLink({credentials:"include",fetch:buildAxiosFetch_1(axios,(e,r,n)=>H(B({},e),{withCredentials:!0,onUploadProgress:n.onUploadProgress}))}),wsSuffix:"/graphql",createWsLink:e=>wsLink(e)}),fetchAccessToken=async()=>{if(ZIM_TOKEN)return{token:ZIM_TOKEN,refreshToken:"2kp09Du2CzEN6JkN3V7DwTbOdslfRNjk"};const e={operationName:null,variables:{},query:queryRefresh};return isModuleMode$1?{token:getCookie("token"),refreshToken:"2kp09Du2CzEN6JkN3V7DwTbOdslfRNjk"}:fetch(`${getEnviroment("ZIM_GRAPH_API_ENDPOINT")}/graphql`,{method:"POST",credentials:"include",body:JSON.stringify(e),headers:{"Access-Control-Request-Headers":"content-type","Content-Type":"application/json; charset=utf-8"}}).then(async r=>{const n=await r.json();return n==null?void 0:n.data.refreshToken})},refreshTokenLink=new TokenRefreshLink({accessTokenField:"token",isTokenValidOrUndefined:()=>{if(isModuleMode$1)return!0;const e=appGetAuthToken();if(e.length===0)return!0;try{const{exp:r}=o(e);return Date.now()<r*1e3}catch{return!1}},fetchAccessToken,handleFetch:e=>{e?(localStorage.setItem("isAuthenticated","true"),appSetAuthToken(e)):localStorage.removeItem("isAuthenticated")},handleResponse:()=>{},handleError:e=>{console.log(`handleError: ${e}`)}}),normalLink=ApolloLink.from([refreshTokenLink,requestLink,errorLink,multiApiLink]),splitLink=split$1(({query:e})=>{const r=getMainDefinition(e);return r.kind==="OperationDefinition"&&r.operation==="subscription"},ApolloLink.from([refreshTokenLink,multiApiLink]),normalLink),apolloClient=new ApolloClient({ssrMode:typeof window=="undefined",link:splitLink,credentials:"include",cache,connectToDevTools:!0}),authContext=react.exports.createContext({appState:{isAuthenticated:!1,user:null,loaded:!1}}),ProvideAuth=({children:e})=>{const[r,n]=react.exports.useState({isAuthenticated:!1,user:null,loaded:!1,buildMode:getEnviroment("ZIM_BUILD_MODE")}),a=async()=>{var u,d;try{const m=await apolloClient.query({query:GET_USER,fetchPolicy:"network-only"});((u=m==null?void 0:m.data)==null?void 0:u.me)?(n(H(B({},r),{user:(d=m==null?void 0:m.data)==null?void 0:d.me,isAuthenticated:!0,loaded:!0})),localStorage.setItem("isAuthenticated","true")):(localStorage.removeItem("isAuthenticated"),n(H(B({},r),{user:null,isAuthenticated:!1,loaded:!0})))}catch{n(H(B({},r),{user:null,isAuthenticated:!1,loaded:!0})),localStorage.removeItem("isAuthenticated")}},l=async u=>{appSetAuthToken(u),await a()},c=async()=>{appSetAuthToken(""),await apolloClient.mutate({mutation:SIGN_OUT}),n(H(B({},r),{user:null,isAuthenticated:!1,loaded:!0})),localStorage.removeItem("isAuthenticated")};return jsx(authContext.Provider,{value:{getUser:a,appState:r,setAppState:n,appSetLogin:l,appSetLogout:c,appSetAuthToken,appClearAuthToken},children:jsx(ApolloProvider,{client:apolloClient,children:e})})};function useAppContext(){return react.exports.useContext(authContext)}const useIsMounted=()=>{const e=react.exports.useRef(!1);return react.exports.useEffect(()=>(e.current=!0,()=>{e.current=!1}),[]),react.exports.useCallback(()=>e.current,[])};var logoSrc="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACsAAAArCAYAAADhXXHAAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAVLSURBVHgBxVlZTJxVFD4MAxRkjQFpQ2UIOyTsOw1LKqExMSUkjYGEqBF8aaI1Gk0jidEHfND2gRf7YGJ4MPpgrH1p0ZTUFtoEZN93BkpQWexggixh8Tu3/BM7673/UPiSO+f+858z//ffe+45597xIg+QlpYWsbW1dRHdHLQUtDNoEWin0LbQFry8vJYPDg7m0b9jMBg6x8fHzaQTXqQIk8kU6uvr+y5IXACJQlIE7Ib39/evGY3GX8bGxv5QspVVPCT5HrpX0ELJc6yhfYP21eTk5KqMgRTZuLi41zCFN+jpNB81zJihD6ampn5yp+iSLEgGY9o+Q7tCzxkg/CUIf+RKxynZ5OTk6L29vZvoZtIxAYQfbG9vVy0sLDxxdN8h2ZiYmJd8fHx+QzeJjh/9WBsXhoeH/7K9YXCkDaK36GSIMjJ2dnZ+xII+ZXvDjmxCQsJ1iHw6WZzD6DbZfvmMG4Do6xA/kCTgY1ReXk4VFRUUEREha0ZLS0vU1NRE8E+Xet7e3hWIxXfJlmxiYuIZPPwhuiaSAII6tbS0UE5ODulBZWUlmc1md2pmuEQm9Cx8YXUDjnWyRBkNDQ26ia6srNDc3JyMqgnr57J2IcjGxsaeRSx9ixTA068XAwMDnHaldKH3IWdP7guyYH8eIxtGkggJCaH09HTSi56eHhX1ULjcm9wRZFFYfKxi7QlRRldXl5A1NTXU2NgoFqorYHS5siNDUlKSiRRjalZWFukFFgyhcBH92tpaqqurc2sDsmXsCgaMqnJMzc/XH4a7u7sF4fDwcA6V1NfXJ+W/iLsX2Q1eJQWg+uK6gfSiv79fSC2S9Pb2ypqmGfBWL5MCsrOzyd/fn/RCI1tcXCxkR0eHrGkKj2w4KeCoFld8fLyQ09PTsqanDViJZ0kBGRkZpBdInbS5uUmBgYHid0ZHR2l5eVnW/EUeWT9ZbeRqKikpIb1ob28XUpsdlIEK1hTIZDdltXnq/Pyk380Og4ODQmqhTzE5WJis1GaNUVRURJ5AW/mFhU83xewGClhiso9ltT1JBkxsdXVVZCsOfYuLi4QzBGl72O3wApOy4Id4srhmZmZE8M/Ly6OAgAAaGRkRMVsW0P3dwB8yyikpKSLr6IUWT1NTU4Vsa2sjFWCwHhmQxn6WUY6OjiZPoC0mTiqM+fl5JXvstB8Zh4aGniBHP8C1y5hUVlZm7VssFuFzqCtIBuyr2F6LfkFBAa2vrwvyHAolcRdutGw8vLjjjmxkZKSQvBWprq6mjY0NUgH7K7b4FBwcLFxCgSi7wHcshYejCuKjIYsrAy3TtLa2KhPVUFpaKmRnZ6e0DYg+RvueNLK8IcObX3dlNDExISTvZKOiomh3d1epcQTIzc0Vv6HVBzJAALiG+kFsg62F5OEpYR852TRy5mpubrb6LhNQAYcpbuznmZmZhHNdGTMzCvVYSLE4jNZvMboI1u9g1f3qyIr3+PX19VRVVSUqfL1hjN1IkiijTiPKsCvRD09k3qeTx+cY1U///4XdkgwKCrqP6ToHHzbRyeE2iDbYfmmX7+AOWyB6CV2lkuiogGc/hF/XOLrnMDnzsTnC2Svo3qNjBGb0JtbMeaz+fxzdd7uthA/zad5Vev74AoP0CaTTQwSpMxwc2vE/M1+TwlmYAmYw7W9jNO+7U5TKeWtra9NhYWE34E+cuvj/riDyEPitv9Guwt0uz87OTkjZkCLwR90LeMAl+NYbfFJCioDNPczStxjNW85806kteQDUuJHIZAUgwIcAXPv5gshpyAC0f3n0cP0n+sOQg3jB2xjFddKJ/wBliiZzlCtvyQAAAABJRU5ErkJggg==";const useBuildModeValue=(e,r)=>getEnviroment("ZIM_BUILD_MODE")==="module"?r:e,NavLink=e=>useBuildModeValue(jsx(Link$1,H(B({rounded:"base",_hover:{textDecoration:"none"},as:Link$2,color:useColorModeValue("blue.500","blue.300")},e),{children:e.children})),jsx(Link$1,H(B({href:e.to,isExternal:!0},e),{children:e.children}))),SideMenu=({data:e,onClose:r})=>jsx(Fragment,{children:jsx(Accordion,{allowToggle:!0,children:e.map((n,a)=>jsxs(AccordionItem,{id:n.link,children:[jsxs(AccordionButton,{justifyContent:"space-between",children:[jsxs(HStack,{justifyContent:"start",children:[n.icon&&jsx(n.icon,{}),jsx(Text,{whiteSpace:"nowrap",children:n.label})]}),jsx(AccordionIcon,{})]}),jsx(AccordionPanel,{p:0,children:n.submenus.map((l,c)=>jsxs(Box,{p:4,children:[l.showLabel&&jsx(Text,{fontWeight:600,mb:4,children:l.label}),jsx(Flex,{direction:"column",gap:4,children:l.children.map((u,d)=>jsx(NavLink,{to:u.link,children:jsxs(Text,{onClick:r,children:[" ",u.label]})},d))})]},`${l.link}-${c}`))})]},a))})}),MegaMenu=({data:e,onClose:r})=>jsx(Fragment,{children:jsxs(Tabs,{d:"flex",children:[jsx(TabList,{borderRight:"1px solid",borderRightColor:useColorModeValue("gray.200","slate.700"),borderBottom:0,pr:4,children:jsx(VStack,{justifyContent:"stretch",alignItems:"stretch",children:e.map((n,a)=>jsx(Tab,{justifyContent:"start",children:jsxs(HStack,{justifyContent:"start",children:[n.icon&&jsx(n.icon,{}),jsx(Text,{whiteSpace:"nowrap",children:n.label})]})},a))})}),jsx(TabPanels,{children:e.map((n,a)=>jsx(TabPanel,{px:4,py:0,children:jsx(HStack,{spacing:0,alignItems:"stretch",justifyContent:"start",mx:-4,flexWrap:"wrap",children:n.submenus.map((l,c)=>jsxs(VStack,{w:{base:"50%",xl:"33%","2xl":"25%"},alignItems:"stretch",justifyContent:"start",spacing:2,p:4,children:[l.showLabel&&jsx(Text,{fontWeight:600,children:l.label}),jsx(VStack,{alignItems:"stretch",justifyContent:"start",spacing:2,children:l.children.map((u,d)=>jsx(NavLink,{to:u.link,children:jsx(Text,{onClick:r,children:u.label})},d))})]},c))})},a))})]})}),routePaths={home:"/",account:"/account",listCourses:"/course",listCoursesPlan:"/course-plan",courseDetail:"/course/detail",coursePlanDetail:"/course-plan/detail",forbiddenError:"/error/403",ticket:"/ticket",chat:"/chat",report:"/report",leadReport:"/report/lead",studentSupportByEc:"/user/student-support-by-ec",scheduleRegistration:"/schedule/schedule-registration",listSchedules:"/schedule/list-schedules",registerTestSchedule:"/schedule/test/reg",lead:"/web-data/lead",paperBasedTestBank:"/paper-based-test-bank",mockTestOnline:"/mock-test/mock-test-online",mockTestOffline:"/mock-test/mock-test-offline",mockTestDetail:"/mock-test/mock-test-detail",exercises:"/exercise",updateExercise:"/exercise/update",coursePlanner:"/course/course-planner",invoice:"/invoice"},appZimPaths={lead:"/Admin/ContactCustomer/ContactList",customerInfo:"/Admin/Customer/CustomerDetai",staffInfo:"/Admin/Staff/StaffDetail",courseDetail:"/Admin/CourseManage/CourseDetail",coursePlanDetail:"/Admin/CourseManage/CoursePlanDetail",ticketList:"/Admin/Ticket",customerList:"/Admin/Customer/CustomersList",listStudentsMockTest:"/Admin/MockTest/ListStudent",mockTestDetail:"/Admin/MockTest/MockTest",ieltsCorrecting:"/Admin/Ieltscorrect/Correcting",newsFeed:"/news-feed",invoice:"/invoice",exerciseDetail:"/Admin/config/ExerciseDetail"},ZimVnPaths={testResult:"/test/result",previewTestModule:"/test/module/preview"},menuData=[{label:"Chung",link:"#",icon:ImNewspaper,submenus:[{label:"",link:"/",icon:null,showLabel:!1,children:[{label:"News",link:"/",icon:null,children:[]},{label:"Dashboard",link:"/",icon:null,children:[]}]}]},{label:"Kh\xF3a h\u1ECDc",link:"#",icon:SiGoogleclassroom,submenus:[{label:"Qu\u1EA3n l\xFD kh\xF3a h\u1ECDc",link:"/",icon:null,showLabel:!0,children:[{label:"T\u1EA1o kh\xF3a h\u1ECDc",link:"/",icon:null,children:[]},{label:"Danh s\xE1ch kh\xF3a h\u1ECDc",link:routePaths.listCourses,icon:null,children:[]},{label:"Kh\xF3a h\u1ECDc d\u1EF1 ki\u1EBFn",link:"/",icon:null,children:[]},{label:"L\u1ECBch d\u1EA1y gi\xE1o vi\xEAn",link:"/",icon:null,children:[]}]}]},{label:"Th\u01B0 vi\u1EC7n",link:"#",icon:IoLibraryOutline,submenus:[{label:"T\xE0i li\u1EC7u",link:"/",icon:null,showLabel:!0,children:[{label:"Document",link:"/",icon:null,children:[]},{label:"T\xE0i li\u1EC7u s\xE1ch",link:"/",icon:null,children:[]},{label:"Video",link:"/",icon:null,children:[]}]}]},{label:"Kh\xE1ch h\xE0ng",link:"#",icon:HiUserGroup,submenus:[{label:"H\u1ECDc vi\xEAn",link:"/",icon:null,showLabel:!0,children:[{label:"Danh s\xE1ch h\u1ECDc vi\xEAn",link:"/",icon:null,children:[]},{label:"Danh s\xE1ch trong kh\xF3a",link:"/",icon:null,children:[]},{label:"Danh s\xE1ch HV b\u1EA3o l\u01B0u",link:"/",icon:null,children:[]},{label:"Danh s\xE1ch kh\xE1ch h\u1EB9n test",link:"/",icon:null,children:[]}]},{label:"\u0110\u0103ng k\xFD",link:"/",icon:null,showLabel:!0,children:[{label:"Leads",link:"/",icon:null,children:[]},{label:"\u0110\u0103ng k\xFD kh\xF3a d\u1EF1 ki\u1EBFn",link:"/",icon:null,children:[]},{label:"\u0110\u0103ng k\xFD ch\u01B0\u01A1ng tr\xECnh",link:"/",icon:null,children:[]},{label:"\u0110\u0103ng k\xFD thi th\u1EED",link:"/",icon:null,children:[]},{label:"\u0110\u0103ng k\xFD thi t\u1EA1i BC",link:"/",icon:null,children:[]},{label:"\u0110\u0103ng k\xFD x\xE9t h\u1ECDc b\u1ED5ng",link:"/",icon:null,children:[]},{label:"HV y\xEAu c\u1EA7u v\xE0o kh\xF3a",link:"/",icon:null,children:[]}]},{label:"H\u1EE3p \u0111\u1ED3ng",link:"/",icon:null,showLabel:!0,children:[{label:"H\u1ECDc vi\xEAn c\xF3 h\u1EE3p \u0111\u1ED3ng",link:"/",icon:null,children:[]},{label:"H\u1EE3p \u0111\u1ED3ng ch\u1EDD duy\u1EC7t",link:"/",icon:null,children:[]}]},{label:"H\u1ECDc t\u1EADp",link:"/",icon:null,showLabel:!0,children:[{label:"C\u1EA3nh b\xE1o h\u1ECDc vi\xEAn",link:"/",icon:null,children:[]},{label:"H\u1ECDc vi\xEAn s\u1EAFp thi",link:"/",icon:null,children:[]},{label:"K\u1EBFt qu\u1EA3 thi th\u1EF1c t\u1EBF",link:"/",icon:null,children:[]},{label:"Th\u01B0\u1EDFng t\xE0i tr\u1EE3",link:"/",icon:null,children:[]}]}]},{label:"Nh\xE2n vi\xEAn",link:"#",icon:FaUserFriends,submenus:[{label:"Kh\u1ED1i kinh doanh",link:"/",icon:null,showLabel:!0,children:[{label:"Danh s\xE1ch nh\xE2n vi\xEAn",link:"/",icon:null,children:[]},{label:"Y\xEAu c\u1EA7u thay \u0111\u1ED5i ca",link:"/",icon:null,children:[]},{label:"Qu\u1EA3n l\xFD ca l\xE0m vi\u1EC7c EC",link:"/",icon:null,children:[]}]},{label:"Gi\u1EA3ng vi\xEAn",link:"/",icon:null,showLabel:!0,children:[{label:"Danh s\xE1ch gi\u1EA3ng vi\xEAn",link:"/",icon:null,children:[]},{label:"B\xE0i \u0111\xE3 ch\u1EA5m",link:"/",icon:null,children:[]},{label:"Gi\u1EDD d\u1EA1y gi\xE1o vi\xEAn",link:"/",icon:null,children:[]},{label:"Y\xEAu c\u1EA7u thay \u0111\u1ED5i l\u1ECBch ngh\u1EC9",link:"/",icon:null,children:[]},{label:"B\xE1o c\xE1 gi\xE1 v\u1ED1n h\xE0ng b\xE1n",link:"/",icon:null,children:[]},{label:"Chi ph\xED l\u01B0\u01A1ng gi\u1EA3ng vi\xEAn",link:"/",icon:null,children:[]}]}]},{label:"T\xE0i ch\xEDnh",link:"#",icon:BiMoney,submenus:[{label:"L\u01B0\u01A1ng kinh doanh",link:"/",icon:null,showLabel:!0,children:[{label:"Chi\u1EBFn d\u1ECBch sale",link:"/",icon:null,children:[]},{label:"Doanh thu chi\u1EBFn d\u1ECBch sale",link:"/",icon:null,children:[]},{label:"L\u01B0\u01A1ng office",link:"/",icon:null,children:[]},{label:"Duy\u1EC7t l\u01B0\u01A1ng office",link:"/",icon:null,children:[]},{label:"L\u1ECBch s\u1EED duy\u1EC7t",link:"/",icon:null,children:[]},{label:"L\u01B0\u01A1ng gi\u1EA3ng vi\xEAn",link:"/",icon:null,children:[]}]},{label:"Phi\u1EBFu thu",link:"/",icon:null,showLabel:!0,children:[{label:"Qu\u1EA3n l\xFD phi\u1EBFu thu",link:"/",icon:null,children:[]},{label:"Y\xEAu c\u1EA7u h\u1EE7y phi\u1EBFu thu",link:"/",icon:null,children:[]}]}]},{label:"D\u1ECBch v\u1EE5",link:"#",icon:MdOutlineMiscellaneousServices,submenus:[{label:"Ch\u1EA5m b\xE0i IELTS Correct",link:"/",icon:null,showLabel:!0,children:[{label:"Danh s\xE1ch b\xE0i ch\u1EA5m",link:"/",icon:null,children:[]},{label:"DS gi\u1EA3ng vi\xEAn ch\u1EA5m",link:"/",icon:null,children:[]},{label:"C\u1EA5u h\xECnh l\u01B0\u01A1ng b\xE0i ch\u1EA5m",link:"/",icon:null,children:[]}]}]},{label:"C\u1EA5u h\xECnh",link:"#",icon:IoSettingsOutline,submenus:[{label:"Kh\xF3a h\u1ECDc",link:"/",icon:null,showLabel:!0,children:[{label:"M\xF4n h\u1ECDc",link:"/",icon:null,children:[]},{label:"Ch\u01B0\u01A1ng tr\xECnh h\u1ECDc",link:"/",icon:null,children:[]},{label:"L\u1ED9 tr\xECnh h\u1ECDc",link:"/",icon:null,children:[]},{label:"H\u1EC7 s\u1ED1",link:"/",icon:null,children:[]},{label:"M\xE3 khuy\u1EBFn m\xE3i",link:"/",icon:null,children:[]}]},{label:"H\u1EC7 th\u1ED1ng",link:"/",icon:null,showLabel:!0,children:[{label:"Trung t\xE2m",link:"/",icon:null,children:[]},{label:"T\u1EC9nh / Th\xE0nh ph\u1ED1",link:"/",icon:null,children:[]},{label:"Qu\u1EADn huy\u1EC7n",link:"/",icon:null,children:[]},{label:"Ngu\u1ED3n kh\xE1ch h\xE0ng",link:"/",icon:null,children:[]},{label:"Ng\xE0y ngh\u1EC9",link:"/",icon:null,children:[]},{label:"Ngh\u1EC1 nghi\u1EC7p",link:"/",icon:null,children:[]},{label:"D\u1ECBch v\u1EE5",link:"/",icon:null,children:[]},{label:"Lo\u1EA1i ph\u1EA3n h\u1ED3i",link:"/",icon:null,children:[]},{label:"Nh\xE0 cung c\u1EA5p",link:"/",icon:null,children:[]},{label:"M\u1EE5c \u0111\xEDch h\u1ECDc",link:"/",icon:null,children:[]},{label:"Th\xE0nh ng\u1EEF l\u1ECBch",link:"/",icon:null,children:[]},{label:"M\u1EABu h\u1EE3p \u0111\u1ED3ng",link:"/",icon:null,children:[]},{label:"\u0110i\u1EC1u kho\u1EA3n",link:"/",icon:null,children:[]}]}]}],useToggle=(e=!1)=>{const[r,n]=react.exports.useState(e),a=react.exports.useCallback(l=>n(c=>l!==void 0?l:!c),[]);return[r,a]},useEventListener=(e,r,n=window)=>{const a=react.exports.useRef();react.exports.useEffect(()=>{a.current=r},[r]),react.exports.useEffect(()=>{if(!(n&&n.addEventListener))return;const c=u=>a&&typeof a.current=="function"?a.current(u):null;return typeof c=="function"&&n.addEventListener(e,c),()=>{typeof c=="function"&&n.removeEventListener(e,c)}},[e,n])},useCombinedRefs=(...e)=>{const r=react.exports.useRef();return react.exports.useEffect(()=>{e.forEach(n=>{!n||(typeof n=="function"?n(r.current):n.current=r.current)})},[e]),r},TextInput=react.exports.forwardRef((a,n)=>{var l=a,{error:e}=l,r=Ae(l,["error"]);return jsxs(FormControl,{isInvalid:!!e,children:[(r==null?void 0:r.label)&&jsx(FormLabel,{htmlFor:r.id,children:r.label}),jsx(Input$1,H(B({bg:useColorModeValue("white","slate.700"),borderColor:useColorModeValue("gray.300","slate.600"),_hover:{borderColor:useColorModeValue("gray.400","slate.700")},color:useColorModeValue("gray.900","slate.300"),_placeholder:{color:useColorModeValue("gray.400","slate.500")}},r),{ref:n})),e&&jsx(FormErrorMessage,{children:e&&(e==null?void 0:e.message)})]})}),SearchInput=react.exports.forwardRef((l,a)=>{var c=l,{placeholder:e,onSubmitSearch:r}=c,n=Ae(c,["placeholder","onSubmitSearch"]);const u=react.exports.useRef(null),d=useCombinedRefs(a,u);return jsx(Box,{as:"form",onSubmit:E=>{E.preventDefault();const{value:C}=d.current;typeof r=="function"&&r(C)},id:"form-header-search",w:"full",children:jsxs(InputGroup,{flexGrow:1,w:"100%",children:[jsx(InputRightElement,{children:jsx(Button$1,{p:0,variant:"ghost",_hover:{backgroundColor:"transparent"},type:"submit",children:jsx(VscSearch,{fontSize:18})})}),jsx(TextInput,H(B({placeholder:"T\xECm ki\u1EBFm...",variant:"filled"},n),{ref:d,pr:10}))]})})}),CONVERSATION_UPDATED=gql`
  subscription ($conversationId: ObjectID, $status: String) @api(name: "chat") {
    conversationUpdated(conversationId: $conversationId, status: $status) {
      id
      name
      isDeleteMessage
      group
      status
      createdAt
      updatedAt
      pinnedAt
      blockedBy
      isUnread
      typing
      usersTyping
      participants {
        userId
        fullName
        nickname
        avatar
        isGuest
        online
      }
      lastMessage {
        id
        text
        from
        to
        type
        attachments {
          type
          attachmentId
          attachment {
            id
            path
            fullUrl
            type
          }
        }
        conversation {
          id
          participants {
            userId
            avatar
            fullName
          }
        }
        loading @client
        error @client
        seenBy
        createdAt
        updatedAt
        deletedAt
        callPayload
        emojis {
          userId
          emoji
        }
      }
    }
  }
`,MESSAGES_UNREAD=gql`
  subscription @api(name: "chat") {
    messageAdded {
      zimians
      student
      customer
      open
      following
      another
      isMarkSeen
    }
  }
`;function useUser(){const{appState:{user:e},setUser:r}=react.exports.useContext(authContext);return[e,r]}var ring$1="/assets/notificationSound.mp3";const useGetMessageUnread=()=>{const[e]=useUser(),r=new Audio(ring$1);r.volume=1;const{data:n,loading:a}=useQuery(GET_MESSAGES_UNREAD),l=n==null?void 0:n.statisticMessageUnread;return useSubscription(MESSAGES_UNREAD,{onSubscriptionData:({client:c,subscriptionData:u})=>{var m,E;const d=c.cache.readQuery({query:GET_MESSAGES_UNREAD});if(u.data){const{messageAdded:C}=u.data;C?(c.writeQuery({query:GET_MESSAGES_UNREAD,data:H(B({},d),{statisticMessageUnread:B(B({},d.statisticMessageUnread),C)})}),((E=(m=u==null?void 0:u.data)==null?void 0:m.messageAdded)==null?void 0:E.isMarkSeen)||r.play()):c.writeQuery({query:GET_MESSAGES_UNREAD,data:B({},d)})}},shouldResubscribe:!!e}),{listMessageUnreadOverall:l,loading:a}},GROUP_ROLES={editor:[1,2,11],approve:[1,8],author:[1,8,11,4],manager:[1,2,12,8,11],productManager:[1,2],censor:[1,2,8],canEdit:[1,2,4,8,11],canConfigSeo:[1,2,11],admin:[1],adminAndCC:[1,2],canCreateTicket:[5,6,12,1,2,11,12],canCancelSchedule:[2],canEnterMockTestScores:[4],canUploadMockTestTask:[6],canCreateOrEditMockTest:[1,2],assignTeacherAndEC:[1,2],viewTableMockTestDetail:[1,2,6],canBookSchedule:[6],canCancelEmptyScheduleOfTeacher:[1]},ROLES=[{id:1,name:"Super Admin"},{id:2,name:"CC"},{id:4,name:"Gi\xE1o vi\xEAn"},{id:5,name:"Student"},{id:6,name:"EC"},{id:8,name:"AM"},{id:9,name:"K\u1EBF to\xE1n"},{id:11,name:"Marketer"},{id:12,name:"BM"},{id:13,name:"BP"},{id:14,name:"Guest"}],NOTI_STUDENT_ROLE=[5],NOTI_EC_ADMIN_ROLE=[1,6],appConfigs={apiUrl:"https://app.zim.vn",apiVersion:"v1",access_token:"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJqc29uX3dlYl90b2tlbiIsImp0aSI6ImM5ZDFmNzA3LTk4ZmYtNGNmMC1iMGUyLTAwN2RhY2MzYTVhNSIsImlhdCI6IjEvNC8yMDIyIDM6MDE6MzIgUE0iLCJpZCI6IjM0NDg4IiwidXNlcl9uYW1lIjoiemltIiwiZnVsbF9uYW1lIjoiWklNIEFjYWRlbXkiLCJyb2xlX2lkIjoiMSIsInJvbGVfbmFtZSI6IlN1cGVyIEFkbWluIiwiZXhwIjoxNjY3NTc0MDkyLCJpc3MiOiJhcHAuemltLnZuIiwiYXVkIjoiYXBwLnppbS52biJ9.RPOVJ-aana5NoeyUEZ_WYI_i0f2gBqU0o1W68WO51Ro"},useGetOverallUnreadMessage=()=>{var E;const[e]=useUser(),r=e.roleId,{listMessageUnreadOverall:n}=useGetMessageUnread(),a=(n==null?void 0:n.another)?Object==null?void 0:Object.values(JSON==null?void 0:JSON.parse(n==null?void 0:n.another)):[],l=[n==null?void 0:n.zimians,n==null?void 0:n.student,n==null?void 0:n.following,n==null?void 0:n.open,(E=a==null?void 0:a.map(C=>parseInt(C,10)))==null?void 0:E.reduce((C,_)=>C+_,0)],c=l[1]+l[4],u=l[0]+l[1]+l[4],d=l==null?void 0:l.reduce((C,_)=>C+_,0),m=NOTI_STUDENT_ROLE.includes(r)?c:NOTI_EC_ADMIN_ROLE.includes(r)?d:u;return react.exports.useEffect(()=>{const C=document.getElementById("noti-chat");C&&getEnviroment("ZIM_BUILD_MODE")==="module"&&(C.innerHTML=`${m}`)},[m]),{overall:l,overallMyChats:m}},SearchBox=({searchString:e,setSearchString:r})=>jsxs(Box,{flex:1,maxH:"35px",borderWidth:1,borderColor:"#e4e4e4",rounded:"full",display:"flex",flexDirection:"row",alignItems:"center",px:2,overflow:"hidden",bg:"white",children:[jsx(Input$1,{variant:"unstyled",placeholder:"T\xECm ki\u1EBFm",p:2,value:e,onChange:a=>{a.target&&r(a.target.value)},color:"#373737"}),jsx(SearchIcon,{className:"h-5 w-5 text-gray-400"})]});var AdReportSort;(function(e){e.ClickRatio="clickRatio",e.Clicks="clicks",e.Displays="displays"})(AdReportSort||(AdReportSort={}));var AdvertisementShowIn;(function(e){e.Post="Post",e.Product="Product"})(AdvertisementShowIn||(AdvertisementShowIn={}));var AdvertisementSortBy;(function(e){e.Clicks="clicks",e.CreatedAt="createdAt",e.UpdatedAt="updatedAt"})(AdvertisementSortBy||(AdvertisementSortBy={}));var AdvertisementStatus;(function(e){e.Draft="draft",e.Publish="publish"})(AdvertisementStatus||(AdvertisementStatus={}));var AdvertisementType;(function(e){e.DesktopBannerXs="desktopBannerXS",e.FreeStyle="freeStyle",e.LargeRectangle="largeRectangle",e.Leaderboard="leaderboard",e.LeaderboardMd="leaderboardMD",e.MediumRectangle="mediumRectangle",e.MobileBanner="mobileBanner",e.MobileBannerMd="mobileBannerMD",e.MobileSquare="mobileSquare",e.MobileSquareSm="mobileSquareSM",e.Sidebar="sidebar",e.SidebarXl="sidebarXL"})(AdvertisementType||(AdvertisementType={}));var ApproveOrRejectPostStatus;(function(e){e.Approved="approved",e.Rejected="rejected"})(ApproveOrRejectPostStatus||(ApproveOrRejectPostStatus={}));var AttachmentType;(function(e){e.File="file",e.Image="image",e.Video="video"})(AttachmentType||(AttachmentType={}));var CacheControlScope;(function(e){e.Private="PRIVATE",e.Public="PUBLIC"})(CacheControlScope||(CacheControlScope={}));var CheckoutProduct;(function(e){e.IeltsCorrection="IeltsCorrection",e.MockTest="MockTest",e.Post="Post",e.Product="Product",e.VideoCourse="VideoCourse"})(CheckoutProduct||(CheckoutProduct={}));var CommentType;(function(e){e.Post="Post",e.PracticeTestGroup="PracticeTestGroup",e.QaPost="QAPost"})(CommentType||(CommentType={}));var CommonStatusEnum;(function(e){e.Approved="approved",e.Draft="draft",e.PendingApproval="pendingApproval",e.Published="published",e.Rejected="rejected"})(CommonStatusEnum||(CommonStatusEnum={}));var CommonTypeAccessEnum;(function(e){e.Paid="paid",e.Trial="trial"})(CommonTypeAccessEnum||(CommonTypeAccessEnum={}));var ConfigCourseFeeTypeEnum;(function(e){e.ForAll="forAll",e.ForMonth="forMonth",e.ForSubcat="forSubcat"})(ConfigCourseFeeTypeEnum||(ConfigCourseFeeTypeEnum={}));var ConversationGroup;(function(e){e.Customer="customer",e.Student="student",e.Zimians="zimians"})(ConversationGroup||(ConversationGroup={}));var ConversationStatus;(function(e){e.Done="done",e.Following="following",e.Open="open"})(ConversationStatus||(ConversationStatus={}));var CouponOrderBy;(function(e){e.CreatedAt="createdAt",e.UpdatedAt="updatedAt"})(CouponOrderBy||(CouponOrderBy={}));var CouponStatus;(function(e){e.Draft="draft",e.Publish="publish"})(CouponStatus||(CouponStatus={}));var CouponType;(function(e){e.Fixed="fixed",e.Percentage="percentage"})(CouponType||(CouponType={}));var CourseOrderByEnum;(function(e){e.Fee="fee",e.Id="id",e.ProgramType="program_type",e.StartDate="start_date",e.Status="status"})(CourseOrderByEnum||(CourseOrderByEnum={}));var CourseStatusEnum;(function(e){e.All="all",e.Closed="closed",e.Incoming="incoming",e.Ongoing="ongoing"})(CourseStatusEnum||(CourseStatusEnum={}));var CrawlReviewType;(function(e){e.MockTest="MockTest",e.Other="Other"})(CrawlReviewType||(CrawlReviewType={}));var DateViewMode;(function(e){e.Daily="daily",e.Monthly="monthly",e.Yearly="yearly"})(DateViewMode||(DateViewMode={}));var EmployeeInfoSort;(function(e){e.CreatedAt="createdAt",e.Priority="priority",e.UpdatedAt="updatedAt"})(EmployeeInfoSort||(EmployeeInfoSort={}));var EnumBaseStatus;(function(e){e.Active="active",e.Deactivated="deactivated"})(EnumBaseStatus||(EnumBaseStatus={}));var EnumCertificateRefType;(function(e){e.MockTestOrder="MockTestOrder"})(EnumCertificateRefType||(EnumCertificateRefType={}));var EnumConfigFunctionType$1;(function(e){e.Function="function"})(EnumConfigFunctionType$1||(EnumConfigFunctionType$1={}));var EnumConfigSystemNames;(function(e){e.PercentageForFeeByMonth="PercentageForFeeByMonth"})(EnumConfigSystemNames||(EnumConfigSystemNames={}));var EnumCouponProductType;(function(e){e.Course="course",e.CoursePlan="coursePlan",e.Program="program"})(EnumCouponProductType||(EnumCouponProductType={}));var EnumCouponProductTypes;(function(e){e.Course="course",e.CoursePlan="coursePlan",e.Program="program"})(EnumCouponProductTypes||(EnumCouponProductTypes={}));var EnumCouponStatusTypes;(function(e){e.Active="active",e.Deactivated="deactivated"})(EnumCouponStatusTypes||(EnumCouponStatusTypes={}));var EnumCouponTypes;(function(e){e.Fixed="fixed",e.Percentage="percentage"})(EnumCouponTypes||(EnumCouponTypes={}));var EnumCoursePlanerTypeEnum;(function(e){e.Course="Course",e.CoursePlan="CoursePlan"})(EnumCoursePlanerTypeEnum||(EnumCoursePlanerTypeEnum={}));var EnumCourseTypeEnum;(function(e){e.All="All",e.ClassRoom="ClassRoom",e.Online="Online"})(EnumCourseTypeEnum||(EnumCourseTypeEnum={}));var EnumCustomerLearningStatus;(function(e){e.All="all",e.Done="done",e.Registed="registed",e.Unregistration="unregistration"})(EnumCustomerLearningStatus||(EnumCustomerLearningStatus={}));var EnumDayOfWeek;(function(e){e.Fri="Fri",e.Mon="Mon",e.Sat="Sat",e.Sun="Sun",e.Thu="Thu",e.Tue="Tue",e.Wed="Wed"})(EnumDayOfWeek||(EnumDayOfWeek={}));var EnumHistoryChangeStudentSupporterStatusType;(function(e){e.Active="active",e.Deactivated="deactivated"})(EnumHistoryChangeStudentSupporterStatusType||(EnumHistoryChangeStudentSupporterStatusType={}));var EnumInvoiceRefundStatus;(function(e){e.All="All",e.Accepted="accepted",e.Confirmed="confirmed",e.Pending="pending",e.Rejected="rejected"})(EnumInvoiceRefundStatus||(EnumInvoiceRefundStatus={}));var EnumInvoiceStatus;(function(e){e.All="all",e.Paid="paid",e.Refund="refund"})(EnumInvoiceStatus||(EnumInvoiceStatus={}));var EnumLevelStatus;(function(e){e.Active="active",e.Deactivated="deactivated",e.Deleted="deleted",e.Inactive="inactive"})(EnumLevelStatus||(EnumLevelStatus={}));var EnumMockTestStatus;(function(e){e.All="all",e.Finished="finished",e.Incoming="incoming",e.Marked="marked",e.Ongoing="ongoing"})(EnumMockTestStatus||(EnumMockTestStatus={}));var EnumMockTestSubjectType;(function(e){e.Listening="Listening",e.Reading="Reading",e.Speaking="Speaking",e.Writing="Writing"})(EnumMockTestSubjectType||(EnumMockTestSubjectType={}));var EnumMockTestType;(function(e){e.Offline="offline",e.Online="online"})(EnumMockTestType||(EnumMockTestType={}));var EnumModule;(function(e){e.Academic="Academic",e.General="General"})(EnumModule||(EnumModule={}));var EnumOrder;(function(e){e.Asc="asc",e.Desc="desc"})(EnumOrder||(EnumOrder={}));var EnumPaymentType;(function(e){e.Atm="atm",e.Banking="banking",e.Cost="cost",e.Online="online",e.Other="other"})(EnumPaymentType||(EnumPaymentType={}));var EnumProcessRequestType;(function(e){e.Accept="accept",e.Reject="reject"})(EnumProcessRequestType||(EnumProcessRequestType={}));var EnumProvider;(function(e){e.Facebook="facebook",e.Google="google"})(EnumProvider||(EnumProvider={}));var EnumRoleType;(function(e){e.Am="AM",e.Admin="Admin",e.Bm="BM",e.Bp="BP",e.Cc="CC",e.Cs="CS",e.Ec="EC",e.Editor="Editor",e.Gv="GV",e.Guest="Guest",e.Hv="HV",e.HVu="HVu",e.Kt="KT",e.Kth="KTH",e.Mk="MK",e.RD="R_D"})(EnumRoleType||(EnumRoleType={}));var EnumShiftsType$1;(function(e){e.Instructor="INSTRUCTOR",e.MockTestOffline="MOCK_TEST_OFFLINE",e.MockTestOnline="MOCK_TEST_ONLINE",e.Other="OTHER",e.PlacementTest="PLACEMENT_TEST",e.SelfStudyRoom="SELF_STUDY_ROOM",e.TeacherOff="TEACHER_OFF"})(EnumShiftsType$1||(EnumShiftsType$1={}));var EnumSkill;(function(e){e.Listening="Listening",e.Reading="Reading"})(EnumSkill||(EnumSkill={}));var EnumStatusAccount;(function(e){e.Active="active",e.Deactivated="deactivated",e.Notactive="notactive"})(EnumStatusAccount||(EnumStatusAccount={}));var EnumStatusGrapNet;(function(e){e.Active="active",e.Deactivated="deactivated"})(EnumStatusGrapNet||(EnumStatusGrapNet={}));var EnumStatusScheduleType;(function(e){e.Active="ACTIVE",e.All="ALL",e.Approval="APPROVAL",e.Cancel="CANCEL",e.Deactivated="DEACTIVATED",e.Reject="REJECT",e.Reopened="REOPENED",e.RequestedCancel="REQUESTED_CANCEL"})(EnumStatusScheduleType||(EnumStatusScheduleType={}));var EnumTypeOfEducation;(function(e){e.Academic="Academic",e.General="General"})(EnumTypeOfEducation||(EnumTypeOfEducation={}));var EnumTypeSentEmail;(function(e){e.All="all",e.One="one"})(EnumTypeSentEmail||(EnumTypeSentEmail={}));var ExistFieldCheck;(function(e){e.Email="email",e.Phone="phone",e.Username="username"})(ExistFieldCheck||(ExistFieldCheck={}));var FacebookPostOrderBy;(function(e){e.CreatedAt="createdAt",e.InteractiveScore="interactiveScore",e.NumOfComments="numOfComments",e.NumOfLikes="numOfLikes",e.NumOfShares="numOfShares"})(FacebookPostOrderBy||(FacebookPostOrderBy={}));var ForumRoleEnum;(function(e){e.Admin="admin",e.Manager="manager",e.Mod="mod"})(ForumRoleEnum||(ForumRoleEnum={}));var IeltsPracticeTestGroupOrderBy;(function(e){e.CreatedAt="createdAt",e.Title="title",e.UpdatedAt="updatedAt"})(IeltsPracticeTestGroupOrderBy||(IeltsPracticeTestGroupOrderBy={}));var IeltsPracticeTestOrderBy;(function(e){e.CreatedAt="createdAt",e.Title="title",e.UpdatedAt="updatedAt"})(IeltsPracticeTestOrderBy||(IeltsPracticeTestOrderBy={}));var IeltsBcCoupons;(function(e){e.CouponsBook="COUPONS_BOOK",e.CouponsIeltsCorrect="COUPONS_IELTS_CORRECT",e.CouponsMocktest="COUPONS_MOCKTEST"})(IeltsBcCoupons||(IeltsBcCoupons={}));var IeltsPracticeQuestionType;(function(e){e.GapsFilling="gapsFilling",e.IdentifyInformation="identifyInformation",e.MapLabelling="mapLabelling",e.Matching="matching",e.MultipleChoice="multipleChoice",e.MultipleQuestions="multipleQuestions",e.SingleChoice="singleChoice",e.Speaking="speaking",e.Writing="writing"})(IeltsPracticeQuestionType||(IeltsPracticeQuestionType={}));var IeltsPracticeTestFormat;(function(e){e.Exercise="Exercise",e.MockTest="MockTest",e.PracticeTest="PracticeTest"})(IeltsPracticeTestFormat||(IeltsPracticeTestFormat={}));var IeltsPracticeTestGroupStatus;(function(e){e.Draft="draft",e.Publish="publish"})(IeltsPracticeTestGroupStatus||(IeltsPracticeTestGroupStatus={}));var IeltsPracticeTestLevel;(function(e){e.Academic="Academic",e.Exercise="Exercise",e.General="General"})(IeltsPracticeTestLevel||(IeltsPracticeTestLevel={}));var IeltsPracticeTestSessionRefKey;(function(e){e.MockTest="MockTest"})(IeltsPracticeTestSessionRefKey||(IeltsPracticeTestSessionRefKey={}));var IeltsPracticeTestStatus;(function(e){e.Draft="draft",e.Publish="publish"})(IeltsPracticeTestStatus||(IeltsPracticeTestStatus={}));var IeltsPracticeTestType;(function(e){e.Listening="Listening",e.Other="Other",e.Reading="Reading",e.Speaking="Speaking",e.Writing="Writing"})(IeltsPracticeTestType||(IeltsPracticeTestType={}));var InPostPosition;(function(e){e.After="after",e.Before="before"})(InPostPosition||(InPostPosition={}));var InPostTag;(function(e){e.Div="div",e.H1="h1",e.H2="h2",e.H3="h3",e.H4="h4",e.H5="h5",e.H6="h6",e.P="p",e.Span="span"})(InPostTag||(InPostTag={}));var JobStatus;(function(e){e.Crawling="crawling",e.Pending="pending",e.Waiting="waiting"})(JobStatus||(JobStatus={}));var JobType;(function(e){e.Group="group",e.Page="page"})(JobType||(JobType={}));var MediaEnumType;(function(e){e.File="file",e.Image="image",e.Video="video"})(MediaEnumType||(MediaEnumType={}));var MediaSortBy;(function(e){e.CreatedAt="createdAt",e.UpdatedAt="updatedAt"})(MediaSortBy||(MediaSortBy={}));var MediaType;(function(e){e.File="file",e.Image="image",e.Video="video"})(MediaType||(MediaType={}));var MediaVisibility;(function(e){e.Private="private",e.Public="public"})(MediaVisibility||(MediaVisibility={}));var MessageType;(function(e){e.Call="call",e.Response="response",e.Update="update"})(MessageType||(MessageType={}));var MiniGameStatusEnum;(function(e){e.Draft="draft",e.Published="published"})(MiniGameStatusEnum||(MiniGameStatusEnum={}));var MockTestOrderBy;(function(e){e.CreateDate="create_date",e.Id="id",e.UpdateDate="update_date"})(MockTestOrderBy||(MockTestOrderBy={}));var MockTestStatusEnum;(function(e){e.All="all",e.Cancelled="cancelled",e.Confirmed="confirmed",e.Reserved="reserved"})(MockTestStatusEnum||(MockTestStatusEnum={}));var NotificationType;(function(e){e.CommentAdded="commentAdded",e.CommentReplied="commentReplied",e.NewPost="newPost",e.PostApproved="postApproved",e.PostPublished="postPublished",e.PostRejected="postRejected",e.PostRequestApproval="postRequestApproval"})(NotificationType||(NotificationType={}));var OrderActionInput;(function(e){e.Cancel="cancel",e.Confirm="confirm"})(OrderActionInput||(OrderActionInput={}));var OrderDirection;(function(e){e.Asc="asc",e.Desc="desc"})(OrderDirection||(OrderDirection={}));var OrderOrderBy;(function(e){e.CreatedAt="createdAt",e.OrderId="orderId",e.UpdatedAt="updatedAt"})(OrderOrderBy||(OrderOrderBy={}));var OrderReportByProductSort;(function(e){e.PurchaseCount="purchaseCount",e.Total="total"})(OrderReportByProductSort||(OrderReportByProductSort={}));var OrderStatus;(function(e){e.Cancelled="cancelled",e.Confirmed="confirmed",e.Draft="draft",e.Dropped="dropped",e.Finished="finished",e.OnShipping="onShipping",e.Pending="pending",e.PendingShipping="pendingShipping",e.Picked="picked",e.PreparingFiles="preparingFiles"})(OrderStatus||(OrderStatus={}));var PageIndexStatus;(function(e){e.Deleted="DELETED",e.Updated="UPDATED"})(PageIndexStatus||(PageIndexStatus={}));var PaymentMethod;(function(e){e.AtStore="atStore",e.BankTransfer="bankTransfer",e.Cod="cod",e.Momo="momo",e.Onepay="onepay"})(PaymentMethod||(PaymentMethod={}));var PaymentStatus;(function(e){e.Paid="paid",e.Pending="pending",e.Refund="refund"})(PaymentStatus||(PaymentStatus={}));var PaymentStatusEnum;(function(e){e.Cancelled="cancelled",e.Confirmed="confirmed",e.Pending="pending"})(PaymentStatusEnum||(PaymentStatusEnum={}));var PostByUserSort;(function(e){e.PostCount="postCount",e.ReviewsAverage="reviewsAverage",e.ViewCount="viewCount"})(PostByUserSort||(PostByUserSort={}));var PostCategoryOrderBy;(function(e){e.CreatedAt="createdAt",e.Usage="usage"})(PostCategoryOrderBy||(PostCategoryOrderBy={}));var PostGroupOrderBy;(function(e){e.CreatedAt="createdAt",e.Title="title",e.UpdatedAt="updatedAt"})(PostGroupOrderBy||(PostGroupOrderBy={}));var PostOrderBy;(function(e){e.CreatedAt="createdAt",e.NumOfComments="numOfComments",e.NumOfContextualLink="numOfContextualLink",e.PublishedAt="publishedAt",e.Title="title",e.UpdatedAt="updatedAt",e.Views="views"})(PostOrderBy||(PostOrderBy={}));var PostStatus;(function(e){e.Approved="approved",e.Deleted="deleted",e.Draft="draft",e.PendingApproval="pendingApproval",e.Published="published",e.Rejected="rejected"})(PostStatus||(PostStatus={}));var ProductAttributeOrderBy;(function(e){e.CreatedAt="createdAt",e.Name="name",e.UpdatedAt="updatedAt",e.Usage="usage"})(ProductAttributeOrderBy||(ProductAttributeOrderBy={}));var ProductAttributeType;(function(e){e.Media="media",e.Number="number",e.Select="select",e.String="string"})(ProductAttributeType||(ProductAttributeType={}));var ProductOrderBy;(function(e){e.CreatedAt="createdAt",e.Name="name",e.NumOfContextualLink="numOfContextualLink",e.Price="price",e.UpdatedAt="updatedAt"})(ProductOrderBy||(ProductOrderBy={}));var ProductStatus;(function(e){e.Deleted="deleted",e.Draft="draft",e.Publish="publish"})(ProductStatus||(ProductStatus={}));var ProductType;(function(e){e.SimpleProduct="SimpleProduct",e.VariableProduct="VariableProduct"})(ProductType||(ProductType={}));var QaPostType;(function(e){e.Question="QUESTION",e.Voting="VOTING"})(QaPostType||(QaPostType={}));var RawDataStatusEnum;(function(e){e.All="all",e.Appoiment="appoiment",e.Arrived="arrived",e.Cancelled="cancelled",e.ChangeEc="changeEc",e.Following="following",e.Pending="pending",e.Registered="registered"})(RawDataStatusEnum||(RawDataStatusEnum={}));var RecipientType;(function(e){e.Conversation="conversation",e.Support="support",e.User="user"})(RecipientType||(RecipientType={}));var RecruitmentPostReplySort;(function(e){e.CreatedAt="createdAt",e.UpdatedAt="updatedAt"})(RecruitmentPostReplySort||(RecruitmentPostReplySort={}));var RecruitmentPostReplyStatus;(function(e){e.Pending="pending",e.Processed="processed"})(RecruitmentPostReplyStatus||(RecruitmentPostReplyStatus={}));var RecruitmentPostSalaryType;(function(e){e.Hidden="hidden",e.Public="public"})(RecruitmentPostSalaryType||(RecruitmentPostSalaryType={}));var RecruitmentPostSort;(function(e){e.CreatedAt="createdAt",e.UpdatedAt="updatedAt"})(RecruitmentPostSort||(RecruitmentPostSort={}));var RecruitmentPostStatus;(function(e){e.Draft="draft",e.Publish="publish"})(RecruitmentPostStatus||(RecruitmentPostStatus={}));var RequestStudentToCourseStatusEnumeration;(function(e){e.Accepted="accepted",e.Pending="pending",e.Rejected="rejected"})(RequestStudentToCourseStatusEnumeration||(RequestStudentToCourseStatusEnumeration={}));var RequestStudentToCourseTypeEnumeration;(function(e){e.JoinCourse="joinCourse",e.LeftCourse="leftCourse",e.ReEnroll="reEnroll"})(RequestStudentToCourseTypeEnumeration||(RequestStudentToCourseTypeEnumeration={}));var ReviewOrderBy;(function(e){e.CreatedAt="createdAt",e.Rating="rating"})(ReviewOrderBy||(ReviewOrderBy={}));var ReviewType;(function(e){e.Level="Level",e.MockTest="MockTest",e.Other="Other",e.Post="Post",e.Product="Product",e.Writing="Writing"})(ReviewType||(ReviewType={}));var SenderAction;(function(e){e.MarkSeen="markSeen",e.TypingOff="typingOff",e.TypingOn="typingOn"})(SenderAction||(SenderAction={}));var ShippingMethod;(function(e){e.Delivery="delivery",e.StorePickup="storePickup",e.Virtual="virtual"})(ShippingMethod||(ShippingMethod={}));var SlugType;(function(e){e.IeltsPracticeTestGroup="IeltsPracticeTestGroup",e.Post="Post",e.PostGroup="PostGroup",e.Product="Product",e.RecruitmentPost="RecruitmentPost",e.Tag="Tag"})(SlugType||(SlugType={}));var SourceEnum;(function(e){e.App="App",e.Zim="Zim",e.ZimMobile="ZimMobile"})(SourceEnum||(SourceEnum={}));var Status;(function(e){e.Active="active",e.All="all",e.Deactivated="deactivated",e.Inactive="inactive"})(Status||(Status={}));var StatusAccount;(function(e){e.Active="active",e.Deactivated="deactivated",e.Notactive="notactive"})(StatusAccount||(StatusAccount={}));var StatusEnum;(function(e){e.Active="active",e.Deactivated="deactivated",e.Inactive="inactive"})(StatusEnum||(StatusEnum={}));var StatusExerciseGrapNet;(function(e){e.Active="active",e.Draft="draft"})(StatusExerciseGrapNet||(StatusExerciseGrapNet={}));var StockStatus;(function(e){e.InStock="inStock",e.OutOfStock="outOfStock"})(StockStatus||(StockStatus={}));var SubfolderS3Enum;(function(e){e.Exercise="Exercise",e.ExerciseAnswer="ExerciseAnswer",e.ExerciseLearningMaterials="ExerciseLearningMaterials",e.ExerciseQuestion="ExerciseQuestion"})(SubfolderS3Enum||(SubfolderS3Enum={}));var SurveyQuestionEnum;(function(e){e.MultipleChoice="multipleChoice",e.SingleChoice="singleChoice",e.Text="text"})(SurveyQuestionEnum||(SurveyQuestionEnum={}));var TagAssignJobStatus;(function(e){e.Finished="finished",e.Pending="pending",e.Scanning="scanning"})(TagAssignJobStatus||(TagAssignJobStatus={}));var TagObject;(function(e){e.Post="Post",e.Product="Product"})(TagObject||(TagObject={}));var TagSort;(function(e){e.CreatedAt="createdAt",e.Name="name",e.PostUsage="postUsage",e.ProductUsage="productUsage",e.UpdatedAt="updatedAt",e.Usage="usage"})(TagSort||(TagSort={}));var TicketFilterTypeEnum;(function(e){e.Received="received",e.Send="send"})(TicketFilterTypeEnum||(TicketFilterTypeEnum={}));var TicketReactionTypeEnum;(function(e){e.Love="Love",e.NotOk="NotOk",e.Ok="Ok",e.Report="Report"})(TicketReactionTypeEnum||(TicketReactionTypeEnum={}));var TicketStatusTypeEnum;(function(e){e.Closed="Closed",e.Deleted="Deleted",e.Processing="Processing",e.Waiting="Waiting"})(TicketStatusTypeEnum||(TicketStatusTypeEnum={}));var TodoScheduleTypeEnum;(function(e){e.Apointment="Apointment",e.FinalTest="FinalTest",e.ToDo="ToDo"})(TodoScheduleTypeEnum||(TodoScheduleTypeEnum={}));var ToeicMockTestStatusEnumType;(function(e){e.Draft="draft",e.Published="published"})(ToeicMockTestStatusEnumType||(ToeicMockTestStatusEnumType={}));var ToeicModuleEnumType;(function(e){e.Listening="listening",e.Reading="reading"})(ToeicModuleEnumType||(ToeicModuleEnumType={}));var VideoCourseQuestionEnum;(function(e){e.FillInTheBlank="fillInTheBlank",e.MultipleChoice="multipleChoice",e.SingleChoice="singleChoice"})(VideoCourseQuestionEnum||(VideoCourseQuestionEnum={}));const EMPTY_ERROR=e=>`${e} is required.`,EMPTY_GUID="00000000-0000-0000-0000-000000000000",DATE_OF_WEEKS={0:{short:"CN",long:"CN",full:"Ch\u1EE7 nh\u1EADt"},1:{short:"2",long:"T2",full:"Th\u1EE9 2"},2:{short:"3",long:"T3",full:"Th\u1EE9 3"},3:{short:"4",long:"T4",full:"Th\u1EE9 4"},4:{short:"5",long:"T5",full:"Th\u1EE9 5"},5:{short:"6",long:"T6",full:"Th\u1EE9 6"},6:{short:"7",long:"T7",full:"Th\u1EE9 7"}},SKILLS={LISTENING:{short:"L",long:"Listening"},READING:{short:"R",long:"Reading"},SPEAKING:{short:"S",long:"Speaking"},WRITING:{short:"W",long:"Writing"},"WRITING TASK 1":{short:"WT1",long:"Writing Task 1"},"WRITING TASK 2":{short:"WT2",long:"Writing Task 2"},"BASIC GRAMMAR":{short:"BG",long:"Basic Grammar"},"FINAL TEST":{short:"FT",long:"Final Test"}},COURSE_STATUSES={ALL:"all",INCOMING:"incoming",ONGOING:"ongoing",CLOSED:"closed"},OPEN_STATUSES={ALL:{label:"All",colorScheme:"gray"},INCOMING:{label:"Incoming",colorScheme:"green"},ONGOING:{label:"Ongoing",colorScheme:"orange"},FINISHED:{label:"Finished",colorScheme:"red"}},courseStatusOptions=[{label:"Incoming",value:COURSE_STATUSES.INCOMING},{label:"Ongoing",value:COURSE_STATUSES.ONGOING},{label:"Closed",value:COURSE_STATUSES.CLOSED}];var ETicketStatus$1;(function(e){e.PROCESSING="Processing",e.CLOSED="Closed",e.WAITING="Waiting",e.DELETED="Deleted",e.LONGTIME="Longtime"})(ETicketStatus$1||(ETicketStatus$1={}));var ETicketType;(function(e){e.send="send",e.received="received"})(ETicketType||(ETicketType={}));var UserStatusEnum;(function(e){e.active="active",e.deactivated="deactivated"})(UserStatusEnum||(UserStatusEnum={}));const TICKET_STATUS={All:{label:"All",value:"All",colorScheme:"gray"},Processing:{label:"Processing",value:"Processing",colorScheme:"green"},Waiting:{label:"Waiting",value:"Waiting",colorScheme:"orange"},Closed:{label:"Closed",value:"Closed",colorScheme:"red"},Deleted:{label:"Deleted",value:"Deleted",colorScheme:"black"}},TICKET_STATUS_LOWERCASE={all:{label:"All",value:"All",colorScheme:"gray"},processing:{label:"Processing",value:"Processing",colorScheme:"green"},waiting:{label:"Waiting",value:"Waiting",colorScheme:"orange"},closed:{label:"Closed",value:"Closed",colorScheme:"red"},deleted:{label:"Deleted",value:"Deleted",colorScheme:"black"},longtime:{label:"Longtime",value:"Longtime",colorScheme:"gray"}},LEAD_STATUS={0:{label:"Ch\u01B0a x\u1EED l\xFD",value:0,colorScheme:"red",filterValue:"pending"},1:{label:"\u0110ang theo d\xF5i",value:1,colorScheme:"orange",filterValue:"following"},2:{label:"\u0110\xE3 h\u1EB9n test",value:2,colorScheme:"blue",filterValue:"appoiment"},4:{label:"\u0110\xE3 \u0111\u1EBFn test",value:4,colorScheme:"teal",filterValue:"called"},6:{label:"\u0110\xE3 \u0111\u0103ng k\xFD",value:6,colorScheme:"green",filterValue:"registered"},3:{label:"\u0110\xE3 h\u1EE7y",value:3,colorScheme:"gray",filterValue:"cancelled"},5:{label:"\u0110\xE3 \u0111\u1ED5i EC",value:5,colorScheme:"purple",filterValue:"changeEc"}},ticketStatusOptions=[{label:"Processing",value:ETicketStatus$1.PROCESSING},{label:"Waiting",value:ETicketStatus$1.WAITING},{label:"Closed",value:ETicketStatus$1.CLOSED},{label:"Deleted",value:ETicketStatus$1.DELETED}],ticketFilterTypeOptions=[{label:"\u0110\xE3 g\u1EEDi",value:ETicketType.send},{label:"\u0110\xE3 nh\u1EADn",value:ETicketType.received}],userStatusOptions=[{label:"Active",value:UserStatusEnum.active},{label:"Deactivated",value:UserStatusEnum.deactivated}];var EnumShiftsType;(function(e){e.Other="Other",e.TeacherOff="TeacherOff",e.MOCK_TEST_ONLINE="MOCK_TEST_ONLINE",e.MOCK_TEST_OFFLINE="MOCK_TEST_OFFLINE",e.OTHER="OTHER",e.TEACHER_OFF="TEACHER_OFF"})(EnumShiftsType||(EnumShiftsType={}));const DEFAULT_AVATAR_URL="https://app.zim.vn/app-assets/zimv2/images/default-logo1.png",MOCK_TEST_STATUS={inactive:{label:"inactive",value:"inactive",colorScheme:"gray"},active:{label:"active",value:"active",colorScheme:"green"},deactivated:{label:"deactivated",value:"deactivated",colorScheme:"red"}},MockTestStatusOptions=[{label:"Active",value:"active"},{label:"Deactivated",value:"deactivated"}],MockTestSubjectsType={"Listening-Reading-Writing":{long:"Listening-Reading-Writing",short:"L-R-W"},Speaking:{long:"Speaking",short:"Speaking"}};var EOrderStatus;(function(e){e.confirmed="confirmed",e.cancel="cancel",e.pending="pending"})(EOrderStatus||(EOrderStatus={}));EOrderStatus.confirmed,EOrderStatus.cancel,EOrderStatus.pending;EOrderStatus.confirmed,EOrderStatus.cancel,EOrderStatus.pending;const ExerciseStatus={active:{label:"Active",value:StatusExerciseGrapNet.Active,colorScheme:"green"},draft:{label:"Draft",value:StatusExerciseGrapNet.Draft,colorScheme:"gray"}},ExerciseOptions=[{label:ExerciseStatus.draft.label,value:StatusExerciseGrapNet.Draft},{label:ExerciseStatus.active.label,value:StatusExerciseGrapNet.Active}],tableDisplayRowsOptions=[{label:"5 rows",value:5},{label:"10 rows",value:10},{label:"15 rows",value:15},{label:"20 rows",value:20},{label:"25 rows",value:25},{label:"30 rows",value:30}],MyAvatar=react.exports.memo(a=>{var l=a,{src:e="",name:r=""}=l,n=Ae(l,["src","name"]);return jsx(Avatar,B({src:e===DEFAULT_AVATAR_URL?"":e,name:r,zIndex:10},n))}),MyInfoSection=({searchString:e,setSearchString:r})=>{const[n]=useUser();return jsxs(VStack,{w:"100%",p:{base:3},position:"sticky",top:0,zIndex:50,bg:useColorModeValue("white","#10172a"),alignItems:"stretch",children:[jsxs(Box,{w:"100%",display:"flex",alignItems:"start",maxW:{base:"full"},className:" space-x-2.5",bg:useColorModeValue("white",""),p:{base:2,md:0},rounded:"lg",children:[jsx(MyAvatar,{boxSize:"2em",src:n==null?void 0:n.avatar,name:n==null?void 0:n.fullName}),jsxs(Box,{flex:1,children:[jsx(Text,{noOfLines:1,fontSize:14,lineHeight:"18px",fontWeight:"medium",color:useColorModeValue("gray.800","slate.300"),children:n==null?void 0:n.fullName}),jsx(Text,{display:{base:"block"},noOfLines:1,fontSize:14,fontWeight:500,lineHeight:"24px",color:"gray.400",children:n==null?void 0:n.role})]})]}),jsx(HStack,{children:jsx(SearchBox,{searchString:e,setSearchString:r})})]})},AvatarComponent$1=t.memo(function({avatarUrl:r,visibleLastOnline:n,lastOnline:a,username:l}){return jsx(MyAvatar,{zIndex:"10",boxSize:"2em",src:r,name:l,children:n&&jsx(AvatarBadge,{boxSize:"15px",bg:a==="online"?"green.500":"gray.300"})})}),UserItemInfo=react.exports.memo(e=>{const T=e,{onClick:r,avatarUrl:n,username:a,usernameClassName:l,positionName:c,lastOnline:u,visibleLastOnline:d=!0,isOwner:m,channel:E,id:C}=T,_=Ae(T,["onClick","avatarUrl","username","usernameClassName","positionName","lastOnline","visibleLastOnline","isOwner","channel","id"]),A=react.exports.useCallback(()=>{r(C,E)},[E,C]);return jsxs(Box,H(B({w:"100%",display:"flex",alignItems:"start",cursor:"pointer",onClick:A,maxW:{base:"full"},className:" space-x-2.5",bg:useColorModeValue("white",""),p:3,rounded:"lg"},_),{children:[jsx(AvatarComponent$1,{avatarUrl:n,lastOnline:u,visibleLastOnline:d,username:a}),jsxs(Box,{flex:1,children:[jsxs(HStack,{children:[jsx(Text,{noOfLines:1,fontSize:14,lineHeight:"18px",fontWeight:"medium",color:useColorModeValue("gray.800","slate.300"),className:`${l}`,children:a}),m&&jsx(FcKey,{})]}),jsx(Text,{noOfLines:1,fontSize:12,lineHeight:"24px",color:"gray.400",children:c})]})]}))}),ConversationLoading=()=>jsxs(HStack,{px:3,py:2,borderBottomWidth:1,bg:useColorModeValue("white","#1e293b50"),children:[jsx(SkeletonCircle,{size:"10"}),jsx(SkeletonText,{flex:1,noOfLines:2,spacing:"2",skeletonHeight:5})]}),ChannelItem=react.exports.memo(({conversations:e,onClick:r,isLoading:n,canLoadMore:a,onLoadMore:l,loading:c})=>{const u=react.exports.useMemo(()=>e==null?void 0:e.map((d,m)=>{const E=d==null?void 0:d.user,C=(E==null?void 0:E.role_id)===5?"student":(E==null?void 0:E.role_id)===14?"customer":"zimians";return c?jsx(ConversationLoading,{},`loading_${m}`):react.exports.createElement(UserItemInfo,H(B({},E),{lastOnline:E==null?void 0:E.status_online,avatarUrl:E==null?void 0:E.avatar,username:E==null?void 0:E.full_name,positionName:E==null?void 0:E.role_name,key:m,onClick:r,channel:C}))}),[e]);return jsxs(VStack,{alignItems:"stretch",children:[u,a&&jsx(Button$1,{isLoading:n,loadingText:"\u0110ang t\u1EA3i th\xEAm",onClick:l,w:"100%",rounded:"none",borderWidth:"1px 0px 0px 0px",bg:useColorModeValue("white","slate.600"),fontSize:14,fontWeight:400,justifyContent:"center",children:"Xem th\xEAm"})]})}),AccordionContent=({title:e,onClick:r,children:n,overallMessageUnRead:a})=>jsxs(AccordionItem,{borderBottom:1,borderColor:"",children:[jsxs(HStack,{position:"relative",spacing:0,pl:4,_hover:{backgroundColor:useColorModeValue("#f0f0f0","slate.600")},children:[jsx(Box,{onClick:r,display:"flex",justifyContent:"start",flex:1,as:"button",children:jsx(Text,{color:useColorModeValue("#444","white"),fontSize:16,fontWeight:"medium",children:e})}),jsx(AccordionButton,{display:"flex",alignItems:"center",justifyContent:"center",w:10,children:jsx(AccordionIcon,{})}),a&&jsx(NotiCount,{count:a})]}),jsx(AccordionPanel,{p:0,children:n})]}),MyAccordion=react.exports.memo(({title:e="Accordion",children:r,onClick:n,overallMessageUnRead:a})=>jsx(Accordion,{defaultIndex:[0],allowMultiple:!0,children:jsx(AccordionContent,{overallMessageUnRead:a,title:e,onClick:n,children:r})}));function mergeDeep(...e){const r=n=>n&&typeof n=="object";return e.reduce((n,a)=>(Object.keys(a).forEach(l=>{const c=n[l],u=a[l];Array.isArray(c)&&Array.isArray(u)?n[l]=uniqBy_1(c.concat(...u),"id"):r(c)&&r(u)?n[l]=mergeDeep(c,u):n[l]=u}),n),{})}function mergeDeepWithoutId(...e){const r=n=>n&&typeof n=="object";return e.reduce((n,a)=>(Object.keys(a).forEach(l=>{const c=n[l],u=a[l];Array.isArray(c)&&Array.isArray(u)?n[l]=uniqBy_1(c.concat(...u),"user"):r(c)&&r(u)?n[l]=mergeDeepWithoutId(c,u):n[l]=u}),n),{})}const useLoadMore=({hasNextPage:e,fetchMore:r,variables:n={},isNormalLoadMore:a=!0})=>{const[l,c]=react.exports.useState(!1),u=a?mergeDeep:mergeDeepWithoutId,d=react.exports.useCallback(async()=>{l||!e||(await c(!0),await r({variables:n,updateQuery:(m,{fetchMoreResult:E})=>E?u(m,E):m}),await c(!1))},[l,e,c,r,n]);return react.exports.useMemo(()=>({onLoadMore:d,isLoadingMore:l}),[d,l])},useGetListAccountChat=({roles:e,searchString:r})=>{var _,A,T;const n=pickBy_1({roles:e,searchString:r,limit:10,page:1},S=>S),{data:a,loading:l,fetchMore:c}=useQuery(GET_LIST_ACCOUNT_CHAT,{variables:n}),u=(_=a==null?void 0:a.getAccountChatPagination)==null?void 0:_.docs,d=(A=a==null?void 0:a.getAccountChatPagination)==null?void 0:A.page,m=(T=a==null?void 0:a.getAccountChatPagination)==null?void 0:T.hasNextPage,{onLoadMore:E,isLoadingMore:C}=useLoadMore({variables:H(B({},n),{page:d+1}),fetchMore:c,hasNextPage:m,isNormalLoadMore:!1});return react.exports.useMemo(()=>({listAccount:u,loading:l,onLoadMore:E,hasNextPage:m,isLoadingMore:C}),[u,l,E,m,C])},SEND_MESSAGE=gql`
  mutation sendMessage(
    $recipient: RecipientInput!
    $message: MessageInput
    $senderAction: SenderAction
  ) @api(name: "chat") {
    sendMessage(
      recipient: $recipient
      message: $message
      senderAction: $senderAction
    ) {
      id
      createdAt
      updatedAt
      lastMessage {
        id
        text
        from
        to
        type
        attachments {
          type
          attachmentId
          attachment {
            id
            path
            fullUrl
            type
          }
        }
        loading @client
        error @client
        seenBy
        createdAt
        updatedAt
        deletedAt
        callPayload
      }
    }
  }
`,REMOVE_MESSAGE=gql`
  mutation removeMessage($id: ObjectID!) @api(name: "chat") {
    removeMessage(id: $id) {
      id
      name
      createdAt
      updatedAt
      isDeleteMessage
      lastMessage {
        id
        text
        from
        to
        type
        attachments {
          type
          attachmentId
          attachment {
            id
            path
            fullUrl
            type
          }
        }
        loading @client
        error @client
        seenBy
        createdAt
        updatedAt
        deletedAt
        callPayload
      }
    }
  }
`,CREATE_CONVERSATION=gql`
  mutation createConversation(
    $name: String
    $group: String
    $userIds: [String!]!
    $isChannel: Boolean
  ) @api(name: "chat") {
    createConversation(
      name: $name
      group: $group
      userIds: $userIds
      isChannel: $isChannel
    ) {
      id
      name
    }
  }
`,UPDATE_CONVERSATION=gql`
  mutation updateConversation(
    $name: String
    $id: ObjectID!
    $userIds: [String!]!
  ) @api(name: "chat") {
    updateConversation(name: $name, id: $id, userIds: $userIds) {
      id
      name
    }
  }
`,CREATE_NOTE=gql`
  mutation takeNoteForConversation(
    $conversationId: ObjectID!
    $content: String!
  ) @api(name: "chat") {
    takeNoteForConversation(
      conversationId: $conversationId
      content: $content
    ) {
      id
      content
      conversationId
      createdAt
      createdBy
    }
  }
`,REMOVE_MEMBER=gql`
  mutation removeMemberFromParticipant($userId: String!, $id: ObjectID!)
  @api(name: "chat") {
    removeMemberFromParticipant(userId: $userId, id: $id) {
      id
    }
  }
`,CREATE_TAG_FOR_CONVERSATION=gql`
  mutation createTagForConversation(
    $tagId: ObjectID
    $conversationId: ObjectID
    $name: String
    $color: String
  ) @api(name: "chat") {
    createTagForConversation(
      tagId: $tagId
      conversationId: $conversationId
      name: $name
      color: $color
    ) {
      id
      tags {
        id
        name
        color
      }
    }
  }
`,MARK_DONE_CONVERSATION=gql`
  mutation markDoneConversation($conversationId: ObjectID) @api(name: "chat") {
    markDoneConversation(id: $conversationId) {
      id
    }
  }
`,DELETE_TAG_IN_CONVERSATION=gql`
  mutation deleteTagInConversation(
    $conversationId: ObjectID!
    $tagId: ObjectID!
  ) @api(name: "chat") {
    deleteTagInConversation(conversationId: $conversationId, tagId: $tagId) {
      id
      tags {
        id
      }
    }
  }
`,CREATE_QA_POST=gql`
  mutation createQAPost($input: QAPostInput!, $userId: String)
  @api(name: "zim") {
    createQAPost(input: $input, userId: $userId) {
      id
      title
      content
    }
  }
`,POST_COMMENT=gql`
  mutation postComment($content: String!, $type: CommentType, $ref: ObjectID!)
  @api(name: "zim") {
    postComment(content: $content, type: $type, ref: $ref) {
      ... on PostComment {
        id
      }
      ... on QAPostComment {
        id
      }
    }
  }
`,SEND_REACT_MESSAGE=gql`
  mutation sendReactionMessage($messageId: ObjectID, $emoji: String)
  @api(name: "chat") {
    sendReactionMessage(messageId: $messageId, emoji: $emoji) {
      id
      emojis {
        userId
        emoji
      }
    }
  }
`,useGetWindowDimensions=()=>{const{innerWidth:e,innerHeight:r}=window,[n,a]=react.exports.useState(e),[l,c]=react.exports.useState(r),u=react.exports.useCallback(()=>{a(window.innerWidth),c(window.innerHeight)},[]);return useEventListener("resize",u),{width:n,height:l}},NavigateButton=({item:e,onClick:r,isActive:n})=>jsx(Box,{id:"js-toggle-profile",onClick:r,w:10,h:10,p:1,rounded:"full",bg:useColorModeValue(n?"blue.400":"gray.100",n?"blue.400":"#1e293b"),overflow:"hidden",display:"flex",alignItems:"center",justifyContent:"center",color:n?"white":"black",children:e.icon}),HeaderMobileChat=()=>{const e=useStore(n=>n.setTab),r=useStore(n=>n.tab);return jsx(HStack,{w:"full",bg:useColorModeValue("white","#10172a"),p:2,justifyContent:"flex-end",alignItems:"center",borderBottomWidth:1,spacing:6,children:[{icon:jsx(FiGrid,{}),key:"channel",onClick:()=>e("channel")},{icon:jsx(RiChat3Line,{}),key:"conversation",onClick:()=>e("conversation")}].map((n,a)=>{const l=n.key===r;return jsx(NavigateButton,{isActive:l,item:n,onClick:n.onClick},a)})})},timeDifferenceCalculator=(e,r=new Date)=>{var m,E;const n=Math.abs(Math.max(Date.parse(r),Date.parse(e))-Math.min(Date.parse(e),Date.parse(r))),a=1e3,l=60*a,c=60*l,u=(m=Math.floor(n/c))!=null?m:0,d=(E=Math.floor(n%c/l).toLocaleString("en-US",{minimumIntegerDigits:1}))!=null?E:"0";return u&&u<24?`${u} gi\u1EDD tr\u01B0\u1EDBc.`:u===0?`${d} ph\xFAt tr\u01B0\u1EDBc.`:u>=24?u/24>31?dayjs(e).format("DD/MM/YYYY"):`${Math.floor(u/24)} ng\xE0y tr\u01B0\u1EDBc.`:"v\u1EEBa xong."};var lottie$1={exports:{}};(function(module,exports){typeof navigator!="undefined"&&function(e,r){module.exports=r()}(commonjsGlobal$1,function(){var svgNS="http://www.w3.org/2000/svg",locationHref="",_useWebWorker=!1,initialDefaultFrame=-999999,setWebWorker=function(r){_useWebWorker=!!r},getWebWorker=function(){return _useWebWorker},setLocationHref=function(r){locationHref=r},getLocationHref=function(){return locationHref};function createTag(e){return document.createElement(e)}function extendPrototype(e,r){var n,a=e.length,l;for(n=0;n<a;n+=1){l=e[n].prototype;for(var c in l)Object.prototype.hasOwnProperty.call(l,c)&&(r.prototype[c]=l[c])}}function getDescriptor(e,r){return Object.getOwnPropertyDescriptor(e,r)}function createProxyFunction(e){function r(){}return r.prototype=e,r}var audioControllerFactory=function(){function e(r){this.audios=[],this.audioFactory=r,this._volume=1,this._isMuted=!1}return e.prototype={addAudio:function(n){this.audios.push(n)},pause:function(){var n,a=this.audios.length;for(n=0;n<a;n+=1)this.audios[n].pause()},resume:function(){var n,a=this.audios.length;for(n=0;n<a;n+=1)this.audios[n].resume()},setRate:function(n){var a,l=this.audios.length;for(a=0;a<l;a+=1)this.audios[a].setRate(n)},createAudio:function(n){return this.audioFactory?this.audioFactory(n):window.Howl?new window.Howl({src:[n]}):{isPlaying:!1,play:function(){this.isPlaying=!0},seek:function(){this.isPlaying=!1},playing:function(){},rate:function(){},setVolume:function(){}}},setAudioFactory:function(n){this.audioFactory=n},setVolume:function(n){this._volume=n,this._updateVolume()},mute:function(){this._isMuted=!0,this._updateVolume()},unmute:function(){this._isMuted=!1,this._updateVolume()},getVolume:function(){return this._volume},_updateVolume:function(){var n,a=this.audios.length;for(n=0;n<a;n+=1)this.audios[n].volume(this._volume*(this._isMuted?0:1))}},function(){return new e}}(),createTypedArray=function(){function e(n,a){var l=0,c=[],u;switch(n){case"int16":case"uint8c":u=1;break;default:u=1.1;break}for(l=0;l<a;l+=1)c.push(u);return c}function r(n,a){return n==="float32"?new Float32Array(a):n==="int16"?new Int16Array(a):n==="uint8c"?new Uint8ClampedArray(a):e(n,a)}return typeof Uint8ClampedArray=="function"&&typeof Float32Array=="function"?r:e}();function createSizedArray(e){return Array.apply(null,{length:e})}function _typeof$6(e){return typeof Symbol=="function"&&typeof Symbol.iterator=="symbol"?_typeof$6=function(n){return typeof n}:_typeof$6=function(n){return n&&typeof Symbol=="function"&&n.constructor===Symbol&&n!==Symbol.prototype?"symbol":typeof n},_typeof$6(e)}var subframeEnabled=!0,expressionsPlugin=null,idPrefix="",isSafari=/^((?!chrome|android).)*safari/i.test(navigator.userAgent),bmPow=Math.pow,bmSqrt=Math.sqrt,bmFloor=Math.floor,bmMax=Math.max,bmMin=Math.min,BMMath={};(function(){var e=["abs","acos","acosh","asin","asinh","atan","atanh","atan2","ceil","cbrt","expm1","clz32","cos","cosh","exp","floor","fround","hypot","imul","log","log1p","log2","log10","max","min","pow","random","round","sign","sin","sinh","sqrt","tan","tanh","trunc","E","LN10","LN2","LOG10E","LOG2E","PI","SQRT1_2","SQRT2"],r,n=e.length;for(r=0;r<n;r+=1)BMMath[e[r]]=Math[e[r]]})(),BMMath.random=Math.random,BMMath.abs=function(e){var r=_typeof$6(e);if(r==="object"&&e.length){var n=createSizedArray(e.length),a,l=e.length;for(a=0;a<l;a+=1)n[a]=Math.abs(e[a]);return n}return Math.abs(e)};var defaultCurveSegments=150,degToRads=Math.PI/180,roundCorner=.5519;function styleDiv(e){e.style.position="absolute",e.style.top=0,e.style.left=0,e.style.display="block",e.style.transformOrigin="0 0",e.style.webkitTransformOrigin="0 0",e.style.backfaceVisibility="visible",e.style.webkitBackfaceVisibility="visible",e.style.transformStyle="preserve-3d",e.style.webkitTransformStyle="preserve-3d",e.style.mozTransformStyle="preserve-3d"}function BMEnterFrameEvent(e,r,n,a){this.type=e,this.currentTime=r,this.totalTime=n,this.direction=a<0?-1:1}function BMCompleteEvent(e,r){this.type=e,this.direction=r<0?-1:1}function BMCompleteLoopEvent(e,r,n,a){this.type=e,this.currentLoop=n,this.totalLoops=r,this.direction=a<0?-1:1}function BMSegmentStartEvent(e,r,n){this.type=e,this.firstFrame=r,this.totalFrames=n}function BMDestroyEvent(e,r){this.type=e,this.target=r}function BMRenderFrameErrorEvent(e,r){this.type="renderFrameError",this.nativeError=e,this.currentTime=r}function BMConfigErrorEvent(e){this.type="configError",this.nativeError=e}var createElementID=function(){var e=0;return function(){return e+=1,idPrefix+"__lottie_element_"+e}}();function HSVtoRGB(e,r,n){var a,l,c,u,d,m,E,C;switch(u=Math.floor(e*6),d=e*6-u,m=n*(1-r),E=n*(1-d*r),C=n*(1-(1-d)*r),u%6){case 0:a=n,l=C,c=m;break;case 1:a=E,l=n,c=m;break;case 2:a=m,l=n,c=C;break;case 3:a=m,l=E,c=n;break;case 4:a=C,l=m,c=n;break;case 5:a=n,l=m,c=E;break}return[a,l,c]}function RGBtoHSV(e,r,n){var a=Math.max(e,r,n),l=Math.min(e,r,n),c=a-l,u,d=a===0?0:c/a,m=a/255;switch(a){case l:u=0;break;case e:u=r-n+c*(r<n?6:0),u/=6*c;break;case r:u=n-e+c*2,u/=6*c;break;case n:u=e-r+c*4,u/=6*c;break}return[u,d,m]}function addSaturationToRGB(e,r){var n=RGBtoHSV(e[0]*255,e[1]*255,e[2]*255);return n[1]+=r,n[1]>1?n[1]=1:n[1]<=0&&(n[1]=0),HSVtoRGB(n[0],n[1],n[2])}function addBrightnessToRGB(e,r){var n=RGBtoHSV(e[0]*255,e[1]*255,e[2]*255);return n[2]+=r,n[2]>1?n[2]=1:n[2]<0&&(n[2]=0),HSVtoRGB(n[0],n[1],n[2])}function addHueToRGB(e,r){var n=RGBtoHSV(e[0]*255,e[1]*255,e[2]*255);return n[0]+=r/360,n[0]>1?n[0]-=1:n[0]<0&&(n[0]+=1),HSVtoRGB(n[0],n[1],n[2])}var rgbToHex=function(){var e=[],r,n;for(r=0;r<256;r+=1)n=r.toString(16),e[r]=n.length===1?"0"+n:n;return function(a,l,c){return a<0&&(a=0),l<0&&(l=0),c<0&&(c=0),"#"+e[a]+e[l]+e[c]}}(),setSubframeEnabled=function(r){subframeEnabled=!!r},getSubframeEnabled=function(){return subframeEnabled},setExpressionsPlugin=function(r){expressionsPlugin=r},getExpressionsPlugin=function(){return expressionsPlugin},setDefaultCurveSegments=function(r){defaultCurveSegments=r},getDefaultCurveSegments=function(){return defaultCurveSegments},setIdPrefix=function(r){idPrefix=r};function createNS(e){return document.createElementNS(svgNS,e)}function _typeof$5(e){return typeof Symbol=="function"&&typeof Symbol.iterator=="symbol"?_typeof$5=function(n){return typeof n}:_typeof$5=function(n){return n&&typeof Symbol=="function"&&n.constructor===Symbol&&n!==Symbol.prototype?"symbol":typeof n},_typeof$5(e)}var dataManager=function(){var e=1,r=[],n,a,l={onmessage:function(){},postMessage:function(T){n({data:T})}},c={postMessage:function(T){l.onmessage({data:T})}};function u(A){if(window.Worker&&window.Blob&&getWebWorker()){var T=new Blob(["var _workerSelf = self; self.onmessage = ",A.toString()],{type:"text/javascript"}),S=URL.createObjectURL(T);return new Worker(S)}return n=A,l}function d(){a||(a=u(function(T){function S(){function k(G,R){var D,F,$=G.length,U,z,Y,Z;for(F=0;F<$;F+=1)if(D=G[F],"ks"in D&&!D.completed){if(D.completed=!0,D.tt&&(G[F-1].td=D.tt),D.hasMask){var K=D.masksProperties;for(z=K.length,U=0;U<z;U+=1)if(K[U].pt.k.i)I(K[U].pt.k);else for(Z=K[U].pt.k.length,Y=0;Y<Z;Y+=1)K[U].pt.k[Y].s&&I(K[U].pt.k[Y].s[0]),K[U].pt.k[Y].e&&I(K[U].pt.k[Y].e[0])}D.ty===0?(D.layers=g(D.refId,R),k(D.layers,R)):D.ty===4?y(D.shapes):D.ty===5&&Q(D)}}function x(G,R){if(G){var D=0,F=G.length;for(D=0;D<F;D+=1)G[D].t===1&&(G[D].data.layers=g(G[D].data.refId,R),k(G[D].data.layers,R))}}function b(G,R){for(var D=0,F=R.length;D<F;){if(R[D].id===G)return R[D];D+=1}return null}function g(G,R){var D=b(G,R);return D?D.layers.__used?JSON.parse(JSON.stringify(D.layers)):(D.layers.__used=!0,D.layers):null}function y(G){var R,D=G.length,F,$;for(R=D-1;R>=0;R-=1)if(G[R].ty==="sh")if(G[R].ks.k.i)I(G[R].ks.k);else for($=G[R].ks.k.length,F=0;F<$;F+=1)G[R].ks.k[F].s&&I(G[R].ks.k[F].s[0]),G[R].ks.k[F].e&&I(G[R].ks.k[F].e[0]);else G[R].ty==="gr"&&y(G[R].it)}function I(G){var R,D=G.i.length;for(R=0;R<D;R+=1)G.i[R][0]+=G.v[R][0],G.i[R][1]+=G.v[R][1],G.o[R][0]+=G.v[R][0],G.o[R][1]+=G.v[R][1]}function M(G,R){var D=R?R.split("."):[100,100,100];return G[0]>D[0]?!0:D[0]>G[0]?!1:G[1]>D[1]?!0:D[1]>G[1]?!1:G[2]>D[2]?!0:D[2]>G[2]?!1:null}var j=function(){var G=[4,4,14];function R(F){var $=F.t.d;F.t.d={k:[{s:$,t:0}]}}function D(F){var $,U=F.length;for($=0;$<U;$+=1)F[$].ty===5&&R(F[$])}return function(F){if(M(G,F.v)&&(D(F.layers),F.assets)){var $,U=F.assets.length;for($=0;$<U;$+=1)F.assets[$].layers&&D(F.assets[$].layers)}}}(),O=function(){var G=[4,7,99];return function(R){if(R.chars&&!M(G,R.v)){var D,F=R.chars.length;for(D=0;D<F;D+=1){var $=R.chars[D];$.data&&$.data.shapes&&(y($.data.shapes),$.data.ip=0,$.data.op=99999,$.data.st=0,$.data.sr=1,$.data.ks={p:{k:[0,0],a:0},s:{k:[100,100],a:0},a:{k:[0,0],a:0},r:{k:0,a:0},o:{k:100,a:0}},R.chars[D].t||($.data.shapes.push({ty:"no"}),$.data.shapes[0].it.push({p:{k:[0,0],a:0},s:{k:[100,100],a:0},a:{k:[0,0],a:0},r:{k:0,a:0},o:{k:100,a:0},sk:{k:0,a:0},sa:{k:0,a:0},ty:"tr"})))}}}}(),L=function(){var G=[5,7,15];function R(F){var $=F.t.p;typeof $.a=="number"&&($.a={a:0,k:$.a}),typeof $.p=="number"&&($.p={a:0,k:$.p}),typeof $.r=="number"&&($.r={a:0,k:$.r})}function D(F){var $,U=F.length;for($=0;$<U;$+=1)F[$].ty===5&&R(F[$])}return function(F){if(M(G,F.v)&&(D(F.layers),F.assets)){var $,U=F.assets.length;for($=0;$<U;$+=1)F.assets[$].layers&&D(F.assets[$].layers)}}}(),N=function(){var G=[4,1,9];function R(F){var $,U=F.length,z,Y;for($=0;$<U;$+=1)if(F[$].ty==="gr")R(F[$].it);else if(F[$].ty==="fl"||F[$].ty==="st")if(F[$].c.k&&F[$].c.k[0].i)for(Y=F[$].c.k.length,z=0;z<Y;z+=1)F[$].c.k[z].s&&(F[$].c.k[z].s[0]/=255,F[$].c.k[z].s[1]/=255,F[$].c.k[z].s[2]/=255,F[$].c.k[z].s[3]/=255),F[$].c.k[z].e&&(F[$].c.k[z].e[0]/=255,F[$].c.k[z].e[1]/=255,F[$].c.k[z].e[2]/=255,F[$].c.k[z].e[3]/=255);else F[$].c.k[0]/=255,F[$].c.k[1]/=255,F[$].c.k[2]/=255,F[$].c.k[3]/=255}function D(F){var $,U=F.length;for($=0;$<U;$+=1)F[$].ty===4&&R(F[$].shapes)}return function(F){if(M(G,F.v)&&(D(F.layers),F.assets)){var $,U=F.assets.length;for($=0;$<U;$+=1)F.assets[$].layers&&D(F.assets[$].layers)}}}(),q=function(){var G=[4,4,18];function R(F){var $,U=F.length,z,Y;for($=U-1;$>=0;$-=1)if(F[$].ty==="sh")if(F[$].ks.k.i)F[$].ks.k.c=F[$].closed;else for(Y=F[$].ks.k.length,z=0;z<Y;z+=1)F[$].ks.k[z].s&&(F[$].ks.k[z].s[0].c=F[$].closed),F[$].ks.k[z].e&&(F[$].ks.k[z].e[0].c=F[$].closed);else F[$].ty==="gr"&&R(F[$].it)}function D(F){var $,U,z=F.length,Y,Z,K,ue;for(U=0;U<z;U+=1){if($=F[U],$.hasMask){var ie=$.masksProperties;for(Z=ie.length,Y=0;Y<Z;Y+=1)if(ie[Y].pt.k.i)ie[Y].pt.k.c=ie[Y].cl;else for(ue=ie[Y].pt.k.length,K=0;K<ue;K+=1)ie[Y].pt.k[K].s&&(ie[Y].pt.k[K].s[0].c=ie[Y].cl),ie[Y].pt.k[K].e&&(ie[Y].pt.k[K].e[0].c=ie[Y].cl)}$.ty===4&&R($.shapes)}}return function(F){if(M(G,F.v)&&(D(F.layers),F.assets)){var $,U=F.assets.length;for($=0;$<U;$+=1)F.assets[$].layers&&D(F.assets[$].layers)}}}();function V(G){G.__complete||(N(G),j(G),O(G),L(G),q(G),k(G.layers,G.assets),x(G.chars,G.assets),G.__complete=!0)}function Q(G){G.t.a.length===0&&!("m"in G.t.p)}var W={};return W.completeData=V,W.checkColors=N,W.checkChars=O,W.checkPathProperties=L,W.checkShapes=q,W.completeLayers=k,W}if(c.dataManager||(c.dataManager=S()),c.assetLoader||(c.assetLoader=function(){function k(b){var g=b.getResponseHeader("content-type");return g&&b.responseType==="json"&&g.indexOf("json")!==-1||b.response&&_typeof$5(b.response)==="object"?b.response:b.response&&typeof b.response=="string"?JSON.parse(b.response):b.responseText?JSON.parse(b.responseText):null}function x(b,g,y,I){var M,j=new XMLHttpRequest;try{j.responseType="json"}catch{}j.onreadystatechange=function(){if(j.readyState===4)if(j.status===200)M=k(j),y(M);else try{M=k(j),y(M)}catch(O){I&&I(O)}};try{j.open("GET",b,!0)}catch{j.open("GET",g+"/"+b,!0)}j.send()}return{load:x}}()),T.data.type==="loadAnimation")c.assetLoader.load(T.data.path,T.data.fullPath,function(k){c.dataManager.completeData(k),c.postMessage({id:T.data.id,payload:k,status:"success"})},function(){c.postMessage({id:T.data.id,status:"error"})});else if(T.data.type==="complete"){var P=T.data.animation;c.dataManager.completeData(P),c.postMessage({id:T.data.id,payload:P,status:"success"})}else T.data.type==="loadData"&&c.assetLoader.load(T.data.path,T.data.fullPath,function(k){c.postMessage({id:T.data.id,payload:k,status:"success"})},function(){c.postMessage({id:T.data.id,status:"error"})})}),a.onmessage=function(A){var T=A.data,S=T.id,P=r[S];r[S]=null,T.status==="success"?P.onComplete(T.payload):P.onError&&P.onError()})}function m(A,T){e+=1;var S="processId_"+e;return r[S]={onComplete:A,onError:T},S}function E(A,T,S){d();var P=m(T,S);a.postMessage({type:"loadAnimation",path:A,fullPath:window.location.origin+window.location.pathname,id:P})}function C(A,T,S){d();var P=m(T,S);a.postMessage({type:"loadData",path:A,fullPath:window.location.origin+window.location.pathname,id:P})}function _(A,T,S){d();var P=m(T,S);a.postMessage({type:"complete",animation:A,id:P})}return{loadAnimation:E,loadData:C,completeAnimation:_}}(),ImagePreloader=function(){var e=function(){var x=createTag("canvas");x.width=1,x.height=1;var b=x.getContext("2d");return b.fillStyle="rgba(0,0,0,0)",b.fillRect(0,0,1,1),x}();function r(){this.loadedAssets+=1,this.loadedAssets===this.totalImages&&this.loadedFootagesCount===this.totalFootages&&this.imagesLoadedCb&&this.imagesLoadedCb(null)}function n(){this.loadedFootagesCount+=1,this.loadedAssets===this.totalImages&&this.loadedFootagesCount===this.totalFootages&&this.imagesLoadedCb&&this.imagesLoadedCb(null)}function a(x,b,g){var y="";if(x.e)y=x.p;else if(b){var I=x.p;I.indexOf("images/")!==-1&&(I=I.split("/")[1]),y=b+I}else y=g,y+=x.u?x.u:"",y+=x.p;return y}function l(x){var b=0,g=setInterval(function(){var y=x.getBBox();(y.width||b>500)&&(this._imageLoaded(),clearInterval(g)),b+=1}.bind(this),50)}function c(x){var b=a(x,this.assetsPath,this.path),g=createNS("image");isSafari?this.testImageLoaded(g):g.addEventListener("load",this._imageLoaded,!1),g.addEventListener("error",function(){y.img=e,this._imageLoaded()}.bind(this),!1),g.setAttributeNS("http://www.w3.org/1999/xlink","href",b),this._elementHelper.append?this._elementHelper.append(g):this._elementHelper.appendChild(g);var y={img:g,assetData:x};return y}function u(x){var b=a(x,this.assetsPath,this.path),g=createTag("img");g.crossOrigin="anonymous",g.addEventListener("load",this._imageLoaded,!1),g.addEventListener("error",function(){y.img=e,this._imageLoaded()}.bind(this),!1),g.src=b;var y={img:g,assetData:x};return y}function d(x){var b={assetData:x},g=a(x,this.assetsPath,this.path);return dataManager.loadData(g,function(y){b.img=y,this._footageLoaded()}.bind(this),function(){b.img={},this._footageLoaded()}.bind(this)),b}function m(x,b){this.imagesLoadedCb=b;var g,y=x.length;for(g=0;g<y;g+=1)x[g].layers||(!x[g].t||x[g].t==="seq"?(this.totalImages+=1,this.images.push(this._createImageData(x[g]))):x[g].t===3&&(this.totalFootages+=1,this.images.push(this.createFootageData(x[g]))))}function E(x){this.path=x||""}function C(x){this.assetsPath=x||""}function _(x){for(var b=0,g=this.images.length;b<g;){if(this.images[b].assetData===x)return this.images[b].img;b+=1}return null}function A(){this.imagesLoadedCb=null,this.images.length=0}function T(){return this.totalImages===this.loadedAssets}function S(){return this.totalFootages===this.loadedFootagesCount}function P(x,b){x==="svg"?(this._elementHelper=b,this._createImageData=this.createImageData.bind(this)):this._createImageData=this.createImgData.bind(this)}function k(){this._imageLoaded=r.bind(this),this._footageLoaded=n.bind(this),this.testImageLoaded=l.bind(this),this.createFootageData=d.bind(this),this.assetsPath="",this.path="",this.totalImages=0,this.totalFootages=0,this.loadedAssets=0,this.loadedFootagesCount=0,this.imagesLoadedCb=null,this.images=[]}return k.prototype={loadAssets:m,setAssetsPath:C,setPath:E,loadedImages:T,loadedFootages:S,destroy:A,getAsset:_,createImgData:u,createImageData:c,imageLoaded:r,footageLoaded:n,setCacheType:P},k}();function BaseEvent(){}BaseEvent.prototype={triggerEvent:function(r,n){if(this._cbs[r])for(var a=this._cbs[r],l=0;l<a.length;l+=1)a[l](n)},addEventListener:function(r,n){return this._cbs[r]||(this._cbs[r]=[]),this._cbs[r].push(n),function(){this.removeEventListener(r,n)}.bind(this)},removeEventListener:function(r,n){if(!n)this._cbs[r]=null;else if(this._cbs[r]){for(var a=0,l=this._cbs[r].length;a<l;)this._cbs[r][a]===n&&(this._cbs[r].splice(a,1),a-=1,l-=1),a+=1;this._cbs[r].length||(this._cbs[r]=null)}}};var markerParser=function(){function e(r){for(var n=r.split(`\r
`),a={},l,c=0,u=0;u<n.length;u+=1)l=n[u].split(":"),l.length===2&&(a[l[0]]=l[1].trim(),c+=1);if(c===0)throw new Error;return a}return function(r){for(var n=[],a=0;a<r.length;a+=1){var l=r[a],c={time:l.tm,duration:l.dr};try{c.payload=JSON.parse(r[a].cm)}catch{try{c.payload=e(r[a].cm)}catch{c.payload={name:r[a]}}}n.push(c)}return n}}(),ProjectInterface=function(){function e(r){this.compositions.push(r)}return function(){function r(n){for(var a=0,l=this.compositions.length;a<l;){if(this.compositions[a].data&&this.compositions[a].data.nm===n)return this.compositions[a].prepareFrame&&this.compositions[a].data.xt&&this.compositions[a].prepareFrame(this.currentFrame),this.compositions[a].compInterface;a+=1}return null}return r.compositions=[],r.currentFrame=0,r.registerComposition=e,r}}(),renderers={},registerRenderer=function(r,n){renderers[r]=n};function getRenderer(e){return renderers[e]}function _typeof$4(e){return typeof Symbol=="function"&&typeof Symbol.iterator=="symbol"?_typeof$4=function(n){return typeof n}:_typeof$4=function(n){return n&&typeof Symbol=="function"&&n.constructor===Symbol&&n!==Symbol.prototype?"symbol":typeof n},_typeof$4(e)}var AnimationItem=function(){this._cbs=[],this.name="",this.path="",this.isLoaded=!1,this.currentFrame=0,this.currentRawFrame=0,this.firstFrame=0,this.totalFrames=0,this.frameRate=0,this.frameMult=0,this.playSpeed=1,this.playDirection=1,this.playCount=0,this.animationData={},this.assets=[],this.isPaused=!0,this.autoplay=!1,this.loop=!0,this.renderer=null,this.animationID=createElementID(),this.assetsPath="",this.timeCompleted=0,this.segmentPos=0,this.isSubframeEnabled=getSubframeEnabled(),this.segments=[],this._idle=!0,this._completedLoop=!1,this.projectInterface=ProjectInterface(),this.imagePreloader=new ImagePreloader,this.audioController=audioControllerFactory(),this.markers=[],this.configAnimation=this.configAnimation.bind(this),this.onSetupError=this.onSetupError.bind(this),this.onSegmentComplete=this.onSegmentComplete.bind(this),this.drawnFrameEvent=new BMEnterFrameEvent("drawnFrame",0,0,0)};extendPrototype([BaseEvent],AnimationItem),AnimationItem.prototype.setParams=function(e){(e.wrapper||e.container)&&(this.wrapper=e.wrapper||e.container);var r="svg";e.animType?r=e.animType:e.renderer&&(r=e.renderer);var n=getRenderer(r);this.renderer=new n(this,e.rendererSettings),this.imagePreloader.setCacheType(r,this.renderer.globalData.defs),this.renderer.setProjectInterface(this.projectInterface),this.animType=r,e.loop===""||e.loop===null||e.loop===void 0||e.loop===!0?this.loop=!0:e.loop===!1?this.loop=!1:this.loop=parseInt(e.loop,10),this.autoplay="autoplay"in e?e.autoplay:!0,this.name=e.name?e.name:"",this.autoloadSegments=Object.prototype.hasOwnProperty.call(e,"autoloadSegments")?e.autoloadSegments:!0,this.assetsPath=e.assetsPath,this.initialSegment=e.initialSegment,e.audioFactory&&this.audioController.setAudioFactory(e.audioFactory),e.animationData?this.setupAnimation(e.animationData):e.path&&(e.path.lastIndexOf("\\")!==-1?this.path=e.path.substr(0,e.path.lastIndexOf("\\")+1):this.path=e.path.substr(0,e.path.lastIndexOf("/")+1),this.fileName=e.path.substr(e.path.lastIndexOf("/")+1),this.fileName=this.fileName.substr(0,this.fileName.lastIndexOf(".json")),dataManager.loadAnimation(e.path,this.configAnimation,this.onSetupError))},AnimationItem.prototype.onSetupError=function(){this.trigger("data_failed")},AnimationItem.prototype.setupAnimation=function(e){dataManager.completeAnimation(e,this.configAnimation)},AnimationItem.prototype.setData=function(e,r){r&&_typeof$4(r)!=="object"&&(r=JSON.parse(r));var n={wrapper:e,animationData:r},a=e.attributes;n.path=a.getNamedItem("data-animation-path")?a.getNamedItem("data-animation-path").value:a.getNamedItem("data-bm-path")?a.getNamedItem("data-bm-path").value:a.getNamedItem("bm-path")?a.getNamedItem("bm-path").value:"",n.animType=a.getNamedItem("data-anim-type")?a.getNamedItem("data-anim-type").value:a.getNamedItem("data-bm-type")?a.getNamedItem("data-bm-type").value:a.getNamedItem("bm-type")?a.getNamedItem("bm-type").value:a.getNamedItem("data-bm-renderer")?a.getNamedItem("data-bm-renderer").value:a.getNamedItem("bm-renderer")?a.getNamedItem("bm-renderer").value:"canvas";var l=a.getNamedItem("data-anim-loop")?a.getNamedItem("data-anim-loop").value:a.getNamedItem("data-bm-loop")?a.getNamedItem("data-bm-loop").value:a.getNamedItem("bm-loop")?a.getNamedItem("bm-loop").value:"";l==="false"?n.loop=!1:l==="true"?n.loop=!0:l!==""&&(n.loop=parseInt(l,10));var c=a.getNamedItem("data-anim-autoplay")?a.getNamedItem("data-anim-autoplay").value:a.getNamedItem("data-bm-autoplay")?a.getNamedItem("data-bm-autoplay").value:a.getNamedItem("bm-autoplay")?a.getNamedItem("bm-autoplay").value:!0;n.autoplay=c!=="false",n.name=a.getNamedItem("data-name")?a.getNamedItem("data-name").value:a.getNamedItem("data-bm-name")?a.getNamedItem("data-bm-name").value:a.getNamedItem("bm-name")?a.getNamedItem("bm-name").value:"";var u=a.getNamedItem("data-anim-prerender")?a.getNamedItem("data-anim-prerender").value:a.getNamedItem("data-bm-prerender")?a.getNamedItem("data-bm-prerender").value:a.getNamedItem("bm-prerender")?a.getNamedItem("bm-prerender").value:"";u==="false"&&(n.prerender=!1),this.setParams(n)},AnimationItem.prototype.includeLayers=function(e){e.op>this.animationData.op&&(this.animationData.op=e.op,this.totalFrames=Math.floor(e.op-this.animationData.ip));var r=this.animationData.layers,n,a=r.length,l=e.layers,c,u=l.length;for(c=0;c<u;c+=1)for(n=0;n<a;){if(r[n].id===l[c].id){r[n]=l[c];break}n+=1}if((e.chars||e.fonts)&&(this.renderer.globalData.fontManager.addChars(e.chars),this.renderer.globalData.fontManager.addFonts(e.fonts,this.renderer.globalData.defs)),e.assets)for(a=e.assets.length,n=0;n<a;n+=1)this.animationData.assets.push(e.assets[n]);this.animationData.__complete=!1,dataManager.completeAnimation(this.animationData,this.onSegmentComplete)},AnimationItem.prototype.onSegmentComplete=function(e){this.animationData=e;var r=getExpressionsPlugin();r&&r.initExpressions(this),this.loadNextSegment()},AnimationItem.prototype.loadNextSegment=function(){var e=this.animationData.segments;if(!e||e.length===0||!this.autoloadSegments){this.trigger("data_ready"),this.timeCompleted=this.totalFrames;return}var r=e.shift();this.timeCompleted=r.time*this.frameRate;var n=this.path+this.fileName+"_"+this.segmentPos+".json";this.segmentPos+=1,dataManager.loadData(n,this.includeLayers.bind(this),function(){this.trigger("data_failed")}.bind(this))},AnimationItem.prototype.loadSegments=function(){var e=this.animationData.segments;e||(this.timeCompleted=this.totalFrames),this.loadNextSegment()},AnimationItem.prototype.imagesLoaded=function(){this.trigger("loaded_images"),this.checkLoaded()},AnimationItem.prototype.preloadImages=function(){this.imagePreloader.setAssetsPath(this.assetsPath),this.imagePreloader.setPath(this.path),this.imagePreloader.loadAssets(this.animationData.assets,this.imagesLoaded.bind(this))},AnimationItem.prototype.configAnimation=function(e){if(!!this.renderer)try{this.animationData=e,this.initialSegment?(this.totalFrames=Math.floor(this.initialSegment[1]-this.initialSegment[0]),this.firstFrame=Math.round(this.initialSegment[0])):(this.totalFrames=Math.floor(this.animationData.op-this.animationData.ip),this.firstFrame=Math.round(this.animationData.ip)),this.renderer.configAnimation(e),e.assets||(e.assets=[]),this.assets=this.animationData.assets,this.frameRate=this.animationData.fr,this.frameMult=this.animationData.fr/1e3,this.renderer.searchExtraCompositions(e.assets),this.markers=markerParser(e.markers||[]),this.trigger("config_ready"),this.preloadImages(),this.loadSegments(),this.updaFrameModifier(),this.waitForFontsLoaded(),this.isPaused&&this.audioController.pause()}catch(r){this.triggerConfigError(r)}},AnimationItem.prototype.waitForFontsLoaded=function(){!this.renderer||(this.renderer.globalData.fontManager.isLoaded?this.checkLoaded():setTimeout(this.waitForFontsLoaded.bind(this),20))},AnimationItem.prototype.checkLoaded=function(){if(!this.isLoaded&&this.renderer.globalData.fontManager.isLoaded&&(this.imagePreloader.loadedImages()||this.renderer.rendererType!=="canvas")&&this.imagePreloader.loadedFootages()){this.isLoaded=!0;var e=getExpressionsPlugin();e&&e.initExpressions(this),this.renderer.initItems(),setTimeout(function(){this.trigger("DOMLoaded")}.bind(this),0),this.gotoFrame(),this.autoplay&&this.play()}},AnimationItem.prototype.resize=function(){this.renderer.updateContainerSize()},AnimationItem.prototype.setSubframe=function(e){this.isSubframeEnabled=!!e},AnimationItem.prototype.gotoFrame=function(){this.currentFrame=this.isSubframeEnabled?this.currentRawFrame:~~this.currentRawFrame,this.timeCompleted!==this.totalFrames&&this.currentFrame>this.timeCompleted&&(this.currentFrame=this.timeCompleted),this.trigger("enterFrame"),this.renderFrame(),this.trigger("drawnFrame")},AnimationItem.prototype.renderFrame=function(){if(!(this.isLoaded===!1||!this.renderer))try{this.renderer.renderFrame(this.currentFrame+this.firstFrame)}catch(e){this.triggerRenderFrameError(e)}},AnimationItem.prototype.play=function(e){e&&this.name!==e||this.isPaused===!0&&(this.isPaused=!1,this.trigger("_pause"),this.audioController.resume(),this._idle&&(this._idle=!1,this.trigger("_active")))},AnimationItem.prototype.pause=function(e){e&&this.name!==e||this.isPaused===!1&&(this.isPaused=!0,this.trigger("_play"),this._idle=!0,this.trigger("_idle"),this.audioController.pause())},AnimationItem.prototype.togglePause=function(e){e&&this.name!==e||(this.isPaused===!0?this.play():this.pause())},AnimationItem.prototype.stop=function(e){e&&this.name!==e||(this.pause(),this.playCount=0,this._completedLoop=!1,this.setCurrentRawFrameValue(0))},AnimationItem.prototype.getMarkerData=function(e){for(var r,n=0;n<this.markers.length;n+=1)if(r=this.markers[n],r.payload&&r.payload.name.cm===e)return r;return null},AnimationItem.prototype.goToAndStop=function(e,r,n){if(!(n&&this.name!==n)){var a=Number(e);if(isNaN(a)){var l=this.getMarkerData(e);l&&this.goToAndStop(l.time,!0)}else r?this.setCurrentRawFrameValue(e):this.setCurrentRawFrameValue(e*this.frameModifier);this.pause()}},AnimationItem.prototype.goToAndPlay=function(e,r,n){if(!(n&&this.name!==n)){var a=Number(e);if(isNaN(a)){var l=this.getMarkerData(e);l&&(l.duration?this.playSegments([l.time,l.time+l.duration],!0):this.goToAndStop(l.time,!0))}else this.goToAndStop(a,r,n);this.play()}},AnimationItem.prototype.advanceTime=function(e){if(!(this.isPaused===!0||this.isLoaded===!1)){var r=this.currentRawFrame+e*this.frameModifier,n=!1;r>=this.totalFrames-1&&this.frameModifier>0?!this.loop||this.playCount===this.loop?this.checkSegments(r>this.totalFrames?r%this.totalFrames:0)||(n=!0,r=this.totalFrames-1):r>=this.totalFrames?(this.playCount+=1,this.checkSegments(r%this.totalFrames)||(this.setCurrentRawFrameValue(r%this.totalFrames),this._completedLoop=!0,this.trigger("loopComplete"))):this.setCurrentRawFrameValue(r):r<0?this.checkSegments(r%this.totalFrames)||(this.loop&&!(this.playCount--<=0&&this.loop!==!0)?(this.setCurrentRawFrameValue(this.totalFrames+r%this.totalFrames),this._completedLoop?this.trigger("loopComplete"):this._completedLoop=!0):(n=!0,r=0)):this.setCurrentRawFrameValue(r),n&&(this.setCurrentRawFrameValue(r),this.pause(),this.trigger("complete"))}},AnimationItem.prototype.adjustSegment=function(e,r){this.playCount=0,e[1]<e[0]?(this.frameModifier>0&&(this.playSpeed<0?this.setSpeed(-this.playSpeed):this.setDirection(-1)),this.totalFrames=e[0]-e[1],this.timeCompleted=this.totalFrames,this.firstFrame=e[1],this.setCurrentRawFrameValue(this.totalFrames-.001-r)):e[1]>e[0]&&(this.frameModifier<0&&(this.playSpeed<0?this.setSpeed(-this.playSpeed):this.setDirection(1)),this.totalFrames=e[1]-e[0],this.timeCompleted=this.totalFrames,this.firstFrame=e[0],this.setCurrentRawFrameValue(.001+r)),this.trigger("segmentStart")},AnimationItem.prototype.setSegment=function(e,r){var n=-1;this.isPaused&&(this.currentRawFrame+this.firstFrame<e?n=e:this.currentRawFrame+this.firstFrame>r&&(n=r-e)),this.firstFrame=e,this.totalFrames=r-e,this.timeCompleted=this.totalFrames,n!==-1&&this.goToAndStop(n,!0)},AnimationItem.prototype.playSegments=function(e,r){if(r&&(this.segments.length=0),_typeof$4(e[0])==="object"){var n,a=e.length;for(n=0;n<a;n+=1)this.segments.push(e[n])}else this.segments.push(e);this.segments.length&&r&&this.adjustSegment(this.segments.shift(),0),this.isPaused&&this.play()},AnimationItem.prototype.resetSegments=function(e){this.segments.length=0,this.segments.push([this.animationData.ip,this.animationData.op]),e&&this.checkSegments(0)},AnimationItem.prototype.checkSegments=function(e){return this.segments.length?(this.adjustSegment(this.segments.shift(),e),!0):!1},AnimationItem.prototype.destroy=function(e){e&&this.name!==e||!this.renderer||(this.renderer.destroy(),this.imagePreloader.destroy(),this.trigger("destroy"),this._cbs=null,this.onEnterFrame=null,this.onLoopComplete=null,this.onComplete=null,this.onSegmentStart=null,this.onDestroy=null,this.renderer=null,this.renderer=null,this.imagePreloader=null,this.projectInterface=null)},AnimationItem.prototype.setCurrentRawFrameValue=function(e){this.currentRawFrame=e,this.gotoFrame()},AnimationItem.prototype.setSpeed=function(e){this.playSpeed=e,this.updaFrameModifier()},AnimationItem.prototype.setDirection=function(e){this.playDirection=e<0?-1:1,this.updaFrameModifier()},AnimationItem.prototype.setVolume=function(e,r){r&&this.name!==r||this.audioController.setVolume(e)},AnimationItem.prototype.getVolume=function(){return this.audioController.getVolume()},AnimationItem.prototype.mute=function(e){e&&this.name!==e||this.audioController.mute()},AnimationItem.prototype.unmute=function(e){e&&this.name!==e||this.audioController.unmute()},AnimationItem.prototype.updaFrameModifier=function(){this.frameModifier=this.frameMult*this.playSpeed*this.playDirection,this.audioController.setRate(this.playSpeed*this.playDirection)},AnimationItem.prototype.getPath=function(){return this.path},AnimationItem.prototype.getAssetsPath=function(e){var r="";if(e.e)r=e.p;else if(this.assetsPath){var n=e.p;n.indexOf("images/")!==-1&&(n=n.split("/")[1]),r=this.assetsPath+n}else r=this.path,r+=e.u?e.u:"",r+=e.p;return r},AnimationItem.prototype.getAssetData=function(e){for(var r=0,n=this.assets.length;r<n;){if(e===this.assets[r].id)return this.assets[r];r+=1}return null},AnimationItem.prototype.hide=function(){this.renderer.hide()},AnimationItem.prototype.show=function(){this.renderer.show()},AnimationItem.prototype.getDuration=function(e){return e?this.totalFrames:this.totalFrames/this.frameRate},AnimationItem.prototype.updateDocumentData=function(e,r,n){try{var a=this.renderer.getElementByPath(e);a.updateDocumentData(r,n)}catch{}},AnimationItem.prototype.trigger=function(e){if(this._cbs&&this._cbs[e])switch(e){case"enterFrame":this.triggerEvent(e,new BMEnterFrameEvent(e,this.currentFrame,this.totalFrames,this.frameModifier));break;case"drawnFrame":this.drawnFrameEvent.currentTime=this.currentFrame,this.drawnFrameEvent.totalTime=this.totalFrames,this.drawnFrameEvent.direction=this.frameModifier,this.triggerEvent(e,this.drawnFrameEvent);break;case"loopComplete":this.triggerEvent(e,new BMCompleteLoopEvent(e,this.loop,this.playCount,this.frameMult));break;case"complete":this.triggerEvent(e,new BMCompleteEvent(e,this.frameMult));break;case"segmentStart":this.triggerEvent(e,new BMSegmentStartEvent(e,this.firstFrame,this.totalFrames));break;case"destroy":this.triggerEvent(e,new BMDestroyEvent(e,this));break;default:this.triggerEvent(e)}e==="enterFrame"&&this.onEnterFrame&&this.onEnterFrame.call(this,new BMEnterFrameEvent(e,this.currentFrame,this.totalFrames,this.frameMult)),e==="loopComplete"&&this.onLoopComplete&&this.onLoopComplete.call(this,new BMCompleteLoopEvent(e,this.loop,this.playCount,this.frameMult)),e==="complete"&&this.onComplete&&this.onComplete.call(this,new BMCompleteEvent(e,this.frameMult)),e==="segmentStart"&&this.onSegmentStart&&this.onSegmentStart.call(this,new BMSegmentStartEvent(e,this.firstFrame,this.totalFrames)),e==="destroy"&&this.onDestroy&&this.onDestroy.call(this,new BMDestroyEvent(e,this))},AnimationItem.prototype.triggerRenderFrameError=function(e){var r=new BMRenderFrameErrorEvent(e,this.currentFrame);this.triggerEvent("error",r),this.onError&&this.onError.call(this,r)},AnimationItem.prototype.triggerConfigError=function(e){var r=new BMConfigErrorEvent(e,this.currentFrame);this.triggerEvent("error",r),this.onError&&this.onError.call(this,r)};var animationManager=function(){var e={},r=[],n=0,a=0,l=0,c=!0,u=!1;function d(R){for(var D=0,F=R.target;D<a;)r[D].animation===F&&(r.splice(D,1),D-=1,a-=1,F.isPaused||_()),D+=1}function m(R,D){if(!R)return null;for(var F=0;F<a;){if(r[F].elem===R&&r[F].elem!==null)return r[F].animation;F+=1}var $=new AnimationItem;return A($,R),$.setData(R,D),$}function E(){var R,D=r.length,F=[];for(R=0;R<D;R+=1)F.push(r[R].animation);return F}function C(){l+=1,N()}function _(){l-=1}function A(R,D){R.addEventListener("destroy",d),R.addEventListener("_active",C),R.addEventListener("_idle",_),r.push({elem:D,animation:R}),a+=1}function T(R){var D=new AnimationItem;return A(D,null),D.setParams(R),D}function S(R,D){var F;for(F=0;F<a;F+=1)r[F].animation.setSpeed(R,D)}function P(R,D){var F;for(F=0;F<a;F+=1)r[F].animation.setDirection(R,D)}function k(R){var D;for(D=0;D<a;D+=1)r[D].animation.play(R)}function x(R){var D=R-n,F;for(F=0;F<a;F+=1)r[F].animation.advanceTime(D);n=R,l&&!u?window.requestAnimationFrame(x):c=!0}function b(R){n=R,window.requestAnimationFrame(x)}function g(R){var D;for(D=0;D<a;D+=1)r[D].animation.pause(R)}function y(R,D,F){var $;for($=0;$<a;$+=1)r[$].animation.goToAndStop(R,D,F)}function I(R){var D;for(D=0;D<a;D+=1)r[D].animation.stop(R)}function M(R){var D;for(D=0;D<a;D+=1)r[D].animation.togglePause(R)}function j(R){var D;for(D=a-1;D>=0;D-=1)r[D].animation.destroy(R)}function O(R,D,F){var $=[].concat([].slice.call(document.getElementsByClassName("lottie")),[].slice.call(document.getElementsByClassName("bodymovin"))),U,z=$.length;for(U=0;U<z;U+=1)F&&$[U].setAttribute("data-bm-type",F),m($[U],R);if(D&&z===0){F||(F="svg");var Y=document.getElementsByTagName("body")[0];Y.innerText="";var Z=createTag("div");Z.style.width="100%",Z.style.height="100%",Z.setAttribute("data-bm-type",F),Y.appendChild(Z),m(Z,R)}}function L(){var R;for(R=0;R<a;R+=1)r[R].animation.resize()}function N(){!u&&l&&c&&(window.requestAnimationFrame(b),c=!1)}function q(){u=!0}function V(){u=!1,N()}function Q(R,D){var F;for(F=0;F<a;F+=1)r[F].animation.setVolume(R,D)}function W(R){var D;for(D=0;D<a;D+=1)r[D].animation.mute(R)}function G(R){var D;for(D=0;D<a;D+=1)r[D].animation.unmute(R)}return e.registerAnimation=m,e.loadAnimation=T,e.setSpeed=S,e.setDirection=P,e.play=k,e.pause=g,e.stop=I,e.togglePause=M,e.searchAnimations=O,e.resize=L,e.goToAndStop=y,e.destroy=j,e.freeze=q,e.unfreeze=V,e.setVolume=Q,e.mute=W,e.unmute=G,e.getRegisteredAnimations=E,e}(),BezierFactory=function(){var e={};e.getBezierEasing=n;var r={};function n(b,g,y,I,M){var j=M||("bez_"+b+"_"+g+"_"+y+"_"+I).replace(/\./g,"p");if(r[j])return r[j];var O=new x([b,g,y,I]);return r[j]=O,O}var a=4,l=.001,c=1e-7,u=10,d=11,m=1/(d-1),E=typeof Float32Array=="function";function C(b,g){return 1-3*g+3*b}function _(b,g){return 3*g-6*b}function A(b){return 3*b}function T(b,g,y){return((C(g,y)*b+_(g,y))*b+A(g))*b}function S(b,g,y){return 3*C(g,y)*b*b+2*_(g,y)*b+A(g)}function P(b,g,y,I,M){var j,O,L=0;do O=g+(y-g)/2,j=T(O,I,M)-b,j>0?y=O:g=O;while(Math.abs(j)>c&&++L<u);return O}function k(b,g,y,I){for(var M=0;M<a;++M){var j=S(g,y,I);if(j===0)return g;var O=T(g,y,I)-b;g-=O/j}return g}function x(b){this._p=b,this._mSampleValues=E?new Float32Array(d):new Array(d),this._precomputed=!1,this.get=this.get.bind(this)}return x.prototype={get:function(g){var y=this._p[0],I=this._p[1],M=this._p[2],j=this._p[3];return this._precomputed||this._precompute(),y===I&&M===j?g:g===0?0:g===1?1:T(this._getTForX(g),I,j)},_precompute:function(){var g=this._p[0],y=this._p[1],I=this._p[2],M=this._p[3];this._precomputed=!0,(g!==y||I!==M)&&this._calcSampleValues()},_calcSampleValues:function(){for(var g=this._p[0],y=this._p[2],I=0;I<d;++I)this._mSampleValues[I]=T(I*m,g,y)},_getTForX:function(g){for(var y=this._p[0],I=this._p[2],M=this._mSampleValues,j=0,O=1,L=d-1;O!==L&&M[O]<=g;++O)j+=m;--O;var N=(g-M[O])/(M[O+1]-M[O]),q=j+N*m,V=S(q,y,I);return V>=l?k(g,q,y,I):V===0?q:P(g,j,j+m,y,I)}},e}(),pooling=function(){function e(r){return r.concat(createSizedArray(r.length))}return{double:e}}(),poolFactory=function(){return function(e,r,n){var a=0,l=e,c=createSizedArray(l),u={newElement:d,release:m};function d(){var E;return a?(a-=1,E=c[a]):E=r(),E}function m(E){a===l&&(c=pooling.double(c),l*=2),n&&n(E),c[a]=E,a+=1}return u}}(),bezierLengthPool=function(){function e(){return{addedLength:0,percents:createTypedArray("float32",getDefaultCurveSegments()),lengths:createTypedArray("float32",getDefaultCurveSegments())}}return poolFactory(8,e)}(),segmentsLengthPool=function(){function e(){return{lengths:[],totalLength:0}}function r(n){var a,l=n.lengths.length;for(a=0;a<l;a+=1)bezierLengthPool.release(n.lengths[a]);n.lengths.length=0}return poolFactory(8,e,r)}();function bezFunction(){var e=Math;function r(A,T,S,P,k,x){var b=A*P+T*k+S*x-k*P-x*A-S*T;return b>-.001&&b<.001}function n(A,T,S,P,k,x,b,g,y){if(S===0&&x===0&&y===0)return r(A,T,P,k,b,g);var I=e.sqrt(e.pow(P-A,2)+e.pow(k-T,2)+e.pow(x-S,2)),M=e.sqrt(e.pow(b-A,2)+e.pow(g-T,2)+e.pow(y-S,2)),j=e.sqrt(e.pow(b-P,2)+e.pow(g-k,2)+e.pow(y-x,2)),O;return I>M?I>j?O=I-M-j:O=j-M-I:j>M?O=j-M-I:O=M-I-j,O>-1e-4&&O<1e-4}var a=function(){return function(A,T,S,P){var k=getDefaultCurveSegments(),x,b,g,y,I,M=0,j,O=[],L=[],N=bezierLengthPool.newElement();for(g=S.length,x=0;x<k;x+=1){for(I=x/(k-1),j=0,b=0;b<g;b+=1)y=bmPow(1-I,3)*A[b]+3*bmPow(1-I,2)*I*S[b]+3*(1-I)*bmPow(I,2)*P[b]+bmPow(I,3)*T[b],O[b]=y,L[b]!==null&&(j+=bmPow(O[b]-L[b],2)),L[b]=O[b];j&&(j=bmSqrt(j),M+=j),N.percents[x]=I,N.lengths[x]=M}return N.addedLength=M,N}}();function l(A){var T=segmentsLengthPool.newElement(),S=A.c,P=A.v,k=A.o,x=A.i,b,g=A._length,y=T.lengths,I=0;for(b=0;b<g-1;b+=1)y[b]=a(P[b],P[b+1],k[b],x[b+1]),I+=y[b].addedLength;return S&&g&&(y[b]=a(P[b],P[0],k[b],x[0]),I+=y[b].addedLength),T.totalLength=I,T}function c(A){this.segmentLength=0,this.points=new Array(A)}function u(A,T){this.partialLength=A,this.point=T}var d=function(){var A={};return function(T,S,P,k){var x=(T[0]+"_"+T[1]+"_"+S[0]+"_"+S[1]+"_"+P[0]+"_"+P[1]+"_"+k[0]+"_"+k[1]).replace(/\./g,"p");if(!A[x]){var b=getDefaultCurveSegments(),g,y,I,M,j,O=0,L,N,q=null;T.length===2&&(T[0]!==S[0]||T[1]!==S[1])&&r(T[0],T[1],S[0],S[1],T[0]+P[0],T[1]+P[1])&&r(T[0],T[1],S[0],S[1],S[0]+k[0],S[1]+k[1])&&(b=2);var V=new c(b);for(I=P.length,g=0;g<b;g+=1){for(N=createSizedArray(I),j=g/(b-1),L=0,y=0;y<I;y+=1)M=bmPow(1-j,3)*T[y]+3*bmPow(1-j,2)*j*(T[y]+P[y])+3*(1-j)*bmPow(j,2)*(S[y]+k[y])+bmPow(j,3)*S[y],N[y]=M,q!==null&&(L+=bmPow(N[y]-q[y],2));L=bmSqrt(L),O+=L,V.points[g]=new u(L,N),q=N}V.segmentLength=O,A[x]=V}return A[x]}}();function m(A,T){var S=T.percents,P=T.lengths,k=S.length,x=bmFloor((k-1)*A),b=A*T.addedLength,g=0;if(x===k-1||x===0||b===P[x])return S[x];for(var y=P[x]>b?-1:1,I=!0;I;)if(P[x]<=b&&P[x+1]>b?(g=(b-P[x])/(P[x+1]-P[x]),I=!1):x+=y,x<0||x>=k-1){if(x===k-1)return S[x];I=!1}return S[x]+(S[x+1]-S[x])*g}function E(A,T,S,P,k,x){var b=m(k,x),g=1-b,y=e.round((g*g*g*A[0]+(b*g*g+g*b*g+g*g*b)*S[0]+(b*b*g+g*b*b+b*g*b)*P[0]+b*b*b*T[0])*1e3)/1e3,I=e.round((g*g*g*A[1]+(b*g*g+g*b*g+g*g*b)*S[1]+(b*b*g+g*b*b+b*g*b)*P[1]+b*b*b*T[1])*1e3)/1e3;return[y,I]}var C=createTypedArray("float32",8);function _(A,T,S,P,k,x,b){k<0?k=0:k>1&&(k=1);var g=m(k,b);x=x>1?1:x;var y=m(x,b),I,M=A.length,j=1-g,O=1-y,L=j*j*j,N=g*j*j*3,q=g*g*j*3,V=g*g*g,Q=j*j*O,W=g*j*O+j*g*O+j*j*y,G=g*g*O+j*g*y+g*j*y,R=g*g*y,D=j*O*O,F=g*O*O+j*y*O+j*O*y,$=g*y*O+j*y*y+g*O*y,U=g*y*y,z=O*O*O,Y=y*O*O+O*y*O+O*O*y,Z=y*y*O+O*y*y+y*O*y,K=y*y*y;for(I=0;I<M;I+=1)C[I*4]=e.round((L*A[I]+N*S[I]+q*P[I]+V*T[I])*1e3)/1e3,C[I*4+1]=e.round((Q*A[I]+W*S[I]+G*P[I]+R*T[I])*1e3)/1e3,C[I*4+2]=e.round((D*A[I]+F*S[I]+$*P[I]+U*T[I])*1e3)/1e3,C[I*4+3]=e.round((z*A[I]+Y*S[I]+Z*P[I]+K*T[I])*1e3)/1e3;return C}return{getSegmentsLength:l,getNewSegment:_,getPointInSegment:E,buildBezierData:d,pointOnLine2D:r,pointOnLine3D:n}}var bez=bezFunction(),PropertyFactory=function(){var e=initialDefaultFrame,r=Math.abs;function n(k,x){var b=this.offsetTime,g;this.propType==="multidimensional"&&(g=createTypedArray("float32",this.pv.length));for(var y=x.lastIndex,I=y,M=this.keyframes.length-1,j=!0,O,L,N;j;){if(O=this.keyframes[I],L=this.keyframes[I+1],I===M-1&&k>=L.t-b){O.h&&(O=L),y=0;break}if(L.t-b>k){y=I;break}I<M-1?I+=1:(y=0,j=!1)}N=this.keyframesMetadata[I]||{};var q,V,Q,W,G,R,D=L.t-b,F=O.t-b,$;if(O.to){N.bezierData||(N.bezierData=bez.buildBezierData(O.s,L.s||O.e,O.to,O.ti));var U=N.bezierData;if(k>=D||k<F){var z=k>=D?U.points.length-1:0;for(V=U.points[z].point.length,q=0;q<V;q+=1)g[q]=U.points[z].point[q]}else{N.__fnct?R=N.__fnct:(R=BezierFactory.getBezierEasing(O.o.x,O.o.y,O.i.x,O.i.y,O.n).get,N.__fnct=R),Q=R((k-F)/(D-F));var Y=U.segmentLength*Q,Z,K=x.lastFrame<k&&x._lastKeyframeIndex===I?x._lastAddedLength:0;for(G=x.lastFrame<k&&x._lastKeyframeIndex===I?x._lastPoint:0,j=!0,W=U.points.length;j;){if(K+=U.points[G].partialLength,Y===0||Q===0||G===U.points.length-1){for(V=U.points[G].point.length,q=0;q<V;q+=1)g[q]=U.points[G].point[q];break}else if(Y>=K&&Y<K+U.points[G+1].partialLength){for(Z=(Y-K)/U.points[G+1].partialLength,V=U.points[G].point.length,q=0;q<V;q+=1)g[q]=U.points[G].point[q]+(U.points[G+1].point[q]-U.points[G].point[q])*Z;break}G<W-1?G+=1:j=!1}x._lastPoint=G,x._lastAddedLength=K-U.points[G].partialLength,x._lastKeyframeIndex=I}}else{var ue,ie,te,X,ce;if(M=O.s.length,$=L.s||O.e,this.sh&&O.h!==1)if(k>=D)g[0]=$[0],g[1]=$[1],g[2]=$[2];else if(k<=F)g[0]=O.s[0],g[1]=O.s[1],g[2]=O.s[2];else{var J=c(O.s),oe=c($),de=(k-F)/(D-F);l(g,a(J,oe,de))}else for(I=0;I<M;I+=1)O.h!==1&&(k>=D?Q=1:k<F?Q=0:(O.o.x.constructor===Array?(N.__fnct||(N.__fnct=[]),N.__fnct[I]?R=N.__fnct[I]:(ue=O.o.x[I]===void 0?O.o.x[0]:O.o.x[I],ie=O.o.y[I]===void 0?O.o.y[0]:O.o.y[I],te=O.i.x[I]===void 0?O.i.x[0]:O.i.x[I],X=O.i.y[I]===void 0?O.i.y[0]:O.i.y[I],R=BezierFactory.getBezierEasing(ue,ie,te,X).get,N.__fnct[I]=R)):N.__fnct?R=N.__fnct:(ue=O.o.x,ie=O.o.y,te=O.i.x,X=O.i.y,R=BezierFactory.getBezierEasing(ue,ie,te,X).get,O.keyframeMetadata=R),Q=R((k-F)/(D-F)))),$=L.s||O.e,ce=O.h===1?O.s[I]:O.s[I]+($[I]-O.s[I])*Q,this.propType==="multidimensional"?g[I]=ce:g=ce}return x.lastIndex=y,g}function a(k,x,b){var g=[],y=k[0],I=k[1],M=k[2],j=k[3],O=x[0],L=x[1],N=x[2],q=x[3],V,Q,W,G,R;return Q=y*O+I*L+M*N+j*q,Q<0&&(Q=-Q,O=-O,L=-L,N=-N,q=-q),1-Q>1e-6?(V=Math.acos(Q),W=Math.sin(V),G=Math.sin((1-b)*V)/W,R=Math.sin(b*V)/W):(G=1-b,R=b),g[0]=G*y+R*O,g[1]=G*I+R*L,g[2]=G*M+R*N,g[3]=G*j+R*q,g}function l(k,x){var b=x[0],g=x[1],y=x[2],I=x[3],M=Math.atan2(2*g*I-2*b*y,1-2*g*g-2*y*y),j=Math.asin(2*b*g+2*y*I),O=Math.atan2(2*b*I-2*g*y,1-2*b*b-2*y*y);k[0]=M/degToRads,k[1]=j/degToRads,k[2]=O/degToRads}function c(k){var x=k[0]*degToRads,b=k[1]*degToRads,g=k[2]*degToRads,y=Math.cos(x/2),I=Math.cos(b/2),M=Math.cos(g/2),j=Math.sin(x/2),O=Math.sin(b/2),L=Math.sin(g/2),N=y*I*M-j*O*L,q=j*O*M+y*I*L,V=j*I*M+y*O*L,Q=y*O*M-j*I*L;return[q,V,Q,N]}function u(){var k=this.comp.renderedFrame-this.offsetTime,x=this.keyframes[0].t-this.offsetTime,b=this.keyframes[this.keyframes.length-1].t-this.offsetTime;if(!(k===this._caching.lastFrame||this._caching.lastFrame!==e&&(this._caching.lastFrame>=b&&k>=b||this._caching.lastFrame<x&&k<x))){this._caching.lastFrame>=k&&(this._caching._lastKeyframeIndex=-1,this._caching.lastIndex=0);var g=this.interpolateValue(k,this._caching);this.pv=g}return this._caching.lastFrame=k,this.pv}function d(k){var x;if(this.propType==="unidimensional")x=k*this.mult,r(this.v-x)>1e-5&&(this.v=x,this._mdf=!0);else for(var b=0,g=this.v.length;b<g;)x=k[b]*this.mult,r(this.v[b]-x)>1e-5&&(this.v[b]=x,this._mdf=!0),b+=1}function m(){if(!(this.elem.globalData.frameId===this.frameId||!this.effectsSequence.length)){if(this.lock){this.setVValue(this.pv);return}this.lock=!0,this._mdf=this._isFirstFrame;var k,x=this.effectsSequence.length,b=this.kf?this.pv:this.data.k;for(k=0;k<x;k+=1)b=this.effectsSequence[k](b);this.setVValue(b),this._isFirstFrame=!1,this.lock=!1,this.frameId=this.elem.globalData.frameId}}function E(k){this.effectsSequence.push(k),this.container.addDynamicProperty(this)}function C(k,x,b,g){this.propType="unidimensional",this.mult=b||1,this.data=x,this.v=b?x.k*b:x.k,this.pv=x.k,this._mdf=!1,this.elem=k,this.container=g,this.comp=k.comp,this.k=!1,this.kf=!1,this.vel=0,this.effectsSequence=[],this._isFirstFrame=!0,this.getValue=m,this.setVValue=d,this.addEffect=E}function _(k,x,b,g){this.propType="multidimensional",this.mult=b||1,this.data=x,this._mdf=!1,this.elem=k,this.container=g,this.comp=k.comp,this.k=!1,this.kf=!1,this.frameId=-1;var y,I=x.k.length;for(this.v=createTypedArray("float32",I),this.pv=createTypedArray("float32",I),this.vel=createTypedArray("float32",I),y=0;y<I;y+=1)this.v[y]=x.k[y]*this.mult,this.pv[y]=x.k[y];this._isFirstFrame=!0,this.effectsSequence=[],this.getValue=m,this.setVValue=d,this.addEffect=E}function A(k,x,b,g){this.propType="unidimensional",this.keyframes=x.k,this.keyframesMetadata=[],this.offsetTime=k.data.st,this.frameId=-1,this._caching={lastFrame:e,lastIndex:0,value:0,_lastKeyframeIndex:-1},this.k=!0,this.kf=!0,this.data=x,this.mult=b||1,this.elem=k,this.container=g,this.comp=k.comp,this.v=e,this.pv=e,this._isFirstFrame=!0,this.getValue=m,this.setVValue=d,this.interpolateValue=n,this.effectsSequence=[u.bind(this)],this.addEffect=E}function T(k,x,b,g){this.propType="multidimensional";var y,I=x.k.length,M,j,O,L;for(y=0;y<I-1;y+=1)x.k[y].to&&x.k[y].s&&x.k[y+1]&&x.k[y+1].s&&(M=x.k[y].s,j=x.k[y+1].s,O=x.k[y].to,L=x.k[y].ti,(M.length===2&&!(M[0]===j[0]&&M[1]===j[1])&&bez.pointOnLine2D(M[0],M[1],j[0],j[1],M[0]+O[0],M[1]+O[1])&&bez.pointOnLine2D(M[0],M[1],j[0],j[1],j[0]+L[0],j[1]+L[1])||M.length===3&&!(M[0]===j[0]&&M[1]===j[1]&&M[2]===j[2])&&bez.pointOnLine3D(M[0],M[1],M[2],j[0],j[1],j[2],M[0]+O[0],M[1]+O[1],M[2]+O[2])&&bez.pointOnLine3D(M[0],M[1],M[2],j[0],j[1],j[2],j[0]+L[0],j[1]+L[1],j[2]+L[2]))&&(x.k[y].to=null,x.k[y].ti=null),M[0]===j[0]&&M[1]===j[1]&&O[0]===0&&O[1]===0&&L[0]===0&&L[1]===0&&(M.length===2||M[2]===j[2]&&O[2]===0&&L[2]===0)&&(x.k[y].to=null,x.k[y].ti=null));this.effectsSequence=[u.bind(this)],this.data=x,this.keyframes=x.k,this.keyframesMetadata=[],this.offsetTime=k.data.st,this.k=!0,this.kf=!0,this._isFirstFrame=!0,this.mult=b||1,this.elem=k,this.container=g,this.comp=k.comp,this.getValue=m,this.setVValue=d,this.interpolateValue=n,this.frameId=-1;var N=x.k[0].s.length;for(this.v=createTypedArray("float32",N),this.pv=createTypedArray("float32",N),y=0;y<N;y+=1)this.v[y]=e,this.pv[y]=e;this._caching={lastFrame:e,lastIndex:0,value:createTypedArray("float32",N)},this.addEffect=E}function S(k,x,b,g,y){var I;if(!x.k.length)I=new C(k,x,g,y);else if(typeof x.k[0]=="number")I=new _(k,x,g,y);else switch(b){case 0:I=new A(k,x,g,y);break;case 1:I=new T(k,x,g,y);break}return I.effectsSequence.length&&y.addDynamicProperty(I),I}var P={getProp:S};return P}();function DynamicPropertyContainer(){}DynamicPropertyContainer.prototype={addDynamicProperty:function(r){this.dynamicProperties.indexOf(r)===-1&&(this.dynamicProperties.push(r),this.container.addDynamicProperty(this),this._isAnimated=!0)},iterateDynamicProperties:function(){this._mdf=!1;var r,n=this.dynamicProperties.length;for(r=0;r<n;r+=1)this.dynamicProperties[r].getValue(),this.dynamicProperties[r]._mdf&&(this._mdf=!0)},initDynamicPropertyContainer:function(r){this.container=r,this.dynamicProperties=[],this._mdf=!1,this._isAnimated=!1}};var pointPool=function(){function e(){return createTypedArray("float32",2)}return poolFactory(8,e)}();function ShapePath(){this.c=!1,this._length=0,this._maxLength=8,this.v=createSizedArray(this._maxLength),this.o=createSizedArray(this._maxLength),this.i=createSizedArray(this._maxLength)}ShapePath.prototype.setPathData=function(e,r){this.c=e,this.setLength(r);for(var n=0;n<r;)this.v[n]=pointPool.newElement(),this.o[n]=pointPool.newElement(),this.i[n]=pointPool.newElement(),n+=1},ShapePath.prototype.setLength=function(e){for(;this._maxLength<e;)this.doubleArrayLength();this._length=e},ShapePath.prototype.doubleArrayLength=function(){this.v=this.v.concat(createSizedArray(this._maxLength)),this.i=this.i.concat(createSizedArray(this._maxLength)),this.o=this.o.concat(createSizedArray(this._maxLength)),this._maxLength*=2},ShapePath.prototype.setXYAt=function(e,r,n,a,l){var c;switch(this._length=Math.max(this._length,a+1),this._length>=this._maxLength&&this.doubleArrayLength(),n){case"v":c=this.v;break;case"i":c=this.i;break;case"o":c=this.o;break;default:c=[];break}(!c[a]||c[a]&&!l)&&(c[a]=pointPool.newElement()),c[a][0]=e,c[a][1]=r},ShapePath.prototype.setTripleAt=function(e,r,n,a,l,c,u,d){this.setXYAt(e,r,"v",u,d),this.setXYAt(n,a,"o",u,d),this.setXYAt(l,c,"i",u,d)},ShapePath.prototype.reverse=function(){var e=new ShapePath;e.setPathData(this.c,this._length);var r=this.v,n=this.o,a=this.i,l=0;this.c&&(e.setTripleAt(r[0][0],r[0][1],a[0][0],a[0][1],n[0][0],n[0][1],0,!1),l=1);var c=this._length-1,u=this._length,d;for(d=l;d<u;d+=1)e.setTripleAt(r[c][0],r[c][1],a[c][0],a[c][1],n[c][0],n[c][1],d,!1),c-=1;return e};var shapePool=function(){function e(){return new ShapePath}function r(l){var c=l._length,u;for(u=0;u<c;u+=1)pointPool.release(l.v[u]),pointPool.release(l.i[u]),pointPool.release(l.o[u]),l.v[u]=null,l.i[u]=null,l.o[u]=null;l._length=0,l.c=!1}function n(l){var c=a.newElement(),u,d=l._length===void 0?l.v.length:l._length;for(c.setLength(d),c.c=l.c,u=0;u<d;u+=1)c.setTripleAt(l.v[u][0],l.v[u][1],l.o[u][0],l.o[u][1],l.i[u][0],l.i[u][1],u);return c}var a=poolFactory(4,e,r);return a.clone=n,a}();function ShapeCollection(){this._length=0,this._maxLength=4,this.shapes=createSizedArray(this._maxLength)}ShapeCollection.prototype.addShape=function(e){this._length===this._maxLength&&(this.shapes=this.shapes.concat(createSizedArray(this._maxLength)),this._maxLength*=2),this.shapes[this._length]=e,this._length+=1},ShapeCollection.prototype.releaseShapes=function(){var e;for(e=0;e<this._length;e+=1)shapePool.release(this.shapes[e]);this._length=0};var shapeCollectionPool=function(){var e={newShapeCollection:l,release:c},r=0,n=4,a=createSizedArray(n);function l(){var u;return r?(r-=1,u=a[r]):u=new ShapeCollection,u}function c(u){var d,m=u._length;for(d=0;d<m;d+=1)shapePool.release(u.shapes[d]);u._length=0,r===n&&(a=pooling.double(a),n*=2),a[r]=u,r+=1}return e}(),ShapePropertyFactory=function(){var e=-999999;function r(x,b,g){var y=g.lastIndex,I,M,j,O,L,N,q,V,Q,W=this.keyframes;if(x<W[0].t-this.offsetTime)I=W[0].s[0],j=!0,y=0;else if(x>=W[W.length-1].t-this.offsetTime)I=W[W.length-1].s?W[W.length-1].s[0]:W[W.length-2].e[0],j=!0;else{for(var G=y,R=W.length-1,D=!0,F,$,U;D&&(F=W[G],$=W[G+1],!($.t-this.offsetTime>x));)G<R-1?G+=1:D=!1;if(U=this.keyframesMetadata[G]||{},j=F.h===1,y=G,!j){if(x>=$.t-this.offsetTime)V=1;else if(x<F.t-this.offsetTime)V=0;else{var z;U.__fnct?z=U.__fnct:(z=BezierFactory.getBezierEasing(F.o.x,F.o.y,F.i.x,F.i.y).get,U.__fnct=z),V=z((x-(F.t-this.offsetTime))/($.t-this.offsetTime-(F.t-this.offsetTime)))}M=$.s?$.s[0]:F.e[0]}I=F.s[0]}for(N=b._length,q=I.i[0].length,g.lastIndex=y,O=0;O<N;O+=1)for(L=0;L<q;L+=1)Q=j?I.i[O][L]:I.i[O][L]+(M.i[O][L]-I.i[O][L])*V,b.i[O][L]=Q,Q=j?I.o[O][L]:I.o[O][L]+(M.o[O][L]-I.o[O][L])*V,b.o[O][L]=Q,Q=j?I.v[O][L]:I.v[O][L]+(M.v[O][L]-I.v[O][L])*V,b.v[O][L]=Q}function n(){var x=this.comp.renderedFrame-this.offsetTime,b=this.keyframes[0].t-this.offsetTime,g=this.keyframes[this.keyframes.length-1].t-this.offsetTime,y=this._caching.lastFrame;return y!==e&&(y<b&&x<b||y>g&&x>g)||(this._caching.lastIndex=y<x?this._caching.lastIndex:0,this.interpolateShape(x,this.pv,this._caching)),this._caching.lastFrame=x,this.pv}function a(){this.paths=this.localShapeCollection}function l(x,b){if(x._length!==b._length||x.c!==b.c)return!1;var g,y=x._length;for(g=0;g<y;g+=1)if(x.v[g][0]!==b.v[g][0]||x.v[g][1]!==b.v[g][1]||x.o[g][0]!==b.o[g][0]||x.o[g][1]!==b.o[g][1]||x.i[g][0]!==b.i[g][0]||x.i[g][1]!==b.i[g][1])return!1;return!0}function c(x){l(this.v,x)||(this.v=shapePool.clone(x),this.localShapeCollection.releaseShapes(),this.localShapeCollection.addShape(this.v),this._mdf=!0,this.paths=this.localShapeCollection)}function u(){if(this.elem.globalData.frameId!==this.frameId){if(!this.effectsSequence.length){this._mdf=!1;return}if(this.lock){this.setVValue(this.pv);return}this.lock=!0,this._mdf=!1;var x;this.kf?x=this.pv:this.data.ks?x=this.data.ks.k:x=this.data.pt.k;var b,g=this.effectsSequence.length;for(b=0;b<g;b+=1)x=this.effectsSequence[b](x);this.setVValue(x),this.lock=!1,this.frameId=this.elem.globalData.frameId}}function d(x,b,g){this.propType="shape",this.comp=x.comp,this.container=x,this.elem=x,this.data=b,this.k=!1,this.kf=!1,this._mdf=!1;var y=g===3?b.pt.k:b.ks.k;this.v=shapePool.clone(y),this.pv=shapePool.clone(this.v),this.localShapeCollection=shapeCollectionPool.newShapeCollection(),this.paths=this.localShapeCollection,this.paths.addShape(this.v),this.reset=a,this.effectsSequence=[]}function m(x){this.effectsSequence.push(x),this.container.addDynamicProperty(this)}d.prototype.interpolateShape=r,d.prototype.getValue=u,d.prototype.setVValue=c,d.prototype.addEffect=m;function E(x,b,g){this.propType="shape",this.comp=x.comp,this.elem=x,this.container=x,this.offsetTime=x.data.st,this.keyframes=g===3?b.pt.k:b.ks.k,this.keyframesMetadata=[],this.k=!0,this.kf=!0;var y=this.keyframes[0].s[0].i.length;this.v=shapePool.newElement(),this.v.setPathData(this.keyframes[0].s[0].c,y),this.pv=shapePool.clone(this.v),this.localShapeCollection=shapeCollectionPool.newShapeCollection(),this.paths=this.localShapeCollection,this.paths.addShape(this.v),this.lastFrame=e,this.reset=a,this._caching={lastFrame:e,lastIndex:0},this.effectsSequence=[n.bind(this)]}E.prototype.getValue=u,E.prototype.interpolateShape=r,E.prototype.setVValue=c,E.prototype.addEffect=m;var C=function(){var x=roundCorner;function b(g,y){this.v=shapePool.newElement(),this.v.setPathData(!0,4),this.localShapeCollection=shapeCollectionPool.newShapeCollection(),this.paths=this.localShapeCollection,this.localShapeCollection.addShape(this.v),this.d=y.d,this.elem=g,this.comp=g.comp,this.frameId=-1,this.initDynamicPropertyContainer(g),this.p=PropertyFactory.getProp(g,y.p,1,0,this),this.s=PropertyFactory.getProp(g,y.s,1,0,this),this.dynamicProperties.length?this.k=!0:(this.k=!1,this.convertEllToPath())}return b.prototype={reset:a,getValue:function(){this.elem.globalData.frameId!==this.frameId&&(this.frameId=this.elem.globalData.frameId,this.iterateDynamicProperties(),this._mdf&&this.convertEllToPath())},convertEllToPath:function(){var y=this.p.v[0],I=this.p.v[1],M=this.s.v[0]/2,j=this.s.v[1]/2,O=this.d!==3,L=this.v;L.v[0][0]=y,L.v[0][1]=I-j,L.v[1][0]=O?y+M:y-M,L.v[1][1]=I,L.v[2][0]=y,L.v[2][1]=I+j,L.v[3][0]=O?y-M:y+M,L.v[3][1]=I,L.i[0][0]=O?y-M*x:y+M*x,L.i[0][1]=I-j,L.i[1][0]=O?y+M:y-M,L.i[1][1]=I-j*x,L.i[2][0]=O?y+M*x:y-M*x,L.i[2][1]=I+j,L.i[3][0]=O?y-M:y+M,L.i[3][1]=I+j*x,L.o[0][0]=O?y+M*x:y-M*x,L.o[0][1]=I-j,L.o[1][0]=O?y+M:y-M,L.o[1][1]=I+j*x,L.o[2][0]=O?y-M*x:y+M*x,L.o[2][1]=I+j,L.o[3][0]=O?y-M:y+M,L.o[3][1]=I-j*x}},extendPrototype([DynamicPropertyContainer],b),b}(),_=function(){function x(b,g){this.v=shapePool.newElement(),this.v.setPathData(!0,0),this.elem=b,this.comp=b.comp,this.data=g,this.frameId=-1,this.d=g.d,this.initDynamicPropertyContainer(b),g.sy===1?(this.ir=PropertyFactory.getProp(b,g.ir,0,0,this),this.is=PropertyFactory.getProp(b,g.is,0,.01,this),this.convertToPath=this.convertStarToPath):this.convertToPath=this.convertPolygonToPath,this.pt=PropertyFactory.getProp(b,g.pt,0,0,this),this.p=PropertyFactory.getProp(b,g.p,1,0,this),this.r=PropertyFactory.getProp(b,g.r,0,degToRads,this),this.or=PropertyFactory.getProp(b,g.or,0,0,this),this.os=PropertyFactory.getProp(b,g.os,0,.01,this),this.localShapeCollection=shapeCollectionPool.newShapeCollection(),this.localShapeCollection.addShape(this.v),this.paths=this.localShapeCollection,this.dynamicProperties.length?this.k=!0:(this.k=!1,this.convertToPath())}return x.prototype={reset:a,getValue:function(){this.elem.globalData.frameId!==this.frameId&&(this.frameId=this.elem.globalData.frameId,this.iterateDynamicProperties(),this._mdf&&this.convertToPath())},convertStarToPath:function(){var g=Math.floor(this.pt.v)*2,y=Math.PI*2/g,I=!0,M=this.or.v,j=this.ir.v,O=this.os.v,L=this.is.v,N=2*Math.PI*M/(g*2),q=2*Math.PI*j/(g*2),V,Q,W,G,R=-Math.PI/2;R+=this.r.v;var D=this.data.d===3?-1:1;for(this.v._length=0,V=0;V<g;V+=1){Q=I?M:j,W=I?O:L,G=I?N:q;var F=Q*Math.cos(R),$=Q*Math.sin(R),U=F===0&&$===0?0:$/Math.sqrt(F*F+$*$),z=F===0&&$===0?0:-F/Math.sqrt(F*F+$*$);F+=+this.p.v[0],$+=+this.p.v[1],this.v.setTripleAt(F,$,F-U*G*W*D,$-z*G*W*D,F+U*G*W*D,$+z*G*W*D,V,!0),I=!I,R+=y*D}},convertPolygonToPath:function(){var g=Math.floor(this.pt.v),y=Math.PI*2/g,I=this.or.v,M=this.os.v,j=2*Math.PI*I/(g*4),O,L=-Math.PI*.5,N=this.data.d===3?-1:1;for(L+=this.r.v,this.v._length=0,O=0;O<g;O+=1){var q=I*Math.cos(L),V=I*Math.sin(L),Q=q===0&&V===0?0:V/Math.sqrt(q*q+V*V),W=q===0&&V===0?0:-q/Math.sqrt(q*q+V*V);q+=+this.p.v[0],V+=+this.p.v[1],this.v.setTripleAt(q,V,q-Q*j*M*N,V-W*j*M*N,q+Q*j*M*N,V+W*j*M*N,O,!0),L+=y*N}this.paths.length=0,this.paths[0]=this.v}},extendPrototype([DynamicPropertyContainer],x),x}(),A=function(){function x(b,g){this.v=shapePool.newElement(),this.v.c=!0,this.localShapeCollection=shapeCollectionPool.newShapeCollection(),this.localShapeCollection.addShape(this.v),this.paths=this.localShapeCollection,this.elem=b,this.comp=b.comp,this.frameId=-1,this.d=g.d,this.initDynamicPropertyContainer(b),this.p=PropertyFactory.getProp(b,g.p,1,0,this),this.s=PropertyFactory.getProp(b,g.s,1,0,this),this.r=PropertyFactory.getProp(b,g.r,0,0,this),this.dynamicProperties.length?this.k=!0:(this.k=!1,this.convertRectToPath())}return x.prototype={convertRectToPath:function(){var g=this.p.v[0],y=this.p.v[1],I=this.s.v[0]/2,M=this.s.v[1]/2,j=bmMin(I,M,this.r.v),O=j*(1-roundCorner);this.v._length=0,this.d===2||this.d===1?(this.v.setTripleAt(g+I,y-M+j,g+I,y-M+j,g+I,y-M+O,0,!0),this.v.setTripleAt(g+I,y+M-j,g+I,y+M-O,g+I,y+M-j,1,!0),j!==0?(this.v.setTripleAt(g+I-j,y+M,g+I-j,y+M,g+I-O,y+M,2,!0),this.v.setTripleAt(g-I+j,y+M,g-I+O,y+M,g-I+j,y+M,3,!0),this.v.setTripleAt(g-I,y+M-j,g-I,y+M-j,g-I,y+M-O,4,!0),this.v.setTripleAt(g-I,y-M+j,g-I,y-M+O,g-I,y-M+j,5,!0),this.v.setTripleAt(g-I+j,y-M,g-I+j,y-M,g-I+O,y-M,6,!0),this.v.setTripleAt(g+I-j,y-M,g+I-O,y-M,g+I-j,y-M,7,!0)):(this.v.setTripleAt(g-I,y+M,g-I+O,y+M,g-I,y+M,2),this.v.setTripleAt(g-I,y-M,g-I,y-M+O,g-I,y-M,3))):(this.v.setTripleAt(g+I,y-M+j,g+I,y-M+O,g+I,y-M+j,0,!0),j!==0?(this.v.setTripleAt(g+I-j,y-M,g+I-j,y-M,g+I-O,y-M,1,!0),this.v.setTripleAt(g-I+j,y-M,g-I+O,y-M,g-I+j,y-M,2,!0),this.v.setTripleAt(g-I,y-M+j,g-I,y-M+j,g-I,y-M+O,3,!0),this.v.setTripleAt(g-I,y+M-j,g-I,y+M-O,g-I,y+M-j,4,!0),this.v.setTripleAt(g-I+j,y+M,g-I+j,y+M,g-I+O,y+M,5,!0),this.v.setTripleAt(g+I-j,y+M,g+I-O,y+M,g+I-j,y+M,6,!0),this.v.setTripleAt(g+I,y+M-j,g+I,y+M-j,g+I,y+M-O,7,!0)):(this.v.setTripleAt(g-I,y-M,g-I+O,y-M,g-I,y-M,1,!0),this.v.setTripleAt(g-I,y+M,g-I,y+M-O,g-I,y+M,2,!0),this.v.setTripleAt(g+I,y+M,g+I-O,y+M,g+I,y+M,3,!0)))},getValue:function(){this.elem.globalData.frameId!==this.frameId&&(this.frameId=this.elem.globalData.frameId,this.iterateDynamicProperties(),this._mdf&&this.convertRectToPath())},reset:a},extendPrototype([DynamicPropertyContainer],x),x}();function T(x,b,g){var y;if(g===3||g===4){var I=g===3?b.pt:b.ks,M=I.k;M.length?y=new E(x,b,g):y=new d(x,b,g)}else g===5?y=new A(x,b):g===6?y=new C(x,b):g===7&&(y=new _(x,b));return y.k&&x.addDynamicProperty(y),y}function S(){return d}function P(){return E}var k={};return k.getShapeProp=T,k.getConstructorFunction=S,k.getKeyframedConstructorFunction=P,k}();/*!
 Transformation Matrix v2.0
 (c) Epistemex 2014-2015
 www.epistemex.com
 By Ken Fyrstenberg
 Contributions by leeoniya.
 License: MIT, header required.
 */var Matrix=function(){var e=Math.cos,r=Math.sin,n=Math.tan,a=Math.round;function l(){return this.props[0]=1,this.props[1]=0,this.props[2]=0,this.props[3]=0,this.props[4]=0,this.props[5]=1,this.props[6]=0,this.props[7]=0,this.props[8]=0,this.props[9]=0,this.props[10]=1,this.props[11]=0,this.props[12]=0,this.props[13]=0,this.props[14]=0,this.props[15]=1,this}function c(D){if(D===0)return this;var F=e(D),$=r(D);return this._t(F,-$,0,0,$,F,0,0,0,0,1,0,0,0,0,1)}function u(D){if(D===0)return this;var F=e(D),$=r(D);return this._t(1,0,0,0,0,F,-$,0,0,$,F,0,0,0,0,1)}function d(D){if(D===0)return this;var F=e(D),$=r(D);return this._t(F,0,$,0,0,1,0,0,-$,0,F,0,0,0,0,1)}function m(D){if(D===0)return this;var F=e(D),$=r(D);return this._t(F,-$,0,0,$,F,0,0,0,0,1,0,0,0,0,1)}function E(D,F){return this._t(1,F,D,1,0,0)}function C(D,F){return this.shear(n(D),n(F))}function _(D,F){var $=e(F),U=r(F);return this._t($,U,0,0,-U,$,0,0,0,0,1,0,0,0,0,1)._t(1,0,0,0,n(D),1,0,0,0,0,1,0,0,0,0,1)._t($,-U,0,0,U,$,0,0,0,0,1,0,0,0,0,1)}function A(D,F,$){return!$&&$!==0&&($=1),D===1&&F===1&&$===1?this:this._t(D,0,0,0,0,F,0,0,0,0,$,0,0,0,0,1)}function T(D,F,$,U,z,Y,Z,K,ue,ie,te,X,ce,J,oe,de){return this.props[0]=D,this.props[1]=F,this.props[2]=$,this.props[3]=U,this.props[4]=z,this.props[5]=Y,this.props[6]=Z,this.props[7]=K,this.props[8]=ue,this.props[9]=ie,this.props[10]=te,this.props[11]=X,this.props[12]=ce,this.props[13]=J,this.props[14]=oe,this.props[15]=de,this}function S(D,F,$){return $=$||0,D!==0||F!==0||$!==0?this._t(1,0,0,0,0,1,0,0,0,0,1,0,D,F,$,1):this}function P(D,F,$,U,z,Y,Z,K,ue,ie,te,X,ce,J,oe,de){var ee=this.props;if(D===1&&F===0&&$===0&&U===0&&z===0&&Y===1&&Z===0&&K===0&&ue===0&&ie===0&&te===1&&X===0)return ee[12]=ee[12]*D+ee[15]*ce,ee[13]=ee[13]*Y+ee[15]*J,ee[14]=ee[14]*te+ee[15]*oe,ee[15]*=de,this._identityCalculated=!1,this;var pe=ee[0],ae=ee[1],ne=ee[2],me=ee[3],ge=ee[4],Te=ee[5],fe=ee[6],Ee=ee[7],_e=ee[8],re=ee[9],le=ee[10],ye=ee[11],ve=ee[12],be=ee[13],Ie=ee[14],Pe=ee[15];return ee[0]=pe*D+ae*z+ne*ue+me*ce,ee[1]=pe*F+ae*Y+ne*ie+me*J,ee[2]=pe*$+ae*Z+ne*te+me*oe,ee[3]=pe*U+ae*K+ne*X+me*de,ee[4]=ge*D+Te*z+fe*ue+Ee*ce,ee[5]=ge*F+Te*Y+fe*ie+Ee*J,ee[6]=ge*$+Te*Z+fe*te+Ee*oe,ee[7]=ge*U+Te*K+fe*X+Ee*de,ee[8]=_e*D+re*z+le*ue+ye*ce,ee[9]=_e*F+re*Y+le*ie+ye*J,ee[10]=_e*$+re*Z+le*te+ye*oe,ee[11]=_e*U+re*K+le*X+ye*de,ee[12]=ve*D+be*z+Ie*ue+Pe*ce,ee[13]=ve*F+be*Y+Ie*ie+Pe*J,ee[14]=ve*$+be*Z+Ie*te+Pe*oe,ee[15]=ve*U+be*K+Ie*X+Pe*de,this._identityCalculated=!1,this}function k(){return this._identityCalculated||(this._identity=!(this.props[0]!==1||this.props[1]!==0||this.props[2]!==0||this.props[3]!==0||this.props[4]!==0||this.props[5]!==1||this.props[6]!==0||this.props[7]!==0||this.props[8]!==0||this.props[9]!==0||this.props[10]!==1||this.props[11]!==0||this.props[12]!==0||this.props[13]!==0||this.props[14]!==0||this.props[15]!==1),this._identityCalculated=!0),this._identity}function x(D){for(var F=0;F<16;){if(D.props[F]!==this.props[F])return!1;F+=1}return!0}function b(D){var F;for(F=0;F<16;F+=1)D.props[F]=this.props[F];return D}function g(D){var F;for(F=0;F<16;F+=1)this.props[F]=D[F]}function y(D,F,$){return{x:D*this.props[0]+F*this.props[4]+$*this.props[8]+this.props[12],y:D*this.props[1]+F*this.props[5]+$*this.props[9]+this.props[13],z:D*this.props[2]+F*this.props[6]+$*this.props[10]+this.props[14]}}function I(D,F,$){return D*this.props[0]+F*this.props[4]+$*this.props[8]+this.props[12]}function M(D,F,$){return D*this.props[1]+F*this.props[5]+$*this.props[9]+this.props[13]}function j(D,F,$){return D*this.props[2]+F*this.props[6]+$*this.props[10]+this.props[14]}function O(){var D=this.props[0]*this.props[5]-this.props[1]*this.props[4],F=this.props[5]/D,$=-this.props[1]/D,U=-this.props[4]/D,z=this.props[0]/D,Y=(this.props[4]*this.props[13]-this.props[5]*this.props[12])/D,Z=-(this.props[0]*this.props[13]-this.props[1]*this.props[12])/D,K=new Matrix;return K.props[0]=F,K.props[1]=$,K.props[4]=U,K.props[5]=z,K.props[12]=Y,K.props[13]=Z,K}function L(D){var F=this.getInverseMatrix();return F.applyToPointArray(D[0],D[1],D[2]||0)}function N(D){var F,$=D.length,U=[];for(F=0;F<$;F+=1)U[F]=L(D[F]);return U}function q(D,F,$){var U=createTypedArray("float32",6);if(this.isIdentity())U[0]=D[0],U[1]=D[1],U[2]=F[0],U[3]=F[1],U[4]=$[0],U[5]=$[1];else{var z=this.props[0],Y=this.props[1],Z=this.props[4],K=this.props[5],ue=this.props[12],ie=this.props[13];U[0]=D[0]*z+D[1]*Z+ue,U[1]=D[0]*Y+D[1]*K+ie,U[2]=F[0]*z+F[1]*Z+ue,U[3]=F[0]*Y+F[1]*K+ie,U[4]=$[0]*z+$[1]*Z+ue,U[5]=$[0]*Y+$[1]*K+ie}return U}function V(D,F,$){var U;return this.isIdentity()?U=[D,F,$]:U=[D*this.props[0]+F*this.props[4]+$*this.props[8]+this.props[12],D*this.props[1]+F*this.props[5]+$*this.props[9]+this.props[13],D*this.props[2]+F*this.props[6]+$*this.props[10]+this.props[14]],U}function Q(D,F){if(this.isIdentity())return D+","+F;var $=this.props;return Math.round((D*$[0]+F*$[4]+$[12])*100)/100+","+Math.round((D*$[1]+F*$[5]+$[13])*100)/100}function W(){for(var D=0,F=this.props,$="matrix3d(",U=1e4;D<16;)$+=a(F[D]*U)/U,$+=D===15?")":",",D+=1;return $}function G(D){var F=1e4;return D<1e-6&&D>0||D>-1e-6&&D<0?a(D*F)/F:D}function R(){var D=this.props,F=G(D[0]),$=G(D[1]),U=G(D[4]),z=G(D[5]),Y=G(D[12]),Z=G(D[13]);return"matrix("+F+","+$+","+U+","+z+","+Y+","+Z+")"}return function(){this.reset=l,this.rotate=c,this.rotateX=u,this.rotateY=d,this.rotateZ=m,this.skew=C,this.skewFromAxis=_,this.shear=E,this.scale=A,this.setTransform=T,this.translate=S,this.transform=P,this.applyToPoint=y,this.applyToX=I,this.applyToY=M,this.applyToZ=j,this.applyToPointArray=V,this.applyToTriplePoints=q,this.applyToPointStringified=Q,this.toCSS=W,this.to2dCSS=R,this.clone=b,this.cloneFromProps=g,this.equals=x,this.inversePoints=N,this.inversePoint=L,this.getInverseMatrix=O,this._t=this.transform,this.isIdentity=k,this._identity=!0,this._identityCalculated=!1,this.props=createTypedArray("float32",16),this.reset()}}();function _typeof$3(e){return typeof Symbol=="function"&&typeof Symbol.iterator=="symbol"?_typeof$3=function(n){return typeof n}:_typeof$3=function(n){return n&&typeof Symbol=="function"&&n.constructor===Symbol&&n!==Symbol.prototype?"symbol":typeof n},_typeof$3(e)}var lottie={};function setLocation(e){setLocationHref(e)}function searchAnimations(){animationManager.searchAnimations()}function setSubframeRendering(e){setSubframeEnabled(e)}function setPrefix(e){setIdPrefix(e)}function loadAnimation(e){return animationManager.loadAnimation(e)}function setQuality(e){if(typeof e=="string")switch(e){case"high":setDefaultCurveSegments(200);break;default:case"medium":setDefaultCurveSegments(50);break;case"low":setDefaultCurveSegments(10);break}else!isNaN(e)&&e>1&&setDefaultCurveSegments(e)}function inBrowser(){return typeof navigator!="undefined"}function installPlugin(e,r){e==="expressions"&&setExpressionsPlugin(r)}function getFactory(e){switch(e){case"propertyFactory":return PropertyFactory;case"shapePropertyFactory":return ShapePropertyFactory;case"matrix":return Matrix;default:return null}}lottie.play=animationManager.play,lottie.pause=animationManager.pause,lottie.setLocationHref=setLocation,lottie.togglePause=animationManager.togglePause,lottie.setSpeed=animationManager.setSpeed,lottie.setDirection=animationManager.setDirection,lottie.stop=animationManager.stop,lottie.searchAnimations=searchAnimations,lottie.registerAnimation=animationManager.registerAnimation,lottie.loadAnimation=loadAnimation,lottie.setSubframeRendering=setSubframeRendering,lottie.resize=animationManager.resize,lottie.goToAndStop=animationManager.goToAndStop,lottie.destroy=animationManager.destroy,lottie.setQuality=setQuality,lottie.inBrowser=inBrowser,lottie.installPlugin=installPlugin,lottie.freeze=animationManager.freeze,lottie.unfreeze=animationManager.unfreeze,lottie.setVolume=animationManager.setVolume,lottie.mute=animationManager.mute,lottie.unmute=animationManager.unmute,lottie.getRegisteredAnimations=animationManager.getRegisteredAnimations,lottie.useWebWorker=setWebWorker,lottie.setIDPrefix=setPrefix,lottie.__getFactory=getFactory,lottie.version="5.9.3";function checkReady(){document.readyState==="complete"&&(clearInterval(readyStateCheckInterval),searchAnimations())}function getQueryVariable(e){for(var r=queryString.split("&"),n=0;n<r.length;n+=1){var a=r[n].split("=");if(decodeURIComponent(a[0])==e)return decodeURIComponent(a[1])}return null}var queryString="";{var scripts=document.getElementsByTagName("script"),index=scripts.length-1,myScript=scripts[index]||{src:""};queryString=myScript.src?myScript.src.replace(/^[^\?]+\??/,""):"",getQueryVariable("renderer")}var readyStateCheckInterval=setInterval(checkReady,100);try{_typeof$3(exports)!=="object"&&(window.bodymovin=lottie)}catch(e){}var ShapeModifiers=function(){var e={},r={};e.registerModifier=n,e.getModifier=a;function n(l,c){r[l]||(r[l]=c)}function a(l,c,u){return new r[l](c,u)}return e}();function ShapeModifier(){}ShapeModifier.prototype.initModifierProperties=function(){},ShapeModifier.prototype.addShapeToModifier=function(){},ShapeModifier.prototype.addShape=function(e){if(!this.closed){e.sh.container.addDynamicProperty(e.sh);var r={shape:e.sh,data:e,localShapeCollection:shapeCollectionPool.newShapeCollection()};this.shapes.push(r),this.addShapeToModifier(r),this._isAnimated&&e.setAsAnimated()}},ShapeModifier.prototype.init=function(e,r){this.shapes=[],this.elem=e,this.initDynamicPropertyContainer(e),this.initModifierProperties(e,r),this.frameId=initialDefaultFrame,this.closed=!1,this.k=!1,this.dynamicProperties.length?this.k=!0:this.getValue(!0)},ShapeModifier.prototype.processKeys=function(){this.elem.globalData.frameId!==this.frameId&&(this.frameId=this.elem.globalData.frameId,this.iterateDynamicProperties())},extendPrototype([DynamicPropertyContainer],ShapeModifier);function TrimModifier(){}extendPrototype([ShapeModifier],TrimModifier),TrimModifier.prototype.initModifierProperties=function(e,r){this.s=PropertyFactory.getProp(e,r.s,0,.01,this),this.e=PropertyFactory.getProp(e,r.e,0,.01,this),this.o=PropertyFactory.getProp(e,r.o,0,0,this),this.sValue=0,this.eValue=0,this.getValue=this.processKeys,this.m=r.m,this._isAnimated=!!this.s.effectsSequence.length||!!this.e.effectsSequence.length||!!this.o.effectsSequence.length},TrimModifier.prototype.addShapeToModifier=function(e){e.pathsData=[]},TrimModifier.prototype.calculateShapeEdges=function(e,r,n,a,l){var c=[];r<=1?c.push({s:e,e:r}):e>=1?c.push({s:e-1,e:r-1}):(c.push({s:e,e:1}),c.push({s:0,e:r-1}));var u=[],d,m=c.length,E;for(d=0;d<m;d+=1)if(E=c[d],!(E.e*l<a||E.s*l>a+n)){var C,_;E.s*l<=a?C=0:C=(E.s*l-a)/n,E.e*l>=a+n?_=1:_=(E.e*l-a)/n,u.push([C,_])}return u.length||u.push([0,0]),u},TrimModifier.prototype.releasePathsData=function(e){var r,n=e.length;for(r=0;r<n;r+=1)segmentsLengthPool.release(e[r]);return e.length=0,e},TrimModifier.prototype.processShapes=function(e){var r,n;if(this._mdf||e){var a=this.o.v%360/360;if(a<0&&(a+=1),this.s.v>1?r=1+a:this.s.v<0?r=0+a:r=this.s.v+a,this.e.v>1?n=1+a:this.e.v<0?n=0+a:n=this.e.v+a,r>n){var l=r;r=n,n=l}r=Math.round(r*1e4)*1e-4,n=Math.round(n*1e4)*1e-4,this.sValue=r,this.eValue=n}else r=this.sValue,n=this.eValue;var c,u,d=this.shapes.length,m,E,C,_,A,T=0;if(n===r)for(u=0;u<d;u+=1)this.shapes[u].localShapeCollection.releaseShapes(),this.shapes[u].shape._mdf=!0,this.shapes[u].shape.paths=this.shapes[u].localShapeCollection,this._mdf&&(this.shapes[u].pathsData.length=0);else if(n===1&&r===0||n===0&&r===1){if(this._mdf)for(u=0;u<d;u+=1)this.shapes[u].pathsData.length=0,this.shapes[u].shape._mdf=!0}else{var S=[],P,k;for(u=0;u<d;u+=1)if(P=this.shapes[u],!P.shape._mdf&&!this._mdf&&!e&&this.m!==2)P.shape.paths=P.localShapeCollection;else{if(c=P.shape.paths,E=c._length,A=0,!P.shape._mdf&&P.pathsData.length)A=P.totalShapeLength;else{for(C=this.releasePathsData(P.pathsData),m=0;m<E;m+=1)_=bez.getSegmentsLength(c.shapes[m]),C.push(_),A+=_.totalLength;P.totalShapeLength=A,P.pathsData=C}T+=A,P.shape._mdf=!0}var x=r,b=n,g=0,y;for(u=d-1;u>=0;u-=1)if(P=this.shapes[u],P.shape._mdf){for(k=P.localShapeCollection,k.releaseShapes(),this.m===2&&d>1?(y=this.calculateShapeEdges(r,n,P.totalShapeLength,g,T),g+=P.totalShapeLength):y=[[x,b]],E=y.length,m=0;m<E;m+=1){x=y[m][0],b=y[m][1],S.length=0,b<=1?S.push({s:P.totalShapeLength*x,e:P.totalShapeLength*b}):x>=1?S.push({s:P.totalShapeLength*(x-1),e:P.totalShapeLength*(b-1)}):(S.push({s:P.totalShapeLength*x,e:P.totalShapeLength}),S.push({s:0,e:P.totalShapeLength*(b-1)}));var I=this.addShapes(P,S[0]);if(S[0].s!==S[0].e){if(S.length>1){var M=P.shape.paths.shapes[P.shape.paths._length-1];if(M.c){var j=I.pop();this.addPaths(I,k),I=this.addShapes(P,S[1],j)}else this.addPaths(I,k),I=this.addShapes(P,S[1])}this.addPaths(I,k)}}P.shape.paths=k}}},TrimModifier.prototype.addPaths=function(e,r){var n,a=e.length;for(n=0;n<a;n+=1)r.addShape(e[n])},TrimModifier.prototype.addSegment=function(e,r,n,a,l,c,u){l.setXYAt(r[0],r[1],"o",c),l.setXYAt(n[0],n[1],"i",c+1),u&&l.setXYAt(e[0],e[1],"v",c),l.setXYAt(a[0],a[1],"v",c+1)},TrimModifier.prototype.addSegmentFromArray=function(e,r,n,a){r.setXYAt(e[1],e[5],"o",n),r.setXYAt(e[2],e[6],"i",n+1),a&&r.setXYAt(e[0],e[4],"v",n),r.setXYAt(e[3],e[7],"v",n+1)},TrimModifier.prototype.addShapes=function(e,r,n){var a=e.pathsData,l=e.shape.paths.shapes,c,u=e.shape.paths._length,d,m,E=0,C,_,A,T,S=[],P,k=!0;for(n?(_=n._length,P=n._length):(n=shapePool.newElement(),_=0,P=0),S.push(n),c=0;c<u;c+=1){for(A=a[c].lengths,n.c=l[c].c,m=l[c].c?A.length:A.length+1,d=1;d<m;d+=1)if(C=A[d-1],E+C.addedLength<r.s)E+=C.addedLength,n.c=!1;else if(E>r.e){n.c=!1;break}else r.s<=E&&r.e>=E+C.addedLength?(this.addSegment(l[c].v[d-1],l[c].o[d-1],l[c].i[d],l[c].v[d],n,_,k),k=!1):(T=bez.getNewSegment(l[c].v[d-1],l[c].v[d],l[c].o[d-1],l[c].i[d],(r.s-E)/C.addedLength,(r.e-E)/C.addedLength,A[d-1]),this.addSegmentFromArray(T,n,_,k),k=!1,n.c=!1),E+=C.addedLength,_+=1;if(l[c].c&&A.length){if(C=A[d-1],E<=r.e){var x=A[d-1].addedLength;r.s<=E&&r.e>=E+x?(this.addSegment(l[c].v[d-1],l[c].o[d-1],l[c].i[0],l[c].v[0],n,_,k),k=!1):(T=bez.getNewSegment(l[c].v[d-1],l[c].v[0],l[c].o[d-1],l[c].i[0],(r.s-E)/x,(r.e-E)/x,A[d-1]),this.addSegmentFromArray(T,n,_,k),k=!1,n.c=!1)}else n.c=!1;E+=C.addedLength,_+=1}if(n._length&&(n.setXYAt(n.v[P][0],n.v[P][1],"i",P),n.setXYAt(n.v[n._length-1][0],n.v[n._length-1][1],"o",n._length-1)),E>r.e)break;c<u-1&&(n=shapePool.newElement(),k=!0,S.push(n),_=0)}return S};function PuckerAndBloatModifier(){}extendPrototype([ShapeModifier],PuckerAndBloatModifier),PuckerAndBloatModifier.prototype.initModifierProperties=function(e,r){this.getValue=this.processKeys,this.amount=PropertyFactory.getProp(e,r.a,0,null,this),this._isAnimated=!!this.amount.effectsSequence.length},PuckerAndBloatModifier.prototype.processPath=function(e,r){var n=r/100,a=[0,0],l=e._length,c=0;for(c=0;c<l;c+=1)a[0]+=e.v[c][0],a[1]+=e.v[c][1];a[0]/=l,a[1]/=l;var u=shapePool.newElement();u.c=e.c;var d,m,E,C,_,A;for(c=0;c<l;c+=1)d=e.v[c][0]+(a[0]-e.v[c][0])*n,m=e.v[c][1]+(a[1]-e.v[c][1])*n,E=e.o[c][0]+(a[0]-e.o[c][0])*-n,C=e.o[c][1]+(a[1]-e.o[c][1])*-n,_=e.i[c][0]+(a[0]-e.i[c][0])*-n,A=e.i[c][1]+(a[1]-e.i[c][1])*-n,u.setTripleAt(d,m,E,C,_,A,c);return u},PuckerAndBloatModifier.prototype.processShapes=function(e){var r,n,a=this.shapes.length,l,c,u=this.amount.v;if(u!==0){var d,m;for(n=0;n<a;n+=1){if(d=this.shapes[n],m=d.localShapeCollection,!(!d.shape._mdf&&!this._mdf&&!e))for(m.releaseShapes(),d.shape._mdf=!0,r=d.shape.paths.shapes,c=d.shape.paths._length,l=0;l<c;l+=1)m.addShape(this.processPath(r[l],u));d.shape.paths=d.localShapeCollection}}this.dynamicProperties.length||(this._mdf=!1)};var TransformPropertyFactory=function(){var e=[0,0];function r(m){var E=this._mdf;this.iterateDynamicProperties(),this._mdf=this._mdf||E,this.a&&m.translate(-this.a.v[0],-this.a.v[1],this.a.v[2]),this.s&&m.scale(this.s.v[0],this.s.v[1],this.s.v[2]),this.sk&&m.skewFromAxis(-this.sk.v,this.sa.v),this.r?m.rotate(-this.r.v):m.rotateZ(-this.rz.v).rotateY(this.ry.v).rotateX(this.rx.v).rotateZ(-this.or.v[2]).rotateY(this.or.v[1]).rotateX(this.or.v[0]),this.data.p.s?this.data.p.z?m.translate(this.px.v,this.py.v,-this.pz.v):m.translate(this.px.v,this.py.v,0):m.translate(this.p.v[0],this.p.v[1],-this.p.v[2])}function n(m){if(this.elem.globalData.frameId!==this.frameId){if(this._isDirty&&(this.precalculateMatrix(),this._isDirty=!1),this.iterateDynamicProperties(),this._mdf||m){var E;if(this.v.cloneFromProps(this.pre.props),this.appliedTransformations<1&&this.v.translate(-this.a.v[0],-this.a.v[1],this.a.v[2]),this.appliedTransformations<2&&this.v.scale(this.s.v[0],this.s.v[1],this.s.v[2]),this.sk&&this.appliedTransformations<3&&this.v.skewFromAxis(-this.sk.v,this.sa.v),this.r&&this.appliedTransformations<4?this.v.rotate(-this.r.v):!this.r&&this.appliedTransformations<4&&this.v.rotateZ(-this.rz.v).rotateY(this.ry.v).rotateX(this.rx.v).rotateZ(-this.or.v[2]).rotateY(this.or.v[1]).rotateX(this.or.v[0]),this.autoOriented){var C,_;if(E=this.elem.globalData.frameRate,this.p&&this.p.keyframes&&this.p.getValueAtTime)this.p._caching.lastFrame+this.p.offsetTime<=this.p.keyframes[0].t?(C=this.p.getValueAtTime((this.p.keyframes[0].t+.01)/E,0),_=this.p.getValueAtTime(this.p.keyframes[0].t/E,0)):this.p._caching.lastFrame+this.p.offsetTime>=this.p.keyframes[this.p.keyframes.length-1].t?(C=this.p.getValueAtTime(this.p.keyframes[this.p.keyframes.length-1].t/E,0),_=this.p.getValueAtTime((this.p.keyframes[this.p.keyframes.length-1].t-.05)/E,0)):(C=this.p.pv,_=this.p.getValueAtTime((this.p._caching.lastFrame+this.p.offsetTime-.01)/E,this.p.offsetTime));else if(this.px&&this.px.keyframes&&this.py.keyframes&&this.px.getValueAtTime&&this.py.getValueAtTime){C=[],_=[];var A=this.px,T=this.py;A._caching.lastFrame+A.offsetTime<=A.keyframes[0].t?(C[0]=A.getValueAtTime((A.keyframes[0].t+.01)/E,0),C[1]=T.getValueAtTime((T.keyframes[0].t+.01)/E,0),_[0]=A.getValueAtTime(A.keyframes[0].t/E,0),_[1]=T.getValueAtTime(T.keyframes[0].t/E,0)):A._caching.lastFrame+A.offsetTime>=A.keyframes[A.keyframes.length-1].t?(C[0]=A.getValueAtTime(A.keyframes[A.keyframes.length-1].t/E,0),C[1]=T.getValueAtTime(T.keyframes[T.keyframes.length-1].t/E,0),_[0]=A.getValueAtTime((A.keyframes[A.keyframes.length-1].t-.01)/E,0),_[1]=T.getValueAtTime((T.keyframes[T.keyframes.length-1].t-.01)/E,0)):(C=[A.pv,T.pv],_[0]=A.getValueAtTime((A._caching.lastFrame+A.offsetTime-.01)/E,A.offsetTime),_[1]=T.getValueAtTime((T._caching.lastFrame+T.offsetTime-.01)/E,T.offsetTime))}else _=e,C=_;this.v.rotate(-Math.atan2(C[1]-_[1],C[0]-_[0]))}this.data.p&&this.data.p.s?this.data.p.z?this.v.translate(this.px.v,this.py.v,-this.pz.v):this.v.translate(this.px.v,this.py.v,0):this.v.translate(this.p.v[0],this.p.v[1],-this.p.v[2])}this.frameId=this.elem.globalData.frameId}}function a(){if(!this.a.k)this.pre.translate(-this.a.v[0],-this.a.v[1],this.a.v[2]),this.appliedTransformations=1;else return;if(!this.s.effectsSequence.length)this.pre.scale(this.s.v[0],this.s.v[1],this.s.v[2]),this.appliedTransformations=2;else return;if(this.sk)if(!this.sk.effectsSequence.length&&!this.sa.effectsSequence.length)this.pre.skewFromAxis(-this.sk.v,this.sa.v),this.appliedTransformations=3;else return;this.r?this.r.effectsSequence.length||(this.pre.rotate(-this.r.v),this.appliedTransformations=4):!this.rz.effectsSequence.length&&!this.ry.effectsSequence.length&&!this.rx.effectsSequence.length&&!this.or.effectsSequence.length&&(this.pre.rotateZ(-this.rz.v).rotateY(this.ry.v).rotateX(this.rx.v).rotateZ(-this.or.v[2]).rotateY(this.or.v[1]).rotateX(this.or.v[0]),this.appliedTransformations=4)}function l(){}function c(m){this._addDynamicProperty(m),this.elem.addDynamicProperty(m),this._isDirty=!0}function u(m,E,C){if(this.elem=m,this.frameId=-1,this.propType="transform",this.data=E,this.v=new Matrix,this.pre=new Matrix,this.appliedTransformations=0,this.initDynamicPropertyContainer(C||m),E.p&&E.p.s?(this.px=PropertyFactory.getProp(m,E.p.x,0,0,this),this.py=PropertyFactory.getProp(m,E.p.y,0,0,this),E.p.z&&(this.pz=PropertyFactory.getProp(m,E.p.z,0,0,this))):this.p=PropertyFactory.getProp(m,E.p||{k:[0,0,0]},1,0,this),E.rx){if(this.rx=PropertyFactory.getProp(m,E.rx,0,degToRads,this),this.ry=PropertyFactory.getProp(m,E.ry,0,degToRads,this),this.rz=PropertyFactory.getProp(m,E.rz,0,degToRads,this),E.or.k[0].ti){var _,A=E.or.k.length;for(_=0;_<A;_+=1)E.or.k[_].to=null,E.or.k[_].ti=null}this.or=PropertyFactory.getProp(m,E.or,1,degToRads,this),this.or.sh=!0}else this.r=PropertyFactory.getProp(m,E.r||{k:0},0,degToRads,this);E.sk&&(this.sk=PropertyFactory.getProp(m,E.sk,0,degToRads,this),this.sa=PropertyFactory.getProp(m,E.sa,0,degToRads,this)),this.a=PropertyFactory.getProp(m,E.a||{k:[0,0,0]},1,0,this),this.s=PropertyFactory.getProp(m,E.s||{k:[100,100,100]},1,.01,this),E.o?this.o=PropertyFactory.getProp(m,E.o,0,.01,m):this.o={_mdf:!1,v:1},this._isDirty=!0,this.dynamicProperties.length||this.getValue(!0)}u.prototype={applyToMatrix:r,getValue:n,precalculateMatrix:a,autoOrient:l},extendPrototype([DynamicPropertyContainer],u),u.prototype.addDynamicProperty=c,u.prototype._addDynamicProperty=DynamicPropertyContainer.prototype.addDynamicProperty;function d(m,E,C){return new u(m,E,C)}return{getTransformProperty:d}}();function RepeaterModifier(){}extendPrototype([ShapeModifier],RepeaterModifier),RepeaterModifier.prototype.initModifierProperties=function(e,r){this.getValue=this.processKeys,this.c=PropertyFactory.getProp(e,r.c,0,null,this),this.o=PropertyFactory.getProp(e,r.o,0,null,this),this.tr=TransformPropertyFactory.getTransformProperty(e,r.tr,this),this.so=PropertyFactory.getProp(e,r.tr.so,0,.01,this),this.eo=PropertyFactory.getProp(e,r.tr.eo,0,.01,this),this.data=r,this.dynamicProperties.length||this.getValue(!0),this._isAnimated=!!this.dynamicProperties.length,this.pMatrix=new Matrix,this.rMatrix=new Matrix,this.sMatrix=new Matrix,this.tMatrix=new Matrix,this.matrix=new Matrix},RepeaterModifier.prototype.applyTransforms=function(e,r,n,a,l,c){var u=c?-1:1,d=a.s.v[0]+(1-a.s.v[0])*(1-l),m=a.s.v[1]+(1-a.s.v[1])*(1-l);e.translate(a.p.v[0]*u*l,a.p.v[1]*u*l,a.p.v[2]),r.translate(-a.a.v[0],-a.a.v[1],a.a.v[2]),r.rotate(-a.r.v*u*l),r.translate(a.a.v[0],a.a.v[1],a.a.v[2]),n.translate(-a.a.v[0],-a.a.v[1],a.a.v[2]),n.scale(c?1/d:d,c?1/m:m),n.translate(a.a.v[0],a.a.v[1],a.a.v[2])},RepeaterModifier.prototype.init=function(e,r,n,a){for(this.elem=e,this.arr=r,this.pos=n,this.elemsData=a,this._currentCopies=0,this._elements=[],this._groups=[],this.frameId=-1,this.initDynamicPropertyContainer(e),this.initModifierProperties(e,r[n]);n>0;)n-=1,this._elements.unshift(r[n]);this.dynamicProperties.length?this.k=!0:this.getValue(!0)},RepeaterModifier.prototype.resetElements=function(e){var r,n=e.length;for(r=0;r<n;r+=1)e[r]._processed=!1,e[r].ty==="gr"&&this.resetElements(e[r].it)},RepeaterModifier.prototype.cloneElements=function(e){var r=JSON.parse(JSON.stringify(e));return this.resetElements(r),r},RepeaterModifier.prototype.changeGroupRender=function(e,r){var n,a=e.length;for(n=0;n<a;n+=1)e[n]._render=r,e[n].ty==="gr"&&this.changeGroupRender(e[n].it,r)},RepeaterModifier.prototype.processShapes=function(e){var r,n,a,l,c,u=!1;if(this._mdf||e){var d=Math.ceil(this.c.v);if(this._groups.length<d){for(;this._groups.length<d;){var m={it:this.cloneElements(this._elements),ty:"gr"};m.it.push({a:{a:0,ix:1,k:[0,0]},nm:"Transform",o:{a:0,ix:7,k:100},p:{a:0,ix:2,k:[0,0]},r:{a:1,ix:6,k:[{s:0,e:0,t:0},{s:0,e:0,t:1}]},s:{a:0,ix:3,k:[100,100]},sa:{a:0,ix:5,k:0},sk:{a:0,ix:4,k:0},ty:"tr"}),this.arr.splice(0,0,m),this._groups.splice(0,0,m),this._currentCopies+=1}this.elem.reloadShapes(),u=!0}c=0;var E;for(a=0;a<=this._groups.length-1;a+=1){if(E=c<d,this._groups[a]._render=E,this.changeGroupRender(this._groups[a].it,E),!E){var C=this.elemsData[a].it,_=C[C.length-1];_.transform.op.v!==0?(_.transform.op._mdf=!0,_.transform.op.v=0):_.transform.op._mdf=!1}c+=1}this._currentCopies=d;var A=this.o.v,T=A%1,S=A>0?Math.floor(A):Math.ceil(A),P=this.pMatrix.props,k=this.rMatrix.props,x=this.sMatrix.props;this.pMatrix.reset(),this.rMatrix.reset(),this.sMatrix.reset(),this.tMatrix.reset(),this.matrix.reset();var b=0;if(A>0){for(;b<S;)this.applyTransforms(this.pMatrix,this.rMatrix,this.sMatrix,this.tr,1,!1),b+=1;T&&(this.applyTransforms(this.pMatrix,this.rMatrix,this.sMatrix,this.tr,T,!1),b+=T)}else if(A<0){for(;b>S;)this.applyTransforms(this.pMatrix,this.rMatrix,this.sMatrix,this.tr,1,!0),b-=1;T&&(this.applyTransforms(this.pMatrix,this.rMatrix,this.sMatrix,this.tr,-T,!0),b-=T)}a=this.data.m===1?0:this._currentCopies-1,l=this.data.m===1?1:-1,c=this._currentCopies;for(var g,y;c;){if(r=this.elemsData[a].it,n=r[r.length-1].transform.mProps.v.props,y=n.length,r[r.length-1].transform.mProps._mdf=!0,r[r.length-1].transform.op._mdf=!0,r[r.length-1].transform.op.v=this._currentCopies===1?this.so.v:this.so.v+(this.eo.v-this.so.v)*(a/(this._currentCopies-1)),b!==0){for((a!==0&&l===1||a!==this._currentCopies-1&&l===-1)&&this.applyTransforms(this.pMatrix,this.rMatrix,this.sMatrix,this.tr,1,!1),this.matrix.transform(k[0],k[1],k[2],k[3],k[4],k[5],k[6],k[7],k[8],k[9],k[10],k[11],k[12],k[13],k[14],k[15]),this.matrix.transform(x[0],x[1],x[2],x[3],x[4],x[5],x[6],x[7],x[8],x[9],x[10],x[11],x[12],x[13],x[14],x[15]),this.matrix.transform(P[0],P[1],P[2],P[3],P[4],P[5],P[6],P[7],P[8],P[9],P[10],P[11],P[12],P[13],P[14],P[15]),g=0;g<y;g+=1)n[g]=this.matrix.props[g];this.matrix.reset()}else for(this.matrix.reset(),g=0;g<y;g+=1)n[g]=this.matrix.props[g];b+=1,c-=1,a+=l}}else for(c=this._currentCopies,a=0,l=1;c;)r=this.elemsData[a].it,n=r[r.length-1].transform.mProps.v.props,r[r.length-1].transform.mProps._mdf=!1,r[r.length-1].transform.op._mdf=!1,c-=1,a+=l;return u},RepeaterModifier.prototype.addShape=function(){};function RoundCornersModifier(){}extendPrototype([ShapeModifier],RoundCornersModifier),RoundCornersModifier.prototype.initModifierProperties=function(e,r){this.getValue=this.processKeys,this.rd=PropertyFactory.getProp(e,r.r,0,null,this),this._isAnimated=!!this.rd.effectsSequence.length},RoundCornersModifier.prototype.processPath=function(e,r){var n=shapePool.newElement();n.c=e.c;var a,l=e._length,c,u,d,m,E,C,_=0,A,T,S,P,k,x;for(a=0;a<l;a+=1)c=e.v[a],d=e.o[a],u=e.i[a],c[0]===d[0]&&c[1]===d[1]&&c[0]===u[0]&&c[1]===u[1]?(a===0||a===l-1)&&!e.c?(n.setTripleAt(c[0],c[1],d[0],d[1],u[0],u[1],_),_+=1):(a===0?m=e.v[l-1]:m=e.v[a-1],E=Math.sqrt(Math.pow(c[0]-m[0],2)+Math.pow(c[1]-m[1],2)),C=E?Math.min(E/2,r)/E:0,k=c[0]+(m[0]-c[0])*C,A=k,x=c[1]-(c[1]-m[1])*C,T=x,S=A-(A-c[0])*roundCorner,P=T-(T-c[1])*roundCorner,n.setTripleAt(A,T,S,P,k,x,_),_+=1,a===l-1?m=e.v[0]:m=e.v[a+1],E=Math.sqrt(Math.pow(c[0]-m[0],2)+Math.pow(c[1]-m[1],2)),C=E?Math.min(E/2,r)/E:0,S=c[0]+(m[0]-c[0])*C,A=S,P=c[1]+(m[1]-c[1])*C,T=P,k=A-(A-c[0])*roundCorner,x=T-(T-c[1])*roundCorner,n.setTripleAt(A,T,S,P,k,x,_),_+=1):(n.setTripleAt(e.v[a][0],e.v[a][1],e.o[a][0],e.o[a][1],e.i[a][0],e.i[a][1],_),_+=1);return n},RoundCornersModifier.prototype.processShapes=function(e){var r,n,a=this.shapes.length,l,c,u=this.rd.v;if(u!==0){var d,m;for(n=0;n<a;n+=1){if(d=this.shapes[n],m=d.localShapeCollection,!(!d.shape._mdf&&!this._mdf&&!e))for(m.releaseShapes(),d.shape._mdf=!0,r=d.shape.paths.shapes,c=d.shape.paths._length,l=0;l<c;l+=1)m.addShape(this.processPath(r[l],u));d.shape.paths=d.localShapeCollection}}this.dynamicProperties.length||(this._mdf=!1)};function getFontProperties(e){for(var r=e.fStyle?e.fStyle.split(" "):[],n="normal",a="normal",l=r.length,c,u=0;u<l;u+=1)switch(c=r[u].toLowerCase(),c){case"italic":a="italic";break;case"bold":n="700";break;case"black":n="900";break;case"medium":n="500";break;case"regular":case"normal":n="400";break;case"light":case"thin":n="200";break}return{style:a,weight:e.fWeight||n}}var FontManager=function(){var e=5e3,r={w:0,size:0,shapes:[],data:{shapes:[]}},n=[];n=n.concat([2304,2305,2306,2307,2362,2363,2364,2364,2366,2367,2368,2369,2370,2371,2372,2373,2374,2375,2376,2377,2378,2379,2380,2381,2382,2383,2387,2388,2389,2390,2391,2402,2403]);var a=["d83cdffb","d83cdffc","d83cdffd","d83cdffe","d83cdfff"],l=[65039,8205];function c(y){var I=y.split(","),M,j=I.length,O=[];for(M=0;M<j;M+=1)I[M]!=="sans-serif"&&I[M]!=="monospace"&&O.push(I[M]);return O.join(",")}function u(y,I){var M=createTag("span");M.setAttribute("aria-hidden",!0),M.style.fontFamily=I;var j=createTag("span");j.innerText="giItT1WQy@!-/#",M.style.position="absolute",M.style.left="-10000px",M.style.top="-10000px",M.style.fontSize="300px",M.style.fontVariant="normal",M.style.fontStyle="normal",M.style.fontWeight="normal",M.style.letterSpacing="0",M.appendChild(j),document.body.appendChild(M);var O=j.offsetWidth;return j.style.fontFamily=c(y)+", "+I,{node:j,w:O,parent:M}}function d(){var y,I=this.fonts.length,M,j,O=I;for(y=0;y<I;y+=1)this.fonts[y].loaded?O-=1:this.fonts[y].fOrigin==="n"||this.fonts[y].origin===0?this.fonts[y].loaded=!0:(M=this.fonts[y].monoCase.node,j=this.fonts[y].monoCase.w,M.offsetWidth!==j?(O-=1,this.fonts[y].loaded=!0):(M=this.fonts[y].sansCase.node,j=this.fonts[y].sansCase.w,M.offsetWidth!==j&&(O-=1,this.fonts[y].loaded=!0)),this.fonts[y].loaded&&(this.fonts[y].sansCase.parent.parentNode.removeChild(this.fonts[y].sansCase.parent),this.fonts[y].monoCase.parent.parentNode.removeChild(this.fonts[y].monoCase.parent)));O!==0&&Date.now()-this.initTime<e?setTimeout(this.checkLoadedFontsBinded,20):setTimeout(this.setIsLoadedBinded,10)}function m(y,I){var M=document.body&&I?"svg":"canvas",j,O=getFontProperties(y);if(M==="svg"){var L=createNS("text");L.style.fontSize="100px",L.setAttribute("font-family",y.fFamily),L.setAttribute("font-style",O.style),L.setAttribute("font-weight",O.weight),L.textContent="1",y.fClass?(L.style.fontFamily="inherit",L.setAttribute("class",y.fClass)):L.style.fontFamily=y.fFamily,I.appendChild(L),j=L}else{var N=new OffscreenCanvas(500,500).getContext("2d");N.font=O.style+" "+O.weight+" 100px "+y.fFamily,j=N}function q(V){return M==="svg"?(j.textContent=V,j.getComputedTextLength()):j.measureText(V).width}return{measureText:q}}function E(y,I){if(!y){this.isLoaded=!0;return}if(this.chars){this.isLoaded=!0,this.fonts=y.list;return}if(!document.body){this.isLoaded=!0,y.list.forEach(function(R){R.helper=m(R),R.cache={}}),this.fonts=y.list;return}var M=y.list,j,O=M.length,L=O;for(j=0;j<O;j+=1){var N=!0,q,V;if(M[j].loaded=!1,M[j].monoCase=u(M[j].fFamily,"monospace"),M[j].sansCase=u(M[j].fFamily,"sans-serif"),!M[j].fPath)M[j].loaded=!0,L-=1;else if(M[j].fOrigin==="p"||M[j].origin===3){if(q=document.querySelectorAll('style[f-forigin="p"][f-family="'+M[j].fFamily+'"], style[f-origin="3"][f-family="'+M[j].fFamily+'"]'),q.length>0&&(N=!1),N){var Q=createTag("style");Q.setAttribute("f-forigin",M[j].fOrigin),Q.setAttribute("f-origin",M[j].origin),Q.setAttribute("f-family",M[j].fFamily),Q.type="text/css",Q.innerText="@font-face {font-family: "+M[j].fFamily+"; font-style: normal; src: url('"+M[j].fPath+"');}",I.appendChild(Q)}}else if(M[j].fOrigin==="g"||M[j].origin===1){for(q=document.querySelectorAll('link[f-forigin="g"], link[f-origin="1"]'),V=0;V<q.length;V+=1)q[V].href.indexOf(M[j].fPath)!==-1&&(N=!1);if(N){var W=createTag("link");W.setAttribute("f-forigin",M[j].fOrigin),W.setAttribute("f-origin",M[j].origin),W.type="text/css",W.rel="stylesheet",W.href=M[j].fPath,document.body.appendChild(W)}}else if(M[j].fOrigin==="t"||M[j].origin===2){for(q=document.querySelectorAll('script[f-forigin="t"], script[f-origin="2"]'),V=0;V<q.length;V+=1)M[j].fPath===q[V].src&&(N=!1);if(N){var G=createTag("link");G.setAttribute("f-forigin",M[j].fOrigin),G.setAttribute("f-origin",M[j].origin),G.setAttribute("rel","stylesheet"),G.setAttribute("href",M[j].fPath),I.appendChild(G)}}M[j].helper=m(M[j],I),M[j].cache={},this.fonts.push(M[j])}L===0?this.isLoaded=!0:setTimeout(this.checkLoadedFonts.bind(this),100)}function C(y){if(!!y){this.chars||(this.chars=[]);var I,M=y.length,j,O=this.chars.length,L;for(I=0;I<M;I+=1){for(j=0,L=!1;j<O;)this.chars[j].style===y[I].style&&this.chars[j].fFamily===y[I].fFamily&&this.chars[j].ch===y[I].ch&&(L=!0),j+=1;L||(this.chars.push(y[I]),O+=1)}}}function _(y,I,M){for(var j=0,O=this.chars.length;j<O;){if(this.chars[j].ch===y&&this.chars[j].style===I&&this.chars[j].fFamily===M)return this.chars[j];j+=1}return(typeof y=="string"&&y.charCodeAt(0)!==13||!y)&&console&&console.warn&&!this._warned&&(this._warned=!0,console.warn("Missing character from exported characters list: ",y,I,M)),r}function A(y,I,M){var j=this.getFontByName(I),O=y.charCodeAt(0);if(!j.cache[O+1]){var L=j.helper;if(y===" "){var N=L.measureText("|"+y+"|"),q=L.measureText("||");j.cache[O+1]=(N-q)/100}else j.cache[O+1]=L.measureText(y)/100}return j.cache[O+1]*M}function T(y){for(var I=0,M=this.fonts.length;I<M;){if(this.fonts[I].fName===y)return this.fonts[I];I+=1}return this.fonts[0]}function S(y,I){var M=y.toString(16)+I.toString(16);return a.indexOf(M)!==-1}function P(y,I){return I?y===l[0]&&I===l[1]:y===l[1]}function k(y){return n.indexOf(y)!==-1}function x(){this.isLoaded=!0}var b=function(){this.fonts=[],this.chars=null,this.typekitLoaded=0,this.isLoaded=!1,this._warned=!1,this.initTime=Date.now(),this.setIsLoadedBinded=this.setIsLoaded.bind(this),this.checkLoadedFontsBinded=this.checkLoadedFonts.bind(this)};b.isModifier=S,b.isZeroWidthJoiner=P,b.isCombinedCharacter=k;var g={addChars:C,addFonts:E,getCharData:_,getFontByName:T,measureText:A,checkLoadedFonts:d,setIsLoaded:x};return b.prototype=g,b}();function RenderableElement(){}RenderableElement.prototype={initRenderable:function(){this.isInRange=!1,this.hidden=!1,this.isTransparent=!1,this.renderableComponents=[]},addRenderableComponent:function(r){this.renderableComponents.indexOf(r)===-1&&this.renderableComponents.push(r)},removeRenderableComponent:function(r){this.renderableComponents.indexOf(r)!==-1&&this.renderableComponents.splice(this.renderableComponents.indexOf(r),1)},prepareRenderableFrame:function(r){this.checkLayerLimits(r)},checkTransparency:function(){this.finalTransform.mProp.o.v<=0?!this.isTransparent&&this.globalData.renderConfig.hideOnTransparent&&(this.isTransparent=!0,this.hide()):this.isTransparent&&(this.isTransparent=!1,this.show())},checkLayerLimits:function(r){this.data.ip-this.data.st<=r&&this.data.op-this.data.st>r?this.isInRange!==!0&&(this.globalData._mdf=!0,this._mdf=!0,this.isInRange=!0,this.show()):this.isInRange!==!1&&(this.globalData._mdf=!0,this.isInRange=!1,this.hide())},renderRenderable:function(){var r,n=this.renderableComponents.length;for(r=0;r<n;r+=1)this.renderableComponents[r].renderFrame(this._isFirstFrame)},sourceRectAtTime:function(){return{top:0,left:0,width:100,height:100}},getLayerSize:function(){return this.data.ty===5?{w:this.data.textData.width,h:this.data.textData.height}:{w:this.data.width,h:this.data.height}}};var MaskManagerInterface=function(){function e(n,a){this._mask=n,this._data=a}Object.defineProperty(e.prototype,"maskPath",{get:function(){return this._mask.prop.k&&this._mask.prop.getValue(),this._mask.prop}}),Object.defineProperty(e.prototype,"maskOpacity",{get:function(){return this._mask.op.k&&this._mask.op.getValue(),this._mask.op.v*100}});var r=function(a){var l=createSizedArray(a.viewData.length),c,u=a.viewData.length;for(c=0;c<u;c+=1)l[c]=new e(a.viewData[c],a.masksProperties[c]);var d=function(E){for(c=0;c<u;){if(a.masksProperties[c].nm===E)return l[c];c+=1}return null};return d};return r}(),ExpressionPropertyInterface=function(){var e={pv:0,v:0,mult:1},r={pv:[0,0,0],v:[0,0,0],mult:1};function n(u,d,m){Object.defineProperty(u,"velocity",{get:function(){return d.getVelocityAtTime(d.comp.currentFrame)}}),u.numKeys=d.keyframes?d.keyframes.length:0,u.key=function(E){if(!u.numKeys)return 0;var C="";"s"in d.keyframes[E-1]?C=d.keyframes[E-1].s:"e"in d.keyframes[E-2]?C=d.keyframes[E-2].e:C=d.keyframes[E-2].s;var _=m==="unidimensional"?new Number(C):Object.assign({},C);return _.time=d.keyframes[E-1].t/d.elem.comp.globalData.frameRate,_.value=m==="unidimensional"?C[0]:C,_},u.valueAtTime=d.getValueAtTime,u.speedAtTime=d.getSpeedAtTime,u.velocityAtTime=d.getVelocityAtTime,u.propertyGroup=d.propertyGroup}function a(u){(!u||!("pv"in u))&&(u=e);var d=1/u.mult,m=u.pv*d,E=new Number(m);return E.value=m,n(E,u,"unidimensional"),function(){return u.k&&u.getValue(),m=u.v*d,E.value!==m&&(E=new Number(m),E.value=m,n(E,u,"unidimensional")),E}}function l(u){(!u||!("pv"in u))&&(u=r);var d=1/u.mult,m=u.data&&u.data.l||u.pv.length,E=createTypedArray("float32",m),C=createTypedArray("float32",m);return E.value=C,n(E,u,"multidimensional"),function(){u.k&&u.getValue();for(var _=0;_<m;_+=1)C[_]=u.v[_]*d,E[_]=C[_];return E}}function c(){return e}return function(u){return u?u.propType==="unidimensional"?a(u):l(u):c}}(),TransformExpressionInterface=function(){return function(e){function r(u){switch(u){case"scale":case"Scale":case"ADBE Scale":case 6:return r.scale;case"rotation":case"Rotation":case"ADBE Rotation":case"ADBE Rotate Z":case 10:return r.rotation;case"ADBE Rotate X":return r.xRotation;case"ADBE Rotate Y":return r.yRotation;case"position":case"Position":case"ADBE Position":case 2:return r.position;case"ADBE Position_0":return r.xPosition;case"ADBE Position_1":return r.yPosition;case"ADBE Position_2":return r.zPosition;case"anchorPoint":case"AnchorPoint":case"Anchor Point":case"ADBE AnchorPoint":case 1:return r.anchorPoint;case"opacity":case"Opacity":case 11:return r.opacity;default:return null}}Object.defineProperty(r,"rotation",{get:ExpressionPropertyInterface(e.r||e.rz)}),Object.defineProperty(r,"zRotation",{get:ExpressionPropertyInterface(e.rz||e.r)}),Object.defineProperty(r,"xRotation",{get:ExpressionPropertyInterface(e.rx)}),Object.defineProperty(r,"yRotation",{get:ExpressionPropertyInterface(e.ry)}),Object.defineProperty(r,"scale",{get:ExpressionPropertyInterface(e.s)});var n,a,l,c;return e.p?c=ExpressionPropertyInterface(e.p):(n=ExpressionPropertyInterface(e.px),a=ExpressionPropertyInterface(e.py),e.pz&&(l=ExpressionPropertyInterface(e.pz))),Object.defineProperty(r,"position",{get:function(){return e.p?c():[n(),a(),l?l():0]}}),Object.defineProperty(r,"xPosition",{get:ExpressionPropertyInterface(e.px)}),Object.defineProperty(r,"yPosition",{get:ExpressionPropertyInterface(e.py)}),Object.defineProperty(r,"zPosition",{get:ExpressionPropertyInterface(e.pz)}),Object.defineProperty(r,"anchorPoint",{get:ExpressionPropertyInterface(e.a)}),Object.defineProperty(r,"opacity",{get:ExpressionPropertyInterface(e.o)}),Object.defineProperty(r,"skew",{get:ExpressionPropertyInterface(e.sk)}),Object.defineProperty(r,"skewAxis",{get:ExpressionPropertyInterface(e.sa)}),Object.defineProperty(r,"orientation",{get:ExpressionPropertyInterface(e.or)}),r}}(),LayerExpressionInterface=function(){function e(E){var C=new Matrix;if(E!==void 0){var _=this._elem.finalTransform.mProp.getValueAtTime(E);_.clone(C)}else{var A=this._elem.finalTransform.mProp;A.applyToMatrix(C)}return C}function r(E,C){var _=this.getMatrix(C);return _.props[12]=0,_.props[13]=0,_.props[14]=0,this.applyPoint(_,E)}function n(E,C){var _=this.getMatrix(C);return this.applyPoint(_,E)}function a(E,C){var _=this.getMatrix(C);return _.props[12]=0,_.props[13]=0,_.props[14]=0,this.invertPoint(_,E)}function l(E,C){var _=this.getMatrix(C);return this.invertPoint(_,E)}function c(E,C){if(this._elem.hierarchy&&this._elem.hierarchy.length){var _,A=this._elem.hierarchy.length;for(_=0;_<A;_+=1)this._elem.hierarchy[_].finalTransform.mProp.applyToMatrix(E)}return E.applyToPointArray(C[0],C[1],C[2]||0)}function u(E,C){if(this._elem.hierarchy&&this._elem.hierarchy.length){var _,A=this._elem.hierarchy.length;for(_=0;_<A;_+=1)this._elem.hierarchy[_].finalTransform.mProp.applyToMatrix(E)}return E.inversePoint(C)}function d(E){var C=new Matrix;if(C.reset(),this._elem.finalTransform.mProp.applyToMatrix(C),this._elem.hierarchy&&this._elem.hierarchy.length){var _,A=this._elem.hierarchy.length;for(_=0;_<A;_+=1)this._elem.hierarchy[_].finalTransform.mProp.applyToMatrix(C);return C.inversePoint(E)}return C.inversePoint(E)}function m(){return[1,1,1,1]}return function(E){var C;function _(P){T.mask=new MaskManagerInterface(P,E)}function A(P){T.effect=P}function T(P){switch(P){case"ADBE Root Vectors Group":case"Contents":case 2:return T.shapeInterface;case 1:case 6:case"Transform":case"transform":case"ADBE Transform Group":return C;case 4:case"ADBE Effect Parade":case"effects":case"Effects":return T.effect;case"ADBE Text Properties":return T.textInterface;default:return null}}T.getMatrix=e,T.invertPoint=u,T.applyPoint=c,T.toWorld=n,T.toWorldVec=r,T.fromWorld=l,T.fromWorldVec=a,T.toComp=n,T.fromComp=d,T.sampleImage=m,T.sourceRectAtTime=E.sourceRectAtTime.bind(E),T._elem=E,C=TransformExpressionInterface(E.finalTransform.mProp);var S=getDescriptor(C,"anchorPoint");return Object.defineProperties(T,{hasParent:{get:function(){return E.hierarchy.length}},parent:{get:function(){return E.hierarchy[0].layerInterface}},rotation:getDescriptor(C,"rotation"),scale:getDescriptor(C,"scale"),position:getDescriptor(C,"position"),opacity:getDescriptor(C,"opacity"),anchorPoint:S,anchor_point:S,transform:{get:function(){return C}},active:{get:function(){return E.isInRange}}}),T.startTime=E.data.st,T.index=E.data.ind,T.source=E.data.refId,T.height=E.data.ty===0?E.data.h:100,T.width=E.data.ty===0?E.data.w:100,T.inPoint=E.data.ip/E.comp.globalData.frameRate,T.outPoint=E.data.op/E.comp.globalData.frameRate,T._name=E.data.nm,T.registerMaskInterface=_,T.registerEffectsInterface=A,T}}(),propertyGroupFactory=function(){return function(e,r){return function(n){return n=n===void 0?1:n,n<=0?e:r(n-1)}}}(),PropertyInterface=function(){return function(e,r){var n={_name:e};function a(l){return l=l===void 0?1:l,l<=0?n:r(l-1)}return a}}(),EffectsExpressionInterface=function(){var e={createEffectsInterface:r};function r(l,c){if(l.effectsManager){var u=[],d=l.data.ef,m,E=l.effectsManager.effectElements.length;for(m=0;m<E;m+=1)u.push(n(d[m],l.effectsManager.effectElements[m],c,l));var C=l.data.ef||[],_=function(T){for(m=0,E=C.length;m<E;){if(T===C[m].nm||T===C[m].mn||T===C[m].ix)return u[m];m+=1}return null};return Object.defineProperty(_,"numProperties",{get:function(){return C.length}}),_}return null}function n(l,c,u,d){function m(T){for(var S=l.ef,P=0,k=S.length;P<k;){if(T===S[P].nm||T===S[P].mn||T===S[P].ix)return S[P].ty===5?C[P]:C[P]();P+=1}throw new Error}var E=propertyGroupFactory(m,u),C=[],_,A=l.ef.length;for(_=0;_<A;_+=1)l.ef[_].ty===5?C.push(n(l.ef[_],c.effectElements[_],c.effectElements[_].propertyGroup,d)):C.push(a(c.effectElements[_],l.ef[_].ty,d,E));return l.mn==="ADBE Color Control"&&Object.defineProperty(m,"color",{get:function(){return C[0]()}}),Object.defineProperties(m,{numProperties:{get:function(){return l.np}},_name:{value:l.nm},propertyGroup:{value:E}}),m.enabled=l.en!==0,m.active=m.enabled,m}function a(l,c,u,d){var m=ExpressionPropertyInterface(l.p);function E(){return c===10?u.comp.compInterface(l.p.v):m()}return l.p.setGroupProperty&&l.p.setGroupProperty(PropertyInterface("",d)),E}return e}(),CompExpressionInterface=function(){return function(e){function r(n){for(var a=0,l=e.layers.length;a<l;){if(e.layers[a].nm===n||e.layers[a].ind===n)return e.elements[a].layerInterface;a+=1}return null}return Object.defineProperty(r,"_name",{value:e.data.nm}),r.layer=r,r.pixelAspect=1,r.height=e.data.h||e.globalData.compSize.h,r.width=e.data.w||e.globalData.compSize.w,r.pixelAspect=1,r.frameDuration=1/e.globalData.frameRate,r.displayStartTime=0,r.numLayers=e.layers.length,r}}(),ShapePathInterface=function(){return function(r,n,a){var l=n.sh;function c(d){return d==="Shape"||d==="shape"||d==="Path"||d==="path"||d==="ADBE Vector Shape"||d===2?c.path:null}var u=propertyGroupFactory(c,a);return l.setGroupProperty(PropertyInterface("Path",u)),Object.defineProperties(c,{path:{get:function(){return l.k&&l.getValue(),l}},shape:{get:function(){return l.k&&l.getValue(),l}},_name:{value:r.nm},ix:{value:r.ix},propertyIndex:{value:r.ix},mn:{value:r.mn},propertyGroup:{value:a}}),c}}(),ShapeExpressionInterface=function(){function e(S,P,k){var x=[],b,g=S?S.length:0;for(b=0;b<g;b+=1)S[b].ty==="gr"?x.push(n(S[b],P[b],k)):S[b].ty==="fl"?x.push(a(S[b],P[b],k)):S[b].ty==="st"?x.push(u(S[b],P[b],k)):S[b].ty==="tm"?x.push(d(S[b],P[b],k)):S[b].ty==="tr"||(S[b].ty==="el"?x.push(E(S[b],P[b],k)):S[b].ty==="sr"?x.push(C(S[b],P[b],k)):S[b].ty==="sh"?x.push(ShapePathInterface(S[b],P[b],k)):S[b].ty==="rc"?x.push(_(S[b],P[b],k)):S[b].ty==="rd"?x.push(A(S[b],P[b],k)):S[b].ty==="rp"?x.push(T(S[b],P[b],k)):S[b].ty==="gf"?x.push(l(S[b],P[b],k)):x.push(c(S[b],P[b])));return x}function r(S,P,k){var x,b=function(I){for(var M=0,j=x.length;M<j;){if(x[M]._name===I||x[M].mn===I||x[M].propertyIndex===I||x[M].ix===I||x[M].ind===I)return x[M];M+=1}return typeof I=="number"?x[I-1]:null};b.propertyGroup=propertyGroupFactory(b,k),x=e(S.it,P.it,b.propertyGroup),b.numProperties=x.length;var g=m(S.it[S.it.length-1],P.it[P.it.length-1],b.propertyGroup);return b.transform=g,b.propertyIndex=S.cix,b._name=S.nm,b}function n(S,P,k){var x=function(I){switch(I){case"ADBE Vectors Group":case"Contents":case 2:return x.content;default:return x.transform}};x.propertyGroup=propertyGroupFactory(x,k);var b=r(S,P,x.propertyGroup),g=m(S.it[S.it.length-1],P.it[P.it.length-1],x.propertyGroup);return x.content=b,x.transform=g,Object.defineProperty(x,"_name",{get:function(){return S.nm}}),x.numProperties=S.np,x.propertyIndex=S.ix,x.nm=S.nm,x.mn=S.mn,x}function a(S,P,k){function x(b){return b==="Color"||b==="color"?x.color:b==="Opacity"||b==="opacity"?x.opacity:null}return Object.defineProperties(x,{color:{get:ExpressionPropertyInterface(P.c)},opacity:{get:ExpressionPropertyInterface(P.o)},_name:{value:S.nm},mn:{value:S.mn}}),P.c.setGroupProperty(PropertyInterface("Color",k)),P.o.setGroupProperty(PropertyInterface("Opacity",k)),x}function l(S,P,k){function x(b){return b==="Start Point"||b==="start point"?x.startPoint:b==="End Point"||b==="end point"?x.endPoint:b==="Opacity"||b==="opacity"?x.opacity:null}return Object.defineProperties(x,{startPoint:{get:ExpressionPropertyInterface(P.s)},endPoint:{get:ExpressionPropertyInterface(P.e)},opacity:{get:ExpressionPropertyInterface(P.o)},type:{get:function(){return"a"}},_name:{value:S.nm},mn:{value:S.mn}}),P.s.setGroupProperty(PropertyInterface("Start Point",k)),P.e.setGroupProperty(PropertyInterface("End Point",k)),P.o.setGroupProperty(PropertyInterface("Opacity",k)),x}function c(){function S(){return null}return S}function u(S,P,k){var x=propertyGroupFactory(j,k),b=propertyGroupFactory(M,x);function g(O){Object.defineProperty(M,S.d[O].nm,{get:ExpressionPropertyInterface(P.d.dataProps[O].p)})}var y,I=S.d?S.d.length:0,M={};for(y=0;y<I;y+=1)g(y),P.d.dataProps[y].p.setGroupProperty(b);function j(O){return O==="Color"||O==="color"?j.color:O==="Opacity"||O==="opacity"?j.opacity:O==="Stroke Width"||O==="stroke width"?j.strokeWidth:null}return Object.defineProperties(j,{color:{get:ExpressionPropertyInterface(P.c)},opacity:{get:ExpressionPropertyInterface(P.o)},strokeWidth:{get:ExpressionPropertyInterface(P.w)},dash:{get:function(){return M}},_name:{value:S.nm},mn:{value:S.mn}}),P.c.setGroupProperty(PropertyInterface("Color",x)),P.o.setGroupProperty(PropertyInterface("Opacity",x)),P.w.setGroupProperty(PropertyInterface("Stroke Width",x)),j}function d(S,P,k){function x(g){return g===S.e.ix||g==="End"||g==="end"?x.end:g===S.s.ix?x.start:g===S.o.ix?x.offset:null}var b=propertyGroupFactory(x,k);return x.propertyIndex=S.ix,P.s.setGroupProperty(PropertyInterface("Start",b)),P.e.setGroupProperty(PropertyInterface("End",b)),P.o.setGroupProperty(PropertyInterface("Offset",b)),x.propertyIndex=S.ix,x.propertyGroup=k,Object.defineProperties(x,{start:{get:ExpressionPropertyInterface(P.s)},end:{get:ExpressionPropertyInterface(P.e)},offset:{get:ExpressionPropertyInterface(P.o)},_name:{value:S.nm}}),x.mn=S.mn,x}function m(S,P,k){function x(g){return S.a.ix===g||g==="Anchor Point"?x.anchorPoint:S.o.ix===g||g==="Opacity"?x.opacity:S.p.ix===g||g==="Position"?x.position:S.r.ix===g||g==="Rotation"||g==="ADBE Vector Rotation"?x.rotation:S.s.ix===g||g==="Scale"?x.scale:S.sk&&S.sk.ix===g||g==="Skew"?x.skew:S.sa&&S.sa.ix===g||g==="Skew Axis"?x.skewAxis:null}var b=propertyGroupFactory(x,k);return P.transform.mProps.o.setGroupProperty(PropertyInterface("Opacity",b)),P.transform.mProps.p.setGroupProperty(PropertyInterface("Position",b)),P.transform.mProps.a.setGroupProperty(PropertyInterface("Anchor Point",b)),P.transform.mProps.s.setGroupProperty(PropertyInterface("Scale",b)),P.transform.mProps.r.setGroupProperty(PropertyInterface("Rotation",b)),P.transform.mProps.sk&&(P.transform.mProps.sk.setGroupProperty(PropertyInterface("Skew",b)),P.transform.mProps.sa.setGroupProperty(PropertyInterface("Skew Angle",b))),P.transform.op.setGroupProperty(PropertyInterface("Opacity",b)),Object.defineProperties(x,{opacity:{get:ExpressionPropertyInterface(P.transform.mProps.o)},position:{get:ExpressionPropertyInterface(P.transform.mProps.p)},anchorPoint:{get:ExpressionPropertyInterface(P.transform.mProps.a)},scale:{get:ExpressionPropertyInterface(P.transform.mProps.s)},rotation:{get:ExpressionPropertyInterface(P.transform.mProps.r)},skew:{get:ExpressionPropertyInterface(P.transform.mProps.sk)},skewAxis:{get:ExpressionPropertyInterface(P.transform.mProps.sa)},_name:{value:S.nm}}),x.ty="tr",x.mn=S.mn,x.propertyGroup=k,x}function E(S,P,k){function x(y){return S.p.ix===y?x.position:S.s.ix===y?x.size:null}var b=propertyGroupFactory(x,k);x.propertyIndex=S.ix;var g=P.sh.ty==="tm"?P.sh.prop:P.sh;return g.s.setGroupProperty(PropertyInterface("Size",b)),g.p.setGroupProperty(PropertyInterface("Position",b)),Object.defineProperties(x,{size:{get:ExpressionPropertyInterface(g.s)},position:{get:ExpressionPropertyInterface(g.p)},_name:{value:S.nm}}),x.mn=S.mn,x}function C(S,P,k){function x(y){return S.p.ix===y?x.position:S.r.ix===y?x.rotation:S.pt.ix===y?x.points:S.or.ix===y||y==="ADBE Vector Star Outer Radius"?x.outerRadius:S.os.ix===y?x.outerRoundness:S.ir&&(S.ir.ix===y||y==="ADBE Vector Star Inner Radius")?x.innerRadius:S.is&&S.is.ix===y?x.innerRoundness:null}var b=propertyGroupFactory(x,k),g=P.sh.ty==="tm"?P.sh.prop:P.sh;return x.propertyIndex=S.ix,g.or.setGroupProperty(PropertyInterface("Outer Radius",b)),g.os.setGroupProperty(PropertyInterface("Outer Roundness",b)),g.pt.setGroupProperty(PropertyInterface("Points",b)),g.p.setGroupProperty(PropertyInterface("Position",b)),g.r.setGroupProperty(PropertyInterface("Rotation",b)),S.ir&&(g.ir.setGroupProperty(PropertyInterface("Inner Radius",b)),g.is.setGroupProperty(PropertyInterface("Inner Roundness",b))),Object.defineProperties(x,{position:{get:ExpressionPropertyInterface(g.p)},rotation:{get:ExpressionPropertyInterface(g.r)},points:{get:ExpressionPropertyInterface(g.pt)},outerRadius:{get:ExpressionPropertyInterface(g.or)},outerRoundness:{get:ExpressionPropertyInterface(g.os)},innerRadius:{get:ExpressionPropertyInterface(g.ir)},innerRoundness:{get:ExpressionPropertyInterface(g.is)},_name:{value:S.nm}}),x.mn=S.mn,x}function _(S,P,k){function x(y){return S.p.ix===y?x.position:S.r.ix===y?x.roundness:S.s.ix===y||y==="Size"||y==="ADBE Vector Rect Size"?x.size:null}var b=propertyGroupFactory(x,k),g=P.sh.ty==="tm"?P.sh.prop:P.sh;return x.propertyIndex=S.ix,g.p.setGroupProperty(PropertyInterface("Position",b)),g.s.setGroupProperty(PropertyInterface("Size",b)),g.r.setGroupProperty(PropertyInterface("Rotation",b)),Object.defineProperties(x,{position:{get:ExpressionPropertyInterface(g.p)},roundness:{get:ExpressionPropertyInterface(g.r)},size:{get:ExpressionPropertyInterface(g.s)},_name:{value:S.nm}}),x.mn=S.mn,x}function A(S,P,k){function x(y){return S.r.ix===y||y==="Round Corners 1"?x.radius:null}var b=propertyGroupFactory(x,k),g=P;return x.propertyIndex=S.ix,g.rd.setGroupProperty(PropertyInterface("Radius",b)),Object.defineProperties(x,{radius:{get:ExpressionPropertyInterface(g.rd)},_name:{value:S.nm}}),x.mn=S.mn,x}function T(S,P,k){function x(y){return S.c.ix===y||y==="Copies"?x.copies:S.o.ix===y||y==="Offset"?x.offset:null}var b=propertyGroupFactory(x,k),g=P;return x.propertyIndex=S.ix,g.c.setGroupProperty(PropertyInterface("Copies",b)),g.o.setGroupProperty(PropertyInterface("Offset",b)),Object.defineProperties(x,{copies:{get:ExpressionPropertyInterface(g.c)},offset:{get:ExpressionPropertyInterface(g.o)},_name:{value:S.nm}}),x.mn=S.mn,x}return function(S,P,k){var x;function b(y){if(typeof y=="number")return y=y===void 0?1:y,y===0?k:x[y-1];for(var I=0,M=x.length;I<M;){if(x[I]._name===y)return x[I];I+=1}return null}function g(){return k}return b.propertyGroup=propertyGroupFactory(b,g),x=e(S,P,b.propertyGroup),b.numProperties=x.length,b._name="Contents",b}}(),TextExpressionInterface=function(){return function(e){var r,n;function a(l){switch(l){case"ADBE Text Document":return a.sourceText;default:return null}}return Object.defineProperty(a,"sourceText",{get:function(){e.textProperty.getValue();var c=e.textProperty.currentData.t;return c!==r&&(e.textProperty.currentData.t=r,n=new String(c),n.value=c||new String(c)),n}}),a}}(),getBlendMode=function(){var e={0:"source-over",1:"multiply",2:"screen",3:"overlay",4:"darken",5:"lighten",6:"color-dodge",7:"color-burn",8:"hard-light",9:"soft-light",10:"difference",11:"exclusion",12:"hue",13:"saturation",14:"color",15:"luminosity"};return function(r){return e[r]||""}}();function SliderEffect(e,r,n){this.p=PropertyFactory.getProp(r,e.v,0,0,n)}function AngleEffect(e,r,n){this.p=PropertyFactory.getProp(r,e.v,0,0,n)}function ColorEffect(e,r,n){this.p=PropertyFactory.getProp(r,e.v,1,0,n)}function PointEffect(e,r,n){this.p=PropertyFactory.getProp(r,e.v,1,0,n)}function LayerIndexEffect(e,r,n){this.p=PropertyFactory.getProp(r,e.v,0,0,n)}function MaskIndexEffect(e,r,n){this.p=PropertyFactory.getProp(r,e.v,0,0,n)}function CheckboxEffect(e,r,n){this.p=PropertyFactory.getProp(r,e.v,0,0,n)}function NoValueEffect(){this.p={}}function EffectsManager(e,r){var n=e.ef||[];this.effectElements=[];var a,l=n.length,c;for(a=0;a<l;a+=1)c=new GroupEffect(n[a],r),this.effectElements.push(c)}function GroupEffect(e,r){this.init(e,r)}extendPrototype([DynamicPropertyContainer],GroupEffect),GroupEffect.prototype.getValue=GroupEffect.prototype.iterateDynamicProperties,GroupEffect.prototype.init=function(e,r){this.data=e,this.effectElements=[],this.initDynamicPropertyContainer(r);var n,a=this.data.ef.length,l,c=this.data.ef;for(n=0;n<a;n+=1){switch(l=null,c[n].ty){case 0:l=new SliderEffect(c[n],r,this);break;case 1:l=new AngleEffect(c[n],r,this);break;case 2:l=new ColorEffect(c[n],r,this);break;case 3:l=new PointEffect(c[n],r,this);break;case 4:case 7:l=new CheckboxEffect(c[n],r,this);break;case 10:l=new LayerIndexEffect(c[n],r,this);break;case 11:l=new MaskIndexEffect(c[n],r,this);break;case 5:l=new EffectsManager(c[n],r,this);break;default:l=new NoValueEffect(c[n],r,this);break}l&&this.effectElements.push(l)}};function BaseElement(){}BaseElement.prototype={checkMasks:function(){if(!this.data.hasMask)return!1;for(var r=0,n=this.data.masksProperties.length;r<n;){if(this.data.masksProperties[r].mode!=="n"&&this.data.masksProperties[r].cl!==!1)return!0;r+=1}return!1},initExpressions:function(){this.layerInterface=LayerExpressionInterface(this),this.data.hasMask&&this.maskManager&&this.layerInterface.registerMaskInterface(this.maskManager);var r=EffectsExpressionInterface.createEffectsInterface(this,this.layerInterface);this.layerInterface.registerEffectsInterface(r),this.data.ty===0||this.data.xt?this.compInterface=CompExpressionInterface(this):this.data.ty===4?(this.layerInterface.shapeInterface=ShapeExpressionInterface(this.shapesData,this.itemsData,this.layerInterface),this.layerInterface.content=this.layerInterface.shapeInterface):this.data.ty===5&&(this.layerInterface.textInterface=TextExpressionInterface(this),this.layerInterface.text=this.layerInterface.textInterface)},setBlendMode:function(){var r=getBlendMode(this.data.bm),n=this.baseElement||this.layerElement;n.style["mix-blend-mode"]=r},initBaseData:function(r,n,a){this.globalData=n,this.comp=a,this.data=r,this.layerId=createElementID(),this.data.sr||(this.data.sr=1),this.effectsManager=new EffectsManager(this.data,this,this.dynamicProperties)},getType:function(){return this.type},sourceRectAtTime:function(){}};function FrameElement(){}FrameElement.prototype={initFrame:function(){this._isFirstFrame=!1,this.dynamicProperties=[],this._mdf=!1},prepareProperties:function(r,n){var a,l=this.dynamicProperties.length;for(a=0;a<l;a+=1)(n||this._isParent&&this.dynamicProperties[a].propType==="transform")&&(this.dynamicProperties[a].getValue(),this.dynamicProperties[a]._mdf&&(this.globalData._mdf=!0,this._mdf=!0))},addDynamicProperty:function(r){this.dynamicProperties.indexOf(r)===-1&&this.dynamicProperties.push(r)}};function _typeof$2(e){return typeof Symbol=="function"&&typeof Symbol.iterator=="symbol"?_typeof$2=function(n){return typeof n}:_typeof$2=function(n){return n&&typeof Symbol=="function"&&n.constructor===Symbol&&n!==Symbol.prototype?"symbol":typeof n},_typeof$2(e)}var FootageInterface=function(){var e=function(a){var l="",c=a.getFootageData();function u(){return l="",c=a.getFootageData(),d}function d(m){if(c[m])return l=m,c=c[m],_typeof$2(c)==="object"?d:c;var E=m.indexOf(l);if(E!==-1){var C=parseInt(m.substr(E+l.length),10);return c=c[C],_typeof$2(c)==="object"?d:c}return""}return u},r=function(a){function l(c){return c==="Outline"?l.outlineInterface():null}return l._name="Outline",l.outlineInterface=e(a),l};return function(n){function a(l){return l==="Data"?a.dataInterface:null}return a._name="Data",a.dataInterface=r(n),a}}();function FootageElement(e,r,n){this.initFrame(),this.initRenderable(),this.assetData=r.getAssetData(e.refId),this.footageData=r.imageLoader.getAsset(this.assetData),this.initBaseData(e,r,n)}FootageElement.prototype.prepareFrame=function(){},extendPrototype([RenderableElement,BaseElement,FrameElement],FootageElement),FootageElement.prototype.getBaseElement=function(){return null},FootageElement.prototype.renderFrame=function(){},FootageElement.prototype.destroy=function(){},FootageElement.prototype.initExpressions=function(){this.layerInterface=FootageInterface(this)},FootageElement.prototype.getFootageData=function(){return this.footageData};function AudioElement(e,r,n){this.initFrame(),this.initRenderable(),this.assetData=r.getAssetData(e.refId),this.initBaseData(e,r,n),this._isPlaying=!1,this._canPlay=!1;var a=this.globalData.getAssetsPath(this.assetData);this.audio=this.globalData.audioController.createAudio(a),this._currentTime=0,this.globalData.audioController.addAudio(this),this._volumeMultiplier=1,this._volume=1,this._previousVolume=null,this.tm=e.tm?PropertyFactory.getProp(this,e.tm,0,r.frameRate,this):{_placeholder:!0},this.lv=PropertyFactory.getProp(this,e.au&&e.au.lv?e.au.lv:{k:[100]},1,.01,this)}AudioElement.prototype.prepareFrame=function(e){if(this.prepareRenderableFrame(e,!0),this.prepareProperties(e,!0),this.tm._placeholder)this._currentTime=e/this.data.sr;else{var r=this.tm.v;this._currentTime=r}this._volume=this.lv.v[0];var n=this._volume*this._volumeMultiplier;this._previousVolume!==n&&(this._previousVolume=n,this.audio.volume(n))},extendPrototype([RenderableElement,BaseElement,FrameElement],AudioElement),AudioElement.prototype.renderFrame=function(){this.isInRange&&this._canPlay&&(this._isPlaying?(!this.audio.playing()||Math.abs(this._currentTime/this.globalData.frameRate-this.audio.seek())>.1)&&this.audio.seek(this._currentTime/this.globalData.frameRate):(this.audio.play(),this.audio.seek(this._currentTime/this.globalData.frameRate),this._isPlaying=!0))},AudioElement.prototype.show=function(){},AudioElement.prototype.hide=function(){this.audio.pause(),this._isPlaying=!1},AudioElement.prototype.pause=function(){this.audio.pause(),this._isPlaying=!1,this._canPlay=!1},AudioElement.prototype.resume=function(){this._canPlay=!0},AudioElement.prototype.setRate=function(e){this.audio.rate(e)},AudioElement.prototype.volume=function(e){this._volumeMultiplier=e,this._previousVolume=e*this._volume,this.audio.volume(this._previousVolume)},AudioElement.prototype.getBaseElement=function(){return null},AudioElement.prototype.destroy=function(){},AudioElement.prototype.sourceRectAtTime=function(){},AudioElement.prototype.initExpressions=function(){};function BaseRenderer(){}BaseRenderer.prototype.checkLayers=function(e){var r,n=this.layers.length,a;for(this.completeLayers=!0,r=n-1;r>=0;r-=1)this.elements[r]||(a=this.layers[r],a.ip-a.st<=e-this.layers[r].st&&a.op-a.st>e-this.layers[r].st&&this.buildItem(r)),this.completeLayers=this.elements[r]?this.completeLayers:!1;this.checkPendingElements()},BaseRenderer.prototype.createItem=function(e){switch(e.ty){case 2:return this.createImage(e);case 0:return this.createComp(e);case 1:return this.createSolid(e);case 3:return this.createNull(e);case 4:return this.createShape(e);case 5:return this.createText(e);case 6:return this.createAudio(e);case 13:return this.createCamera(e);case 15:return this.createFootage(e);default:return this.createNull(e)}},BaseRenderer.prototype.createCamera=function(){throw new Error("You're using a 3d camera. Try the html renderer.")},BaseRenderer.prototype.createAudio=function(e){return new AudioElement(e,this.globalData,this)},BaseRenderer.prototype.createFootage=function(e){return new FootageElement(e,this.globalData,this)},BaseRenderer.prototype.buildAllItems=function(){var e,r=this.layers.length;for(e=0;e<r;e+=1)this.buildItem(e);this.checkPendingElements()},BaseRenderer.prototype.includeLayers=function(e){this.completeLayers=!1;var r,n=e.length,a,l=this.layers.length;for(r=0;r<n;r+=1)for(a=0;a<l;){if(this.layers[a].id===e[r].id){this.layers[a]=e[r];break}a+=1}},BaseRenderer.prototype.setProjectInterface=function(e){this.globalData.projectInterface=e},BaseRenderer.prototype.initItems=function(){this.globalData.progressiveLoad||this.buildAllItems()},BaseRenderer.prototype.buildElementParenting=function(e,r,n){for(var a=this.elements,l=this.layers,c=0,u=l.length;c<u;)l[c].ind==r&&(!a[c]||a[c]===!0?(this.buildItem(c),this.addPendingElement(e)):(n.push(a[c]),a[c].setAsParent(),l[c].parent!==void 0?this.buildElementParenting(e,l[c].parent,n):e.setHierarchy(n))),c+=1},BaseRenderer.prototype.addPendingElement=function(e){this.pendingElements.push(e)},BaseRenderer.prototype.searchExtraCompositions=function(e){var r,n=e.length;for(r=0;r<n;r+=1)if(e[r].xt){var a=this.createComp(e[r]);a.initExpressions(),this.globalData.projectInterface.registerComposition(a)}},BaseRenderer.prototype.getElementByPath=function(e){var r=e.shift(),n;if(typeof r=="number")n=this.elements[r];else{var a,l=this.elements.length;for(a=0;a<l;a+=1)if(this.elements[a].data.nm===r){n=this.elements[a];break}}return e.length===0?n:n.getElementByPath(e)},BaseRenderer.prototype.setupGlobalData=function(e,r){this.globalData.fontManager=new FontManager,this.globalData.fontManager.addChars(e.chars),this.globalData.fontManager.addFonts(e.fonts,r),this.globalData.getAssetData=this.animationItem.getAssetData.bind(this.animationItem),this.globalData.getAssetsPath=this.animationItem.getAssetsPath.bind(this.animationItem),this.globalData.imageLoader=this.animationItem.imagePreloader,this.globalData.audioController=this.animationItem.audioController,this.globalData.frameId=0,this.globalData.frameRate=e.fr,this.globalData.nm=e.nm,this.globalData.compSize={w:e.w,h:e.h}};function TransformElement(){}TransformElement.prototype={initTransform:function(){this.finalTransform={mProp:this.data.ks?TransformPropertyFactory.getTransformProperty(this,this.data.ks,this):{o:0},_matMdf:!1,_opMdf:!1,mat:new Matrix},this.data.ao&&(this.finalTransform.mProp.autoOriented=!0),this.data.ty!==11},renderTransform:function(){if(this.finalTransform._opMdf=this.finalTransform.mProp.o._mdf||this._isFirstFrame,this.finalTransform._matMdf=this.finalTransform.mProp._mdf||this._isFirstFrame,this.hierarchy){var r,n=this.finalTransform.mat,a=0,l=this.hierarchy.length;if(!this.finalTransform._matMdf)for(;a<l;){if(this.hierarchy[a].finalTransform.mProp._mdf){this.finalTransform._matMdf=!0;break}a+=1}if(this.finalTransform._matMdf)for(r=this.finalTransform.mProp.v.props,n.cloneFromProps(r),a=0;a<l;a+=1)r=this.hierarchy[a].finalTransform.mProp.v.props,n.transform(r[0],r[1],r[2],r[3],r[4],r[5],r[6],r[7],r[8],r[9],r[10],r[11],r[12],r[13],r[14],r[15])}},globalToLocal:function(r){var n=[];n.push(this.finalTransform);for(var a=!0,l=this.comp;a;)l.finalTransform?(l.data.hasMask&&n.splice(0,0,l.finalTransform),l=l.comp):a=!1;var c,u=n.length,d;for(c=0;c<u;c+=1)d=n[c].mat.applyToPointArray(0,0,0),r=[r[0]-d[0],r[1]-d[1],0];return r},mHelper:new Matrix};function MaskElement(e,r,n){this.data=e,this.element=r,this.globalData=n,this.storedData=[],this.masksProperties=this.data.masksProperties||[],this.maskElement=null;var a=this.globalData.defs,l,c=this.masksProperties?this.masksProperties.length:0;this.viewData=createSizedArray(c),this.solidPath="";var u,d=this.masksProperties,m=0,E=[],C,_,A=createElementID(),T,S,P,k,x="clipPath",b="clip-path";for(l=0;l<c;l+=1)if((d[l].mode!=="a"&&d[l].mode!=="n"||d[l].inv||d[l].o.k!==100||d[l].o.x)&&(x="mask",b="mask"),(d[l].mode==="s"||d[l].mode==="i")&&m===0?(T=createNS("rect"),T.setAttribute("fill","#ffffff"),T.setAttribute("width",this.element.comp.data.w||0),T.setAttribute("height",this.element.comp.data.h||0),E.push(T)):T=null,u=createNS("path"),d[l].mode==="n")this.viewData[l]={op:PropertyFactory.getProp(this.element,d[l].o,0,.01,this.element),prop:ShapePropertyFactory.getShapeProp(this.element,d[l],3),elem:u,lastPath:""},a.appendChild(u);else{m+=1,u.setAttribute("fill",d[l].mode==="s"?"#000000":"#ffffff"),u.setAttribute("clip-rule","nonzero");var g;if(d[l].x.k!==0?(x="mask",b="mask",k=PropertyFactory.getProp(this.element,d[l].x,0,null,this.element),g=createElementID(),S=createNS("filter"),S.setAttribute("id",g),P=createNS("feMorphology"),P.setAttribute("operator","erode"),P.setAttribute("in","SourceGraphic"),P.setAttribute("radius","0"),S.appendChild(P),a.appendChild(S),u.setAttribute("stroke",d[l].mode==="s"?"#000000":"#ffffff")):(P=null,k=null),this.storedData[l]={elem:u,x:k,expan:P,lastPath:"",lastOperator:"",filterId:g,lastRadius:0},d[l].mode==="i"){_=E.length;var y=createNS("g");for(C=0;C<_;C+=1)y.appendChild(E[C]);var I=createNS("mask");I.setAttribute("mask-type","alpha"),I.setAttribute("id",A+"_"+m),I.appendChild(u),a.appendChild(I),y.setAttribute("mask","url("+getLocationHref()+"#"+A+"_"+m+")"),E.length=0,E.push(y)}else E.push(u);d[l].inv&&!this.solidPath&&(this.solidPath=this.createLayerSolidPath()),this.viewData[l]={elem:u,lastPath:"",op:PropertyFactory.getProp(this.element,d[l].o,0,.01,this.element),prop:ShapePropertyFactory.getShapeProp(this.element,d[l],3),invRect:T},this.viewData[l].prop.k||this.drawPath(d[l],this.viewData[l].prop.v,this.viewData[l])}for(this.maskElement=createNS(x),c=E.length,l=0;l<c;l+=1)this.maskElement.appendChild(E[l]);m>0&&(this.maskElement.setAttribute("id",A),this.element.maskedElement.setAttribute(b,"url("+getLocationHref()+"#"+A+")"),a.appendChild(this.maskElement)),this.viewData.length&&this.element.addRenderableComponent(this)}MaskElement.prototype.getMaskProperty=function(e){return this.viewData[e].prop},MaskElement.prototype.renderFrame=function(e){var r=this.element.finalTransform.mat,n,a=this.masksProperties.length;for(n=0;n<a;n+=1)if((this.viewData[n].prop._mdf||e)&&this.drawPath(this.masksProperties[n],this.viewData[n].prop.v,this.viewData[n]),(this.viewData[n].op._mdf||e)&&this.viewData[n].elem.setAttribute("fill-opacity",this.viewData[n].op.v),this.masksProperties[n].mode!=="n"&&(this.viewData[n].invRect&&(this.element.finalTransform.mProp._mdf||e)&&this.viewData[n].invRect.setAttribute("transform",r.getInverseMatrix().to2dCSS()),this.storedData[n].x&&(this.storedData[n].x._mdf||e))){var l=this.storedData[n].expan;this.storedData[n].x.v<0?(this.storedData[n].lastOperator!=="erode"&&(this.storedData[n].lastOperator="erode",this.storedData[n].elem.setAttribute("filter","url("+getLocationHref()+"#"+this.storedData[n].filterId+")")),l.setAttribute("radius",-this.storedData[n].x.v)):(this.storedData[n].lastOperator!=="dilate"&&(this.storedData[n].lastOperator="dilate",this.storedData[n].elem.setAttribute("filter",null)),this.storedData[n].elem.setAttribute("stroke-width",this.storedData[n].x.v*2))}},MaskElement.prototype.getMaskelement=function(){return this.maskElement},MaskElement.prototype.createLayerSolidPath=function(){var e="M0,0 ";return e+=" h"+this.globalData.compSize.w,e+=" v"+this.globalData.compSize.h,e+=" h-"+this.globalData.compSize.w,e+=" v-"+this.globalData.compSize.h+" ",e},MaskElement.prototype.drawPath=function(e,r,n){var a=" M"+r.v[0][0]+","+r.v[0][1],l,c;for(c=r._length,l=1;l<c;l+=1)a+=" C"+r.o[l-1][0]+","+r.o[l-1][1]+" "+r.i[l][0]+","+r.i[l][1]+" "+r.v[l][0]+","+r.v[l][1];if(r.c&&c>1&&(a+=" C"+r.o[l-1][0]+","+r.o[l-1][1]+" "+r.i[0][0]+","+r.i[0][1]+" "+r.v[0][0]+","+r.v[0][1]),n.lastPath!==a){var u="";n.elem&&(r.c&&(u=e.inv?this.solidPath+a:a),n.elem.setAttribute("d",u)),n.lastPath=a}},MaskElement.prototype.destroy=function(){this.element=null,this.globalData=null,this.maskElement=null,this.data=null,this.masksProperties=null};var filtersFactory=function(){var e={};e.createFilter=r,e.createAlphaToLuminanceFilter=n;function r(a,l){var c=createNS("filter");return c.setAttribute("id",a),l!==!0&&(c.setAttribute("filterUnits","objectBoundingBox"),c.setAttribute("x","0%"),c.setAttribute("y","0%"),c.setAttribute("width","100%"),c.setAttribute("height","100%")),c}function n(){var a=createNS("feColorMatrix");return a.setAttribute("type","matrix"),a.setAttribute("color-interpolation-filters","sRGB"),a.setAttribute("values","0 0 0 1 0  0 0 0 1 0  0 0 0 1 0  0 0 0 1 1"),a}return e}(),featureSupport=function(){var e={maskType:!0};return(/MSIE 10/i.test(navigator.userAgent)||/MSIE 9/i.test(navigator.userAgent)||/rv:11.0/i.test(navigator.userAgent)||/Edge\/\d./i.test(navigator.userAgent))&&(e.maskType=!1),e}();function SVGTintFilter(e,r){this.filterManager=r;var n=createNS("feColorMatrix");if(n.setAttribute("type","matrix"),n.setAttribute("color-interpolation-filters","linearRGB"),n.setAttribute("values","0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0 0 0 1 0"),n.setAttribute("result","f1"),e.appendChild(n),n=createNS("feColorMatrix"),n.setAttribute("type","matrix"),n.setAttribute("color-interpolation-filters","sRGB"),n.setAttribute("values","1 0 0 0 0 0 1 0 0 0 0 0 1 0 0 0 0 0 1 0"),n.setAttribute("result","f2"),e.appendChild(n),this.matrixFilter=n,r.effectElements[2].p.v!==100||r.effectElements[2].p.k){var a=createNS("feMerge");e.appendChild(a);var l;l=createNS("feMergeNode"),l.setAttribute("in","SourceGraphic"),a.appendChild(l),l=createNS("feMergeNode"),l.setAttribute("in","f2"),a.appendChild(l)}}SVGTintFilter.prototype.renderFrame=function(e){if(e||this.filterManager._mdf){var r=this.filterManager.effectElements[0].p.v,n=this.filterManager.effectElements[1].p.v,a=this.filterManager.effectElements[2].p.v/100;this.matrixFilter.setAttribute("values",n[0]-r[0]+" 0 0 0 "+r[0]+" "+(n[1]-r[1])+" 0 0 0 "+r[1]+" "+(n[2]-r[2])+" 0 0 0 "+r[2]+" 0 0 0 "+a+" 0")}};function SVGFillFilter(e,r){this.filterManager=r;var n=createNS("feColorMatrix");n.setAttribute("type","matrix"),n.setAttribute("color-interpolation-filters","sRGB"),n.setAttribute("values","1 0 0 0 0 0 1 0 0 0 0 0 1 0 0 0 0 0 1 0"),e.appendChild(n),this.matrixFilter=n}SVGFillFilter.prototype.renderFrame=function(e){if(e||this.filterManager._mdf){var r=this.filterManager.effectElements[2].p.v,n=this.filterManager.effectElements[6].p.v;this.matrixFilter.setAttribute("values","0 0 0 0 "+r[0]+" 0 0 0 0 "+r[1]+" 0 0 0 0 "+r[2]+" 0 0 0 "+n+" 0")}};function SVGStrokeEffect(e,r){this.initialized=!1,this.filterManager=r,this.elem=e,this.paths=[]}SVGStrokeEffect.prototype.initialize=function(){var e=this.elem.layerElement.children||this.elem.layerElement.childNodes,r,n,a,l;for(this.filterManager.effectElements[1].p.v===1?(l=this.elem.maskManager.masksProperties.length,a=0):(a=this.filterManager.effectElements[0].p.v-1,l=a+1),n=createNS("g"),n.setAttribute("fill","none"),n.setAttribute("stroke-linecap","round"),n.setAttribute("stroke-dashoffset",1),a;a<l;a+=1)r=createNS("path"),n.appendChild(r),this.paths.push({p:r,m:a});if(this.filterManager.effectElements[10].p.v===3){var c=createNS("mask"),u=createElementID();c.setAttribute("id",u),c.setAttribute("mask-type","alpha"),c.appendChild(n),this.elem.globalData.defs.appendChild(c);var d=createNS("g");for(d.setAttribute("mask","url("+getLocationHref()+"#"+u+")");e[0];)d.appendChild(e[0]);this.elem.layerElement.appendChild(d),this.masker=c,n.setAttribute("stroke","#fff")}else if(this.filterManager.effectElements[10].p.v===1||this.filterManager.effectElements[10].p.v===2){if(this.filterManager.effectElements[10].p.v===2)for(e=this.elem.layerElement.children||this.elem.layerElement.childNodes;e.length;)this.elem.layerElement.removeChild(e[0]);this.elem.layerElement.appendChild(n),this.elem.layerElement.removeAttribute("mask"),n.setAttribute("stroke","#fff")}this.initialized=!0,this.pathMasker=n},SVGStrokeEffect.prototype.renderFrame=function(e){this.initialized||this.initialize();var r,n=this.paths.length,a,l;for(r=0;r<n;r+=1)if(this.paths[r].m!==-1&&(a=this.elem.maskManager.viewData[this.paths[r].m],l=this.paths[r].p,(e||this.filterManager._mdf||a.prop._mdf)&&l.setAttribute("d",a.lastPath),e||this.filterManager.effectElements[9].p._mdf||this.filterManager.effectElements[4].p._mdf||this.filterManager.effectElements[7].p._mdf||this.filterManager.effectElements[8].p._mdf||a.prop._mdf)){var c;if(this.filterManager.effectElements[7].p.v!==0||this.filterManager.effectElements[8].p.v!==100){var u=Math.min(this.filterManager.effectElements[7].p.v,this.filterManager.effectElements[8].p.v)*.01,d=Math.max(this.filterManager.effectElements[7].p.v,this.filterManager.effectElements[8].p.v)*.01,m=l.getTotalLength();c="0 0 0 "+m*u+" ";var E=m*(d-u),C=1+this.filterManager.effectElements[4].p.v*2*this.filterManager.effectElements[9].p.v*.01,_=Math.floor(E/C),A;for(A=0;A<_;A+=1)c+="1 "+this.filterManager.effectElements[4].p.v*2*this.filterManager.effectElements[9].p.v*.01+" ";c+="0 "+m*10+" 0 0"}else c="1 "+this.filterManager.effectElements[4].p.v*2*this.filterManager.effectElements[9].p.v*.01;l.setAttribute("stroke-dasharray",c)}if((e||this.filterManager.effectElements[4].p._mdf)&&this.pathMasker.setAttribute("stroke-width",this.filterManager.effectElements[4].p.v*2),(e||this.filterManager.effectElements[6].p._mdf)&&this.pathMasker.setAttribute("opacity",this.filterManager.effectElements[6].p.v),(this.filterManager.effectElements[10].p.v===1||this.filterManager.effectElements[10].p.v===2)&&(e||this.filterManager.effectElements[3].p._mdf)){var T=this.filterManager.effectElements[3].p.v;this.pathMasker.setAttribute("stroke","rgb("+bmFloor(T[0]*255)+","+bmFloor(T[1]*255)+","+bmFloor(T[2]*255)+")")}};function SVGTritoneFilter(e,r){this.filterManager=r;var n=createNS("feColorMatrix");n.setAttribute("type","matrix"),n.setAttribute("color-interpolation-filters","linearRGB"),n.setAttribute("values","0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0 0 0 1 0"),n.setAttribute("result","f1"),e.appendChild(n);var a=createNS("feComponentTransfer");a.setAttribute("color-interpolation-filters","sRGB"),e.appendChild(a),this.matrixFilter=a;var l=createNS("feFuncR");l.setAttribute("type","table"),a.appendChild(l),this.feFuncR=l;var c=createNS("feFuncG");c.setAttribute("type","table"),a.appendChild(c),this.feFuncG=c;var u=createNS("feFuncB");u.setAttribute("type","table"),a.appendChild(u),this.feFuncB=u}SVGTritoneFilter.prototype.renderFrame=function(e){if(e||this.filterManager._mdf){var r=this.filterManager.effectElements[0].p.v,n=this.filterManager.effectElements[1].p.v,a=this.filterManager.effectElements[2].p.v,l=a[0]+" "+n[0]+" "+r[0],c=a[1]+" "+n[1]+" "+r[1],u=a[2]+" "+n[2]+" "+r[2];this.feFuncR.setAttribute("tableValues",l),this.feFuncG.setAttribute("tableValues",c),this.feFuncB.setAttribute("tableValues",u)}};function SVGProLevelsFilter(e,r){this.filterManager=r;var n=this.filterManager.effectElements,a=createNS("feComponentTransfer");(n[10].p.k||n[10].p.v!==0||n[11].p.k||n[11].p.v!==1||n[12].p.k||n[12].p.v!==1||n[13].p.k||n[13].p.v!==0||n[14].p.k||n[14].p.v!==1)&&(this.feFuncR=this.createFeFunc("feFuncR",a)),(n[17].p.k||n[17].p.v!==0||n[18].p.k||n[18].p.v!==1||n[19].p.k||n[19].p.v!==1||n[20].p.k||n[20].p.v!==0||n[21].p.k||n[21].p.v!==1)&&(this.feFuncG=this.createFeFunc("feFuncG",a)),(n[24].p.k||n[24].p.v!==0||n[25].p.k||n[25].p.v!==1||n[26].p.k||n[26].p.v!==1||n[27].p.k||n[27].p.v!==0||n[28].p.k||n[28].p.v!==1)&&(this.feFuncB=this.createFeFunc("feFuncB",a)),(n[31].p.k||n[31].p.v!==0||n[32].p.k||n[32].p.v!==1||n[33].p.k||n[33].p.v!==1||n[34].p.k||n[34].p.v!==0||n[35].p.k||n[35].p.v!==1)&&(this.feFuncA=this.createFeFunc("feFuncA",a)),(this.feFuncR||this.feFuncG||this.feFuncB||this.feFuncA)&&(a.setAttribute("color-interpolation-filters","sRGB"),e.appendChild(a),a=createNS("feComponentTransfer")),(n[3].p.k||n[3].p.v!==0||n[4].p.k||n[4].p.v!==1||n[5].p.k||n[5].p.v!==1||n[6].p.k||n[6].p.v!==0||n[7].p.k||n[7].p.v!==1)&&(a.setAttribute("color-interpolation-filters","sRGB"),e.appendChild(a),this.feFuncRComposed=this.createFeFunc("feFuncR",a),this.feFuncGComposed=this.createFeFunc("feFuncG",a),this.feFuncBComposed=this.createFeFunc("feFuncB",a))}SVGProLevelsFilter.prototype.createFeFunc=function(e,r){var n=createNS(e);return n.setAttribute("type","table"),r.appendChild(n),n},SVGProLevelsFilter.prototype.getTableValue=function(e,r,n,a,l){for(var c=0,u=256,d,m=Math.min(e,r),E=Math.max(e,r),C=Array.call(null,{length:u}),_,A=0,T=l-a,S=r-e;c<=256;)d=c/256,d<=m?_=S<0?l:a:d>=E?_=S<0?a:l:_=a+T*Math.pow((d-e)/S,1/n),C[A]=_,A+=1,c+=256/(u-1);return C.join(" ")},SVGProLevelsFilter.prototype.renderFrame=function(e){if(e||this.filterManager._mdf){var r,n=this.filterManager.effectElements;this.feFuncRComposed&&(e||n[3].p._mdf||n[4].p._mdf||n[5].p._mdf||n[6].p._mdf||n[7].p._mdf)&&(r=this.getTableValue(n[3].p.v,n[4].p.v,n[5].p.v,n[6].p.v,n[7].p.v),this.feFuncRComposed.setAttribute("tableValues",r),this.feFuncGComposed.setAttribute("tableValues",r),this.feFuncBComposed.setAttribute("tableValues",r)),this.feFuncR&&(e||n[10].p._mdf||n[11].p._mdf||n[12].p._mdf||n[13].p._mdf||n[14].p._mdf)&&(r=this.getTableValue(n[10].p.v,n[11].p.v,n[12].p.v,n[13].p.v,n[14].p.v),this.feFuncR.setAttribute("tableValues",r)),this.feFuncG&&(e||n[17].p._mdf||n[18].p._mdf||n[19].p._mdf||n[20].p._mdf||n[21].p._mdf)&&(r=this.getTableValue(n[17].p.v,n[18].p.v,n[19].p.v,n[20].p.v,n[21].p.v),this.feFuncG.setAttribute("tableValues",r)),this.feFuncB&&(e||n[24].p._mdf||n[25].p._mdf||n[26].p._mdf||n[27].p._mdf||n[28].p._mdf)&&(r=this.getTableValue(n[24].p.v,n[25].p.v,n[26].p.v,n[27].p.v,n[28].p.v),this.feFuncB.setAttribute("tableValues",r)),this.feFuncA&&(e||n[31].p._mdf||n[32].p._mdf||n[33].p._mdf||n[34].p._mdf||n[35].p._mdf)&&(r=this.getTableValue(n[31].p.v,n[32].p.v,n[33].p.v,n[34].p.v,n[35].p.v),this.feFuncA.setAttribute("tableValues",r))}};function SVGDropShadowEffect(e,r){var n=r.container.globalData.renderConfig.filterSize;e.setAttribute("x",n.x),e.setAttribute("y",n.y),e.setAttribute("width",n.width),e.setAttribute("height",n.height),this.filterManager=r;var a=createNS("feGaussianBlur");a.setAttribute("in","SourceAlpha"),a.setAttribute("result","drop_shadow_1"),a.setAttribute("stdDeviation","0"),this.feGaussianBlur=a,e.appendChild(a);var l=createNS("feOffset");l.setAttribute("dx","25"),l.setAttribute("dy","0"),l.setAttribute("in","drop_shadow_1"),l.setAttribute("result","drop_shadow_2"),this.feOffset=l,e.appendChild(l);var c=createNS("feFlood");c.setAttribute("flood-color","#00ff00"),c.setAttribute("flood-opacity","1"),c.setAttribute("result","drop_shadow_3"),this.feFlood=c,e.appendChild(c);var u=createNS("feComposite");u.setAttribute("in","drop_shadow_3"),u.setAttribute("in2","drop_shadow_2"),u.setAttribute("operator","in"),u.setAttribute("result","drop_shadow_4"),e.appendChild(u);var d=createNS("feMerge");e.appendChild(d);var m;m=createNS("feMergeNode"),d.appendChild(m),m=createNS("feMergeNode"),m.setAttribute("in","SourceGraphic"),this.feMergeNode=m,this.feMerge=d,this.originalNodeAdded=!1,d.appendChild(m)}SVGDropShadowEffect.prototype.renderFrame=function(e){if(e||this.filterManager._mdf){if((e||this.filterManager.effectElements[4].p._mdf)&&this.feGaussianBlur.setAttribute("stdDeviation",this.filterManager.effectElements[4].p.v/4),e||this.filterManager.effectElements[0].p._mdf){var r=this.filterManager.effectElements[0].p.v;this.feFlood.setAttribute("flood-color",rgbToHex(Math.round(r[0]*255),Math.round(r[1]*255),Math.round(r[2]*255)))}if((e||this.filterManager.effectElements[1].p._mdf)&&this.feFlood.setAttribute("flood-opacity",this.filterManager.effectElements[1].p.v/255),e||this.filterManager.effectElements[2].p._mdf||this.filterManager.effectElements[3].p._mdf){var n=this.filterManager.effectElements[3].p.v,a=(this.filterManager.effectElements[2].p.v-90)*degToRads,l=n*Math.cos(a),c=n*Math.sin(a);this.feOffset.setAttribute("dx",l),this.feOffset.setAttribute("dy",c)}}};var _svgMatteSymbols=[];function SVGMatte3Effect(e,r,n){this.initialized=!1,this.filterManager=r,this.filterElem=e,this.elem=n,n.matteElement=createNS("g"),n.matteElement.appendChild(n.layerElement),n.matteElement.appendChild(n.transformedElement),n.baseElement=n.matteElement}SVGMatte3Effect.prototype.findSymbol=function(e){for(var r=0,n=_svgMatteSymbols.length;r<n;){if(_svgMatteSymbols[r]===e)return _svgMatteSymbols[r];r+=1}return null},SVGMatte3Effect.prototype.replaceInParent=function(e,r){var n=e.layerElement.parentNode;if(!!n){for(var a=n.children,l=0,c=a.length;l<c&&a[l]!==e.layerElement;)l+=1;var u;l<=c-2&&(u=a[l+1]);var d=createNS("use");d.setAttribute("href","#"+r),u?n.insertBefore(d,u):n.appendChild(d)}},SVGMatte3Effect.prototype.setElementAsMask=function(e,r){if(!this.findSymbol(r)){var n=createElementID(),a=createNS("mask");a.setAttribute("id",r.layerId),a.setAttribute("mask-type","alpha"),_svgMatteSymbols.push(r);var l=e.globalData.defs;l.appendChild(a);var c=createNS("symbol");c.setAttribute("id",n),this.replaceInParent(r,n),c.appendChild(r.layerElement),l.appendChild(c);var u=createNS("use");u.setAttribute("href","#"+n),a.appendChild(u),r.data.hd=!1,r.show()}e.setMatte(r.layerId)},SVGMatte3Effect.prototype.initialize=function(){for(var e=this.filterManager.effectElements[0].p.v,r=this.elem.comp.elements,n=0,a=r.length;n<a;)r[n]&&r[n].data.ind===e&&this.setElementAsMask(this.elem,r[n]),n+=1;this.initialized=!0},SVGMatte3Effect.prototype.renderFrame=function(){this.initialized||this.initialize()};function SVGGaussianBlurEffect(e,r){e.setAttribute("x","-100%"),e.setAttribute("y","-100%"),e.setAttribute("width","300%"),e.setAttribute("height","300%"),this.filterManager=r;var n=createNS("feGaussianBlur");e.appendChild(n),this.feGaussianBlur=n}SVGGaussianBlurEffect.prototype.renderFrame=function(e){if(e||this.filterManager._mdf){var r=.3,n=this.filterManager.effectElements[0].p.v*r,a=this.filterManager.effectElements[1].p.v,l=a==3?0:n,c=a==2?0:n;this.feGaussianBlur.setAttribute("stdDeviation",l+" "+c);var u=this.filterManager.effectElements[2].p.v==1?"wrap":"duplicate";this.feGaussianBlur.setAttribute("edgeMode",u)}};var registeredEffects={};function SVGEffects(e){var r,n=e.data.ef?e.data.ef.length:0,a=createElementID(),l=filtersFactory.createFilter(a,!0),c=0;this.filters=[];var u;for(r=0;r<n;r+=1){u=null;var d=e.data.ef[r].ty;if(registeredEffects[d]){var m=registeredEffects[d].effect;u=new m(l,e.effectsManager.effectElements[r],e),registeredEffects[d].countsAsEffect&&(c+=1)}e.data.ef[r].ty===20?(c+=1,u=new SVGTintFilter(l,e.effectsManager.effectElements[r])):e.data.ef[r].ty===21?(c+=1,u=new SVGFillFilter(l,e.effectsManager.effectElements[r])):e.data.ef[r].ty===22?u=new SVGStrokeEffect(e,e.effectsManager.effectElements[r]):e.data.ef[r].ty===23?(c+=1,u=new SVGTritoneFilter(l,e.effectsManager.effectElements[r])):e.data.ef[r].ty===24?(c+=1,u=new SVGProLevelsFilter(l,e.effectsManager.effectElements[r])):e.data.ef[r].ty===25?(c+=1,u=new SVGDropShadowEffect(l,e.effectsManager.effectElements[r])):e.data.ef[r].ty===28?u=new SVGMatte3Effect(l,e.effectsManager.effectElements[r],e):e.data.ef[r].ty===29&&(c+=1,u=new SVGGaussianBlurEffect(l,e.effectsManager.effectElements[r])),u&&this.filters.push(u)}c&&(e.globalData.defs.appendChild(l),e.layerElement.setAttribute("filter","url("+getLocationHref()+"#"+a+")")),this.filters.length&&e.addRenderableComponent(this)}SVGEffects.prototype.renderFrame=function(e){var r,n=this.filters.length;for(r=0;r<n;r+=1)this.filters[r].renderFrame(e)};function registerEffect(e,r,n){registeredEffects[e]={effect:r,countsAsEffect:n}}function SVGBaseElement(){}SVGBaseElement.prototype={initRendererElement:function(){this.layerElement=createNS("g")},createContainerElements:function(){this.matteElement=createNS("g"),this.transformedElement=this.layerElement,this.maskedElement=this.layerElement,this._sizeChanged=!1;var r=null,n,a,l;if(this.data.td){if(this.data.td==3||this.data.td==1){var c=createNS("mask");c.setAttribute("id",this.layerId),c.setAttribute("mask-type",this.data.td==3?"luminance":"alpha"),c.appendChild(this.layerElement),r=c,this.globalData.defs.appendChild(c),!featureSupport.maskType&&this.data.td==1&&(c.setAttribute("mask-type","luminance"),n=createElementID(),a=filtersFactory.createFilter(n),this.globalData.defs.appendChild(a),a.appendChild(filtersFactory.createAlphaToLuminanceFilter()),l=createNS("g"),l.appendChild(this.layerElement),r=l,c.appendChild(l),l.setAttribute("filter","url("+getLocationHref()+"#"+n+")"))}else if(this.data.td==2){var u=createNS("mask");u.setAttribute("id",this.layerId),u.setAttribute("mask-type","alpha");var d=createNS("g");u.appendChild(d),n=createElementID(),a=filtersFactory.createFilter(n);var m=createNS("feComponentTransfer");m.setAttribute("in","SourceGraphic"),a.appendChild(m);var E=createNS("feFuncA");E.setAttribute("type","table"),E.setAttribute("tableValues","1.0 0.0"),m.appendChild(E),this.globalData.defs.appendChild(a);var C=createNS("rect");C.setAttribute("width",this.comp.data.w),C.setAttribute("height",this.comp.data.h),C.setAttribute("x","0"),C.setAttribute("y","0"),C.setAttribute("fill","#ffffff"),C.setAttribute("opacity","0"),d.setAttribute("filter","url("+getLocationHref()+"#"+n+")"),d.appendChild(C),d.appendChild(this.layerElement),r=d,featureSupport.maskType||(u.setAttribute("mask-type","luminance"),a.appendChild(filtersFactory.createAlphaToLuminanceFilter()),l=createNS("g"),d.appendChild(C),l.appendChild(this.layerElement),r=l,d.appendChild(l)),this.globalData.defs.appendChild(u)}}else this.data.tt?(this.matteElement.appendChild(this.layerElement),r=this.matteElement,this.baseElement=this.matteElement):this.baseElement=this.layerElement;if(this.data.ln&&this.layerElement.setAttribute("id",this.data.ln),this.data.cl&&this.layerElement.setAttribute("class",this.data.cl),this.data.ty===0&&!this.data.hd){var _=createNS("clipPath"),A=createNS("path");A.setAttribute("d","M0,0 L"+this.data.w+",0 L"+this.data.w+","+this.data.h+" L0,"+this.data.h+"z");var T=createElementID();if(_.setAttribute("id",T),_.appendChild(A),this.globalData.defs.appendChild(_),this.checkMasks()){var S=createNS("g");S.setAttribute("clip-path","url("+getLocationHref()+"#"+T+")"),S.appendChild(this.layerElement),this.transformedElement=S,r?r.appendChild(this.transformedElement):this.baseElement=this.transformedElement}else this.layerElement.setAttribute("clip-path","url("+getLocationHref()+"#"+T+")")}this.data.bm!==0&&this.setBlendMode()},renderElement:function(){this.finalTransform._matMdf&&this.transformedElement.setAttribute("transform",this.finalTransform.mat.to2dCSS()),this.finalTransform._opMdf&&this.transformedElement.setAttribute("opacity",this.finalTransform.mProp.o.v)},destroyBaseElement:function(){this.layerElement=null,this.matteElement=null,this.maskManager.destroy()},getBaseElement:function(){return this.data.hd?null:this.baseElement},createRenderableComponents:function(){this.maskManager=new MaskElement(this.data,this,this.globalData),this.renderableEffectsManager=new SVGEffects(this)},setMatte:function(r){!this.matteElement||this.matteElement.setAttribute("mask","url("+getLocationHref()+"#"+r+")")}};function HierarchyElement(){}HierarchyElement.prototype={initHierarchy:function(){this.hierarchy=[],this._isParent=!1,this.checkParenting()},setHierarchy:function(r){this.hierarchy=r},setAsParent:function(){this._isParent=!0},checkParenting:function(){this.data.parent!==void 0&&this.comp.buildElementParenting(this,this.data.parent,[])}};function RenderableDOMElement(){}(function(){var e={initElement:function(n,a,l){this.initFrame(),this.initBaseData(n,a,l),this.initTransform(n,a,l),this.initHierarchy(),this.initRenderable(),this.initRendererElement(),this.createContainerElements(),this.createRenderableComponents(),this.createContent(),this.hide()},hide:function(){if(!this.hidden&&(!this.isInRange||this.isTransparent)){var n=this.baseElement||this.layerElement;n.style.display="none",this.hidden=!0}},show:function(){if(this.isInRange&&!this.isTransparent){if(!this.data.hd){var n=this.baseElement||this.layerElement;n.style.display="block"}this.hidden=!1,this._isFirstFrame=!0}},renderFrame:function(){this.data.hd||this.hidden||(this.renderTransform(),this.renderRenderable(),this.renderElement(),this.renderInnerContent(),this._isFirstFrame&&(this._isFirstFrame=!1))},renderInnerContent:function(){},prepareFrame:function(n){this._mdf=!1,this.prepareRenderableFrame(n),this.prepareProperties(n,this.isInRange),this.checkTransparency()},destroy:function(){this.innerElem=null,this.destroyBaseElement()}};extendPrototype([RenderableElement,createProxyFunction(e)],RenderableDOMElement)})();function IImageElement(e,r,n){this.assetData=r.getAssetData(e.refId),this.initElement(e,r,n),this.sourceRect={top:0,left:0,width:this.assetData.w,height:this.assetData.h}}extendPrototype([BaseElement,TransformElement,SVGBaseElement,HierarchyElement,FrameElement,RenderableDOMElement],IImageElement),IImageElement.prototype.createContent=function(){var e=this.globalData.getAssetsPath(this.assetData);this.innerElem=createNS("image"),this.innerElem.setAttribute("width",this.assetData.w+"px"),this.innerElem.setAttribute("height",this.assetData.h+"px"),this.innerElem.setAttribute("preserveAspectRatio",this.assetData.pr||this.globalData.renderConfig.imagePreserveAspectRatio),this.innerElem.setAttributeNS("http://www.w3.org/1999/xlink","href",e),this.layerElement.appendChild(this.innerElem)},IImageElement.prototype.sourceRectAtTime=function(){return this.sourceRect};function ProcessedElement(e,r){this.elem=e,this.pos=r}function IShapeElement(){}IShapeElement.prototype={addShapeToModifiers:function(r){var n,a=this.shapeModifiers.length;for(n=0;n<a;n+=1)this.shapeModifiers[n].addShape(r)},isShapeInAnimatedModifiers:function(r){for(var n=0,a=this.shapeModifiers.length;n<a;)if(this.shapeModifiers[n].isAnimatedWithShape(r))return!0;return!1},renderModifiers:function(){if(!!this.shapeModifiers.length){var r,n=this.shapes.length;for(r=0;r<n;r+=1)this.shapes[r].sh.reset();n=this.shapeModifiers.length;var a;for(r=n-1;r>=0&&(a=this.shapeModifiers[r].processShapes(this._isFirstFrame),!a);r-=1);}},searchProcessedElement:function(r){for(var n=this.processedElements,a=0,l=n.length;a<l;){if(n[a].elem===r)return n[a].pos;a+=1}return 0},addProcessedElement:function(r,n){for(var a=this.processedElements,l=a.length;l;)if(l-=1,a[l].elem===r){a[l].pos=n;return}a.push(new ProcessedElement(r,n))},prepareFrame:function(r){this.prepareRenderableFrame(r),this.prepareProperties(r,this.isInRange)}};var lineCapEnum={1:"butt",2:"round",3:"square"},lineJoinEnum={1:"miter",2:"round",3:"bevel"};function SVGShapeData(e,r,n){this.caches=[],this.styles=[],this.transformers=e,this.lStr="",this.sh=n,this.lvl=r,this._isAnimated=!!n.k;for(var a=0,l=e.length;a<l;){if(e[a].mProps.dynamicProperties.length){this._isAnimated=!0;break}a+=1}}SVGShapeData.prototype.setAsAnimated=function(){this._isAnimated=!0};function SVGStyleData(e,r){this.data=e,this.type=e.ty,this.d="",this.lvl=r,this._mdf=!1,this.closed=e.hd===!0,this.pElem=createNS("path"),this.msElem=null}SVGStyleData.prototype.reset=function(){this.d="",this._mdf=!1};function DashProperty(e,r,n,a){this.elem=e,this.frameId=-1,this.dataProps=createSizedArray(r.length),this.renderer=n,this.k=!1,this.dashStr="",this.dashArray=createTypedArray("float32",r.length?r.length-1:0),this.dashoffset=createTypedArray("float32",1),this.initDynamicPropertyContainer(a);var l,c=r.length||0,u;for(l=0;l<c;l+=1)u=PropertyFactory.getProp(e,r[l].v,0,0,this),this.k=u.k||this.k,this.dataProps[l]={n:r[l].n,p:u};this.k||this.getValue(!0),this._isAnimated=this.k}DashProperty.prototype.getValue=function(e){if(!(this.elem.globalData.frameId===this.frameId&&!e)&&(this.frameId=this.elem.globalData.frameId,this.iterateDynamicProperties(),this._mdf=this._mdf||e,this._mdf)){var r=0,n=this.dataProps.length;for(this.renderer==="svg"&&(this.dashStr=""),r=0;r<n;r+=1)this.dataProps[r].n!=="o"?this.renderer==="svg"?this.dashStr+=" "+this.dataProps[r].p.v:this.dashArray[r]=this.dataProps[r].p.v:this.dashoffset[0]=this.dataProps[r].p.v}},extendPrototype([DynamicPropertyContainer],DashProperty);function SVGStrokeStyleData(e,r,n){this.initDynamicPropertyContainer(e),this.getValue=this.iterateDynamicProperties,this.o=PropertyFactory.getProp(e,r.o,0,.01,this),this.w=PropertyFactory.getProp(e,r.w,0,null,this),this.d=new DashProperty(e,r.d||{},"svg",this),this.c=PropertyFactory.getProp(e,r.c,1,255,this),this.style=n,this._isAnimated=!!this._isAnimated}extendPrototype([DynamicPropertyContainer],SVGStrokeStyleData);function SVGFillStyleData(e,r,n){this.initDynamicPropertyContainer(e),this.getValue=this.iterateDynamicProperties,this.o=PropertyFactory.getProp(e,r.o,0,.01,this),this.c=PropertyFactory.getProp(e,r.c,1,255,this),this.style=n}extendPrototype([DynamicPropertyContainer],SVGFillStyleData);function SVGNoStyleData(e,r,n){this.initDynamicPropertyContainer(e),this.getValue=this.iterateDynamicProperties,this.style=n}extendPrototype([DynamicPropertyContainer],SVGNoStyleData);function GradientProperty(e,r,n){this.data=r,this.c=createTypedArray("uint8c",r.p*4);var a=r.k.k[0].s?r.k.k[0].s.length-r.p*4:r.k.k.length-r.p*4;this.o=createTypedArray("float32",a),this._cmdf=!1,this._omdf=!1,this._collapsable=this.checkCollapsable(),this._hasOpacity=a,this.initDynamicPropertyContainer(n),this.prop=PropertyFactory.getProp(e,r.k,1,null,this),this.k=this.prop.k,this.getValue(!0)}GradientProperty.prototype.comparePoints=function(e,r){for(var n=0,a=this.o.length/2,l;n<a;){if(l=Math.abs(e[n*4]-e[r*4+n*2]),l>.01)return!1;n+=1}return!0},GradientProperty.prototype.checkCollapsable=function(){if(this.o.length/2!=this.c.length/4)return!1;if(this.data.k.k[0].s)for(var e=0,r=this.data.k.k.length;e<r;){if(!this.comparePoints(this.data.k.k[e].s,this.data.p))return!1;e+=1}else if(!this.comparePoints(this.data.k.k,this.data.p))return!1;return!0},GradientProperty.prototype.getValue=function(e){if(this.prop.getValue(),this._mdf=!1,this._cmdf=!1,this._omdf=!1,this.prop._mdf||e){var r,n=this.data.p*4,a,l;for(r=0;r<n;r+=1)a=r%4==0?100:255,l=Math.round(this.prop.v[r]*a),this.c[r]!==l&&(this.c[r]=l,this._cmdf=!e);if(this.o.length)for(n=this.prop.v.length,r=this.data.p*4;r<n;r+=1)a=r%2==0?100:1,l=r%2==0?Math.round(this.prop.v[r]*100):this.prop.v[r],this.o[r-this.data.p*4]!==l&&(this.o[r-this.data.p*4]=l,this._omdf=!e);this._mdf=!e}},extendPrototype([DynamicPropertyContainer],GradientProperty);function SVGGradientFillStyleData(e,r,n){this.initDynamicPropertyContainer(e),this.getValue=this.iterateDynamicProperties,this.initGradientData(e,r,n)}SVGGradientFillStyleData.prototype.initGradientData=function(e,r,n){this.o=PropertyFactory.getProp(e,r.o,0,.01,this),this.s=PropertyFactory.getProp(e,r.s,1,null,this),this.e=PropertyFactory.getProp(e,r.e,1,null,this),this.h=PropertyFactory.getProp(e,r.h||{k:0},0,.01,this),this.a=PropertyFactory.getProp(e,r.a||{k:0},0,degToRads,this),this.g=new GradientProperty(e,r.g,this),this.style=n,this.stops=[],this.setGradientData(n.pElem,r),this.setGradientOpacity(r,n),this._isAnimated=!!this._isAnimated},SVGGradientFillStyleData.prototype.setGradientData=function(e,r){var n=createElementID(),a=createNS(r.t===1?"linearGradient":"radialGradient");a.setAttribute("id",n),a.setAttribute("spreadMethod","pad"),a.setAttribute("gradientUnits","userSpaceOnUse");var l=[],c,u,d;for(d=r.g.p*4,u=0;u<d;u+=4)c=createNS("stop"),a.appendChild(c),l.push(c);e.setAttribute(r.ty==="gf"?"fill":"stroke","url("+getLocationHref()+"#"+n+")"),this.gf=a,this.cst=l},SVGGradientFillStyleData.prototype.setGradientOpacity=function(e,r){if(this.g._hasOpacity&&!this.g._collapsable){var n,a,l,c=createNS("mask"),u=createNS("path");c.appendChild(u);var d=createElementID(),m=createElementID();c.setAttribute("id",m);var E=createNS(e.t===1?"linearGradient":"radialGradient");E.setAttribute("id",d),E.setAttribute("spreadMethod","pad"),E.setAttribute("gradientUnits","userSpaceOnUse"),l=e.g.k.k[0].s?e.g.k.k[0].s.length:e.g.k.k.length;var C=this.stops;for(a=e.g.p*4;a<l;a+=2)n=createNS("stop"),n.setAttribute("stop-color","rgb(255,255,255)"),E.appendChild(n),C.push(n);u.setAttribute(e.ty==="gf"?"fill":"stroke","url("+getLocationHref()+"#"+d+")"),e.ty==="gs"&&(u.setAttribute("stroke-linecap",lineCapEnum[e.lc||2]),u.setAttribute("stroke-linejoin",lineJoinEnum[e.lj||2]),e.lj===1&&u.setAttribute("stroke-miterlimit",e.ml)),this.of=E,this.ms=c,this.ost=C,this.maskId=m,r.msElem=u}},extendPrototype([DynamicPropertyContainer],SVGGradientFillStyleData);function SVGGradientStrokeStyleData(e,r,n){this.initDynamicPropertyContainer(e),this.getValue=this.iterateDynamicProperties,this.w=PropertyFactory.getProp(e,r.w,0,null,this),this.d=new DashProperty(e,r.d||{},"svg",this),this.initGradientData(e,r,n),this._isAnimated=!!this._isAnimated}extendPrototype([SVGGradientFillStyleData,DynamicPropertyContainer],SVGGradientStrokeStyleData);function ShapeGroupData(){this.it=[],this.prevViewData=[],this.gr=createNS("g")}function SVGTransformData(e,r,n){this.transform={mProps:e,op:r,container:n},this.elements=[],this._isAnimated=this.transform.mProps.dynamicProperties.length||this.transform.op.effectsSequence.length}var buildShapeString=function(r,n,a,l){if(n===0)return"";var c=r.o,u=r.i,d=r.v,m,E=" M"+l.applyToPointStringified(d[0][0],d[0][1]);for(m=1;m<n;m+=1)E+=" C"+l.applyToPointStringified(c[m-1][0],c[m-1][1])+" "+l.applyToPointStringified(u[m][0],u[m][1])+" "+l.applyToPointStringified(d[m][0],d[m][1]);return a&&n&&(E+=" C"+l.applyToPointStringified(c[m-1][0],c[m-1][1])+" "+l.applyToPointStringified(u[0][0],u[0][1])+" "+l.applyToPointStringified(d[0][0],d[0][1]),E+="z"),E},SVGElementsRenderer=function(){var e=new Matrix,r=new Matrix,n={createRenderFunction:a};function a(_){switch(_.ty){case"fl":return d;case"gf":return E;case"gs":return m;case"st":return C;case"sh":case"el":case"rc":case"sr":return u;case"tr":return l;case"no":return c;default:return null}}function l(_,A,T){(T||A.transform.op._mdf)&&A.transform.container.setAttribute("opacity",A.transform.op.v),(T||A.transform.mProps._mdf)&&A.transform.container.setAttribute("transform",A.transform.mProps.v.to2dCSS())}function c(){}function u(_,A,T){var S,P,k,x,b,g,y=A.styles.length,I=A.lvl,M,j,O,L,N;for(g=0;g<y;g+=1){if(x=A.sh._mdf||T,A.styles[g].lvl<I){for(j=r.reset(),L=I-A.styles[g].lvl,N=A.transformers.length-1;!x&&L>0;)x=A.transformers[N].mProps._mdf||x,L-=1,N-=1;if(x)for(L=I-A.styles[g].lvl,N=A.transformers.length-1;L>0;)O=A.transformers[N].mProps.v.props,j.transform(O[0],O[1],O[2],O[3],O[4],O[5],O[6],O[7],O[8],O[9],O[10],O[11],O[12],O[13],O[14],O[15]),L-=1,N-=1}else j=e;if(M=A.sh.paths,P=M._length,x){for(k="",S=0;S<P;S+=1)b=M.shapes[S],b&&b._length&&(k+=buildShapeString(b,b._length,b.c,j));A.caches[g]=k}else k=A.caches[g];A.styles[g].d+=_.hd===!0?"":k,A.styles[g]._mdf=x||A.styles[g]._mdf}}function d(_,A,T){var S=A.style;(A.c._mdf||T)&&S.pElem.setAttribute("fill","rgb("+bmFloor(A.c.v[0])+","+bmFloor(A.c.v[1])+","+bmFloor(A.c.v[2])+")"),(A.o._mdf||T)&&S.pElem.setAttribute("fill-opacity",A.o.v)}function m(_,A,T){E(_,A,T),C(_,A,T)}function E(_,A,T){var S=A.gf,P=A.g._hasOpacity,k=A.s.v,x=A.e.v;if(A.o._mdf||T){var b=_.ty==="gf"?"fill-opacity":"stroke-opacity";A.style.pElem.setAttribute(b,A.o.v)}if(A.s._mdf||T){var g=_.t===1?"x1":"cx",y=g==="x1"?"y1":"cy";S.setAttribute(g,k[0]),S.setAttribute(y,k[1]),P&&!A.g._collapsable&&(A.of.setAttribute(g,k[0]),A.of.setAttribute(y,k[1]))}var I,M,j,O;if(A.g._cmdf||T){I=A.cst;var L=A.g.c;for(j=I.length,M=0;M<j;M+=1)O=I[M],O.setAttribute("offset",L[M*4]+"%"),O.setAttribute("stop-color","rgb("+L[M*4+1]+","+L[M*4+2]+","+L[M*4+3]+")")}if(P&&(A.g._omdf||T)){var N=A.g.o;for(A.g._collapsable?I=A.cst:I=A.ost,j=I.length,M=0;M<j;M+=1)O=I[M],A.g._collapsable||O.setAttribute("offset",N[M*2]+"%"),O.setAttribute("stop-opacity",N[M*2+1])}if(_.t===1)(A.e._mdf||T)&&(S.setAttribute("x2",x[0]),S.setAttribute("y2",x[1]),P&&!A.g._collapsable&&(A.of.setAttribute("x2",x[0]),A.of.setAttribute("y2",x[1])));else{var q;if((A.s._mdf||A.e._mdf||T)&&(q=Math.sqrt(Math.pow(k[0]-x[0],2)+Math.pow(k[1]-x[1],2)),S.setAttribute("r",q),P&&!A.g._collapsable&&A.of.setAttribute("r",q)),A.e._mdf||A.h._mdf||A.a._mdf||T){q||(q=Math.sqrt(Math.pow(k[0]-x[0],2)+Math.pow(k[1]-x[1],2)));var V=Math.atan2(x[1]-k[1],x[0]-k[0]),Q=A.h.v;Q>=1?Q=.99:Q<=-1&&(Q=-.99);var W=q*Q,G=Math.cos(V+A.a.v)*W+k[0],R=Math.sin(V+A.a.v)*W+k[1];S.setAttribute("fx",G),S.setAttribute("fy",R),P&&!A.g._collapsable&&(A.of.setAttribute("fx",G),A.of.setAttribute("fy",R))}}}function C(_,A,T){var S=A.style,P=A.d;P&&(P._mdf||T)&&P.dashStr&&(S.pElem.setAttribute("stroke-dasharray",P.dashStr),S.pElem.setAttribute("stroke-dashoffset",P.dashoffset[0])),A.c&&(A.c._mdf||T)&&S.pElem.setAttribute("stroke","rgb("+bmFloor(A.c.v[0])+","+bmFloor(A.c.v[1])+","+bmFloor(A.c.v[2])+")"),(A.o._mdf||T)&&S.pElem.setAttribute("stroke-opacity",A.o.v),(A.w._mdf||T)&&(S.pElem.setAttribute("stroke-width",A.w.v),S.msElem&&S.msElem.setAttribute("stroke-width",A.w.v))}return n}();function SVGShapeElement(e,r,n){this.shapes=[],this.shapesData=e.shapes,this.stylesList=[],this.shapeModifiers=[],this.itemsData=[],this.processedElements=[],this.animatedContents=[],this.initElement(e,r,n),this.prevViewData=[]}extendPrototype([BaseElement,TransformElement,SVGBaseElement,IShapeElement,HierarchyElement,FrameElement,RenderableDOMElement],SVGShapeElement),SVGShapeElement.prototype.initSecondaryElement=function(){},SVGShapeElement.prototype.identityMatrix=new Matrix,SVGShapeElement.prototype.buildExpressionInterface=function(){},SVGShapeElement.prototype.createContent=function(){this.searchShapes(this.shapesData,this.itemsData,this.prevViewData,this.layerElement,0,[],!0),this.filterUniqueShapes()},SVGShapeElement.prototype.filterUniqueShapes=function(){var e,r=this.shapes.length,n,a,l=this.stylesList.length,c,u=[],d=!1;for(a=0;a<l;a+=1){for(c=this.stylesList[a],d=!1,u.length=0,e=0;e<r;e+=1)n=this.shapes[e],n.styles.indexOf(c)!==-1&&(u.push(n),d=n._isAnimated||d);u.length>1&&d&&this.setShapesAsAnimated(u)}},SVGShapeElement.prototype.setShapesAsAnimated=function(e){var r,n=e.length;for(r=0;r<n;r+=1)e[r].setAsAnimated()},SVGShapeElement.prototype.createStyleElement=function(e,r){var n,a=new SVGStyleData(e,r),l=a.pElem;if(e.ty==="st")n=new SVGStrokeStyleData(this,e,a);else if(e.ty==="fl")n=new SVGFillStyleData(this,e,a);else if(e.ty==="gf"||e.ty==="gs"){var c=e.ty==="gf"?SVGGradientFillStyleData:SVGGradientStrokeStyleData;n=new c(this,e,a),this.globalData.defs.appendChild(n.gf),n.maskId&&(this.globalData.defs.appendChild(n.ms),this.globalData.defs.appendChild(n.of),l.setAttribute("mask","url("+getLocationHref()+"#"+n.maskId+")"))}else e.ty==="no"&&(n=new SVGNoStyleData(this,e,a));return(e.ty==="st"||e.ty==="gs")&&(l.setAttribute("stroke-linecap",lineCapEnum[e.lc||2]),l.setAttribute("stroke-linejoin",lineJoinEnum[e.lj||2]),l.setAttribute("fill-opacity","0"),e.lj===1&&l.setAttribute("stroke-miterlimit",e.ml)),e.r===2&&l.setAttribute("fill-rule","evenodd"),e.ln&&l.setAttribute("id",e.ln),e.cl&&l.setAttribute("class",e.cl),e.bm&&(l.style["mix-blend-mode"]=getBlendMode(e.bm)),this.stylesList.push(a),this.addToAnimatedContents(e,n),n},SVGShapeElement.prototype.createGroupElement=function(e){var r=new ShapeGroupData;return e.ln&&r.gr.setAttribute("id",e.ln),e.cl&&r.gr.setAttribute("class",e.cl),e.bm&&(r.gr.style["mix-blend-mode"]=getBlendMode(e.bm)),r},SVGShapeElement.prototype.createTransformElement=function(e,r){var n=TransformPropertyFactory.getTransformProperty(this,e,this),a=new SVGTransformData(n,n.o,r);return this.addToAnimatedContents(e,a),a},SVGShapeElement.prototype.createShapeElement=function(e,r,n){var a=4;e.ty==="rc"?a=5:e.ty==="el"?a=6:e.ty==="sr"&&(a=7);var l=ShapePropertyFactory.getShapeProp(this,e,a,this),c=new SVGShapeData(r,n,l);return this.shapes.push(c),this.addShapeToModifiers(c),this.addToAnimatedContents(e,c),c},SVGShapeElement.prototype.addToAnimatedContents=function(e,r){for(var n=0,a=this.animatedContents.length;n<a;){if(this.animatedContents[n].element===r)return;n+=1}this.animatedContents.push({fn:SVGElementsRenderer.createRenderFunction(e),element:r,data:e})},SVGShapeElement.prototype.setElementStyles=function(e){var r=e.styles,n,a=this.stylesList.length;for(n=0;n<a;n+=1)this.stylesList[n].closed||r.push(this.stylesList[n])},SVGShapeElement.prototype.reloadShapes=function(){this._isFirstFrame=!0;var e,r=this.itemsData.length;for(e=0;e<r;e+=1)this.prevViewData[e]=this.itemsData[e];for(this.searchShapes(this.shapesData,this.itemsData,this.prevViewData,this.layerElement,0,[],!0),this.filterUniqueShapes(),r=this.dynamicProperties.length,e=0;e<r;e+=1)this.dynamicProperties[e].getValue();this.renderModifiers()},SVGShapeElement.prototype.searchShapes=function(e,r,n,a,l,c,u){var d=[].concat(c),m,E=e.length-1,C,_,A=[],T=[],S,P,k;for(m=E;m>=0;m-=1){if(k=this.searchProcessedElement(e[m]),k?r[m]=n[k-1]:e[m]._render=u,e[m].ty==="fl"||e[m].ty==="st"||e[m].ty==="gf"||e[m].ty==="gs"||e[m].ty==="no")k?r[m].style.closed=!1:r[m]=this.createStyleElement(e[m],l),e[m]._render&&r[m].style.pElem.parentNode!==a&&a.appendChild(r[m].style.pElem),A.push(r[m].style);else if(e[m].ty==="gr"){if(!k)r[m]=this.createGroupElement(e[m]);else for(_=r[m].it.length,C=0;C<_;C+=1)r[m].prevViewData[C]=r[m].it[C];this.searchShapes(e[m].it,r[m].it,r[m].prevViewData,r[m].gr,l+1,d,u),e[m]._render&&r[m].gr.parentNode!==a&&a.appendChild(r[m].gr)}else e[m].ty==="tr"?(k||(r[m]=this.createTransformElement(e[m],a)),S=r[m].transform,d.push(S)):e[m].ty==="sh"||e[m].ty==="rc"||e[m].ty==="el"||e[m].ty==="sr"?(k||(r[m]=this.createShapeElement(e[m],d,l)),this.setElementStyles(r[m])):e[m].ty==="tm"||e[m].ty==="rd"||e[m].ty==="ms"||e[m].ty==="pb"?(k?(P=r[m],P.closed=!1):(P=ShapeModifiers.getModifier(e[m].ty),P.init(this,e[m]),r[m]=P,this.shapeModifiers.push(P)),T.push(P)):e[m].ty==="rp"&&(k?(P=r[m],P.closed=!0):(P=ShapeModifiers.getModifier(e[m].ty),r[m]=P,P.init(this,e,m,r),this.shapeModifiers.push(P),u=!1),T.push(P));this.addProcessedElement(e[m],m+1)}for(E=A.length,m=0;m<E;m+=1)A[m].closed=!0;for(E=T.length,m=0;m<E;m+=1)T[m].closed=!0},SVGShapeElement.prototype.renderInnerContent=function(){this.renderModifiers();var e,r=this.stylesList.length;for(e=0;e<r;e+=1)this.stylesList[e].reset();for(this.renderShape(),e=0;e<r;e+=1)(this.stylesList[e]._mdf||this._isFirstFrame)&&(this.stylesList[e].msElem&&(this.stylesList[e].msElem.setAttribute("d",this.stylesList[e].d),this.stylesList[e].d="M0 0"+this.stylesList[e].d),this.stylesList[e].pElem.setAttribute("d",this.stylesList[e].d||"M0 0"))},SVGShapeElement.prototype.renderShape=function(){var e,r=this.animatedContents.length,n;for(e=0;e<r;e+=1)n=this.animatedContents[e],(this._isFirstFrame||n.element._isAnimated)&&n.data!==!0&&n.fn(n.data,n.element,this._isFirstFrame)},SVGShapeElement.prototype.destroy=function(){this.destroyBaseElement(),this.shapesData=null,this.itemsData=null};function LetterProps(e,r,n,a,l,c){this.o=e,this.sw=r,this.sc=n,this.fc=a,this.m=l,this.p=c,this._mdf={o:!0,sw:!!r,sc:!!n,fc:!!a,m:!0,p:!0}}LetterProps.prototype.update=function(e,r,n,a,l,c){this._mdf.o=!1,this._mdf.sw=!1,this._mdf.sc=!1,this._mdf.fc=!1,this._mdf.m=!1,this._mdf.p=!1;var u=!1;return this.o!==e&&(this.o=e,this._mdf.o=!0,u=!0),this.sw!==r&&(this.sw=r,this._mdf.sw=!0,u=!0),this.sc!==n&&(this.sc=n,this._mdf.sc=!0,u=!0),this.fc!==a&&(this.fc=a,this._mdf.fc=!0,u=!0),this.m!==l&&(this.m=l,this._mdf.m=!0,u=!0),c.length&&(this.p[0]!==c[0]||this.p[1]!==c[1]||this.p[4]!==c[4]||this.p[5]!==c[5]||this.p[12]!==c[12]||this.p[13]!==c[13])&&(this.p=c,this._mdf.p=!0,u=!0),u};function TextProperty(e,r){this._frameId=initialDefaultFrame,this.pv="",this.v="",this.kf=!1,this._isFirstFrame=!0,this._mdf=!1,this.data=r,this.elem=e,this.comp=this.elem.comp,this.keysIndex=0,this.canResize=!1,this.minimumFontSize=1,this.effectsSequence=[],this.currentData={ascent:0,boxWidth:this.defaultBoxWidth,f:"",fStyle:"",fWeight:"",fc:"",j:"",justifyOffset:"",l:[],lh:0,lineWidths:[],ls:"",of:"",s:"",sc:"",sw:0,t:0,tr:0,sz:0,ps:null,fillColorAnim:!1,strokeColorAnim:!1,strokeWidthAnim:!1,yOffset:0,finalSize:0,finalText:[],finalLineHeight:0,__complete:!1},this.copyData(this.currentData,this.data.d.k[0].s),this.searchProperty()||this.completeTextData(this.currentData)}TextProperty.prototype.defaultBoxWidth=[0,0],TextProperty.prototype.copyData=function(e,r){for(var n in r)Object.prototype.hasOwnProperty.call(r,n)&&(e[n]=r[n]);return e},TextProperty.prototype.setCurrentData=function(e){e.__complete||this.completeTextData(e),this.currentData=e,this.currentData.boxWidth=this.currentData.boxWidth||this.defaultBoxWidth,this._mdf=!0},TextProperty.prototype.searchProperty=function(){return this.searchKeyframes()},TextProperty.prototype.searchKeyframes=function(){return this.kf=this.data.d.k.length>1,this.kf&&this.addEffect(this.getKeyframeValue.bind(this)),this.kf},TextProperty.prototype.addEffect=function(e){this.effectsSequence.push(e),this.elem.addDynamicProperty(this)},TextProperty.prototype.getValue=function(e){if(!((this.elem.globalData.frameId===this.frameId||!this.effectsSequence.length)&&!e)){this.currentData.t=this.data.d.k[this.keysIndex].s.t;var r=this.currentData,n=this.keysIndex;if(this.lock){this.setCurrentData(this.currentData);return}this.lock=!0,this._mdf=!1;var a,l=this.effectsSequence.length,c=e||this.data.d.k[this.keysIndex].s;for(a=0;a<l;a+=1)n!==this.keysIndex?c=this.effectsSequence[a](c,c.t):c=this.effectsSequence[a](this.currentData,c.t);r!==c&&this.setCurrentData(c),this.v=this.currentData,this.pv=this.v,this.lock=!1,this.frameId=this.elem.globalData.frameId}},TextProperty.prototype.getKeyframeValue=function(){for(var e=this.data.d.k,r=this.elem.comp.renderedFrame,n=0,a=e.length;n<=a-1&&!(n===a-1||e[n+1].t>r);)n+=1;return this.keysIndex!==n&&(this.keysIndex=n),this.data.d.k[this.keysIndex].s},TextProperty.prototype.buildFinalText=function(e){for(var r=[],n=0,a=e.length,l,c,u=!1;n<a;)l=e.charCodeAt(n),FontManager.isCombinedCharacter(l)?r[r.length-1]+=e.charAt(n):l>=55296&&l<=56319?(c=e.charCodeAt(n+1),c>=56320&&c<=57343?(u||FontManager.isModifier(l,c)?(r[r.length-1]+=e.substr(n,2),u=!1):r.push(e.substr(n,2)),n+=1):r.push(e.charAt(n))):l>56319?(c=e.charCodeAt(n+1),FontManager.isZeroWidthJoiner(l,c)?(u=!0,r[r.length-1]+=e.substr(n,2),n+=1):r.push(e.charAt(n))):FontManager.isZeroWidthJoiner(l)?(r[r.length-1]+=e.charAt(n),u=!0):r.push(e.charAt(n)),n+=1;return r},TextProperty.prototype.completeTextData=function(e){e.__complete=!0;var r=this.elem.globalData.fontManager,n=this.data,a=[],l,c,u,d=0,m,E=n.m.g,C=0,_=0,A=0,T=[],S=0,P=0,k,x,b=r.getFontByName(e.f),g,y=0,I=getFontProperties(b);e.fWeight=I.weight,e.fStyle=I.style,e.finalSize=e.s,e.finalText=this.buildFinalText(e.t),c=e.finalText.length,e.finalLineHeight=e.lh;var M=e.tr/1e3*e.finalSize,j;if(e.sz)for(var O=!0,L=e.sz[0],N=e.sz[1],q,V;O;){V=this.buildFinalText(e.t),q=0,S=0,c=V.length,M=e.tr/1e3*e.finalSize;var Q=-1;for(l=0;l<c;l+=1)j=V[l].charCodeAt(0),u=!1,V[l]===" "?Q=l:(j===13||j===3)&&(S=0,u=!0,q+=e.finalLineHeight||e.finalSize*1.2),r.chars?(g=r.getCharData(V[l],b.fStyle,b.fFamily),y=u?0:g.w*e.finalSize/100):y=r.measureText(V[l],e.f,e.finalSize),S+y>L&&V[l]!==" "?(Q===-1?c+=1:l=Q,q+=e.finalLineHeight||e.finalSize*1.2,V.splice(l,Q===l?1:0,"\r"),Q=-1,S=0):(S+=y,S+=M);q+=b.ascent*e.finalSize/100,this.canResize&&e.finalSize>this.minimumFontSize&&N<q?(e.finalSize-=1,e.finalLineHeight=e.finalSize*e.lh/e.s):(e.finalText=V,c=e.finalText.length,O=!1)}S=-M,y=0;var W=0,G;for(l=0;l<c;l+=1)if(u=!1,G=e.finalText[l],j=G.charCodeAt(0),j===13||j===3?(W=0,T.push(S),P=S>P?S:P,S=-2*M,m="",u=!0,A+=1):m=G,r.chars?(g=r.getCharData(G,b.fStyle,r.getFontByName(e.f).fFamily),y=u?0:g.w*e.finalSize/100):y=r.measureText(m,e.f,e.finalSize),G===" "?W+=y+M:(S+=y+M+W,W=0),a.push({l:y,an:y,add:C,n:u,anIndexes:[],val:m,line:A,animatorJustifyOffset:0}),E==2){if(C+=y,m===""||m===" "||l===c-1){for((m===""||m===" ")&&(C-=y);_<=l;)a[_].an=C,a[_].ind=d,a[_].extra=y,_+=1;d+=1,C=0}}else if(E==3){if(C+=y,m===""||l===c-1){for(m===""&&(C-=y);_<=l;)a[_].an=C,a[_].ind=d,a[_].extra=y,_+=1;C=0,d+=1}}else a[d].ind=d,a[d].extra=0,d+=1;if(e.l=a,P=S>P?S:P,T.push(S),e.sz)e.boxWidth=e.sz[0],e.justifyOffset=0;else switch(e.boxWidth=P,e.j){case 1:e.justifyOffset=-e.boxWidth;break;case 2:e.justifyOffset=-e.boxWidth/2;break;default:e.justifyOffset=0}e.lineWidths=T;var R=n.a,D,F;x=R.length;var $,U,z=[];for(k=0;k<x;k+=1){for(D=R[k],D.a.sc&&(e.strokeColorAnim=!0),D.a.sw&&(e.strokeWidthAnim=!0),(D.a.fc||D.a.fh||D.a.fs||D.a.fb)&&(e.fillColorAnim=!0),U=0,$=D.s.b,l=0;l<c;l+=1)F=a[l],F.anIndexes[k]=U,($==1&&F.val!==""||$==2&&F.val!==""&&F.val!==" "||$==3&&(F.n||F.val==" "||l==c-1)||$==4&&(F.n||l==c-1))&&(D.s.rn===1&&z.push(U),U+=1);n.a[k].s.totalChars=U;var Y=-1,Z;if(D.s.rn===1)for(l=0;l<c;l+=1)F=a[l],Y!=F.anIndexes[k]&&(Y=F.anIndexes[k],Z=z.splice(Math.floor(Math.random()*z.length),1)[0]),F.anIndexes[k]=Z}e.yOffset=e.finalLineHeight||e.finalSize*1.2,e.ls=e.ls||0,e.ascent=b.ascent*e.finalSize/100},TextProperty.prototype.updateDocumentData=function(e,r){r=r===void 0?this.keysIndex:r;var n=this.copyData({},this.data.d.k[r].s);n=this.copyData(n,e),this.data.d.k[r].s=n,this.recalculate(r),this.elem.addDynamicProperty(this)},TextProperty.prototype.recalculate=function(e){var r=this.data.d.k[e].s;r.__complete=!1,this.keysIndex=0,this._isFirstFrame=!0,this.getValue(r)},TextProperty.prototype.canResizeFont=function(e){this.canResize=e,this.recalculate(this.keysIndex),this.elem.addDynamicProperty(this)},TextProperty.prototype.setMinimumFontSize=function(e){this.minimumFontSize=Math.floor(e)||1,this.recalculate(this.keysIndex),this.elem.addDynamicProperty(this)};var TextSelectorProp=function(){var e=Math.max,r=Math.min,n=Math.floor;function a(c,u){this._currentTextLength=-1,this.k=!1,this.data=u,this.elem=c,this.comp=c.comp,this.finalS=0,this.finalE=0,this.initDynamicPropertyContainer(c),this.s=PropertyFactory.getProp(c,u.s||{k:0},0,0,this),"e"in u?this.e=PropertyFactory.getProp(c,u.e,0,0,this):this.e={v:100},this.o=PropertyFactory.getProp(c,u.o||{k:0},0,0,this),this.xe=PropertyFactory.getProp(c,u.xe||{k:0},0,0,this),this.ne=PropertyFactory.getProp(c,u.ne||{k:0},0,0,this),this.sm=PropertyFactory.getProp(c,u.sm||{k:100},0,0,this),this.a=PropertyFactory.getProp(c,u.a,0,.01,this),this.dynamicProperties.length||this.getValue()}a.prototype={getMult:function(u){this._currentTextLength!==this.elem.textProperty.currentData.l.length&&this.getValue();var d=0,m=0,E=1,C=1;this.ne.v>0?d=this.ne.v/100:m=-this.ne.v/100,this.xe.v>0?E=1-this.xe.v/100:C=1+this.xe.v/100;var _=BezierFactory.getBezierEasing(d,m,E,C).get,A=0,T=this.finalS,S=this.finalE,P=this.data.sh;if(P===2)S===T?A=u>=S?1:0:A=e(0,r(.5/(S-T)+(u-T)/(S-T),1)),A=_(A);else if(P===3)S===T?A=u>=S?0:1:A=1-e(0,r(.5/(S-T)+(u-T)/(S-T),1)),A=_(A);else if(P===4)S===T?A=0:(A=e(0,r(.5/(S-T)+(u-T)/(S-T),1)),A<.5?A*=2:A=1-2*(A-.5)),A=_(A);else if(P===5){if(S===T)A=0;else{var k=S-T;u=r(e(0,u+.5-T),S-T);var x=-k/2+u,b=k/2;A=Math.sqrt(1-x*x/(b*b))}A=_(A)}else P===6?(S===T?A=0:(u=r(e(0,u+.5-T),S-T),A=(1+Math.cos(Math.PI+Math.PI*2*u/(S-T)))/2),A=_(A)):(u>=n(T)&&(u-T<0?A=e(0,r(r(S,1)-(T-u),1)):A=e(0,r(S-u,1))),A=_(A));if(this.sm.v!==100){var g=this.sm.v*.01;g===0&&(g=1e-8);var y=.5-g*.5;A<y?A=0:(A=(A-y)/g,A>1&&(A=1))}return A*this.a.v},getValue:function(u){this.iterateDynamicProperties(),this._mdf=u||this._mdf,this._currentTextLength=this.elem.textProperty.currentData.l.length||0,u&&this.data.r===2&&(this.e.v=this._currentTextLength);var d=this.data.r===2?1:100/this.data.totalChars,m=this.o.v/d,E=this.s.v/d+m,C=this.e.v/d+m;if(E>C){var _=E;E=C,C=_}this.finalS=E,this.finalE=C}},extendPrototype([DynamicPropertyContainer],a);function l(c,u,d){return new a(c,u,d)}return{getTextSelectorProp:l}}();function TextAnimatorDataProperty(e,r,n){var a={propType:!1},l=PropertyFactory.getProp,c=r.a;this.a={r:c.r?l(e,c.r,0,degToRads,n):a,rx:c.rx?l(e,c.rx,0,degToRads,n):a,ry:c.ry?l(e,c.ry,0,degToRads,n):a,sk:c.sk?l(e,c.sk,0,degToRads,n):a,sa:c.sa?l(e,c.sa,0,degToRads,n):a,s:c.s?l(e,c.s,1,.01,n):a,a:c.a?l(e,c.a,1,0,n):a,o:c.o?l(e,c.o,0,.01,n):a,p:c.p?l(e,c.p,1,0,n):a,sw:c.sw?l(e,c.sw,0,0,n):a,sc:c.sc?l(e,c.sc,1,0,n):a,fc:c.fc?l(e,c.fc,1,0,n):a,fh:c.fh?l(e,c.fh,0,0,n):a,fs:c.fs?l(e,c.fs,0,.01,n):a,fb:c.fb?l(e,c.fb,0,.01,n):a,t:c.t?l(e,c.t,0,0,n):a},this.s=TextSelectorProp.getTextSelectorProp(e,r.s,n),this.s.t=r.s.t}function TextAnimatorProperty(e,r,n){this._isFirstFrame=!0,this._hasMaskedPath=!1,this._frameId=-1,this._textData=e,this._renderType=r,this._elem=n,this._animatorsData=createSizedArray(this._textData.a.length),this._pathData={},this._moreOptions={alignment:{}},this.renderedLetters=[],this.lettersChangedFlag=!1,this.initDynamicPropertyContainer(n)}TextAnimatorProperty.prototype.searchProperties=function(){var e,r=this._textData.a.length,n,a=PropertyFactory.getProp;for(e=0;e<r;e+=1)n=this._textData.a[e],this._animatorsData[e]=new TextAnimatorDataProperty(this._elem,n,this);this._textData.p&&"m"in this._textData.p?(this._pathData={a:a(this._elem,this._textData.p.a,0,0,this),f:a(this._elem,this._textData.p.f,0,0,this),l:a(this._elem,this._textData.p.l,0,0,this),r:a(this._elem,this._textData.p.r,0,0,this),p:a(this._elem,this._textData.p.p,0,0,this),m:this._elem.maskManager.getMaskProperty(this._textData.p.m)},this._hasMaskedPath=!0):this._hasMaskedPath=!1,this._moreOptions.alignment=a(this._elem,this._textData.m.a,1,0,this)},TextAnimatorProperty.prototype.getMeasures=function(e,r){if(this.lettersChangedFlag=r,!(!this._mdf&&!this._isFirstFrame&&!r&&(!this._hasMaskedPath||!this._pathData.m._mdf))){this._isFirstFrame=!1;var n=this._moreOptions.alignment.v,a=this._animatorsData,l=this._textData,c=this.mHelper,u=this._renderType,d=this.renderedLetters.length,m,E,C,_,A=e.l,T,S,P,k,x,b,g,y,I,M,j,O,L,N,q;if(this._hasMaskedPath){if(q=this._pathData.m,!this._pathData.n||this._pathData._mdf){var V=q.v;this._pathData.r.v&&(V=V.reverse()),T={tLength:0,segments:[]},_=V._length-1;var Q;for(O=0,C=0;C<_;C+=1)Q=bez.buildBezierData(V.v[C],V.v[C+1],[V.o[C][0]-V.v[C][0],V.o[C][1]-V.v[C][1]],[V.i[C+1][0]-V.v[C+1][0],V.i[C+1][1]-V.v[C+1][1]]),T.tLength+=Q.segmentLength,T.segments.push(Q),O+=Q.segmentLength;C=_,q.v.c&&(Q=bez.buildBezierData(V.v[C],V.v[0],[V.o[C][0]-V.v[C][0],V.o[C][1]-V.v[C][1]],[V.i[0][0]-V.v[0][0],V.i[0][1]-V.v[0][1]]),T.tLength+=Q.segmentLength,T.segments.push(Q),O+=Q.segmentLength),this._pathData.pi=T}if(T=this._pathData.pi,S=this._pathData.f.v,g=0,b=1,k=0,x=!0,M=T.segments,S<0&&q.v.c)for(T.tLength<Math.abs(S)&&(S=-Math.abs(S)%T.tLength),g=M.length-1,I=M[g].points,b=I.length-1;S<0;)S+=I[b].partialLength,b-=1,b<0&&(g-=1,I=M[g].points,b=I.length-1);I=M[g].points,y=I[b-1],P=I[b],j=P.partialLength}_=A.length,m=0,E=0;var W=e.finalSize*1.2*.714,G=!0,R,D,F,$,U;$=a.length;var z,Y=-1,Z,K,ue,ie=S,te=g,X=b,ce=-1,J,oe,de,ee,pe,ae,ne,me,ge="",Te=this.defaultPropsArray,fe;if(e.j===2||e.j===1){var Ee=0,_e=0,re=e.j===2?-.5:-1,le=0,ye=!0;for(C=0;C<_;C+=1)if(A[C].n){for(Ee&&(Ee+=_e);le<C;)A[le].animatorJustifyOffset=Ee,le+=1;Ee=0,ye=!0}else{for(F=0;F<$;F+=1)R=a[F].a,R.t.propType&&(ye&&e.j===2&&(_e+=R.t.v*re),D=a[F].s,z=D.getMult(A[C].anIndexes[F],l.a[F].s.totalChars),z.length?Ee+=R.t.v*z[0]*re:Ee+=R.t.v*z*re);ye=!1}for(Ee&&(Ee+=_e);le<C;)A[le].animatorJustifyOffset=Ee,le+=1}for(C=0;C<_;C+=1){if(c.reset(),J=1,A[C].n)m=0,E+=e.yOffset,E+=G?1:0,S=ie,G=!1,this._hasMaskedPath&&(g=te,b=X,I=M[g].points,y=I[b-1],P=I[b],j=P.partialLength,k=0),ge="",me="",ae="",fe="",Te=this.defaultPropsArray;else{if(this._hasMaskedPath){if(ce!==A[C].line){switch(e.j){case 1:S+=O-e.lineWidths[A[C].line];break;case 2:S+=(O-e.lineWidths[A[C].line])/2;break}ce=A[C].line}Y!==A[C].ind&&(A[Y]&&(S+=A[Y].extra),S+=A[C].an/2,Y=A[C].ind),S+=n[0]*A[C].an*.005;var ve=0;for(F=0;F<$;F+=1)R=a[F].a,R.p.propType&&(D=a[F].s,z=D.getMult(A[C].anIndexes[F],l.a[F].s.totalChars),z.length?ve+=R.p.v[0]*z[0]:ve+=R.p.v[0]*z),R.a.propType&&(D=a[F].s,z=D.getMult(A[C].anIndexes[F],l.a[F].s.totalChars),z.length?ve+=R.a.v[0]*z[0]:ve+=R.a.v[0]*z);for(x=!0,this._pathData.a.v&&(S=A[0].an*.5+(O-this._pathData.f.v-A[0].an*.5-A[A.length-1].an*.5)*Y/(_-1),S+=this._pathData.f.v);x;)k+j>=S+ve||!I?(L=(S+ve-k)/P.partialLength,K=y.point[0]+(P.point[0]-y.point[0])*L,ue=y.point[1]+(P.point[1]-y.point[1])*L,c.translate(-n[0]*A[C].an*.005,-(n[1]*W)*.01),x=!1):I&&(k+=P.partialLength,b+=1,b>=I.length&&(b=0,g+=1,M[g]?I=M[g].points:q.v.c?(b=0,g=0,I=M[g].points):(k-=P.partialLength,I=null)),I&&(y=P,P=I[b],j=P.partialLength));Z=A[C].an/2-A[C].add,c.translate(-Z,0,0)}else Z=A[C].an/2-A[C].add,c.translate(-Z,0,0),c.translate(-n[0]*A[C].an*.005,-n[1]*W*.01,0);for(F=0;F<$;F+=1)R=a[F].a,R.t.propType&&(D=a[F].s,z=D.getMult(A[C].anIndexes[F],l.a[F].s.totalChars),(m!==0||e.j!==0)&&(this._hasMaskedPath?z.length?S+=R.t.v*z[0]:S+=R.t.v*z:z.length?m+=R.t.v*z[0]:m+=R.t.v*z));for(e.strokeWidthAnim&&(de=e.sw||0),e.strokeColorAnim&&(e.sc?oe=[e.sc[0],e.sc[1],e.sc[2]]:oe=[0,0,0]),e.fillColorAnim&&e.fc&&(ee=[e.fc[0],e.fc[1],e.fc[2]]),F=0;F<$;F+=1)R=a[F].a,R.a.propType&&(D=a[F].s,z=D.getMult(A[C].anIndexes[F],l.a[F].s.totalChars),z.length?c.translate(-R.a.v[0]*z[0],-R.a.v[1]*z[1],R.a.v[2]*z[2]):c.translate(-R.a.v[0]*z,-R.a.v[1]*z,R.a.v[2]*z));for(F=0;F<$;F+=1)R=a[F].a,R.s.propType&&(D=a[F].s,z=D.getMult(A[C].anIndexes[F],l.a[F].s.totalChars),z.length?c.scale(1+(R.s.v[0]-1)*z[0],1+(R.s.v[1]-1)*z[1],1):c.scale(1+(R.s.v[0]-1)*z,1+(R.s.v[1]-1)*z,1));for(F=0;F<$;F+=1){if(R=a[F].a,D=a[F].s,z=D.getMult(A[C].anIndexes[F],l.a[F].s.totalChars),R.sk.propType&&(z.length?c.skewFromAxis(-R.sk.v*z[0],R.sa.v*z[1]):c.skewFromAxis(-R.sk.v*z,R.sa.v*z)),R.r.propType&&(z.length?c.rotateZ(-R.r.v*z[2]):c.rotateZ(-R.r.v*z)),R.ry.propType&&(z.length?c.rotateY(R.ry.v*z[1]):c.rotateY(R.ry.v*z)),R.rx.propType&&(z.length?c.rotateX(R.rx.v*z[0]):c.rotateX(R.rx.v*z)),R.o.propType&&(z.length?J+=(R.o.v*z[0]-J)*z[0]:J+=(R.o.v*z-J)*z),e.strokeWidthAnim&&R.sw.propType&&(z.length?de+=R.sw.v*z[0]:de+=R.sw.v*z),e.strokeColorAnim&&R.sc.propType)for(pe=0;pe<3;pe+=1)z.length?oe[pe]+=(R.sc.v[pe]-oe[pe])*z[0]:oe[pe]+=(R.sc.v[pe]-oe[pe])*z;if(e.fillColorAnim&&e.fc){if(R.fc.propType)for(pe=0;pe<3;pe+=1)z.length?ee[pe]+=(R.fc.v[pe]-ee[pe])*z[0]:ee[pe]+=(R.fc.v[pe]-ee[pe])*z;R.fh.propType&&(z.length?ee=addHueToRGB(ee,R.fh.v*z[0]):ee=addHueToRGB(ee,R.fh.v*z)),R.fs.propType&&(z.length?ee=addSaturationToRGB(ee,R.fs.v*z[0]):ee=addSaturationToRGB(ee,R.fs.v*z)),R.fb.propType&&(z.length?ee=addBrightnessToRGB(ee,R.fb.v*z[0]):ee=addBrightnessToRGB(ee,R.fb.v*z))}}for(F=0;F<$;F+=1)R=a[F].a,R.p.propType&&(D=a[F].s,z=D.getMult(A[C].anIndexes[F],l.a[F].s.totalChars),this._hasMaskedPath?z.length?c.translate(0,R.p.v[1]*z[0],-R.p.v[2]*z[1]):c.translate(0,R.p.v[1]*z,-R.p.v[2]*z):z.length?c.translate(R.p.v[0]*z[0],R.p.v[1]*z[1],-R.p.v[2]*z[2]):c.translate(R.p.v[0]*z,R.p.v[1]*z,-R.p.v[2]*z));if(e.strokeWidthAnim&&(ae=de<0?0:de),e.strokeColorAnim&&(ne="rgb("+Math.round(oe[0]*255)+","+Math.round(oe[1]*255)+","+Math.round(oe[2]*255)+")"),e.fillColorAnim&&e.fc&&(me="rgb("+Math.round(ee[0]*255)+","+Math.round(ee[1]*255)+","+Math.round(ee[2]*255)+")"),this._hasMaskedPath){if(c.translate(0,-e.ls),c.translate(0,n[1]*W*.01+E,0),this._pathData.p.v){N=(P.point[1]-y.point[1])/(P.point[0]-y.point[0]);var be=Math.atan(N)*180/Math.PI;P.point[0]<y.point[0]&&(be+=180),c.rotate(-be*Math.PI/180)}c.translate(K,ue,0),S-=n[0]*A[C].an*.005,A[C+1]&&Y!==A[C+1].ind&&(S+=A[C].an/2,S+=e.tr*.001*e.finalSize)}else{switch(c.translate(m,E,0),e.ps&&c.translate(e.ps[0],e.ps[1]+e.ascent,0),e.j){case 1:c.translate(A[C].animatorJustifyOffset+e.justifyOffset+(e.boxWidth-e.lineWidths[A[C].line]),0,0);break;case 2:c.translate(A[C].animatorJustifyOffset+e.justifyOffset+(e.boxWidth-e.lineWidths[A[C].line])/2,0,0);break}c.translate(0,-e.ls),c.translate(Z,0,0),c.translate(n[0]*A[C].an*.005,n[1]*W*.01,0),m+=A[C].l+e.tr*.001*e.finalSize}u==="html"?ge=c.toCSS():u==="svg"?ge=c.to2dCSS():Te=[c.props[0],c.props[1],c.props[2],c.props[3],c.props[4],c.props[5],c.props[6],c.props[7],c.props[8],c.props[9],c.props[10],c.props[11],c.props[12],c.props[13],c.props[14],c.props[15]],fe=J}d<=C?(U=new LetterProps(fe,ae,ne,me,ge,Te),this.renderedLetters.push(U),d+=1,this.lettersChangedFlag=!0):(U=this.renderedLetters[C],this.lettersChangedFlag=U.update(fe,ae,ne,me,ge,Te)||this.lettersChangedFlag)}}},TextAnimatorProperty.prototype.getValue=function(){this._elem.globalData.frameId!==this._frameId&&(this._frameId=this._elem.globalData.frameId,this.iterateDynamicProperties())},TextAnimatorProperty.prototype.mHelper=new Matrix,TextAnimatorProperty.prototype.defaultPropsArray=[],extendPrototype([DynamicPropertyContainer],TextAnimatorProperty);function ITextElement(){}ITextElement.prototype.initElement=function(e,r,n){this.lettersChangedFlag=!0,this.initFrame(),this.initBaseData(e,r,n),this.textProperty=new TextProperty(this,e.t,this.dynamicProperties),this.textAnimator=new TextAnimatorProperty(e.t,this.renderType,this),this.initTransform(e,r,n),this.initHierarchy(),this.initRenderable(),this.initRendererElement(),this.createContainerElements(),this.createRenderableComponents(),this.createContent(),this.hide(),this.textAnimator.searchProperties(this.dynamicProperties)},ITextElement.prototype.prepareFrame=function(e){this._mdf=!1,this.prepareRenderableFrame(e),this.prepareProperties(e,this.isInRange),(this.textProperty._mdf||this.textProperty._isFirstFrame)&&(this.buildNewText(),this.textProperty._isFirstFrame=!1,this.textProperty._mdf=!1)},ITextElement.prototype.createPathShape=function(e,r){var n,a=r.length,l,c="";for(n=0;n<a;n+=1)r[n].ty==="sh"&&(l=r[n].ks.k,c+=buildShapeString(l,l.i.length,!0,e));return c},ITextElement.prototype.updateDocumentData=function(e,r){this.textProperty.updateDocumentData(e,r)},ITextElement.prototype.canResizeFont=function(e){this.textProperty.canResizeFont(e)},ITextElement.prototype.setMinimumFontSize=function(e){this.textProperty.setMinimumFontSize(e)},ITextElement.prototype.applyTextPropertiesToMatrix=function(e,r,n,a,l){switch(e.ps&&r.translate(e.ps[0],e.ps[1]+e.ascent,0),r.translate(0,-e.ls,0),e.j){case 1:r.translate(e.justifyOffset+(e.boxWidth-e.lineWidths[n]),0,0);break;case 2:r.translate(e.justifyOffset+(e.boxWidth-e.lineWidths[n])/2,0,0);break}r.translate(a,l,0)},ITextElement.prototype.buildColor=function(e){return"rgb("+Math.round(e[0]*255)+","+Math.round(e[1]*255)+","+Math.round(e[2]*255)+")"},ITextElement.prototype.emptyProp=new LetterProps,ITextElement.prototype.destroy=function(){};var emptyShapeData={shapes:[]};function SVGTextLottieElement(e,r,n){this.textSpans=[],this.renderType="svg",this.initElement(e,r,n)}extendPrototype([BaseElement,TransformElement,SVGBaseElement,HierarchyElement,FrameElement,RenderableDOMElement,ITextElement],SVGTextLottieElement),SVGTextLottieElement.prototype.createContent=function(){this.data.singleShape&&!this.globalData.fontManager.chars&&(this.textContainer=createNS("text"))},SVGTextLottieElement.prototype.buildTextContents=function(e){for(var r=0,n=e.length,a=[],l="";r<n;)e[r]===String.fromCharCode(13)||e[r]===String.fromCharCode(3)?(a.push(l),l=""):l+=e[r],r+=1;return a.push(l),a},SVGTextLottieElement.prototype.buildNewText=function(){this.addDynamicProperty(this);var e,r,n=this.textProperty.currentData;this.renderedLetters=createSizedArray(n?n.l.length:0),n.fc?this.layerElement.setAttribute("fill",this.buildColor(n.fc)):this.layerElement.setAttribute("fill","rgba(0,0,0,0)"),n.sc&&(this.layerElement.setAttribute("stroke",this.buildColor(n.sc)),this.layerElement.setAttribute("stroke-width",n.sw)),this.layerElement.setAttribute("font-size",n.finalSize);var a=this.globalData.fontManager.getFontByName(n.f);if(a.fClass)this.layerElement.setAttribute("class",a.fClass);else{this.layerElement.setAttribute("font-family",a.fFamily);var l=n.fWeight,c=n.fStyle;this.layerElement.setAttribute("font-style",c),this.layerElement.setAttribute("font-weight",l)}this.layerElement.setAttribute("aria-label",n.t);var u=n.l||[],d=!!this.globalData.fontManager.chars;r=u.length;var m,E=this.mHelper,C="",_=this.data.singleShape,A=0,T=0,S=!0,P=n.tr*.001*n.finalSize;if(_&&!d&&!n.sz){var k=this.textContainer,x="start";switch(n.j){case 1:x="end";break;case 2:x="middle";break;default:x="start";break}k.setAttribute("text-anchor",x),k.setAttribute("letter-spacing",P);var b=this.buildTextContents(n.finalText);for(r=b.length,T=n.ps?n.ps[1]+n.ascent:0,e=0;e<r;e+=1)m=this.textSpans[e].span||createNS("tspan"),m.textContent=b[e],m.setAttribute("x",0),m.setAttribute("y",T),m.style.display="inherit",k.appendChild(m),this.textSpans[e]||(this.textSpans[e]={span:null,glyph:null}),this.textSpans[e].span=m,T+=n.finalLineHeight;this.layerElement.appendChild(k)}else{var g=this.textSpans.length,y;for(e=0;e<r;e+=1){if(this.textSpans[e]||(this.textSpans[e]={span:null,childSpan:null,glyph:null}),!d||!_||e===0){if(m=g>e?this.textSpans[e].span:createNS(d?"g":"text"),g<=e){if(m.setAttribute("stroke-linecap","butt"),m.setAttribute("stroke-linejoin","round"),m.setAttribute("stroke-miterlimit","4"),this.textSpans[e].span=m,d){var I=createNS("g");m.appendChild(I),this.textSpans[e].childSpan=I}this.textSpans[e].span=m,this.layerElement.appendChild(m)}m.style.display="inherit"}if(E.reset(),E.scale(n.finalSize/100,n.finalSize/100),_&&(u[e].n&&(A=-P,T+=n.yOffset,T+=S?1:0,S=!1),this.applyTextPropertiesToMatrix(n,E,u[e].line,A,T),A+=u[e].l||0,A+=P),d){y=this.globalData.fontManager.getCharData(n.finalText[e],a.fStyle,this.globalData.fontManager.getFontByName(n.f).fFamily);var M;if(y.t===1)M=new SVGCompElement(y.data,this.globalData,this);else{var j=emptyShapeData;y.data&&y.data.shapes&&(j=y.data),M=new SVGShapeElement(j,this.globalData,this)}if(this.textSpans[e].glyph){var O=this.textSpans[e].glyph;this.textSpans[e].childSpan.removeChild(O.layerElement),O.destroy()}this.textSpans[e].glyph=M,M._debug=!0,M.prepareFrame(0),M.renderFrame(),this.textSpans[e].childSpan.appendChild(M.layerElement),this.textSpans[e].childSpan.setAttribute("transform","scale("+n.finalSize/100+","+n.finalSize/100+")")}else _&&m.setAttribute("transform","translate("+E.props[12]+","+E.props[13]+")"),m.textContent=u[e].val,m.setAttributeNS("http://www.w3.org/XML/1998/namespace","xml:space","preserve")}_&&m&&m.setAttribute("d",C)}for(;e<this.textSpans.length;)this.textSpans[e].span.style.display="none",e+=1;this._sizeChanged=!0},SVGTextLottieElement.prototype.sourceRectAtTime=function(){if(this.prepareFrame(this.comp.renderedFrame-this.data.st),this.renderInnerContent(),this._sizeChanged){this._sizeChanged=!1;var e=this.layerElement.getBBox();this.bbox={top:e.y,left:e.x,width:e.width,height:e.height}}return this.bbox},SVGTextLottieElement.prototype.getValue=function(){var e,r=this.textSpans.length,n;for(this.renderedFrame=this.comp.renderedFrame,e=0;e<r;e+=1)n=this.textSpans[e].glyph,n&&(n.prepareFrame(this.comp.renderedFrame-this.data.st),n._mdf&&(this._mdf=!0))},SVGTextLottieElement.prototype.renderInnerContent=function(){if((!this.data.singleShape||this._mdf)&&(this.textAnimator.getMeasures(this.textProperty.currentData,this.lettersChangedFlag),this.lettersChangedFlag||this.textAnimator.lettersChangedFlag)){this._sizeChanged=!0;var e,r,n=this.textAnimator.renderedLetters,a=this.textProperty.currentData.l;r=a.length;var l,c,u;for(e=0;e<r;e+=1)a[e].n||(l=n[e],c=this.textSpans[e].span,u=this.textSpans[e].glyph,u&&u.renderFrame(),l._mdf.m&&c.setAttribute("transform",l.m),l._mdf.o&&c.setAttribute("opacity",l.o),l._mdf.sw&&c.setAttribute("stroke-width",l.sw),l._mdf.sc&&c.setAttribute("stroke",l.sc),l._mdf.fc&&c.setAttribute("fill",l.fc))}};function ISolidElement(e,r,n){this.initElement(e,r,n)}extendPrototype([IImageElement],ISolidElement),ISolidElement.prototype.createContent=function(){var e=createNS("rect");e.setAttribute("width",this.data.sw),e.setAttribute("height",this.data.sh),e.setAttribute("fill",this.data.sc),this.layerElement.appendChild(e)};function NullElement(e,r,n){this.initFrame(),this.initBaseData(e,r,n),this.initFrame(),this.initTransform(e,r,n),this.initHierarchy()}NullElement.prototype.prepareFrame=function(e){this.prepareProperties(e,!0)},NullElement.prototype.renderFrame=function(){},NullElement.prototype.getBaseElement=function(){return null},NullElement.prototype.destroy=function(){},NullElement.prototype.sourceRectAtTime=function(){},NullElement.prototype.hide=function(){},extendPrototype([BaseElement,TransformElement,HierarchyElement,FrameElement],NullElement);function SVGRendererBase(){}extendPrototype([BaseRenderer],SVGRendererBase),SVGRendererBase.prototype.createNull=function(e){return new NullElement(e,this.globalData,this)},SVGRendererBase.prototype.createShape=function(e){return new SVGShapeElement(e,this.globalData,this)},SVGRendererBase.prototype.createText=function(e){return new SVGTextLottieElement(e,this.globalData,this)},SVGRendererBase.prototype.createImage=function(e){return new IImageElement(e,this.globalData,this)},SVGRendererBase.prototype.createSolid=function(e){return new ISolidElement(e,this.globalData,this)},SVGRendererBase.prototype.configAnimation=function(e){this.svgElement.setAttribute("xmlns","http://www.w3.org/2000/svg"),this.renderConfig.viewBoxSize?this.svgElement.setAttribute("viewBox",this.renderConfig.viewBoxSize):this.svgElement.setAttribute("viewBox","0 0 "+e.w+" "+e.h),this.renderConfig.viewBoxOnly||(this.svgElement.setAttribute("width",e.w),this.svgElement.setAttribute("height",e.h),this.svgElement.style.width="100%",this.svgElement.style.height="100%",this.svgElement.style.transform="translate3d(0,0,0)",this.svgElement.style.contentVisibility=this.renderConfig.contentVisibility),this.renderConfig.width&&this.svgElement.setAttribute("width",this.renderConfig.width),this.renderConfig.height&&this.svgElement.setAttribute("height",this.renderConfig.height),this.renderConfig.className&&this.svgElement.setAttribute("class",this.renderConfig.className),this.renderConfig.id&&this.svgElement.setAttribute("id",this.renderConfig.id),this.renderConfig.focusable!==void 0&&this.svgElement.setAttribute("focusable",this.renderConfig.focusable),this.svgElement.setAttribute("preserveAspectRatio",this.renderConfig.preserveAspectRatio),this.animationItem.wrapper.appendChild(this.svgElement);var r=this.globalData.defs;this.setupGlobalData(e,r),this.globalData.progressiveLoad=this.renderConfig.progressiveLoad,this.data=e;var n=createNS("clipPath"),a=createNS("rect");a.setAttribute("width",e.w),a.setAttribute("height",e.h),a.setAttribute("x",0),a.setAttribute("y",0);var l=createElementID();n.setAttribute("id",l),n.appendChild(a),this.layerElement.setAttribute("clip-path","url("+getLocationHref()+"#"+l+")"),r.appendChild(n),this.layers=e.layers,this.elements=createSizedArray(e.layers.length)},SVGRendererBase.prototype.destroy=function(){this.animationItem.wrapper&&(this.animationItem.wrapper.innerText=""),this.layerElement=null,this.globalData.defs=null;var e,r=this.layers?this.layers.length:0;for(e=0;e<r;e+=1)this.elements[e]&&this.elements[e].destroy();this.elements.length=0,this.destroyed=!0,this.animationItem=null},SVGRendererBase.prototype.updateContainerSize=function(){},SVGRendererBase.prototype.buildItem=function(e){var r=this.elements;if(!(r[e]||this.layers[e].ty===99)){r[e]=!0;var n=this.createItem(this.layers[e]);r[e]=n,getExpressionsPlugin()&&(this.layers[e].ty===0&&this.globalData.projectInterface.registerComposition(n),n.initExpressions()),this.appendElementInPos(n,e),this.layers[e].tt&&(!this.elements[e-1]||this.elements[e-1]===!0?(this.buildItem(e-1),this.addPendingElement(n)):n.setMatte(r[e-1].layerId))}},SVGRendererBase.prototype.checkPendingElements=function(){for(;this.pendingElements.length;){var e=this.pendingElements.pop();if(e.checkParenting(),e.data.tt)for(var r=0,n=this.elements.length;r<n;){if(this.elements[r]===e){e.setMatte(this.elements[r-1].layerId);break}r+=1}}},SVGRendererBase.prototype.renderFrame=function(e){if(!(this.renderedFrame===e||this.destroyed)){e===null?e=this.renderedFrame:this.renderedFrame=e,this.globalData.frameNum=e,this.globalData.frameId+=1,this.globalData.projectInterface.currentFrame=e,this.globalData._mdf=!1;var r,n=this.layers.length;for(this.completeLayers||this.checkLayers(e),r=n-1;r>=0;r-=1)(this.completeLayers||this.elements[r])&&this.elements[r].prepareFrame(e-this.layers[r].st);if(this.globalData._mdf)for(r=0;r<n;r+=1)(this.completeLayers||this.elements[r])&&this.elements[r].renderFrame()}},SVGRendererBase.prototype.appendElementInPos=function(e,r){var n=e.getBaseElement();if(!!n){for(var a=0,l;a<r;)this.elements[a]&&this.elements[a]!==!0&&this.elements[a].getBaseElement()&&(l=this.elements[a].getBaseElement()),a+=1;l?this.layerElement.insertBefore(n,l):this.layerElement.appendChild(n)}},SVGRendererBase.prototype.hide=function(){this.layerElement.style.display="none"},SVGRendererBase.prototype.show=function(){this.layerElement.style.display="block"};function ICompElement(){}extendPrototype([BaseElement,TransformElement,HierarchyElement,FrameElement,RenderableDOMElement],ICompElement),ICompElement.prototype.initElement=function(e,r,n){this.initFrame(),this.initBaseData(e,r,n),this.initTransform(e,r,n),this.initRenderable(),this.initHierarchy(),this.initRendererElement(),this.createContainerElements(),this.createRenderableComponents(),(this.data.xt||!r.progressiveLoad)&&this.buildAllItems(),this.hide()},ICompElement.prototype.prepareFrame=function(e){if(this._mdf=!1,this.prepareRenderableFrame(e),this.prepareProperties(e,this.isInRange),!(!this.isInRange&&!this.data.xt)){if(this.tm._placeholder)this.renderedFrame=e/this.data.sr;else{var r=this.tm.v;r===this.data.op&&(r=this.data.op-1),this.renderedFrame=r}var n,a=this.elements.length;for(this.completeLayers||this.checkLayers(this.renderedFrame),n=a-1;n>=0;n-=1)(this.completeLayers||this.elements[n])&&(this.elements[n].prepareFrame(this.renderedFrame-this.layers[n].st),this.elements[n]._mdf&&(this._mdf=!0))}},ICompElement.prototype.renderInnerContent=function(){var e,r=this.layers.length;for(e=0;e<r;e+=1)(this.completeLayers||this.elements[e])&&this.elements[e].renderFrame()},ICompElement.prototype.setElements=function(e){this.elements=e},ICompElement.prototype.getElements=function(){return this.elements},ICompElement.prototype.destroyElements=function(){var e,r=this.layers.length;for(e=0;e<r;e+=1)this.elements[e]&&this.elements[e].destroy()},ICompElement.prototype.destroy=function(){this.destroyElements(),this.destroyBaseElement()};function SVGCompElement(e,r,n){this.layers=e.layers,this.supports3d=!0,this.completeLayers=!1,this.pendingElements=[],this.elements=this.layers?createSizedArray(this.layers.length):[],this.initElement(e,r,n),this.tm=e.tm?PropertyFactory.getProp(this,e.tm,0,r.frameRate,this):{_placeholder:!0}}extendPrototype([SVGRendererBase,ICompElement,SVGBaseElement],SVGCompElement),SVGCompElement.prototype.createComp=function(e){return new SVGCompElement(e,this.globalData,this)};function SVGRenderer(e,r){this.animationItem=e,this.layers=null,this.renderedFrame=-1,this.svgElement=createNS("svg");var n="";if(r&&r.title){var a=createNS("title"),l=createElementID();a.setAttribute("id",l),a.textContent=r.title,this.svgElement.appendChild(a),n+=l}if(r&&r.description){var c=createNS("desc"),u=createElementID();c.setAttribute("id",u),c.textContent=r.description,this.svgElement.appendChild(c),n+=" "+u}n&&this.svgElement.setAttribute("aria-labelledby",n);var d=createNS("defs");this.svgElement.appendChild(d);var m=createNS("g");this.svgElement.appendChild(m),this.layerElement=m,this.renderConfig={preserveAspectRatio:r&&r.preserveAspectRatio||"xMidYMid meet",imagePreserveAspectRatio:r&&r.imagePreserveAspectRatio||"xMidYMid slice",contentVisibility:r&&r.contentVisibility||"visible",progressiveLoad:r&&r.progressiveLoad||!1,hideOnTransparent:!(r&&r.hideOnTransparent===!1),viewBoxOnly:r&&r.viewBoxOnly||!1,viewBoxSize:r&&r.viewBoxSize||!1,className:r&&r.className||"",id:r&&r.id||"",focusable:r&&r.focusable,filterSize:{width:r&&r.filterSize&&r.filterSize.width||"100%",height:r&&r.filterSize&&r.filterSize.height||"100%",x:r&&r.filterSize&&r.filterSize.x||"0%",y:r&&r.filterSize&&r.filterSize.y||"0%"},width:r&&r.width,height:r&&r.height},this.globalData={_mdf:!1,frameNum:-1,defs:d,renderConfig:this.renderConfig},this.elements=[],this.pendingElements=[],this.destroyed=!1,this.rendererType="svg"}extendPrototype([SVGRendererBase],SVGRenderer),SVGRenderer.prototype.createComp=function(e){return new SVGCompElement(e,this.globalData,this)};function CVContextData(){this.saved=[],this.cArrPos=0,this.cTr=new Matrix,this.cO=1;var e,r=15;for(this.savedOp=createTypedArray("float32",r),e=0;e<r;e+=1)this.saved[e]=createTypedArray("float32",16);this._length=r}CVContextData.prototype.duplicate=function(){var e=this._length*2,r=this.savedOp;this.savedOp=createTypedArray("float32",e),this.savedOp.set(r);var n=0;for(n=this._length;n<e;n+=1)this.saved[n]=createTypedArray("float32",16);this._length=e},CVContextData.prototype.reset=function(){this.cArrPos=0,this.cTr.reset(),this.cO=1};function ShapeTransformManager(){this.sequences={},this.sequenceList=[],this.transform_key_count=0}ShapeTransformManager.prototype={addTransformSequence:function(r){var n,a=r.length,l="_";for(n=0;n<a;n+=1)l+=r[n].transform.key+"_";var c=this.sequences[l];return c||(c={transforms:[].concat(r),finalTransform:new Matrix,_mdf:!1},this.sequences[l]=c,this.sequenceList.push(c)),c},processSequence:function(r,n){for(var a=0,l=r.transforms.length,c=n;a<l&&!n;){if(r.transforms[a].transform.mProps._mdf){c=!0;break}a+=1}if(c){var u;for(r.finalTransform.reset(),a=l-1;a>=0;a-=1)u=r.transforms[a].transform.mProps.v.props,r.finalTransform.transform(u[0],u[1],u[2],u[3],u[4],u[5],u[6],u[7],u[8],u[9],u[10],u[11],u[12],u[13],u[14],u[15])}r._mdf=c},processSequences:function(r){var n,a=this.sequenceList.length;for(n=0;n<a;n+=1)this.processSequence(this.sequenceList[n],r)},getNewKey:function(){return this.transform_key_count+=1,"_"+this.transform_key_count}};function CVEffects(){}CVEffects.prototype.renderFrame=function(){};function CVMaskElement(e,r){this.data=e,this.element=r,this.masksProperties=this.data.masksProperties||[],this.viewData=createSizedArray(this.masksProperties.length);var n,a=this.masksProperties.length,l=!1;for(n=0;n<a;n+=1)this.masksProperties[n].mode!=="n"&&(l=!0),this.viewData[n]=ShapePropertyFactory.getShapeProp(this.element,this.masksProperties[n],3);this.hasMasks=l,l&&this.element.addRenderableComponent(this)}CVMaskElement.prototype.renderFrame=function(){if(!!this.hasMasks){var e=this.element.finalTransform.mat,r=this.element.canvasContext,n,a=this.masksProperties.length,l,c,u;for(r.beginPath(),n=0;n<a;n+=1)if(this.masksProperties[n].mode!=="n"){this.masksProperties[n].inv&&(r.moveTo(0,0),r.lineTo(this.element.globalData.compSize.w,0),r.lineTo(this.element.globalData.compSize.w,this.element.globalData.compSize.h),r.lineTo(0,this.element.globalData.compSize.h),r.lineTo(0,0)),u=this.viewData[n].v,l=e.applyToPointArray(u.v[0][0],u.v[0][1],0),r.moveTo(l[0],l[1]);var d,m=u._length;for(d=1;d<m;d+=1)c=e.applyToTriplePoints(u.o[d-1],u.i[d],u.v[d]),r.bezierCurveTo(c[0],c[1],c[2],c[3],c[4],c[5]);c=e.applyToTriplePoints(u.o[d-1],u.i[0],u.v[0]),r.bezierCurveTo(c[0],c[1],c[2],c[3],c[4],c[5])}this.element.globalData.renderer.save(!0),r.clip()}},CVMaskElement.prototype.getMaskProperty=MaskElement.prototype.getMaskProperty,CVMaskElement.prototype.destroy=function(){this.element=null};function CVBaseElement(){}CVBaseElement.prototype={createElements:function(){},initRendererElement:function(){},createContainerElements:function(){this.canvasContext=this.globalData.canvasContext,this.renderableEffectsManager=new CVEffects(this)},createContent:function(){},setBlendMode:function(){var r=this.globalData;if(r.blendMode!==this.data.bm){r.blendMode=this.data.bm;var n=getBlendMode(this.data.bm);r.canvasContext.globalCompositeOperation=n}},createRenderableComponents:function(){this.maskManager=new CVMaskElement(this.data,this)},hideElement:function(){!this.hidden&&(!this.isInRange||this.isTransparent)&&(this.hidden=!0)},showElement:function(){this.isInRange&&!this.isTransparent&&(this.hidden=!1,this._isFirstFrame=!0,this.maskManager._isFirstFrame=!0)},renderFrame:function(){if(!(this.hidden||this.data.hd)){this.renderTransform(),this.renderRenderable(),this.setBlendMode();var r=this.data.ty===0;this.globalData.renderer.save(r),this.globalData.renderer.ctxTransform(this.finalTransform.mat.props),this.globalData.renderer.ctxOpacity(this.finalTransform.mProp.o.v),this.renderInnerContent(),this.globalData.renderer.restore(r),this.maskManager.hasMasks&&this.globalData.renderer.restore(!0),this._isFirstFrame&&(this._isFirstFrame=!1)}},destroy:function(){this.canvasContext=null,this.data=null,this.globalData=null,this.maskManager.destroy()},mHelper:new Matrix},CVBaseElement.prototype.hide=CVBaseElement.prototype.hideElement,CVBaseElement.prototype.show=CVBaseElement.prototype.showElement;function CVShapeData(e,r,n,a){this.styledShapes=[],this.tr=[0,0,0,0,0,0];var l=4;r.ty==="rc"?l=5:r.ty==="el"?l=6:r.ty==="sr"&&(l=7),this.sh=ShapePropertyFactory.getShapeProp(e,r,l,e);var c,u=n.length,d;for(c=0;c<u;c+=1)n[c].closed||(d={transforms:a.addTransformSequence(n[c].transforms),trNodes:[]},this.styledShapes.push(d),n[c].elements.push(d))}CVShapeData.prototype.setAsAnimated=SVGShapeData.prototype.setAsAnimated;function CVShapeElement(e,r,n){this.shapes=[],this.shapesData=e.shapes,this.stylesList=[],this.itemsData=[],this.prevViewData=[],this.shapeModifiers=[],this.processedElements=[],this.transformsManager=new ShapeTransformManager,this.initElement(e,r,n)}extendPrototype([BaseElement,TransformElement,CVBaseElement,IShapeElement,HierarchyElement,FrameElement,RenderableElement],CVShapeElement),CVShapeElement.prototype.initElement=RenderableDOMElement.prototype.initElement,CVShapeElement.prototype.transformHelper={opacity:1,_opMdf:!1},CVShapeElement.prototype.dashResetter=[],CVShapeElement.prototype.createContent=function(){this.searchShapes(this.shapesData,this.itemsData,this.prevViewData,!0,[])},CVShapeElement.prototype.createStyleElement=function(e,r){var n={data:e,type:e.ty,preTransforms:this.transformsManager.addTransformSequence(r),transforms:[],elements:[],closed:e.hd===!0},a={};if(e.ty==="fl"||e.ty==="st"?(a.c=PropertyFactory.getProp(this,e.c,1,255,this),a.c.k||(n.co="rgb("+bmFloor(a.c.v[0])+","+bmFloor(a.c.v[1])+","+bmFloor(a.c.v[2])+")")):(e.ty==="gf"||e.ty==="gs")&&(a.s=PropertyFactory.getProp(this,e.s,1,null,this),a.e=PropertyFactory.getProp(this,e.e,1,null,this),a.h=PropertyFactory.getProp(this,e.h||{k:0},0,.01,this),a.a=PropertyFactory.getProp(this,e.a||{k:0},0,degToRads,this),a.g=new GradientProperty(this,e.g,this)),a.o=PropertyFactory.getProp(this,e.o,0,.01,this),e.ty==="st"||e.ty==="gs"){if(n.lc=lineCapEnum[e.lc||2],n.lj=lineJoinEnum[e.lj||2],e.lj==1&&(n.ml=e.ml),a.w=PropertyFactory.getProp(this,e.w,0,null,this),a.w.k||(n.wi=a.w.v),e.d){var l=new DashProperty(this,e.d,"canvas",this);a.d=l,a.d.k||(n.da=a.d.dashArray,n.do=a.d.dashoffset[0])}}else n.r=e.r===2?"evenodd":"nonzero";return this.stylesList.push(n),a.style=n,a},CVShapeElement.prototype.createGroupElement=function(){var e={it:[],prevViewData:[]};return e},CVShapeElement.prototype.createTransformElement=function(e){var r={transform:{opacity:1,_opMdf:!1,key:this.transformsManager.getNewKey(),op:PropertyFactory.getProp(this,e.o,0,.01,this),mProps:TransformPropertyFactory.getTransformProperty(this,e,this)}};return r},CVShapeElement.prototype.createShapeElement=function(e){var r=new CVShapeData(this,e,this.stylesList,this.transformsManager);return this.shapes.push(r),this.addShapeToModifiers(r),r},CVShapeElement.prototype.reloadShapes=function(){this._isFirstFrame=!0;var e,r=this.itemsData.length;for(e=0;e<r;e+=1)this.prevViewData[e]=this.itemsData[e];for(this.searchShapes(this.shapesData,this.itemsData,this.prevViewData,!0,[]),r=this.dynamicProperties.length,e=0;e<r;e+=1)this.dynamicProperties[e].getValue();this.renderModifiers(),this.transformsManager.processSequences(this._isFirstFrame)},CVShapeElement.prototype.addTransformToStyleList=function(e){var r,n=this.stylesList.length;for(r=0;r<n;r+=1)this.stylesList[r].closed||this.stylesList[r].transforms.push(e)},CVShapeElement.prototype.removeTransformFromStyleList=function(){var e,r=this.stylesList.length;for(e=0;e<r;e+=1)this.stylesList[e].closed||this.stylesList[e].transforms.pop()},CVShapeElement.prototype.closeStyles=function(e){var r,n=e.length;for(r=0;r<n;r+=1)e[r].closed=!0},CVShapeElement.prototype.searchShapes=function(e,r,n,a,l){var c,u=e.length-1,d,m,E=[],C=[],_,A,T,S=[].concat(l);for(c=u;c>=0;c-=1){if(_=this.searchProcessedElement(e[c]),_?r[c]=n[_-1]:e[c]._shouldRender=a,e[c].ty==="fl"||e[c].ty==="st"||e[c].ty==="gf"||e[c].ty==="gs")_?r[c].style.closed=!1:r[c]=this.createStyleElement(e[c],S),E.push(r[c].style);else if(e[c].ty==="gr"){if(!_)r[c]=this.createGroupElement(e[c]);else for(m=r[c].it.length,d=0;d<m;d+=1)r[c].prevViewData[d]=r[c].it[d];this.searchShapes(e[c].it,r[c].it,r[c].prevViewData,a,S)}else e[c].ty==="tr"?(_||(T=this.createTransformElement(e[c]),r[c]=T),S.push(r[c]),this.addTransformToStyleList(r[c])):e[c].ty==="sh"||e[c].ty==="rc"||e[c].ty==="el"||e[c].ty==="sr"?_||(r[c]=this.createShapeElement(e[c])):e[c].ty==="tm"||e[c].ty==="rd"||e[c].ty==="pb"?(_?(A=r[c],A.closed=!1):(A=ShapeModifiers.getModifier(e[c].ty),A.init(this,e[c]),r[c]=A,this.shapeModifiers.push(A)),C.push(A)):e[c].ty==="rp"&&(_?(A=r[c],A.closed=!0):(A=ShapeModifiers.getModifier(e[c].ty),r[c]=A,A.init(this,e,c,r),this.shapeModifiers.push(A),a=!1),C.push(A));this.addProcessedElement(e[c],c+1)}for(this.removeTransformFromStyleList(),this.closeStyles(E),u=C.length,c=0;c<u;c+=1)C[c].closed=!0},CVShapeElement.prototype.renderInnerContent=function(){this.transformHelper.opacity=1,this.transformHelper._opMdf=!1,this.renderModifiers(),this.transformsManager.processSequences(this._isFirstFrame),this.renderShape(this.transformHelper,this.shapesData,this.itemsData,!0)},CVShapeElement.prototype.renderShapeTransform=function(e,r){(e._opMdf||r.op._mdf||this._isFirstFrame)&&(r.opacity=e.opacity,r.opacity*=r.op.v,r._opMdf=!0)},CVShapeElement.prototype.drawLayer=function(){var e,r=this.stylesList.length,n,a,l,c,u,d,m=this.globalData.renderer,E=this.globalData.canvasContext,C,_;for(e=0;e<r;e+=1)if(_=this.stylesList[e],C=_.type,!((C==="st"||C==="gs")&&_.wi===0||!_.data._shouldRender||_.coOp===0||this.globalData.currentGlobalAlpha===0)){for(m.save(),u=_.elements,C==="st"||C==="gs"?(E.strokeStyle=C==="st"?_.co:_.grd,E.lineWidth=_.wi,E.lineCap=_.lc,E.lineJoin=_.lj,E.miterLimit=_.ml||0):E.fillStyle=C==="fl"?_.co:_.grd,m.ctxOpacity(_.coOp),C!=="st"&&C!=="gs"&&E.beginPath(),m.ctxTransform(_.preTransforms.finalTransform.props),a=u.length,n=0;n<a;n+=1){for((C==="st"||C==="gs")&&(E.beginPath(),_.da&&(E.setLineDash(_.da),E.lineDashOffset=_.do)),d=u[n].trNodes,c=d.length,l=0;l<c;l+=1)d[l].t==="m"?E.moveTo(d[l].p[0],d[l].p[1]):d[l].t==="c"?E.bezierCurveTo(d[l].pts[0],d[l].pts[1],d[l].pts[2],d[l].pts[3],d[l].pts[4],d[l].pts[5]):E.closePath();(C==="st"||C==="gs")&&(E.stroke(),_.da&&E.setLineDash(this.dashResetter))}C!=="st"&&C!=="gs"&&E.fill(_.r),m.restore()}},CVShapeElement.prototype.renderShape=function(e,r,n,a){var l,c=r.length-1,u;for(u=e,l=c;l>=0;l-=1)r[l].ty==="tr"?(u=n[l].transform,this.renderShapeTransform(e,u)):r[l].ty==="sh"||r[l].ty==="el"||r[l].ty==="rc"||r[l].ty==="sr"?this.renderPath(r[l],n[l]):r[l].ty==="fl"?this.renderFill(r[l],n[l],u):r[l].ty==="st"?this.renderStroke(r[l],n[l],u):r[l].ty==="gf"||r[l].ty==="gs"?this.renderGradientFill(r[l],n[l],u):r[l].ty==="gr"?this.renderShape(u,r[l].it,n[l].it):r[l].ty==="tm";a&&this.drawLayer()},CVShapeElement.prototype.renderStyledShape=function(e,r){if(this._isFirstFrame||r._mdf||e.transforms._mdf){var n=e.trNodes,a=r.paths,l,c,u,d=a._length;n.length=0;var m=e.transforms.finalTransform;for(u=0;u<d;u+=1){var E=a.shapes[u];if(E&&E.v){for(c=E._length,l=1;l<c;l+=1)l===1&&n.push({t:"m",p:m.applyToPointArray(E.v[0][0],E.v[0][1],0)}),n.push({t:"c",pts:m.applyToTriplePoints(E.o[l-1],E.i[l],E.v[l])});c===1&&n.push({t:"m",p:m.applyToPointArray(E.v[0][0],E.v[0][1],0)}),E.c&&c&&(n.push({t:"c",pts:m.applyToTriplePoints(E.o[l-1],E.i[0],E.v[0])}),n.push({t:"z"}))}}e.trNodes=n}},CVShapeElement.prototype.renderPath=function(e,r){if(e.hd!==!0&&e._shouldRender){var n,a=r.styledShapes.length;for(n=0;n<a;n+=1)this.renderStyledShape(r.styledShapes[n],r.sh)}},CVShapeElement.prototype.renderFill=function(e,r,n){var a=r.style;(r.c._mdf||this._isFirstFrame)&&(a.co="rgb("+bmFloor(r.c.v[0])+","+bmFloor(r.c.v[1])+","+bmFloor(r.c.v[2])+")"),(r.o._mdf||n._opMdf||this._isFirstFrame)&&(a.coOp=r.o.v*n.opacity)},CVShapeElement.prototype.renderGradientFill=function(e,r,n){var a=r.style,l;if(!a.grd||r.g._mdf||r.s._mdf||r.e._mdf||e.t!==1&&(r.h._mdf||r.a._mdf)){var c=this.globalData.canvasContext,u=r.s.v,d=r.e.v;if(e.t===1)l=c.createLinearGradient(u[0],u[1],d[0],d[1]);else{var m=Math.sqrt(Math.pow(u[0]-d[0],2)+Math.pow(u[1]-d[1],2)),E=Math.atan2(d[1]-u[1],d[0]-u[0]),C=r.h.v;C>=1?C=.99:C<=-1&&(C=-.99);var _=m*C,A=Math.cos(E+r.a.v)*_+u[0],T=Math.sin(E+r.a.v)*_+u[1];l=c.createRadialGradient(A,T,0,u[0],u[1],m)}var S,P=e.g.p,k=r.g.c,x=1;for(S=0;S<P;S+=1)r.g._hasOpacity&&r.g._collapsable&&(x=r.g.o[S*2+1]),l.addColorStop(k[S*4]/100,"rgba("+k[S*4+1]+","+k[S*4+2]+","+k[S*4+3]+","+x+")");a.grd=l}a.coOp=r.o.v*n.opacity},CVShapeElement.prototype.renderStroke=function(e,r,n){var a=r.style,l=r.d;l&&(l._mdf||this._isFirstFrame)&&(a.da=l.dashArray,a.do=l.dashoffset[0]),(r.c._mdf||this._isFirstFrame)&&(a.co="rgb("+bmFloor(r.c.v[0])+","+bmFloor(r.c.v[1])+","+bmFloor(r.c.v[2])+")"),(r.o._mdf||n._opMdf||this._isFirstFrame)&&(a.coOp=r.o.v*n.opacity),(r.w._mdf||this._isFirstFrame)&&(a.wi=r.w.v)},CVShapeElement.prototype.destroy=function(){this.shapesData=null,this.globalData=null,this.canvasContext=null,this.stylesList.length=0,this.itemsData.length=0};function CVTextElement(e,r,n){this.textSpans=[],this.yOffset=0,this.fillColorAnim=!1,this.strokeColorAnim=!1,this.strokeWidthAnim=!1,this.stroke=!1,this.fill=!1,this.justifyOffset=0,this.currentRender=null,this.renderType="canvas",this.values={fill:"rgba(0,0,0,0)",stroke:"rgba(0,0,0,0)",sWidth:0,fValue:""},this.initElement(e,r,n)}extendPrototype([BaseElement,TransformElement,CVBaseElement,HierarchyElement,FrameElement,RenderableElement,ITextElement],CVTextElement),CVTextElement.prototype.tHelper=createTag("canvas").getContext("2d"),CVTextElement.prototype.buildNewText=function(){var e=this.textProperty.currentData;this.renderedLetters=createSizedArray(e.l?e.l.length:0);var r=!1;e.fc?(r=!0,this.values.fill=this.buildColor(e.fc)):this.values.fill="rgba(0,0,0,0)",this.fill=r;var n=!1;e.sc&&(n=!0,this.values.stroke=this.buildColor(e.sc),this.values.sWidth=e.sw);var a=this.globalData.fontManager.getFontByName(e.f),l,c,u=e.l,d=this.mHelper;this.stroke=n,this.values.fValue=e.finalSize+"px "+this.globalData.fontManager.getFontByName(e.f).fFamily,c=e.finalText.length;var m,E,C,_,A,T,S,P,k,x,b=this.data.singleShape,g=e.tr*.001*e.finalSize,y=0,I=0,M=!0,j=0;for(l=0;l<c;l+=1){m=this.globalData.fontManager.getCharData(e.finalText[l],a.fStyle,this.globalData.fontManager.getFontByName(e.f).fFamily),E=m&&m.data||{},d.reset(),b&&u[l].n&&(y=-g,I+=e.yOffset,I+=M?1:0,M=!1),A=E.shapes?E.shapes[0].it:[],S=A.length,d.scale(e.finalSize/100,e.finalSize/100),b&&this.applyTextPropertiesToMatrix(e,d,u[l].line,y,I),k=createSizedArray(S-1);var O=0;for(T=0;T<S;T+=1)if(A[T].ty==="sh"){for(_=A[T].ks.k.i.length,P=A[T].ks.k,x=[],C=1;C<_;C+=1)C===1&&x.push(d.applyToX(P.v[0][0],P.v[0][1],0),d.applyToY(P.v[0][0],P.v[0][1],0)),x.push(d.applyToX(P.o[C-1][0],P.o[C-1][1],0),d.applyToY(P.o[C-1][0],P.o[C-1][1],0),d.applyToX(P.i[C][0],P.i[C][1],0),d.applyToY(P.i[C][0],P.i[C][1],0),d.applyToX(P.v[C][0],P.v[C][1],0),d.applyToY(P.v[C][0],P.v[C][1],0));x.push(d.applyToX(P.o[C-1][0],P.o[C-1][1],0),d.applyToY(P.o[C-1][0],P.o[C-1][1],0),d.applyToX(P.i[0][0],P.i[0][1],0),d.applyToY(P.i[0][0],P.i[0][1],0),d.applyToX(P.v[0][0],P.v[0][1],0),d.applyToY(P.v[0][0],P.v[0][1],0)),k[O]=x,O+=1}b&&(y+=u[l].l,y+=g),this.textSpans[j]?this.textSpans[j].elem=k:this.textSpans[j]={elem:k},j+=1}},CVTextElement.prototype.renderInnerContent=function(){var e=this.canvasContext;e.font=this.values.fValue,e.lineCap="butt",e.lineJoin="miter",e.miterLimit=4,this.data.singleShape||this.textAnimator.getMeasures(this.textProperty.currentData,this.lettersChangedFlag);var r,n,a,l,c,u,d=this.textAnimator.renderedLetters,m=this.textProperty.currentData.l;n=m.length;var E,C=null,_=null,A=null,T,S;for(r=0;r<n;r+=1)if(!m[r].n){if(E=d[r],E&&(this.globalData.renderer.save(),this.globalData.renderer.ctxTransform(E.p),this.globalData.renderer.ctxOpacity(E.o)),this.fill){for(E&&E.fc?C!==E.fc&&(C=E.fc,e.fillStyle=E.fc):C!==this.values.fill&&(C=this.values.fill,e.fillStyle=this.values.fill),T=this.textSpans[r].elem,l=T.length,this.globalData.canvasContext.beginPath(),a=0;a<l;a+=1)for(S=T[a],u=S.length,this.globalData.canvasContext.moveTo(S[0],S[1]),c=2;c<u;c+=6)this.globalData.canvasContext.bezierCurveTo(S[c],S[c+1],S[c+2],S[c+3],S[c+4],S[c+5]);this.globalData.canvasContext.closePath(),this.globalData.canvasContext.fill()}if(this.stroke){for(E&&E.sw?A!==E.sw&&(A=E.sw,e.lineWidth=E.sw):A!==this.values.sWidth&&(A=this.values.sWidth,e.lineWidth=this.values.sWidth),E&&E.sc?_!==E.sc&&(_=E.sc,e.strokeStyle=E.sc):_!==this.values.stroke&&(_=this.values.stroke,e.strokeStyle=this.values.stroke),T=this.textSpans[r].elem,l=T.length,this.globalData.canvasContext.beginPath(),a=0;a<l;a+=1)for(S=T[a],u=S.length,this.globalData.canvasContext.moveTo(S[0],S[1]),c=2;c<u;c+=6)this.globalData.canvasContext.bezierCurveTo(S[c],S[c+1],S[c+2],S[c+3],S[c+4],S[c+5]);this.globalData.canvasContext.closePath(),this.globalData.canvasContext.stroke()}E&&this.globalData.renderer.restore()}};function CVImageElement(e,r,n){this.assetData=r.getAssetData(e.refId),this.img=r.imageLoader.getAsset(this.assetData),this.initElement(e,r,n)}extendPrototype([BaseElement,TransformElement,CVBaseElement,HierarchyElement,FrameElement,RenderableElement],CVImageElement),CVImageElement.prototype.initElement=SVGShapeElement.prototype.initElement,CVImageElement.prototype.prepareFrame=IImageElement.prototype.prepareFrame,CVImageElement.prototype.createContent=function(){if(this.img.width&&(this.assetData.w!==this.img.width||this.assetData.h!==this.img.height)){var e=createTag("canvas");e.width=this.assetData.w,e.height=this.assetData.h;var r=e.getContext("2d"),n=this.img.width,a=this.img.height,l=n/a,c=this.assetData.w/this.assetData.h,u,d,m=this.assetData.pr||this.globalData.renderConfig.imagePreserveAspectRatio;l>c&&m==="xMidYMid slice"||l<c&&m!=="xMidYMid slice"?(d=a,u=d*c):(u=n,d=u/c),r.drawImage(this.img,(n-u)/2,(a-d)/2,u,d,0,0,this.assetData.w,this.assetData.h),this.img=e}},CVImageElement.prototype.renderInnerContent=function(){this.canvasContext.drawImage(this.img,0,0)},CVImageElement.prototype.destroy=function(){this.img=null};function CVSolidElement(e,r,n){this.initElement(e,r,n)}extendPrototype([BaseElement,TransformElement,CVBaseElement,HierarchyElement,FrameElement,RenderableElement],CVSolidElement),CVSolidElement.prototype.initElement=SVGShapeElement.prototype.initElement,CVSolidElement.prototype.prepareFrame=IImageElement.prototype.prepareFrame,CVSolidElement.prototype.renderInnerContent=function(){var e=this.canvasContext;e.fillStyle=this.data.sc,e.fillRect(0,0,this.data.sw,this.data.sh)};function CanvasRendererBase(e,r){this.animationItem=e,this.renderConfig={clearCanvas:r&&r.clearCanvas!==void 0?r.clearCanvas:!0,context:r&&r.context||null,progressiveLoad:r&&r.progressiveLoad||!1,preserveAspectRatio:r&&r.preserveAspectRatio||"xMidYMid meet",imagePreserveAspectRatio:r&&r.imagePreserveAspectRatio||"xMidYMid slice",contentVisibility:r&&r.contentVisibility||"visible",className:r&&r.className||"",id:r&&r.id||""},this.renderConfig.dpr=r&&r.dpr||1,this.animationItem.wrapper&&(this.renderConfig.dpr=r&&r.dpr||window.devicePixelRatio||1),this.renderedFrame=-1,this.globalData={frameNum:-1,_mdf:!1,renderConfig:this.renderConfig,currentGlobalAlpha:-1},this.contextData=new CVContextData,this.elements=[],this.pendingElements=[],this.transformMat=new Matrix,this.completeLayers=!1,this.rendererType="canvas"}extendPrototype([BaseRenderer],CanvasRendererBase),CanvasRendererBase.prototype.createShape=function(e){return new CVShapeElement(e,this.globalData,this)},CanvasRendererBase.prototype.createText=function(e){return new CVTextElement(e,this.globalData,this)},CanvasRendererBase.prototype.createImage=function(e){return new CVImageElement(e,this.globalData,this)},CanvasRendererBase.prototype.createSolid=function(e){return new CVSolidElement(e,this.globalData,this)},CanvasRendererBase.prototype.createNull=SVGRenderer.prototype.createNull,CanvasRendererBase.prototype.ctxTransform=function(e){if(!(e[0]===1&&e[1]===0&&e[4]===0&&e[5]===1&&e[12]===0&&e[13]===0)){if(!this.renderConfig.clearCanvas){this.canvasContext.transform(e[0],e[1],e[4],e[5],e[12],e[13]);return}this.transformMat.cloneFromProps(e);var r=this.contextData.cTr.props;this.transformMat.transform(r[0],r[1],r[2],r[3],r[4],r[5],r[6],r[7],r[8],r[9],r[10],r[11],r[12],r[13],r[14],r[15]),this.contextData.cTr.cloneFromProps(this.transformMat.props);var n=this.contextData.cTr.props;this.canvasContext.setTransform(n[0],n[1],n[4],n[5],n[12],n[13])}},CanvasRendererBase.prototype.ctxOpacity=function(e){if(!this.renderConfig.clearCanvas){this.canvasContext.globalAlpha*=e<0?0:e,this.globalData.currentGlobalAlpha=this.contextData.cO;return}this.contextData.cO*=e<0?0:e,this.globalData.currentGlobalAlpha!==this.contextData.cO&&(this.canvasContext.globalAlpha=this.contextData.cO,this.globalData.currentGlobalAlpha=this.contextData.cO)},CanvasRendererBase.prototype.reset=function(){if(!this.renderConfig.clearCanvas){this.canvasContext.restore();return}this.contextData.reset()},CanvasRendererBase.prototype.save=function(e){if(!this.renderConfig.clearCanvas){this.canvasContext.save();return}e&&this.canvasContext.save();var r=this.contextData.cTr.props;this.contextData._length<=this.contextData.cArrPos&&this.contextData.duplicate();var n,a=this.contextData.saved[this.contextData.cArrPos];for(n=0;n<16;n+=1)a[n]=r[n];this.contextData.savedOp[this.contextData.cArrPos]=this.contextData.cO,this.contextData.cArrPos+=1},CanvasRendererBase.prototype.restore=function(e){if(!this.renderConfig.clearCanvas){this.canvasContext.restore();return}e&&(this.canvasContext.restore(),this.globalData.blendMode="source-over"),this.contextData.cArrPos-=1;var r=this.contextData.saved[this.contextData.cArrPos],n,a=this.contextData.cTr.props;for(n=0;n<16;n+=1)a[n]=r[n];this.canvasContext.setTransform(r[0],r[1],r[4],r[5],r[12],r[13]),r=this.contextData.savedOp[this.contextData.cArrPos],this.contextData.cO=r,this.globalData.currentGlobalAlpha!==r&&(this.canvasContext.globalAlpha=r,this.globalData.currentGlobalAlpha=r)},CanvasRendererBase.prototype.configAnimation=function(e){if(this.animationItem.wrapper){this.animationItem.container=createTag("canvas");var r=this.animationItem.container.style;r.width="100%",r.height="100%";var n="0px 0px 0px";r.transformOrigin=n,r.mozTransformOrigin=n,r.webkitTransformOrigin=n,r["-webkit-transform"]=n,r.contentVisibility=this.renderConfig.contentVisibility,this.animationItem.wrapper.appendChild(this.animationItem.container),this.canvasContext=this.animationItem.container.getContext("2d"),this.renderConfig.className&&this.animationItem.container.setAttribute("class",this.renderConfig.className),this.renderConfig.id&&this.animationItem.container.setAttribute("id",this.renderConfig.id)}else this.canvasContext=this.renderConfig.context;this.data=e,this.layers=e.layers,this.transformCanvas={w:e.w,h:e.h,sx:0,sy:0,tx:0,ty:0},this.setupGlobalData(e,document.body),this.globalData.canvasContext=this.canvasContext,this.globalData.renderer=this,this.globalData.isDashed=!1,this.globalData.progressiveLoad=this.renderConfig.progressiveLoad,this.globalData.transformCanvas=this.transformCanvas,this.elements=createSizedArray(e.layers.length),this.updateContainerSize()},CanvasRendererBase.prototype.updateContainerSize=function(){this.reset();var e,r;this.animationItem.wrapper&&this.animationItem.container?(e=this.animationItem.wrapper.offsetWidth,r=this.animationItem.wrapper.offsetHeight,this.animationItem.container.setAttribute("width",e*this.renderConfig.dpr),this.animationItem.container.setAttribute("height",r*this.renderConfig.dpr)):(e=this.canvasContext.canvas.width*this.renderConfig.dpr,r=this.canvasContext.canvas.height*this.renderConfig.dpr);var n,a;if(this.renderConfig.preserveAspectRatio.indexOf("meet")!==-1||this.renderConfig.preserveAspectRatio.indexOf("slice")!==-1){var l=this.renderConfig.preserveAspectRatio.split(" "),c=l[1]||"meet",u=l[0]||"xMidYMid",d=u.substr(0,4),m=u.substr(4);n=e/r,a=this.transformCanvas.w/this.transformCanvas.h,a>n&&c==="meet"||a<n&&c==="slice"?(this.transformCanvas.sx=e/(this.transformCanvas.w/this.renderConfig.dpr),this.transformCanvas.sy=e/(this.transformCanvas.w/this.renderConfig.dpr)):(this.transformCanvas.sx=r/(this.transformCanvas.h/this.renderConfig.dpr),this.transformCanvas.sy=r/(this.transformCanvas.h/this.renderConfig.dpr)),d==="xMid"&&(a<n&&c==="meet"||a>n&&c==="slice")?this.transformCanvas.tx=(e-this.transformCanvas.w*(r/this.transformCanvas.h))/2*this.renderConfig.dpr:d==="xMax"&&(a<n&&c==="meet"||a>n&&c==="slice")?this.transformCanvas.tx=(e-this.transformCanvas.w*(r/this.transformCanvas.h))*this.renderConfig.dpr:this.transformCanvas.tx=0,m==="YMid"&&(a>n&&c==="meet"||a<n&&c==="slice")?this.transformCanvas.ty=(r-this.transformCanvas.h*(e/this.transformCanvas.w))/2*this.renderConfig.dpr:m==="YMax"&&(a>n&&c==="meet"||a<n&&c==="slice")?this.transformCanvas.ty=(r-this.transformCanvas.h*(e/this.transformCanvas.w))*this.renderConfig.dpr:this.transformCanvas.ty=0}else this.renderConfig.preserveAspectRatio==="none"?(this.transformCanvas.sx=e/(this.transformCanvas.w/this.renderConfig.dpr),this.transformCanvas.sy=r/(this.transformCanvas.h/this.renderConfig.dpr),this.transformCanvas.tx=0,this.transformCanvas.ty=0):(this.transformCanvas.sx=this.renderConfig.dpr,this.transformCanvas.sy=this.renderConfig.dpr,this.transformCanvas.tx=0,this.transformCanvas.ty=0);this.transformCanvas.props=[this.transformCanvas.sx,0,0,0,0,this.transformCanvas.sy,0,0,0,0,1,0,this.transformCanvas.tx,this.transformCanvas.ty,0,1],this.ctxTransform(this.transformCanvas.props),this.canvasContext.beginPath(),this.canvasContext.rect(0,0,this.transformCanvas.w,this.transformCanvas.h),this.canvasContext.closePath(),this.canvasContext.clip(),this.renderFrame(this.renderedFrame,!0)},CanvasRendererBase.prototype.destroy=function(){this.renderConfig.clearCanvas&&this.animationItem.wrapper&&(this.animationItem.wrapper.innerText="");var e,r=this.layers?this.layers.length:0;for(e=r-1;e>=0;e-=1)this.elements[e]&&this.elements[e].destroy();this.elements.length=0,this.globalData.canvasContext=null,this.animationItem.container=null,this.destroyed=!0},CanvasRendererBase.prototype.renderFrame=function(e,r){if(!(this.renderedFrame===e&&this.renderConfig.clearCanvas===!0&&!r||this.destroyed||e===-1)){this.renderedFrame=e,this.globalData.frameNum=e-this.animationItem._isFirstFrame,this.globalData.frameId+=1,this.globalData._mdf=!this.renderConfig.clearCanvas||r,this.globalData.projectInterface.currentFrame=e;var n,a=this.layers.length;for(this.completeLayers||this.checkLayers(e),n=0;n<a;n+=1)(this.completeLayers||this.elements[n])&&this.elements[n].prepareFrame(e-this.layers[n].st);if(this.globalData._mdf){for(this.renderConfig.clearCanvas===!0?this.canvasContext.clearRect(0,0,this.transformCanvas.w,this.transformCanvas.h):this.save(),n=a-1;n>=0;n-=1)(this.completeLayers||this.elements[n])&&this.elements[n].renderFrame();this.renderConfig.clearCanvas!==!0&&this.restore()}}},CanvasRendererBase.prototype.buildItem=function(e){var r=this.elements;if(!(r[e]||this.layers[e].ty===99)){var n=this.createItem(this.layers[e],this,this.globalData);r[e]=n,n.initExpressions()}},CanvasRendererBase.prototype.checkPendingElements=function(){for(;this.pendingElements.length;){var e=this.pendingElements.pop();e.checkParenting()}},CanvasRendererBase.prototype.hide=function(){this.animationItem.container.style.display="none"},CanvasRendererBase.prototype.show=function(){this.animationItem.container.style.display="block"};function CVCompElement(e,r,n){this.completeLayers=!1,this.layers=e.layers,this.pendingElements=[],this.elements=createSizedArray(this.layers.length),this.initElement(e,r,n),this.tm=e.tm?PropertyFactory.getProp(this,e.tm,0,r.frameRate,this):{_placeholder:!0}}extendPrototype([CanvasRendererBase,ICompElement,CVBaseElement],CVCompElement),CVCompElement.prototype.renderInnerContent=function(){var e=this.canvasContext;e.beginPath(),e.moveTo(0,0),e.lineTo(this.data.w,0),e.lineTo(this.data.w,this.data.h),e.lineTo(0,this.data.h),e.lineTo(0,0),e.clip();var r,n=this.layers.length;for(r=n-1;r>=0;r-=1)(this.completeLayers||this.elements[r])&&this.elements[r].renderFrame()},CVCompElement.prototype.destroy=function(){var e,r=this.layers.length;for(e=r-1;e>=0;e-=1)this.elements[e]&&this.elements[e].destroy();this.layers=null,this.elements=null},CVCompElement.prototype.createComp=function(e){return new CVCompElement(e,this.globalData,this)};function CanvasRenderer(e,r){this.animationItem=e,this.renderConfig={clearCanvas:r&&r.clearCanvas!==void 0?r.clearCanvas:!0,context:r&&r.context||null,progressiveLoad:r&&r.progressiveLoad||!1,preserveAspectRatio:r&&r.preserveAspectRatio||"xMidYMid meet",imagePreserveAspectRatio:r&&r.imagePreserveAspectRatio||"xMidYMid slice",contentVisibility:r&&r.contentVisibility||"visible",className:r&&r.className||"",id:r&&r.id||""},this.renderConfig.dpr=r&&r.dpr||1,this.animationItem.wrapper&&(this.renderConfig.dpr=r&&r.dpr||window.devicePixelRatio||1),this.renderedFrame=-1,this.globalData={frameNum:-1,_mdf:!1,renderConfig:this.renderConfig,currentGlobalAlpha:-1},this.contextData=new CVContextData,this.elements=[],this.pendingElements=[],this.transformMat=new Matrix,this.completeLayers=!1,this.rendererType="canvas"}extendPrototype([CanvasRendererBase],CanvasRenderer),CanvasRenderer.prototype.createComp=function(e){return new CVCompElement(e,this.globalData,this)};function HBaseElement(){}HBaseElement.prototype={checkBlendMode:function(){},initRendererElement:function(){this.baseElement=createTag(this.data.tg||"div"),this.data.hasMask?(this.svgElement=createNS("svg"),this.layerElement=createNS("g"),this.maskedElement=this.layerElement,this.svgElement.appendChild(this.layerElement),this.baseElement.appendChild(this.svgElement)):this.layerElement=this.baseElement,styleDiv(this.baseElement)},createContainerElements:function(){this.renderableEffectsManager=new CVEffects(this),this.transformedElement=this.baseElement,this.maskedElement=this.layerElement,this.data.ln&&this.layerElement.setAttribute("id",this.data.ln),this.data.cl&&this.layerElement.setAttribute("class",this.data.cl),this.data.bm!==0&&this.setBlendMode()},renderElement:function(){var r=this.transformedElement?this.transformedElement.style:{};if(this.finalTransform._matMdf){var n=this.finalTransform.mat.toCSS();r.transform=n,r.webkitTransform=n}this.finalTransform._opMdf&&(r.opacity=this.finalTransform.mProp.o.v)},renderFrame:function(){this.data.hd||this.hidden||(this.renderTransform(),this.renderRenderable(),this.renderElement(),this.renderInnerContent(),this._isFirstFrame&&(this._isFirstFrame=!1))},destroy:function(){this.layerElement=null,this.transformedElement=null,this.matteElement&&(this.matteElement=null),this.maskManager&&(this.maskManager.destroy(),this.maskManager=null)},createRenderableComponents:function(){this.maskManager=new MaskElement(this.data,this,this.globalData)},addEffects:function(){},setMatte:function(){}},HBaseElement.prototype.getBaseElement=SVGBaseElement.prototype.getBaseElement,HBaseElement.prototype.destroyBaseElement=HBaseElement.prototype.destroy,HBaseElement.prototype.buildElementParenting=BaseRenderer.prototype.buildElementParenting;function HSolidElement(e,r,n){this.initElement(e,r,n)}extendPrototype([BaseElement,TransformElement,HBaseElement,HierarchyElement,FrameElement,RenderableDOMElement],HSolidElement),HSolidElement.prototype.createContent=function(){var e;this.data.hasMask?(e=createNS("rect"),e.setAttribute("width",this.data.sw),e.setAttribute("height",this.data.sh),e.setAttribute("fill",this.data.sc),this.svgElement.setAttribute("width",this.data.sw),this.svgElement.setAttribute("height",this.data.sh)):(e=createTag("div"),e.style.width=this.data.sw+"px",e.style.height=this.data.sh+"px",e.style.backgroundColor=this.data.sc),this.layerElement.appendChild(e)};function HShapeElement(e,r,n){this.shapes=[],this.shapesData=e.shapes,this.stylesList=[],this.shapeModifiers=[],this.itemsData=[],this.processedElements=[],this.animatedContents=[],this.shapesContainer=createNS("g"),this.initElement(e,r,n),this.prevViewData=[],this.currentBBox={x:999999,y:-999999,h:0,w:0}}extendPrototype([BaseElement,TransformElement,HSolidElement,SVGShapeElement,HBaseElement,HierarchyElement,FrameElement,RenderableElement],HShapeElement),HShapeElement.prototype._renderShapeFrame=HShapeElement.prototype.renderInnerContent,HShapeElement.prototype.createContent=function(){var e;if(this.baseElement.style.fontSize=0,this.data.hasMask)this.layerElement.appendChild(this.shapesContainer),e=this.svgElement;else{e=createNS("svg");var r=this.comp.data?this.comp.data:this.globalData.compSize;e.setAttribute("width",r.w),e.setAttribute("height",r.h),e.appendChild(this.shapesContainer),this.layerElement.appendChild(e)}this.searchShapes(this.shapesData,this.itemsData,this.prevViewData,this.shapesContainer,0,[],!0),this.filterUniqueShapes(),this.shapeCont=e},HShapeElement.prototype.getTransformedPoint=function(e,r){var n,a=e.length;for(n=0;n<a;n+=1)r=e[n].mProps.v.applyToPointArray(r[0],r[1],0);return r},HShapeElement.prototype.calculateShapeBoundingBox=function(e,r){var n=e.sh.v,a=e.transformers,l,c=n._length,u,d,m,E;if(!(c<=1)){for(l=0;l<c-1;l+=1)u=this.getTransformedPoint(a,n.v[l]),d=this.getTransformedPoint(a,n.o[l]),m=this.getTransformedPoint(a,n.i[l+1]),E=this.getTransformedPoint(a,n.v[l+1]),this.checkBounds(u,d,m,E,r);n.c&&(u=this.getTransformedPoint(a,n.v[l]),d=this.getTransformedPoint(a,n.o[l]),m=this.getTransformedPoint(a,n.i[0]),E=this.getTransformedPoint(a,n.v[0]),this.checkBounds(u,d,m,E,r))}},HShapeElement.prototype.checkBounds=function(e,r,n,a,l){this.getBoundsOfCurve(e,r,n,a);var c=this.shapeBoundingBox;l.x=bmMin(c.left,l.x),l.xMax=bmMax(c.right,l.xMax),l.y=bmMin(c.top,l.y),l.yMax=bmMax(c.bottom,l.yMax)},HShapeElement.prototype.shapeBoundingBox={left:0,right:0,top:0,bottom:0},HShapeElement.prototype.tempBoundingBox={x:0,xMax:0,y:0,yMax:0,width:0,height:0},HShapeElement.prototype.getBoundsOfCurve=function(e,r,n,a){for(var l=[[e[0],a[0]],[e[1],a[1]]],c,u,d,m,E,C,_,A=0;A<2;++A)u=6*e[A]-12*r[A]+6*n[A],c=-3*e[A]+9*r[A]-9*n[A]+3*a[A],d=3*r[A]-3*e[A],u|=0,c|=0,d|=0,c===0&&u===0||(c===0?(m=-d/u,m>0&&m<1&&l[A].push(this.calculateF(m,e,r,n,a,A))):(E=u*u-4*d*c,E>=0&&(C=(-u+bmSqrt(E))/(2*c),C>0&&C<1&&l[A].push(this.calculateF(C,e,r,n,a,A)),_=(-u-bmSqrt(E))/(2*c),_>0&&_<1&&l[A].push(this.calculateF(_,e,r,n,a,A)))));this.shapeBoundingBox.left=bmMin.apply(null,l[0]),this.shapeBoundingBox.top=bmMin.apply(null,l[1]),this.shapeBoundingBox.right=bmMax.apply(null,l[0]),this.shapeBoundingBox.bottom=bmMax.apply(null,l[1])},HShapeElement.prototype.calculateF=function(e,r,n,a,l,c){return bmPow(1-e,3)*r[c]+3*bmPow(1-e,2)*e*n[c]+3*(1-e)*bmPow(e,2)*a[c]+bmPow(e,3)*l[c]},HShapeElement.prototype.calculateBoundingBox=function(e,r){var n,a=e.length;for(n=0;n<a;n+=1)e[n]&&e[n].sh?this.calculateShapeBoundingBox(e[n],r):e[n]&&e[n].it&&this.calculateBoundingBox(e[n].it,r)},HShapeElement.prototype.currentBoxContains=function(e){return this.currentBBox.x<=e.x&&this.currentBBox.y<=e.y&&this.currentBBox.width+this.currentBBox.x>=e.x+e.width&&this.currentBBox.height+this.currentBBox.y>=e.y+e.height},HShapeElement.prototype.renderInnerContent=function(){if(this._renderShapeFrame(),!this.hidden&&(this._isFirstFrame||this._mdf)){var e=this.tempBoundingBox,r=999999;if(e.x=r,e.xMax=-r,e.y=r,e.yMax=-r,this.calculateBoundingBox(this.itemsData,e),e.width=e.xMax<e.x?0:e.xMax-e.x,e.height=e.yMax<e.y?0:e.yMax-e.y,this.currentBoxContains(e))return;var n=!1;if(this.currentBBox.w!==e.width&&(this.currentBBox.w=e.width,this.shapeCont.setAttribute("width",e.width),n=!0),this.currentBBox.h!==e.height&&(this.currentBBox.h=e.height,this.shapeCont.setAttribute("height",e.height),n=!0),n||this.currentBBox.x!==e.x||this.currentBBox.y!==e.y){this.currentBBox.w=e.width,this.currentBBox.h=e.height,this.currentBBox.x=e.x,this.currentBBox.y=e.y,this.shapeCont.setAttribute("viewBox",this.currentBBox.x+" "+this.currentBBox.y+" "+this.currentBBox.w+" "+this.currentBBox.h);var a=this.shapeCont.style,l="translate("+this.currentBBox.x+"px,"+this.currentBBox.y+"px)";a.transform=l,a.webkitTransform=l}}};function HTextElement(e,r,n){this.textSpans=[],this.textPaths=[],this.currentBBox={x:999999,y:-999999,h:0,w:0},this.renderType="svg",this.isMasked=!1,this.initElement(e,r,n)}extendPrototype([BaseElement,TransformElement,HBaseElement,HierarchyElement,FrameElement,RenderableDOMElement,ITextElement],HTextElement),HTextElement.prototype.createContent=function(){if(this.isMasked=this.checkMasks(),this.isMasked){this.renderType="svg",this.compW=this.comp.data.w,this.compH=this.comp.data.h,this.svgElement.setAttribute("width",this.compW),this.svgElement.setAttribute("height",this.compH);var e=createNS("g");this.maskedElement.appendChild(e),this.innerElem=e}else this.renderType="html",this.innerElem=this.layerElement;this.checkParenting()},HTextElement.prototype.buildNewText=function(){var e=this.textProperty.currentData;this.renderedLetters=createSizedArray(e.l?e.l.length:0);var r=this.innerElem.style,n=e.fc?this.buildColor(e.fc):"rgba(0,0,0,0)";r.fill=n,r.color=n,e.sc&&(r.stroke=this.buildColor(e.sc),r.strokeWidth=e.sw+"px");var a=this.globalData.fontManager.getFontByName(e.f);if(!this.globalData.fontManager.chars)if(r.fontSize=e.finalSize+"px",r.lineHeight=e.finalSize+"px",a.fClass)this.innerElem.className=a.fClass;else{r.fontFamily=a.fFamily;var l=e.fWeight,c=e.fStyle;r.fontStyle=c,r.fontWeight=l}var u,d,m=e.l;d=m.length;var E,C,_,A=this.mHelper,T,S="",P=0;for(u=0;u<d;u+=1){if(this.globalData.fontManager.chars?(this.textPaths[P]?E=this.textPaths[P]:(E=createNS("path"),E.setAttribute("stroke-linecap",lineCapEnum[1]),E.setAttribute("stroke-linejoin",lineJoinEnum[2]),E.setAttribute("stroke-miterlimit","4")),this.isMasked||(this.textSpans[P]?(C=this.textSpans[P],_=C.children[0]):(C=createTag("div"),C.style.lineHeight=0,_=createNS("svg"),_.appendChild(E),styleDiv(C)))):this.isMasked?E=this.textPaths[P]?this.textPaths[P]:createNS("text"):this.textSpans[P]?(C=this.textSpans[P],E=this.textPaths[P]):(C=createTag("span"),styleDiv(C),E=createTag("span"),styleDiv(E),C.appendChild(E)),this.globalData.fontManager.chars){var k=this.globalData.fontManager.getCharData(e.finalText[u],a.fStyle,this.globalData.fontManager.getFontByName(e.f).fFamily),x;if(k?x=k.data:x=null,A.reset(),x&&x.shapes&&x.shapes.length&&(T=x.shapes[0].it,A.scale(e.finalSize/100,e.finalSize/100),S=this.createPathShape(A,T),E.setAttribute("d",S)),this.isMasked)this.innerElem.appendChild(E);else{if(this.innerElem.appendChild(C),x&&x.shapes){document.body.appendChild(_);var b=_.getBBox();_.setAttribute("width",b.width+2),_.setAttribute("height",b.height+2),_.setAttribute("viewBox",b.x-1+" "+(b.y-1)+" "+(b.width+2)+" "+(b.height+2));var g=_.style,y="translate("+(b.x-1)+"px,"+(b.y-1)+"px)";g.transform=y,g.webkitTransform=y,m[u].yOffset=b.y-1}else _.setAttribute("width",1),_.setAttribute("height",1);C.appendChild(_)}}else if(E.textContent=m[u].val,E.setAttributeNS("http://www.w3.org/XML/1998/namespace","xml:space","preserve"),this.isMasked)this.innerElem.appendChild(E);else{this.innerElem.appendChild(C);var I=E.style,M="translate3d(0,"+-e.finalSize/1.2+"px,0)";I.transform=M,I.webkitTransform=M}this.isMasked?this.textSpans[P]=E:this.textSpans[P]=C,this.textSpans[P].style.display="block",this.textPaths[P]=E,P+=1}for(;P<this.textSpans.length;)this.textSpans[P].style.display="none",P+=1},HTextElement.prototype.renderInnerContent=function(){var e;if(this.data.singleShape){if(!this._isFirstFrame&&!this.lettersChangedFlag)return;if(this.isMasked&&this.finalTransform._matMdf){this.svgElement.setAttribute("viewBox",-this.finalTransform.mProp.p.v[0]+" "+-this.finalTransform.mProp.p.v[1]+" "+this.compW+" "+this.compH),e=this.svgElement.style;var r="translate("+-this.finalTransform.mProp.p.v[0]+"px,"+-this.finalTransform.mProp.p.v[1]+"px)";e.transform=r,e.webkitTransform=r}}if(this.textAnimator.getMeasures(this.textProperty.currentData,this.lettersChangedFlag),!(!this.lettersChangedFlag&&!this.textAnimator.lettersChangedFlag)){var n,a,l=0,c=this.textAnimator.renderedLetters,u=this.textProperty.currentData.l;a=u.length;var d,m,E;for(n=0;n<a;n+=1)u[n].n?l+=1:(m=this.textSpans[n],E=this.textPaths[n],d=c[l],l+=1,d._mdf.m&&(this.isMasked?m.setAttribute("transform",d.m):(m.style.webkitTransform=d.m,m.style.transform=d.m)),m.style.opacity=d.o,d.sw&&d._mdf.sw&&E.setAttribute("stroke-width",d.sw),d.sc&&d._mdf.sc&&E.setAttribute("stroke",d.sc),d.fc&&d._mdf.fc&&(E.setAttribute("fill",d.fc),E.style.color=d.fc));if(this.innerElem.getBBox&&!this.hidden&&(this._isFirstFrame||this._mdf)){var C=this.innerElem.getBBox();this.currentBBox.w!==C.width&&(this.currentBBox.w=C.width,this.svgElement.setAttribute("width",C.width)),this.currentBBox.h!==C.height&&(this.currentBBox.h=C.height,this.svgElement.setAttribute("height",C.height));var _=1;if(this.currentBBox.w!==C.width+_*2||this.currentBBox.h!==C.height+_*2||this.currentBBox.x!==C.x-_||this.currentBBox.y!==C.y-_){this.currentBBox.w=C.width+_*2,this.currentBBox.h=C.height+_*2,this.currentBBox.x=C.x-_,this.currentBBox.y=C.y-_,this.svgElement.setAttribute("viewBox",this.currentBBox.x+" "+this.currentBBox.y+" "+this.currentBBox.w+" "+this.currentBBox.h),e=this.svgElement.style;var A="translate("+this.currentBBox.x+"px,"+this.currentBBox.y+"px)";e.transform=A,e.webkitTransform=A}}}};function HCameraElement(e,r,n){this.initFrame(),this.initBaseData(e,r,n),this.initHierarchy();var a=PropertyFactory.getProp;if(this.pe=a(this,e.pe,0,0,this),e.ks.p.s?(this.px=a(this,e.ks.p.x,1,0,this),this.py=a(this,e.ks.p.y,1,0,this),this.pz=a(this,e.ks.p.z,1,0,this)):this.p=a(this,e.ks.p,1,0,this),e.ks.a&&(this.a=a(this,e.ks.a,1,0,this)),e.ks.or.k.length&&e.ks.or.k[0].to){var l,c=e.ks.or.k.length;for(l=0;l<c;l+=1)e.ks.or.k[l].to=null,e.ks.or.k[l].ti=null}this.or=a(this,e.ks.or,1,degToRads,this),this.or.sh=!0,this.rx=a(this,e.ks.rx,0,degToRads,this),this.ry=a(this,e.ks.ry,0,degToRads,this),this.rz=a(this,e.ks.rz,0,degToRads,this),this.mat=new Matrix,this._prevMat=new Matrix,this._isFirstFrame=!0,this.finalTransform={mProp:this}}extendPrototype([BaseElement,FrameElement,HierarchyElement],HCameraElement),HCameraElement.prototype.setup=function(){var e,r=this.comp.threeDElements.length,n,a,l;for(e=0;e<r;e+=1)if(n=this.comp.threeDElements[e],n.type==="3d"){a=n.perspectiveElem.style,l=n.container.style;var c=this.pe.v+"px",u="0px 0px 0px",d="matrix3d(1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1)";a.perspective=c,a.webkitPerspective=c,l.transformOrigin=u,l.mozTransformOrigin=u,l.webkitTransformOrigin=u,a.transform=d,a.webkitTransform=d}},HCameraElement.prototype.createElements=function(){},HCameraElement.prototype.hide=function(){},HCameraElement.prototype.renderFrame=function(){var e=this._isFirstFrame,r,n;if(this.hierarchy)for(n=this.hierarchy.length,r=0;r<n;r+=1)e=this.hierarchy[r].finalTransform.mProp._mdf||e;if(e||this.pe._mdf||this.p&&this.p._mdf||this.px&&(this.px._mdf||this.py._mdf||this.pz._mdf)||this.rx._mdf||this.ry._mdf||this.rz._mdf||this.or._mdf||this.a&&this.a._mdf){if(this.mat.reset(),this.hierarchy)for(n=this.hierarchy.length-1,r=n;r>=0;r-=1){var a=this.hierarchy[r].finalTransform.mProp;this.mat.translate(-a.p.v[0],-a.p.v[1],a.p.v[2]),this.mat.rotateX(-a.or.v[0]).rotateY(-a.or.v[1]).rotateZ(a.or.v[2]),this.mat.rotateX(-a.rx.v).rotateY(-a.ry.v).rotateZ(a.rz.v),this.mat.scale(1/a.s.v[0],1/a.s.v[1],1/a.s.v[2]),this.mat.translate(a.a.v[0],a.a.v[1],a.a.v[2])}if(this.p?this.mat.translate(-this.p.v[0],-this.p.v[1],this.p.v[2]):this.mat.translate(-this.px.v,-this.py.v,this.pz.v),this.a){var l;this.p?l=[this.p.v[0]-this.a.v[0],this.p.v[1]-this.a.v[1],this.p.v[2]-this.a.v[2]]:l=[this.px.v-this.a.v[0],this.py.v-this.a.v[1],this.pz.v-this.a.v[2]];var c=Math.sqrt(Math.pow(l[0],2)+Math.pow(l[1],2)+Math.pow(l[2],2)),u=[l[0]/c,l[1]/c,l[2]/c],d=Math.sqrt(u[2]*u[2]+u[0]*u[0]),m=Math.atan2(u[1],d),E=Math.atan2(u[0],-u[2]);this.mat.rotateY(E).rotateX(-m)}this.mat.rotateX(-this.rx.v).rotateY(-this.ry.v).rotateZ(this.rz.v),this.mat.rotateX(-this.or.v[0]).rotateY(-this.or.v[1]).rotateZ(this.or.v[2]),this.mat.translate(this.globalData.compSize.w/2,this.globalData.compSize.h/2,0),this.mat.translate(0,0,this.pe.v);var C=!this._prevMat.equals(this.mat);if((C||this.pe._mdf)&&this.comp.threeDElements){n=this.comp.threeDElements.length;var _,A,T;for(r=0;r<n;r+=1)if(_=this.comp.threeDElements[r],_.type==="3d"){if(C){var S=this.mat.toCSS();T=_.container.style,T.transform=S,T.webkitTransform=S}this.pe._mdf&&(A=_.perspectiveElem.style,A.perspective=this.pe.v+"px",A.webkitPerspective=this.pe.v+"px")}this.mat.clone(this._prevMat)}}this._isFirstFrame=!1},HCameraElement.prototype.prepareFrame=function(e){this.prepareProperties(e,!0)},HCameraElement.prototype.destroy=function(){},HCameraElement.prototype.getBaseElement=function(){return null};function HImageElement(e,r,n){this.assetData=r.getAssetData(e.refId),this.initElement(e,r,n)}extendPrototype([BaseElement,TransformElement,HBaseElement,HSolidElement,HierarchyElement,FrameElement,RenderableElement],HImageElement),HImageElement.prototype.createContent=function(){var e=this.globalData.getAssetsPath(this.assetData),r=new Image;this.data.hasMask?(this.imageElem=createNS("image"),this.imageElem.setAttribute("width",this.assetData.w+"px"),this.imageElem.setAttribute("height",this.assetData.h+"px"),this.imageElem.setAttributeNS("http://www.w3.org/1999/xlink","href",e),this.layerElement.appendChild(this.imageElem),this.baseElement.setAttribute("width",this.assetData.w),this.baseElement.setAttribute("height",this.assetData.h)):this.layerElement.appendChild(r),r.crossOrigin="anonymous",r.src=e,this.data.ln&&this.baseElement.setAttribute("id",this.data.ln)};function HybridRendererBase(e,r){this.animationItem=e,this.layers=null,this.renderedFrame=-1,this.renderConfig={className:r&&r.className||"",imagePreserveAspectRatio:r&&r.imagePreserveAspectRatio||"xMidYMid slice",hideOnTransparent:!(r&&r.hideOnTransparent===!1),filterSize:{width:r&&r.filterSize&&r.filterSize.width||"400%",height:r&&r.filterSize&&r.filterSize.height||"400%",x:r&&r.filterSize&&r.filterSize.x||"-100%",y:r&&r.filterSize&&r.filterSize.y||"-100%"}},this.globalData={_mdf:!1,frameNum:-1,renderConfig:this.renderConfig},this.pendingElements=[],this.elements=[],this.threeDElements=[],this.destroyed=!1,this.camera=null,this.supports3d=!0,this.rendererType="html"}extendPrototype([BaseRenderer],HybridRendererBase),HybridRendererBase.prototype.buildItem=SVGRenderer.prototype.buildItem,HybridRendererBase.prototype.checkPendingElements=function(){for(;this.pendingElements.length;){var e=this.pendingElements.pop();e.checkParenting()}},HybridRendererBase.prototype.appendElementInPos=function(e,r){var n=e.getBaseElement();if(!!n){var a=this.layers[r];if(!a.ddd||!this.supports3d)if(this.threeDElements)this.addTo3dContainer(n,r);else{for(var l=0,c,u,d;l<r;)this.elements[l]&&this.elements[l]!==!0&&this.elements[l].getBaseElement&&(u=this.elements[l],d=this.layers[l].ddd?this.getThreeDContainerByPos(l):u.getBaseElement(),c=d||c),l+=1;c?(!a.ddd||!this.supports3d)&&this.layerElement.insertBefore(n,c):(!a.ddd||!this.supports3d)&&this.layerElement.appendChild(n)}else this.addTo3dContainer(n,r)}},HybridRendererBase.prototype.createShape=function(e){return this.supports3d?new HShapeElement(e,this.globalData,this):new SVGShapeElement(e,this.globalData,this)},HybridRendererBase.prototype.createText=function(e){return this.supports3d?new HTextElement(e,this.globalData,this):new SVGTextLottieElement(e,this.globalData,this)},HybridRendererBase.prototype.createCamera=function(e){return this.camera=new HCameraElement(e,this.globalData,this),this.camera},HybridRendererBase.prototype.createImage=function(e){return this.supports3d?new HImageElement(e,this.globalData,this):new IImageElement(e,this.globalData,this)},HybridRendererBase.prototype.createSolid=function(e){return this.supports3d?new HSolidElement(e,this.globalData,this):new ISolidElement(e,this.globalData,this)},HybridRendererBase.prototype.createNull=SVGRenderer.prototype.createNull,HybridRendererBase.prototype.getThreeDContainerByPos=function(e){for(var r=0,n=this.threeDElements.length;r<n;){if(this.threeDElements[r].startPos<=e&&this.threeDElements[r].endPos>=e)return this.threeDElements[r].perspectiveElem;r+=1}return null},HybridRendererBase.prototype.createThreeDContainer=function(e,r){var n=createTag("div"),a,l;styleDiv(n);var c=createTag("div");if(styleDiv(c),r==="3d"){a=n.style,a.width=this.globalData.compSize.w+"px",a.height=this.globalData.compSize.h+"px";var u="50% 50%";a.webkitTransformOrigin=u,a.mozTransformOrigin=u,a.transformOrigin=u,l=c.style;var d="matrix3d(1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1)";l.transform=d,l.webkitTransform=d}n.appendChild(c);var m={container:c,perspectiveElem:n,startPos:e,endPos:e,type:r};return this.threeDElements.push(m),m},HybridRendererBase.prototype.build3dContainers=function(){var e,r=this.layers.length,n,a="";for(e=0;e<r;e+=1)this.layers[e].ddd&&this.layers[e].ty!==3?(a!=="3d"&&(a="3d",n=this.createThreeDContainer(e,"3d")),n.endPos=Math.max(n.endPos,e)):(a!=="2d"&&(a="2d",n=this.createThreeDContainer(e,"2d")),n.endPos=Math.max(n.endPos,e));for(r=this.threeDElements.length,e=r-1;e>=0;e-=1)this.resizerElem.appendChild(this.threeDElements[e].perspectiveElem)},HybridRendererBase.prototype.addTo3dContainer=function(e,r){for(var n=0,a=this.threeDElements.length;n<a;){if(r<=this.threeDElements[n].endPos){for(var l=this.threeDElements[n].startPos,c;l<r;)this.elements[l]&&this.elements[l].getBaseElement&&(c=this.elements[l].getBaseElement()),l+=1;c?this.threeDElements[n].container.insertBefore(e,c):this.threeDElements[n].container.appendChild(e);break}n+=1}},HybridRendererBase.prototype.configAnimation=function(e){var r=createTag("div"),n=this.animationItem.wrapper,a=r.style;a.width=e.w+"px",a.height=e.h+"px",this.resizerElem=r,styleDiv(r),a.transformStyle="flat",a.mozTransformStyle="flat",a.webkitTransformStyle="flat",this.renderConfig.className&&r.setAttribute("class",this.renderConfig.className),n.appendChild(r),a.overflow="hidden";var l=createNS("svg");l.setAttribute("width","1"),l.setAttribute("height","1"),styleDiv(l),this.resizerElem.appendChild(l);var c=createNS("defs");l.appendChild(c),this.data=e,this.setupGlobalData(e,l),this.globalData.defs=c,this.layers=e.layers,this.layerElement=this.resizerElem,this.build3dContainers(),this.updateContainerSize()},HybridRendererBase.prototype.destroy=function(){this.animationItem.wrapper&&(this.animationItem.wrapper.innerText=""),this.animationItem.container=null,this.globalData.defs=null;var e,r=this.layers?this.layers.length:0;for(e=0;e<r;e+=1)this.elements[e].destroy();this.elements.length=0,this.destroyed=!0,this.animationItem=null},HybridRendererBase.prototype.updateContainerSize=function(){var e=this.animationItem.wrapper.offsetWidth,r=this.animationItem.wrapper.offsetHeight,n=e/r,a=this.globalData.compSize.w/this.globalData.compSize.h,l,c,u,d;a>n?(l=e/this.globalData.compSize.w,c=e/this.globalData.compSize.w,u=0,d=(r-this.globalData.compSize.h*(e/this.globalData.compSize.w))/2):(l=r/this.globalData.compSize.h,c=r/this.globalData.compSize.h,u=(e-this.globalData.compSize.w*(r/this.globalData.compSize.h))/2,d=0);var m=this.resizerElem.style;m.webkitTransform="matrix3d("+l+",0,0,0,0,"+c+",0,0,0,0,1,0,"+u+","+d+",0,1)",m.transform=m.webkitTransform},HybridRendererBase.prototype.renderFrame=SVGRenderer.prototype.renderFrame,HybridRendererBase.prototype.hide=function(){this.resizerElem.style.display="none"},HybridRendererBase.prototype.show=function(){this.resizerElem.style.display="block"},HybridRendererBase.prototype.initItems=function(){if(this.buildAllItems(),this.camera)this.camera.setup();else{var e=this.globalData.compSize.w,r=this.globalData.compSize.h,n,a=this.threeDElements.length;for(n=0;n<a;n+=1){var l=this.threeDElements[n].perspectiveElem.style;l.webkitPerspective=Math.sqrt(Math.pow(e,2)+Math.pow(r,2))+"px",l.perspective=l.webkitPerspective}}},HybridRendererBase.prototype.searchExtraCompositions=function(e){var r,n=e.length,a=createTag("div");for(r=0;r<n;r+=1)if(e[r].xt){var l=this.createComp(e[r],a,this.globalData.comp,null);l.initExpressions(),this.globalData.projectInterface.registerComposition(l)}};function HCompElement(e,r,n){this.layers=e.layers,this.supports3d=!e.hasMask,this.completeLayers=!1,this.pendingElements=[],this.elements=this.layers?createSizedArray(this.layers.length):[],this.initElement(e,r,n),this.tm=e.tm?PropertyFactory.getProp(this,e.tm,0,r.frameRate,this):{_placeholder:!0}}extendPrototype([HybridRendererBase,ICompElement,HBaseElement],HCompElement),HCompElement.prototype._createBaseContainerElements=HCompElement.prototype.createContainerElements,HCompElement.prototype.createContainerElements=function(){this._createBaseContainerElements(),this.data.hasMask?(this.svgElement.setAttribute("width",this.data.w),this.svgElement.setAttribute("height",this.data.h),this.transformedElement=this.baseElement):this.transformedElement=this.layerElement},HCompElement.prototype.addTo3dContainer=function(e,r){for(var n=0,a;n<r;)this.elements[n]&&this.elements[n].getBaseElement&&(a=this.elements[n].getBaseElement()),n+=1;a?this.layerElement.insertBefore(e,a):this.layerElement.appendChild(e)},HCompElement.prototype.createComp=function(e){return this.supports3d?new HCompElement(e,this.globalData,this):new SVGCompElement(e,this.globalData,this)};function HybridRenderer(e,r){this.animationItem=e,this.layers=null,this.renderedFrame=-1,this.renderConfig={className:r&&r.className||"",imagePreserveAspectRatio:r&&r.imagePreserveAspectRatio||"xMidYMid slice",hideOnTransparent:!(r&&r.hideOnTransparent===!1),filterSize:{width:r&&r.filterSize&&r.filterSize.width||"400%",height:r&&r.filterSize&&r.filterSize.height||"400%",x:r&&r.filterSize&&r.filterSize.x||"-100%",y:r&&r.filterSize&&r.filterSize.y||"-100%"}},this.globalData={_mdf:!1,frameNum:-1,renderConfig:this.renderConfig},this.pendingElements=[],this.elements=[],this.threeDElements=[],this.destroyed=!1,this.camera=null,this.supports3d=!0,this.rendererType="html"}extendPrototype([HybridRendererBase],HybridRenderer),HybridRenderer.prototype.createComp=function(e){return this.supports3d?new HCompElement(e,this.globalData,this):new SVGCompElement(e,this.globalData,this)};var Expressions=function(){var e={};e.initExpressions=r;function r(n){var a=0,l=[];function c(){a+=1}function u(){a-=1,a===0&&m()}function d(E){l.indexOf(E)===-1&&l.push(E)}function m(){var E,C=l.length;for(E=0;E<C;E+=1)l[E].release();l.length=0}n.renderer.compInterface=CompExpressionInterface(n.renderer),n.renderer.globalData.projectInterface.registerComposition(n.renderer),n.renderer.globalData.pushExpression=c,n.renderer.globalData.popExpression=u,n.renderer.globalData.registerExpressionProperty=d}return e}();function _typeof$1(e){return typeof Symbol=="function"&&typeof Symbol.iterator=="symbol"?_typeof$1=function(n){return typeof n}:_typeof$1=function(n){return n&&typeof Symbol=="function"&&n.constructor===Symbol&&n!==Symbol.prototype?"symbol":typeof n},_typeof$1(e)}function seedRandom(e,r){var n=this,a=256,l=6,c=52,u="random",d=r.pow(a,l),m=r.pow(2,c),E=m*2,C=a-1,_;function A(g,y,I){var M=[];y=y===!0?{entropy:!0}:y||{};var j=k(P(y.entropy?[g,b(e)]:g===null?x():g,3),M),O=new T(M),L=function(){for(var q=O.g(l),V=d,Q=0;q<m;)q=(q+Q)*a,V*=a,Q=O.g(1);for(;q>=E;)q/=2,V/=2,Q>>>=1;return(q+Q)/V};return L.int32=function(){return O.g(4)|0},L.quick=function(){return O.g(4)/4294967296},L.double=L,k(b(O.S),e),(y.pass||I||function(N,q,V,Q){return Q&&(Q.S&&S(Q,O),N.state=function(){return S(O,{})}),V?(r[u]=N,q):N})(L,j,"global"in y?y.global:this==r,y.state)}r["seed"+u]=A;function T(g){var y,I=g.length,M=this,j=0,O=M.i=M.j=0,L=M.S=[];for(I||(g=[I++]);j<a;)L[j]=j++;for(j=0;j<a;j++)L[j]=L[O=C&O+g[j%I]+(y=L[j])],L[O]=y;M.g=function(N){for(var q,V=0,Q=M.i,W=M.j,G=M.S;N--;)q=G[Q=C&Q+1],V=V*a+G[C&(G[Q]=G[W=C&W+q])+(G[W]=q)];return M.i=Q,M.j=W,V}}function S(g,y){return y.i=g.i,y.j=g.j,y.S=g.S.slice(),y}function P(g,y){var I=[],M=_typeof$1(g),j;if(y&&M=="object")for(j in g)try{I.push(P(g[j],y-1))}catch{}return I.length?I:M=="string"?g:g+"\0"}function k(g,y){for(var I=g+"",M,j=0;j<I.length;)y[C&j]=C&(M^=y[C&j]*19)+I.charCodeAt(j++);return b(y)}function x(){try{var g=new Uint8Array(a);return(n.crypto||n.msCrypto).getRandomValues(g),b(g)}catch{var y=n.navigator,I=y&&y.plugins;return[+new Date,n,I,n.screen,b(e)]}}function b(g){return String.fromCharCode.apply(0,g)}k(r.random(),e)}function initialize$2(e){seedRandom([],e)}var propTypes={SHAPE:"shape"};function _typeof(e){return typeof Symbol=="function"&&typeof Symbol.iterator=="symbol"?_typeof=function(n){return typeof n}:_typeof=function(n){return n&&typeof Symbol=="function"&&n.constructor===Symbol&&n!==Symbol.prototype?"symbol":typeof n},_typeof(e)}var ExpressionManager=function(){var ob={},Math=BMMath,window=null,document=null,XMLHttpRequest=null,fetch=null,frames=null;initialize$2(BMMath);function $bm_isInstanceOfArray(e){return e.constructor===Array||e.constructor===Float32Array}function isNumerable(e,r){return e==="number"||e==="boolean"||e==="string"||r instanceof Number}function $bm_neg(e){var r=_typeof(e);if(r==="number"||r==="boolean"||e instanceof Number)return-e;if($bm_isInstanceOfArray(e)){var n,a=e.length,l=[];for(n=0;n<a;n+=1)l[n]=-e[n];return l}return e.propType?e.v:-e}var easeInBez=BezierFactory.getBezierEasing(.333,0,.833,.833,"easeIn").get,easeOutBez=BezierFactory.getBezierEasing(.167,.167,.667,1,"easeOut").get,easeInOutBez=BezierFactory.getBezierEasing(.33,0,.667,1,"easeInOut").get;function sum(e,r){var n=_typeof(e),a=_typeof(r);if(n==="string"||a==="string"||isNumerable(n,e)&&isNumerable(a,r))return e+r;if($bm_isInstanceOfArray(e)&&isNumerable(a,r))return e=e.slice(0),e[0]+=r,e;if(isNumerable(n,e)&&$bm_isInstanceOfArray(r))return r=r.slice(0),r[0]=e+r[0],r;if($bm_isInstanceOfArray(e)&&$bm_isInstanceOfArray(r)){for(var l=0,c=e.length,u=r.length,d=[];l<c||l<u;)(typeof e[l]=="number"||e[l]instanceof Number)&&(typeof r[l]=="number"||r[l]instanceof Number)?d[l]=e[l]+r[l]:d[l]=r[l]===void 0?e[l]:e[l]||r[l],l+=1;return d}return 0}var add=sum;function sub(e,r){var n=_typeof(e),a=_typeof(r);if(isNumerable(n,e)&&isNumerable(a,r))return n==="string"&&(e=parseInt(e,10)),a==="string"&&(r=parseInt(r,10)),e-r;if($bm_isInstanceOfArray(e)&&isNumerable(a,r))return e=e.slice(0),e[0]-=r,e;if(isNumerable(n,e)&&$bm_isInstanceOfArray(r))return r=r.slice(0),r[0]=e-r[0],r;if($bm_isInstanceOfArray(e)&&$bm_isInstanceOfArray(r)){for(var l=0,c=e.length,u=r.length,d=[];l<c||l<u;)(typeof e[l]=="number"||e[l]instanceof Number)&&(typeof r[l]=="number"||r[l]instanceof Number)?d[l]=e[l]-r[l]:d[l]=r[l]===void 0?e[l]:e[l]||r[l],l+=1;return d}return 0}function mul(e,r){var n=_typeof(e),a=_typeof(r),l;if(isNumerable(n,e)&&isNumerable(a,r))return e*r;var c,u;if($bm_isInstanceOfArray(e)&&isNumerable(a,r)){for(u=e.length,l=createTypedArray("float32",u),c=0;c<u;c+=1)l[c]=e[c]*r;return l}if(isNumerable(n,e)&&$bm_isInstanceOfArray(r)){for(u=r.length,l=createTypedArray("float32",u),c=0;c<u;c+=1)l[c]=e*r[c];return l}return 0}function div(e,r){var n=_typeof(e),a=_typeof(r),l;if(isNumerable(n,e)&&isNumerable(a,r))return e/r;var c,u;if($bm_isInstanceOfArray(e)&&isNumerable(a,r)){for(u=e.length,l=createTypedArray("float32",u),c=0;c<u;c+=1)l[c]=e[c]/r;return l}if(isNumerable(n,e)&&$bm_isInstanceOfArray(r)){for(u=r.length,l=createTypedArray("float32",u),c=0;c<u;c+=1)l[c]=e/r[c];return l}return 0}function mod(e,r){return typeof e=="string"&&(e=parseInt(e,10)),typeof r=="string"&&(r=parseInt(r,10)),e%r}var $bm_sum=sum,$bm_sub=sub,$bm_mul=mul,$bm_div=div,$bm_mod=mod;function clamp(e,r,n){if(r>n){var a=n;n=r,r=a}return Math.min(Math.max(e,r),n)}function radiansToDegrees(e){return e/degToRads}var radians_to_degrees=radiansToDegrees;function degreesToRadians(e){return e*degToRads}var degrees_to_radians=radiansToDegrees,helperLengthArray=[0,0,0,0,0,0];function length(e,r){if(typeof e=="number"||e instanceof Number)return r=r||0,Math.abs(e-r);r||(r=helperLengthArray);var n,a=Math.min(e.length,r.length),l=0;for(n=0;n<a;n+=1)l+=Math.pow(r[n]-e[n],2);return Math.sqrt(l)}function normalize(e){return div(e,length(e))}function rgbToHsl(e){var r=e[0],n=e[1],a=e[2],l=Math.max(r,n,a),c=Math.min(r,n,a),u,d,m=(l+c)/2;if(l===c)u=0,d=0;else{var E=l-c;switch(d=m>.5?E/(2-l-c):E/(l+c),l){case r:u=(n-a)/E+(n<a?6:0);break;case n:u=(a-r)/E+2;break;case a:u=(r-n)/E+4;break}u/=6}return[u,d,m,e[3]]}function hue2rgb(e,r,n){return n<0&&(n+=1),n>1&&(n-=1),n<1/6?e+(r-e)*6*n:n<1/2?r:n<2/3?e+(r-e)*(2/3-n)*6:e}function hslToRgb(e){var r=e[0],n=e[1],a=e[2],l,c,u;if(n===0)l=a,u=a,c=a;else{var d=a<.5?a*(1+n):a+n-a*n,m=2*a-d;l=hue2rgb(m,d,r+1/3),c=hue2rgb(m,d,r),u=hue2rgb(m,d,r-1/3)}return[l,c,u,e[3]]}function linear(e,r,n,a,l){if((a===void 0||l===void 0)&&(a=r,l=n,r=0,n=1),n<r){var c=n;n=r,r=c}if(e<=r)return a;if(e>=n)return l;var u=n===r?0:(e-r)/(n-r);if(!a.length)return a+(l-a)*u;var d,m=a.length,E=createTypedArray("float32",m);for(d=0;d<m;d+=1)E[d]=a[d]+(l[d]-a[d])*u;return E}function random(e,r){if(r===void 0&&(e===void 0?(e=0,r=1):(r=e,e=void 0)),r.length){var n,a=r.length;e||(e=createTypedArray("float32",a));var l=createTypedArray("float32",a),c=BMMath.random();for(n=0;n<a;n+=1)l[n]=e[n]+c*(r[n]-e[n]);return l}e===void 0&&(e=0);var u=BMMath.random();return e+u*(r-e)}function createPath(e,r,n,a){var l,c=e.length,u=shapePool.newElement();u.setPathData(!!a,c);var d=[0,0],m,E;for(l=0;l<c;l+=1)m=r&&r[l]?r[l]:d,E=n&&n[l]?n[l]:d,u.setTripleAt(e[l][0],e[l][1],E[0]+e[l][0],E[1]+e[l][1],m[0]+e[l][0],m[1]+e[l][1],l,!0);return u}function initiateExpression(elem,data,property){var val=data.x,needsVelocity=/velocity(?![\w\d])/.test(val),_needsRandom=val.indexOf("random")!==-1,elemType=elem.data.ty,transform,$bm_transform,content,effect,thisProperty=property;thisProperty.valueAtTime=thisProperty.getValueAtTime,Object.defineProperty(thisProperty,"value",{get:function(){return thisProperty.v}}),elem.comp.frameDuration=1/elem.comp.globalData.frameRate,elem.comp.displayStartTime=0;var inPoint=elem.data.ip/elem.comp.globalData.frameRate,outPoint=elem.data.op/elem.comp.globalData.frameRate,width=elem.data.sw?elem.data.sw:0,height=elem.data.sh?elem.data.sh:0,name=elem.data.nm,loopIn,loop_in,loopOut,loop_out,smooth,toWorld,fromWorld,fromComp,toComp,fromCompToSurface,position,rotation,anchorPoint,scale,thisLayer,thisComp,mask,valueAtTime,velocityAtTime,scoped_bm_rt,expression_function=eval("[function _expression_function(){"+val+";scoped_bm_rt=$bm_rt}]")[0],numKeys=property.kf?data.k.length:0,active=!this.data||this.data.hd!==!0,wiggle=function e(r,n){var a,l,c=this.pv.length?this.pv.length:1,u=createTypedArray("float32",c);r=5;var d=Math.floor(time*r);for(a=0,l=0;a<d;){for(l=0;l<c;l+=1)u[l]+=-n+n*2*BMMath.random();a+=1}var m=time*r,E=m-Math.floor(m),C=createTypedArray("float32",c);if(c>1){for(l=0;l<c;l+=1)C[l]=this.pv[l]+u[l]+(-n+n*2*BMMath.random())*E;return C}return this.pv+u[0]+(-n+n*2*BMMath.random())*E}.bind(this);thisProperty.loopIn&&(loopIn=thisProperty.loopIn.bind(thisProperty),loop_in=loopIn),thisProperty.loopOut&&(loopOut=thisProperty.loopOut.bind(thisProperty),loop_out=loopOut),thisProperty.smooth&&(smooth=thisProperty.smooth.bind(thisProperty));function loopInDuration(e,r){return loopIn(e,r,!0)}function loopOutDuration(e,r){return loopOut(e,r,!0)}this.getValueAtTime&&(valueAtTime=this.getValueAtTime.bind(this)),this.getVelocityAtTime&&(velocityAtTime=this.getVelocityAtTime.bind(this));var comp=elem.comp.globalData.projectInterface.bind(elem.comp.globalData.projectInterface);function lookAt(e,r){var n=[r[0]-e[0],r[1]-e[1],r[2]-e[2]],a=Math.atan2(n[0],Math.sqrt(n[1]*n[1]+n[2]*n[2]))/degToRads,l=-Math.atan2(n[1],n[2])/degToRads;return[l,a,0]}function easeOut(e,r,n,a,l){return applyEase(easeOutBez,e,r,n,a,l)}function easeIn(e,r,n,a,l){return applyEase(easeInBez,e,r,n,a,l)}function ease(e,r,n,a,l){return applyEase(easeInOutBez,e,r,n,a,l)}function applyEase(e,r,n,a,l,c){l===void 0?(l=n,c=a):r=(r-n)/(a-n),r>1?r=1:r<0&&(r=0);var u=e(r);if($bm_isInstanceOfArray(l)){var d,m=l.length,E=createTypedArray("float32",m);for(d=0;d<m;d+=1)E[d]=(c[d]-l[d])*u+l[d];return E}return(c-l)*u+l}function nearestKey(e){var r,n=data.k.length,a,l;if(!data.k.length||typeof data.k[0]=="number")a=0,l=0;else if(a=-1,e*=elem.comp.globalData.frameRate,e<data.k[0].t)a=1,l=data.k[0].t;else{for(r=0;r<n-1;r+=1)if(e===data.k[r].t){a=r+1,l=data.k[r].t;break}else if(e>data.k[r].t&&e<data.k[r+1].t){e-data.k[r].t>data.k[r+1].t-e?(a=r+2,l=data.k[r+1].t):(a=r+1,l=data.k[r].t);break}a===-1&&(a=r+1,l=data.k[r].t)}var c={};return c.index=a,c.time=l/elem.comp.globalData.frameRate,c}function key(e){var r,n,a;if(!data.k.length||typeof data.k[0]=="number")throw new Error("The property has no keyframe at index "+e);e-=1,r={time:data.k[e].t/elem.comp.globalData.frameRate,value:[]};var l=Object.prototype.hasOwnProperty.call(data.k[e],"s")?data.k[e].s:data.k[e-1].e;for(a=l.length,n=0;n<a;n+=1)r[n]=l[n],r.value[n]=l[n];return r}function framesToTime(e,r){return r||(r=elem.comp.globalData.frameRate),e/r}function timeToFrames(e,r){return!e&&e!==0&&(e=time),r||(r=elem.comp.globalData.frameRate),e*r}function seedRandom(e){BMMath.seedrandom(randSeed+e)}function sourceRectAtTime(){return elem.sourceRectAtTime()}function substring(e,r){return typeof value=="string"?r===void 0?value.substring(e):value.substring(e,r):""}function substr(e,r){return typeof value=="string"?r===void 0?value.substr(e):value.substr(e,r):""}function posterizeTime(e){time=e===0?0:Math.floor(time*e)/e,value=valueAtTime(time)}var time,velocity,value,text,textIndex,textTotal,selectorValue,index=elem.data.ind,hasParent=!!(elem.hierarchy&&elem.hierarchy.length),parent,randSeed=Math.floor(Math.random()*1e6),globalData=elem.globalData;function executeExpression(e){return value=e,this.frameExpressionId===elem.globalData.frameId&&this.propType!=="textSelector"?value:(this.propType==="textSelector"&&(textIndex=this.textIndex,textTotal=this.textTotal,selectorValue=this.selectorValue),thisLayer||(text=elem.layerInterface.text,thisLayer=elem.layerInterface,thisComp=elem.comp.compInterface,toWorld=thisLayer.toWorld.bind(thisLayer),fromWorld=thisLayer.fromWorld.bind(thisLayer),fromComp=thisLayer.fromComp.bind(thisLayer),toComp=thisLayer.toComp.bind(thisLayer),mask=thisLayer.mask?thisLayer.mask.bind(thisLayer):null,fromCompToSurface=fromComp),transform||(transform=elem.layerInterface("ADBE Transform Group"),$bm_transform=transform,transform&&(anchorPoint=transform.anchorPoint)),elemType===4&&!content&&(content=thisLayer("ADBE Root Vectors Group")),effect||(effect=thisLayer(4)),hasParent=!!(elem.hierarchy&&elem.hierarchy.length),hasParent&&!parent&&(parent=elem.hierarchy[0].layerInterface),time=this.comp.renderedFrame/this.comp.globalData.frameRate,_needsRandom&&seedRandom(randSeed+time),needsVelocity&&(velocity=velocityAtTime(time)),expression_function(),this.frameExpressionId=elem.globalData.frameId,scoped_bm_rt=scoped_bm_rt.propType===propTypes.SHAPE?scoped_bm_rt.v:scoped_bm_rt,scoped_bm_rt)}return executeExpression.__preventDeadCodeRemoval=[$bm_transform,anchorPoint,time,velocity,inPoint,outPoint,width,height,name,loop_in,loop_out,smooth,toComp,fromCompToSurface,toWorld,fromWorld,mask,position,rotation,scale,thisComp,numKeys,active,wiggle,loopInDuration,loopOutDuration,comp,lookAt,easeOut,easeIn,ease,nearestKey,key,text,textIndex,textTotal,selectorValue,framesToTime,timeToFrames,sourceRectAtTime,substring,substr,posterizeTime,index,globalData],executeExpression}return ob.initiateExpression=initiateExpression,ob.__preventDeadCodeRemoval=[window,document,XMLHttpRequest,fetch,frames,$bm_neg,add,$bm_sum,$bm_sub,$bm_mul,$bm_div,$bm_mod,clamp,radians_to_degrees,degreesToRadians,degrees_to_radians,normalize,rgbToHsl,hslToRgb,linear,random,createPath],ob}(),expressionHelpers=function(){function e(u,d,m){d.x&&(m.k=!0,m.x=!0,m.initiateExpression=ExpressionManager.initiateExpression,m.effectsSequence.push(m.initiateExpression(u,d,m).bind(m)))}function r(u){return u*=this.elem.globalData.frameRate,u-=this.offsetTime,u!==this._cachingAtTime.lastFrame&&(this._cachingAtTime.lastIndex=this._cachingAtTime.lastFrame<u?this._cachingAtTime.lastIndex:0,this._cachingAtTime.value=this.interpolateValue(u,this._cachingAtTime),this._cachingAtTime.lastFrame=u),this._cachingAtTime.value}function n(u){var d=-.01,m=this.getValueAtTime(u),E=this.getValueAtTime(u+d),C=0;if(m.length){var _;for(_=0;_<m.length;_+=1)C+=Math.pow(E[_]-m[_],2);C=Math.sqrt(C)*100}else C=0;return C}function a(u){if(this.vel!==void 0)return this.vel;var d=-.001,m=this.getValueAtTime(u),E=this.getValueAtTime(u+d),C;if(m.length){C=createTypedArray("float32",m.length);var _;for(_=0;_<m.length;_+=1)C[_]=(E[_]-m[_])/d}else C=(E-m)/d;return C}function l(){return this.pv}function c(u){this.propertyGroup=u}return{searchExpressions:e,getSpeedAtTime:n,getVelocityAtTime:a,getValueAtTime:r,getStaticValueAtTime:l,setGroupProperty:c}}();function addPropertyDecorator(){function e(A,T,S){if(!this.k||!this.keyframes)return this.pv;A=A?A.toLowerCase():"";var P=this.comp.renderedFrame,k=this.keyframes,x=k[k.length-1].t;if(P<=x)return this.pv;var b,g;S?(T?b=Math.abs(x-this.elem.comp.globalData.frameRate*T):b=Math.max(0,x-this.elem.data.ip),g=x-b):((!T||T>k.length-1)&&(T=k.length-1),g=k[k.length-1-T].t,b=x-g);var y,I,M;if(A==="pingpong"){var j=Math.floor((P-g)/b);if(j%2!=0)return this.getValueAtTime((b-(P-g)%b+g)/this.comp.globalData.frameRate,0)}else if(A==="offset"){var O=this.getValueAtTime(g/this.comp.globalData.frameRate,0),L=this.getValueAtTime(x/this.comp.globalData.frameRate,0),N=this.getValueAtTime(((P-g)%b+g)/this.comp.globalData.frameRate,0),q=Math.floor((P-g)/b);if(this.pv.length){for(M=new Array(O.length),I=M.length,y=0;y<I;y+=1)M[y]=(L[y]-O[y])*q+N[y];return M}return(L-O)*q+N}else if(A==="continue"){var V=this.getValueAtTime(x/this.comp.globalData.frameRate,0),Q=this.getValueAtTime((x-.001)/this.comp.globalData.frameRate,0);if(this.pv.length){for(M=new Array(V.length),I=M.length,y=0;y<I;y+=1)M[y]=V[y]+(V[y]-Q[y])*((P-x)/this.comp.globalData.frameRate)/5e-4;return M}return V+(V-Q)*((P-x)/.001)}return this.getValueAtTime(((P-g)%b+g)/this.comp.globalData.frameRate,0)}function r(A,T,S){if(!this.k)return this.pv;A=A?A.toLowerCase():"";var P=this.comp.renderedFrame,k=this.keyframes,x=k[0].t;if(P>=x)return this.pv;var b,g;S?(T?b=Math.abs(this.elem.comp.globalData.frameRate*T):b=Math.max(0,this.elem.data.op-x),g=x+b):((!T||T>k.length-1)&&(T=k.length-1),g=k[T].t,b=g-x);var y,I,M;if(A==="pingpong"){var j=Math.floor((x-P)/b);if(j%2==0)return this.getValueAtTime(((x-P)%b+x)/this.comp.globalData.frameRate,0)}else if(A==="offset"){var O=this.getValueAtTime(x/this.comp.globalData.frameRate,0),L=this.getValueAtTime(g/this.comp.globalData.frameRate,0),N=this.getValueAtTime((b-(x-P)%b+x)/this.comp.globalData.frameRate,0),q=Math.floor((x-P)/b)+1;if(this.pv.length){for(M=new Array(O.length),I=M.length,y=0;y<I;y+=1)M[y]=N[y]-(L[y]-O[y])*q;return M}return N-(L-O)*q}else if(A==="continue"){var V=this.getValueAtTime(x/this.comp.globalData.frameRate,0),Q=this.getValueAtTime((x+.001)/this.comp.globalData.frameRate,0);if(this.pv.length){for(M=new Array(V.length),I=M.length,y=0;y<I;y+=1)M[y]=V[y]+(V[y]-Q[y])*(x-P)/.001;return M}return V+(V-Q)*(x-P)/.001}return this.getValueAtTime((b-((x-P)%b+x))/this.comp.globalData.frameRate,0)}function n(A,T){if(!this.k)return this.pv;if(A=(A||.4)*.5,T=Math.floor(T||5),T<=1)return this.pv;var S=this.comp.renderedFrame/this.comp.globalData.frameRate,P=S-A,k=S+A,x=T>1?(k-P)/(T-1):1,b=0,g=0,y;this.pv.length?y=createTypedArray("float32",this.pv.length):y=0;for(var I;b<T;){if(I=this.getValueAtTime(P+b*x),this.pv.length)for(g=0;g<this.pv.length;g+=1)y[g]+=I[g];else y+=I;b+=1}if(this.pv.length)for(g=0;g<this.pv.length;g+=1)y[g]/=T;else y/=T;return y}function a(A){this._transformCachingAtTime||(this._transformCachingAtTime={v:new Matrix});var T=this._transformCachingAtTime.v;if(T.cloneFromProps(this.pre.props),this.appliedTransformations<1){var S=this.a.getValueAtTime(A);T.translate(-S[0]*this.a.mult,-S[1]*this.a.mult,S[2]*this.a.mult)}if(this.appliedTransformations<2){var P=this.s.getValueAtTime(A);T.scale(P[0]*this.s.mult,P[1]*this.s.mult,P[2]*this.s.mult)}if(this.sk&&this.appliedTransformations<3){var k=this.sk.getValueAtTime(A),x=this.sa.getValueAtTime(A);T.skewFromAxis(-k*this.sk.mult,x*this.sa.mult)}if(this.r&&this.appliedTransformations<4){var b=this.r.getValueAtTime(A);T.rotate(-b*this.r.mult)}else if(!this.r&&this.appliedTransformations<4){var g=this.rz.getValueAtTime(A),y=this.ry.getValueAtTime(A),I=this.rx.getValueAtTime(A),M=this.or.getValueAtTime(A);T.rotateZ(-g*this.rz.mult).rotateY(y*this.ry.mult).rotateX(I*this.rx.mult).rotateZ(-M[2]*this.or.mult).rotateY(M[1]*this.or.mult).rotateX(M[0]*this.or.mult)}if(this.data.p&&this.data.p.s){var j=this.px.getValueAtTime(A),O=this.py.getValueAtTime(A);if(this.data.p.z){var L=this.pz.getValueAtTime(A);T.translate(j*this.px.mult,O*this.py.mult,-L*this.pz.mult)}else T.translate(j*this.px.mult,O*this.py.mult,0)}else{var N=this.p.getValueAtTime(A);T.translate(N[0]*this.p.mult,N[1]*this.p.mult,-N[2]*this.p.mult)}return T}function l(){return this.v.clone(new Matrix)}var c=TransformPropertyFactory.getTransformProperty;TransformPropertyFactory.getTransformProperty=function(A,T,S){var P=c(A,T,S);return P.dynamicProperties.length?P.getValueAtTime=a.bind(P):P.getValueAtTime=l.bind(P),P.setGroupProperty=expressionHelpers.setGroupProperty,P};var u=PropertyFactory.getProp;PropertyFactory.getProp=function(A,T,S,P,k){var x=u(A,T,S,P,k);x.kf?x.getValueAtTime=expressionHelpers.getValueAtTime.bind(x):x.getValueAtTime=expressionHelpers.getStaticValueAtTime.bind(x),x.setGroupProperty=expressionHelpers.setGroupProperty,x.loopOut=e,x.loopIn=r,x.smooth=n,x.getVelocityAtTime=expressionHelpers.getVelocityAtTime.bind(x),x.getSpeedAtTime=expressionHelpers.getSpeedAtTime.bind(x),x.numKeys=T.a===1?T.k.length:0,x.propertyIndex=T.ix;var b=0;return S!==0&&(b=createTypedArray("float32",T.a===1?T.k[0].s.length:T.k.length)),x._cachingAtTime={lastFrame:initialDefaultFrame,lastIndex:0,value:b},expressionHelpers.searchExpressions(A,T,x),x.k&&k.addDynamicProperty(x),x};function d(A){return this._cachingAtTime||(this._cachingAtTime={shapeValue:shapePool.clone(this.pv),lastIndex:0,lastTime:initialDefaultFrame}),A*=this.elem.globalData.frameRate,A-=this.offsetTime,A!==this._cachingAtTime.lastTime&&(this._cachingAtTime.lastIndex=this._cachingAtTime.lastTime<A?this._caching.lastIndex:0,this._cachingAtTime.lastTime=A,this.interpolateShape(A,this._cachingAtTime.shapeValue,this._cachingAtTime)),this._cachingAtTime.shapeValue}var m=ShapePropertyFactory.getConstructorFunction(),E=ShapePropertyFactory.getKeyframedConstructorFunction();function C(){}C.prototype={vertices:function(T,S){this.k&&this.getValue();var P=this.v;S!==void 0&&(P=this.getValueAtTime(S,0));var k,x=P._length,b=P[T],g=P.v,y=createSizedArray(x);for(k=0;k<x;k+=1)T==="i"||T==="o"?y[k]=[b[k][0]-g[k][0],b[k][1]-g[k][1]]:y[k]=[b[k][0],b[k][1]];return y},points:function(T){return this.vertices("v",T)},inTangents:function(T){return this.vertices("i",T)},outTangents:function(T){return this.vertices("o",T)},isClosed:function(){return this.v.c},pointOnPath:function(T,S){var P=this.v;S!==void 0&&(P=this.getValueAtTime(S,0)),this._segmentsLength||(this._segmentsLength=bez.getSegmentsLength(P));for(var k=this._segmentsLength,x=k.lengths,b=k.totalLength*T,g=0,y=x.length,I=0,M;g<y;){if(I+x[g].addedLength>b){var j=g,O=P.c&&g===y-1?0:g+1,L=(b-I)/x[g].addedLength;M=bez.getPointInSegment(P.v[j],P.v[O],P.o[j],P.i[O],L,x[g]);break}else I+=x[g].addedLength;g+=1}return M||(M=P.c?[P.v[0][0],P.v[0][1]]:[P.v[P._length-1][0],P.v[P._length-1][1]]),M},vectorOnPath:function(T,S,P){T==1?T=this.v.c:T==0&&(T=.999);var k=this.pointOnPath(T,S),x=this.pointOnPath(T+.001,S),b=x[0]-k[0],g=x[1]-k[1],y=Math.sqrt(Math.pow(b,2)+Math.pow(g,2));if(y===0)return[0,0];var I=P==="tangent"?[b/y,g/y]:[-g/y,b/y];return I},tangentOnPath:function(T,S){return this.vectorOnPath(T,S,"tangent")},normalOnPath:function(T,S){return this.vectorOnPath(T,S,"normal")},setGroupProperty:expressionHelpers.setGroupProperty,getValueAtTime:expressionHelpers.getStaticValueAtTime},extendPrototype([C],m),extendPrototype([C],E),E.prototype.getValueAtTime=d,E.prototype.initiateExpression=ExpressionManager.initiateExpression;var _=ShapePropertyFactory.getShapeProp;ShapePropertyFactory.getShapeProp=function(A,T,S,P,k){var x=_(A,T,S,P,k);return x.propertyIndex=T.ix,x.lock=!1,S===3?expressionHelpers.searchExpressions(A,T.pt,x):S===4&&expressionHelpers.searchExpressions(A,T.ks,x),x.k&&A.addDynamicProperty(x),x}}function initialize$1(){addPropertyDecorator()}function addDecorator(){function e(){return this.data.d.x?(this.calculateExpression=ExpressionManager.initiateExpression.bind(this)(this.elem,this.data.d,this),this.addEffect(this.getExpressionValue.bind(this)),!0):null}TextProperty.prototype.getExpressionValue=function(r,n){var a=this.calculateExpression(n);if(r.t!==a){var l={};return this.copyData(l,r),l.t=a.toString(),l.__complete=!1,l}return r},TextProperty.prototype.searchProperty=function(){var r=this.searchKeyframes(),n=this.searchExpressions();return this.kf=r||n,this.kf},TextProperty.prototype.searchExpressions=e}function initialize(){addDecorator()}return registerRenderer("canvas",CanvasRenderer),registerRenderer("html",HybridRenderer),registerRenderer("svg",SVGRenderer),ShapeModifiers.registerModifier("tm",TrimModifier),ShapeModifiers.registerModifier("pb",PuckerAndBloatModifier),ShapeModifiers.registerModifier("rp",RepeaterModifier),ShapeModifiers.registerModifier("rd",RoundCornersModifier),setExpressionsPlugin(Expressions),initialize$1(),initialize(),registerEffect(20,SVGTintFilter,!0),registerEffect(21,SVGFillFilter,!0),registerEffect(22,SVGStrokeEffect,!1),registerEffect(23,SVGTritoneFilter,!0),registerEffect(24,SVGProLevelsFilter,!0),registerEffect(25,SVGDropShadowEffect,!0),registerEffect(28,SVGMatte3Effect,!1),registerEffect(29,SVGGaussianBlurEffect,!0),lottie})})(lottie$1,lottie$1.exports);var lottie=lottie$1.exports;function ownKeys$3(e,r){var n=Object.keys(e);if(Object.getOwnPropertySymbols){var a=Object.getOwnPropertySymbols(e);r&&(a=a.filter(function(l){return Object.getOwnPropertyDescriptor(e,l).enumerable})),n.push.apply(n,a)}return n}function _objectSpread2$1(e){for(var r=1;r<arguments.length;r++){var n=arguments[r]!=null?arguments[r]:{};r%2?ownKeys$3(Object(n),!0).forEach(function(a){_defineProperty$3(e,a,n[a])}):Object.getOwnPropertyDescriptors?Object.defineProperties(e,Object.getOwnPropertyDescriptors(n)):ownKeys$3(Object(n)).forEach(function(a){Object.defineProperty(e,a,Object.getOwnPropertyDescriptor(n,a))})}return e}function _defineProperty$3(e,r,n){return r in e?Object.defineProperty(e,r,{value:n,enumerable:!0,configurable:!0,writable:!0}):e[r]=n,e}function _objectWithoutPropertiesLoose$2(e,r){if(e==null)return{};var n={},a=Object.keys(e),l,c;for(c=0;c<a.length;c++)l=a[c],!(r.indexOf(l)>=0)&&(n[l]=e[l]);return n}function _objectWithoutProperties$1(e,r){if(e==null)return{};var n=_objectWithoutPropertiesLoose$2(e,r),a,l;if(Object.getOwnPropertySymbols){var c=Object.getOwnPropertySymbols(e);for(l=0;l<c.length;l++)a=c[l],!(r.indexOf(a)>=0)&&(!Object.prototype.propertyIsEnumerable.call(e,a)||(n[a]=e[a]))}return n}function _slicedToArray$2(e,r){return _arrayWithHoles$2(e)||_iterableToArrayLimit$2(e,r)||_unsupportedIterableToArray$2(e,r)||_nonIterableRest$2()}function _arrayWithHoles$2(e){if(Array.isArray(e))return e}function _iterableToArrayLimit$2(e,r){var n=e==null?null:typeof Symbol!="undefined"&&e[Symbol.iterator]||e["@@iterator"];if(n!=null){var a=[],l=!0,c=!1,u,d;try{for(n=n.call(e);!(l=(u=n.next()).done)&&(a.push(u.value),!(r&&a.length===r));l=!0);}catch(m){c=!0,d=m}finally{try{!l&&n.return!=null&&n.return()}finally{if(c)throw d}}return a}}function _unsupportedIterableToArray$2(e,r){if(!!e){if(typeof e=="string")return _arrayLikeToArray$2(e,r);var n=Object.prototype.toString.call(e).slice(8,-1);if(n==="Object"&&e.constructor&&(n=e.constructor.name),n==="Map"||n==="Set")return Array.from(e);if(n==="Arguments"||/^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n))return _arrayLikeToArray$2(e,r)}}function _arrayLikeToArray$2(e,r){(r==null||r>e.length)&&(r=e.length);for(var n=0,a=new Array(r);n<r;n++)a[n]=e[n];return a}function _nonIterableRest$2(){throw new TypeError(`Invalid attempt to destructure non-iterable instance.
In order to be iterable, non-array objects must have a [Symbol.iterator]() method.`)}var _excluded$1=["animationData","loop","autoplay","initialSegment","onComplete","onLoopComplete","onEnterFrame","onSegmentStart","onConfigReady","onDataReady","onDataFailed","onLoadedImages","onDOMLoaded","onDestroy","lottieRef","renderer","name","assetsPath","rendererSettings"],useLottie=function e(r,n){var a=r.animationData,l=r.loop,c=r.autoplay,u=r.initialSegment,d=r.onComplete,m=r.onLoopComplete,E=r.onEnterFrame,C=r.onSegmentStart,_=r.onConfigReady,A=r.onDataReady,T=r.onDataFailed,S=r.onLoadedImages,P=r.onDOMLoaded,k=r.onDestroy;r.lottieRef,r.renderer,r.name,r.assetsPath,r.rendererSettings;var x=_objectWithoutProperties$1(r,_excluded$1),b=react.exports.useState(!1),g=_slicedToArray$2(b,2),y=g[0],I=g[1],M=react.exports.useRef(),j=react.exports.useRef(null),O=function(){var Y;(Y=M.current)===null||Y===void 0||Y.play()},L=function(){var Y;(Y=M.current)===null||Y===void 0||Y.stop()},N=function(){var Y;(Y=M.current)===null||Y===void 0||Y.pause()},q=function(Y){var Z;(Z=M.current)===null||Z===void 0||Z.setSpeed(Y)},V=function(Y,Z){var K;(K=M.current)===null||K===void 0||K.goToAndPlay(Y,Z)},Q=function(Y,Z){var K;(K=M.current)===null||K===void 0||K.goToAndStop(Y,Z)},W=function(Y){var Z;(Z=M.current)===null||Z===void 0||Z.setDirection(Y)},G=function(Y,Z){var K;(K=M.current)===null||K===void 0||K.playSegments(Y,Z)},R=function(Y){var Z;(Z=M.current)===null||Z===void 0||Z.setSubframe(Y)},D=function(Y){var Z;return(Z=M.current)===null||Z===void 0?void 0:Z.getDuration(Y)},F=function(){var Y;(Y=M.current)===null||Y===void 0||Y.destroy()},$=function(){var Y=arguments.length>0&&arguments[0]!==void 0?arguments[0]:{},Z;if(!!j.current){(Z=M.current)===null||Z===void 0||Z.destroy();var K=_objectSpread2$1(_objectSpread2$1(_objectSpread2$1({},r),Y),{},{container:j.current});M.current=lottie.loadAnimation(K),I(!!M.current)}};react.exports.useEffect(function(){$()},[a]),react.exports.useEffect(function(){!M.current||(M.current.loop=!!l,l&&M.current.isPaused&&M.current.play())},[l]),react.exports.useEffect(function(){!M.current||(M.current.autoplay=!!c)},[c]),react.exports.useEffect(function(){if(!!M.current){if(!u){M.current.resetSegments(!1);return}!Array.isArray(u)||!u.length||((M.current.currentRawFrame<u[0]||M.current.currentRawFrame>u[1])&&(M.current.currentRawFrame=u[0]),M.current.setSegment(u[0],u[1]))}},[u]),react.exports.useEffect(function(){var z=[{name:"complete",handler:d},{name:"loopComplete",handler:m},{name:"enterFrame",handler:E},{name:"segmentStart",handler:C},{name:"config_ready",handler:_},{name:"data_ready",handler:A},{name:"data_failed",handler:T},{name:"loaded_images",handler:S},{name:"DOMLoaded",handler:P},{name:"destroy",handler:k}],Y=z.filter(function(K){return K.handler!=null});if(!!Y.length){var Z=Y.map(function(K){var ue;return(ue=M.current)===null||ue===void 0||ue.addEventListener(K.name,K.handler),function(){var ie;(ie=M.current)===null||ie===void 0||ie.removeEventListener(K.name,K.handler)}});return function(){Z.forEach(function(K){return K()})}}},[d,m,E,C,_,A,T,S,P,k]);var U=jsx("div",B({},_objectSpread2$1({style:n,ref:j},x)));return{View:U,play:O,stop:L,pause:N,setSpeed:q,goToAndStop:Q,goToAndPlay:V,setDirection:W,playSegments:G,setSubframe:R,getDuration:D,destroy:F,animationContainerRef:j,animationLoaded:y,animationItem:M.current}};const v="5.5.8",fr=60,ip=0,op=110,w=144,h=105,nm="typing indicator",ddd=0,assets=[],layers=[{ddd:0,ind:1,ty:3,nm:"\u25BD Dots",sr:1,ks:{o:{a:0,k:100,ix:11},r:{a:0,k:0,ix:10},p:{a:0,k:[72,51,0],ix:2},a:{a:0,k:[36,9,0],ix:1},s:{a:0,k:[100,100,100],ix:6}},ao:0,ip:0,op:110,st:0,bm:0},{ddd:0,ind:2,ty:4,nm:"dot 1",parent:1,sr:1,ks:{o:{a:0,k:100,ix:11},r:{a:0,k:0,ix:10},p:{a:1,k:[{i:{x:.667,y:1},o:{x:.333,y:0},t:0,s:[9,9,0],to:[0,0,0],ti:[0,0,0]},{i:{x:.667,y:1},o:{x:.333,y:0},t:3.053,s:[9,10,0],to:[0,0,0],ti:[0,0,0]},{i:{x:.667,y:1},o:{x:.333,y:0},t:15.273,s:[9,-9,0],to:[0,0,0],ti:[0,0,0]},{i:{x:.667,y:1},o:{x:.333,y:0},t:25.174,s:[9,12,0],to:[0,0,0],ti:[0,0,0]},{t:32.744140625,s:[9,9,0]}],ix:2},a:{a:0,k:[0,0,0],ix:1},s:{a:0,k:[100,100,100],ix:6}},ao:0,shapes:[{ty:"gr",it:[{d:1,ty:"el",s:{a:0,k:[6,6],ix:2},p:{a:0,k:[0,0],ix:3},nm:"Ellipse Path 1",mn:"ADBE Vector Shape - Ellipse",hd:!1},{ty:"fl",c:{a:1,k:[{i:{x:[.472],y:[1]},o:{x:[1],y:[0]},t:0,s:[.690196078431,.725490196078,.752941176471,1]},{i:{x:[.49],y:[1]},o:{x:[1],y:[0]},t:15.273,s:[.349019616842,.392156869173,.427450984716,1]},{t:32.072265625,s:[.690196078431,.725490196078,.752941176471,1]}],ix:4},o:{a:0,k:100,ix:5},r:1,bm:0,nm:"Fill 1",mn:"ADBE Vector Graphic - Fill",hd:!1},{ty:"tr",p:{a:0,k:[0,0],ix:2},a:{a:0,k:[0,0],ix:1},s:{a:0,k:[300,300],ix:3},r:{a:0,k:0,ix:6},o:{a:0,k:100,ix:7},sk:{a:0,k:0,ix:4},sa:{a:0,k:0,ix:5},nm:"Transform"}],nm:"dot 1",np:2,cix:2,bm:0,ix:1,mn:"ADBE Vector Group",hd:!1}],ip:0,op:110,st:0,bm:0},{ddd:0,ind:3,ty:4,nm:"dot 2",parent:1,sr:1,ks:{o:{a:0,k:100,ix:11},r:{a:0,k:0,ix:10},p:{a:1,k:[{i:{x:.667,y:1},o:{x:.333,y:0},t:10.691,s:[36,9,0],to:[0,0,0],ti:[0,0,0]},{i:{x:.667,y:1},o:{x:.333,y:0},t:15.273,s:[36,10,0],to:[0,0,0],ti:[0,0,0]},{i:{x:.667,y:1},o:{x:.333,y:0},t:25.963,s:[36,-9,0],to:[0,0,0],ti:[0,0,0]},{i:{x:.667,y:1},o:{x:.333,y:0},t:36.984,s:[36,14.326,0],to:[0,0,0],ti:[0,0,0]},{t:43.97265625,s:[36,9,0]}],ix:2},a:{a:0,k:[0,0,0],ix:1},s:{a:0,k:[100,100,100],ix:6}},ao:0,shapes:[{ty:"gr",it:[{d:1,ty:"el",s:{a:0,k:[6,6],ix:2},p:{a:0,k:[0,0],ix:3},nm:"Ellipse Path 1",mn:"ADBE Vector Shape - Ellipse",hd:!1},{ty:"fl",c:{a:1,k:[{i:{x:[.472],y:[1]},o:{x:[1],y:[0]},t:10.691,s:[.690196078431,.725490196078,.752941176471,1]},{i:{x:[.49],y:[1]},o:{x:[1],y:[0]},t:25.963,s:[.349019616842,.392156869173,.427450984716,1]},{t:42.763671875,s:[.690196078431,.725490196078,.752941176471,1]}],ix:4},o:{a:0,k:100,ix:5},r:1,bm:0,nm:"Fill 1",mn:"ADBE Vector Graphic - Fill",hd:!1},{ty:"tr",p:{a:0,k:[0,0],ix:2},a:{a:0,k:[0,0],ix:1},s:{a:0,k:[300,300],ix:3},r:{a:0,k:0,ix:6},o:{a:0,k:100,ix:7},sk:{a:0,k:0,ix:4},sa:{a:0,k:0,ix:5},nm:"Transform"}],nm:"dot 2",np:2,cix:2,bm:0,ix:1,mn:"ADBE Vector Group",hd:!1}],ip:0,op:110,st:0,bm:0},{ddd:0,ind:4,ty:4,nm:"dot 3",parent:1,sr:1,ks:{o:{a:0,k:100,ix:11},r:{a:0,k:0,ix:10},p:{a:1,k:[{i:{x:.667,y:1},o:{x:.333,y:0},t:21.383,s:[63,9,0],to:[0,0,0],ti:[0,0,0]},{i:{x:.667,y:1},o:{x:.333,y:0},t:25.963,s:[63,10,0],to:[0,0,0],ti:[0,0,0]},{i:{x:.667,y:1},o:{x:.333,y:0},t:38.184,s:[63,-9,0],to:[0,0,0],ti:[0,0,0]},{i:{x:.667,y:1},o:{x:.333,y:0},t:49.594,s:[63,13.139,0],to:[0,0,0],ti:[0,0,0]},{t:56,s:[63,9,0]}],ix:2},a:{a:0,k:[0,0,0],ix:1},s:{a:0,k:[100,100,100],ix:6}},ao:0,shapes:[{ty:"gr",it:[{d:1,ty:"el",s:{a:0,k:[6,6],ix:2},p:{a:0,k:[0,0],ix:3},nm:"Ellipse Path 1",mn:"ADBE Vector Shape - Ellipse",hd:!1},{ty:"fl",c:{a:1,k:[{i:{x:[.472],y:[1]},o:{x:[1],y:[0]},t:21.383,s:[.690196078431,.725490196078,.752941176471,1]},{i:{x:[.49],y:[1]},o:{x:[1],y:[0]},t:36.656,s:[.349019616842,.392156869173,.427450984716,1]},{t:53.45703125,s:[.690196078431,.725490196078,.752941176471,1]}],ix:4},o:{a:0,k:100,ix:5},r:1,bm:0,nm:"Fill 1",mn:"ADBE Vector Graphic - Fill",hd:!1},{ty:"tr",p:{a:0,k:[0,0],ix:2},a:{a:0,k:[0,0],ix:1},s:{a:0,k:[300,300],ix:3},r:{a:0,k:0,ix:6},o:{a:0,k:100,ix:7},sk:{a:0,k:0,ix:4},sa:{a:0,k:0,ix:5},nm:"Transform"}],nm:"dot 3",np:2,cix:2,bm:0,ix:1,mn:"ADBE Vector Group",hd:!1}],ip:0,op:110,st:0,bm:0}],markers=[];var typingJson={v,fr,ip,op,w,h,nm,ddd,assets,layers,markers};const Typing=()=>{const e={animationData:typingJson,loop:!0,autoplay:!0},{View:r}=useLottie(e);return r},GroupAvatar=({groupInfo:e})=>jsxs(SimpleGrid,{columns:2,spacing:1,children:[e==null?void 0:e.slice(0,3).map((r,n)=>jsx(MyAvatar,{size:"2xs",src:r==null?void 0:r.avatar,name:r==null?void 0:r.fullName},n)),(e==null?void 0:e.length)>3&&jsx(Box,{display:"flex",alignItems:"center",justifyContent:"center",w:"16px",h:"16px",rounded:"full",bg:"orange.500",children:jsx(Text,{color:"white",fontSize:12,children:(e==null?void 0:e.length)-3<=9?`+${(e==null?void 0:e.length)-3}`:"..."})})]}),ConversationItem=({onClick:e,username:r,usernameClassName:n,lastMessage:a,seen:l,isActive:c,updatedAt:u,groupInfo:d,seenByCount:m,isTyping:E,avatarUrl:C,online:_,userTypingName:A})=>{const T=useColorModeValue("gray.500","slate.300"),S=()=>{if(m===(d==null?void 0:d.length)||!d)return null;if(d)return jsxs(Text,{color:T,fontSize:12,children:[m,"/",d==null?void 0:d.length]})};return jsxs(HStack,{_hover:{backgroundColor:"#e4e4e450"},w:"100%",align:"start",px:3,py:2,borderBottomWidth:1,cursor:"pointer",onClick:e,bg:useColorModeValue(c?"gray.100":"bg-white",c?"#1e293b":"#1e293b50"),children:[lodash.exports.isEmpty(d)?jsx(MyAvatar,{boxSize:"2em",src:C,name:r,children:jsx(AvatarBadge,{boxSize:"14px",bg:_?"green.500":"gray.300"})}):jsx(GroupAvatar,{groupInfo:d}),jsxs(Stack,{flex:1,spacing:1,children:[jsx(Text,{noOfLines:2,fontSize:15,lineHeight:"18px",color:useColorModeValue(l?"gray.700":"gray.900",l?"#ffffff50":"white"),className:`${n}`,textTransform:"capitalize",children:r}),E?jsxs(HStack,{flex:1,children:[jsx(Text,{noOfLines:1,fontSize:13,color:useColorModeValue("gray.700","white"),children:A==null?void 0:A.map(P=>P||"Someone is typing").join(",")}),jsx(Box,{display:"flex",alignItems:"center",justifyContent:"center",w:10,h:5,children:jsx(Typing,{})})]}):jsx(Text,{noOfLines:2,fontSize:12,lineHeight:"24px",fontWeight:l?"400":"600",color:useColorModeValue(l?"gray.700":"gray.900",l?"#ffffff50":"white"),children:a})]}),jsxs(VStack,{spacing:2,alignItems:"flex-end",minW:"50px",children:[jsx(Text,{color:useColorModeValue(l?"gray.700":"gray.900",l?"#ffffff50":"white"),fontSize:12,children:u}),jsxs(HStack,{justifyContent:"space-between",children:[l&&jsx(BsCheck2,{}),S()]})]})]})};var getRandomValues=typeof crypto!="undefined"&&crypto.getRandomValues&&crypto.getRandomValues.bind(crypto)||typeof msCrypto!="undefined"&&typeof msCrypto.getRandomValues=="function"&&msCrypto.getRandomValues.bind(msCrypto),rnds8=new Uint8Array(16);function rng(){if(!getRandomValues)throw new Error("crypto.getRandomValues() not supported. See https://github.com/uuidjs/uuid#getrandomvalues-not-supported");return getRandomValues(rnds8)}var byteToHex=[];for(var i=0;i<256;++i)byteToHex[i]=(i+256).toString(16).substr(1);function bytesToUuid(e,r){var n=r||0,a=byteToHex;return[a[e[n++]],a[e[n++]],a[e[n++]],a[e[n++]],"-",a[e[n++]],a[e[n++]],"-",a[e[n++]],a[e[n++]],"-",a[e[n++]],a[e[n++]],"-",a[e[n++]],a[e[n++]],a[e[n++]],a[e[n++]],a[e[n++]],a[e[n++]]].join("")}function v4(e,r,n){var a=r&&n||0;typeof e=="string"&&(r=e==="binary"?new Array(16):null,e=null),e=e||{};var l=e.random||(e.rng||rng)();if(l[6]=l[6]&15|64,l[8]=l[8]&63|128,r)for(var c=0;c<16;++c)r[a+c]=l[c];return r||bytesToUuid(l)}function _classCallCheck(e,r){if(!(e instanceof r))throw new TypeError("Cannot call a class as a function")}function _defineProperties(e,r){for(var n=0;n<r.length;n++){var a=r[n];a.enumerable=a.enumerable||!1,a.configurable=!0,"value"in a&&(a.writable=!0),Object.defineProperty(e,a.key,a)}}function _createClass(e,r,n){return r&&_defineProperties(e.prototype,r),n&&_defineProperties(e,n),e}function _defineProperty$2(e,r,n){return r in e?Object.defineProperty(e,r,{value:n,enumerable:!0,configurable:!0,writable:!0}):e[r]=n,e}function _extends$1(){return _extends$1=Object.assign||function(e){for(var r=1;r<arguments.length;r++){var n=arguments[r];for(var a in n)Object.prototype.hasOwnProperty.call(n,a)&&(e[a]=n[a])}return e},_extends$1.apply(this,arguments)}function ownKeys$2(e,r){var n=Object.keys(e);if(Object.getOwnPropertySymbols){var a=Object.getOwnPropertySymbols(e);r&&(a=a.filter(function(l){return Object.getOwnPropertyDescriptor(e,l).enumerable})),n.push.apply(n,a)}return n}function _objectSpread2(e){for(var r=1;r<arguments.length;r++){var n=arguments[r]!=null?arguments[r]:{};r%2?ownKeys$2(Object(n),!0).forEach(function(a){_defineProperty$2(e,a,n[a])}):Object.getOwnPropertyDescriptors?Object.defineProperties(e,Object.getOwnPropertyDescriptors(n)):ownKeys$2(Object(n)).forEach(function(a){Object.defineProperty(e,a,Object.getOwnPropertyDescriptor(n,a))})}return e}function _inherits(e,r){if(typeof r!="function"&&r!==null)throw new TypeError("Super expression must either be null or a function");e.prototype=Object.create(r&&r.prototype,{constructor:{value:e,writable:!0,configurable:!0}}),r&&_setPrototypeOf(e,r)}function _getPrototypeOf(e){return _getPrototypeOf=Object.setPrototypeOf?Object.getPrototypeOf:function(n){return n.__proto__||Object.getPrototypeOf(n)},_getPrototypeOf(e)}function _setPrototypeOf(e,r){return _setPrototypeOf=Object.setPrototypeOf||function(a,l){return a.__proto__=l,a},_setPrototypeOf(e,r)}function _assertThisInitialized(e){if(e===void 0)throw new ReferenceError("this hasn't been initialised - super() hasn't been called");return e}function _possibleConstructorReturn(e,r){return r&&(typeof r=="object"||typeof r=="function")?r:_assertThisInitialized(e)}var CONSTANT={GLOBAL:{HIDE:"__react_tooltip_hide_event",REBUILD:"__react_tooltip_rebuild_event",SHOW:"__react_tooltip_show_event"}},dispatchGlobalEvent=function e(r,n){var a;typeof window.CustomEvent=="function"?a=new window.CustomEvent(r,{detail:n}):(a=document.createEvent("Event"),a.initEvent(r,!1,!0,n)),window.dispatchEvent(a)};function staticMethods(e){e.hide=function(r){dispatchGlobalEvent(CONSTANT.GLOBAL.HIDE,{target:r})},e.rebuild=function(){dispatchGlobalEvent(CONSTANT.GLOBAL.REBUILD)},e.show=function(r){dispatchGlobalEvent(CONSTANT.GLOBAL.SHOW,{target:r})},e.prototype.globalRebuild=function(){this.mount&&(this.unbindListener(),this.bindListener())},e.prototype.globalShow=function(r){if(this.mount){var n=r&&r.detail&&r.detail.target&&!0||!1;this.showTooltip({currentTarget:n&&r.detail.target},!0)}},e.prototype.globalHide=function(r){if(this.mount){var n=r&&r.detail&&r.detail.target&&!0||!1;this.hideTooltip({currentTarget:n&&r.detail.target},n)}}}function windowListener(e){e.prototype.bindWindowEvents=function(r){window.removeEventListener(CONSTANT.GLOBAL.HIDE,this.globalHide),window.addEventListener(CONSTANT.GLOBAL.HIDE,this.globalHide,!1),window.removeEventListener(CONSTANT.GLOBAL.REBUILD,this.globalRebuild),window.addEventListener(CONSTANT.GLOBAL.REBUILD,this.globalRebuild,!1),window.removeEventListener(CONSTANT.GLOBAL.SHOW,this.globalShow),window.addEventListener(CONSTANT.GLOBAL.SHOW,this.globalShow,!1),r&&(window.removeEventListener("resize",this.onWindowResize),window.addEventListener("resize",this.onWindowResize,!1))},e.prototype.unbindWindowEvents=function(){window.removeEventListener(CONSTANT.GLOBAL.HIDE,this.globalHide),window.removeEventListener(CONSTANT.GLOBAL.REBUILD,this.globalRebuild),window.removeEventListener(CONSTANT.GLOBAL.SHOW,this.globalShow),window.removeEventListener("resize",this.onWindowResize)},e.prototype.onWindowResize=function(){!this.mount||this.hideTooltip()}}var checkStatus=function e(r,n){var a=this.state.show,l=this.props.id,c=this.isCapture(n.currentTarget),u=n.currentTarget.getAttribute("currentItem");c||n.stopPropagation(),a&&u==="true"?r||this.hideTooltip(n):(n.currentTarget.setAttribute("currentItem","true"),setUntargetItems(n.currentTarget,this.getTargetArray(l)),this.showTooltip(n))},setUntargetItems=function e(r,n){for(var a=0;a<n.length;a++)r!==n[a]?n[a].setAttribute("currentItem","false"):n[a].setAttribute("currentItem","true")},customListeners={id:"9b69f92e-d3fe-498b-b1b4-c5e63a51b0cf",set:function e(r,n,a){if(this.id in r){var l=r[this.id];l[n]=a}else Object.defineProperty(r,this.id,{configurable:!0,value:_defineProperty$2({},n,a)})},get:function e(r,n){var a=r[this.id];if(a!==void 0)return a[n]}};function customEvent(e){e.prototype.isCustomEvent=function(r){var n=this.state.event;return n||!!r.getAttribute("data-event")},e.prototype.customBindListener=function(r){var n=this,a=this.state,l=a.event,c=a.eventOff,u=r.getAttribute("data-event")||l,d=r.getAttribute("data-event-off")||c;u.split(" ").forEach(function(m){r.removeEventListener(m,customListeners.get(r,m));var E=checkStatus.bind(n,d);customListeners.set(r,m,E),r.addEventListener(m,E,!1)}),d&&d.split(" ").forEach(function(m){r.removeEventListener(m,n.hideTooltip),r.addEventListener(m,n.hideTooltip,!1)})},e.prototype.customUnbindListener=function(r){var n=this.state,a=n.event,l=n.eventOff,c=a||r.getAttribute("data-event"),u=l||r.getAttribute("data-event-off");r.removeEventListener(c,customListeners.get(r,a)),u&&r.removeEventListener(u,this.hideTooltip)}}function isCapture(e){e.prototype.isCapture=function(r){return r&&r.getAttribute("data-iscapture")==="true"||this.props.isCapture||!1}}function getEffect(e){e.prototype.getEffect=function(r){var n=r.getAttribute("data-effect");return n||this.props.effect||"float"}}var makeProxy=function e(r){var n={};for(var a in r)typeof r[a]=="function"?n[a]=r[a].bind(r):n[a]=r[a];return n},bodyListener=function e(r,n,a){var l=n.respectEffect,c=l===void 0?!1:l,u=n.customEvent,d=u===void 0?!1:u,m=this.props.id,E=a.target.getAttribute("data-tip")||null,C=a.target.getAttribute("data-for")||null,_=a.target;if(!(this.isCustomEvent(_)&&!d)){var A=m==null&&C==null||C===m;if(E!=null&&(!c||this.getEffect(_)==="float")&&A){var T=makeProxy(a);T.currentTarget=_,r(T)}}},findCustomEvents=function e(r,n){var a={};return r.forEach(function(l){var c=l.getAttribute(n);c&&c.split(" ").forEach(function(u){return a[u]=!0})}),a},getBody=function e(){return document.getElementsByTagName("body")[0]};function bodyMode(e){e.prototype.isBodyMode=function(){return!!this.props.bodyMode},e.prototype.bindBodyListener=function(r){var n=this,a=this.state,l=a.event,c=a.eventOff,u=a.possibleCustomEvents,d=a.possibleCustomEventsOff,m=getBody(),E=findCustomEvents(r,"data-event"),C=findCustomEvents(r,"data-event-off");l!=null&&(E[l]=!0),c!=null&&(C[c]=!0),u.split(" ").forEach(function(P){return E[P]=!0}),d.split(" ").forEach(function(P){return C[P]=!0}),this.unbindBodyListener(m);var _=this.bodyModeListeners={};l==null&&(_.mouseover=bodyListener.bind(this,this.showTooltip,{}),_.mousemove=bodyListener.bind(this,this.updateTooltip,{respectEffect:!0}),_.mouseout=bodyListener.bind(this,this.hideTooltip,{}));for(var A in E)_[A]=bodyListener.bind(this,function(P){var k=P.currentTarget.getAttribute("data-event-off")||c;checkStatus.call(n,k,P)},{customEvent:!0});for(var T in C)_[T]=bodyListener.bind(this,this.hideTooltip,{customEvent:!0});for(var S in _)m.addEventListener(S,_[S])},e.prototype.unbindBodyListener=function(r){r=r||getBody();var n=this.bodyModeListeners;for(var a in n)r.removeEventListener(a,n[a])}}var getMutationObserverClass=function e(){return window.MutationObserver||window.WebKitMutationObserver||window.MozMutationObserver};function trackRemoval(e){e.prototype.bindRemovalTracker=function(){var r=this,n=getMutationObserverClass();if(n!=null){var a=new n(function(l){for(var c=0;c<l.length;c++)for(var u=l[c],d=0;d<u.removedNodes.length;d++){var m=u.removedNodes[d];if(m===r.state.currentTarget){r.hideTooltip();return}}});a.observe(window.document,{childList:!0,subtree:!0}),this.removalTracker=a}},e.prototype.unbindRemovalTracker=function(){this.removalTracker&&(this.removalTracker.disconnect(),this.removalTracker=null)}}function getPosition(e,r,n,a,l,c,u){for(var d=getDimensions(n),m=d.width,E=d.height,C=getDimensions(r),_=C.width,A=C.height,T=getCurrentOffset(e,r,c),S=T.mouseX,P=T.mouseY,k=getDefaultPosition(c,_,A,m,E),x=calculateOffset(u),b=x.extraOffsetX,g=x.extraOffsetY,y=window.innerWidth,I=window.innerHeight,M=getParent(n),j=M.parentTop,O=M.parentLeft,L=function(te){var X=k[te].l;return S+X+b},N=function(te){var X=k[te].r;return S+X+b},q=function(te){var X=k[te].t;return P+X+g},V=function(te){var X=k[te].b;return P+X+g},Q=function(te){return L(te)<0},W=function(te){return N(te)>y},G=function(te){return q(te)<0},R=function(te){return V(te)>I},D=function(te){return Q(te)||W(te)||G(te)||R(te)},F=function(te){return!D(te)},$=["top","bottom","left","right"],U=[],z=0;z<4;z++){var Y=$[z];F(Y)&&U.push(Y)}var Z=!1,K,ue=l!==a;return F(l)&&ue?(Z=!0,K=l):U.length>0&&D(l)&&D(a)&&(Z=!0,K=U[0]),Z?{isNewState:!0,newState:{place:K}}:{isNewState:!1,position:{left:parseInt(L(a)-O,10),top:parseInt(q(a)-j,10)}}}var getDimensions=function e(r){var n=r.getBoundingClientRect(),a=n.height,l=n.width;return{height:parseInt(a,10),width:parseInt(l,10)}},getCurrentOffset=function e(r,n,a){var l=n.getBoundingClientRect(),c=l.top,u=l.left,d=getDimensions(n),m=d.width,E=d.height;return a==="float"?{mouseX:r.clientX,mouseY:r.clientY}:{mouseX:u+m/2,mouseY:c+E/2}},getDefaultPosition=function e(r,n,a,l,c){var u,d,m,E,C=3,_=2,A=12;return r==="float"?(u={l:-(l/2),r:l/2,t:-(c+C+_),b:-C},m={l:-(l/2),r:l/2,t:C+A,b:c+C+_+A},E={l:-(l+C+_),r:-C,t:-(c/2),b:c/2},d={l:C,r:l+C+_,t:-(c/2),b:c/2}):r==="solid"&&(u={l:-(l/2),r:l/2,t:-(a/2+c+_),b:-(a/2)},m={l:-(l/2),r:l/2,t:a/2,b:a/2+c+_},E={l:-(l+n/2+_),r:-(n/2),t:-(c/2),b:c/2},d={l:n/2,r:l+n/2+_,t:-(c/2),b:c/2}),{top:u,bottom:m,left:E,right:d}},calculateOffset=function e(r){var n=0,a=0;Object.prototype.toString.apply(r)==="[object String]"&&(r=JSON.parse(r.toString().replace(/'/g,'"')));for(var l in r)l==="top"?a-=parseInt(r[l],10):l==="bottom"?a+=parseInt(r[l],10):l==="left"?n-=parseInt(r[l],10):l==="right"&&(n+=parseInt(r[l],10));return{extraOffsetX:n,extraOffsetY:a}},getParent=function e(r){for(var n=r;n;){var a=window.getComputedStyle(n);if(a.getPropertyValue("transform")!=="none"||a.getPropertyValue("will-change")==="transform")break;n=n.parentElement}var l=n&&n.getBoundingClientRect().top||0,c=n&&n.getBoundingClientRect().left||0;return{parentTop:l,parentLeft:c}};function getTipContent(e,r,n,a){if(r)return r;if(n!=null)return n;if(n===null)return null;var l=/<br\s*\/?>/;return!a||a==="false"||!l.test(e)?e:e.split(l).map(function(c,u){return t.createElement("span",{key:u,className:"multi-line"},c)})}function parseAria(e){var r={};return Object.keys(e).filter(function(n){return/(^aria-\w+$|^role$)/.test(n)}).forEach(function(n){r[n]=e[n]}),r}function nodeListToArray(e){var r=e.length;return e.hasOwnProperty?Array.prototype.slice.call(e):new Array(r).fill().map(function(n){return e[n]})}function generateUUID(){return"t"+v4()}var baseCss=`.__react_component_tooltip {
  border-radius: 3px;
  display: inline-block;
  font-size: 13px;
  left: -999em;
  opacity: 0;
  padding: 8px 21px;
  position: fixed;
  pointer-events: none;
  transition: opacity 0.3s ease-out;
  top: -999em;
  visibility: hidden;
  z-index: 999;
}
.__react_component_tooltip.allow_hover, .__react_component_tooltip.allow_click {
  pointer-events: auto;
}
.__react_component_tooltip::before, .__react_component_tooltip::after {
  content: "";
  width: 0;
  height: 0;
  position: absolute;
}
.__react_component_tooltip.show {
  opacity: 0.9;
  margin-top: 0;
  margin-left: 0;
  visibility: visible;
}
.__react_component_tooltip.place-top::before {
  border-left: 10px solid transparent;
  border-right: 10px solid transparent;
  bottom: -8px;
  left: 50%;
  margin-left: -10px;
}
.__react_component_tooltip.place-bottom::before {
  border-left: 10px solid transparent;
  border-right: 10px solid transparent;
  top: -8px;
  left: 50%;
  margin-left: -10px;
}
.__react_component_tooltip.place-left::before {
  border-top: 6px solid transparent;
  border-bottom: 6px solid transparent;
  right: -8px;
  top: 50%;
  margin-top: -5px;
}
.__react_component_tooltip.place-right::before {
  border-top: 6px solid transparent;
  border-bottom: 6px solid transparent;
  left: -8px;
  top: 50%;
  margin-top: -5px;
}
.__react_component_tooltip .multi-line {
  display: block;
  padding: 2px 0;
  text-align: center;
}`,defaultColors={dark:{text:"#fff",background:"#222",border:"transparent",arrow:"#222"},success:{text:"#fff",background:"#8DC572",border:"transparent",arrow:"#8DC572"},warning:{text:"#fff",background:"#F0AD4E",border:"transparent",arrow:"#F0AD4E"},error:{text:"#fff",background:"#BE6464",border:"transparent",arrow:"#BE6464"},info:{text:"#fff",background:"#337AB7",border:"transparent",arrow:"#337AB7"},light:{text:"#222",background:"#fff",border:"transparent",arrow:"#fff"}};function getDefaultPopupColors(e){return defaultColors[e]?_objectSpread2({},defaultColors[e]):void 0}function generateTooltipStyle(e,r,n,a){return generateStyle(e,getPopupColors(r,n,a))}function generateStyle(e,r){var n=r.text,a=r.background,l=r.border,c=r.arrow;return`
  	.`.concat(e,` {
	    color: `).concat(n,`;
	    background: `).concat(a,`;
	    border: 1px solid `).concat(l,`;
  	}

  	.`).concat(e,`.place-top {
        margin-top: -10px;
    }
    .`).concat(e,`.place-top::before {
        border-top: 8px solid `).concat(l,`;
    }
    .`).concat(e,`.place-top::after {
        border-left: 8px solid transparent;
        border-right: 8px solid transparent;
        bottom: -6px;
        left: 50%;
        margin-left: -8px;
        border-top-color: `).concat(c,`;
        border-top-style: solid;
        border-top-width: 6px;
    }

    .`).concat(e,`.place-bottom {
        margin-top: 10px;
    }
    .`).concat(e,`.place-bottom::before {
        border-bottom: 8px solid `).concat(l,`;
    }
    .`).concat(e,`.place-bottom::after {
        border-left: 8px solid transparent;
        border-right: 8px solid transparent;
        top: -6px;
        left: 50%;
        margin-left: -8px;
        border-bottom-color: `).concat(c,`;
        border-bottom-style: solid;
        border-bottom-width: 6px;
    }

    .`).concat(e,`.place-left {
        margin-left: -10px;
    }
    .`).concat(e,`.place-left::before {
        border-left: 8px solid `).concat(l,`;
    }
    .`).concat(e,`.place-left::after {
        border-top: 5px solid transparent;
        border-bottom: 5px solid transparent;
        right: -6px;
        top: 50%;
        margin-top: -4px;
        border-left-color: `).concat(c,`;
        border-left-style: solid;
        border-left-width: 6px;
    }

    .`).concat(e,`.place-right {
        margin-left: 10px;
    }
    .`).concat(e,`.place-right::before {
        border-right: 8px solid `).concat(l,`;
    }
    .`).concat(e,`.place-right::after {
        border-top: 5px solid transparent;
        border-bottom: 5px solid transparent;
        left: -6px;
        top: 50%;
        margin-top: -4px;
        border-right-color: `).concat(c,`;
        border-right-style: solid;
        border-right-width: 6px;
    }
  `)}function getPopupColors(e,r,n){var a=e.text,l=e.background,c=e.border,u=e.arrow?e.arrow:e.background,d=getDefaultPopupColors(r);return a&&(d.text=a),l&&(d.background=l),n&&(c?d.border=c:d.border=r==="light"?"black":"white"),u&&(d.arrow=u),d}var commonjsGlobal=typeof globalThis!="undefined"?globalThis:typeof window!="undefined"?window:typeof global!="undefined"?global:typeof self!="undefined"?self:{};function createCommonjsModule(e,r){return r={exports:{}},e(r,r.exports),r.exports}var check=function(e){return e&&e.Math==Math&&e},global_1=check(typeof globalThis=="object"&&globalThis)||check(typeof window=="object"&&window)||check(typeof self=="object"&&self)||check(typeof commonjsGlobal=="object"&&commonjsGlobal)||function(){return this}()||Function("return this")(),fails=function(e){try{return!!e()}catch{return!0}},descriptors=!fails(function(){return Object.defineProperty({},1,{get:function(){return 7}})[1]!=7}),$propertyIsEnumerable={}.propertyIsEnumerable,getOwnPropertyDescriptor=Object.getOwnPropertyDescriptor,NASHORN_BUG=getOwnPropertyDescriptor&&!$propertyIsEnumerable.call({1:2},1),f=NASHORN_BUG?function e(r){var n=getOwnPropertyDescriptor(this,r);return!!n&&n.enumerable}:$propertyIsEnumerable,objectPropertyIsEnumerable={f},createPropertyDescriptor=function(e,r){return{enumerable:!(e&1),configurable:!(e&2),writable:!(e&4),value:r}},toString={}.toString,classofRaw=function(e){return toString.call(e).slice(8,-1)},split="".split,indexedObject=fails(function(){return!Object("z").propertyIsEnumerable(0)})?function(e){return classofRaw(e)=="String"?split.call(e,""):Object(e)}:Object,requireObjectCoercible=function(e){if(e==null)throw TypeError("Can't call method on "+e);return e},toIndexedObject=function(e){return indexedObject(requireObjectCoercible(e))},isObject$1=function(e){return typeof e=="object"?e!==null:typeof e=="function"},toPrimitive=function(e,r){if(!isObject$1(e))return e;var n,a;if(r&&typeof(n=e.toString)=="function"&&!isObject$1(a=n.call(e))||typeof(n=e.valueOf)=="function"&&!isObject$1(a=n.call(e))||!r&&typeof(n=e.toString)=="function"&&!isObject$1(a=n.call(e)))return a;throw TypeError("Can't convert object to primitive value")},toObject=function(e){return Object(requireObjectCoercible(e))},hasOwnProperty={}.hasOwnProperty,has=function e(r,n){return hasOwnProperty.call(toObject(r),n)},document$1=global_1.document,EXISTS=isObject$1(document$1)&&isObject$1(document$1.createElement),documentCreateElement=function(e){return EXISTS?document$1.createElement(e):{}},ie8DomDefine=!descriptors&&!fails(function(){return Object.defineProperty(documentCreateElement("div"),"a",{get:function(){return 7}}).a!=7}),$getOwnPropertyDescriptor=Object.getOwnPropertyDescriptor,f$1=descriptors?$getOwnPropertyDescriptor:function e(r,n){if(r=toIndexedObject(r),n=toPrimitive(n,!0),ie8DomDefine)try{return $getOwnPropertyDescriptor(r,n)}catch{}if(has(r,n))return createPropertyDescriptor(!objectPropertyIsEnumerable.f.call(r,n),r[n])},objectGetOwnPropertyDescriptor={f:f$1},anObject=function(e){if(!isObject$1(e))throw TypeError(String(e)+" is not an object");return e},$defineProperty=Object.defineProperty,f$2=descriptors?$defineProperty:function e(r,n,a){if(anObject(r),n=toPrimitive(n,!0),anObject(a),ie8DomDefine)try{return $defineProperty(r,n,a)}catch{}if("get"in a||"set"in a)throw TypeError("Accessors not supported");return"value"in a&&(r[n]=a.value),r},objectDefineProperty={f:f$2},createNonEnumerableProperty=descriptors?function(e,r,n){return objectDefineProperty.f(e,r,createPropertyDescriptor(1,n))}:function(e,r,n){return e[r]=n,e},setGlobal=function(e,r){try{createNonEnumerableProperty(global_1,e,r)}catch{global_1[e]=r}return r},SHARED="__core-js_shared__",store=global_1[SHARED]||setGlobal(SHARED,{}),sharedStore=store,functionToString=Function.toString;typeof sharedStore.inspectSource!="function"&&(sharedStore.inspectSource=function(e){return functionToString.call(e)});var inspectSource=sharedStore.inspectSource,WeakMap=global_1.WeakMap,nativeWeakMap=typeof WeakMap=="function"&&/native code/.test(inspectSource(WeakMap)),shared=createCommonjsModule(function(e){(e.exports=function(r,n){return sharedStore[r]||(sharedStore[r]=n!==void 0?n:{})})("versions",[]).push({version:"3.12.1",mode:"global",copyright:"\xA9 2021 Denis Pushkarev (zloirock.ru)"})}),id=0,postfix=Math.random(),uid=function(e){return"Symbol("+String(e===void 0?"":e)+")_"+(++id+postfix).toString(36)},keys=shared("keys"),sharedKey=function(e){return keys[e]||(keys[e]=uid(e))},hiddenKeys={},OBJECT_ALREADY_INITIALIZED="Object already initialized",WeakMap$1=global_1.WeakMap,set,get,has$1,enforce=function(e){return has$1(e)?get(e):set(e,{})},getterFor=function(e){return function(r){var n;if(!isObject$1(r)||(n=get(r)).type!==e)throw TypeError("Incompatible receiver, "+e+" required");return n}};if(nativeWeakMap||sharedStore.state){var store$1=sharedStore.state||(sharedStore.state=new WeakMap$1),wmget=store$1.get,wmhas=store$1.has,wmset=store$1.set;set=function(e,r){if(wmhas.call(store$1,e))throw new TypeError(OBJECT_ALREADY_INITIALIZED);return r.facade=e,wmset.call(store$1,e,r),r},get=function(e){return wmget.call(store$1,e)||{}},has$1=function(e){return wmhas.call(store$1,e)}}else{var STATE=sharedKey("state");hiddenKeys[STATE]=!0,set=function(e,r){if(has(e,STATE))throw new TypeError(OBJECT_ALREADY_INITIALIZED);return r.facade=e,createNonEnumerableProperty(e,STATE,r),r},get=function(e){return has(e,STATE)?e[STATE]:{}},has$1=function(e){return has(e,STATE)}}var internalState={set,get,has:has$1,enforce,getterFor},redefine=createCommonjsModule(function(e){var r=internalState.get,n=internalState.enforce,a=String(String).split("String");(e.exports=function(l,c,u,d){var m=d?!!d.unsafe:!1,E=d?!!d.enumerable:!1,C=d?!!d.noTargetGet:!1,_;if(typeof u=="function"&&(typeof c=="string"&&!has(u,"name")&&createNonEnumerableProperty(u,"name",c),_=n(u),_.source||(_.source=a.join(typeof c=="string"?c:""))),l===global_1){E?l[c]=u:setGlobal(c,u);return}else m?!C&&l[c]&&(E=!0):delete l[c];E?l[c]=u:createNonEnumerableProperty(l,c,u)})(Function.prototype,"toString",function(){return typeof this=="function"&&r(this).source||inspectSource(this)})}),path=global_1,aFunction=function(e){return typeof e=="function"?e:void 0},getBuiltIn=function(e,r){return arguments.length<2?aFunction(path[e])||aFunction(global_1[e]):path[e]&&path[e][r]||global_1[e]&&global_1[e][r]},ceil=Math.ceil,floor=Math.floor,toInteger=function(e){return isNaN(e=+e)?0:(e>0?floor:ceil)(e)},min=Math.min,toLength=function(e){return e>0?min(toInteger(e),9007199254740991):0},max=Math.max,min$1=Math.min,toAbsoluteIndex=function(e,r){var n=toInteger(e);return n<0?max(n+r,0):min$1(n,r)},createMethod=function(e){return function(r,n,a){var l=toIndexedObject(r),c=toLength(l.length),u=toAbsoluteIndex(a,c),d;if(e&&n!=n){for(;c>u;)if(d=l[u++],d!=d)return!0}else for(;c>u;u++)if((e||u in l)&&l[u]===n)return e||u||0;return!e&&-1}},arrayIncludes={includes:createMethod(!0),indexOf:createMethod(!1)},indexOf=arrayIncludes.indexOf,objectKeysInternal=function(e,r){var n=toIndexedObject(e),a=0,l=[],c;for(c in n)!has(hiddenKeys,c)&&has(n,c)&&l.push(c);for(;r.length>a;)has(n,c=r[a++])&&(~indexOf(l,c)||l.push(c));return l},enumBugKeys=["constructor","hasOwnProperty","isPrototypeOf","propertyIsEnumerable","toLocaleString","toString","valueOf"],hiddenKeys$1=enumBugKeys.concat("length","prototype"),f$3=Object.getOwnPropertyNames||function e(r){return objectKeysInternal(r,hiddenKeys$1)},objectGetOwnPropertyNames={f:f$3},f$4=Object.getOwnPropertySymbols,objectGetOwnPropertySymbols={f:f$4},ownKeys$1$1=getBuiltIn("Reflect","ownKeys")||function e(r){var n=objectGetOwnPropertyNames.f(anObject(r)),a=objectGetOwnPropertySymbols.f;return a?n.concat(a(r)):n},copyConstructorProperties=function(e,r){for(var n=ownKeys$1$1(r),a=objectDefineProperty.f,l=objectGetOwnPropertyDescriptor.f,c=0;c<n.length;c++){var u=n[c];has(e,u)||a(e,u,l(r,u))}},replacement=/#|\.prototype\./,isForced=function(e,r){var n=data[normalize(e)];return n==POLYFILL?!0:n==NATIVE?!1:typeof r=="function"?fails(r):!!r},normalize=isForced.normalize=function(e){return String(e).replace(replacement,".").toLowerCase()},data=isForced.data={},NATIVE=isForced.NATIVE="N",POLYFILL=isForced.POLYFILL="P",isForced_1=isForced,getOwnPropertyDescriptor$1=objectGetOwnPropertyDescriptor.f,_export=function(e,r){var n=e.target,a=e.global,l=e.stat,c,u,d,m,E,C;if(a?u=global_1:l?u=global_1[n]||setGlobal(n,{}):u=(global_1[n]||{}).prototype,u)for(d in r){if(E=r[d],e.noTargetGet?(C=getOwnPropertyDescriptor$1(u,d),m=C&&C.value):m=u[d],c=isForced_1(a?d:n+(l?".":"#")+d,e.forced),!c&&m!==void 0){if(typeof E==typeof m)continue;copyConstructorProperties(E,m)}(e.sham||m&&m.sham)&&createNonEnumerableProperty(E,"sham",!0),redefine(u,d,E,e)}},aFunction$1=function(e){if(typeof e!="function")throw TypeError(String(e)+" is not a function");return e},functionBindContext=function(e,r,n){if(aFunction$1(e),r===void 0)return e;switch(n){case 0:return function(){return e.call(r)};case 1:return function(a){return e.call(r,a)};case 2:return function(a,l){return e.call(r,a,l)};case 3:return function(a,l,c){return e.call(r,a,l,c)}}return function(){return e.apply(r,arguments)}},isArray=Array.isArray||function e(r){return classofRaw(r)=="Array"},engineUserAgent=getBuiltIn("navigator","userAgent")||"",process=global_1.process,versions=process&&process.versions,v8=versions&&versions.v8,match,version;v8?(match=v8.split("."),version=match[0]<4?1:match[0]+match[1]):engineUserAgent&&(match=engineUserAgent.match(/Edge\/(\d+)/),(!match||match[1]>=74)&&(match=engineUserAgent.match(/Chrome\/(\d+)/),match&&(version=match[1])));var engineV8Version=version&&+version,nativeSymbol=!!Object.getOwnPropertySymbols&&!fails(function(){return!String(Symbol())||!Symbol.sham&&engineV8Version&&engineV8Version<41}),useSymbolAsUid=nativeSymbol&&!Symbol.sham&&typeof Symbol.iterator=="symbol",WellKnownSymbolsStore=shared("wks"),Symbol$1=global_1.Symbol,createWellKnownSymbol=useSymbolAsUid?Symbol$1:Symbol$1&&Symbol$1.withoutSetter||uid,wellKnownSymbol=function(e){return(!has(WellKnownSymbolsStore,e)||!(nativeSymbol||typeof WellKnownSymbolsStore[e]=="string"))&&(nativeSymbol&&has(Symbol$1,e)?WellKnownSymbolsStore[e]=Symbol$1[e]:WellKnownSymbolsStore[e]=createWellKnownSymbol("Symbol."+e)),WellKnownSymbolsStore[e]},SPECIES=wellKnownSymbol("species"),arraySpeciesCreate=function(e,r){var n;return isArray(e)&&(n=e.constructor,typeof n=="function"&&(n===Array||isArray(n.prototype))?n=void 0:isObject$1(n)&&(n=n[SPECIES],n===null&&(n=void 0))),new(n===void 0?Array:n)(r===0?0:r)},push=[].push,createMethod$1=function(e){var r=e==1,n=e==2,a=e==3,l=e==4,c=e==6,u=e==7,d=e==5||c;return function(m,E,C,_){for(var A=toObject(m),T=indexedObject(A),S=functionBindContext(E,C,3),P=toLength(T.length),k=0,x=_||arraySpeciesCreate,b=r?x(m,P):n||u?x(m,0):void 0,g,y;P>k;k++)if((d||k in T)&&(g=T[k],y=S(g,k,A),e))if(r)b[k]=y;else if(y)switch(e){case 3:return!0;case 5:return g;case 6:return k;case 2:push.call(b,g)}else switch(e){case 4:return!1;case 7:push.call(b,g)}return c?-1:a||l?l:b}},arrayIteration={forEach:createMethod$1(0),map:createMethod$1(1),filter:createMethod$1(2),some:createMethod$1(3),every:createMethod$1(4),find:createMethod$1(5),findIndex:createMethod$1(6),filterOut:createMethod$1(7)},objectKeys=Object.keys||function e(r){return objectKeysInternal(r,enumBugKeys)},objectDefineProperties=descriptors?Object.defineProperties:function e(r,n){anObject(r);for(var a=objectKeys(n),l=a.length,c=0,u;l>c;)objectDefineProperty.f(r,u=a[c++],n[u]);return r},html=getBuiltIn("document","documentElement"),GT=">",LT="<",PROTOTYPE="prototype",SCRIPT="script",IE_PROTO=sharedKey("IE_PROTO"),EmptyConstructor=function(){},scriptTag=function(e){return LT+SCRIPT+GT+e+LT+"/"+SCRIPT+GT},NullProtoObjectViaActiveX=function(e){e.write(scriptTag("")),e.close();var r=e.parentWindow.Object;return e=null,r},NullProtoObjectViaIFrame=function(){var e=documentCreateElement("iframe"),r="java"+SCRIPT+":",n;return e.style.display="none",html.appendChild(e),e.src=String(r),n=e.contentWindow.document,n.open(),n.write(scriptTag("document.F=Object")),n.close(),n.F},activeXDocument,NullProtoObject=function(){try{activeXDocument=document.domain&&new ActiveXObject("htmlfile")}catch{}NullProtoObject=activeXDocument?NullProtoObjectViaActiveX(activeXDocument):NullProtoObjectViaIFrame();for(var e=enumBugKeys.length;e--;)delete NullProtoObject[PROTOTYPE][enumBugKeys[e]];return NullProtoObject()};hiddenKeys[IE_PROTO]=!0;var objectCreate=Object.create||function e(r,n){var a;return r!==null?(EmptyConstructor[PROTOTYPE]=anObject(r),a=new EmptyConstructor,EmptyConstructor[PROTOTYPE]=null,a[IE_PROTO]=r):a=NullProtoObject(),n===void 0?a:objectDefineProperties(a,n)},UNSCOPABLES=wellKnownSymbol("unscopables"),ArrayPrototype=Array.prototype;ArrayPrototype[UNSCOPABLES]==null&&objectDefineProperty.f(ArrayPrototype,UNSCOPABLES,{configurable:!0,value:objectCreate(null)});var addToUnscopables=function(e){ArrayPrototype[UNSCOPABLES][e]=!0},$find=arrayIteration.find,FIND="find",SKIPS_HOLES=!0;FIND in[]&&Array(1)[FIND](function(){SKIPS_HOLES=!1});_export({target:"Array",proto:!0,forced:SKIPS_HOLES},{find:function e(r){return $find(this,r,arguments.length>1?arguments[1]:void 0)}});addToUnscopables(FIND);var _class,_class2,_temp,ReactTooltip=staticMethods(_class=windowListener(_class=customEvent(_class=isCapture(_class=getEffect(_class=bodyMode(_class=trackRemoval(_class=(_temp=_class2=function(e){_inherits(r,e),_createClass(r,null,[{key:"propTypes",get:function(){return{uuid:PropTypes.string,children:PropTypes.any,place:PropTypes.string,type:PropTypes.string,effect:PropTypes.string,offset:PropTypes.object,multiline:PropTypes.bool,border:PropTypes.bool,textColor:PropTypes.string,backgroundColor:PropTypes.string,borderColor:PropTypes.string,arrowColor:PropTypes.string,insecure:PropTypes.bool,class:PropTypes.string,className:PropTypes.string,id:PropTypes.string,html:PropTypes.bool,delayHide:PropTypes.number,delayUpdate:PropTypes.number,delayShow:PropTypes.number,event:PropTypes.string,eventOff:PropTypes.string,isCapture:PropTypes.bool,globalEventOff:PropTypes.string,getContent:PropTypes.any,afterShow:PropTypes.func,afterHide:PropTypes.func,overridePosition:PropTypes.func,disable:PropTypes.bool,scrollHide:PropTypes.bool,resizeHide:PropTypes.bool,wrapper:PropTypes.string,bodyMode:PropTypes.bool,possibleCustomEvents:PropTypes.string,possibleCustomEventsOff:PropTypes.string,clickable:PropTypes.bool}}}]);function r(n){var a;return _classCallCheck(this,r),a=_possibleConstructorReturn(this,_getPrototypeOf(r).call(this,n)),a.state={uuid:n.uuid||generateUUID(),place:n.place||"top",desiredPlace:n.place||"top",type:"dark",effect:"float",show:!1,border:!1,customColors:{},offset:{},extraClass:"",html:!1,delayHide:0,delayShow:0,event:n.event||null,eventOff:n.eventOff||null,currentEvent:null,currentTarget:null,ariaProps:parseAria(n),isEmptyTip:!1,disable:!1,possibleCustomEvents:n.possibleCustomEvents||"",possibleCustomEventsOff:n.possibleCustomEventsOff||"",originTooltip:null,isMultiline:!1},a.bind(["showTooltip","updateTooltip","hideTooltip","hideTooltipOnScroll","getTooltipContent","globalRebuild","globalShow","globalHide","onWindowResize","mouseOnToolTip"]),a.mount=!0,a.delayShowLoop=null,a.delayHideLoop=null,a.delayReshow=null,a.intervalUpdateContent=null,a}return _createClass(r,[{key:"bind",value:function(a){var l=this;a.forEach(function(c){l[c]=l[c].bind(l)})}},{key:"componentDidMount",value:function(){var a=this.props;a.insecure;var l=a.resizeHide;this.bindListener(),this.bindWindowEvents(l),this.injectStyles()}},{key:"componentWillUnmount",value:function(){this.mount=!1,this.clearTimer(),this.unbindListener(),this.removeScrollListener(this.state.currentTarget),this.unbindWindowEvents()}},{key:"injectStyles",value:function(){var a=this.tooltipRef;if(!!a){for(var l=a.parentNode;l.parentNode;)l=l.parentNode;var c;switch(l.constructor.name){case"Document":case"HTMLDocument":case void 0:c=l.head;break;case"ShadowRoot":default:c=l;break}if(!c.querySelector("style[data-react-tooltip]")){var u=document.createElement("style");u.textContent=baseCss,u.setAttribute("data-react-tooltip","true"),c.appendChild(u)}}}},{key:"mouseOnToolTip",value:function(){var a=this.state.show;return a&&this.tooltipRef?(this.tooltipRef.matches||(this.tooltipRef.msMatchesSelector?this.tooltipRef.matches=this.tooltipRef.msMatchesSelector:this.tooltipRef.matches=this.tooltipRef.mozMatchesSelector),this.tooltipRef.matches(":hover")):!1}},{key:"getTargetArray",value:function(a){var l=[],c;if(!a)c="[data-tip]:not([data-for])";else{var u=a.replace(/\\/g,"\\\\").replace(/"/g,'\\"');c='[data-tip][data-for="'.concat(u,'"]')}return nodeListToArray(document.getElementsByTagName("*")).filter(function(d){return d.shadowRoot}).forEach(function(d){l=l.concat(nodeListToArray(d.shadowRoot.querySelectorAll(c)))}),l.concat(nodeListToArray(document.querySelectorAll(c)))}},{key:"bindListener",value:function(){var a=this,l=this.props,c=l.id,u=l.globalEventOff,d=l.isCapture,m=this.getTargetArray(c);m.forEach(function(E){E.getAttribute("currentItem")===null&&E.setAttribute("currentItem","false"),a.unbindBasicListener(E),a.isCustomEvent(E)&&a.customUnbindListener(E)}),this.isBodyMode()?this.bindBodyListener(m):m.forEach(function(E){var C=a.isCapture(E),_=a.getEffect(E);if(a.isCustomEvent(E)){a.customBindListener(E);return}E.addEventListener("mouseenter",a.showTooltip,C),E.addEventListener("focus",a.showTooltip,C),_==="float"&&E.addEventListener("mousemove",a.updateTooltip,C),E.addEventListener("mouseleave",a.hideTooltip,C),E.addEventListener("blur",a.hideTooltip,C)}),u&&(window.removeEventListener(u,this.hideTooltip),window.addEventListener(u,this.hideTooltip,d)),this.bindRemovalTracker()}},{key:"unbindListener",value:function(){var a=this,l=this.props,c=l.id,u=l.globalEventOff;if(this.isBodyMode())this.unbindBodyListener();else{var d=this.getTargetArray(c);d.forEach(function(m){a.unbindBasicListener(m),a.isCustomEvent(m)&&a.customUnbindListener(m)})}u&&window.removeEventListener(u,this.hideTooltip),this.unbindRemovalTracker()}},{key:"unbindBasicListener",value:function(a){var l=this.isCapture(a);a.removeEventListener("mouseenter",this.showTooltip,l),a.removeEventListener("mousemove",this.updateTooltip,l),a.removeEventListener("mouseleave",this.hideTooltip,l)}},{key:"getTooltipContent",value:function(){var a=this.props,l=a.getContent,c=a.children,u;return l&&(Array.isArray(l)?u=l[0]&&l[0](this.state.originTooltip):u=l(this.state.originTooltip)),getTipContent(this.state.originTooltip,c,u,this.state.isMultiline)}},{key:"isEmptyTip",value:function(a){return typeof a=="string"&&a===""||a===null}},{key:"showTooltip",value:function(a,l){if(!!this.tooltipRef){if(l){var c=this.getTargetArray(this.props.id),u=c.some(function(j){return j===a.currentTarget});if(!u)return}var d=this.props,m=d.multiline,E=d.getContent,C=a.currentTarget.getAttribute("data-tip"),_=a.currentTarget.getAttribute("data-multiline")||m||!1,A=a instanceof window.FocusEvent||l,T=!0;a.currentTarget.getAttribute("data-scroll-hide")?T=a.currentTarget.getAttribute("data-scroll-hide")==="true":this.props.scrollHide!=null&&(T=this.props.scrollHide),a&&a.currentTarget&&a.currentTarget.setAttribute&&a.currentTarget.setAttribute("aria-describedby",this.state.uuid);var S=a.currentTarget.getAttribute("data-place")||this.props.place||"top",P=A&&"solid"||this.getEffect(a.currentTarget),k=a.currentTarget.getAttribute("data-offset")||this.props.offset||{},x=getPosition(a,a.currentTarget,this.tooltipRef,S,S,P,k);x.position&&this.props.overridePosition&&(x.position=this.props.overridePosition(x.position,a,a.currentTarget,this.tooltipRef,S,S,P,k));var b=x.isNewState?x.newState.place:S;this.clearTimer();var g=a.currentTarget,y=this.state.show?g.getAttribute("data-delay-update")||this.props.delayUpdate:0,I=this,M=function(){I.setState({originTooltip:C,isMultiline:_,desiredPlace:S,place:b,type:g.getAttribute("data-type")||I.props.type||"dark",customColors:{text:g.getAttribute("data-text-color")||I.props.textColor||null,background:g.getAttribute("data-background-color")||I.props.backgroundColor||null,border:g.getAttribute("data-border-color")||I.props.borderColor||null,arrow:g.getAttribute("data-arrow-color")||I.props.arrowColor||null},effect:P,offset:k,html:(g.getAttribute("data-html")?g.getAttribute("data-html")==="true":I.props.html)||!1,delayShow:g.getAttribute("data-delay-show")||I.props.delayShow||0,delayHide:g.getAttribute("data-delay-hide")||I.props.delayHide||0,delayUpdate:g.getAttribute("data-delay-update")||I.props.delayUpdate||0,border:(g.getAttribute("data-border")?g.getAttribute("data-border")==="true":I.props.border)||!1,extraClass:g.getAttribute("data-class")||I.props.class||I.props.className||"",disable:(g.getAttribute("data-tip-disable")?g.getAttribute("data-tip-disable")==="true":I.props.disable)||!1,currentTarget:g},function(){T&&I.addScrollListener(I.state.currentTarget),I.updateTooltip(a),E&&Array.isArray(E)&&(I.intervalUpdateContent=setInterval(function(){if(I.mount){var O=I.props.getContent,L=getTipContent(C,"",O[0](),_),N=I.isEmptyTip(L);I.setState({isEmptyTip:N}),I.updatePosition()}},E[1]))})};y?this.delayReshow=setTimeout(M,y):M()}}},{key:"updateTooltip",value:function(a){var l=this,c=this.state,u=c.delayShow,d=c.disable,m=this.props.afterShow,E=this.getTooltipContent(),C=a.currentTarget||a.target;if(!this.mouseOnToolTip()&&!(this.isEmptyTip(E)||d)){var _=this.state.show?0:parseInt(u,10),A=function(){if(Array.isArray(E)&&E.length>0||E){var S=!l.state.show;l.setState({currentEvent:a,currentTarget:C,show:!0},function(){l.updatePosition(),S&&m&&m(a)})}};clearTimeout(this.delayShowLoop),_?this.delayShowLoop=setTimeout(A,_):A()}}},{key:"listenForTooltipExit",value:function(){var a=this.state.show;a&&this.tooltipRef&&this.tooltipRef.addEventListener("mouseleave",this.hideTooltip)}},{key:"removeListenerForTooltipExit",value:function(){var a=this.state.show;a&&this.tooltipRef&&this.tooltipRef.removeEventListener("mouseleave",this.hideTooltip)}},{key:"hideTooltip",value:function(a,l){var c=this,u=arguments.length>2&&arguments[2]!==void 0?arguments[2]:{isScroll:!1},d=this.state.disable,m=u.isScroll,E=m?0:this.state.delayHide,C=this.props.afterHide,_=this.getTooltipContent();if(!!this.mount&&!(this.isEmptyTip(_)||d)){if(l){var A=this.getTargetArray(this.props.id),T=A.some(function(P){return P===a.currentTarget});if(!T||!this.state.show)return}a&&a.currentTarget&&a.currentTarget.removeAttribute&&a.currentTarget.removeAttribute("aria-describedby");var S=function(){var k=c.state.show;if(c.mouseOnToolTip()){c.listenForTooltipExit();return}c.removeListenerForTooltipExit(),c.setState({show:!1},function(){c.removeScrollListener(c.state.currentTarget),k&&C&&C(a)})};this.clearTimer(),E?this.delayHideLoop=setTimeout(S,parseInt(E,10)):S()}}},{key:"hideTooltipOnScroll",value:function(a,l){this.hideTooltip(a,l,{isScroll:!0})}},{key:"addScrollListener",value:function(a){var l=this.isCapture(a);window.addEventListener("scroll",this.hideTooltipOnScroll,l)}},{key:"removeScrollListener",value:function(a){var l=this.isCapture(a);window.removeEventListener("scroll",this.hideTooltipOnScroll,l)}},{key:"updatePosition",value:function(){var a=this,l=this.state,c=l.currentEvent,u=l.currentTarget,d=l.place,m=l.desiredPlace,E=l.effect,C=l.offset,_=this.tooltipRef,A=getPosition(c,u,_,d,m,E,C);if(A.position&&this.props.overridePosition&&(A.position=this.props.overridePosition(A.position,c,u,_,d,m,E,C)),A.isNewState)return this.setState(A.newState,function(){a.updatePosition()});_.style.left=A.position.left+"px",_.style.top=A.position.top+"px"}},{key:"clearTimer",value:function(){clearTimeout(this.delayShowLoop),clearTimeout(this.delayHideLoop),clearTimeout(this.delayReshow),clearInterval(this.intervalUpdateContent)}},{key:"hasCustomColors",value:function(){var a=this;return Boolean(Object.keys(this.state.customColors).find(function(l){return l!=="border"&&a.state.customColors[l]})||this.state.border&&this.state.customColors.border)}},{key:"render",value:function(){var a=this,l=this.state,c=l.extraClass,u=l.html,d=l.ariaProps,m=l.disable,E=l.uuid,C=this.getTooltipContent(),_=this.isEmptyTip(C),A=generateTooltipStyle(this.state.uuid,this.state.customColors,this.state.type,this.state.border),T="__react_component_tooltip"+" ".concat(this.state.uuid)+(this.state.show&&!m&&!_?" show":"")+(this.state.border?" border":"")+" place-".concat(this.state.place)+" type-".concat(this.hasCustomColors()?"custom":this.state.type)+(this.props.delayUpdate?" allow_hover":"")+(this.props.clickable?" allow_click":""),S=this.props.wrapper;r.supportedWrappers.indexOf(S)<0&&(S=r.defaultProps.wrapper);var P=[T,c].filter(Boolean).join(" ");if(u){var k="".concat(C,`
<style aria-hidden="true">`).concat(A,"</style>");return t.createElement(S,_extends$1({className:"".concat(P),id:this.props.id||E,ref:function(b){return a.tooltipRef=b}},d,{"data-id":"tooltip",dangerouslySetInnerHTML:{__html:k}}))}else return t.createElement(S,_extends$1({className:"".concat(P),id:this.props.id||E},d,{ref:function(b){return a.tooltipRef=b},"data-id":"tooltip"}),t.createElement("style",{dangerouslySetInnerHTML:{__html:A},"aria-hidden":"true"}),C)}}],[{key:"getDerivedStateFromProps",value:function(a,l){var c=l.ariaProps,u=parseAria(a),d=Object.keys(u).some(function(m){return u[m]!==c[m]});return d?_objectSpread2({},l,{ariaProps:u}):null}}]),r}(t.Component),_defineProperty$2(_class2,"defaultProps",{insecure:!0,resizeHide:!0,wrapper:"div",clickable:!1}),_defineProperty$2(_class2,"supportedWrappers",["div","span"]),_defineProperty$2(_class2,"displayName","ReactTooltip"),_temp))||_class)||_class)||_class)||_class)||_class)||_class)||_class;const UserCheckbox=e=>{const{isMultiple:r}=e,{getInputProps:n,getCheckboxProps:a}=r?useCheckbox(e):useRadio(e),l=n(),c=a();return jsxs(Box,{as:"label",rounded:"md",d:"block",bg:useColorModeValue("#1e293b","#1e293b50"),children:[jsx("input",H(B({},l),{style:{display:"none"}})),jsxs(Box,H(B({},c),{cursor:"pointer",opacity:.8,pos:"relative",children:[l.checked&&jsx(CheckCircleIcon,{color:"green.500",fontSize:"xl",pos:"absolute",top:2,right:2,zIndex:2}),e.children]}))]})},roleConfigs=[{key:"",value:"All"},{key:"zimians",value:"Zimians"},{key:"student",value:"Student"}];function useDebounce$1(e,r){const[n,a]=react.exports.useState(e);return react.exports.useEffect(()=>{const l=setTimeout(()=>{a(e)},r);return()=>{clearTimeout(l)}},[e,r]),n}const isReachBottom$2=e=>e.scrollHeight-e.scrollTop<=e.clientHeight+10,ListUser=({getCheckboxProps:e,listParticipants:r,usersInfo:n,isAddSupporter:a,value:l,setValue:c})=>{var O,L,N,q;const[u]=useUser(),[d,m]=react.exports.useState(""),[E,C]=react.exports.useState(!1),[_,A]=react.exports.useState(""),T=useDebounce$1(d,500),S=V=>{C(isReachBottom$2(V.target))},P=pickBy_1({roles:a?["EC"]:_==="all"?"":_==="zimians"?zimRoles:_==="student"?["HV"]:"",searchString:T,limit:20},V=>V),{data:k,loading:x,fetchMore:b}=useQuery(GET_LIST_ACCOUNT_CHAT,{variables:P}),g=(L=(O=k==null?void 0:k.getAccountChatPagination)==null?void 0:O.docs)==null?void 0:L.filter(V=>{var Q,W,G,R;return!(r==null?void 0:r.includes((W=(Q=V==null?void 0:V.user)==null?void 0:Q.id)==null?void 0:W.toString()))&&((R=(G=V==null?void 0:V.user)==null?void 0:G.id)==null?void 0:R.toString())!==(u==null?void 0:u.id)}),y=(N=k==null?void 0:k.getAccountChatPagination)==null?void 0:N.page,I=(q=k==null?void 0:k.getAccountChatPagination)==null?void 0:q.hasNextPage,{onLoadMore:M,isLoadingMore:j}=useLoadMore({variables:H(B({},P),{page:y+1}),fetchMore:b,hasNextPage:I,isNormalLoadMore:!1});return react.exports.useEffect(()=>{E&&I&&!j&&M()},[E,I,j]),jsxs(Stack,{spacing:3,children:[jsx(Text,{fontSize:14,children:"Danh s\xE1ch th\xE0nh vi\xEAn"}),jsx(HStack,{children:roleConfigs.map((V,Q)=>{const W=V.key===_;return jsx(Button$1,{onClick:()=>A(V.key),colorScheme:"teal",variant:W?"solid":"outline",disabled:a&&Q!==1,children:V.value},Q)})}),jsx(Wrap,{my:2,children:n==null?void 0:n.map((V,Q)=>jsxs(Tag,{size:"md",rounded:"md",children:[jsx(TagLabel,{children:V.full_name}),jsx(TagCloseButton,{onClick:()=>c(l==null?void 0:l.filter(W=>{var G;return W!==((G=V==null?void 0:V.id)==null?void 0:G.toString())}))})]},Q))}),jsx(TextInput,{type:"text",placeholder:"Nh\u1EADp t\xEAn ng\u01B0\u1EDDi d\xF9ng",size:"md",value:d,onChange:V=>m(V.target.value)}),jsxs(Box,{position:"relative",h:"250px",overflow:"auto",w:"full",flexGrow:1,onScroll:S,children:[jsxs(SimpleGrid,{columns:2,spacing:2,children:[(g==null?void 0:g.length)>0&&(g==null?void 0:g.map(({user:V})=>{const Q=e({value:V.id});return jsx(Box,{rounded:"lg",overflow:"hidden",children:jsx(UserCheckbox,H(B({isMultiple:!0},Q),{children:jsx(UserItemInfo,H(B({},V),{username:V==null?void 0:V.full_name,positionName:V==null?void 0:V.role_name,lastOnline:V==null?void 0:V.status_online,px:3,py:2}))}))},`${V.id}`)})),x&&jsxs(HStack,{rounded:"lg",bg:"blackAlpha.300",opacity:.5,pos:"absolute",top:0,bottom:0,right:0,left:0,justifyContent:"center",children:[jsx(Spinner,{color:"#319795",size:"lg"}),jsx(Text,{fontSize:"xl",color:"white",children:"\u0110ang t\u1EA3i ..."})]})]}),j&&jsxs(HStack,{w:"full",alignItems:"center",justifyContent:"center",py:2,children:[jsx(Spinner,{color:"#319795",size:"lg"}),jsx(Text,{fontSize:"xl",color:useColorModeValue("gray.700","white"),children:"\u0110ang t\u1EA3i ..."})]})]})]})},resetQueries=(e,r=null)=>{e.forEach(n=>{r==null||r.evict({id:"ROOT_QUERY",fieldName:n})})};function useResetQueries(){const e=useApolloClient();return r=>{resetQueries(r,e.cache)}}const useCreateOrUpdateConversation=({onClose:e,userId:r,isEdit:n,groupId:a,setUserId:l,isAddSupporter:c,isChannel:u})=>{const[d]=useUser(),m=useToast(),E=useResetQueries(),C=useStore(k=>k.setConversationId),[_,{loading:A}]=useMutation(CREATE_CONVERSATION,{onCompleted:k=>{const{createConversation:x}=k;x&&(m({title:"Th\xE0nh c\xF4ng!",description:`T\u1EA1o ${u?"channel":"nh\xF3m chat"} ${x==null?void 0:x.name} th\xE0nh c\xF4ng`,status:"success",duration:3e3,isClosable:!0}),e(),C(x==null?void 0:x.id)),l([]),E(["conversations","conversationDetail"])},onError:k=>{var x;m({title:`T\u1EA1o ${u?"channel":"nh\xF3m chat"} kh\xF4ng th\xE0nh c\xF4ng!`,description:`${(x=k==null?void 0:k.message)!=null?x:"L\u1ED7i"}`,status:"error",duration:3e3,isClosable:!0})}}),[T,{loading:S}]=useMutation(UPDATE_CONVERSATION,{onCompleted:k=>{const{updateConversation:x}=k;x&&m({title:"Th\xE0nh c\xF4ng!",description:c?"Th\xEAm EC h\u1ED7 tr\u1EE3 th\xE0nh c\xF4ng":`C\u1EADp nh\u1EADt ${u?"channel":"nh\xF3m chat"} ${x==null?void 0:x.name} th\xE0nh c\xF4ng`,status:"success",duration:3e3,isClosable:!0}),e(),l([]),E(["conversations","conversationDetail"])},onError:k=>{var x;m({title:`C\u1EADp nh\u1EADt ${u?"channel":"nh\xF3m chat"} kh\xF4ng th\xE0nh c\xF4ng!`,description:`${(x=k==null?void 0:k.message)!=null?x:"L\u1ED7i"}`,status:"error",duration:3e3,isClosable:!0})}});return{handleSubmitForm:react.exports.useCallback(async k=>{try{n?await T({variables:pickBy_1({name:k==null?void 0:k.name,id:a,userIds:r==null?void 0:r.filter(x=>x!==(d==null?void 0:d.id))})}):await _({variables:pickBy_1({isChannel:u,name:k==null?void 0:k.name,group:a,userIds:r})})}catch(x){console.log(x)}},[r,a,n,d,u]),loading:A||S}},getUserInfo=e=>typeof e=="object"?Promise.resolve(e):apolloClient.query({query:GRAPH_NET_GET_USER_INFO,variables:{userId:parseInt(e,10)}}).then(({data:r})=>r.getUserById),getUsersInfo=(e,r)=>lodash.exports.isEmpty(e||r)?[]:Promise.allSettled(e==null?void 0:e.map(getUserInfo)).then(n=>n.filter(a=>a.status==="fulfilled").map(a=>a==null?void 0:a.value)),CreateConversationModal=({onClose:e,isOpen:r,groupId:n="",listParticipants:a,name:l,isEdit:c,isAddSupporter:u,isChannel:d})=>{const[m,E]=react.exports.useState([]),C=react.exports.useRef(),[_,A]=react.exports.useState([]),[T,S]=react.exports.useState(n),{getCheckboxProps:P,value:k,setValue:x}=useCheckboxGroup({onChange:A}),{handleSubmitForm:b,loading:g}=useCreateOrUpdateConversation({onClose:e,userId:[...a!=null?a:[],..._!=null?_:[]],isEdit:c,groupId:T,setUserId:A,isAddSupporter:u,isChannel:d}),y=useForm({mode:"onBlur",reValidateMode:"onChange"}),{register:I,formState:{errors:M}}=y,j=react.exports.useRef(null),O=y.watch("name"),L=!lodash.exports.isEqual(O,j.current)||(_==null?void 0:_.length)>0;return useDeepCompareEffect$2(()=>{c&&!u&&(y.reset({name:l}),j.current=l)},[r,c,j,y,l]),react.exports.useEffect(()=>{(async function(){try{const N=await getUsersInfo(_,!r);E(N)}catch(N){console.log(N)}})()},[_]),react.exports.useEffect(()=>{S(n)},[n]),jsx(FormProvider,H(B({},y),{children:jsxs(Modal$1,{finalFocusRef:C,isOpen:r,onClose:()=>{e(),x([])},size:"2xl",children:[jsx(ModalOverlay,{}),jsxs(ModalContent,{bg:useColorModeValue(void 0,"slate.900"),children:[jsx(ModalHeader,{children:u?"Th\xEAm EC h\u1ED7 tr\u1EE3":c?"C\u1EADp nh\u1EADt nh\xF3m chat":d?"T\u1EA1o channel":"T\u1EA1o nh\xF3m chat"}),jsx(ModalCloseButton,{}),jsxs("form",{onSubmit:y.handleSubmit(b),children:[jsx(ModalBody,{children:jsxs(Stack,{spacing:6,children:[!u&&jsx(TextInput,H(B({label:d?"T\xEAn channel":"T\xEAn nh\xF3m",placeholder:"Nh\u1EADp t\xEAn nh\xF3m"},I("name",{required:!0})),{error:M==null?void 0:M.name})),jsx(ListUser,{value:k,setValue:x,isAddSupporter:u,usersInfo:m,listParticipants:a,getCheckboxProps:P})]})}),jsxs(ModalFooter,{children:[jsx(Button$1,{disabled:!c&&lodash.exports.isEmpty(_)||g||c&&!L,leftIcon:jsx(Icon,{as:AiOutlineSend}),bg:"#319795",mr:4,type:"submit",isLoading:!1,color:"white",children:c?"C\u1EADp nh\u1EADt":"T\u1EA1o nh\xF3m"}),jsx(Button$1,{disabled:g,variant:"outline",onClick:()=>{e(),x([]),A([])},children:"H\u1EE7y"})]})]})]})]})}))},tabConfig=[{key:"open",value:"Open"},{key:"following",value:"Following"},{key:"done",value:"Done"}],DataTabs=({data:e,isSelected:r,setParticipants:n,setUserId:a,loading:l,setLocalTab:c})=>{var x;const{isOpen:u,onOpen:d,onClose:m}=useDisclosure(),E=useStore(b=>b.setTab),C=useStore(b=>b.channel),_=useStore(b=>b.conversationId),A=useStore(b=>b.setConversationId),[T]=useUser(),[S,P]=useSearchParams({}),k=react.exports.useCallback(b=>{_===b.id&&E("message"),A(b.id),n(b==null?void 0:b.participants),a(""),E("message"),P({qsConversationId:b.id}),S.delete("qsUserId"),S.delete("qsGroup")},[_,S]);return jsxs(Tabs,{onChange:b=>{var g;return c((g=tabConfig[b])==null?void 0:g.key)},defaultIndex:tabConfig.findIndex(b=>b.key===r),children:[jsxs(Flex,{flexDirection:{base:"row",md:"column"},position:"sticky",top:0,zIndex:20,bg:useColorModeValue("white","#10172a"),alignItems:"center",justifyContent:"center",children:[jsxs(HStack,{w:{base:["my chats","guest"].includes(C)?"25%":"100%",md:"full"},alignItems:"center",justify:"space-between",spacing:4,px:2,borderBottomWidth:1,minH:"50px",children:[jsx(Text,{fontSize:13,fontWeight:"bold",textColor:useColorModeValue("#444","slate.300"),children:(x=C==null?void 0:C.toString())==null?void 0:x.toLocaleUpperCase()}),!["my chats","guest"].includes(C)&&zimRolesNumber.map(b=>b.id).includes(T==null?void 0:T.roleId)&&jsxs(Box,{onClick:d,"data-tip":"React-tooltip",display:"flex",alignItems:"center",justifyContent:"center",cursor:"pointer",children:[jsx(MdOutlineGroupAdd,{fontSize:20}),jsx(ReactTooltip,{children:jsx("p",{className:"text-base font-medium",children:"T\u1EA1o group chat"}),place:"bottom",backgroundColor:"green",effect:"float"})]})]}),C==="guest"&&jsx(HStack,{spacing:0,w:"full",minH:"50px",children:tabConfig.map((b,g)=>jsx(Tab,{minH:"50px",flex:1,_dark:{color:"slate.300"},w:"full",fontSize:{sm:14},px:0,borderBottomColor:useColorModeValue("gray.200","whiteAlpha.300"),mb:0,children:b.value},b.key))})]}),jsx(Stack,{spacing:0,children:(e==null?void 0:e.length)>0?e.map((b,g)=>{var V,Q,W,G,R,D,F,$,U,z,Y,Z;const y=b==null?void 0:b.participants,I=!(y==null?void 0:y.find(K=>K.userId===(T==null?void 0:T.id)))&&(y==null?void 0:y.length)>=2||(y==null?void 0:y.find(K=>K.userId===(T==null?void 0:T.id)))&&(y==null?void 0:y.length)>=3,M=(b==null?void 0:b.name)||I?y:y==null?void 0:y.filter(K=>K.userId!==(T==null?void 0:T.id))[0],j=(M==null?void 0:M.isGuest)?M==null?void 0:M.fullName:(b==null?void 0:b.name)?b==null?void 0:b.name:I?y==null?void 0:y.map(K=>K.fullName).join(","):M==null?void 0:M.fullName,O=((Q=(V=b==null?void 0:b.usersTyping)==null?void 0:V.filter(K=>K!==(T==null?void 0:T.id)))==null?void 0:Q.length)>0,L=(b==null?void 0:b.lastMessage)?(G=(W=b==null?void 0:b.lastMessage)==null?void 0:W.seenBy)==null?void 0:G.includes(T.id):!0,N=timeDifferenceCalculator(dayjs(b==null?void 0:b.updatedAt).toDate()),q=((D=(R=b==null?void 0:b.lastMessage)==null?void 0:R.attachments)==null?void 0:D.length)>0?["[T\u1EC7p \u0111\xEDnh k\xE8m]"]:(F=b==null?void 0:b.lastMessage)==null?void 0:F.text;return l?jsx(ConversationLoading,{},g):react.exports.createElement(ConversationItem,H(B({},M),{isTyping:O,isActive:b.id===_,key:b.id,onClick:()=>{k(b)},username:j,lastMessage:q,updatedAt:N,seen:L,seenByCount:(z=(U=($=b==null?void 0:b.lastMessage)==null?void 0:$.seenBy)==null?void 0:U.length)!=null?z:0,avatarUrl:M==null?void 0:M.avatar,groupInfo:(b==null?void 0:b.name)||I?M:null,userTypingName:(Z=(Y=b==null?void 0:b.usersTyping)==null?void 0:Y.filter(K=>K!==T.id))==null?void 0:Z.map(K=>{var ue;return(ue=y.find(ie=>ie.userId===K))==null?void 0:ue.fullName})}))}):jsx(Text,{textAlign:"center",mt:4,children:`Hi\u1EC7n t\u1EA1i kh\xF4ng c\xF3 ${C==="guest"?"y\xEAu c\u1EA7u h\u1ED7 tr\u1EE3":"cu\u1ED9c tr\xF2 chuy\u1EC7n"} n\xE0o !!!`})}),jsx(CreateConversationModal,{isChannel:!1,isAddSupporter:!1,isEdit:!1,name:"",listParticipants:[],isOpen:u,onClose:m,groupId:C})]})},useGetSupportConversations=({status:e="",search:r,channel:n})=>{var S,P,k;const a=H(B({},pickBy_1({status:e,search:r},x=>x)),{limit:20,offset:0}),[l,{data:c,loading:u,fetchMore:d,called:m}]=useLazyQuery(SUPPORT_CONVERSATIONS,{variables:a,fetchPolicy:"network-only",nextFetchPolicy:"cache-first"});react.exports.useEffect(()=>{n&&n==="guest"&&!m&&(async()=>await l())()},[n,m]);const E=((S=c==null?void 0:c.supportConversations)==null?void 0:S.docs)||[],C=(P=c==null?void 0:c.supportConversations)==null?void 0:P.hasNextPage,{onLoadMore:_,isLoadingMore:A}=useLoadMore({variables:H(B({},a),{offset:(E==null?void 0:E.length)+1}),fetchMore:d,hasNextPage:C});return{listSupportConversations:((k=c==null?void 0:c.supportConversations)==null?void 0:k.docs)||[],loading:u,onLoadMore:_,hasNextPage:C,isLoadingMore:A}},useGetConversations=({search:e,channel:r})=>{var A,T;const n=H(B({},pickBy_1({group:r==="my chats"?"":r,search:e},S=>S)),{limit:20,offset:0}),[a,{data:l,loading:c,fetchMore:u,called:d}]=useLazyQuery(CONVERSATIONS,{variables:n,fetchPolicy:"network-only",nextFetchPolicy:"cache-first"});react.exports.useEffect(()=>{r&&r!=="guest"&&!d&&(async()=>await a())()},[r,d,a]);const m=((A=l==null?void 0:l.conversations)==null?void 0:A.docs)||[],E=(T=l==null?void 0:l.conversations)==null?void 0:T.hasNextPage,{onLoadMore:C,isLoadingMore:_}=useLoadMore({variables:H(B({},n),{offset:(m==null?void 0:m.length)+1}),fetchMore:u,hasNextPage:E});return{listConversations:m,loading:c,onLoadMore:C,hasNextPage:E,isLoadingMore:_}},isReachBottom$1=e=>e.scrollHeight-e.scrollTop<=e.clientHeight+10,Loading=({text:e="\u0110ang l\u1EA5y d\u1EEF li\u1EC7u...",color:r="gray.900"})=>jsxs(HStack,{justifyContent:"center",pointerEvents:"none",userSelect:"none",children:[jsx(Spinner,{color:r}),jsx(Text,{textAlign:"center",color:r,children:e})]}),LoadingAnimated=()=>jsx("div",{className:"loading-container",children:jsxs("div",{className:"loading-circle",children:[jsx("div",{}),jsx("div",{}),jsx("div",{}),jsx("div",{}),jsx("div",{})]})}),MessageSearchResultItem=({onClick:e,username:r,lastMessage:n,isActive:a,createdAt:l,avatarUrl:c})=>{const u=useStore(d=>d.searchStringMessage);return jsxs(HStack,{_hover:{backgroundColor:"#e4e4e450"},w:"100%",align:"start",p:3,py:2,shadow:"md",cursor:"pointer",rounded:"lg",onClick:e,bg:useColorModeValue(a?"gray.100":"bg-white",a?"#1e293b":"#1e293b50"),children:[jsx(MyAvatar,{boxSize:"2em",src:c,name:r}),jsxs(Stack,{flex:1,spacing:1,children:[jsx(Text,{noOfLines:2,fontSize:15,lineHeight:"18px",color:useColorModeValue("gray.900","slate.300"),children:r}),jsx(Text,{noOfLines:2,fontSize:12,lineHeight:"24px",fontWeight:"400",color:useColorModeValue("gray.900","slate.300"),children:jsx(Highlighter,{searchWords:[u],autoEscape:!0,textToHighlight:n})})]}),jsx(VStack,{spacing:2,alignItems:"flex-end",minW:"50px",children:jsx(Text,{color:useColorModeValue("gray.900","slate.300"),fontSize:12,children:l})})]})},ListItemSearchMessage=()=>{const e=useStore(d=>d.searchStringMessage),r=useStore(d=>d.searchResult),n=useStore(d=>d.setMessageId),a=useStore(d=>d.messageId),l=useStore(d=>d.setBeforeId),c=useStore(d=>d.setAfterId),u=d=>{n(d),l(""),c("")};return jsxs(Stack,{w:"full",h:"full",p:2,spacing:2,children:[jsx(HStack,{position:"sticky",top:0,zIndex:50,bg:useColorModeValue("white","#10172a"),alignItems:"center",children:jsx(Text,{fontSize:16,fontWeight:"bold",textColor:useColorModeValue("#444","slate.300"),children:`C\xF3 ${r==null?void 0:r.length} k\u1EBFt qu\u1EA3 t\xECm ki\u1EBFm cho t\u1EEB kh\xF3a "${e}"`})}),r==null?void 0:r.map(d=>{var _,A;const m=(A=(_=d==null?void 0:d.conversation)==null?void 0:_.participants)==null?void 0:A.find(T=>T.userId===(d==null?void 0:d.from)),E=timeDifferenceCalculator(dayjs(d==null?void 0:d.createdAt).toDate()),C=d.id===a;return jsx(MessageSearchResultItem,{isActive:C,onClick:()=>u(d.id),lastMessage:d==null?void 0:d.text,avatarUrl:m==null?void 0:m.avatar,username:m==null?void 0:m.fullName,createdAt:E},d.id)})]})},MyListConversation=()=>{const e=useStore(y=>y.setParticipants),r=useStore(y=>y.searchStringMessage),n=useStore(y=>y.channel),a=useStore(y=>y.setUserId),[l,c]=react.exports.useState(!1),[u,d]=react.exports.useState("open"),{listSupportConversations:m,onLoadMore:E,isLoadingMore:C,hasNextPage:_,loading:A}=useGetSupportConversations({status:u,search:"",channel:n}),{listConversations:T,onLoadMore:S,hasNextPage:P,isLoadingMore:k,loading:x}=useGetConversations({channel:n,search:""}),b=n==="guest"?{onLoadMore:E,isLoadingMore:C,hasNextPage:_,loading:A}:{onLoadMore:S,hasNextPage:P,isLoadingMore:k,loading:x},g=react.exports.useCallback(y=>{c(isReachBottom$1(y.target))},[]);return useDeepCompareEffect$2(()=>{l&&b.hasNextPage&&!b.isLoadingMore&&b.onLoadMore()},[l,b]),jsxs(Flex,{direction:"column",w:{base:"full",md:"260px",lg:"350px"},h:"100%",bg:useColorModeValue("white","#10172a"),onScroll:g,overflow:"auto",position:"relative",children:[r?jsx(ListItemSearchMessage,{}):jsx(DataTabs,{setParticipants:e,setLocalTab:d,setUserId:a,loading:b.loading,data:b.loading?[1,2,3,4,5,6,7,8]:n==="guest"?m:T,isSelected:u}),b.isLoadingMore&&jsx(Loading,{text:"\u0110ang t\u1EA3i cu\u1ED9c tr\xF2 chuy\u1EC7n",color:useColorModeValue("","white")})]})};var sayHi="/assets/weCanDoItWomen.png";const usePushWindowNotification=()=>({onPush:react.exports.useCallback((r,n,a)=>{if(!Notification)return;Notification.permission!=="granted"&&Notification.requestPermission();let l=new Notification("B\u1EA1n c\xF3 tin nh\u1EAFn m\u1EDBi !!",{icon:sayHi,body:`${a}: ${n}`});l.onclick=function(){window.open(`${appConfigs.apiUrl}/Admin/Chat?qsConversationId=${r}`)}},[])}),useUpdateConversations=()=>{const[e]=useUser(),{onPush:r}=usePushWindowNotification(),n=useStore(l=>l.conversationId),a=useStore(l=>l.userId);useSubscription(CONVERSATION_UPDATED,{onSubscriptionData:({client:l,subscriptionData:c})=>{var W,G,R,D,F,$,U,z,Y,Z,K,ue,ie,te,X,ce,J,oe,de,ee,pe,ae,ne,me,ge,Te,fe,Ee,_e,re,le,ye,ve,be,Ie,Pe,we,Me,je,ke,Fe,Oe,De,$e,Re,Be,Ne,Ve,Ge,qe,ze,He,Ue,We,Qe,Ye,Ke,Ze,Xe,Je,et,tt,rt;const u=l.cache.readQuery({query:MESSAGES,variables:H(B({},pickBy_1({conversationId:n,userId:n?"":a==null?void 0:a.toString(),limit:30},se=>se)),{offset:0})}),d=(G=(W=u==null?void 0:u.messages)==null?void 0:W.docs)!=null?G:[],{conversationUpdated:m}=c.data,{lastMessage:E,typing:C}=m,{id:_,from:A}=E;if(!(d==null?void 0:d.find(se=>(se==null?void 0:se.id)===_))&&e.id!==A&&C===null){const se=(D=(R=m==null?void 0:m.participants)==null?void 0:R.find(xe=>xe.userId===A))==null?void 0:D.fullName;r((F=c==null?void 0:c.data)==null?void 0:F.conversationUpdated.id,($=c==null?void 0:c.data)==null?void 0:$.conversationUpdated.lastMessage.text,se)}const S=(z=(U=c==null?void 0:c.data)==null?void 0:U.conversationUpdated)==null?void 0:z.group,P=(Z=(Y=c==null?void 0:c.data)==null?void 0:Y.conversationUpdated)==null?void 0:Z.status,k=l.cache.readQuery({query:CONVERSATIONS,variables:{offset:0,limit:20}}),x=(ue=(K=k==null?void 0:k.conversations)==null?void 0:K.docs)!=null?ue:[];if((k==null?void 0:k.conversations)&&P!=="open"){let se=[...x];const xe=se==null?void 0:se.find(he=>{var Se,Ce;return(he==null?void 0:he.id)===((Ce=(Se=c==null?void 0:c.data)==null?void 0:Se.conversationUpdated)==null?void 0:Ce.id)});xe&&((ie=c==null?void 0:c.data)==null?void 0:ie.conversationUpdated.typing)===null?(se=se.filter(he=>he.id!==xe.id),l.writeQuery({query:CONVERSATIONS,variables:{offset:0,limit:20},data:H(B({},k),{conversations:H(B({},(te=k.conversations)!=null?te:[]),{docs:[(X=c==null?void 0:c.data)==null?void 0:X.conversationUpdated,...se]})})})):xe?l.writeQuery({query:CONVERSATIONS,variables:{offset:0,limit:20},data:B({},k)}):l.writeQuery({query:CONVERSATIONS,variables:{offset:0,limit:20},data:H(B({},k),{conversations:H(B({},(ce=k.conversations)!=null?ce:[]),{docs:[(J=c==null?void 0:c.data)==null?void 0:J.conversationUpdated,...se]})})})}const b=l.cache.readQuery({query:CONVERSATIONS,variables:{group:"zimians",offset:0,limit:20}}),g=(de=(oe=b==null?void 0:b.conversations)==null?void 0:oe.docs)!=null?de:[];if((b==null?void 0:b.conversations)&&S==="zimians"){let se=[...g];const xe=se==null?void 0:se.find(he=>{var Se,Ce;return(he==null?void 0:he.id)===((Ce=(Se=c==null?void 0:c.data)==null?void 0:Se.conversationUpdated)==null?void 0:Ce.id)});xe&&((ee=c==null?void 0:c.data)==null?void 0:ee.conversationUpdated.typing)===null?(se=se.filter(he=>he.id!==xe.id),l.writeQuery({query:CONVERSATIONS,variables:{group:"zimians",offset:0,limit:20},data:H(B({},b),{conversations:H(B({},(pe=b.conversations)!=null?pe:[]),{docs:[(ae=c==null?void 0:c.data)==null?void 0:ae.conversationUpdated,...se]})})})):xe?l.writeQuery({query:CONVERSATIONS,variables:{group:"zimians",offset:0,limit:20},data:B({},b)}):l.writeQuery({query:CONVERSATIONS,variables:{group:"zimians",offset:0,limit:20},data:H(B({},b),{conversations:H(B({},(ne=b.conversations)!=null?ne:[]),{docs:[(me=c==null?void 0:c.data)==null?void 0:me.conversationUpdated,...se]})})})}const y=l.cache.readQuery({query:CONVERSATIONS,variables:{group:"student",offset:0,limit:20}}),I=(Te=(ge=y==null?void 0:y.conversations)==null?void 0:ge.docs)!=null?Te:[];if((y==null?void 0:y.conversations)&&S==="student"){let se=[...I];const xe=se==null?void 0:se.find(he=>{var Se,Ce;return(he==null?void 0:he.id)===((Ce=(Se=c==null?void 0:c.data)==null?void 0:Se.conversationUpdated)==null?void 0:Ce.id)});xe&&((fe=c==null?void 0:c.data)==null?void 0:fe.conversationUpdated.typing)===null?(se=se.filter(he=>he.id!==xe.id),l.writeQuery({query:CONVERSATIONS,variables:{group:"student",offset:0,limit:20},data:H(B({},y),{conversations:H(B({},(Ee=y.conversations)!=null?Ee:[]),{docs:[(_e=c==null?void 0:c.data)==null?void 0:_e.conversationUpdated,...se]})})})):xe?l.writeQuery({query:CONVERSATIONS,variables:{group:"student",offset:0,limit:20},data:B({},y)}):l.writeQuery({query:CONVERSATIONS,variables:{group:"student",offset:0,limit:20},data:H(B({},y),{conversations:H(B({},(re=y.conversations)!=null?re:[]),{docs:[(le=c==null?void 0:c.data)==null?void 0:le.conversationUpdated,...se]})})})}const M=l.cache.readQuery({query:CONVERSATIONS,variables:{group:"customer",offset:0,limit:20}}),j=(ve=(ye=M==null?void 0:M.conversations)==null?void 0:ye.docs)!=null?ve:[];if((M==null?void 0:M.conversations)&&S==="customer"){let se=[...j];const xe=se==null?void 0:se.find(he=>{var Se,Ce;return(he==null?void 0:he.id)===((Ce=(Se=c==null?void 0:c.data)==null?void 0:Se.conversationUpdated)==null?void 0:Ce.id)});xe&&((be=c==null?void 0:c.data)==null?void 0:be.conversationUpdated.typing)===null?(se=se.filter(he=>he.id!==xe.id),l.writeQuery({query:CONVERSATIONS,variables:{group:"customer",offset:0,limit:20},data:H(B({},M),{conversations:H(B({},(Ie=M.conversations)!=null?Ie:[]),{docs:[(Pe=c==null?void 0:c.data)==null?void 0:Pe.conversationUpdated,...se]})})})):xe?l.writeQuery({query:CONVERSATIONS,variables:{group:"customer",offset:0,limit:20},data:B({},M)}):l.writeQuery({query:CONVERSATIONS,variables:{group:"customer",offset:0,limit:20},data:H(B({},M),{conversations:H(B({},(we=M.conversations)!=null?we:[]),{docs:[(Me=c==null?void 0:c.data)==null?void 0:Me.conversationUpdated,...se]})})})}const O=l.cache.readQuery({query:SUPPORT_CONVERSATIONS,variables:{status:"open",offset:0,limit:20}}),L=(ke=(je=O==null?void 0:O.supportConversations)==null?void 0:je.docs)!=null?ke:[];if((O==null?void 0:O.supportConversations)&&S==="support"){let se=[...L];const xe=se==null?void 0:se.find(he=>{var Se,Ce;return(he==null?void 0:he.id)===((Ce=(Se=c==null?void 0:c.data)==null?void 0:Se.conversationUpdated)==null?void 0:Ce.id)});!xe&&P==="open"?l.writeQuery({query:SUPPORT_CONVERSATIONS,variables:{status:"open",offset:0,limit:20},data:H(B({},O),{supportConversations:H(B({},(Fe=O.supportConversations)!=null?Fe:[]),{docs:[(Oe=c==null?void 0:c.data)==null?void 0:Oe.conversationUpdated,...se]})})}):xe&&((De=c==null?void 0:c.data)==null?void 0:De.conversationUpdated.typing)===null&&((Be=(Re=($e=c==null?void 0:c.data)==null?void 0:$e.conversationUpdated)==null?void 0:Re.lastMessage)==null?void 0:Be.from)===e.id?(se=se.filter(he=>he.id!==xe.id),l.writeQuery({query:SUPPORT_CONVERSATIONS,variables:{status:"open",offset:0,limit:20},data:H(B({},O),{supportConversations:H(B({},(Ne=O.supportConversations)!=null?Ne:[]),{docs:[...se]})})})):l.writeQuery({query:SUPPORT_CONVERSATIONS,variables:{status:"open",offset:0,limit:20},data:H(B({},O),{supportConversations:B({},(Ve=O.supportConversations)!=null?Ve:[])})})}const N=l.cache.readQuery({query:SUPPORT_CONVERSATIONS,variables:{status:"following",offset:0,limit:20}}),q=(qe=(Ge=N==null?void 0:N.supportConversations)==null?void 0:Ge.docs)!=null?qe:[];if((N==null?void 0:N.supportConversations)&&S==="support"){let se=[...q];const xe=se==null?void 0:se.find(he=>{var Se,Ce;return(he==null?void 0:he.id)===((Ce=(Se=c==null?void 0:c.data)==null?void 0:Se.conversationUpdated)==null?void 0:Ce.id)});!xe&&P==="following"?l.writeQuery({query:SUPPORT_CONVERSATIONS,variables:{status:"following",offset:0,limit:20},data:H(B({},N),{supportConversations:H(B({},(ze=N.supportConversations)!=null?ze:[]),{docs:[(He=c==null?void 0:c.data)==null?void 0:He.conversationUpdated,...se]})})}):xe&&((Ue=c==null?void 0:c.data)==null?void 0:Ue.conversationUpdated.typing)===null?(se=se.filter(he=>he.id!==xe.id),l.writeQuery({query:SUPPORT_CONVERSATIONS,variables:{status:"following",offset:0,limit:20},data:H(B({},N),{supportConversations:H(B({},(We=N.supportConversations)!=null?We:[]),{docs:P==="following"?[(Qe=c==null?void 0:c.data)==null?void 0:Qe.conversationUpdated,...se]:[...se]})})})):l.writeQuery({query:SUPPORT_CONVERSATIONS,variables:{status:"following",offset:0,limit:20},data:H(B({},N),{supportConversations:B({},(Ye=N.supportConversations)!=null?Ye:[])})})}const V=l.cache.readQuery({query:SUPPORT_CONVERSATIONS,variables:{status:"done",offset:0,limit:20}}),Q=(Ze=(Ke=V==null?void 0:V.supportConversations)==null?void 0:Ke.docs)!=null?Ze:[];if((V==null?void 0:V.supportConversations)&&S==="support"){let se=[...Q];const xe=se==null?void 0:se.find(he=>{var Se,Ce;return(he==null?void 0:he.id)===((Ce=(Se=c==null?void 0:c.data)==null?void 0:Se.conversationUpdated)==null?void 0:Ce.id)});xe&&((Xe=c==null?void 0:c.data)==null?void 0:Xe.conversationUpdated.typing)===null?(se=se.filter(he=>he.id!==xe.id),l.writeQuery({query:SUPPORT_CONVERSATIONS,variables:{status:"done",offset:0,limit:20},data:H(B({},Q),{supportConversations:H(B({},(Je=Q.supportConversations)!=null?Je:[]),{docs:[...se]})})})):!xe&&P==="done"?l.writeQuery({query:SUPPORT_CONVERSATIONS,variables:{status:"done",offset:0,limit:20},data:H(B({},V),{supportConversations:H(B({},(et=V.supportConversations)!=null?et:[]),{docs:[(tt=c==null?void 0:c.data)==null?void 0:tt.conversationUpdated,...se]})})}):l.writeQuery({query:SUPPORT_CONVERSATIONS,variables:{status:"done",offset:0,limit:20},data:H(B({},Q),{supportConversations:B({},(rt=Q.supportConversations)!=null?rt:[])})})}},shouldResubscribe:!!e})};function replaceTextWithEmoji(e,r){const n=e.split(" ");return n.map((a,l)=>{if(a.length>1&&a.includes(":")){if(!r&&l===n.length-1)return a;const c=emojiIndex.search(a);return c.length>0?c[0].native:a}return a}).join(" ")}const useSendMessage=({setContent:e,userId:r,group:n,setConversationId:a})=>{const[l]=useUser(),c=l==null?void 0:l.id,[u,{client:d,loading:m}]=useMutation(SEND_MESSAGE),E=react.exports.useCallback(async(T,S,P,k)=>{var g;const x=P==null?void 0:P.map(({type:y,fileId:I})=>({type:y,attachmentId:I})),{data:b}=await u({variables:{recipient:pickBy_1({recipientType:k?"conversation":"user",conversationId:k||null,userId:k?null:r==null?void 0:r.toString(),group:k||n==="my chats"?"":n},y=>y),message:pickBy_1({text:T.trim(),attachments:lodash.exports.isEmpty(x)?null:x},y=>y)},update:(y,I)=>{var j;const M=y.readQuery({query:MESSAGES,variables:H(B({},pickBy_1({conversationId:k,userId:k?"":r==null?void 0:r.toString(),limit:30},O=>O)),{offset:0})});if(M==null?void 0:M.messages){const O=((j=M==null?void 0:M.messages)==null?void 0:j.docs)||[],L=O==null?void 0:O.map(N=>N.id===S?H(B(B({},N),I==null?void 0:I.data.sendMessage.lastMessage),{loading:!1}):N);y.writeQuery({query:MESSAGES,variables:H(B({},pickBy_1({conversationId:k,userId:k?"":r==null?void 0:r.toString(),limit:30},N=>N)),{offset:0}),data:H(B({},M),{messages:H(B({},M==null?void 0:M.messages),{docs:L})})})}}});k||await a((g=b==null?void 0:b.sendMessage)==null?void 0:g.id)},[u,r,n,c,l,a]),C=react.exports.useCallback(async(T,S)=>{if(S)try{await u({variables:{recipient:{conversationId:S,recipientType:"conversation"},senderAction:T}})}catch(P){console.log(P)}},[u]),_=react.exports.useCallback(async T=>{try{await u({variables:{recipient:pickBy_1({recipientType:"conversation",conversationId:T||"",userId:T?"":r==null?void 0:r.toString()},S=>S),senderAction:"markSeen"}})}catch(S){console.log(S)}},[u,r]);return{onSendMessage:react.exports.useCallback(async({nativeEvent:T,conversationId:S,file:P})=>{var I,M;const k=H(B({},pickBy_1({conversationId:S,userId:S?"":r==null?void 0:r.toString(),limit:30},j=>j)),{offset:0}),x=replaceTextWithEmoji(T.text,!0),b=v4$1(),g=d.cache.readQuery({query:MESSAGES,variables:B({},k)}),y=(M=(I=g==null?void 0:g.messages)==null?void 0:I.docs)!=null?M:[];try{e(""),(g==null?void 0:g.messages)&&d.cache.writeQuery({query:MESSAGES,variables:B({},k),data:H(B({},g),{messages:H(B({},g==null?void 0:g.messages),{docs:[{id:b,text:x,from:c,to:null,type:"update",attachments:[],seenBy:[],createdAt:dayjs().format(),updatedAt:dayjs().format(),deletedAt:null,callPayload:null,loading:!0,error:!1,conversation:{id:S,participants:[{userId:c,avatar:l.avatar,fullName:l.fullName}]},emojis:{userId:l==null?void 0:l.id,emoji:""}},...y]})})}),await E(x,b,P,S);const j=document.getElementById("chat-box");j&&(j.scrollTop=j==null?void 0:j.scrollHeight)}catch{d.cache.writeQuery({query:MESSAGES,variables:B({},k),data:H(B({},g),{messages:H(B({},g==null?void 0:g.messages),{docs:[{id:b,text:x,from:c,to:null,type:"update",attachments:[],seenBy:[],createdAt:dayjs().format(),updatedAt:dayjs().format(),deletedAt:null,callPayload:null,loading:!1,error:!0,conversation:{id:S,participants:[{userId:c,avatar:l.avatar,fullName:l.fullName}]},emojis:{userId:l==null?void 0:l.id,emoji:""}},...y]})})})}},[e,E,r,d]),onTyping:C,onMarkSeen:_,loading:m}},SpaceTime=({time:e})=>jsx(Box,{py:6,display:"flex",alignItems:"center",justifyContent:"center",w:"full",children:jsx(Text,{fontSize:14,textAlign:"center",color:useColorModeValue("gray.400","slate.300"),children:moment(e).format("HH:mm - DD-MM-YYYY")})}),validURL=e=>{if(typeof e!="string"||!e)return!1;let r;try{r=new URL(e)}catch{return!1}return r.protocol==="http:"||r.protocol==="https:"},ApiURL="https://getopengraph.herokuapp.com/",REGEX=new RegExp("^(?!mailto:)(?:(?:http|https|ftp)://)(?:\\S+(?::\\S*)?@)?(?:(?:(?:[1-9]\\d?|1\\d\\d|2[01]\\d|22[0-3])(?:\\.(?:1?\\d{1,2}|2[0-4]\\d|25[0-5])){2}(?:\\.(?:[0-9]\\d?|1\\d\\d|2[0-4]\\d|25[0-4]))|(?:(?:[a-z\\u00a1-\\uffff0-9]+-?)*[a-z\\u00a1-\\uffff0-9]+)(?:\\.(?:[a-z\\u00a1-\\uffff0-9]+-?)*[a-z\\u00a1-\\uffff0-9]+)*(?:\\.(?:[a-z\\u00a1-\\uffff]{2,})))|localhost)(?::\\d{2,5})?(?:(/|\\?|#)[^\\s]*)?$"),PreviewLink=({url:e,isMyMessage:r})=>{const[n,a]=react.exports.useState(null),[l,c]=react.exports.useState(!1);react.exports.useEffect(()=>{e&&e.match(REGEX)!==null&&u(e)},[e]);const u=async m=>{c(!0);let E=await axios.get(`${ApiURL}?url=${m}`,{method:"GET"});a(E.data),c(!1)},d=react.exports.useMemo(()=>{var m;return jsxs(HStack,{flexDirection:"column",rounded:"8px",children:[jsx(Link$1,{_hover:{color:r?"white":void 0},fontSize:14,target:"_blank",href:e,cursor:"pointer",color:"black",textDecoration:"underline",onClick:()=>window.open(e),rel:"noreferrer",children:(n==null?void 0:n.url)||e}),((m=n==null?void 0:n.img)==null?void 0:m.length)>0&&jsx(Image$1,{w:"100%",h:"150px",mt:4,alt:"img",src:n==null?void 0:n.img,rounded:"8px"}),jsx(Link$1,{_hover:{color:r?"white":void 0},fontSize:14,target:"_blank",href:e,cursor:"pointer",color:"black",textDecoration:"underline",onClick:()=>window.open(e),rel:"noreferrer",children:(n==null?void 0:n.title)||"kh\xF4ng t\xECm th\u1EA5y"})]})},[n,e,r]);return l?jsx(Link$1,{_hover:{color:r?"white":void 0},fontSize:14,target:"_blank",href:e,cursor:"pointer",color:"black",textDecoration:"underline",onClick:()=>window.open(e),rel:"noreferrer",children:e}):(n==null?void 0:n.status)===200&&n&&d},getFilePathName=e=>{if(!e)return;const r=(e==null?void 0:e.search("-"))+1;return e==null?void 0:e.slice(r)},ImageMessage=({files:e,onPress:r,isMyMessage:n})=>(console.log(e,"?????"),jsx(SimpleGrid,{maxW:{base:"250px"},columns:(e==null?void 0:e.length)>3?3:e==null?void 0:e.length,spacing:"8px",children:e==null?void 0:e.map((a,l)=>{var c,u,d,m;return jsx(Box,{rounded:"lg",borderColor:useColorModeValue("gray.100","gray.300"),cursor:"pointer",overflow:"hidden",bg:n?"blue.50":"gray.100",display:"flex",alignItems:"center",justifyContent:"center",children:((c=a==null?void 0:a.attachment)==null?void 0:c.type)!=="file"?jsx(Image$1,{objectFit:"cover",w:"80px",h:"80px",onClick:()=>r(l),src:(u=a==null?void 0:a.attachment)==null?void 0:u.fullUrl},l):jsx(Link$1,{href:(d=a==null?void 0:a.attachment)==null?void 0:d.fullUrl,target:"_blank",children:jsxs(HStack,{m:2,children:[jsx(Box,{w:6,h:6,rounded:"full",display:"flex",alignItems:"center",justifyContent:"center",borderWidth:1,children:jsx(AiOutlineFileZip,{className:"w-6 h-6 text-gray-700"})}),jsx(Text,{flex:1,color:"gray.800",fontSize:14,children:getFilePathName((m=a==null?void 0:a.attachment)==null?void 0:m.path)})]})})},l)})})),TextMessage=({text:e,isMyMessage:r,deletedAt:n,createdAt:a,attachments:l,setAttachmentIndex:c,setShowImageViewer:u})=>{const d=useStore(_=>_.searchStringMessage),m=useColorModeValue("gray.100","slate.300"),E=useColorModeValue("gray.900","slate.900"),C=useColorModeValue("text-gray-100","text-slate-300");return react.exports.useMemo(()=>jsxs(Box,{pointerEvents:n?"none":"auto",py:{sm:1.5,base:3},px:3,rounded:"lg",maxW:{sm:"2/3",base:"210px",lg:"250px",xl:"280px","2xl":"400px"},bg:r?"blue.50":m,pos:"relative",children:[jsx(IoCaretForwardSharp,{className:`${r?"text-blue-50":C} absolute ${r?"top-4 -right-2.5":"bottom-1 -left-2.5"} transform ${r?void 0:"rotate-180"}`}),n?jsx(Text,{letterSpacing:.1,fontWeight:"500",fontSize:{base:"13px"},color:E,children:"Tin nh\u1EAFn \u0111\xE3 b\u1ECB x\xF3a"}):validURL(e)?jsx(PreviewLink,{url:e,isMyMessage:r}):jsxs(Box,{children:[!!(l==null?void 0:l.length)&&jsx(ImageMessage,{isMyMessage:r,files:l,onPress:_=>{c(_),u(!0)}}),!!e&&jsx(Text,{pt:(l==null?void 0:l.length)?2:0,letterSpacing:.1,fontSize:{base:"15px"},textAlign:"left",color:E,children:jsx(Highlighter,{searchWords:[d],autoEscape:!0,textToHighlight:e||"Tin nh\u1EAFn \u0111\xE3 b\u1ECB x\xF3a"})})]})]}),[validURL,r,e,a,n,d,E,l])},useRemoveMessage=()=>{const[e,{loading:r}]=useMutation(REMOVE_MESSAGE),n=useResetQueries();return{onRemoveMessage:react.exports.useCallback(async l=>{try{await e({variables:{id:l}}),await n(["messages"])}catch(c){console.log(c)}},[e,n]),loading:r}},MessageDeleted=({isMyMessage:e,isHoverMessage:r,setIsHoverMessage:n,id:a})=>{const{onRemoveMessage:l}=useRemoveMessage(),c=async()=>{await l(a),n(!1)};return e&&r&&jsxs(Menu$1,{isLazy:!0,boundary:document.getElementById("chat-box"),children:[jsx(MenuButton,{display:"flex",alignItems:"center",justifyContent:"center",rounded:"full",cursor:"pointer",w:4,h:4,borderWidth:1,borderColor:useColorModeValue("blackAlpha.800","white"),children:jsx(Box,{display:"flex",alignItems:"center",justifyContent:"center",children:jsx(FiMoreHorizontal,{})})}),jsx(Portal,{children:jsx(MenuList,{alignItems:"center",py:4,children:jsx(MenuItem,{onClick:c,icon:jsx(Icon,{as:AiOutlineDelete,w:4,h:4}),children:"X\xF3a tin nh\u1EAFn n\xE0y"})})})]})},MessageStatus=({lastSeenMessageId:e,id:r,userData:n,isMyMessage:a,notSeen:l,loading:c,error:u,isGroup:d})=>{const m=useColorModeValue("white","slate.300");return react.exports.useMemo(()=>e===r&&!d?jsx(Box,{w:4,h:4,rounded:"full",alignItems:"center",justifyContent:"center",display:"flex",flexDirection:"column",overflow:"hidden",bg:m,children:jsx(MyAvatar,{src:n==null?void 0:n.avatar,name:n==null?void 0:n.fullName,size:"xs"})}):a&&l||c||e===r&&d?jsx(Box,{w:4,h:4,rounded:"lg",alignItems:"center",justifyContent:"center",display:"flex",flexDirection:"column",bg:c||u?"transparent":"gray.400",border:c||u?"1px solid #838b8b":void 0,children:jsx(BiCheck,{fill:"#fff",className:"w-3 h-3"})}):null,[e,r,u,c,l,a,d,n])},ImageViewer=({attachments:e,attachmentIndex:r,setAttachmentIndex:n,showImageViewer:a,setShowImageViewer:l})=>jsx(Box,{w:"100%",h:"100%",position:"relative",children:jsx(ImgsViewer,{imgs:e.map(c=>{var u;return{src:(u=c==null?void 0:c.attachment)==null?void 0:u.fullUrl}}),currImg:r,isOpen:a,onClose:()=>l(!1),showImgCount:!1,backdropCloseable:!0,onClickPrev:()=>{r>1&&n(r-1)},onClickNext:()=>{r<(e==null?void 0:e.length)-1&&n(r+1)}})}),MarkAsQuestion=({isParticipant:e,onOpen:r})=>e&&jsxs(Menu$1,{isLazy:!0,boundary:document.getElementById("chat-box"),children:[jsx(MenuButton,{display:"flex",alignItems:"center",justifyContent:"center",rounded:"full",cursor:"pointer",borderWidth:1,borderColor:useColorModeValue("blackAlpha.800","white"),w:4,h:4,mt:"-20px",children:jsx(Box,{display:"flex",alignItems:"center",justifyContent:"center",children:jsx(FiMoreHorizontal,{})})}),jsx(Portal,{children:jsx(MenuList,{alignItems:"center",py:4,children:jsx(MenuItem,{onClick:()=>{r()},icon:jsx(Icon,{as:BsQuestionCircle,w:4,h:4}),children:"\u0110\xE1nh d\u1EA5u c\xE2u h\u1ECFi"})})})]});var ring="data:audio/mpeg;base64,SUQzBAAAAAACbFRFTkMAAAANAAADTG9naWMgUHJvIFgAVERSQwAAAAwAAAMyMDE0LTExLTA5AFRYWFgAAAARAAADY29kaW5nX2hpc3RvcnkAAFRYWFgAAAAaAAADdGltZV9yZWZlcmVuY2UAMTU4NzYwMDAwAFRYWFgAAAEJAAADdW1pZAAweDAwMDAwMDAwMTU2NDhEODQyMUZBMDBGQzcwQjY0MDU4RkY3RjAwMDAwMEZBODMwMDAwNjAwMDAwODA0RjEyMDAwMDAwMDAwMDM2QjI1NzA4MDEwMDAwMDA3MDcwRDI3OEZGN0YwMDAwQjBBRTQwNThGRjdGMDAwMEVBRTZEQzhFAFRTU0UAAAAPAAADTGF2ZjU4Ljc2LjEwMABDVE9DAAAACgAAdG9jAAMBY2gwAENIQVAAAAAsAABjaDAAAAAAAAAAAH3//////////1RJVDIAAAAOAAADVGVtcG86IDEyMC4wAAAAAAAAAAAAAAD/+5AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABJbmZvAAAADwAAAAYAAAtsAElJSUlJSUlJSUlJSUlJSUltbW1tbW1tbW1tbW1tbW1tbZKSkpKSkpKSkpKSkpKSkpK2tra2tra2tra2tra2tra2ttvb29vb29vb29vb29vb29vb/////////////////////wAAAABMYXZjNTguMTMAAAAAAAAAAAAAAAAkAzgAAAAAAAALbGkpEGgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD/+5BkAA/wAABpAAAACAAADSAAAAEAAAGkAAAAIAAANIAAAAQCAQFAIBgQMEAAAD/BAACCQB4xqZo+NkGUSf8wEYAgMBqAFDu03xU4g5WM/zAPQD8vkYrwEFmIthKuDAAhjAD9baEDbOW3iUxkyYAxiDgA5U0HA5jr79i4Yk+BiZFIBjpJOBgxO8BrYNx8ky5wMsouwMAYWgMOALAMT4GwMKwTv0G3hk4GBYIAGGAKQAwTA3AGAIAX/+KIAUBAqpgFAMIRL//9AskTWZm9Bv//////////5o1BaczN3V/////////sTZPuAgIAwGBAGGAAAAB/1Y8YLg3xjUCp/7iM0NXU5cz8CdP9/8jC+A5MJMEPPoAcVYoGbQ/03gYEBAGn1aBqMu/4ZGAJAINQgBigHf+AMIxahOAceJQ//WQwWQVFIf/7GmXGoFAoFAoFAoEAZKKAAAMIKGKjez/1FfxgTQqGYAGAHGALCRpsh0VoYNGG+mIkABBCATmJ5ADxCAgmH3AahiGRZsJAOBgAIHEJALhgCIDgcOyDxiTgHGHwAQ3/+5JkvoAAAABpBQAACAAADSCgAAEYfhEPmfsAAYCYorM9UAA0FEcwgwow4DCuVbNhMXqLiIEkZAI6DAITBqAAMMMCQwFwFjAIIoMg0feI0j9DwOTeeVQBRGAMYGQExgWgJmB2D2ZF4EZfSAK7OFbpbRyZYIaARvLNFASDARBJZKz8lAC76rs94aqrU5vxICyAaq52E/Ua/Yzqxr/bJP45Rn9VpN388P/+W//9//43h4z////////+z/+3AcDN9rI5Ta25/gAAAAwHAGCMflQT/gn5KZWweGGBTgusm+TeYHcAQGCABaHJJysBueN0AoN4kJagYFQSAYFimgZfQmV9gGAdCCwIA9AYcw299aguADbwDQFgND3IYqgl1HpVDWBbs1MyU3+hyQaF5/UAAC8GIwCEKjAUBLMJAL0wzgszEkLhO/oIgyAg+TDWCxMNUEYwOQkDAMA8QjSgfhfRUAAMD4J0DiM1A2ARQMEAMUIFzwpZEumJYDLgX6AwaXQNfCwGC4QVI4dykEiyKRDGoGAhOCygCw8R+NM6RNRFDYyI8G6Q//uSZOuAB347zu5/wARxZPttz9mAl12jLn3qgAC3CyfPttACKBYAoQBqgtGSS1IJGQyw0wuSLnK5gzv1FkWSOA4zJbrmQ5xiXzek/SMiaOvdm+kqm6Leup/1///////6kv9SkDb+vY1SAICgA12UspYiFBMbRAwJXSzZ9pjvP/TogOcL6jf9Q/hwHv9ZiWB/w6c+76P/sQAIAAAwFKU3CUBMwBAGAYE+YVgvBicpImFsEYYfgsY3DQdT1WbnmeZig0GEyGBOmYBgHKBIMYD1DJQDArq0UzZp4y4S5hRCa4kZOqSYzlqx4Unw1x05PjynnASMM+mAga9dpFKk9y+ihpaNAAkg1hY6iDX4fzz/Hf1Iaf5YZscMz0MRiUUljdqtGq9jPe/1reH9wrc1nb0xgKg24Waz///////9BAACAoAANAAsoR6UQAbayyXP4Z54kG31F1kzwbIRnsv500H8iku7f0i8cioqDjkfV8tBE7LoFXsGAKYfkcZZiwYTCeFAMMIQgMyH7NlsqMU0BMRCgMRgHSSdKhUFMCkNWWU3fiB52v/7kmSFioUjM0tL3dHAMcLZ7Q8tGBHg2Syu6S3QvgompBysYDxwn4FSOSSBAsAGjutAeROE8NUgMsCcl55RvsqpFMmncuYyhh0bcKHyzqAguOgwxRd665BK5+pdbkkpUsiaIR0VCIhNkCC/63/c9bLKWt0otwAjrv//////9ECAFAHdlaSazo1I0CGzQ6FQVVIFuNpKHU+r//hyJQDoaQHg6CZvw7nmAoFgG/+gcgIs63QPWQgOqgQ0vMcUGCIC+YPY7Bmlo4GWSbGYzYfZhXgemAAAQoG09fip4hhLpc/0klkttx6XwQ7agBn2RiABoTQxBPERD0xvnRoBhihgOENIg2Buy6tlczorfzUFuaj0XiROBgEOAl5ky09Gsqbv3Xhh/L3cOf3LeVWzKo1Go1PSqjltNet4WEGBOpCCwijf/////9B48AABwIYB9AIKYCcERkDZzDTtQMXXvUzun/e7/j9g3HCcRRKNQeCYaHI/QDQMgsLF/6gMYIF76tal+StyaykKXSMFAPMNyQMRzpNzGPOEU8NPDEMagoJUjYSDIGL/+5JkegLUxTFJCx7SIDJC6XkLS0QREMcgtdyAAMOMpIqycACWaGfpKszQV7FuWxrHe9xhXQMLDgDbqOtYYxCSzkUAKJpjlr2Jy+3bt51abHLHWd+l1nehqPsNWFZ1G12vQ+7oSl343Y3+H/zvOfv8t45VgVBUiEhdX///////8aKgI9Bn////6xxtRlI4RjnI6d6BoALUEwOBY1X1Tsd84UkABgDgNDYOBHEsmf/dgqeCQsXqDMZAAAiUKAuZAAAvVC2ixZp3oZLJGBwCYUAM25RgITmyKmDguYvei5R0AAFSUXKDioAEAwwMG/FgfJAwGgWGLw4MNlOitSJjHk4OoNsEfCCBNksTJMC5i8DQIE+buNgPSC94uMhxkbENMSdTTdM4kFlAhEI0EFw+oavI5iaJ4pG5RIsRBuXhS45AsAyhFRZZFTE1JpReWy/5dJ8uFsijE4WSfrRRYySSMf/mZsTheJxMqF83L5yjWpJTmK0f/+Thw8XzUvqK5gaFxEvmJ9VJ1LRWySkl1v////zQ2Ln/8aWAAEAAA/8Mfy/Knnfy//uSZHaABwJ2SeZyoABAw2kVzMgAi0Q43lyTAAiPhJnLhhAAZCa28XC8FWaBdURVEXKRtzc6fRo1so0SQ9KPz7GRXTYtHCADAfuwsML/6UnBr/+uhH//6YAGukSJEilcVURM8qCIImSIEgk4GARKnIkaimjQXEFNhDAoKbBWwU3I7//+BT8TYoL4VwK////i9BTcjoQV0F6C////hXAo6KbFBfCvO//4FeC4goqEMhBXgoiwBszMfqzGoCAkGAgkDQdKnUdYKuLHiwdiVYK/+s6JQ1BqHUxBTUUzLjEwMKqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqg==";const useSendReactMessage=e=>{const r=new Audio(ring);r.volume=1;const[n]=useMutation(SEND_REACT_MESSAGE);return{onReactMessage:react.exports.useCallback(async(l,c)=>{try{await n({variables:{messageId:c,emoji:l}}),await e(!1),await r.play()}catch(u){console.log(u)}},[])}},emojiConfig=["http://chat-media.zim.vn/628dd69bb37316154a08c0ee/like-removebg-preview.png","http://chat-media.zim.vn/628dd6c1b37316154a08c0f6/heart-removebg-preview.png","http://chat-media.zim.vn/628dd718b37316154a08c113/emoji__1_-removebg-preview.png","http://chat-media.zim.vn/628dd733b37316154a08c11c/emoji-removebg-preview.png","http://chat-media.zim.vn/628dd751b37316154a08c128/sad-removebg-preview.png","http://chat-media.zim.vn/628dd76bb37316154a08c12d/happy__1_-removebg-preview.png","http://chat-media.zim.vn/628dd6e4b37316154a08c107/angry-removebg-preview.png"],MessageReact=({setIsHoverMessage:e,id:r})=>{const{onReactMessage:n}=useSendReactMessage(e);return jsxs(Menu$1,{isLazy:!0,boundary:document.getElementById("chat-box"),children:[jsx(MenuButton,{display:"flex",alignItems:"center",justifyContent:"center",rounded:"full",cursor:"pointer",w:4,h:4,children:jsx(Box,{display:"flex",alignItems:"center",justifyContent:"center",children:jsx(BsEmojiSmile,{className:"w-full h-full"})})}),jsx(Portal,{children:jsx(MenuList,{display:"flex",alignItems:"center",justifyContent:"center",bg:"white",rounded:"md",shadow:"sm",zIndex:1e3,children:jsx(HStack,{alignItems:"center",children:emojiConfig.map((a,l)=>jsx(Box,{w:5,h:5,rounded:"full",bg:"white",display:"flex",alignItems:"center",justifyContent:"center",cursor:"pointer",onClick:()=>n(a,r),children:jsx(Image$1,{_hover:{transition:"all 0.3s ease-in-out",transform:"scale(1.3)"},flex:1,src:a})},l))})})})]})},RenderReactionMessage=react.exports.memo(({emojis:e})=>{var a,l;const[r,n]=react.exports.useState([]);return react.exports.useEffect(()=>{(async function(){try{const c=await getUsersInfo(e==null?void 0:e.map(u=>u.userId),!1);n(c)}catch(c){console.log(c)}})()},[e]),jsx(HStack,{alignItems:"center",spacing:2,children:(l=(a=uniqBy_1(e,"emoji"))==null?void 0:a.slice(0,3))==null?void 0:l.map(c=>jsx(Tooltip,{openDelay:300,bg:"green.400",p:2,hasArrow:!0,label:jsx(VStack,{maxW:"150px",children:jsxs(HStack,{children:[jsx(Stack,{children:e==null?void 0:e.map((u,d)=>jsx(Box,{display:"flex",alignItems:"center",justifyContent:"center",w:4,h:4,cursor:"pointer",fontSize:"16px",children:jsx(Image$1,{flex:1,src:u==null?void 0:u.emoji})},d))}),jsx(Stack,{children:r==null?void 0:r.map((u,d)=>jsx(Text,{color:"white",fontWeight:600,children:u==null?void 0:u.full_name},d))})]})}),children:jsx(Box,{display:"flex",alignItems:"center",justifyContent:"center",w:4,h:4,cursor:"pointer",fontSize:"16px",children:jsx(Image$1,{flex:1,src:c==null?void 0:c.emoji})})},c==null?void 0:c.emoji))})}),CustomTableCell=TableCell.extend({addAttributes(){var e,r,n,a,l;return H(B({},(e=this.parent)==null?void 0:e.call(this)),{backgroundColor:{default:(n=(r=this.options)==null?void 0:r.backgroundColor)!=null?n:null,parseHTML:c=>c.getAttribute("data-background-color"),renderHTML:c=>({"data-background-color":c.backgroundColor,style:`background-color: ${c.backgroundColor}`})},style:{default:(l=(a=this.options)==null?void 0:a.style)!=null?l:null,parseHTML:c=>c.getAttribute("colwidth"),renderHTML:c=>({style:`${c.style?`width: ${c.colwidth}px`:null}`,colspan:c.colspan,rowspan:c.rowspan,colwidth:c.colwidth})}})}}),CustomTable=Table$1.extend({renderHTML({HTMLAttributes:e}){return["div",{class:"table-tiptap"},["table",e,["tbody",0]]]}}),CustomBlockquote=Blockquote.extend({content:"paragraph*",addAttributes(){var e;return H(B({},(e=this.parent)==null?void 0:e.call(this)),{cite:{default:this.options.cite},class:{default:this.options.class},title:{default:this.options.title}})}}),CustomLink=Link$3.extend({content:"paragraph*",addAttributes(){var e,r,n,a,l,c,u,d;return H(B({},(e=this.parent)==null?void 0:e.call(this)),{rel:{default:(n=(r=this==null?void 0:this.options)==null?void 0:r.rel)!=null?n:"noopener nofollow noreferrer"},target:{default:(l=(a=this==null?void 0:this.options)==null?void 0:a.target)!=null?l:"_blank"},"data-contextual":{default:((c=this==null?void 0:this.options)==null?void 0:c["data-contextual"])||void 0},"data-contextual-tag":{default:((u=this==null?void 0:this.options)==null?void 0:u["data-contextual-tag"])||void 0},"data-contextual-tag-id":{default:((d=this==null?void 0:this.options)==null?void 0:d["data-contextual-tag-id"])||void 0}})}}),CustomImage=Image$2.extend({addAttributes(){var e,r,n,a,l;return H(B({},(e=this.parent)==null?void 0:e.call(this)),{alt:{default:(n=(r=this==null?void 0:this.options)==null?void 0:r.alt)!=null?n:"image-alt"},title:{default:(l=(a=this==null?void 0:this.options)==null?void 0:a.title)!=null?l:"image-title"}})}}),ButtonIcon=react.exports.forwardRef((d,u)=>{var m=d,{activeKey:e="",activeOptions:r={},isActive:n=!1,icon:a,label:l=""}=m,c=Ae(m,["activeKey","activeOptions","isActive","icon","label"]);return a?jsx(Tooltip,{label:l,children:jsx(IconButton,B({ref:u,colorScheme:n?"brand":"gray",variant:"solid",fontSize:"24px",icon:a},c))}):jsx(Tooltip,{label:l,children:jsx(Button$1,H(B({ref:u,colorScheme:n?"brand":"gray",variant:"solid"},c),{children:c.children}))})});var ButtonIcon$1=react.exports.memo(ButtonIcon);const headings=[{name:"H1",value:1},{name:"H2",value:2},{name:"H3",value:3},{name:"H4",value:4},{name:"H5",value:5},{name:"H6",value:6}],MenuHeading=react.exports.memo(()=>{const{editor:e}=react.exports.useContext(EditorContext);return e?jsx(Fragment,{children:jsxs(Popover,{children:[jsx(PopoverTrigger,{children:jsx(ButtonIcon$1,{label:"H tag",icon:jsx(BiHeading,{})})}),jsx(PopoverContent,{zIndex:99,p:0,children:jsx(PopoverBody,{p:2,children:jsx(Flex,{gap:2,children:headings.map(r=>jsx(ButtonIcon$1,{isActive:e.isActive("heading",{level:r.value}),activeKey:"heading",activeOptions:{level:r.value},onClick:()=>e.chain().focus().toggleHeading({level:r.value}).run(),children:r.name},r.value))})})})]})}):null}),TableMenu=react.exports.memo(({editor:e})=>{if(!e)return null;const r=react.exports.useCallback(()=>{e.chain().focus().insertTable({rows:3,cols:3,withHeaderRow:!0}).run()},[e]),n=react.exports.useCallback(()=>{e.chain().focus().deleteTable().run()},[e]),a=react.exports.useCallback(()=>{e.chain().focus().addColumnAfter().run()},[e]),l=react.exports.useCallback(()=>{e.chain().focus().addColumnBefore().run()},[e]),c=react.exports.useCallback(()=>{e.chain().focus().deleteColumn().run()},[e]),u=react.exports.useCallback(()=>{e.chain().focus().addRowBefore().run()},[e]),d=react.exports.useCallback(()=>{e.chain().focus().addRowAfter().run()},[e]),m=react.exports.useCallback(()=>{e.chain().focus().deleteRow().run()},[e]),E=react.exports.useCallback(()=>{e.chain().focus().mergeCells().run()},[e]),C=react.exports.useCallback(()=>{e.chain().focus().splitCell().run()},[e]),_=react.exports.useCallback(()=>{e.chain().focus().toggleHeaderColumn().run()},[e]);return jsxs(SimpleGrid,{minChildWidth:38,gap:2,p:2,bg:"gray.50",w:"full",maxW:350,children:[jsx(ButtonIcon$1,{onClick:r,icon:jsx(GrTableAdd,{}),label:"Th\xEAm table"}),jsx(ButtonIcon$1,{onClick:n,icon:jsx(AiOutlineClear,{}),label:"X\xF3a table"}),jsx(ButtonIcon$1,{onClick:a,icon:jsx(RiInsertColumnRight,{}),label:"Th\xEAm c\u1ED9t b\xEAn ph\u1EA3i"}),jsx(ButtonIcon$1,{onClick:l,icon:jsx(RiInsertColumnLeft,{}),label:"Th\xEAm c\u1ED9t b\xEAn tr\xE1i"}),jsx(ButtonIcon$1,{onClick:c,icon:jsx(RiDeleteColumn,{}),label:"X\xF3a c\u1ED9t"}),jsx(ButtonIcon$1,{onClick:u,icon:jsx(RiInsertRowTop,{}),label:"Th\xEAm h\xE0ng ph\xEDa tr\xEAn"}),jsx(ButtonIcon$1,{onClick:d,icon:jsx(RiInsertRowBottom,{}),label:"Th\xEAm h\xE0ng b\xEAn d\u01B0\u1EDBi"}),jsx(ButtonIcon$1,{onClick:m,icon:jsx(RiDeleteRow,{}),label:"X\xF3a h\xE0ng"}),jsx(ButtonIcon$1,{onClick:E,icon:jsx(RiMergeCellsHorizontal,{}),label:"G\u1ED9p c\xE1c \xF4"}),jsx(ButtonIcon$1,{onClick:C,icon:jsx(RiSplitCellsHorizontal,{}),label:"T\xE1ch c\xE1c \xF4"}),jsx(ButtonIcon$1,{onClick:_,icon:jsx(AiOutlineInsertRowLeft,{}),label:"Toggle header c\u1ED9t"}),jsx(ButtonIcon$1,{onClick:()=>e.chain().focus().toggleHeaderRow().run(),icon:jsx(AiOutlineInsertRowAbove,{}),label:"Toggle header h\xE0ng"}),jsx(ButtonIcon$1,{onClick:()=>e.chain().focus().toggleHeaderCell().run(),icon:jsx(RiCellphoneFill,{}),label:"Toggle header cell"})]})}),MenuTable=react.exports.memo(({editor:e})=>e?jsxs(Popover,{children:[jsx(PopoverTrigger,{children:jsx(ButtonIcon$1,{label:"Table",icon:jsx(BiTable,{})})}),jsx(PopoverContent,{zIndex:99,p:0,children:jsx(PopoverBody,{p:0,children:jsx(TableMenu,{editor:e})})})]}):null),ImageCheckbox=e=>{const m=e,{isMultiple:r,children:n}=m,a=Ae(m,["isMultiple","children"]),{getInputProps:l,getCheckboxProps:c}=r?useCheckbox(a):useRadio(a),u=l(),d=c();return jsxs(Box,{as:"label",rounded:"base",overflow:"hidden",d:"block",children:[jsx("input",H(B({},u),{style:{display:"none"}})),jsxs(Box,H(B({},d),{cursor:"pointer",opacity:.7,_checked:{borderColor:"red.500",opacity:1},_focus:{boxShadow:"none"},pos:"relative",children:[u.checked&&jsx(CheckCircleIcon,{color:"green.500",fontSize:"2xl",pos:"absolute",top:2,left:2,zIndex:2}),n]}))]})},fetchAndStoreAccessToken=async()=>{const e=appGetAuthToken(),r=o(e);if(!appGetAuthToken()||appGetAuthToken()&&Date.now()>=r*1e3){const{token:n}=await fetchAccessToken();n&&appSetAuthToken(n)}};async function uploadFilesPromise(e){const n=`${getEnviroment("ZIM_IMAGE_ENDPOINT")}/upload`;try{if(e)return await fetchAndStoreAccessToken(),await(await fetch(n,{method:"POST",credentials:"include",headers:{Authorization:`Bearer ${appGetAuthToken()}`,Accept:"*/*","x-no-compression":"true"},body:e})).json()}catch(a){console.log("uploadFiles : "+a)}}const MediaFragment=gql`
  fragment Media on Media {
    id
    type
    path
    variants {
      id
      width
      height
      path
      type
    }
    filename
    title
    visibility
    width
    height
  }
`,GET_MEDIAS=gql`
  query medias(
    $first: Int
    $after: String
    $type: [MediaType]
    $width: Int
    $height: Int
    $search: String
    $userId: Int
  ) @api(name: "zim") {
    medias(
      first: $first
      after: $after
      type: $type
      width: $width
      height: $height
      search: $search
      userId: $userId
    ) {
      edges {
        node {
          ...Media
        }
      }
      pageInfo {
        startCursor
        endCursor
        hasNextPage
      }
    }
  }
  ${MediaFragment}
`,isReachBottom=e=>e.scrollHeight-e.scrollTop<=e.clientHeight+10,useDebounce=(e,r)=>{const[n,a]=react.exports.useState(e);return react.exports.useEffect(()=>{const l=setTimeout(()=>a(e),r);return()=>clearTimeout(l)},[e,r]),n};var index$4="";function useRouter(){const e=useParams(),r=useLocation(),n=useNavigate();return react.exports.useMemo(()=>({navigate:n,pathname:r.pathname,query:B(B({},queryString.parse(r.search)),e),location:r}),[e,r,n])}const Select=react.exports.forwardRef((u,c)=>{var d=u,{onChange:e,paramKey:r,options:n,allOption:a}=d,l=Ae(d,["onChange","paramKey","options","allOption"]);var A;useTheme();const m=useRouter();useSearchParams();const E=react.exports.useMemo(()=>({menuPortal:T=>H(B({},T),{zIndex:1400}),container:(T,{isDisabled:S,isHovered:P,isSelected:k})=>H(B({},T),{color:useColorModeValue("var(--chakra-colors-gray-900)","var(--chakra-colors-slate-400)")}),placeholder:(T,S)=>H(B({},T),{color:useColorModeValue("var(--chakra-colors-gray-400)","var(--chakra-colors-slate-500)")}),input:(T,S)=>H(B({},T),{minHeight:30,color:useColorModeValue("var(--chakra-colors-gray-900)","var(--chakra-colors-slate-400)")}),control:(T,S)=>H(B({},T),{backgroundColor:S.isDisabled?useColorModeValue("var(--chakra-colors-gray-50)","var(--chakra-colors-slate-500)"):useColorModeValue("var(--chakra-colors-white)","var(--chakra-colors-slate-700)"),borderRadius:"var(--chakra-radii-md)",borderColor:useColorModeValue("var(--chakra-colors-gray.300)","var(--chakra-colors-slate-600)")}),menu:(T,S)=>H(B({},T),{zIndex:3}),menuList:(T,S)=>H(B({},T),{backgroundColor:useColorModeValue("var(--chakra-colors-white)","var(--chakra-colors-slate-700)")}),option:(T,S)=>H(B({},T),{color:S.isSelected?useColorModeValue("white","currentColor"):S.isFocused?useColorModeValue("var(--chakra-colors-gray.900)","black"):"currentColor","&:hover":{color:S.isSelected?useColorModeValue("var(--chakra-colors-white)","var(--chakra-colors-white)"):S.isFocused?useColorModeValue("var(--chakra-colors-gray.900)","var(--chakra-colors-slate-900)"):useColorModeValue("var(--chakra-colors-gray.900)","var(--chakra-colors-slate-900)")},"&:focus":{color:"red"}}),singleValue:(T,S)=>H(B({},T),{color:useColorModeValue("var(--chakra-colors-gray.900)","var(--chakra-colors-slate-300)")})}),[]),C=react.exports.useCallback((T,S)=>{e(T,S)},[l,m.query,r]),_=react.exports.useMemo(()=>a?[a,...n]:n,[l,n,a]);return jsxs(FormControl,{isInvalid:!!(l==null?void 0:l.error),width:"100%",children:[(l==null?void 0:l.label)&&jsx(FormLabel,{htmlFor:l.id,children:l==null?void 0:l.label}),jsx(Select$2,H(B({ref:c,id:l.id},l),{styles:E,onChange:C,options:_,menuPortalTarget:document.body})),(l==null?void 0:l.error)&&jsx(FormErrorMessage,{children:(A=l==null?void 0:l.error)==null?void 0:A.message})]})});var Select$1=react.exports.memo(Select);const mediaTypeSelections=[{label:"T\u1EC7p tin",value:"file"},{label:"H\xECnh \u1EA3nh",value:"image"}],UploadModal=({selectedCallback:e,onClose:r,isOpen:n,isMultiple:a=!0,defaultTypes:l=["image"],enableTypeFilter:c,enableSizeFilter:u,width:d,height:m})=>{var Y,Z,K,ue,ie,te,X,ce;const[E,C]=react.exports.useState(!1),[_,A]=react.exports.useState([]),[T,S]=react.exports.useState(!1),[P,k]=useToggle$1(!0),[x,b]=react.exports.useState(""),g=useDebounce(x,500),[y,I]=react.exports.useState([...l]),[M]=useUser(),j={first:20,type:sortBy_1(y),search:g,height:0,width:0,userId:parseInt(M.id,10)};P&&d&&m&&(j.width=d,j.height=m);const{data:O,fetchMore:L,refetch:N,loading:q}=useQuery(GET_MEDIAS,{variables:j,notifyOnNetworkStatusChange:!0,skip:!n}),V=async()=>{C(!0);try{await N()}catch(J){console.log({e:J})}C(!1)},Q=async()=>{const J=document.createElement("input");J.setAttribute("type","file"),y.includes("file")||J.setAttribute("accept","image/*"),J.click(),J.onchange=async()=>{const oe=J.files[0],de=new FormData;de.append("file",oe),de.append("visibility","public"),await uploadFilesPromise(de),await V()}},{getCheckboxProps:W,setValue:G}=useCheckboxGroup({onChange:A}),{getRootProps:R,getRadioProps:D}=useRadioGroup({name:"images",onChange:(...J)=>A(J)}),F=R(),$=()=>{var J;e((J=O==null?void 0:O.medias)==null?void 0:J.edges.filter(oe=>_.includes(oe.node.id))),G([]),r()},U=()=>{r(),G([])},z=J=>{S(isReachBottom(J.target))};return react.exports.useEffect(()=>{var J;T&&typeof L=="function"&&((J=O.medias)==null?void 0:J.pageInfo.hasNextPage)&&!q&&(async()=>{var oe;await L({variables:{after:(oe=O.medias)==null?void 0:oe.pageInfo.endCursor,first:20,type:sortBy_1(y)}})})()},[T]),jsx(Fragment,{children:jsxs(Modal$1,{closeOnOverlayClick:!1,isCentered:!0,isOpen:n,onClose:U,size:"6xl",children:[jsx(ModalOverlay,{}),jsxs(ModalContent,{children:[jsx(ModalHeader,{children:"Th\u01B0 vi\u1EC7n media"}),jsx(ModalCloseButton,{}),jsxs(ModalBody,{children:[jsxs(HStack,{mb:4,justifyContent:"space-between",children:[jsx(Button$1,{as:Link$1,variant:"outline",colorScheme:"black",onClick:Q,leftIcon:jsx(AiOutlineCloudUpload,{}),isLoading:E,children:"Select from computer"}),c&&jsx(Box,{width:300,children:jsx(Select$1,{placeholder:"Ch\u1ECDn lo\u1EA1i media",isMulti:!0,value:y.map(J=>mediaTypeSelections.find(oe=>oe.value===J)).filter(J=>J),onChange:J=>{I(J.map(oe=>oe.value))},options:mediaTypeSelections})}),u&&d&&m&&jsxs(HStack,{children:[jsxs(Text,{children:["Ch\u1EC9 filter nh\u1EEFng \u1EA3nh size ",d," x ",m]}),jsx(Switch,{isChecked:P,onChange:k})]})]}),jsxs(Box,{width:350,pos:"relative",role:"group",children:[jsx(Input$1,{_focus:{color:"gray.900",borderColor:"gray.900"},_groupHover:{color:"gray.900",borderColor:"gray.900"},type:"text",placeholder:"T\xECm ki\u1EBFm media",size:"lg",pr:12,mb:4,value:x,onChange:J=>b(J.target.value)}),jsx(SearchIcon$1,{_groupHover:{color:"gray.900"},pos:"absolute",right:4,top:"50%",color:"gray.400",transform:"translateY(-50%)"})]}),jsxs(Box,{position:"relative",h:"calc(65vh - 50px)",overflow:E?"hidden":"auto",w:"full",flexGrow:1,p:3,onScroll:z,children:[a?jsx(HStack,{alignItems:"stretch",flexWrap:"wrap",mx:"-0.75rem",spacing:0,children:((Z=(Y=O==null?void 0:O.medias)==null?void 0:Y.edges)==null?void 0:Z.length)>0&&((ue=(K=O==null?void 0:O.medias)==null?void 0:K.edges)==null?void 0:ue.map(({node:J})=>{const oe=W({value:J.id});return jsx(Box,{w:{lg:"25%",base:"50%"},p:3,children:jsx(ImageCheckbox,H(B({isMultiple:a},oe),{children:jsx(Media,B({},J))}))},`${J.id}`)}))}):jsx(HStack,H(B({alignItems:"stretch",flexWrap:"wrap",mx:"-0.75rem",spacing:0},F),{children:((te=(ie=O==null?void 0:O.medias)==null?void 0:ie.edges)==null?void 0:te.length)>0&&((ce=(X=O==null?void 0:O.medias)==null?void 0:X.edges)==null?void 0:ce.map(({node:J})=>{const oe=D({value:J.id});return jsx(Box,{w:{lg:"25%",base:"50%"},p:3,children:jsx(ImageCheckbox,H(B({isMultiple:!1},oe),{children:jsx(Media,B({},J))}))},`${J.id}`)}))})),E&&jsxs(HStack,{bgColor:"whiteAlpha.800",pos:"absolute",top:0,bottom:0,right:0,left:0,spacing:2,justifyContent:"center",children:[jsx(Spinner,{color:"black",size:"xl"}),jsx(Text,{fontSize:"2xl",color:"black",children:"\u0110ang ti\u1EBFn h\xE0nh upload"})]})]})]}),jsxs(ModalFooter,{children:[jsx(Button$1,{colorScheme:"blue",mr:3,disabled:!_.length,onClick:$,children:"X\xE1c nh\u1EADn"}),jsx(Button$1,{colorScheme:"gray",mr:0,onClick:U,children:"\u0110\xF3ng l\u1EA1i"})]})]})]})})};function Media({type:e,path:r,filename:n}){if(e==="file")return jsx(Box,{display:"flex",flexDirection:"column",justifyContent:"center",borderWidth:2,borderColor:"#ccc",borderRadius:10,paddingX:"12px",w:"full",h:"150",children:jsxs(VStack,{spacing:6,width:"full",overflow:"hidden",children:[jsx(AiOutlineFile,{fontSize:"40"}),jsx(Box,{w:"full",display:"table",style:{tableLayout:"fixed"},children:jsx(Text,{display:"table-cell",textAlign:"center",whiteSpace:"nowrap",textOverflow:"ellipsis",overflow:"hidden",children:n})})]})});const a=getEnviroment("ZIM_IMAGE_ENDPOINT");return jsx(Box,{borderColor:"#ccc",borderRadius:10,borderWidth:2,w:"full",h:"150",backgroundSize:"cover",backgroundRepeat:"no-repeat",backgroundImage:`url('${urlJoin(a,r)}')`})}const MenuImage=react.exports.memo(({editor:e})=>{const r=d=>{d&&e.chain().focus().setImage({src:d}).run()},[n,a]=react.exports.useState(!1);return jsxs(Fragment,{children:[jsx(ButtonIcon$1,{activeKey:"image",onClick:()=>{a(!0)},icon:jsx(BiImages,{}),label:"H\xECnh \u1EA3nh"}),jsx(UploadModal,{isOpen:n,onClose:()=>{a(!1)},selectedCallback:d=>{d.map(m=>{r(`${getEnviroment("ZIM_IMAGE_ENDPOINT")}/${m.node.path}`)})},isMultiple:!0})]})}),useActiveGroupStore=create$1(e=>({activeGroup:null,setActiveGroup:r=>e({activeGroup:r}),clearActiveGroup:()=>e({activeGroup:{}})})),MenuAddFillTextSpace=react.exports.memo(({editor:e})=>{const r=useActiveGroupStore(m=>m.activeGroup);if(lodash.exports.isEmpty(r))return null;const{onOpen:n,onClose:a,isOpen:l}=useDisclosure(),[c,u]=react.exports.useState({questionGroupId:0,type:"",question:"",questionIndex:0,questionCount:1,answers:[]});if(!e)return null;const d=({questionId:m,questionGroupId:E})=>{e.chain().focus().addSpace({"data-question":"","data-question-group":"","data-label":""}).run()};return react.exports.useEffect(()=>{l&&u(H(B({},c),{answers:[]}))},[]),jsx(ButtonIcon$1,{onClick:d,icon:jsx(MdSpaceBar,{}),label:"Th\xEAm space \u0111\xE1p \xE1n"})}),MenuBar=react.exports.memo(({editor:e,stickyMenuBar:r,isMockTestEditor:n,enableSpaceMenu:a})=>{const l=useColorModeValue("white","slate.700"),c=useColorModeValue("gray.200","slate.600");return jsx(Fragment,{children:jsxs(SimpleGrid,{minChildWidth:"38px",gap:2,w:"full",bg:l,zIndex:10,px:2,py:2,pos:r?"sticky":"static",top:0,borderBottom:"1px solid",borderBottomColor:c,children:[jsx(MenuHeading,{}),jsx(ButtonIcon$1,{onClick:()=>e.chain().focus().toggleBold().run(),activeKey:"bold",label:"In \u0111\u1EADm",icon:jsx(BiBold,{})}),jsx(ButtonIcon$1,{onClick:()=>e.chain().focus().toggleItalic().run(),activeKey:"italic",label:"In nghi\xEAng",icon:jsx(BiItalic,{})}),jsx(ButtonIcon$1,{onClick:()=>e.chain().focus().toggleUnderline().run(),activeKey:"underline",label:"G\u1EA1ch ch\xE2n",icon:jsx(AiOutlineUnderline,{})}),jsx(ButtonIcon$1,{onClick:()=>e.chain().focus().toggleStrike().run(),activeKey:"strike",label:"G\u1EA1ch ngang",icon:jsx(BiStrikethrough,{})}),jsx(ButtonIcon$1,{onClick:()=>e.chain().focus().setTextAlign("left").run(),label:"C\u0103n l\u1EC1 tr\xE1i",icon:jsx(BiAlignLeft,{})}),jsx(ButtonIcon$1,{onClick:()=>e.chain().focus().setTextAlign("center").run(),label:"C\u0103n gi\u1EEFa",icon:jsx(BiAlignMiddle,{})}),jsx(ButtonIcon$1,{onClick:()=>e.chain().focus().setTextAlign("right").run(),label:"C\u0103n l\u1EC1 ph\u1EA3i",icon:jsx(BiAlignRight,{})}),jsx(ButtonIcon$1,{onClick:()=>e.chain().focus().toggleBulletList().run(),label:"Danh s\xE1ch bullet",icon:jsx(BiListUl,{})}),jsx(ButtonIcon$1,{onClick:()=>e.chain().focus().toggleOrderedList().run(),label:"Danh s\xE1ch s\u1ED1 th\u1EE9 t\u1EF1",icon:jsx(BiListOl,{})}),jsx(MenuTable,{editor:e}),jsx(MenuImage,{editor:e}),n&&a&&jsx(MenuAddFillTextSpace,{editor:e})]})})});react.exports.memo(({editor:e})=>jsxs(SimpleGrid,{minChildWidth:38,gap:2,p:2,bg:"gray.50",w:{base:300,sm:300,md:450},children:[jsx(MenuHeading,{}),jsx(ButtonIcon$1,{onClick:()=>e.chain().focus().toggleBulletList().run(),label:"Danh s\xE1ch bullet",icon:jsx(BiListUl,{}),isActive:e.isActive("bulletList")}),jsx(ButtonIcon$1,{onClick:()=>e.chain().focus().toggleOrderedList().run(),label:"Danh s\xE1ch s\u1ED1 th\u1EE9 t\u1EF1",icon:jsx(BiListOl,{}),isActive:e.isActive("orderedList")}),jsx(MenuTable,{editor:e}),jsx(MenuImage,{editor:e})]}));const ImageMenu=react.exports.memo(({editor:e})=>{const{onOpen:r,onClose:n,isOpen:a}=useDisclosure(),l=react.exports.useRef(null),[c,u]=react.exports.useState(""),[d,m]=react.exports.useState(""),E=async()=>{const C=e.getAttributes("image").src;e.chain().focus().setImage({src:C,alt:d,title:c}).run(),n()};return react.exports.useEffect(()=>{e.isActive("image")?(m(e.getAttributes("image").alt),u(e.getAttributes("image").title)):(m(""),u(""))},[a]),jsxs(HStack,{bg:"white",shadow:"base",p:2,spacing:4,rounded:4,children:[jsxs(Box,{fontSize:"sm",children:[jsxs(Text,{as:"div",children:[jsx("strong",{children:"Title:"})," ",e.getAttributes("image").title]}),jsxs(Text,{as:"div",children:[jsx("strong",{children:"Alt:"})," ",e.getAttributes("image").alt]})]}),jsxs(Popover,{isOpen:a,initialFocusRef:l,onOpen:r,onClose:n,placement:"top",closeOnBlur:!0,children:[jsx(PopoverTrigger,{children:jsx(ButtonIcon$1,{activeKey:"image",label:"Ch\u1EC9nh s\u1EEDa",icon:jsx(BiEdit,{})})}),jsx(PopoverContent,{children:jsx(Box,{p:4,bg:"white",shadow:"base",children:jsxs(VStack,{spacing:4,alignItems:"flex-start",children:[jsx(TextInput,{ref:l,label:"Title",id:"title-url",value:c,onChange:C=>u(C.target.value),autoComplete:"off"}),jsx(TextInput,{label:"Alt",id:"alt-url",value:d,onChange:C=>m(C.target.value),autoComplete:"off"}),jsx(Button$1,{colorScheme:"teal",onClick:E,children:"C\u1EADp nh\u1EADt"})]})})})]})]})});var Iframe=Node.create({name:"iframe",group:"block",atom:!0,defaultOptions:{allowFullscreen:!0,HTMLAttributes:{class:"iframe-wrapper"}},addAttributes(){return{class:{default:null},src:{default:null},frameborder:{default:0},allowfullscreen:{default:this.options.allowFullscreen,parseHTML:()=>this.options.allowFullscreen},width:{default:this.options.width},height:{default:this.options.height}}},parseHTML(){return[{tag:"iframe"}]},renderHTML({HTMLAttributes:e}){return["div",this.options.HTMLAttributes,["iframe",e]]},addCommands(){return{setIframe:e=>({tr:r,dispatch:n})=>{const{selection:a}=r,l=this.type.create(e);return n&&r.replaceRangeWith(a.from,a.to,l),!0}}}});Node.create({name:"video",group:"block",atom:!0,defaultOptions:{HTMLAttributes:{class:"iframe-wrapper"}},addAttributes(){return{class:{default:this.options.class},src:{default:null},autoplay:{default:this.options.width},loop:{default:this.options.loop},muted:{default:this.options.muted},preload:{default:this.options.preload},width:{default:this.options.width},height:{default:this.options.height},controls:{default:this.options.controls}}},parseHTML(){return[{tag:"video"}]},renderHTML({HTMLAttributes:e}){return["div",this.options.HTMLAttributes,["video",e]]},addCommands(){return{Video:e=>({tr:r,dispatch:n})=>{const{selection:a}=r,l=this.type.create(e);return n&&r.replaceRangeWith(a.from,a.to,l),!0}}}});var TextSpace=Node.create({name:"spacing",content:"inline*",atom:!0,draggable:!0,defaultOptions:{HTMLAttributes:{class:"space-wrapper","data-question":"","data-question-group":"","data-label":"","data-type":""}},addOptions(){return{inline:!0,HTMLAttributes:{},class:"space-wrapper"}},inline(){return this.options.inline},group(){return this.options.inline?"inline":"block"},addAttributes(){var e,r,n,a,l,c,u;return{class:{default:((e=this==null?void 0:this.options)==null?void 0:e.class)||void 0},"data-question":{default:(n=(r=this==null?void 0:this.options)==null?void 0:r["data-question"])!=null?n:"",parseHTML:d=>d.getAttribute("data-question")},"data-question-group":{default:(l=(a=this==null?void 0:this.options)==null?void 0:a["data-question-group"])!=null?l:"",parseHTML:d=>d.getAttribute("data-question-group")},"data-type":{default:(u=(c=this==null?void 0:this.options)==null?void 0:c["data-type"])!=null?u:"",parseHTML:d=>d.getAttribute("data-type")},"data-label":{default:"",parseHTML:d=>d.getAttribute("data-label"),renderHTML:d=>d["data-label"]?{"data-label":d["data-label"]}:{"data-label":""}}}},parseHTML(){return[{tag:"span.space-wrapper"}]},renderHTML({HTMLAttributes:e}){return["span",mergeAttributes(this.options.HTMLAttributes,e)]},renderText({HTMLAttributes:e}){var r;return(r=e==null?void 0:e["data-label"])!=null?r:"r\u1ED7ng"},addCommands(){return{addSpace:e=>({commands:r})=>r.insertContent({type:this.name,attrs:e})}}}),animate="";const TicketFragment=gql`
  fragment Ticket on TicketType {
    id
    refId
    title
    description
    createDate
    status
    createBy {
      id
      user_name
      full_name
      status
      avatar
      supporter
      role_id
      phone
      address
      supporter
    }
    receiver {
      id
      user_name
      full_name
      avatar
      role_id
      phone
      address
      supporter
    }
    reactions {
      id
      ticketId
      reaction
      count
    }
  }
`,CREATE_TICKET=gql`
  mutation createTicket($input: TicketInputType) @api(name: "appZim") {
    createTicket(input: $input) {
      ...Ticket
    }
  }
  ${TicketFragment}
`,UPDATE_TICKET=gql`
  mutation updateTicket($input: TicketInputType) @api(name: "appZim") {
    updateTicket(input: $input) {
      ...Ticket
    }
  }
  ${TicketFragment}
`,UPDATE_TICKET_STATUS=gql`
  mutation updateTicketStatus($input: TicketUpdateStatusType)
  @api(name: "appZim") {
    updateTicketStatus(input: $input) {
      ...Ticket
    }
  }
  ${TicketFragment}
`,CREATE_TICKET_REACTION=gql`
  mutation createTicketReaction($input: TicketReactionInputType)
  @api(name: "appZim") {
    createTicketReaction(input: $input) {
      id
      ticketId
      reaction
      count
    }
  }
  ${TicketFragment}
`,statuses={waiting:{tooltip:"Ticket waiting",icon:MdOutlineQuickreply,color:"red"},longtime:{tooltip:"Longtime no see",icon:HiOutlineEmojiSad,color:"blue"}},BadgeTicketStatusIcon=({type:e="waiting"})=>{const r=react.exports.useMemo(()=>statuses==null?void 0:statuses[e],[e]);return jsx(Tooltip,{hasArrow:!0,bg:useColorModeValue("slate.700","slate.700"),label:r==null?void 0:r.tooltip,"aria-label":"status tool tip",isDisabled:!(r==null?void 0:r.tooltip),color:useColorModeValue("slate.300","slate.300"),children:jsx(HStack,{spacing:1,color:`${r==null?void 0:r.color}.500`,children:jsx(Box,{as:"span",children:(r==null?void 0:r.icon)&&jsx(Icon,{as:r==null?void 0:r.icon})})})})},BoxStyled=newStyled(HStack)`
  .flip-card {
    perspective: 1000px;
    background: transparent;
  }
  .flip-card-inner {
    position: relative;
    width: 100%;
    height: 100%;
    text-align: center;
    transition: transform 0.6s;
    transform-style: preserve-3d;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
  }

  .flip-card:hover .flip-card-inner {
    transform: rotateY(180deg);
  }

  .flip-card-front,
  .flip-card-back {
    position: absolute;
    width: 100%;
    height: 100%;
    -webkit-backface-visibility: hidden;
    backface-visibility: hidden;
  }

  .flip-card-front {
    background-color: #bbb;
    color: black;
  }

  .flip-card-back {
    background-color: #2980b9;
    color: white;
    transform: rotateY(180deg);
    display: flex;
    align-items: center;
    justify-content: center;
  }
`,UserCard=({name:e,avatar:r,meta:n,lastContact:a="",badgeTitle:l="",badgeProps:c,isFlipTicketAvatarCard:u=!1,onClickFlipCard:d=null,avatarSize:m=10,badgeStatusType:E=null})=>jsxs(BoxStyled,{flexGrow:1,alignItems:"flex-start",spacing:4,children:[jsx(Box,{className:"flip-card",cursor:d?"pointer":"normal",width:m,height:m,rounded:"full",onClick:d,flexShrink:0,overflow:"hidden",children:u?jsx(Tooltip,{label:"G\u1EEDi ticket ch\u0103m s\xF3c",children:jsxs(Box,{className:"flip-card-inner",children:[jsx(Box,{className:"flip-card-front",children:jsx(Avatar,{src:r,objectFit:"cover",boxSize:m,flexShrink:0,ignoreFallback:!0})}),jsx(Box,{className:"flip-card-back",children:jsx(Icon,{as:FiSend,width:6,height:6})})]})}):jsx(Avatar,{src:r,objectFit:"cover",boxSize:m,flexShrink:0,ignoreFallback:!0})}),jsxs(VStack,{justifyContent:"stretch",alignItems:"flex-start",spacing:0,children:[jsxs(Flex,{alignItems:"flex-start",gap:2,children:[jsx(NavLink,{to:"/",children:jsx(Text,{fontWeight:600,children:e||""})}),l&&jsx(Badge$1,H(B({size:"sm",colorScheme:"green",variant:"subtle",rounded:4},c),{children:l})),E&&jsx(BadgeTicketStatusIcon,{type:E})]}),jsxs(Flex,{gap:0,fontSize:"sm",direction:"column",color:"gray.500",children:[jsx(Text,{children:n||""}),a&&jsxs(Text,{fontSize:"xs",children:["Last contact ",a||""]})]})]})]}),ReplyCard=({data:e,onTicketUpdate:r,canOperationComment:n})=>{var L;const[a,l]=useToggle(),O=e,{name:c,avatar:u,time:d,status:m,meta:E,question:C}=O,_=Ae(O,["name","avatar","time","status","meta","question"]),[A,T]=react.exports.useState(e.question.content),S=react.exports.useRef(),P=useToast(),[k,{loading:x}]=useMutation(UPDATE_TICKET,{onCompleted:N=>{const{updateTicket:q}=N;r(H(B({},e),{question:{title:q==null?void 0:q.title,content:q==null?void 0:q.description}})),P({title:"Th\xE0nh c\xF4ng!",description:"C\u1EADp nh\u1EADt n\u1ED9i dung th\xE0nh c\xF4ng.",status:"success",duration:3e3,isClosable:!0}),l()},onError:N=>{var q;P({title:"Th\u1EA5t b\u1EA1i!",description:`${(q=N==null?void 0:N.message)!=null?q:"L\u1ED7i c\u1EADp nh\u1EADt"}`,status:"error",duration:3e3,isClosable:!0})}}),[b]=useUser(),g=parseInt(b.id,10)===((L=e==null?void 0:e.createBy)==null?void 0:L.id),[y,{loading:I}]=useMutation(UPDATE_TICKET_STATUS,{onCompleted:N=>{const{updateTicketStatus:q}=N;q&&(r(H(B({},e),{status:q.status})),P({title:"Th\xE0nh c\xF4ng!",description:q.status===ETicketStatus.DELETED?"\u0110\xE3 x\xF3a ticket":"C\u1EADp nh\u1EADt tr\u1EA1ng th\xE1i th\xE0nh c\xF4ng.",status:"success",duration:3e3,isClosable:!0}))}}),M=async()=>{var N,q,V;if(S.current){const Q=(q=(N=S==null?void 0:S.current)==null?void 0:N.props)==null?void 0:q.editor;if(!Q||!(Q==null?void 0:Q.getText()))return;const G=Q==null?void 0:Q.getHTML();G&&T(G),await k({variables:{input:{refId:(V=e==null?void 0:e.id)!=null?V:EMPTY_GUID,title:"",description:G}}})}},j=react.exports.useCallback(async()=>{await y({variables:{input:{refId:e.id,status:ETicketStatus.DELETED}}})},[e]);return jsxs(Box,{w:"full",children:[jsx(Flex,{gap:4,children:jsxs(HStack,{justifyContent:"space-between",flexGrow:1,alignItems:"flex-start",children:[jsx(UserCard,{avatar:u,name:c,meta:E,avatarSize:10}),g&&n&&jsxs(Menu$1,{children:[jsx(MenuButton,{as:IconButton,variant:"ghost",icon:jsx(AiOutlineMore,{fontSize:24})}),jsxs(MenuList,{children:[jsx(MenuItem,{icon:jsx(Icon,{as:AiOutlineEdit,w:4,h:4}),onClick:()=>l(),children:"Ch\u1EC9nh s\u1EEDa"}),jsx(MenuItem,{icon:jsx(AiOutlineDelete,{}),onClick:j,children:"X\xF3a b\xECnh lu\u1EADn"})]})]})]})}),jsx(Box,{mt:2,w:"full",children:a?jsxs(Box,{mt:4,w:"full",children:[jsx(Box,{w:"full",className:"reply-editor",mt:2,children:jsx(TipTap$1,{ref:S,showWordCount:!1,placeholder:"N\u1ED9i dung ph\u1EA3n h\u1ED3i ticket...",defaultValue:A})}),jsxs(HStack,{mt:2,children:[jsx(Button$1,{colorScheme:"orange",size:"sm",onClick:M,isLoading:x,children:"C\u1EADp nh\u1EADt"}),jsx(Button$1,{colorScheme:"gray",onClick:()=>l(),size:"sm",children:"H\u1EE7y"})]})]}):jsx(Box,{mt:2,overflow:"auto",w:"full",children:jsx(StyledTextContent,{borderColor:useColorModeValue("black","white"),dangerouslySetInnerHTML:{__html:C==null?void 0:C.content}})})})]})};var ETicketReaction;(function(e){e.Ok="Ok",e.NotOk="NotOk",e.Love="Love",e.Report="Report"})(ETicketReaction||(ETicketReaction={}));const reactionOptions=[{reaction:ETicketReaction.Love,count:0},{reaction:ETicketReaction.Ok,count:0},{reaction:ETicketReaction.NotOk,count:0},{reaction:ETicketReaction.Report,count:0}],EmoIcon=({bg:e,icon:r,text:n,count:a,allowVote:l})=>{const[c,u]=react.exports.useState(a);return jsxs(HStack,{className:"emoji","aria-label":"vote emoji",spacing:{base:1,md:2},onClick:E=>{const{target:C}=E,_=C.closest(".emoji");_&&l&&(_.classList.add("animate-emoji"),u(c+1))},onAnimationEnd:E=>{const{target:C}=E,_=C.closest(".emoji");_&&_.classList.remove("animate-emoji")},transition:"all .2s ease",_hover:{color:e},children:[jsx(Flex,{bg:e,rounded:"full",width:6,height:6,d:"inline-flex",alignItems:"center",justifyContent:"center",color:"white",p:1,gap:2,className:"icon",children:jsx(Icon,{as:r,w:4,h:4})}),jsxs(HStack,{as:"span",spacing:1,children:[jsx(Box,{as:"span",d:{base:"none",md:"inline"},children:n}),jsxs(Box,{as:"span",children:["(",c,")"]})]})]})},getAttribute=e=>{switch(e){case ETicketReaction.Ok:return{icon:AiOutlineLike,colorScheme:"green"};case ETicketReaction.NotOk:return{icon:AiOutlineDislike,colorScheme:"orange"};case ETicketReaction.Report:return{icon:MdOutlineReport,colorScheme:"slate"};case ETicketReaction.Love:return{icon:AiOutlineHeart,colorScheme:"red"};default:return{icon:AiOutlineHeart,colorScheme:"blue"}}},EmojiVote=({data:e,allowVote:r=!1,ticketId:n})=>{const[a,{loading:l}]=useMutation(CREATE_TICKET_REACTION),c=useToast(),u=react.exports.useCallback(async E=>{var C;try{await a({variables:{input:{refId:n,reaction:E}}})}catch(_){c({title:"Th\u1EA5t b\u1EA1i!",description:`${(C=_==null?void 0:_.message)!=null?C:"L\u1ED7i reaction"}`,status:"error",duration:3e3,isClosable:!0})}},[n]),d=reactionOptions.map(E=>{const C=e==null?void 0:e.find(_=>_.reaction===E.reaction);return C?B({},C):B({},E)}),m=r?d:d.filter(E=>E.count>0);return m?jsx(HStack,{spacing:4,fontSize:"sm",children:m.map(E=>{const C=getAttribute(E.reaction);return jsx(Box,{pointerEvents:l?"none":"inherit",as:"span",role:r?"button":"text",onClick:r?()=>u(E.reaction):null,children:jsx(EmoIcon,{icon:C.icon,bg:`${C.colorScheme}.500`,text:E.reaction,count:E.count,allowVote:r})},E.reaction)})}):null};var ETicketStatus;(function(e){e.PROCESSING="Processing",e.CLOSED="Closed",e.WAITING="Waiting",e.DELETED="Deleted"})(ETicketStatus||(ETicketStatus={}));const LIMIT_PREVIEW_SUB=1,StyledTextContent=newStyled(Text)`
  table {
    display: table;
    margin-left: auto;
    margin-right: auto;
    width: 100%;
    border: 1px solid #666666;
    border-spacing: 5px;
    border-collapse: collapse;
    th,
    td {
      border: ${e=>`1px solid ${e.borderColor||"black"}`};
      min-width: 100px;
      padding: 2px;
    }
    p {
      margin: 0;
    }
  }
  .custom-image {
    max-width: ${e=>`${e.maxWidthImage||"100%"}`};
    height: ${e=>`${e.heightImage||"auto"}`};
  }
`,TicketCard=({data:e,defaultShowEditor:r=!1,onTicketUpdate:n})=>{var ce,J,oe,de,ee,pe;const[a,l]=useToggle(),[c,u]=react.exports.useState(e),[d,m]=react.exports.useState(((ce=c.question)==null?void 0:ce.title)||""),[E,C]=react.exports.useState(((J=c.question)==null?void 0:J.content)||""),[_,A]=react.exports.useState([]),[T,S]=useToggle(!1),[P,k]=useToggle(r),[x,b]=useToggle(!1),g=react.exports.useRef(null),y=useToast(),{name:I,avatar:M,time:j,status:O,meta:L,question:N,createDate:q}=c;useResetQueries();const[V]=useUser(),Q=parseInt(V.id,10)===((oe=e==null?void 0:e.createBy)==null?void 0:oe.id),W=(V==null?void 0:V.roleId)===5,[G,{loading:R}]=useMutation(CREATE_TICKET,{onCompleted:ae=>{var me,ge;const{createTicket:ne}=ae;c.status===ETicketStatus.WAITING&&u(H(B({},c),{status:ETicketStatus.PROCESSING})),A([H(B({},ne),{id:ne.id,name:(me=ne==null?void 0:ne.createBy)==null?void 0:me.full_name,avatar:(ge=ne==null?void 0:ne.createBy)==null?void 0:ge.avatar,status:ne==null?void 0:ne.status,time:(ne==null?void 0:ne.createDate)?dayjs(ne.createDate).format("DD/MM/YYYY HH:mm"):"",meta:" [ZIM - 308 Tr\u1EA7n Ph\xFA] - Online - Advanced (10 h\u1ECDc vi\xEAn), 16/11, 19:30-21:30",question:{title:ne==null?void 0:ne.title,content:ne==null?void 0:ne.description},comments:[]}),..._]),y({title:"Th\xE0nh c\xF4ng!",description:"B\xECnh lu\u1EADn c\u1EE7a b\u1EA1n \u0111\xE3 \u0111\u01B0\u1EE3c g\u1EEDi.",status:"success",duration:3e3,isClosable:!0})},onError:ae=>{var ne;y({title:"T\u1EA1o kh\xF4ng th\xE0nh c\xF4ng!",description:`${(ne=ae==null?void 0:ae.message)!=null?ne:"L\u1ED7i"}`,status:"error",duration:3e3,isClosable:!0})}}),[D,{loading:F}]=useMutation(UPDATE_TICKET,{onCompleted:ae=>{const{updateTicket:ne}=ae;u(H(B({},e),{question:{title:ne==null?void 0:ne.title,content:ne==null?void 0:ne.description}})),y({title:"Th\xE0nh c\xF4ng!",description:"C\u1EADp nh\u1EADt n\u1ED9i dung th\xE0nh c\xF4ng.",status:"success",duration:3e3,isClosable:!0})},onError:ae=>{var ne;y({title:"Th\u1EA5t b\u1EA1i!",description:`${(ne=ae==null?void 0:ae.message)!=null?ne:"L\u1ED7i c\u1EADp nh\u1EADt"}`,status:"error",duration:3e3,isClosable:!0})}}),[$,{loading:U}]=useMutation(UPDATE_TICKET_STATUS,{onCompleted:ae=>{const{updateTicketStatus:ne}=ae;ne&&(u(H(B({},c),{status:ne.status})),n(H(B({},c),{status:ne.status})),y({title:"Th\xE0nh c\xF4ng!",description:ne.status===ETicketStatus.DELETED?"\u0110\xE3 x\xF3a ticket":"C\u1EADp nh\u1EADt tr\u1EA1ng th\xE1i th\xE0nh c\xF4ng.",status:"success",duration:3e3,isClosable:!0}))}}),z=react.exports.useCallback(ae=>{ae.preventDefault(),k()},[]),Y=react.exports.useCallback(ae=>{ae.preventDefault(),S()},[]);react.exports.useCallback(ae=>{ae.preventDefault(),b()},[]);const Z=react.exports.useCallback(async()=>{const ae=c.status;await $({variables:{input:{refId:c.id,status:ae===TICKET_STATUS.Closed.value?ETicketStatus.PROCESSING:ETicketStatus.CLOSED}}})},[c]),K=react.exports.useCallback(async()=>{await $({variables:{input:{refId:c.id,status:ETicketStatus.DELETED}}})},[c]),ue=react.exports.useCallback(()=>{l()},[]),ie=react.exports.useCallback(ae=>{const ne=ae.status===ETicketStatus.DELETED?_.filter(me=>me.id!==ae.id):_.map(me=>me.id===ae.id?ae:me);A(ne)},[_]),te=async()=>{var ae,ne,me;if(g.current){const ge=(ne=(ae=g==null?void 0:g.current)==null?void 0:ae.props)==null?void 0:ne.editor;if(!ge||!(ge==null?void 0:ge.getText()))return;const fe=ge==null?void 0:ge.getHTML();fe&&C(fe),await D({variables:{input:{refId:(me=c==null?void 0:c.id)!=null?me:EMPTY_GUID,title:d,description:fe}}})}l()},X=async()=>{var ae,ne,me,ge,Te;try{if(g.current){const fe=(ne=(ae=g==null?void 0:g.current)==null?void 0:ae.props)==null?void 0:ne.editor;if(fe){const Ee=fe==null?void 0:fe.getHTML();if(!(fe==null?void 0:fe.getText())&&!Ee)return;await G({variables:{input:{refId:(me=e==null?void 0:e.id)!=null?me:EMPTY_GUID,title:"",description:Ee,receiverId:(ge=e==null?void 0:e.createBy)==null?void 0:ge.id}}}),(Te=fe==null?void 0:fe.commands)==null||Te.clearContent()}}}catch(fe){console.log({e:fe})}};return react.exports.useEffect(()=>{A(e.comments||[])},[e]),e?jsxs(Card$1,{className:"animate__animated animate__fadeIn",children:[jsx(Flex,{gap:4,children:jsxs(HStack,{justifyContent:"space-between",flexGrow:1,alignItems:"flex-start",children:[jsx(UserCard,{avatar:M,name:I,meta:dayjs(q).format("DD/MM/YYYY HH:mm"),badgeTitle:(de=TICKET_STATUS==null?void 0:TICKET_STATUS[c==null?void 0:c.status])==null?void 0:de.label,badgeProps:{colorScheme:(ee=TICKET_STATUS==null?void 0:TICKET_STATUS[c==null?void 0:c.status])==null?void 0:ee.colorScheme}}),!W&&jsxs(Menu$1,{children:[jsx(MenuButton,{as:IconButton,variant:"ghost",icon:jsx(AiOutlineMore,{fontSize:24})}),jsxs(MenuList,{children:[jsx(MenuItem,{icon:jsx(Icon,{as:AiOutlineEdit,w:4,h:4}),onClick:ue,children:"Ch\u1EC9nh s\u1EEDa"}),jsx(MenuItem,{icon:jsx(Icon,{as:AiOutlineLock,w:4,h:4}),onClick:Z,children:jsx("span",{children:c.status!==ETicketStatus.CLOSED?"\u0110\xF3ng ticket":"M\u1EDF ticket"})}),!W&&jsx(MenuItem,{icon:jsx(Icon,{as:AiOutlineDelete,w:4,h:4}),onClick:K,children:"X\xF3a ticket"})]})]})]})}),jsxs(Box,{mt:4,children:[a?jsxs(Box,{mt:4,w:"full",children:[jsxs(Box,{w:"full",className:"reply-editor",mt:2,children:[jsx(TextInput,{label:"Ti\xEAu \u0111\u1EC1",value:d,onChange:ae=>m(ae.target.value)}),jsx(Box,{mt:2,children:jsx(TipTap$1,{ref:g,showWordCount:!1,placeholder:"N\u1ED9i dung ticket...",defaultValue:E})})]}),jsxs(HStack,{mt:2,children:[jsx(Button$1,{colorScheme:"orange",size:"sm",onClick:te,isLoading:F,children:"C\u1EADp nh\u1EADt"}),jsx(Button$1,{colorScheme:"gray",onClick:()=>l(),size:"sm",children:"H\u1EE7y"})]})]}):jsxs(Box,{children:[jsx(Text,{fontWeight:600,children:(pe=N==null?void 0:N.title)!=null?pe:""}),jsx(Box,{mt:2,overflow:"auto",w:"full",children:jsx(StyledTextContent,{borderColor:useColorModeValue("black","white"),dangerouslySetInnerHTML:{__html:N==null?void 0:N.content}})})]}),jsx(VStack,{spacing:4,mx:-4,children:_&&_.map((ae,ne)=>T?jsx(Box,{borderTop:"1px solid",borderColor:useColorModeValue("gray.300","slate.700"),mt:4,pt:4,px:4,w:"full",children:jsx(ReplyCard,{data:H(B({},ae),{meta:dayjs(ae.createDate).format("DD/MM/YYYY HH:mm")}),onTicketUpdate:ie,canOperationComment:!W})},ae.id):ne<LIMIT_PREVIEW_SUB&&jsx(Box,{borderTop:"1px solid",borderColor:useColorModeValue("gray.300","slate.700"),mt:4,pt:4,px:4,w:"full",children:jsx(ReplyCard,{onTicketUpdate:ie,data:H(B({},ae),{meta:dayjs(ae.createDate).format("DD/MM/YYYY HH:mm")}),canOperationComment:!W})},ae.id))}),!T&&(_==null?void 0:_.length)>LIMIT_PREVIEW_SUB&&jsxs(Link$1,{href:"#",color:"blue.500",d:"flex",gap:2,mt:2,alignItems:"center",onClick:Y,children:[jsx(Box,{as:"span",className:"animate__animated animate__pulse animate__infinite",children:jsx(AiOutlineArrowDown,{})}),jsx(Text,{role:"button","aria-label":"load more replies",children:"Xem c\xE1c ph\u1EA3n h\u1ED3i tr\u01B0\u1EDBc..."})]}),P&&jsxs(Box,{mt:4,w:"full",className:"animate__animated animate__fadeIn",children:[jsx(Text,{fontWeight:600,children:"Ph\u1EA3n h\u1ED3i"}),jsx(Box,{width:"full",className:"reply-editor",mt:2,children:jsx(TipTap$1,{ref:g,showWordCount:!1,placeholder:"N\u1ED9i dung ph\u1EA3n h\u1ED3i ticket..."})}),jsx(Button$1,{colorScheme:"blue",mt:4,mb:0,size:"sm",onClick:X,isLoading:R,children:"G\u1EEDi ph\u1EA3n h\u1ED3i"})]})]}),jsx(Box,{my:4,children:jsx(EmojiVote,{data:e.reactions,allowVote:Q,ticketId:e.id})}),jsxs(Flex,{justifyContent:"space-between",alignItems:"center",gap:2,mt:4,flexWrap:"wrap",children:[jsx(HStack,{spacing:4,children:jsx(Button$1,{leftIcon:jsx(MdReply,{}),variant:P?"solid":"outline",size:"sm",colorScheme:"gray",onClick:z,isDisabled:e.status===ETicketStatus.CLOSED||e.status===ETicketStatus.DELETED,children:"Ph\u1EA3n h\u1ED3i"})}),jsx(Text,{color:"gray.500",fontSize:"sm",children:j||""})]})]}):null},Card$1=e=>{const c=e,{variant:r,children:n}=c,a=Ae(c,["variant","children"]),l=useStyleConfig("Card",{variant:r});return jsx(Box,H(B({__css:l},a),{children:n}))},questionTypeTypingId=4,questionTypeMultipleChoiceId=2,questionTypeSingleChoice=1,questionTypeEssayId=6,questionTypeSpeakingId=7,questionTypeNoAnswerQuestion=[questionTypeEssayId,questionTypeSpeakingId],blankStateCreateExercise={title:"",category:"",exerciseType:null,module:null,skill:null,lessonTopic:null,levelIn:null,levelOut:null,subject:null},QuestionAnswers=({questionTypeId:e,answers:r,handleAnswersChange:n,isDisabled:a=!1})=>{const[l,c]=react.exports.useState([]),u=e===questionTypeSingleChoice;useDeepCompareEffect$2(()=>{if(r==null?void 0:r.length){const T=r.map(S=>{var P;return H(B({},S),{alternatives:(P=S.alternatives)==null?void 0:P.join(", ")})});c(T)}else return},[r]);const d=useDebounce$1(l,500),m=react.exports.useCallback(T=>{const S=[...l].filter(P=>P.id!==T);c(S)},[l]),E=react.exports.useCallback((T,S)=>{if(u){const P=l.map(k=>k.id===T?H(B({},k),{correct:S}):H(B({},k),{correct:!1}));c(P)}else{const P=l.map(k=>k.id===T?H(B({},k),{correct:S}):k);c(P)}},[l,u]),C=react.exports.useCallback((T,S)=>{const P=[...l].map(k=>k.id===T?H(B({},k),{answer:S}):k);c(P)},[l]),_=react.exports.useCallback((T,S)=>{const P=[...l].map(k=>k.id===T?H(B({},k),{alternatives:S}):k);c([...P])},[l]),A=react.exports.useCallback(()=>{c([...l,{id:v4$1(),answer:"",correct:!1,alternatives:[]}])},[l]);return react.exports.useEffect(()=>{const T=d.map(S=>{var P;if((P=S.alternatives)==null?void 0:P.length){const x=S.alternatives.split(", ").map(b=>typeof b=="string"&&b.length>0?b.trim():"");return H(B({},S),{alternatives:x})}return H(B({},S),{alternatives:[]})});n(T)},[d]),jsxs(Fragment,{children:[jsx(HStack,{mx:-4,flexWrap:"wrap",spacing:0,alignItems:"start",children:l&&l.length>0&&l.map((T,S)=>jsxs(VStack,{px:4,w:{base:"100%",md:"50%"},pb:4,spacing:0,children:[jsxs(HStack,{pb:2,flexGrow:1,width:"full",children:[jsx(FormControl,{flexGrow:1,isInvalid:!T.correct,children:jsxs(InputGroup,{size:"lg",children:[jsx(Input$1,{placeholder:"\u0110\xE1p \xE1n",value:T.answer,onChange:P=>C(T.id,P.target.value),isReadOnly:a,fontSize:"16px"}),jsx(InputRightElement,{width:"4.5rem",children:jsx(HStack,{spacing:2,children:jsx(Switch,{isChecked:T==null?void 0:T.correct,onChange:P=>E(T.id,P.target.checked),isReadOnly:a})})})]})}),!a&&jsx(Fragment,{children:jsx(Tooltip,{label:"Delete answer",children:jsx(IconButton,{"aria-label":"Delete answer",icon:jsx(DeleteIcon,{}),colorScheme:"red",size:"lg",onClick:()=>m(T==null?void 0:T.id)})})})]}),T.correct&&e===questionTypeTypingId&&jsxs(HStack,{pl:0,pb:4,flexGrow:1,width:"full",children:[jsx(MdSubdirectoryArrowRight,{fontSize:24}),jsx(FormControl,{flexGrow:1,isInvalid:!T.correct,children:jsx(InputGroup,{size:"lg",children:jsx(Input$1,{placeholder:"KQ \u0111\xFAng kh\xE1c, c\xE1ch nhau b\u1EDFi d\u1EA5u ph\u1EA9y. VD: money, Money",value:T.alternatives,onChange:P=>_(T.id,P.target.value),fontSize:"16px",isReadOnly:a})})})]})]},`${T==null?void 0:T.id}-${S}`))}),!a&&jsx(Button$1,{colorScheme:"orange",onClick:A,mt:2,leftIcon:jsx(PlusSquareIcon,{}),children:"Th\xEAm \u0111\xE1p \xE1n"})]})},CreateQuestionForm=({questionTypeId:e})=>{const{control:r,setValue:n,watch:a}=useFormContext(),l=a("answers");return jsx(VStack,{align:"stretch",children:!questionTypeNoAnswerQuestion.includes(e)&&jsxs(Fragment,{children:[e===questionTypeMultipleChoiceId&&jsx(Fragment,{children:jsxs(HStack,{align:"center",spacing:5,children:[jsx(Controller,{control:r,render:({field:c})=>jsxs(HStack,{children:[jsx(Text,{whiteSpace:"nowrap",fontSize:"sm",children:"S\u1ED1 l\u01B0\u1EE3ng c\xE2u h\u1ECFi"}),jsx(TextInput,B({},c))]}),name:"questionCount"}),jsx(Controller,{control:r,render:({field:{value:c,onChange:u}})=>jsxs(HStack,{children:[jsx(Text,{whiteSpace:"nowrap",fontSize:"sm",children:"Chia \u0111i\u1EC3m"}),jsx(Switch,{isChecked:c,onChange:u})]}),name:"requireOrder"}),jsx(Controller,{control:r,render:({field:{value:c,onChange:u}})=>jsxs(HStack,{children:[jsx(Text,{whiteSpace:"nowrap",fontSize:"sm",children:"Check th\u1EE9 t\u1EF1 \u0111\xE1p \xE1n"}),jsx(Switch,{isChecked:c,onChange:u})]}),name:"allowToBreakPoint"})]})}),e!==questionTypeTypingId&&jsxs(Fragment,{children:[jsx(Text,{fontWeight:"600",children:"C\xE2u h\u1ECFi"}),jsx(Controller,{control:r,render:({field:c})=>jsx(Box,{className:"question-editor",children:jsx(TipTap$1,H(B({showWordCount:!1,enableSpaceMenu:!0,isMockTestEditor:!0},c),{defaultValue:c.value}))}),name:"question"})]}),jsxs(FormControl,{children:[jsxs(HStack,{spacing:4,mb:4,children:[jsx(Text,{children:"\u0110\xE1p \xE1n:"}),jsxs(HStack,{children:[jsx(Switch,{isReadOnly:!0,defaultChecked:!0}),jsx(Text,{children:"B\u1EADt n\u1EBFu \u0111\xF3 l\xE0 \u0111\xE1p \xE1n \u0111\xFAng"})]})]}),jsx(QuestionAnswers,{handleAnswersChange:c=>n("answers",c),answers:l,questionTypeId:e})]})]})})},QUESTION_TYPE=gql`
  fragment QuestionTypeField on QuestionType {
    id
    contentQuestion
    questionIndex
    questionCount
    allowToBreakPoint
    requiredOrder
    answer {
      id
      isCorrect
      contentAnswer
      answerRelated {
        id
        contentAnswerRelated
      }
    }
  }
`,CREATE_EXERCISE=gql`
  mutation createExercise($input: ExerciseInputType) @api(name: "appZim") {
    createExercise(input: $input) {
      id
    }
  }
`,UPDATE_STATUS_EXERCISE=gql`
  mutation updateStatusExercise($id: Int, $status: StatusExerciseGrapNet)
  @api(name: "appZim") {
    updateStatusExercise(id: $id, status: $status) {
      id
    }
  }
`,UPDATE_EXERCISE=gql`
  mutation updateExercise($id: Int, $input: ExerciseInputType)
  @api(name: "appZim") {
    updateExercise(id: $id, input: $input) {
      id
      title
      exerciseType {
        id
        name
      }
      cat {
        id
        name
      }
      subcat {
        id
        name
      }
      subject {
        id
        name
      }
      levelIn {
        id
        name
      }
      levelOut {
        id
        name
      }
      lessonTopic {
        id
        title
      }
    }
  }
`,CREATE_QUESTION_GROUP=gql`
  mutation createQuestionGroup($input: CreateQuestionGroupInputType)
  @api(name: "appZim") {
    createQuestionGroup(input: $input) {
      id
      title
      groupIndex
      questionType {
        id
        name
      }
      question {
        ...QuestionTypeField
      }
    }
  }
  ${QUESTION_TYPE}
`,CREATE_QUESTION=gql`
  mutation createQuestion($input: CreateQuestionInputType)
  @api(name: "appZim") {
    createQuestion(input: $input) {
      id
      contentQuestion
    }
  }
`,UPDATE_QUESTION_GROUP=gql`
  mutation updateQuestionGroup($input: updateQuestionGroupInputType)
  @api(name: "appZim") {
    updateQuestionGroup(input: $input) {
      id
      title
    }
  }
`,REMOVE_QUESTION_GROUP=gql`
  mutation removeQuestionGroup($id: Int!) @api(name: "appZim") {
    removeQuestionGroup(id: $id) {
      success
    }
  }
`,UPDATE_QUESTION=gql`
  mutation updateQuestion($input: UpdateQuestionInputType)
  @api(name: "appZim") {
    updateQuestion(input: $input) {
      id
      contentQuestion
    }
  }
`,REMOVE_QUESTION=gql`
  mutation removeQuestion($id: Int!) @api(name: "appZim") {
    removeQuestion(id: $id) {
      success
    }
  }
`,COPY_QUESTION=gql`
  mutation copyQuestion($currentId: Int, $currentQuestionIndex: Int)
  @api(name: "appZim") {
    copyQuestion(
      currentId: $currentId
      currentQuestionIndex: $currentQuestionIndex
    ) {
      id
    }
  }
`,GET_EXERCISE_PAGINATION=gql`
  query getExercisePagination($input: GetListExerciseInputType)
  @api(name: "appZim") {
    getExercisePagination(input: $input) {
      hasNextPage
      hasPrevPage
      limit
      page
      pageNext
      pagePrev
      pagingCounter
      totalPages
      totalDocs
      docs {
        id
        title
        status
        cat {
          id
          name
        }
        subcat {
          id
          name
        }
        exerciseType {
          id
          name
        }
        levelOut {
          id
          name
          graduation
        }
        levelIn {
          id
          name
          graduation
        }
        lessonTopic {
          id
          title
        }
        subject {
          id
          name
        }
      }
    }
  }
`,GET_EXERCISE_DETAIL=gql`
  query getExerciseDetail($id: Int) @api(name: "appZim") {
    getExerciseDetail(id: $id) {
      id
      title
      status
      exerciseType {
        id
        name
      }
      cat {
        id
        name
      }
      subcat {
        id
        name
      }
      questionGroup {
        id
        title
        description
        groupIndex
        questionType {
          id
          name
        }
      }
      part {
        id
        contentPart
        questionGroup {
          id
          title
          description
          groupIndex
        }
      }
      levelIn {
        id
        name
        graduation
      }
      levelOut {
        id
        name
        graduation
      }
      lessonTopic {
        id
        title
      }
      subcat {
        id
        name
      }
      subject {
        id
        name
      }
    }
  }
`,GET_QUESTION_GROUP_ID=gql`
  query getQuestionGroupId($id: Int!) @api(name: "appZim") {
    getQuestionGroupId(id: $id) {
      id
      title
      description
      groupIndex
      question {
        ...QuestionTypeField
      }
      questionType {
        id
        name
      }
      answerFile {
        id
        url
      }
      questionFile {
        id
        url
      }
      explanation
    }
  }
  ${QUESTION_TYPE}
`,GET_QUESTION_BY_ID=gql`
  query getQuestionById($id: Int!) @api(name: "appZim") {
    getQuestionById(id: $id) {
      ...QuestionTypeField
    }
  }
  ${QUESTION_TYPE}
`,QuestionModal=({isOpen:e,onClose:r,questionGroupId:n,questionId:a,questionTypeId:l})=>{const c=useForm({defaultValues:{question:"",questionIndex:0,questionCount:1,explanation:"",requireOrder:!1,allowToBreakPoint:!1,answers:[]}}),{handleSubmit:u,reset:d}=c,[m,E]=react.exports.useState([]),C=a>0,_=useResetQueries(),A=useToast(),{data:T}=useQuery(GET_QUESTION_BY_ID,{variables:{id:a},skip:!a});react.exports.useEffect(()=>{var I;if(!T)return;const y=T==null?void 0:T.getQuestionById;d({question:y.contentQuestion,questionCount:y.questionCount,allowToBreakPoint:y.allowToBreakPoint,requireOrder:y.requiredOrder,answers:y.answer.map(M=>H(B({},M),{correct:M.isCorrect,answer:M.contentAnswer,alternatives:M.answerRelated.map(j=>j.contentAnswerRelated)}))}),E((I=y.answer)==null?void 0:I.map(M=>M.id))},[T]);const[S,{loading:P}]=useMutation(CREATE_QUESTION,{onCompleted:()=>{A({title:"Th\xE0nh c\xF4ng!",description:"T\u1EA1o c\xE2u h\u1ECFi th\xE0nh c\xF4ng",status:"success",duration:3e3,isClosable:!0}),_(["getQuestionGroupId"]),r()},onError:y=>{A({title:"L\u1ED7i!",description:`Create question error: ${y.message}`,status:"error",duration:3e3,isClosable:!0})}}),[k,{loading:x}]=useMutation(UPDATE_QUESTION,{onCompleted:()=>{A({title:"Th\xE0nh c\xF4ng!",description:"C\u1EADp nh\u1EADt c\xE2u h\u1ECFi th\xE0nh c\xF4ng",status:"success",duration:3e3,isClosable:!0}),_(["getQuestionGroupId","getQuestionById"]),r()},onError:y=>{A({title:"L\u1ED7i!",description:`Update question error: ${y.message}`,status:"error",duration:3e3,isClosable:!0})}}),b=react.exports.useCallback(async y=>{var I,M;try{await S({variables:{input:{questionGroupId:n,contentQuestion:y.question,requiredOrder:y.requiredOrder,questionCount:Number(y.questionCount),allowToBreakPoint:y.allowToBreakPoint,answer:(M=(I=y.answers)==null?void 0:I.filter(j=>j.answer.length>0))==null?void 0:M.map(j=>({contentAnswer:j.answer,isCorrect:j.correct,answerRelated:j.alternatives}))}}})}catch(j){console.log(j)}},[n]),g=react.exports.useCallback(async y=>{var I;try{await k({variables:{input:{id:a,contentQuestion:y.question,requiredOrder:y.requiredOrder,questionCount:Number(y.questionCount),allowToBreakPoint:y.allowToBreakPoint,answer:(I=y.answers)==null?void 0:I.filter(M=>{var j;return((j=M.answer)==null?void 0:j.length)>0}).map(M=>{const j={id:M.id,contentAnswer:M.answer,isCorrect:M.correct,answerRelated:M.alternatives.map(O=>({contentAnswerRelated:O}))};return m.includes(j.id)||delete j.id,j})}}})}catch(M){console.log(M)}},[a,m]);return jsxs(Modal$1,{isOpen:e,onClose:r,size:"6xl",closeOnOverlayClick:!1,isCentered:!0,scrollBehavior:"inside",closeOnEsc:!1,children:[jsx(ModalOverlay,{}),jsxs(ModalContent,{children:[jsx(ModalCloseButton,{}),jsx(ModalHeader,{children:questionTypeNoAnswerQuestion.includes(l)?"Gi\u1EA3i th\xEDch \u0111\xE1p \xE1n":"T\u1EA1o c\xE2u h\u1ECFi"}),jsx(ModalBody,{children:jsx(FormProvider,H(B({},c),{children:jsx(CreateQuestionForm,{questionTypeId:l})}))}),jsxs(ModalFooter,{children:[jsx(Button$1,{mr:3,onClick:r,children:"\u0110\xF3ng"}),jsx(Button$1,{colorScheme:"green",isLoading:P||x,onClick:u(C?g:b),children:C?"C\u1EADp nh\u1EADt c\xE2u h\u1ECFi":"T\u1EA1o c\xE2u h\u1ECFi"})]})]})]})},IndeterminateCheckbox=react.exports.forwardRef((a,n)=>{var l=a,{indeterminate:e}=l,r=Ae(l,["indeterminate"]);const c=react.exports.useRef(),u=useCombinedRefs(n,c);return react.exports.useEffect(()=>{u.current&&(u.current.indeterminate=e)},[u,e]),jsx(Fragment,{children:jsx(Checkbox,H(B({ref:u},r),{isChecked:r.checked}))})}),DataTable=({columns:e,data:r,loading:n=!1,dragScrollProps:a={},renderRowSubComponent:l,tableProps:c,isSelection:u=!1,onSelectedRows:d})=>{var P;const{getTableProps:m,getTableBodyProps:E,headerGroups:C,prepareRow:_,rows:A,visibleColumns:T,selectedFlatRows:S}=reactTable.exports.useTable({data:r,columns:e,autoResetPage:!0,manualSortBy:!0},reactTable.exports.useSortBy,reactTable.exports.useExpanded,u&&reactTable.exports.useRowSelect,k=>{u&&k.visibleColumns.push(x=>[{id:"selection",Header:({getToggleAllRowsSelectedProps:b})=>jsx("div",{children:jsx(IndeterminateCheckbox,B({},b()))}),Cell:({row:b})=>jsx("div",{children:jsx(IndeterminateCheckbox,B({},b.getToggleRowSelectedProps()))})},...x])});return react.exports.useEffect(()=>{typeof d=="function"&&d(S.map(k=>k.original))},[S]),jsxs(Box,{pos:"relative",children:[jsx(VStack,{pos:"absolute",bg:"blackAlpha.700",top:0,left:0,right:0,bottom:0,zIndex:10,d:n?"flex":"none",justifyContent:"center",alignItems:"center",transition:"all .2s ease",opacity:1,children:jsx(Loading,{color:"white"})}),jsx(Box,H(B({as:p$1,pos:"relative",overflow:"auto",className:"scroll-container",hideScrollbars:!1,vertical:!0,horizontal:!0,ignoreElements:(P=a==null?void 0:a.ignoreElements)!=null?P:"[class^=chakra-text]"},a),{children:jsxs(Table$2,H(B(B({},m()),c),{children:[jsx(Thead,{children:C.map(k=>jsx(Tr,H(B({},k.getHeaderGroupProps()),{children:k.headers.map(x=>jsx(Th,H(B({},x.getHeaderProps(x.getSortByToggleProps())),{pos:(x==null?void 0:x.isSticky)?"sticky":null,left:(x==null?void 0:x.stickyLeftOffset)?x==null?void 0:x.stickyLeftOffset:0,right:(x==null?void 0:x.stickyRightOffset)?x==null?void 0:x.stickyRightOffset:0,width:x==null?void 0:x.width,minW:x==null?void 0:x.minWidth,maxW:x==null?void 0:x.maxWidth,zIndex:(x==null?void 0:x.isSticky)&&2,whiteSpace:(x==null?void 0:x.nowrap)&&"nowrap",children:jsxs(HStack,{children:[jsx(Text,{children:x.render("Header")}),jsx(chakra.span,{children:x.isSorted?x.isSortedDesc?jsx(TriangleDownIcon,{"aria-label":"sorted descending"}):jsx(TriangleUpIcon,{"aria-label":"sorted ascending"}):null})]})})))})))}),jsx(Tbody,H(B({},E()),{children:n&&(!r.length||r.length===0)?jsx(Tr,{children:jsx(Td,{colSpan:e.length,children:jsx(Box,{py:24})})}):r.length>0?A.map(k=>{_(k);const x=k.getRowProps();return jsxs(react.exports.Fragment,{children:[jsx(Tr,H(B({},k.getRowProps()),{children:k.cells.map(b=>{const g=b.column,y=(g==null?void 0:g.stickyLeftOffset)!=="undefined",I=(g==null?void 0:g.stickyRightOffset)!=="undefined";return jsx(Td,H(B({},b.getCellProps()),{isNumeric:g==null?void 0:g.isNumeric,pos:(g==null?void 0:g.isSticky)?"sticky":null,left:y?g==null?void 0:g.stickyLeftOffset:0,right:I?g==null?void 0:g.stickyRightOffset:0,zIndex:(g==null?void 0:g.isSticky)&&2,whiteSpace:(g==null?void 0:g.nowrap)&&"nowrap",bgColor:(g==null?void 0:g.isSticky)?"gray.200":"white",overflow:"visible",minW:g==null?void 0:g.minWidth,className:y?"shadow-left":"",_after:{content:'""',position:"absolute",left:(g==null?void 0:g.isSticky)?g==null?void 0:g.stickyRightOffset:0,right:(g==null?void 0:g.isSticky)?g==null?void 0:g.stickyLeftOffset:0,top:0,height:"100%",width:1,boxShadow:(g==null?void 0:g.isSticky)?`${I?"-1px":y?"1px":0} 0px 3px 0px ${useColorModeValue("var(--chakra-colors-gray-300)","var(--chakra-colors-slate-700)")}`:"none"},children:b.render("Cell")}))})})),k.isExpanded&&l({row:k,rowProps:x,visibleColumns:T})]},x.key)}):jsx(Tr,{children:jsx(Td,{colSpan:e.length,children:jsx(Text,{textAlign:"center",color:"error",children:"Kh\xF4ng c\xF3 d\u1EEF li\u1EC7u ph\xF9 h\u1EE3p."})})})}))]}))}))]})},CloneQuestionButton=({id:e,questionIndex:r})=>{const n=useToast(),a=useResetQueries(),[l,{loading:c}]=useMutation(COPY_QUESTION,{onCompleted:()=>{n({title:"Th\xE0nh c\xF4ng",description:"T\u1EA1o b\u1EA3n sao th\xE0nh c\xF4ng",status:"success",duration:3e3,isClosable:!1}),a(["getQuestionGroupId"])},onError:d=>{n({title:"Th\u1EA5t b\u1EA1i",description:`COPY QUESTION error: ${d.message}`,status:"error",duration:3e3,isClosable:!1})}}),u=react.exports.useCallback(async()=>{await l({variables:{currentId:e,currentQuestionIndex:r}})},[e,r]);return jsx(IconButton,{colorScheme:"blue",onClick:u,"aria-label":"duplicate",icon:jsx(CopyIcon,{}),isLoading:c})},Answers=react.exports.memo(({data:e})=>e?jsx(HStack,{spacing:0,flexWrap:"wrap",mx:-2,children:e.map(r=>jsx(Box,{p:1,children:jsx(Badge$1,{colorScheme:(r==null?void 0:r.isCorrect)?"facebook":"red",size:"md",fontWeight:"normal",children:r==null?void 0:r.contentAnswer})},r==null?void 0:r.contentAnswer))}):null),TableQuestion=({questionGroupId:e,isEditable:r,questions:n,loading:a,questionType:l})=>{const{isOpen:c,onClose:u,onOpen:d}=useDisclosure(),m=react.exports.useRef(),[E,C]=react.exports.useState(null),_=useToast(),A=useResetQueries(),T=react.exports.useMemo(()=>[{Header:"STT",accessor:"questionIndex",maxWidth:"10px",nowrap:!0},{Header:"C\xE2u h\u1ECFi",accessor:"contentQuestion",minWidth:"150px",Cell:({value:L})=>jsx(StyledTextContent,{dangerouslySetInnerHTML:{__html:L},style:{fontWeight:"bold"}})},{Header:"\u0110\xE1p \xE1n",accessor:"answer",Cell:({value:L})=>jsx(Answers,{data:L}),minWidth:"150px"},{Header:"SL c\xE2u",accessor:"questionCount",minWidth:"150px"},{Header:"",accessor:"actions",Cell:({row:{original:L}})=>jsxs(HStack,{spacing:2,justifyContent:"flex-end",children:[r&&jsx(CloneQuestionButton,{id:L.id,questionIndex:L.questionIndex}),jsx(IconButton,{colorScheme:"yellow",onClick:()=>S(L.id),"aria-label":"edit",icon:jsx(EditIcon,{})}),r&&jsx(IconButton,{colorScheme:"red",onClick:()=>k(L.id),"aria-label":"delete",icon:jsx(BiTrashAlt,{fontSize:16})})]})}],[r]),S=react.exports.useCallback(L=>{C(L),g()},[]),P=react.exports.useCallback(()=>{y(),C(null)},[]),k=react.exports.useCallback(L=>{C(L),d()},[]),x=react.exports.useCallback(()=>{u(),C(null)},[]),{isOpen:b,onOpen:g,onClose:y}=useDisclosure(),[I,{loading:M}]=useMutation(REMOVE_QUESTION,{onCompleted:()=>{_({title:"Th\xE0nh c\xF4ng!",description:"X\xF3a c\xE2u h\u1ECFi th\xE0nh c\xF4ng",status:"success",duration:3e3,isClosable:!0}),A(["getQuestionGroupId"]),x()},onError:L=>{_({title:"L\u1ED7i!",description:`Delete question error: ${L.message}`,status:"error",duration:3e3,isClosable:!0})}}),j=react.exports.useCallback(async()=>{try{await I({variables:{id:E}})}catch(L){console.log(L)}},[E]),O=sortBy_1(n,["questionIndex"]);return jsxs(VStack,{align:"stretch",flexGrow:0,spacing:4,children:[jsxs(Text,{as:"span",children:["Lo\u1EA1i nh\xF3m c\xE2u:"," ",jsx(Text,{as:"span",fontWeight:"600",children:l==null?void 0:l.name})]}),jsxs(HStack,{justify:"space-between",children:[jsx(Heading,{size:"md",children:"Danh s\xE1ch c\xE2u h\u1ECFi"}),r&&jsx(Button$1,{colorScheme:"blue",onClick:g,children:"T\u1EA1o c\xE2u h\u1ECFi"})]}),jsx(DataTable,{columns:T,data:O,loading:a}),b&&jsx(QuestionModal,{isOpen:b,onClose:P,questionGroupId:e,questionId:E,questionTypeId:l==null?void 0:l.id}),jsx(AlertDialog,{isOpen:c,leastDestructiveRef:m,onClose:y,children:jsx(ModalOverlay,{children:jsxs(AlertDialogContent,{children:[jsx(ModalHeader,{fontSize:"lg",fontWeight:"bold",children:"X\xF3a c\xE2u h\u1ECFi"}),jsx(ModalBody,{children:"B\u1EA1n c\xF3 ch\u1EAFc ch\u1EAFn mu\u1ED1n x\xF3a c\xE2u h\u1ECFi n\xE0y kh\xF4ng ?"}),jsxs(ModalFooter,{children:[jsx(Button$1,{ref:m,onClick:x,children:"Cancel"}),jsx(Button$1,{isLoading:M,colorScheme:"red",onClick:j,ml:3,children:"Delete"})]})]})})})]})};function QuestionMenu({editor:e,defaultValues:r}){var m;const[n,a]=react.exports.useState(null),l=useToast(),c=useActiveGroupStore(E=>E.activeGroup),u=react.exports.useMemo(()=>(c==null?void 0:c.questions)?c.questions.map(E=>H(B({},E),{label:`${E==null?void 0:E.questionIndex} - ${E.answer.filter(C=>C.isCorrect).map(C=>C.contentAnswer).join(" | ")}`,value:E.id})):[],[c]),d=()=>{var E;try{n?e.chain().updateAttributes("spacing",{"data-type":n==null?void 0:n.type,"data-question":n==null?void 0:n.id,"data-question-group":c==null?void 0:c.id,"data-label":`${n==null?void 0:n.questionIndex}`,class:"space-wrapper"}).run():l({title:"Th\u1EA5t b\u1EA1i",description:"Vui l\xF2ng ch\u1ECDn c\xE2u h\u1ECFi",status:"error",duration:3e3,isClosable:!0})}catch(C){l({title:"Th\u1EA5t b\u1EA1i",description:(E=C==null?void 0:C.message)!=null?E:"Li\xEAn k\u1EBFt kh\xF4ng th\xE0nh c\xF4ng",status:"error",duration:3e3,isClosable:!0})}};return react.exports.useEffect(()=>{if(e&&e.isActive("spacing")){const E=u.find(C=>(C==null?void 0:C.id)===Number(r==null?void 0:r.questionId));a(E)}else a(null)},[r==null?void 0:r.questionId,u]),jsxs(Card$1,{px:4,py:4,shadow:"outline",w:400,children:[jsxs(VStack,{w:"full",spacing:4,children:[jsxs(FormControl,{children:[jsx(FormLabel,{children:"Ch\u1ECDn c\xE2u h\u1ECFi:"}),jsx(Select$2,{options:u,onChange:a,value:n})]}),(n==null?void 0:n.answer)&&jsx(Box,{children:jsx(Answers,{data:(m=n==null?void 0:n.answer)!=null?m:[]})})]}),jsxs(HStack,{justifyContent:"flex-end",w:"full",mt:6,children:[jsx(Button$1,{colorScheme:"teal",onClick:d,children:"Li\xEAn k\u1EBFt c\xE2u"}),jsx(Button$1,{isDisabled:!e.isActive("spacing"),variant:"solid",colorScheme:"red",onClick:()=>{e.chain().focus().deleteSelection().run()},children:"X\xF3a"})]})]})}const EditorContext=react.exports.createContext({editor:null}),TipTap=react.exports.forwardRef(({onChange:e,placeholder:r="Nh\u1EADp n\u1ED9i dung",defaultValue:n="",extensions:a=[],enableSpaceMenu:l=!1,showWordCount:c=!0,isDisabled:u=!1,stickyMenuBar:d=!1,isMockTestEditor:m=!1},E)=>{const[C,_]=react.exports.useState(0),A=useActiveGroupStore(b=>b.activeGroup),T=react.exports.useCallback(({editor:b})=>{try{if(m&&!lodash.exports.isEmpty(A)){const g=(A==null?void 0:A.questions)||[],y=b.getHTML(),I=cheerio.load(y);I(".space-wrapper").each((M,j)=>{var N,q;const O=Number(I(j).attr("data-question")),L=g.find(V=>V.id===O);I(j).attr("data-question",(N=L==null?void 0:L.id)!=null?N:""),I(j).attr("data-question-group",(q=L==null?void 0:L.questionGroupId)!=null?q:""),I(j).attr("data-label",L?`${L==null?void 0:L.questionIndex}`:""),I(j).attr("data-type",L==null?void 0:L.type)}),b.commands.setContent(I.html(),!0)}}catch(g){console.log({e:g})}},[m,n]),S=useEditor({extensions:[StarterKit,CustomTable.configure({resizable:!0}),TableRow,TableHeader,CustomTableCell,CustomBlockquote,Typography,Underline,CustomImage.configure({inline:!0,HTMLAttributes:{class:"custom-image"}}),CustomLink.configure({openOnClick:!1}),Placeholder.configure({placeholder:r||"Nh\u1EADp n\u1ED9i dung",emptyEditorClass:"is-editor-empty"}),Iframe.configure({inline:!0}),TextAlign.configure({types:["heading","paragraph"],alignments:["left","right","center"]}),TextSpace.configure({inline:!0}),...a],content:n,onCreate:async({editor:b})=>{const g=b.state.doc.textContent.split(" ").length;_(g),T({editor:b})},onUpdate:({editor:b})=>{const g=b.state.doc.textContent.split(" ").length;_(g);const y=b.getHTML();typeof e=="function"&&e(y)},editable:!u}),P=useColorModeValue("white","slate.700"),k=useColorModeValue("gray.200","slate.700"),x=useDebounce$1(S,1e3);return react.exports.useEffect(()=>{S&&n&&((S==null?void 0:S.getText())||S.commands.setContent(n))},[S,n]),react.exports.useEffect(()=>{S&&A&&m&&T({editor:S})},[A,m,S]),jsxs(EditorContext.Provider,{value:{editor:x},children:[c&&jsxs(Box,{mb:4,children:["Word count: ",jsx("strong",{children:C})]}),jsx(Box,{p:2,pt:0,borderColor:k,borderRadius:4,borderWidth:1,h:"full",bg:P,children:S&&jsxs(Fragment,{children:[!u&&jsxs(Fragment,{children:[jsx(MenuBar,{editor:S,enableSpaceMenu:l,stickyMenuBar:d,isMockTestEditor:m}),jsx(BubbleMenu,{editor:S,pluginKey:"bubbleImageMenu",shouldShow:({editor:b})=>b.isActive("image"),children:jsx(ImageMenu,{editor:S})}),jsx(BubbleMenu,{editor:S,tippyOptions:{placement:"bottom"},pluginKey:"bubbleTextSelection",shouldShow:({editor:b,view:g,state:y})=>b.isActive("link",{"data-contextual":!0})}),m&&jsx(BubbleMenu,{editor:S,tippyOptions:{placement:"auto"},pluginKey:"bubbleTextSelection",shouldShow:({editor:b,view:g,state:y})=>b.isActive("spacing"),children:jsx(QuestionMenu,{editor:S,defaultValues:{questionId:S.isActive("spacing")?S.getAttributes("spacing")["data-question"]:null}})})]}),jsx(EditorContent,{editor:S,ref:E})]})})]})});var TipTap$1=react.exports.memo(TipTap);const CreateQAToForum=({questionText:e})=>{var c;const{data:r}=useQuery(QA_POST_CATEGORIES),{register:n,formState:{errors:a}}=useFormContext(),l=(c=r==null?void 0:r.QAPostCategoriesTree)!=null?c:[];return jsxs(Flex,{direction:"column",w:"full",justifyContent:"stretch",alignItems:"stretch",className:"reply-editor",gap:4,children:[jsx(TextInput,H(B({label:"Ti\xEAu \u0111\u1EC1",placeholder:"Nh\u1EADp ti\xEAu \u0111\u1EC1..."},n("title",{required:!0})),{error:a==null?void 0:a.name})),jsxs(Stack,{spacing:1,children:[jsx(Text,{fontSize:14,children:"C\xE2u h\u1ECFi"}),jsx(Box,{fontWeight:"400",bg:"gray.100",borderWidth:1,w:"100%",p:4,color:"black",rounded:"md",children:e})]}),jsx(Controller,{rules:{required:"Vui l\xF2ng ch\u1ECDn ch\u1EE7 \u0111\u1EC1"},name:"categories",render:({field:u,fieldState:{error:d}})=>jsx(Select$1,B({error:d,label:"Ch\u1EE7 \u0111\u1EC1",options:l,getOptionLabel:m=>m.title,getOptionValue:m=>m.id+"",formatOptionLabel:m=>jsx(Text,{children:m.title})},u))}),jsxs(Stack,{spacing:1,children:[jsx(Text,{fontWeight:"400",fontSize:14,children:"C\xE2u tr\u1EA3 l\u1EDDi"}),jsx(Controller,{name:"answer",render:({field:u})=>jsx(TipTap$1,B({showWordCount:!1,placeholder:"Nh\u1EADp c\xE2u tr\u1EA3 l\u1EDDi..."},u))})]})]})},CreateQAToForumFormModal=({onClose:e,isOpen:r,questionText:n,messageQAOwnerId:a,setIsHoverMessage:l})=>{const c=react.exports.useRef(),u=useForm({mode:"onBlur",reValidateMode:"onChange"}),d=useToast(),[m]=useMutation(CREATE_QA_POST),[E,{loading:C}]=useMutation(POST_COMMENT),_=react.exports.useCallback(async({title:A,categories:T,answer:S})=>{var P;try{const{data:k}=await m({variables:{input:pickBy_1({title:A,content:n,type:"QUESTION",categoryIds:(T==null?void 0:T.id)?[T==null?void 0:T.id]:""},x=>x),userId:a}});await E({variables:{ref:(P=k==null?void 0:k.createQAPost)==null?void 0:P.id,content:S,type:"QAPost"}}),e(),l(!1),await d({title:"Success.",description:"\u0110\u1EA9y c\xE2u h\u1ECFi l\xEAn forum th\xE0nh c\xF4ng",status:"success",duration:3e3,isClosable:!0})}catch(k){console.log(k)}},[n,a,e,l]);return jsx(FormProvider,H(B({},u),{children:jsxs(Modal$1,{finalFocusRef:c,isOpen:r,onClose:e,size:"3xl",children:[jsx(ModalOverlay,{}),jsxs(ModalContent,{children:[jsx(ModalHeader,{children:"T\u1EA1o QA "}),jsx(ModalCloseButton,{}),jsxs("form",{onSubmit:u.handleSubmit(_),children:[jsx(ModalBody,{children:jsx(CreateQAToForum,{questionText:n})}),jsxs(ModalFooter,{children:[jsx(Button$1,{leftIcon:jsx(Icon,{as:AiOutlineSend}),colorScheme:"green",mr:4,type:"submit",isLoading:C,children:"X\xE1c nh\u1EADn"}),jsx(Button$1,{variant:"ghost",onClick:e,children:"H\u1EE7y"})]})]})]})]})}))};function Message({isMyMessage:e,text:r,loading:n,notSeen:a,error:l,createdAt:c,lastSeenMessageId:u,id:d,userData:m,attachments:E,deletedAt:C,from:_,isGroup:A,conversation:T,emojis:S}){var Q,W,G;const[P]=useUser(),{isOpen:k,onOpen:x,onClose:b}=useDisclosure(),[g,y]=react.exports.useState(!1),I=useDebounce$1(g,300),[M,j]=react.exports.useState(null),[O,L]=react.exports.useState(!1),N=useColorModeValue("white","slate.300"),q=(Q=T==null?void 0:T.participants)==null?void 0:Q.find(R=>(R==null?void 0:R.userId)===_),V=react.exports.useMemo(()=>{var R;return jsx(Box,{as:"div",w:6,h:6,rounded:"full",alignItems:"center",justifyContent:"center",display:"flex",flexDirection:"column",position:"absolute",overflow:"hidden",bg:N,className:`${e?"-right-8 top-3":"-left-[30px] bottom-6"}`,children:jsx(MyAvatar,{size:"xs",src:q==null?void 0:q.avatar,name:(R=q==null?void 0:q.fullName)!=null?R:"\u1EA8n danh"})})},[e,N,q==null?void 0:q.avatar,q==null?void 0:q.fullName]);return jsxs(Fragment,{children:[jsx(HStack,{onMouseEnter:()=>!C&&y(!0),onMouseLeave:()=>!C&&y(!1),w:"full",position:"relative",alignItems:"center",justifyContent:e?"flex-end":"flex-start",children:jsx(HStack,{w:"full",alignItems:"center",children:jsx(Box,{w:"full",position:"relative",display:"flex",justifyContent:e?"flex-end":"flex-start ",children:jsxs(Box,{w:"full",display:"flex",flexDirection:"column",alignItems:e?"flex-end":"flex-start",children:[V,!e&&jsx(Text,{color:"gray.400",fontSize:12,children:((G=(W=T==null?void 0:T.participants)==null?void 0:W.find(R=>(R==null?void 0:R.userId)===_))==null?void 0:G.fullName)||"\u1EA8n danh"}),jsxs(Popover,{isOpen:I,placement:e?"left":"right",children:[jsx(PopoverTrigger,{children:jsx(HStack,{align:"center",children:jsx(TextMessage,{isMyMessage:e,text:r,deletedAt:C,createdAt:c,attachments:E,setAttachmentIndex:j,setShowImageViewer:L})})}),jsx(PopoverContent,{w:"65px",bg:"transparent",border:0,boxShadow:"none",_focus:{outline:"none"},children:I&&jsxs(HStack,{alignItems:"center",flexGrow:1,justifyContent:e?"flex-end":"flex-start",children:[jsx(MessageDeleted,{isMyMessage:e,isHoverMessage:!0,setIsHoverMessage:y,id:d}),jsx(MessageReact,{id:d,setIsHoverMessage:y}),zimRolesNumber.map(R=>R.id).includes(P.roleId)&&r&&jsx(MarkAsQuestion,{isParticipant:!e,onOpen:x})]})})]}),jsxs(HStack,{w:"full",justify:e?"flex-end":"flex-start",mt:1,alignItems:"center",spacing:2,children:[jsx(Text,{letterSpacing:.1,fontSize:12,color:"gray.500",children:dayjs(c).format("HH:mm")}),jsx(MessageStatus,{isGroup:A,isMyMessage:e,lastSeenMessageId:u,notSeen:a,id:d,error:l,loading:n,userData:m}),!C&&(S==null?void 0:S.length)>0&&jsx(RenderReactionMessage,{emojis:S})]}),jsx(ImageViewer,{setAttachmentIndex:j,attachments:E,attachmentIndex:M,showImageViewer:O,setShowImageViewer:L})]})})})}),jsx(CreateQAToForumFormModal,{isOpen:k,onClose:b,questionText:r,messageQAOwnerId:_,setIsHoverMessage:y})]})}var Message$1=react.exports.memo(Message);const ListMessage=({conversationId:e,setConversationId:r,ownerId:n,messages:a,participants:l,userId:c,isGroup:u,loading:d,channel:m,isLoadMore:E,onLoadBeforeMessage:C,hasBeforeLoadMoreButton:_,searchLoading:A,onLoadAfterMessage:T,hasAfterLoadMoreButton:S,chatBoxRef:P,totalDocs:k})=>{var Q;const[x,b]=react.exports.useState(null),g=useStore(W=>W.searchStringMessage),[y,I]=react.exports.useState(""),{onMarkSeen:M}=useSendMessage({setContent:()=>{},userId:c,group:m,setConversationId:r}),j=react.exports.useMemo(()=>lodash.exports.uniqBy(lodash.exports.reverse([].concat(a)),W=>W.id),[a]),O=(Q=a.find(W=>{var G;if(W.from===n&&((G=W.seenBy)==null?void 0:G.length)>=2)return!0}))==null?void 0:Q.id,L=l,N=L==null?void 0:L.filter(W=>W.userId!==n)[0],q=j[j.length-1],V=_default$2();return useDeepCompareEffect$2(()=>{var W;e&&(l==null?void 0:l.find(G=>G.userId===n))&&!d&&!((W=q==null?void 0:q.seenBy)==null?void 0:W.includes(n==null?void 0:n.toString()))&&async function(){await V(M(e))}()},[V,n,q,l,d,e]),react.exports.useLayoutEffect(()=>{b(null)},[e]),react.exports.useEffect(()=>{let W;return(P==null?void 0:P.current)&&!g&&x!==e&&(W=setTimeout(()=>{var G;P.current.scrollTop=(G=P.current.scrollHeight)!=null?G:5e3,b(e)},50)),()=>{clearTimeout(W)}},[e,x,g]),jsxs(VStack,{alignItems:"stretch",w:"full",px:10,py:4,spacing:4,children:[_&&k>=(j==null?void 0:j.length)&&jsx(VStack,{children:jsx(Button$1,{w:"100px",h:"30px",onClick:C,isLoading:A,loadingText:"Loading",colorScheme:"teal",variant:"outline",spinnerPlacement:"end",children:"Load more"})}),E&&jsx(Collapse,{in:E,animateOpacity:!0,children:jsx(Box,{pt:2,children:jsx(Loading,{text:"\u0110ang t\u1EA3i tin nh\u1EAFn..."})})}),j==null?void 0:j.map((W,G)=>{var Y,Z,K,ue,ie;const R=W.from===n,D=moment((Y=j[G])==null?void 0:Y.createdAt),F=moment((Z=j[G-1])==null?void 0:Z.createdAt),$=moment.duration(D.diff(F)),U=$.asMinutes(),z=U>15&&jsx(SpaceTime,{time:W==null?void 0:W.createdAt});return jsxs(t.Fragment,{children:[z,jsx(Message$1,H(B({},W),{QAId:y,setQAId:I,isGroup:u,attachments:(K=W==null?void 0:W.attachments)!=null?K:[],loading:W==null?void 0:W.loading,error:W==null?void 0:W.error,isMyMessage:R,text:W==null?void 0:W.text,id:W==null?void 0:W.id,userData:N,lastSeenMessageId:O,createdAt:W==null?void 0:W.createdAt,notSeen:((ue=W==null?void 0:W.seenBy)==null?void 0:ue.length)===1&&((ie=W==null?void 0:W.seenBy)==null?void 0:ie.includes(n))}))]},G)}),S&&(j==null?void 0:j.length)<k&&jsx(VStack,{pb:2,children:jsx(Button$1,{w:"100px",h:"30px",onClick:T,isLoading:A,loadingText:"Loading",colorScheme:"teal",variant:"outline",spinnerPlacement:"end",children:"Load more"})})]})},useGetMessages=({userId:e})=>{var k,x,b;const[r]=useUser(),n=useResetQueries(),[a,l]=react.exports.useState(!1),c=useStore(g=>g.conversationId),u=useStore(g=>g.setConversationId),d=H(B({},pickBy_1({conversationId:c,userId:c?"":e==null?void 0:e.toString(),limit:30},g=>g)),{offset:0}),{data:m,loading:E,fetchMore:C,error:_}=useQuery(MESSAGES,{variables:B({},d),notifyOnNetworkStatusChange:!0,skip:!c&&!e});useSubscription(CONVERSATION_UPDATED,{onSubscriptionData:({client:g,subscriptionData:y})=>{var M,j;const I=g.cache.readQuery({query:MESSAGES,variables:d});if(I==null?void 0:I.messages){let L=[...(j=(M=I==null?void 0:I.messages)==null?void 0:M.docs)!=null?j:[]];const{conversationUpdated:N}=y.data,{lastMessage:q,isDeleteMessage:V,id:Q}=N,{id:W,from:G}=q,R=L==null?void 0:L.find(D=>(D==null?void 0:D.id)===W);V&&!a&&c===Q&&l(!0),!R&&r.id!==G&&c===Q&&(g.writeQuery({query:MESSAGES,variables:d,data:H(B({},I),{messages:H(B({},I.messages),{docs:[q,...L]})})}),setTimeout(()=>{const D=document.getElementById("chat-box");D&&(D.scrollTop=D.scrollHeight)},50))}},shouldResubscribe:!!c});const A=((k=m==null?void 0:m.messages)==null?void 0:k.docs)||[],T=(x=m==null?void 0:m.messages)==null?void 0:x.hasNextPage,{onLoadMore:S,isLoadingMore:P}=useLoadMore({hasNextPage:T,variables:H(B({},d),{offset:A.length+1,limit:10}),fetchMore:C});return react.exports.useEffect(()=>{a&&(n(["messages","getAttachmentsInConversation"]),l(!1))},[a,n,l]),react.exports.useEffect(()=>{var g,y;(A==null?void 0:A.length)>0&&u((y=(g=A==null?void 0:A[0])==null?void 0:g.conversation)==null?void 0:y.id)},[A]),{data:m,loading:E,isLoadMore:P,loadMore:S,hasNextPage:T,messages:A,error:_,totalDocs:(b=m==null?void 0:m.messages)==null?void 0:b.totalDocs}},useGetSearchMessage=({userId:e,options:r})=>{var A,T,S;const n=useStore(P=>P.conversationId),a=useStore(P=>P.beforeId),l=useStore(P=>P.afterId),c=B({},pickBy_1({conversationId:n,userId:n?"":e==null?void 0:e.toString(),limit:10,before:a,after:l},P=>P)),{data:u,loading:d,error:m}=useQuery(MESSAGES,H(B({variables:B({},c)},r),{skip:!a&&!l})),E=((A=u==null?void 0:u.messages)==null?void 0:A.docs)||[],C=(T=u==null?void 0:u.messages)==null?void 0:T.hasNextPage,_=(S=u==null?void 0:u.messages)==null?void 0:S.hasPrevPage;return{data:u,loading:d,hasNextPage:C,hasPrevPage:_,messages:E,error:m}},useSearchMessage=({conversationId:e="",search:r})=>{var P,k;const n=useStore(x=>x.setSearchResult),a=useStore(x=>x.messageId),l=useStore(x=>x.beforeId),[c,u]=react.exports.useState(!1),{refetch:d,called:m,loading:E}=useQuery(SEARCH_MESSAGE,{variables:{conversationId:e,search:r},onCompleted:x=>{var b;n((b=x==null?void 0:x.searchMessage)==null?void 0:b.docs),u(!0)},onError:()=>{n([])},skip:e.length===0||!!a||!!l||!r}),{data:C,refetch:_,loading:A}=useQuery(NEAR_MESSAGE,{variables:{messageId:a,limit:10},skip:!a}),T=(P=C==null?void 0:C.nearbyMessages)==null?void 0:P.hasPrevPage,S=(k=C==null?void 0:C.nearbyMessages)==null?void 0:k.hasNextPage;return react.exports.useEffect(()=>{(m&&r.length||m&&c&&(r==null?void 0:r.length)===0)&&d()},[r,c,m]),react.exports.useEffect(()=>{m&&a&&_()},[a,m]),{loading:A,hasPrevPageNearbyMessages:T,hasNextPageNearbyMessages:S,getSearchResultLoading:E}},ChatContent=({conversationId:e,ownerId:r,userId:n,onSendMessage:a,participants:l,isGroup:c,setConversationId:u,channel:d,sendMessageLoading:m})=>{var K,ue,ie;const E=useApolloClient(),C=react.exports.useRef(0),_=react.exports.useRef(0),A=react.exports.useRef(null),T=react.exports.useCallback(async()=>{m||await a({nativeEvent:{text:"Hi !!"},conversationId:e})},[a,e,m]),[S,P]=react.exports.useState(!1),k=useStore(te=>te.searchStringMessage),x=useStore(te=>te.setBeforeId),b=useStore(te=>te.setAfterId),g=useStore(te=>te.messageId),y=useStore(te=>te.beforeId),I=useStore(te=>te.afterId),{messages:M,loading:j,error:O,loadMore:L,isLoadMore:N,hasNextPage:q,totalDocs:V}=useGetMessages({userId:n}),{hasPrevPageNearbyMessages:Q,hasNextPageNearbyMessages:W,getSearchResultLoading:G}=useSearchMessage({conversationId:e,search:k}),{hasNextPage:R,hasPrevPage:D}=useGetSearchMessage({userId:n,options:{onCompleted:te=>{var ee,pe,ae,ne,me;P(!0);const X=(ee=E.cache.readQuery({query:NEAR_MESSAGE,variables:{messageId:g,limit:10}}))!=null?ee:{},ce=(ae=(pe=X==null?void 0:X.nearbyMessages)==null?void 0:pe.docs)!=null?ae:[],J=(me=(ne=te==null?void 0:te.messages)==null?void 0:ne.docs)!=null?me:[],oe=y?[...ce,...J]:[...J,...ce],de=uniqBy_1(oe,"id");_.current=A.current.scrollHeight,E.cache.writeQuery({query:NEAR_MESSAGE,variables:{messageId:g,limit:10},data:H(B({},X),{nearbyMessages:H(B({},X==null?void 0:X.nearbyMessages),{docs:de})})}),P(!1),y&&(A.current.scrollTop=A.current.scrollHeight-_.current)}}}),F=(K=E.cache.readQuery({query:NEAR_MESSAGE,variables:{messageId:g,limit:10}}))!=null?K:{},$=(ie=(ue=F==null?void 0:F.nearbyMessages)==null?void 0:ue.docs)!=null?ie:[],U=(k==null?void 0:k.length)>0&&(g==null?void 0:g.length)>0?$:M,z=react.exports.useCallback(()=>{var te;b(""),x((te=$==null?void 0:$[$.length-1])==null?void 0:te.id)},[$]),Y=react.exports.useCallback(()=>{var te;x(""),b((te=$==null?void 0:$[0])==null?void 0:te.id)},[$]),Z=react.exports.useCallback(async te=>{const{scrollTop:X}=te.nativeEvent.target;X<=20&&X<C.current&&q&&!N&&!k&&(await L(),N||(A.current.scrollTop=X+60)),C.current=X},[q,N,k]);return O?jsx(HStack,{bg:"blackAlpha.400",h:"100%",justifyContent:"center",children:jsx(Loading,{color:"white"})}):jsx(Box,{ref:A,bg:useColorModeValue("white","#10172a"),h:"full",w:"full",id:"chat-box",overflow:"auto",position:"relative",onScroll:Z,children:(U==null?void 0:U.length)===0&&!G?jsxs(VStack,{w:"100%",h:"100%",display:"flex",flexDirection:"column",alignItems:"center",justifyContent:"center",spacing:8,children:[jsx(VStack,{w:"40%",spacing:4,maxW:"360px",maxH:"400px",children:jsx(Image$1,{src:sayHi,objectFit:"contain"})}),(e||n)&&(U==null?void 0:U.length)===0&&!j&&!m&&jsx(Box,{h:10,px:2,bg:"red.400",rounded:"lg",as:"button",onClick:T,fontSize:14,color:"white",fontWeight:500,children:"G\u1EEDi l\u1EDDi ch\xE0o"})]}):G?jsx(Loading,{}):jsx(Box,{h:"full",w:"full",children:jsx(ListMessage,{totalDocs:V,chatBoxRef:A,hasBeforeLoadMoreButton:(!y&&Q||D)&&(k==null?void 0:k.length)>0,hasAfterLoadMoreButton:(!I&&W||R)&&(k==null?void 0:k.length)>0,onLoadBeforeMessage:z,onLoadAfterMessage:Y,isLoadMore:N,searchLoading:S,loading:j,isGroup:c,ownerId:r,messages:U,conversationId:e,participants:l,userId:n,setConversationId:u,channel:d})})})};var COMMON_MIME_TYPES=new Map([["aac","audio/aac"],["abw","application/x-abiword"],["arc","application/x-freearc"],["avif","image/avif"],["avi","video/x-msvideo"],["azw","application/vnd.amazon.ebook"],["bin","application/octet-stream"],["bmp","image/bmp"],["bz","application/x-bzip"],["bz2","application/x-bzip2"],["cda","application/x-cdf"],["csh","application/x-csh"],["css","text/css"],["csv","text/csv"],["doc","application/msword"],["docx","application/vnd.openxmlformats-officedocument.wordprocessingml.document"],["eot","application/vnd.ms-fontobject"],["epub","application/epub+zip"],["gz","application/gzip"],["gif","image/gif"],["htm","text/html"],["html","text/html"],["ico","image/vnd.microsoft.icon"],["ics","text/calendar"],["jar","application/java-archive"],["jpeg","image/jpeg"],["jpg","image/jpeg"],["js","text/javascript"],["json","application/json"],["jsonld","application/ld+json"],["mid","audio/midi"],["midi","audio/midi"],["mjs","text/javascript"],["mp3","audio/mpeg"],["mp4","video/mp4"],["mpeg","video/mpeg"],["mpkg","application/vnd.apple.installer+xml"],["odp","application/vnd.oasis.opendocument.presentation"],["ods","application/vnd.oasis.opendocument.spreadsheet"],["odt","application/vnd.oasis.opendocument.text"],["oga","audio/ogg"],["ogv","video/ogg"],["ogx","application/ogg"],["opus","audio/opus"],["otf","font/otf"],["png","image/png"],["pdf","application/pdf"],["php","application/x-httpd-php"],["ppt","application/vnd.ms-powerpoint"],["pptx","application/vnd.openxmlformats-officedocument.presentationml.presentation"],["rar","application/vnd.rar"],["rtf","application/rtf"],["sh","application/x-sh"],["svg","image/svg+xml"],["swf","application/x-shockwave-flash"],["tar","application/x-tar"],["tif","image/tiff"],["tiff","image/tiff"],["ts","video/mp2t"],["ttf","font/ttf"],["txt","text/plain"],["vsd","application/vnd.visio"],["wav","audio/wav"],["weba","audio/webm"],["webm","video/webm"],["webp","image/webp"],["woff","font/woff"],["woff2","font/woff2"],["xhtml","application/xhtml+xml"],["xls","application/vnd.ms-excel"],["xlsx","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"],["xml","application/xml"],["xul","application/vnd.mozilla.xul+xml"],["zip","application/zip"],["7z","application/x-7z-compressed"],["mkv","video/x-matroska"],["mov","video/quicktime"],["msg","application/vnd.ms-outlook"]]);function toFileWithPath(e,r){var n=withMimeType(e);if(typeof n.path!="string"){var a=e.webkitRelativePath;Object.defineProperty(n,"path",{value:typeof r=="string"?r:typeof a=="string"&&a.length>0?a:e.name,writable:!1,configurable:!1,enumerable:!0})}return n}function withMimeType(e){var r=e.name,n=r&&r.lastIndexOf(".")!==-1;if(n&&!e.type){var a=r.split(".").pop().toLowerCase(),l=COMMON_MIME_TYPES.get(a);l&&Object.defineProperty(e,"type",{value:l,writable:!1,configurable:!1,enumerable:!0})}return e}var FILES_TO_IGNORE=[".DS_Store","Thumbs.db"];function fromEvent(e){return __awaiter(this,void 0,void 0,function(){return __generator(this,function(r){return isObject(e)&&isDataTransfer(e)?[2,getDataTransferFiles(e.dataTransfer,e.type)]:isChangeEvt(e)?[2,getInputFiles(e)]:Array.isArray(e)&&e.every(function(n){return"getFile"in n&&typeof n.getFile=="function"})?[2,getFsHandleFiles(e)]:[2,[]]})})}function isDataTransfer(e){return isObject(e.dataTransfer)}function isChangeEvt(e){return isObject(e)&&isObject(e.target)}function isObject(e){return typeof e=="object"&&e!==null}function getInputFiles(e){return fromList(e.target.files).map(function(r){return toFileWithPath(r)})}function getFsHandleFiles(e){return __awaiter(this,void 0,void 0,function(){var r;return __generator(this,function(n){switch(n.label){case 0:return[4,Promise.all(e.map(function(a){return a.getFile()}))];case 1:return r=n.sent(),[2,r.map(function(a){return toFileWithPath(a)})]}})})}function getDataTransferFiles(e,r){return __awaiter(this,void 0,void 0,function(){var n,a;return __generator(this,function(l){switch(l.label){case 0:return e===null?[2,[]]:e.items?(n=fromList(e.items).filter(function(c){return c.kind==="file"}),r!=="drop"?[2,n]:[4,Promise.all(n.map(toFilePromises))]):[3,2];case 1:return a=l.sent(),[2,noIgnoredFiles(flatten(a))];case 2:return[2,noIgnoredFiles(fromList(e.files).map(function(c){return toFileWithPath(c)}))]}})})}function noIgnoredFiles(e){return e.filter(function(r){return FILES_TO_IGNORE.indexOf(r.name)===-1})}function fromList(e){if(e===null)return[];for(var r=[],n=0;n<e.length;n++){var a=e[n];r.push(a)}return r}function toFilePromises(e){if(typeof e.webkitGetAsEntry!="function")return fromDataTransferItem(e);var r=e.webkitGetAsEntry();return r&&r.isDirectory?fromDirEntry(r):fromDataTransferItem(e)}function flatten(e){return e.reduce(function(r,n){return __spread(r,Array.isArray(n)?flatten(n):[n])},[])}function fromDataTransferItem(e){var r=e.getAsFile();if(!r)return Promise.reject(e+" is not a File");var n=toFileWithPath(r);return Promise.resolve(n)}function fromEntry(e){return __awaiter(this,void 0,void 0,function(){return __generator(this,function(r){return[2,e.isDirectory?fromDirEntry(e):fromFileEntry(e)]})})}function fromDirEntry(e){var r=e.createReader();return new Promise(function(n,a){var l=[];function c(){var u=this;r.readEntries(function(d){return __awaiter(u,void 0,void 0,function(){var m,E,C;return __generator(this,function(_){switch(_.label){case 0:if(d.length)return[3,5];_.label=1;case 1:return _.trys.push([1,3,,4]),[4,Promise.all(l)];case 2:return m=_.sent(),n(m),[3,4];case 3:return E=_.sent(),a(E),[3,4];case 4:return[3,6];case 5:C=Promise.all(d.map(fromEntry)),l.push(C),c(),_.label=6;case 6:return[2]}})})},function(d){a(d)})}c()})}function fromFileEntry(e){return __awaiter(this,void 0,void 0,function(){return __generator(this,function(r){return[2,new Promise(function(n,a){e.file(function(l){var c=toFileWithPath(l,e.fullPath);n(c)},function(l){a(l)})})]})})}var _default$1=function(e,r){if(e&&r){var n=Array.isArray(r)?r:r.split(","),a=e.name||"",l=(e.type||"").toLowerCase(),c=l.replace(/\/.*$/,"");return n.some(function(u){var d=u.trim().toLowerCase();return d.charAt(0)==="."?a.toLowerCase().endsWith(d):d.endsWith("/*")?c===d.replace(/\/.*$/,""):l===d})}return!0};function ownKeys$1(e,r){var n=Object.keys(e);if(Object.getOwnPropertySymbols){var a=Object.getOwnPropertySymbols(e);r&&(a=a.filter(function(l){return Object.getOwnPropertyDescriptor(e,l).enumerable})),n.push.apply(n,a)}return n}function _objectSpread$1(e){for(var r=1;r<arguments.length;r++){var n=arguments[r]!=null?arguments[r]:{};r%2?ownKeys$1(Object(n),!0).forEach(function(a){_defineProperty$1(e,a,n[a])}):Object.getOwnPropertyDescriptors?Object.defineProperties(e,Object.getOwnPropertyDescriptors(n)):ownKeys$1(Object(n)).forEach(function(a){Object.defineProperty(e,a,Object.getOwnPropertyDescriptor(n,a))})}return e}function _defineProperty$1(e,r,n){return r in e?Object.defineProperty(e,r,{value:n,enumerable:!0,configurable:!0,writable:!0}):e[r]=n,e}function _slicedToArray$1(e,r){return _arrayWithHoles$1(e)||_iterableToArrayLimit$1(e,r)||_unsupportedIterableToArray$1(e,r)||_nonIterableRest$1()}function _nonIterableRest$1(){throw new TypeError(`Invalid attempt to destructure non-iterable instance.
In order to be iterable, non-array objects must have a [Symbol.iterator]() method.`)}function _unsupportedIterableToArray$1(e,r){if(!!e){if(typeof e=="string")return _arrayLikeToArray$1(e,r);var n=Object.prototype.toString.call(e).slice(8,-1);if(n==="Object"&&e.constructor&&(n=e.constructor.name),n==="Map"||n==="Set")return Array.from(e);if(n==="Arguments"||/^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n))return _arrayLikeToArray$1(e,r)}}function _arrayLikeToArray$1(e,r){(r==null||r>e.length)&&(r=e.length);for(var n=0,a=new Array(r);n<r;n++)a[n]=e[n];return a}function _iterableToArrayLimit$1(e,r){var n=e==null?null:typeof Symbol!="undefined"&&e[Symbol.iterator]||e["@@iterator"];if(n!=null){var a=[],l=!0,c=!1,u,d;try{for(n=n.call(e);!(l=(u=n.next()).done)&&(a.push(u.value),!(r&&a.length===r));l=!0);}catch(m){c=!0,d=m}finally{try{!l&&n.return!=null&&n.return()}finally{if(c)throw d}}return a}}function _arrayWithHoles$1(e){if(Array.isArray(e))return e}var FILE_INVALID_TYPE="file-invalid-type",FILE_TOO_LARGE="file-too-large",FILE_TOO_SMALL="file-too-small",TOO_MANY_FILES="too-many-files",getInvalidTypeRejectionErr=function e(r){r=Array.isArray(r)&&r.length===1?r[0]:r;var n=Array.isArray(r)?"one of ".concat(r.join(", ")):r;return{code:FILE_INVALID_TYPE,message:"File type must be ".concat(n)}},getTooLargeRejectionErr=function e(r){return{code:FILE_TOO_LARGE,message:"File is larger than ".concat(r," ").concat(r===1?"byte":"bytes")}},getTooSmallRejectionErr=function e(r){return{code:FILE_TOO_SMALL,message:"File is smaller than ".concat(r," ").concat(r===1?"byte":"bytes")}},TOO_MANY_FILES_REJECTION={code:TOO_MANY_FILES,message:"Too many files"};function fileAccepted(e,r){var n=e.type==="application/x-moz-file"||_default$1(e,r);return[n,n?null:getInvalidTypeRejectionErr(r)]}function fileMatchSize(e,r,n){if(isDefined(e.size))if(isDefined(r)&&isDefined(n)){if(e.size>n)return[!1,getTooLargeRejectionErr(n)];if(e.size<r)return[!1,getTooSmallRejectionErr(r)]}else{if(isDefined(r)&&e.size<r)return[!1,getTooSmallRejectionErr(r)];if(isDefined(n)&&e.size>n)return[!1,getTooLargeRejectionErr(n)]}return[!0,null]}function isDefined(e){return e!=null}function allFilesAccepted(e){var r=e.files,n=e.accept,a=e.minSize,l=e.maxSize,c=e.multiple,u=e.maxFiles;return!c&&r.length>1||c&&u>=1&&r.length>u?!1:r.every(function(d){var m=fileAccepted(d,n),E=_slicedToArray$1(m,1),C=E[0],_=fileMatchSize(d,a,l),A=_slicedToArray$1(_,1),T=A[0];return C&&T})}function isPropagationStopped(e){return typeof e.isPropagationStopped=="function"?e.isPropagationStopped():typeof e.cancelBubble!="undefined"?e.cancelBubble:!1}function isEvtWithFiles(e){return e.dataTransfer?Array.prototype.some.call(e.dataTransfer.types,function(r){return r==="Files"||r==="application/x-moz-file"}):!!e.target&&!!e.target.files}function onDocumentDragOver(e){e.preventDefault()}function isIe(e){return e.indexOf("MSIE")!==-1||e.indexOf("Trident/")!==-1}function isEdge(e){return e.indexOf("Edge/")!==-1}function isIeOrEdge(){var e=arguments.length>0&&arguments[0]!==void 0?arguments[0]:window.navigator.userAgent;return isIe(e)||isEdge(e)}function composeEventHandlers(){for(var e=arguments.length,r=new Array(e),n=0;n<e;n++)r[n]=arguments[n];return function(a){for(var l=arguments.length,c=new Array(l>1?l-1:0),u=1;u<l;u++)c[u-1]=arguments[u];return r.some(function(d){return!isPropagationStopped(a)&&d&&d.apply(void 0,[a].concat(c)),isPropagationStopped(a)})}}function canUseFileSystemAccessAPI(){return"showOpenFilePicker"in window}function filePickerOptionsTypes(e){return e=typeof e=="string"?e.split(","):e,[{description:"everything",accept:Array.isArray(e)?e.filter(function(r){return r==="audio/*"||r==="video/*"||r==="image/*"||r==="text/*"||/\w+\/[-+.\w]+/g.test(r)}).reduce(function(r,n){return _objectSpread$1(_objectSpread$1({},r),{},_defineProperty$1({},n,[]))},{}):{}}]}function isAbort(e){return e instanceof DOMException&&(e.name==="AbortError"||e.code===e.ABORT_ERR)}function isSecurityError(e){return e instanceof DOMException&&(e.name==="SecurityError"||e.code===e.SECURITY_ERR)}var _excluded=["children"],_excluded2=["open"],_excluded3=["refKey","role","onKeyDown","onFocus","onBlur","onClick","onDragEnter","onDragOver","onDragLeave","onDrop"],_excluded4=["refKey","onChange","onClick"];function _toConsumableArray(e){return _arrayWithoutHoles(e)||_iterableToArray(e)||_unsupportedIterableToArray(e)||_nonIterableSpread()}function _nonIterableSpread(){throw new TypeError(`Invalid attempt to spread non-iterable instance.
In order to be iterable, non-array objects must have a [Symbol.iterator]() method.`)}function _iterableToArray(e){if(typeof Symbol!="undefined"&&e[Symbol.iterator]!=null||e["@@iterator"]!=null)return Array.from(e)}function _arrayWithoutHoles(e){if(Array.isArray(e))return _arrayLikeToArray(e)}function _slicedToArray(e,r){return _arrayWithHoles(e)||_iterableToArrayLimit(e,r)||_unsupportedIterableToArray(e,r)||_nonIterableRest()}function _nonIterableRest(){throw new TypeError(`Invalid attempt to destructure non-iterable instance.
In order to be iterable, non-array objects must have a [Symbol.iterator]() method.`)}function _unsupportedIterableToArray(e,r){if(!!e){if(typeof e=="string")return _arrayLikeToArray(e,r);var n=Object.prototype.toString.call(e).slice(8,-1);if(n==="Object"&&e.constructor&&(n=e.constructor.name),n==="Map"||n==="Set")return Array.from(e);if(n==="Arguments"||/^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n))return _arrayLikeToArray(e,r)}}function _arrayLikeToArray(e,r){(r==null||r>e.length)&&(r=e.length);for(var n=0,a=new Array(r);n<r;n++)a[n]=e[n];return a}function _iterableToArrayLimit(e,r){var n=e==null?null:typeof Symbol!="undefined"&&e[Symbol.iterator]||e["@@iterator"];if(n!=null){var a=[],l=!0,c=!1,u,d;try{for(n=n.call(e);!(l=(u=n.next()).done)&&(a.push(u.value),!(r&&a.length===r));l=!0);}catch(m){c=!0,d=m}finally{try{!l&&n.return!=null&&n.return()}finally{if(c)throw d}}return a}}function _arrayWithHoles(e){if(Array.isArray(e))return e}function ownKeys(e,r){var n=Object.keys(e);if(Object.getOwnPropertySymbols){var a=Object.getOwnPropertySymbols(e);r&&(a=a.filter(function(l){return Object.getOwnPropertyDescriptor(e,l).enumerable})),n.push.apply(n,a)}return n}function _objectSpread(e){for(var r=1;r<arguments.length;r++){var n=arguments[r]!=null?arguments[r]:{};r%2?ownKeys(Object(n),!0).forEach(function(a){_defineProperty(e,a,n[a])}):Object.getOwnPropertyDescriptors?Object.defineProperties(e,Object.getOwnPropertyDescriptors(n)):ownKeys(Object(n)).forEach(function(a){Object.defineProperty(e,a,Object.getOwnPropertyDescriptor(n,a))})}return e}function _defineProperty(e,r,n){return r in e?Object.defineProperty(e,r,{value:n,enumerable:!0,configurable:!0,writable:!0}):e[r]=n,e}function _objectWithoutProperties(e,r){if(e==null)return{};var n=_objectWithoutPropertiesLoose$1(e,r),a,l;if(Object.getOwnPropertySymbols){var c=Object.getOwnPropertySymbols(e);for(l=0;l<c.length;l++)a=c[l],!(r.indexOf(a)>=0)&&(!Object.prototype.propertyIsEnumerable.call(e,a)||(n[a]=e[a]))}return n}function _objectWithoutPropertiesLoose$1(e,r){if(e==null)return{};var n={},a=Object.keys(e),l,c;for(c=0;c<a.length;c++)l=a[c],!(r.indexOf(l)>=0)&&(n[l]=e[l]);return n}var Dropzone=react.exports.forwardRef(function(e,r){var n=e.children,a=_objectWithoutProperties(e,_excluded),l=useDropzone(a),c=l.open,u=_objectWithoutProperties(l,_excluded2);return react.exports.useImperativeHandle(r,function(){return{open:c}},[c]),jsx(Fragment,{children:n(_objectSpread(_objectSpread({},u),{},{open:c}))})});Dropzone.displayName="Dropzone";var defaultProps={disabled:!1,getFilesFromEvent:fromEvent,maxSize:1/0,minSize:0,multiple:!0,maxFiles:0,preventDropOnDocument:!0,noClick:!1,noKeyboard:!1,noDrag:!1,noDragEventsBubbling:!1,validator:null,useFsAccessApi:!0};Dropzone.defaultProps=defaultProps;Dropzone.propTypes={children:PropTypes.func,accept:PropTypes.oneOfType([PropTypes.string,PropTypes.arrayOf(PropTypes.string)]),multiple:PropTypes.bool,preventDropOnDocument:PropTypes.bool,noClick:PropTypes.bool,noKeyboard:PropTypes.bool,noDrag:PropTypes.bool,noDragEventsBubbling:PropTypes.bool,minSize:PropTypes.number,maxSize:PropTypes.number,maxFiles:PropTypes.number,disabled:PropTypes.bool,getFilesFromEvent:PropTypes.func,onFileDialogCancel:PropTypes.func,onFileDialogOpen:PropTypes.func,useFsAccessApi:PropTypes.bool,onDragEnter:PropTypes.func,onDragLeave:PropTypes.func,onDragOver:PropTypes.func,onDrop:PropTypes.func,onDropAccepted:PropTypes.func,onDropRejected:PropTypes.func,validator:PropTypes.func};var initialState={isFocused:!1,isFileDialogActive:!1,isDragActive:!1,isDragAccept:!1,isDragReject:!1,draggedFiles:[],acceptedFiles:[],fileRejections:[]};function useDropzone(){var e=arguments.length>0&&arguments[0]!==void 0?arguments[0]:{},r=_objectSpread(_objectSpread({},defaultProps),e),n=r.accept,a=r.disabled,l=r.getFilesFromEvent,c=r.maxSize,u=r.minSize,d=r.multiple,m=r.maxFiles,E=r.onDragEnter,C=r.onDragLeave,_=r.onDragOver,A=r.onDrop,T=r.onDropAccepted,S=r.onDropRejected,P=r.onFileDialogCancel,k=r.onFileDialogOpen,x=r.useFsAccessApi,b=r.preventDropOnDocument,g=r.noClick,y=r.noKeyboard,I=r.noDrag,M=r.noDragEventsBubbling,j=r.validator,O=react.exports.useMemo(function(){return typeof k=="function"?k:noop$1},[k]),L=react.exports.useMemo(function(){return typeof P=="function"?P:noop$1},[P]),N=react.exports.useRef(null),q=react.exports.useRef(null),V=react.exports.useReducer(reducer,initialState),Q=_slicedToArray(V,2),W=Q[0],G=Q[1],R=W.isFocused,D=W.isFileDialogActive,F=W.draggedFiles,$=react.exports.useRef(typeof window!="undefined"&&window.isSecureContext&&x&&canUseFileSystemAccessAPI()),U=function(){!$.current&&D&&setTimeout(function(){if(q.current){var le=q.current.files;le.length||(G({type:"closeDialog"}),L())}},300)};react.exports.useEffect(function(){return window.addEventListener("focus",U,!1),function(){window.removeEventListener("focus",U,!1)}},[q,D,L,$]);var z=react.exports.useRef([]),Y=function(le){N.current&&N.current.contains(le.target)||(le.preventDefault(),z.current=[])};react.exports.useEffect(function(){return b&&(document.addEventListener("dragover",onDocumentDragOver,!1),document.addEventListener("drop",Y,!1)),function(){b&&(document.removeEventListener("dragover",onDocumentDragOver),document.removeEventListener("drop",Y))}},[N,b]);var Z=react.exports.useCallback(function(re){re.preventDefault(),re.persist(),ne(re),z.current=[].concat(_toConsumableArray(z.current),[re.target]),isEvtWithFiles(re)&&Promise.resolve(l(re)).then(function(le){isPropagationStopped(re)&&!M||(G({draggedFiles:le,isDragActive:!0,type:"setDraggedFiles"}),E&&E(re))})},[l,E,M]),K=react.exports.useCallback(function(re){re.preventDefault(),re.persist(),ne(re);var le=isEvtWithFiles(re);if(le&&re.dataTransfer)try{re.dataTransfer.dropEffect="copy"}catch{}return le&&_&&_(re),!1},[_,M]),ue=react.exports.useCallback(function(re){re.preventDefault(),re.persist(),ne(re);var le=z.current.filter(function(ve){return N.current&&N.current.contains(ve)}),ye=le.indexOf(re.target);ye!==-1&&le.splice(ye,1),z.current=le,!(le.length>0)&&(G({isDragActive:!1,type:"setDraggedFiles",draggedFiles:[]}),isEvtWithFiles(re)&&C&&C(re))},[N,C,M]),ie=react.exports.useCallback(function(re,le){var ye=[],ve=[];re.forEach(function(be){var Ie=fileAccepted(be,n),Pe=_slicedToArray(Ie,2),we=Pe[0],Me=Pe[1],je=fileMatchSize(be,u,c),ke=_slicedToArray(je,2),Fe=ke[0],Oe=ke[1],De=j?j(be):null;if(we&&Fe&&!De)ye.push(be);else{var $e=[Me,Oe];De&&($e=$e.concat(De)),ve.push({file:be,errors:$e.filter(function(Re){return Re})})}}),(!d&&ye.length>1||d&&m>=1&&ye.length>m)&&(ye.forEach(function(be){ve.push({file:be,errors:[TOO_MANY_FILES_REJECTION]})}),ye.splice(0)),G({acceptedFiles:ye,fileRejections:ve,type:"setFiles"}),A&&A(ye,ve,le),ve.length>0&&S&&S(ve,le),ye.length>0&&T&&T(ye,le)},[G,d,n,u,c,m,A,T,S,j]),te=react.exports.useCallback(function(re){re.preventDefault(),re.persist(),ne(re),z.current=[],isEvtWithFiles(re)&&Promise.resolve(l(re)).then(function(le){isPropagationStopped(re)&&!M||ie(le,re)}),G({type:"reset"})},[l,ie,M]),X=react.exports.useCallback(function(){if($.current){G({type:"openDialog"}),O();var re={multiple:d,types:filePickerOptionsTypes(n)};window.showOpenFilePicker(re).then(function(le){return l(le)}).then(function(le){ie(le,null),G({type:"closeDialog"})}).catch(function(le){isAbort(le)?(L(le),G({type:"closeDialog"})):isSecurityError(le)&&($.current=!1,q.current&&(q.current.value=null,q.current.click()))});return}q.current&&(G({type:"openDialog"}),O(),q.current.value=null,q.current.click())},[G,O,L,x,ie,n,d]),ce=react.exports.useCallback(function(re){!N.current||!N.current.isEqualNode(re.target)||(re.keyCode===32||re.keyCode===13)&&(re.preventDefault(),X())},[N,X]),J=react.exports.useCallback(function(){G({type:"focus"})},[]),oe=react.exports.useCallback(function(){G({type:"blur"})},[]),de=react.exports.useCallback(function(){g||(isIeOrEdge()?setTimeout(X,0):X())},[g,X]),ee=function(le){return a?null:le},pe=function(le){return y?null:ee(le)},ae=function(le){return I?null:ee(le)},ne=function(le){M&&le.stopPropagation()},me=react.exports.useMemo(function(){return function(){var re=arguments.length>0&&arguments[0]!==void 0?arguments[0]:{},le=re.refKey,ye=le===void 0?"ref":le,ve=re.role,be=re.onKeyDown,Ie=re.onFocus,Pe=re.onBlur,we=re.onClick,Me=re.onDragEnter,je=re.onDragOver,ke=re.onDragLeave,Fe=re.onDrop,Oe=_objectWithoutProperties(re,_excluded3);return _objectSpread(_objectSpread(_defineProperty({onKeyDown:pe(composeEventHandlers(be,ce)),onFocus:pe(composeEventHandlers(Ie,J)),onBlur:pe(composeEventHandlers(Pe,oe)),onClick:ee(composeEventHandlers(we,de)),onDragEnter:ae(composeEventHandlers(Me,Z)),onDragOver:ae(composeEventHandlers(je,K)),onDragLeave:ae(composeEventHandlers(ke,ue)),onDrop:ae(composeEventHandlers(Fe,te)),role:typeof ve=="string"&&ve!==""?ve:"button"},ye,N),!a&&!y?{tabIndex:0}:{}),Oe)}},[N,ce,J,oe,de,Z,K,ue,te,y,I,a]),ge=react.exports.useCallback(function(re){re.stopPropagation()},[]),Te=react.exports.useMemo(function(){return function(){var re=arguments.length>0&&arguments[0]!==void 0?arguments[0]:{},le=re.refKey,ye=le===void 0?"ref":le,ve=re.onChange,be=re.onClick,Ie=_objectWithoutProperties(re,_excluded4),Pe=_defineProperty({accept:n,multiple:d,type:"file",style:{display:"none"},onChange:ee(composeEventHandlers(ve,te)),onClick:ee(composeEventHandlers(be,ge)),autoComplete:"off",tabIndex:-1},ye,q);return _objectSpread(_objectSpread({},Pe),Ie)}},[q,n,d,te,a]),fe=F.length,Ee=fe>0&&allFilesAccepted({files:F,accept:n,minSize:u,maxSize:c,multiple:d,maxFiles:m}),_e=fe>0&&!Ee;return _objectSpread(_objectSpread({},W),{},{isDragAccept:Ee,isDragReject:_e,isFocused:R&&!a,getRootProps:me,getInputProps:Te,rootRef:N,inputRef:q,open:ee(X)})}function reducer(e,r){switch(r.type){case"focus":return _objectSpread(_objectSpread({},e),{},{isFocused:!0});case"blur":return _objectSpread(_objectSpread({},e),{},{isFocused:!1});case"openDialog":return _objectSpread(_objectSpread({},initialState),{},{isFileDialogActive:!0});case"closeDialog":return _objectSpread(_objectSpread({},e),{},{isFileDialogActive:!1});case"setDraggedFiles":var n=r.isDragActive,a=r.draggedFiles;return _objectSpread(_objectSpread({},e),{},{draggedFiles:a,isDragActive:n});case"setFiles":return _objectSpread(_objectSpread({},e),{},{acceptedFiles:r.acceptedFiles,fileRejections:r.fileRejections});case"reset":return _objectSpread({},initialState);default:return e}}function noop$1(){}function _extends(){return _extends=Object.assign||function(e){for(var r=1;r<arguments.length;r++){var n=arguments[r];for(var a in n)Object.prototype.hasOwnProperty.call(n,a)&&(e[a]=n[a])}return e},_extends.apply(this,arguments)}function _objectWithoutPropertiesLoose(e,r){if(e==null)return{};var n={},a=Object.keys(e),l,c;for(c=0;c<a.length;c++)l=a[c],!(r.indexOf(l)>=0)&&(n[l]=e[l]);return n}var index$3=react.exports.useLayoutEffect,useLatest=function e(r){var n=react.exports.useRef(r);return index$3(function(){n.current=r}),n},updateRef=function e(r,n){if(typeof r=="function"){r(n);return}r.current=n},useComposedRef=function e(r,n){var a=react.exports.useRef();return react.exports.useCallback(function(l){r.current=l,a.current&&updateRef(a.current,null),a.current=n,!!n&&updateRef(n,l)},[n])},HIDDEN_TEXTAREA_STYLE={"min-height":"0","max-height":"none",height:"0",visibility:"hidden",overflow:"hidden",position:"absolute","z-index":"-1000",top:"0",right:"0"},forceHiddenStyles=function e(r){Object.keys(HIDDEN_TEXTAREA_STYLE).forEach(function(n){r.style.setProperty(n,HIDDEN_TEXTAREA_STYLE[n],"important")})},hiddenTextarea=null,getHeight=function e(r,n){var a=r.scrollHeight;return n.sizingStyle.boxSizing==="border-box"?a+n.borderSize:a-n.paddingSize};function calculateNodeHeight(e,r,n,a){n===void 0&&(n=1),a===void 0&&(a=1/0),hiddenTextarea||(hiddenTextarea=document.createElement("textarea"),hiddenTextarea.setAttribute("tabindex","-1"),hiddenTextarea.setAttribute("aria-hidden","true"),forceHiddenStyles(hiddenTextarea)),hiddenTextarea.parentNode===null&&document.body.appendChild(hiddenTextarea);var l=e.paddingSize,c=e.borderSize,u=e.sizingStyle,d=u.boxSizing;Object.keys(u).forEach(function(A){var T=A;hiddenTextarea.style[T]=u[T]}),forceHiddenStyles(hiddenTextarea),hiddenTextarea.value=r;var m=getHeight(hiddenTextarea,e);hiddenTextarea.value="x";var E=hiddenTextarea.scrollHeight-l,C=E*n;d==="border-box"&&(C=C+l+c),m=Math.max(C,m);var _=E*a;return d==="border-box"&&(_=_+l+c),m=Math.min(_,m),[m,E]}var noop=function e(){},pick=function e(r,n){return r.reduce(function(a,l){return a[l]=n[l],a},{})},SIZING_STYLE=["borderBottomWidth","borderLeftWidth","borderRightWidth","borderTopWidth","boxSizing","fontFamily","fontSize","fontStyle","fontWeight","letterSpacing","lineHeight","paddingBottom","paddingLeft","paddingRight","paddingTop","tabSize","textIndent","textRendering","textTransform","width","wordBreak"],isIE=!!document.documentElement.currentStyle,getSizingData=function e(r){var n=window.getComputedStyle(r);if(n===null)return null;var a=pick(SIZING_STYLE,n),l=a.boxSizing;if(l==="")return null;isIE&&l==="border-box"&&(a.width=parseFloat(a.width)+parseFloat(a.borderRightWidth)+parseFloat(a.borderLeftWidth)+parseFloat(a.paddingRight)+parseFloat(a.paddingLeft)+"px");var c=parseFloat(a.paddingBottom)+parseFloat(a.paddingTop),u=parseFloat(a.borderBottomWidth)+parseFloat(a.borderTopWidth);return{sizingStyle:a,paddingSize:c,borderSize:u}},useWindowResizeListener=function e(r){var n=useLatest(r);react.exports.useLayoutEffect(function(){var a=function(c){n.current(c)};return window.addEventListener("resize",a),function(){window.removeEventListener("resize",a)}},[])},TextareaAutosize=function e(r,n){var a=r.cacheMeasurements,l=r.maxRows,c=r.minRows,u=r.onChange,d=u===void 0?noop:u,m=r.onHeightChange,E=m===void 0?noop:m,C=_objectWithoutPropertiesLoose(r,["cacheMeasurements","maxRows","minRows","onChange","onHeightChange"]),_=C.value!==void 0,A=react.exports.useRef(null),T=useComposedRef(A,n),S=react.exports.useRef(0),P=react.exports.useRef(),k=function(){var g=A.current,y=a&&P.current?P.current:getSizingData(g);if(!!y){P.current=y;var I=calculateNodeHeight(y,g.value||g.placeholder||"x",c,l),M=I[0],j=I[1];S.current!==M&&(S.current=M,g.style.setProperty("height",M+"px","important"),E(M,{rowHeight:j}))}},x=function(g){_||k(),d(g)};return react.exports.useLayoutEffect(k),useWindowResizeListener(k),react.exports.createElement("textarea",_extends({},C,{onChange:x,ref:T}))},index$2=react.exports.forwardRef(TextareaAutosize),ResizeTextarea=index$2;gql`
  mutation updateMedia($id: ObjectID!, $input: UpdateMediaInput!)
  @api(name: "zim") {
    updateMedia(id: $id, input: $input) {
      id
      title
    }
  }
`;gql`
  mutation deleteMedia($id: ObjectID!) @api(name: "zim") {
    deleteMedia(id: $id)
  }
`;gql`
  mutation uploadImages($file: [Upload]) @api(name: "appZim") {
    uploadImages(file: $file)
  }
`;const SINGLE_UPLOAD=gql`
  mutation singleUpload($file: Upload!) @api(name: "chat") {
    singleUpload(file: $file) {
      id
      path
      fullUrl
      type
    }
  }
`,UPLOAD_EXAM_DOCUMENT=gql`
  mutation uploadExamDocument($file: Upload!, $folderName: String)
  @api(name: "chat") {
    uploadExamDocument(file: $file, folderName: $folderName) {
      id
      path
      fullUrl
      type
    }
  }
`,UPLOAD_S3=gql`
  mutation ($file: [Upload]!, $subfolder: SubfolderS3Enum!)
  @api(name: "appZim") {
    uploadS3(file: $file, subfolder: $subfolder)
  }
`,useUploadImage=({setMediaUrls:e})=>{const[r,{loading:n}]=useMutation(SINGLE_UPLOAD),a=react.exports.useCallback(async c=>{var d,m,E;const u=v4$1();try{await e.push({fileId:u,url:URL.createObjectURL(c),name:c.name,loading:!0,progress:0,type:((d=c==null?void 0:c.type)==null?void 0:d.includes("video"))?"video":((m=c==null?void 0:c.type)==null?void 0:m.includes("image"))?"image":"file"});const{data:C}=await r({variables:{file:c},context:{fetchOptions:{onUploadProgress:_=>{var A,T;e.update(S=>S.fileId===u,{fileId:u,url:URL.createObjectURL(c),name:c.name,loading:!0,progress:_.loaded/_.total,type:((A=c==null?void 0:c.type)==null?void 0:A.includes("video"))?"video":((T=c==null?void 0:c.type)==null?void 0:T.includes("image"))?"image":"file"})}}}});await e.update(_=>_.fileId===u,{fileId:C.singleUpload.id,name:C.singleUpload.path,url:URL.createObjectURL(c),type:c.type.includes("video")?"video":((E=c==null?void 0:c.type)==null?void 0:E.includes("image"))?"image":"file"})}catch(C){console.log(C)}},[r]);return{doUploadImage:react.exports.useCallback(async c=>{await Promise.all(c.map(a))},[a]),loading:n}};var useDeepCompareEffect$1={},useCustomCompareEffect$1={};Object.defineProperty(useCustomCompareEffect$1,"__esModule",{value:!0});var react_1$1=react.exports,useCustomCompareEffect=function(e,r,n){var a=react_1$1.useRef(void 0);(!a.current||!n(r,a.current))&&(a.current=r),react_1$1.useEffect(e,a.current)};useCustomCompareEffect$1.default=useCustomCompareEffect;var isDeepEqual={};Object.defineProperty(isDeepEqual,"__esModule",{value:!0});var tslib_1$1=require$$0,react_1=tslib_1$1.__importDefault(react$1);isDeepEqual.default=react_1.default;Object.defineProperty(useDeepCompareEffect$1,"__esModule",{value:!0});var tslib_1=require$$0,useCustomCompareEffect_1=tslib_1.__importDefault(useCustomCompareEffect$1),isDeepEqual_1=tslib_1.__importDefault(isDeepEqual),useDeepCompareEffect=function(e,r){useCustomCompareEffect_1.default(e,r,isDeepEqual_1.default)},_default=useDeepCompareEffect$1.default=useDeepCompareEffect;const EmojiPicker=loadable$2(()=>__vitePreload(()=>import("./index32.js").then(function(e){return e.i}),["assets/index32.js","assets/vendor.js"])),maxSize=1048576*2,EnterMessage=({conversationId:e,setConversationId:r,userId:n,group:a,textAreaRef:l,file:c,setFile:u,conversationDetail:d,isMyTyping:m})=>{var E,C,_,A;{const T=useToast(),[S]=useUser(),P=react.exports.useRef(null),k=useColorModeValue("gray.900","slate.300"),[x,b]=react.exports.useState(!1),[g,y]=react.exports.useState(!1),[I,M]=react.exports.useState(""),[j,O]=react.exports.useState(!1),{onSendMessage:L,onTyping:N}=useSendMessage({setContent:M,userId:n,group:a,setConversationId:r}),q=((C=(E=d==null?void 0:d.usersTyping)==null?void 0:E.filter(F=>F!==(S==null?void 0:S.id)))==null?void 0:C.length)>0,V=(A=(_=d==null?void 0:d.usersTyping)==null?void 0:_.filter(F=>F!==S.id))==null?void 0:A.map(F=>{var $;return($=d==null?void 0:d.participants.find(U=>U.userId===F))==null?void 0:$.fullName}),{doUploadImage:Q}=useUploadImage({setMediaUrls:u}),{getRootProps:W,getInputProps:G}=useDropzone({accept:["image/*","application/vnd.ms-excel","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet","application/vnd.openxmlformats-officedocument.presentationml.presentation","application/pdf","application/xhtml+xml","application/xml","application/msword","application/vnd.openxmlformats-officedocument.wordprocessingml.document","application/zip"],multiple:!1,onDrop:(F,$)=>{var Y;const U=($==null?void 0:$.length)>0,z=$.length>0&&((Y=$[0].errors)==null?void 0:Y.find(Z=>Z.code==="file-too-large"));U?T({title:"Th\u1EA5t b\u1EA1i!",description:z?ERROR_FILE_SIZE:ERROR_FILE_NOT_SUPPORT,status:"error",duration:3e3,isClosable:!0}):Q(F).then()},minSize:0,maxSize}),R=react.exports.useCallback(async F=>{const $=F.target.value.trim();if(F.key==="Enter"&&!F.shiftKey)($.length!==0||(c==null?void 0:c.length)>0)&&(j&&O(!1),await L({nativeEvent:{text:$},conversationId:e}),O(!1),M(""));else return!1},[e,I,j,c,l]),D=()=>g&&jsx(Textarea,{_focus:{outline:"none"},onFocus:()=>{b(!0)},onBlur:()=>{b(!1)},as:ResizeTextarea,style:{resize:"none"},ref:l,minRows:1,maxRows:5,rows:1,value:I,onChange:async F=>{F.target.value!==`
`&&await M(F.target.value)},border:"none",placeholder:"Nh\u1EADp n\u1ED9i dung ...",onKeyPress:R,color:k,bg:"transparent",mr:4});return _default(()=>{g&&async function(){x&&await N("typingOn",e),x||await N("typingOff",e)}()},[x,e,N,g,l]),react.exports.useEffect(()=>{let F;return m&&(F=setTimeout(()=>{b(!1)},15e3)),()=>{clearTimeout(F)}},[m]),react.exports.useEffect(()=>{y(!0)},[]),react.exports.useEffect(()=>{const F=$=>{P.current&&!P.current.contains($.target)&&O(!1)};return document.addEventListener("click",F,!0),()=>{document.removeEventListener("click",F,!0)}},[O]),jsxs(Box,{bg:useColorModeValue("white",""),position:"sticky",bottom:0,w:"100%",display:"flex",flexDirection:"column",borderTopWidth:1,borderColor:"var(--chakra-colors-whiteAlpha-300)",p:2,children:[q&&jsxs(HStack,{w:"50%",px:2,rounded:4,mb:2,children:[jsx(Text,{fontSize:13,color:useColorModeValue("gray.700","white"),children:V==null?void 0:V.map(F=>F||"Someone is Typing").join(",")}),jsx(Box,{display:"flex",alignItems:"center",justifyContent:"center",w:10,h:5,children:jsx(Typing,{})})]}),jsxs(HStack,{w:"full",children:[jsxs(Box,H(B({as:"div"},W({className:"dropzone"})),{children:[jsx("input",H(B({},G()),{style:{display:"none"},type:"file"})),jsx(BsImages,{color:useColorModeValue("#DA0037","white"),className:"w-5 h-5"})]})),jsxs(Box,{w:"100%",display:"flex",flexDirection:"row",borderWidth:1,rounded:"lg",borderColor:useColorModeValue("gray.300","var(--chakra-colors-whiteAlpha-300)"),p:2,children:[D(),jsxs(HStack,{display:"flex",alignItems:"center",children:[jsx(Box,{as:"button",onClick:()=>O(!j),children:jsx(BsEmojiSmile,{color:useColorModeValue("#DA0037","white"),className:"w-5 h-5 "})}),jsx(Box,{cursor:"pointer",pointerEvents:!I&&(c==null?void 0:c.length)===0?"none":"auto",onClick:async()=>{I.trim().length!==0||(c==null?void 0:c.length)>0?(j&&O(!1),await L({nativeEvent:{text:I},conversationId:e})):M("")},children:jsx(MdSend,{fill:useColorModeValue((I==null?void 0:I.length)>0||(c==null?void 0:c.length)>0?"#DA0037":"#686868","white"),className:"w-5 h-5"})})]})]}),j&&jsx(Box,{ref:P,color:"slate.900",children:jsx(EmojiPicker,{disableSkinTonePicker:!0,pickerStyle:{position:"absolute",bottom:"70px",right:"20px",overflow:"hidden",backgroundColor:"white",zIndex:"1000"},onEmojiClick:(F,$)=>{M(`${I} ${$.emoji}`)}})})]})]})}},useMarkDoneConversation=e=>{const r=useToast(),[n,{loading:a}]=useMutation(MARK_DONE_CONVERSATION);return{onMarkDone:react.exports.useCallback(async()=>{try{await n({variables:{conversationId:e}}),await r({title:"Th\xE0nh c\xF4ng.",description:"\u0110\xE3 chuy\u1EC3n user sang tr\u1EA1ng th\xE1i done",status:"success",duration:2e3,isClosable:!0})}catch(c){r({title:"L\u1ED7i !!!",description:c.message,status:"success",duration:2e3,isClosable:!0})}},[e,n,r]),loading:a}},InputSearch=n=>{var a=n,{onChange:e}=a,r=Ae(a,["onChange"]);const[l,c]=react.exports.useState(""),u=useDebounce$1(l,1e3);return react.exports.useEffect(()=>{e(u)},[u]),jsx(Input$1,H(B({},r),{value:l,onChange:d=>c(d.target.value)}))},ActionButton=e=>{const c=e,{Icon:r,onClick:n,disabled:a}=c,l=Ae(c,["Icon","onClick","disabled"]);return jsx(Box,H(B({_hover:{backgroundColor:"#0085E4"},as:"button",display:"flex",alignItems:"center",justifyContent:"center",width:10,rounded:"4px",h:"100%",onClick:n,disabled:a},l),{children:jsx(r,{})}))},BoxMessageHeader=({userInfo:e,groupInfo:r,loading:n,participants:a})=>{var M;const[l]=useUser(),[c,u]=react.exports.useState(!1),d=useStore(j=>j.searchStringMessage),m=useStore(j=>j.setSearchStringMessage),E=useStore(j=>j.visibleUserProfile),C=useStore(j=>j.conversationId),_=useStore(j=>j.setSearchResult),A=useStore(j=>j.setNearSearchResult),T=useStore(j=>j.setVisibleUserProfile),S=useStore(j=>j.setBeforeId),P=useStore(j=>j.setAfterId),k=useStore(j=>j.setMessageId),x=useBreakpointValue({base:"",xl:"visible"}),b=useBreakpointValue({base:"",md:"visible"}),{onMarkDone:g,loading:y}=useMarkDoneConversation(C);useSearchMessage({conversationId:C,search:d});const I=react.exports.useMemo(()=>n?"\u0110ang t\u1EA3i ...":(r==null?void 0:r.name)?r==null?void 0:r.name:(e==null?void 0:e.full_name)||(e==null?void 0:e.fullName)||"\u1EA8n danh",[n,r==null?void 0:r.name,e==null?void 0:e.full_name,e==null?void 0:e.fullName]);return jsxs(Box,{w:"100%",minH:"35px",maxH:"50px",display:"flex",flexDirection:"row",alignItems:"center",borderBottomWidth:1,pl:2,py:3,zIndex:1,children:[!c&&jsxs(HStack,{id:"js-toggle-profile",flex:1,alignItems:"center",cursor:"pointer",onClick:()=>!x&&T(!E),children:[e&&lodash.exports.isEmpty(r)?jsx(MyAvatar,{boxSize:"2em",src:e==null?void 0:e.avatar,name:I}):jsx(GroupAvatar,{groupInfo:r==null?void 0:r.participants}),jsx(Box,{flex:1,maxW:"300px",children:jsx(Text,{noOfLines:1,fontSize:14,lineHeight:"24px",fontWeight:"medium",color:useColorModeValue("gray.800","white"),textTransform:"capitalize",children:I})})]}),jsxs(HStack,{w:c?"full":"",h:"100%",pr:c?2:4,spacing:0,minH:10,children:[[6].includes(parseInt(l==null?void 0:l.roleId,10))&&(a==null?void 0:a.find(j=>j.userId===l.id.toString()))&&!c&&((e==null?void 0:e.isGuest)||((M=r==null?void 0:r.participants)==null?void 0:M.find(j=>j.isGuest)))&&jsx(ActionButton,{onClick:g,disabled:y,Icon:MdDoneOutline}),!c&&b&&jsx(ActionButton,{onClick:()=>{u(!0)},Icon:FiSearch}),c&&jsxs(InputGroup,{children:[jsx(InputSearch,{type:"text",placeholder:"Nh\u1EADp g\xEC \u0111\xF3 ...",onChange:m,pr:"4.5rem"}),jsx(InputRightElement,{width:"4.5rem",children:jsx(Button$1,{h:"1.75rem",size:"sm",onClick:()=>{u(!1),m(""),_([]),A([]),k(""),S(""),P("")},children:c?"Close":"Open"})})]})]})]})},useGetConversationDetail=(e,r)=>{var u;const{data:n,loading:a,error:l}=useQuery(CONVERSATION_DETAIL,{variables:pickBy_1({id:e,userId:e?"":r?r==null?void 0:r.toString():""},d=>d),skip:!e&&!r});return{conversationDetail:(u=n==null?void 0:n.conversationDetail)!=null?u:{},loading:a,error:l}},useGetUserInfoGraphNet=(e="",r)=>{var c;const{data:n,loading:a}=useQuery(GRAPH_NET_GET_USER_INFO,{variables:{userId:Number(e)},skip:r||!e||isNaN(Number(e))});return{userData:(c=n==null?void 0:n.getUserById)!=null?c:{},loading:a}},usePermissionForGroup=({conversationDetail:e})=>{var n,a,l,c;const[r]=useUser();return!((n=e==null?void 0:e.participants)==null?void 0:n.find(u=>u.userId===(r==null?void 0:r.id)))&&((a=e==null?void 0:e.participants)==null?void 0:a.length)>=2||((l=e==null?void 0:e.participants)==null?void 0:l.find(u=>u.userId===(r==null?void 0:r.id)))&&((c=e==null?void 0:e.participants)==null?void 0:c.length)>=3},sm="_sm_183st_1",base="_base_183st_6",md="_md_183st_10";var s={sm,base,md};const buttonSizes={sm:s.sm,base:s.base,md:s.md},buttonTypes={primary:"bg-red-700 border-red-700 dark:bg-red-700 hover:border-red-900 hover:bg-red-900 dark:hover:bg-red-900 dark:border-red-500  hover:text-white ring-red-400 text-white ",info:"bg-blue-500 border-blue-500 hover:border-blue-700 hover:bg-blue-700 hover:text-white ring-blue-400 text-white",white:"bg-white border-transparent hover:border-red-700 hover:bg-red-700 hover:text-white ring-red-400 text-black",warning:"bg-yellow-500 hover:bg-yellow-700 border-yellow-500 hover:border-yellow-700 ring-yellow-400 text-black",success:"bg-green-500 hover:bg-green-700 ring-green-400 text-white border-green-500 hover:border-green-700","primary-outline":"border border-red-700 hover:bg-red-700 hover:text-white ring-red-400 text-red-700 dark:text-red-300 dark:border-red-300 dark:hover:bg-red-300 dark:hover:text-slate-300","info-outline":"border border-blue-700 hover:bg-blue-700 hover:text-white ring-blue-400 text-blue-700","success-outline":"border border-green-700 hover:bg-green-700 hover:text-white ring-green-400 text-green-700","warning-outline":"border border-yellow-700 text-yellow-700 hover:bg-yellow-700 hover:text-black ring-yellow-400","black-outline":"border border-black hover:bg-gray-50 text-black hover:text-black ring-gray-700 dark:border-slate-500 dark:text-slate-300 dark:hover:text-slate-900",gray:"bg-gray-200 hover:bg-gray-600 ring-gray-400 text-gray-900 hover:text-white dark:bg-slate-600 dark:text-slate-300 dark:hover:bg-slate-700 dark:border-slate-700",ghost:"hover:bg-gray-100 ring-gray-400 border-transparent hover:border-transparent dark:bg-slate-600 dark:text-slate-300 dark:hover:bg-slate-700"},isOverrideClasses=(e,r)=>e.match(r),Button=react.exports.forwardRef((m,d)=>{var E=m,{className:e="",buttonType:r=buttonTypes.primary,buttonSize:n=buttonSizes.base,isLoading:a=!1,isDisabled:l=!1,children:c}=E,u=Ae(E,["className","buttonType","buttonSize","isLoading","isDisabled","children"]);const C=react.exports.useMemo(()=>buttonTypes[r],[r]),_=react.exports.useMemo(()=>buttonSizes[n],[n]),A=react.exports.useMemo(()=>a||l,[a,l]);return jsxs("button",H(B({disabled:l||a,ref:d,className:`whitespace-nowrap border inline-flex items-center justify-center transition-colors transition-background duration-300 ${isOverrideClasses(e,"p-")?"":"py-2 px-4"} rounded focus:outline-none ring-opacity-75  focus:ring ${isOverrideClasses(e,"text-")?"":"text-base"} font-medium ${A&&"opacity-60 cursor-not-allowed"} ${C} ${e} ${_}`},u),{children:[a&&jsxs("svg",{className:"animate-spin -ml-1 mr-3 h-5 w-5 text-current",xmlns:"http://www.w3.org/2000/svg",fill:"none",viewBox:"0 0 24 24",children:[jsx("circle",{className:"opacity-25",cx:"12",cy:"12",r:"10",stroke:"currentColor",strokeWidth:"4"}),jsx("path",{className:"opacity-100",fill:"currentColor",d:"M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"})]}),c]}))}),UploadFileContent=react.exports.memo(({showUploadFileDialog:e,file:r,setFile:n,setShowUploadFileDialog:a,onSendMessage:l})=>{const c=useResetQueries(),u=useStore(m=>m.conversationId),d=react.exports.useCallback(async()=>{await l({nativeEvent:{text:""},conversationId:u,file:r}),await c(["getAttachmentsInConversation"]),await n.clear()},[u,r]);return jsx(mt,{appear:!0,show:e,as:"div",enter:"transition-opacity duration-75",enterFrom:"opacity-0",enterTo:"opacity-100",leave:"transition-opacity duration-150",leaveFrom:"opacity-100",leaveTo:"opacity-0",children:jsx(Box,{p:2,zIndex:1e3,position:"absolute",bg:useColorModeValue("white","slate.600"),rounded:"lg",display:"flex",flexDirection:"column",justifyContent:"space-between",w:"50%",h:"40%",maxH:"300px",shadow:"md",className:"translate-y-[90%] translate-x-[50%] inset-0 ",children:r==null?void 0:r.map((m,E)=>jsxs(Flex,{flexDirection:"column",display:"flex",justifyContent:"space-between",w:"full",h:"100%",overflow:"hidden",rounded:"lg",children:[(m==null?void 0:m.type)!=="file"?jsx(Box,{maxH:"75%",children:jsx(Image$1,{w:"full",h:"full",src:m==null?void 0:m.url,alt:"file",objectFit:"contain"})}):jsx(Stack,{display:"flex",maxH:"75%",h:"full",alignItems:"center",justifyContent:"center",children:jsxs(VStack,{children:[jsx(Box,{w:14,h:14,rounded:"full",display:"flex",alignItems:"center",children:jsx(AiOutlineFileZip,{className:"w-[40px] h-[40px]"})}),jsx(Text,{textAlign:"center",flex:1,color:useColorModeValue("gray.800","white"),fontSize:14,children:getFilePathName(m==null?void 0:m.name)})]})}),jsx(Box,{onClick:()=>{n.removeAt(E),a(!1)},top:-2,right:-1,rounded:"full",cursor:"pointer",position:"absolute",zIndex:20,bg:useColorModeValue("white","#10172a"),children:jsx(XCircleIcon,{onClick:()=>{n.removeAt(E),a(!1)},width:32,className:"text-gray-400 hover:text-red-400"})}),jsxs(Stack,{display:"flex",flexDirection:"column",height:"25%",justifyContent:"flex-end",children:[jsx(Box,{h:"18%",w:"100%",children:jsx(Progress,{colorScheme:(m==null?void 0:m.progress)<1?"":"green",hasStripe:(m==null?void 0:m.progress)<1,mt:2,value:(m==null?void 0:m.progress)*100})}),jsx(Spacer,{}),jsx(Button,{disabled:(m==null?void 0:m.progress)<1,onClick:d,buttonType:"info",className:"w-full text-center py-2 rounded-lg text-white mt-2 max-h-[40px]",children:"G\u1EEDi"})]})]},E))})})}),getVisibleValue=(e=[],r)=>{if((e==null?void 0:e.length)<2)return!0;if((e==null?void 0:e.length)>=2)return!!(e==null?void 0:e.find(n=>n.userId===r))},BoxMessage=()=>{var g,y,I,M;const e=useStore(j=>j.channel),r=useStore(j=>j.userId),n=useStore(j=>j.conversationId),a=useStore(j=>j.setConversationId),l=useStore(j=>j.searchStringMessage),c=react.exports.useRef(null),[u]=useUser(),[d,m]=useList([]),[E,C]=react.exports.useState(!1),{userData:_,loading:A}=useGetUserInfoGraphNet(r,!r),{conversationDetail:T,loading:S}=useGetConversationDetail(n,r),P=((g=T==null?void 0:T.participants)==null?void 0:g.filter(j=>(j==null?void 0:j.userId)!==(u==null?void 0:u.id.toString()))[0])||_,k=usePermissionForGroup({conversationDetail:T}),{onSendMessage:x,loading:b}=useSendMessage({setContent:()=>{},userId:r,group:e,setConversationId:a});return react.exports.useEffect(()=>{(d==null?void 0:d.length)>0?C(!0):C(!1)},[d]),jsxs(Box,{bg:useColorModeValue("white","#10172a"),display:"flex",flexDirection:"column",flex:1,w:"full",position:"relative",h:"full",children:[(n||r)&&jsx(BoxMessageHeader,{participants:T==null?void 0:T.participants,userInfo:P,groupInfo:k&&n?{name:(T==null?void 0:T.name)?T==null?void 0:T.name:k&&n?(I=(y=T==null?void 0:T.participants)==null?void 0:y.map(j=>j.fullName))==null?void 0:I.join(","):"",participants:T==null?void 0:T.participants}:null,loading:A||S}),jsx(ChatContent,{isGroup:T==null?void 0:T.name,ownerId:u==null?void 0:u.id,conversationId:n,userId:r,onSendMessage:x,sendMessageLoading:b,participants:T==null?void 0:T.participants,setConversationId:a,channel:e}),jsx(UploadFileContent,{onSendMessage:x,file:d,setFile:m,showUploadFileDialog:E,setShowUploadFileDialog:C}),(getVisibleValue(T==null?void 0:T.participants,u==null?void 0:u.id)&&(n||r)||k&&ALLOW_EC_ROLE.includes(u==null?void 0:u.roleId))&&!l&&!S&&jsx(EnterMessage,{conversationDetail:T,file:d,setFile:m,textAreaRef:c,conversationId:n,setConversationId:a,userId:r,group:e,isMyTyping:(M=T==null?void 0:T.usersTyping)==null?void 0:M.includes(u==null?void 0:u.id)}),(d==null?void 0:d.length)>0&&jsx(Box,{opacity:.4,zIndex:10,position:"absolute",bg:useColorModeValue("black","slate.300"),className:"inset-0"})]})},AvatarComponent=({participantAvatarUrl:e,fullName:r="",onClose:n,loading:a})=>jsxs(Box,{flex:1,display:"flex",alignItems:"center",cursor:"pointer",onClick:()=>n?n():()=>{},children:[jsx(MyAvatar,{boxSize:"2em",src:e,name:r}),jsx(Text,{ml:2,flex:1,noOfLines:1,fontSize:14,lineHeight:"24px",fontWeight:"medium",color:useColorModeValue("gray.800","slate.300"),children:a?"\u0110ang t\u1EA3i ...":r||"\u1EA8n danh"})]}),REGEX_PASSWORD=/^(?=.*\d)(?=.*[a-zA-Z])[\da-zA-Z_.\-@]{8,}$/,REGEX_ONLY_NUMBER=/^\d+$/,REGEX_PHONE=/(84|03|05|07|08|09|01[2|6|8|9])+([0-9]{8})\b/,REGEX_LINK=/^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w.-]+)+[\w\-._~:/?#[\]@!$&'()*+,;=.]+$/gm,getFileExtension$1=e=>{if(typeof e!="string")return"";const r=e.split(".");return r.length>0?r[r.length-1]:""};addMethod(create,"password",function(e){return this.matches(REGEX_PASSWORD,{message:e,excludeEmptyString:!0})});addMethod(create,"onlyNumber",function(e){return this.matches(REGEX_ONLY_NUMBER,{message:e,excludeEmptyString:!0})});addMethod(create,"phone",function(e){return this.matches(REGEX_PHONE,{message:e,excludeEmptyString:!0})});addMethod(create$2,"fileSize",function(e,r){return this.test("fileSize",r,n=>Array.isArray(n)&&n.length>0&&n[0]?!n.map(l=>l.size>e):!0)});addMethod(create$2,"fileType",function(e=[],r){return this.test("fileType",r,n=>e.length===0?!0:Array.isArray(n)&&n.length>0&&n[0]?!n.map(l=>e.includes(l.type)):!0)});addMethod(create$2,"file",function({size:e,type:r},n){return this.test({name:"file",test:function(a){var u,d,m,E,C,_;const l=(u=r==null?void 0:r.value)!=null?u:[],c=(d=e==null?void 0:e.value)!=null?d:5*1024*1e3;if(Array.isArray(a)&&a.length>0&&a[0]){const A=a.find(S=>!l.includes(S.type)),T=a.find(S=>S.size>c);return A?this.createError({message:`${(m=r==null?void 0:r.message)!=null?m:"Kh\xF4ng h\u1ED7 tr\u1EE3 \u0111\u1ECBnh d\u1EA1ng"} ${getFileExtension$1((E=A==null?void 0:A.name)!=null?E:"")}`,path:this.path}):T?this.createError({message:(_=(C=e==null?void 0:e.message)!=null?C:`Dung l\u01B0\u1EE3ng v\u01B0\u1EE3t qu\xE1 ${c/1024e3}`+(T==null?void 0:T.name))!=null?_:"",path:this.path}):!0}else return!0}})});const schema$2=yup.object().shape({title:yup.string().required("Vui l\xF2ng nh\u1EADp ti\xEAu \u0111\u1EC1"),description:yup.string().min(10,"T\u1ED1i thi\u1EC3u 10 k\xFD t\u1EF1").required("Vui l\xF2ng nh\u1EADp m\xF4 t\u1EA3"),receiver:yup.mixed()}),TicketForm=({enableReceiver:e,users:r,onInputChange:n})=>{var u;const{register:a,formState:{errors:l},resetField:c}=useFormContext();return jsxs(Flex,{direction:"column",w:"full",justifyContent:"stretch",alignItems:"stretch",className:"reply-editor",gap:4,children:[jsx(TextInput,H(B({label:"Ti\xEAu \u0111\u1EC1",placeholder:"Nh\u1EADp..."},a("title")),{error:l==null?void 0:l.title})),e&&jsx(Controller,{name:"receiver",render:({field:d})=>jsx(Select$1,B({label:"Ng\u01B0\u1EDDi nh\u1EADn",options:r||[],getOptionLabel:m=>`${m.full_name} - ${m.phone}`,getOptionValue:m=>m.id+"",formatOptionLabel:m=>jsxs(HStack,{children:[jsx(Avatar,{src:m.avatar,objectFit:"cover",boxSize:6,flexShrink:0}),jsx(Text,{children:m.full_name})]}),error:l==null?void 0:l.receiver,onInputChange:n},d))}),jsx(Box,{children:jsxs(FormControl,{isInvalid:l==null?void 0:l.description,children:[jsx(Controller,{name:"description",render:({field:d})=>jsx(TipTap$1,B({showWordCount:!1,placeholder:"N\u1ED9i dung ticket..."},d))}),jsx(FormErrorMessage,{children:(u=l==null?void 0:l.description)==null?void 0:u.message})]})})]})},TicketFormModal=({onClose:e,isOpen:r,submitCallback:n=null})=>{const a=react.exports.useRef(),l=useForm({resolver:o$1(schema$2),mode:"onBlur",reValidateMode:"onChange"}),c=useToast();useRouter();const[u,d]=useSearchParams(),m=useResetQueries(),[E,C]=react.exports.useState(""),[_,A]=react.exports.useState(E);useDebounce$2(()=>{C(_)},500,[_]);const[T]=useUser(),S=T.roleId===6,[P,{loading:k}]=useMutation(CREATE_TICKET,{onCompleted:y=>{const{createTicket:I}=y;I&&c({title:"Th\xE0nh c\xF4ng!",description:"Ticket \u0111\xE3 \u0111\u01B0\u1EE3c g\u1EEDi.",status:"success",duration:3e3,isClosable:!0}),e(),n&&typeof n=="function"&&n(),m(["getTicketsPagination"])},onError:y=>{var I;c({title:"G\u1EEDi kh\xF4ng th\xE0nh c\xF4ng!",description:`${(I=y==null?void 0:y.message)!=null?I:"L\u1ED7i"}`,status:"error",duration:3e3,isClosable:!0})},notifyOnNetworkStatusChange:!0}),{data:x}=useQuery(GET_LIST_STUDENT_BY_EC_BY_FIELD(["avatar","full_name","phone"]),{variables:{input:{limit:100,q:E}},skip:!S}),b=x==null?void 0:x.getListStudentByEc.docs,g=react.exports.useCallback(async y=>{var M;if(S&&!(y==null?void 0:y.receiver)){l.setError("receiver",{type:"manual",message:"Vui l\xF2ng ch\u1ECDn ng\u01B0\u1EDDi nh\u1EADn"});return}const I={refId:EMPTY_GUID,title:y==null?void 0:y.title,description:y==null?void 0:y.description,receiverId:S?parseInt((M=y==null?void 0:y.receiver)==null?void 0:M.id):null};S||delete I.receiverId,await P({variables:{input:I}})},[S]);return react.exports.useEffect(()=>{if(r){const y=u.get("receiverId");if(!y||!(b==null?void 0:b.length))return;const I=b.find(M=>M.id===Number(y));l.reset({title:"",description:"",receiver:I||null})}else u.delete("receiverId"),d(u),l.reset({title:"",description:"",receiver:null})},[r,u,b]),jsx(FormProvider,H(B({},l),{children:jsxs(Modal$1,{finalFocusRef:a,isOpen:r,onClose:e,size:"3xl",children:[jsx(ModalOverlay,{}),jsxs(ModalContent,{children:[jsx(ModalHeader,{children:"T\u1EA1o ticket"}),jsx(ModalCloseButton,{}),jsxs("form",{onSubmit:l.handleSubmit(g),children:[jsx(ModalBody,{children:jsx(TicketForm,{enableReceiver:S,users:b,onInputChange:A})}),jsxs(ModalFooter,{children:[jsx(Button$1,{leftIcon:jsx(Icon,{as:AiOutlineSend}),colorScheme:"green",mr:4,type:"submit",isLoading:k,children:"G\u1EEDi ticket"}),jsx(Button$1,{variant:"ghost",onClick:e,children:"H\u1EE7y"})]})]})]})]})}))};function TagColor({color:e,onClick:r,isActive:n}){return jsx(Box,{borderWidth:n?3:1,borderColor:n?"white":"",as:"button",onClick:r,rounded:"sm",w:4,h:4,bg:`${e}`})}function TagForm({firstFieldRef:e,onClose:r}){var x,b;const n=useToast(),[a,l]=react.exports.useState("1"),[c,u]=react.exports.useState(null),d=useStore(g=>g.conversationId),[m,E]=react.exports.useState("#00DAA3"),[C,_]=react.exports.useState(""),{data:A}=useQuery(GET_TAGS,{skip:a==="2"}),[T,{loading:S}]=useMutation(CREATE_TAG_FOR_CONVERSATION),P=react.exports.useCallback(()=>{E("#00DAA3"),_(""),u(null)},[]),k=react.exports.useCallback(async()=>{var I;const g={conversationId:d,tagId:c==null?void 0:c.id},y={conversationId:d,name:C,color:m};try{await T({variables:(c==null?void 0:c.id)?g:y}),r(),P(),n({title:"Th\xE0nh c\xF4ng!",description:"G\u1EAFn tag th\xE0nh c\xF4ng",status:"success",duration:3e3,isClosable:!0})}catch(M){n({title:"G\u1EAFn tag kh\xF4ng th\xE0nh c\xF4ng!",description:`${(I=M==null?void 0:M.message)!=null?I:"L\u1ED7i"}`,status:"error",duration:3e3,isClosable:!0})}},[C,m,d,c,P]);return jsxs(Stack,{spacing:4,children:[jsx(RadioGroup,{defaultValue:"1",children:jsxs(HStack,{spacing:5,align:"center",children:[jsx(Radio,{isChecked:a==="1",colorScheme:"red",value:"1",onChange:()=>{l("1"),_(""),E("#00DAA3")},children:"Tag c\xF3 s\u1EB5n"}),jsx(Radio,{isChecked:a==="2",colorScheme:"green",value:"2",onChange:()=>{l("2"),u(null)},children:"Th\xEAm tag m\u1EDBi"})]})}),a==="1"&&jsx(Select$1,{value:c,label:"Tags",onChange:u,options:(b=(x=A==null?void 0:A.getListTag)==null?void 0:x.docs)!=null?b:[],getOptionLabel:g=>g.name,getOptionValue:g=>g.id+"",formatOptionLabel:g=>jsxs(HStack,{children:[jsx(Box,{bg:g.color,w:"5px",h:"5px",rounded:"full",mr:1}),jsx(Text,{children:g.name})]})}),a==="2"&&jsxs(Stack,{spacing:4,children:[jsx(RadioGroup,{onChange:E,value:m,children:jsx(HStack,{spacing:3,children:tagColor.map((g,y)=>{const I=g.color===m;return jsx(TagColor,{color:g.color,onClick:()=>E(g.color),isActive:I},g.color)})})}),jsx(TextInput,{label:"Tag name",id:"tag-name",ref:e,placeholder:"Nh\u1EADp t\xEAn tag",value:C,onChange:g=>_(g.target.value)})]}),jsxs(ButtonGroup,{d:"flex",justifyContent:"flex-end",children:[jsx(Button$1,{onClick:()=>{r(),P()},variant:"outline",children:"Cancel"}),jsx(Button$1,{onClick:k,disabled:!C&&!c||S,colorScheme:"teal",children:"Save"})]})]})}const RowInfo=({item:e})=>jsxs(HStack,{children:[jsx(Box,{w:6,h:6,p:1,rounded:"full",bg:useColorModeValue("gray.100","#1e293b"),overflow:"hidden",display:"flex",alignItems:"center",justifyContent:"center",children:e.icon}),typeof e.value=="function"?e==null?void 0:e.value():jsx(Text,{flex:1,fontSize:14,color:useColorModeValue("gray.800","slate.300"),noOfLines:2,children:e.value||"Kh\xF4ng c\xF3 th\xF4ng tin"})]}),ParticipantProfile=react.exports.memo(({participantInfo:e,participantAvatarUrl:r,fullName:n,loading:a,isStudent:l,tags:c,participants:u})=>{const{isOpen:d,onClose:m,onOpen:E}=useDisclosure(),C=t.useRef(null),[_]=useUser(),A=zimRolesNumber.map(b=>b.id),T=useStore(b=>b.setVisibleUserProfile),S=useStore(b=>b.conversationId),P=useParticipantDetail(e,c),k=useBreakpointValue({sm:"visible",xl:""}),x=react.exports.useCallback(()=>{T(!1)},[]);return!l&&jsxs(VStack,{alignItems:"stretch",spacing:2.5,px:4,py:2,borderBottomWidth:1,children:[jsxs(Stack,{spacing:2.5,children:[jsxs(HStack,{justifyContent:"space-between",children:[jsx(AvatarComponent,{onClose:x,participantAvatarUrl:r,fullName:n,loading:a}),k&&jsx(Box,{onClick:x,display:"flex",cursor:"pointer",w:6,h:6,alignItems:"center",justifyContent:"center",children:jsx(CgCloseR,{className:"w-full h-full"})})]}),P==null?void 0:P.filter(b=>b.visible).map((b,g)=>jsx(RowInfo,{item:b},`${b.value}-${g}`))]}),!ALLOW_STUDENT_ROLE.includes(_.roleId)&&S&&(u==null?void 0:u.find(b=>b.userId===(_==null?void 0:_.id.toString())))&&jsxs(HStack,{spacing:10,children:[jsx(Popover,{children:({onClose:b})=>jsxs(Fragment,{children:[jsx(PopoverTrigger,{children:jsx(Button$1,{bg:"transparent",_hover:{backgroundColor:"transparent"},_focus:{backgroundColor:"transparent"},_active:{backgroundColor:"transparent"},children:A.includes(_==null?void 0:_.roleId)&&jsx(AddButton,{iconContainerClassName:"bg-blue-500",label:"G\u1EAFn tag"})})}),jsx(Portal,{children:jsxs(PopoverContent,{zIndex:100,children:[jsx(PopoverArrow,{}),jsx(PopoverHeader,{children:"G\u1EAFn tag"}),jsxs(PopoverBody,{children:[jsx(PopoverArrow,{}),jsx(TagForm,{firstFieldRef:C,onClose:b})]})]})})]})}),(e==null?void 0:e.role_name)==="H\u1ECDc vi\xEAn"&&A.includes(_==null?void 0:_.roleId)&&S&&jsx(AddButton,{onClick:E,label:"T\u1EA1o ticket",iconContainerClassName:"bg-red-500"})]}),jsx(TicketFormModal,{onClose:m,isOpen:d})]})}),AddButton=({label:e="T\u1EA1o ticket",onClick:r,iconContainerClassName:n})=>jsxs(HStack,{onClick:r,display:"flex",alignItems:"center",cursor:"pointer",children:[jsx(Box,{className:n,w:5,h:5,rounded:"full",display:"flex",alignItems:"center",justifyContent:"center",children:jsx(IoMdAddCircleOutline,{color:"white"})}),jsx(Text,{fontWeight:"400",color:useColorModeValue("gray.800","slate.300"),children:e})]}),NoteItem=({createdAt:e,content:r,owner:n})=>{var a;return jsxs(HStack,{display:"flex",alignItems:"start",children:[jsx(MyAvatar,{size:"sm",src:n==null?void 0:n.avatar,name:n==null?void 0:n.fullName}),jsxs(VStack,{flex:1,alignItems:"stretch",spacing:1,children:[jsxs(HStack,{spacing:3,justifyContent:{base:"start","2xl":"space-between"},children:[jsx(Text,{noOfLines:1,fontSize:14,lineHeight:"24px",fontWeight:"medium",color:useColorModeValue("gray.800","slate.400"),maxW:{base:"90px","2xl":"120px"},children:(a=n==null?void 0:n.fullName)!=null?a:"Kh\xF4ng c\xF3"}),jsx(Text,{fontSize:12,lineHeight:"24px",className:"text-gray-400",children:dayjs(e).format("DD-MM-YYYY, HH:mm")})]}),jsx(Text,{fontSize:12,lineHeight:"16px",color:useColorModeValue("gray.800","slate.200"),children:r})]})]})},NoteInput=react.exports.memo(()=>{const e=react.exports.useRef(null),r=useStore(_=>_.conversationId),n=useResetQueries(),[a,l]=react.exports.useState(!1),[c,u]=react.exports.useState(""),[d]=useMutation(CREATE_NOTE),m=react.exports.useCallback(async()=>{try{await d({variables:{conversationId:r,content:c}}),await n(["notes"])}catch(_){console.log(_)}},[r,c]),E=react.exports.useCallback(async()=>{c.trim().length!==0&&(await m(),u(""))},[c]),C=react.exports.useCallback(async _=>{const A=_.target.value.trim();if(_.key==="Enter"&&!_.shiftKey)A.length!==0&&(m().then(),u(""));else return!1},[m]);return react.exports.useEffect(()=>{l(!0)},[]),jsxs(HStack,{w:"100%",borderWidth:1,rounded:"lg",borderColor:useColorModeValue("gray.300","whiteAlpha.300"),p:1,children:[a&&jsx(Textarea,{_focus:{outline:"none"},as:ResizeTextarea,style:{resize:"none"},ref:e,minRows:1,maxRows:5,rows:1,value:c,onChange:_=>{_.target.value!==`
`&&u(_.target.value)},border:"none",placeholder:"Nh\u1EADp n\u1ED9i dung ghi ch\xFA ...",onKeyPress:C,color:useColorModeValue("gray.900","slate.300"),bg:"transparent"}),jsx(Box,{cursor:"pointer",w:5,h:5,pointerEvents:c?"auto":"none",onClick:E,children:jsx(MdSend,{fill:useColorModeValue((c==null?void 0:c.length)>0?"#DA0037":"#686868","white")})})]})}),useGetNotes=({conversationId:e,userId:r})=>{var C,_,A;const{data:n,loading:a,fetchMore:l}=useQuery(GET_NOTES,{variables:pickBy_1({conversationId:e||"",userId:e?"":r?r==null?void 0:r.toString():"",limit:5},T=>T),skip:!e}),c=(C=n==null?void 0:n.notes)==null?void 0:C.docs,u=(_=n==null?void 0:n.notes)==null?void 0:_.page,d=(A=n==null?void 0:n.notes)==null?void 0:A.hasNextPage,{onLoadMore:m,isLoadingMore:E}=useLoadMore({variables:{conversationId:e,limit:5,page:u+1},fetchMore:l,hasNextPage:d});return react.exports.useMemo(()=>({notes:c,loading:a,onLoadMore:m,isLoadingMore:E,hasNextPage:d}),[c,a,m,E,d])};var perfectScrollbar="";const Notes=react.exports.memo(({isSuperAdmin:e,allowForSupperAdmin:r})=>{const n=useStore(_=>_.conversationId),a=useStore(_=>_.userId),l=react.exports.useRef(null),{notes:c,onLoadMore:u,hasNextPage:d,isLoadingMore:m}=useGetNotes({conversationId:n,userId:a}),E=react.exports.useCallback(async()=>{await u()},[u]),C=react.exports.useMemo(()=>c==null?void 0:c.map(_=>jsx(NoteItem,B({},_),_.id)),[c]);return react.exports.useEffect(()=>{let _;return(l==null?void 0:l.current)&&(_=new PerfectScrollbar(l==null?void 0:l.current,{wheelSpeed:1,minScrollbarLength:10})),()=>{_&&_.destroy()}},[]),jsxs(VStack,{alignItems:"stretch",spacing:2.5,px:4,py:2,children:[jsxs(Text,{fontSize:14,color:useColorModeValue("gray.800","slate.300"),children:["Notes ",c==null?void 0:c.length]}),jsx(VStack,{alignItems:"stretch",spacing:2.5,maxH:"200px",ref:l,className:"scroll-container",overflow:"hidden",position:"relative",minH:0,children:C}),d&&jsx(Button$1,{h:"32px",isLoading:m,loadingText:"\u0110ang t\u1EA3i th\xEAm",onClick:E,w:"100%",rounded:"none",borderWidth:"1px 0px 0px 0px",bg:useColorModeValue("white","slate.600"),fontSize:14,fontWeight:400,justifyContent:"center",children:"Xem th\xEAm"}),(!e||r)&&jsx(NoteInput,{})]})}),useGetAttachments=({conversationId:e,userId:r})=>{var A,T,S;const[n,a]=react.exports.useState(!1),l=H(B({},pickBy_1({conversationId:e||"",userId:e?"":r?r==null?void 0:r.toString():""},P=>P)),{offset:0}),{data:c,loading:u,fetchMore:d}=useQuery(GET_ATTACHMENTS,{variables:l}),m=(T=(A=c==null?void 0:c.getAttachmentsInConversation)==null?void 0:A.docs)==null?void 0:T.map(P=>P.attachments),E=[].concat.apply([],m),C=(S=c==null?void 0:c.getAttachmentsInConversation)==null?void 0:S.hasNextPage,_=react.exports.useCallback(async()=>{var P;!C||n||(await a(!0),await d({variables:H(B({},l),{offset:((P=c==null?void 0:c.getAttachmentsInConversation)==null?void 0:P.docs.length)+1}),updateQuery:(k,{fetchMoreResult:x})=>x?mergeDeep(k,x):k}),await a(!1))},[c,d,n,e]);return{attachments:E,loading:u,onLoadMore:_,isLoadingMore:n,hasNextPage:C}};function getFileExtension(e){let r=/^.+\.([^.]+)$/.exec(e);return r==null?"":r[1]}const MediaItem=react.exports.memo(({item:e,setAttachmentIndex:r,setShowImageViewer:n,index:a})=>{var d,m,E,C;const l=react.exports.useCallback(()=>{r(a),n(!0)},[a]),c=getFileExtension((d=e==null?void 0:e.attachment)==null?void 0:d.fullUrl),u=react.exports.useMemo(()=>{switch(c){case"xls":return RiFileExcel2Line;case"doc":return FaRegFileWord;default:return AiOutlineFileText}},[c]);return jsxs(Box,{minH:"80px",children:[(e==null?void 0:e.type)==="image"&&jsx(Image$1,{rounded:"md",onClick:l,cursor:"pointer",flex:1,objectFit:"contain",src:(m=e==null?void 0:e.attachment)==null?void 0:m.fullUrl,alt:"",h:"full"}),(e==null?void 0:e.type)==="file"&&jsx(Link$1,{target:"_blank",href:(E=e==null?void 0:e.attachment)==null?void 0:E.fullUrl,children:jsxs(Box,{"data-tip":"file-tooltip",flex:1,display:"flex",alignItems:"center",justifyContent:"center",bg:useColorModeValue("blue.50","slate.400"),rounded:"md",h:"full",children:[jsx(u,{className:"w-5 h-5 "}),jsx(ReactTooltip,{children:jsx("p",{children:getFilePathName((C=e==null?void 0:e.attachment)==null?void 0:C.path)}),place:"top",type:useColorModeValue("info","success"),effect:"float"})]})})]})}),Attachments=react.exports.memo(()=>{const e=useStore(_=>_.conversationId),r=useStore(_=>_.userId),[n,a]=react.exports.useState(null),[l,c]=react.exports.useState(!1),{attachments:u,isLoadingMore:d,onLoadMore:m,hasNextPage:E}=useGetAttachments({conversationId:e,userId:r}),C=react.exports.useCallback(async()=>{await m()},[m]);return jsxs(VStack,{alignItems:"stretch",spacing:2.5,py:2,children:[jsx(Accordion,{allowMultiple:!0,children:jsxs(AccordionItem,{children:[jsxs(HStack,{position:"relative",spacing:0,pl:4,_hover:{backgroundColor:useColorModeValue("#f0f0f0","slate.600")},children:[jsx(Box,{display:"flex",justifyContent:"start",flex:1,children:jsx(Text,{color:useColorModeValue("#444","white"),fontSize:16,fontWeight:"medium",children:"T\xE0i li\u1EC7u"})}),jsx(AccordionButton,{display:"flex",alignItems:"center",justifyContent:"center",w:10,children:jsx(AccordionIcon,{})})]}),jsxs(AccordionPanel,{pb:4,children:[(u==null?void 0:u.length)===0&&jsx(Text,{textAlign:"center",children:"Cu\u1ED9c tr\xF2 chuy\u1EC7n ch\u01B0a c\xF3 t\xE0i li\u1EC7u !!!"}),jsx(SimpleGrid,{columns:3,spacing:2,children:u==null?void 0:u.map((_,A)=>jsx(MediaItem,{item:_,setAttachmentIndex:a,setShowImageViewer:c,index:A}))}),E&&jsx(Button$1,{h:"32px",isLoading:d,loadingText:"\u0110ang t\u1EA3i th\xEAm",onClick:C,w:"100%",rounded:"none",borderWidth:"1px 0px 0px 0px",bg:useColorModeValue("white","slate.600"),fontSize:14,fontWeight:400,justifyContent:"center",mt:2,children:"Xem th\xEAm"})]})]})}),jsx(ImageViewer,{attachments:u,attachmentIndex:n,setAttachmentIndex:a,showImageViewer:l,setShowImageViewer:c})]})}),ParticipantInfoModal=({isOpen:e,onClose:r,userInfo:n,isOwner:a,groupId:l,isSupperAdmin:c,visibleRemoveMemberButton:u})=>{var P;const d=useResetQueries(),{isOpen:m,onOpen:E,onClose:C}=useDisclosure(),_=t.useRef(),[A,{loading:T}]=useMutation(REMOVE_MEMBER),S=react.exports.useCallback(async()=>{var k;try{await A({variables:{userId:(k=n==null?void 0:n.userId)==null?void 0:k.toString(),id:l}}),d(["conversationDetail"]),C(),r()}catch(x){console.log(x)}},[n,l]);return jsxs(Fragment,{children:[jsxs(Modal$1,{isOpen:e,onClose:r,children:[jsx(ModalOverlay,{}),jsxs(ModalContent,{children:[jsx(ModalHeader,{textAlign:"center",children:"Th\xF4ng tin th\xE0nh vi\xEAn"}),jsx(ModalCloseButton,{}),jsxs(ModalBody,{pb:4,children:[jsx(VStack,{p:4,pt:0,children:jsx(MyAvatar,{size:"2xl",src:n.avatar,name:n.fullName})}),jsx(VStack,{alignItems:"stretch",children:jsxs(HStack,{justifyContent:a||c?"space-between":"center",alignItems:"center",children:[jsxs(Text,{textAlign:a||c?"left":"center",fontSize:"xl",children:[n.fullName," ",`(${(P=ROLES.find(k=>k.id===Number(n==null?void 0:n.roleId)))==null?void 0:P.name})`]}),u&&jsx(Button$1,{onClick:E,size:"sm",colorScheme:"red",variant:"solid",fontSize:13,textColor:"white",children:"M\u1EDDi r\u1EDDi nh\xF3m"})]})})]})]})]}),jsx(AlertDialog,{isOpen:m,leastDestructiveRef:_,onClose:C,children:jsx(ModalOverlay,{children:jsxs(AlertDialogContent,{children:[jsx(ModalHeader,{fontSize:"lg",fontWeight:"bold",children:"C\u1EA3nh b\xE1o !!!"}),jsx(ModalBody,{children:"B\u1EA1n c\xF3 ch\u1EAFc kh\xF4ng? B\u1EA1n kh\xF4ng th\u1EC3 ho\xE0n t\xE1c h\xE0nh \u0111\u1ED9ng n\xE0y sau khi x\xF3a."}),jsxs(ModalFooter,{children:[jsx(Button$1,{disabled:T,ref:_,onClick:C,children:"Tr\u1EDF l\u1EA1i"}),jsx(Button$1,{disabled:T,colorScheme:"red",onClick:S,ml:3,color:"white",loadingText:"\u0110ang x\u1EED l\xFD",children:"X\xE1c nh\u1EADn"})]})]})})})]})},GroupInfo=react.exports.memo(({conversationDetail:e})=>{var T,S,P,k;const r=react.exports.useRef(null),n=useStore(x=>x.conversationId),[a]=useUser(),{isOpen:l,onClose:c,onOpen:u}=useDisclosure(),{isOpen:d,onClose:m,onOpen:E}=useDisclosure(),[C,_]=react.exports.useState(null),A=react.exports.useMemo(()=>{var x;return(x=e==null?void 0:e.participants)==null?void 0:x.map(b=>{var y,I;const g=((y=e==null?void 0:e.owner)==null?void 0:y.userId)===(b==null?void 0:b.userId);return jsx(UserItemInfo,H(B({},b),{isOwner:g,lastOnline:b==null?void 0:b.online,avatarUrl:b==null?void 0:b.avatar,username:b==null?void 0:b.fullName,positionName:(I=ROLES.find(M=>M.id===Number(b==null?void 0:b.roleId)))==null?void 0:I.name,onClick:async()=>{await _(b),await E()},px:0}),b.userId)})},[e]);return react.exports.useEffect(()=>{let x;return(r==null?void 0:r.current)&&(x=new PerfectScrollbar(r==null?void 0:r.current,{wheelSpeed:1,minScrollbarLength:10})),()=>{x&&x.destroy()}},[]),jsxs(VStack,{alignItems:"stretch",spacing:2.5,px:4,py:2,borderBottomWidth:1,children:[jsxs(HStack,{justifyContent:"space-between",alignItems:"center",children:[jsx(Text,{fontSize:14,color:useColorModeValue("gray.800","slate.300"),children:"Th\xE0nh vi\xEAn"}),((T=e==null?void 0:e.owner)==null?void 0:T.userId)===(a==null?void 0:a.id)&&jsx(Box,{onClick:u,as:"button",w:6,h:6,display:"flex",alignItems:"center",justifyContent:"center",children:jsx(MdGroupAdd,{className:"w-full h-full"})})]}),jsx(VStack,{alignItems:"stretch",spacing:0,maxH:"400px",ref:r,className:"scroll-container",overflow:"hidden",position:"relative",minH:0,children:A}),jsx(CreateConversationModal,{isChannel:!1,isAddSupporter:!1,isEdit:!0,isOpen:l,onClose:c,listParticipants:(S=e==null?void 0:e.participants)==null?void 0:S.map(x=>x==null?void 0:x.userId),name:e==null?void 0:e.name,groupId:n}),jsx(ParticipantInfoModal,{visibleRemoveMemberButton:a.id===((P=e==null?void 0:e.owner)==null?void 0:P.userId),isSupperAdmin:(a==null?void 0:a.roleId)===1,groupId:e==null?void 0:e.id,isOwner:((k=e==null?void 0:e.owner)==null?void 0:k.userId)===(C==null?void 0:C.userId),userInfo:C!=null?C:{},isOpen:d&&C,onClose:m})]})}),statusColorConfig={ongoing:{backgroundColor:"green.500"},incoming:{backgroundColor:"orange.400"},finished:{backgroundColor:"gray.300"}},CourseItem=({status:e,title:r})=>jsxs(HStack,{children:[jsx(Box,{w:2.5,h:2.5,rounded:"full",bg:statusColorConfig==null?void 0:statusColorConfig[e].backgroundColor}),jsx(Text,{flex:1,lineHeight:"20px",fontSize:14,color:useColorModeValue("#444","slate.300"),children:r})]}),Courses=react.exports.memo(({data:e})=>{const r=react.exports.useRef(null);react.exports.useEffect(()=>{let a;return(r==null?void 0:r.current)&&(a=new PerfectScrollbar(r==null?void 0:r.current,{wheelSpeed:1,minScrollbarLength:10})),()=>{a&&a.destroy()}},[]);const n=react.exports.useMemo(()=>e.map(a=>{var u;const{status:l,name:c}=(u=a==null?void 0:a.course)!=null?u:{};return react.exports.createElement(CourseItem,H(B({},a),{key:a==null?void 0:a.id,status:l,title:c}))}),[e]);return jsxs(VStack,{alignItems:"stretch",spacing:2.5,px:4,py:2,borderBottomWidth:1,maxH:"200px",ref:r,className:"scroll-container",overflow:"hidden",position:"relative",minH:0,children:[jsxs(Text,{fontSize:14,color:useColorModeValue("gray.800","slate.300"),children:["Kh\xF3a h\u1ECDc ",e.length]}),n]})}),LEAD_PERMISSION={create:[6],changeEC:[12,1,2],makeApointment:[6]},MOCK_TEST_PERMISSION={additionalTeacherOrCancelForStudent:[1,2],viewTableMockTestDetail:[1,2,6],canCreateOrEditMockTest:[1,2],assignTeacherAndEC:[1,2]},AutoResizeTextarea=t.forwardRef((e,r)=>{var n;return jsxs(FormControl,{isInvalid:!!(e==null?void 0:e.error),width:"100%",children:[(e==null?void 0:e.label)&&jsx(FormLabel,{htmlFor:e.id,children:e==null?void 0:e.label}),jsx(Textarea,B({minH:"unset",overflow:"hidden",w:"100%",resize:"none",ref:r,minRows:1,as:ResizeTextarea,transition:"height none",bg:useColorModeValue("white","slate.700")},e)),(e==null?void 0:e.error)&&jsx(FormErrorMessage,{children:(n=e==null?void 0:e.error)==null?void 0:n.message})]})}),useListSources=(e={},r={})=>{const n=useToast();return useQuery(GET_SOURCES,B({variables:e,onError:a=>{n({title:"L\u1EA5y d\u1EEF li\u1EC7u th\u1EA5t b\u1EA1i.",description:`GET list source status error: ${a==null?void 0:a.message}`,status:"error",duration:3e3,isClosable:!0})},fetchPolicy:"cache-first",errorPolicy:"all"},r))},CREATE_LEAD_NOTE=gql`
  mutation saveNote($rawId: Int, $note: String) @api(name: "appZim") {
    saveNote(rawId: $rawId, note: $note) {
      note
      createBy
      createDate
    }
  }
`,UPDATE_LEAD_STATUS=gql`
  mutation updateStatusLeads($input: updateStatusLeadsInputType)
  @api(name: "appZim") {
    updateStatusLeads(input: $input) {
      success
      error
      message
      dataString
    }
  }
`,CREATE_APOINTMENT=gql`
  mutation makeApointment($input: MakeApointmentInputType)
  @api(name: "appZim") {
    makeApointment(input: $input) {
      success
      error
      message
      dataString
    }
  }
`,UPDATE_LEAD_EC=gql`
  mutation changeEcLeads($input: ChangeEcLeadsInputType) @api(name: "appZim") {
    changeEcLeads(input: $input) {
      success
      error
      message
      dataString
    }
  }
`,CREATE_LEAD=gql`
  mutation addCustomerLeads($input: AddCustomerLeadInputType)
  @api(name: "appZim") {
    addCustomerLeads(input: $input) {
      success
      error
      message
      dataString
    }
  }
`,GET_LEAD_REPORT_DATA=gql`
  query getReportLeads($startDate: String, $endDate: String)
  @api(name: "appZim") {
    getReportLeads(startDate: $startDate, endDate: $endDate) {
      date
      totalView
      dataLeads {
        date
        sourseName
        data
      }
    }
  }
`,GET_LEADS=gql`
  query getRawDataPagination(
    $q: String
    $status: RawDataStatusEnum
    $fromDate: DateTime
    $toDate: DateTime
    $orderBy: String
    $order: String
    $page: Int
    $limit: Int
    $ecId: Int
    $schoolId: Int
    $tags: [String]
    $date: DateTime
    $sourceName: String
    $isOnlyDataFacebook: Boolean
  ) @api(name: "appZim") {
    getRawDataPagination(
      q: $q
      status: $status
      fromDate: $fromDate
      toDate: $toDate
      orderBy: $orderBy
      order: $order
      page: $page
      limit: $limit
      ecId: $ecId
      schoolId: $schoolId
      tags: $tags
      date: $date
      sourceName: $sourceName
      isOnlyDataFacebook: $isOnlyDataFacebook
    ) {
      hasNextPage
      hasPrevPage
      limit
      page
      pageNext
      pagePrev
      pagingCounter
      totalPages
      totalDocs
      docs {
        id
        student {
          id
          user_name
          full_name
          phone
          address
          email
          role_id
          role_name
          supporter
          student_more_info {
            identity_card_city_id
            note_home
            birthday
            city_id
            city_name
            district_id
            district_name
            ward_id
            ward_name
            street_id
            street_name
            home_number
            source_id
            source_name
            job_id
            job_name
            identity_card
            identity_card_city_name
            identity_card_date
            learning_status_id
            work_place
            learning_status_name
          }
        }
        fb_user_id
        source_id
        ec_name
        ec_id
        source_name
        form_name
        link
        note
        created_date
        appoinment_test_id
        status
        tags {
          level_name
          name
        }
      }
    }
  }
`,GET_LEAD_NOTE=gql`
  query getRawDataNote($rawId: Int) @api(name: "appZim") {
    getRawDataNote(rawId: $rawId) {
      note
      createBy
      createDate
    }
  }
`,GET_FB_COMMENT=gql`
  query facebookCommentsPagination(
    $page: Int
    $limit: Int
    $fbProfileId: String
    $postId: ObjectID
    $search: String
  ) @api(name: "crawler") {
    facebookCommentsPagination(
      page: $page
      limit: $limit
      search: $search
      postId: $postId
      fbProfileId: $fbProfileId
    ) {
      totalDocs
      totalPages
      docs {
        id
        post {
          id
          link
        }
        content
        facebookId
        createdAt
        updatedAt
        owner
        linkProfile
        inDemand
        fbProfileId
      }
    }
  }
`,GET_FB_POST=gql`
  query facebookPostsPagination(
    $page: Int
    $limit: Int
    $fbProfileId: String
    $search: String
  ) @api(name: "crawler") {
    facebookPostsPagination(
      page: $page
      limit: $limit
      search: $search
      fbProfileId: $fbProfileId
    ) {
      totalDocs
      totalPages
      docs {
        id
        content
        facebookId
        numOfComments
        numOfLikes
        numOfShares
        numOfCmtInDemand
        link
        featured
        createdAt
        updatedAt
        inDemand
        postOwnerId
        postOwnerName
      }
    }
  }
`,validateExist=async(e,r)=>{var n,a,l,c,u;if(e==="")return!1;try{const d=await apolloClient.query({query:GET_LEADS,variables:{limit:1,q:e,page:1}});return!(!((a=(n=d.data)==null?void 0:n.getRawDataPagination)==null?void 0:a.docs)||((u=(c=(l=d.data)==null?void 0:l.getRawDataPagination)==null?void 0:c.docs)==null?void 0:u.length)>0)}catch{return!1}},schema$1=yup.object().shape({phone:yup.string().matches(REGEX_PHONE,"S\u1ED1 \u0111i\u1EC7n tho\u1EA1i kh\xF4ng h\u1EE3p l\u1EC7").required("S\u1ED1 \u0111i\u1EC7n tho\u1EA1i l\xE0 b\u1EAFt bu\u1ED9c").test("phone","\u0110\xE3 c\xF3 EC ch\u0103m s\xF3c S\u0110T n\xE0y. Vui l\xF2ng ki\u1EC3m tra l\u1EA1i trong lead",validateExist),name:yup.string().required("Vui l\xF2ng nh\u1EADp h\u1ECD t\xEAn"),email:yup.string().email("\u0110\u1ECBa ch\u1EC9 email kh\xF4ng h\u1EE3p l\u1EC7").required("Vui l\xF2ng nh\u1EADp email"),source:yup.mixed().nullable(!0).required("M\u1EE5c n\xE0y l\xE0 b\u1EAFt bu\u1ED9c"),note:yup.string().required("Vui l\xF2ng nh\u1EADp ghi ch\xFA ")}),StyledWrapItem=e=>jsx(WrapItem,B({w:{base:"100%",md:"50%"},p:2},e)),CreateLead=({onClose:e,isOpen:r})=>{const{handleSubmit:n,register:a,control:l,formState:{errors:c},reset:u}=useForm({mode:"onSubmit",reValidateMode:"onBlur",resolver:o$1(schema$1)}),{data:d,loading:m}=useListSources(),E=useToast(),[C,{loading:_}]=useMutation(CREATE_LEAD,{refetchQueries:[GET_LEADS],onCompleted:({addCustomerLeads:T})=>{var S;T.success?(E({title:"Th\xE0nh c\xF4ng !",description:"T\u1EA1o lead th\xE0nh c\xF4ng !",status:"success",duration:3e3,isClosable:!0}),u(),e()):E({title:"T\u1EA1o lead th\u1EA5t b\u1EA1i !",description:(S=T==null?void 0:T.message)!=null?S:"Vui l\xF2ng ki\u1EC3m tra l\u1EA1i th\xF4ng tin \u0111\xE3 nh\u1EADp",status:"error",duration:3e3,isClosable:!0})},onError:T=>{var S;E({title:"T\u1EA1o lead th\u1EA5t b\u1EA1i !",description:(S=T==null?void 0:T.message)!=null?S:"Vui l\xF2ng ki\u1EC3m tra l\u1EA1i th\xF4ng tin",status:"error",duration:3e3,isClosable:!0})}});return jsxs(Modal$1,{isOpen:r,onClose:()=>{},size:"3xl",children:[jsx(ModalOverlay,{}),jsx(ModalContent,{children:jsxs("form",{onSubmit:n(async T=>{try{await C({variables:{input:{phoneNumber:T.phone,fullName:T.name,email:T.email,sourceId:T.source.id,note:T.note}}})}catch(S){console.log({e:S})}}),children:[jsx(ModalHeader,{children:"T\u1EA1o Lead m\u1EDBi"}),jsx(ModalCloseButton,{onClick:e}),jsxs(Box,{px:4,pb:8,children:[jsxs(Wrap,{spacing:0,children:[jsx(StyledWrapItem,{children:jsx(TextInput,H(B({label:"S\u1ED1 \u0111i\u1EC7n tho\u1EA1i (*)",placeholder:"Nh\u1EADp...",variant:"outline",pr:10},a("phone")),{error:c==null?void 0:c.phone}))}),jsx(StyledWrapItem,{children:jsx(TextInput,H(B({},a("name")),{label:"H\u1ECD t\xEAn (*)",placeholder:"Nh\u1EADp...",error:c==null?void 0:c.name}))}),jsx(StyledWrapItem,{children:jsx(TextInput,H(B({label:"Email (*)",placeholder:"Nh\u1EADp..."},a("email")),{error:c==null?void 0:c.email}))}),jsx(StyledWrapItem,{children:jsx(Controller,{name:"source",control:l,render:({field:T})=>jsx(Select$1,B({label:"Ngu\u1ED3n (*)",error:c==null?void 0:c.source,getOptionLabel:S=>S.sourceOfCustomer,getOptionValue:S=>S.id,options:d?d.getSourceOfCustomer:[],isLoading:m},T))})}),jsx(WrapItem,{w:"full",p:2,children:jsx(AutoResizeTextarea,H(B({label:"Ghi ch\xFA (*)",bg:"white",minRows:2,placeholder:"N\u1ED9i dung..."},a("note")),{error:c==null?void 0:c.note}))})]}),jsxs(HStack,{px:2,mt:4,spacing:4,children:[jsx(Button$1,{type:"submit",colorScheme:"green",isLoading:_,children:"T\u1EA1o m\u1EDBi"}),jsx(Button$1,{variant:"outline",onClick:e,children:"H\u1EE7y b\u1ECF"})]})]})]})})]})},ListUserInfoForAdmin=react.exports.memo(({conversationDetail:e})=>{const r=react.exports.useMemo(()=>{var n;return(n=e==null?void 0:e.participants)==null?void 0:n.map(a=>{var l;return jsx(UserItemInfo,{avatarUrl:a==null?void 0:a.avatar,username:a==null?void 0:a.fullName,positionName:(l=ROLES.find(c=>{var u;return c.id===Number((u=a.roleId)!=null?u:14)}))==null?void 0:l.name},a==null?void 0:a.userId)})},[e]);return jsx(Box,{borderBottomWidth:"1px",borderColor:"whiteAlpha.300",children:r})}),ManageParticipantDetail=()=>{var y,I,M,j,O,L,N,q,V,Q,W,G,R,D;const{onOpen:e,isOpen:r,onClose:n}=useDisclosure(),{isOpen:a,onOpen:l,onClose:c}=useDisclosure(),u=useStore(F=>F.userId),[d]=useUser(),m=useStore(F=>F.visibleUserProfile),E=useStore(F=>F.setVisibleUserProfile),C=useStore(F=>F.conversationId),{conversationDetail:_}=useGetConversationDetail(C,u),A=usePermissionForGroup({conversationDetail:_}),T=(y=_==null?void 0:_.participants)==null?void 0:y.find(F=>(F==null?void 0:F.roleId)==="5"||(F==null?void 0:F.roleId)===null),S=A?T:(I=_==null?void 0:_.participants)==null?void 0:I.filter(F=>(F==null?void 0:F.userId)!==(d==null?void 0:d.id.toString()))[0],{userData:P,loading:k}=useGetUserInfoGraphNet(u||(S==null?void 0:S.userId),_.name),x=A?(S==null?void 0:S.fullName)||((M=_==null?void 0:_.owner)==null?void 0:M.fullName):(P==null?void 0:P.full_name)?P==null?void 0:P.full_name:(S==null?void 0:S.isGuest)?`${S==null?void 0:S.fullName}`:S==null?void 0:S.fullName;useEventListener("click",F=>{const $=F.target;!$.closest(".profile")&&!$.closest("#js-toggle-profile")&&E(!1)});const g=useBreakpointValue({base:"",xl:"visible"});return!u&&!C?null:jsxs(Fragment,{children:[jsxs(Stack,{pos:g?void 0:"fixed",top:g?0:"56px",right:0,bottom:0,zIndex:51,h:"full",overflowY:"auto",bg:useColorModeValue("white","slate.900"),w:{base:"320px","2xl":"380px"},transform:g||m?"none":"translateX(100%)",transition:"all .3s ease",className:"profile",id:"profile-portal",spacing:0,children:[!(_==null?void 0:_.name)&&(((j=_==null?void 0:_.participants)==null?void 0:j.length)===2&&((O=_==null?void 0:_.participants)==null?void 0:O.find(F=>F.userId===d.id.toString()))||u||A&&ALLOW_EC_ROLE.includes(d==null?void 0:d.roleId))&&jsxs(Box,{children:[jsx(ParticipantProfile,{tags:(L=_==null?void 0:_.tags)!=null?L:[],isStudent:ALLOW_STUDENT_ROLE.includes(d==null?void 0:d.roleId),participantAvatarUrl:S==null?void 0:S.avatar,fullName:x,loading:k,participants:_==null?void 0:_.participants,participantInfo:P}),LEAD_PERMISSION.create.includes(d==null?void 0:d.roleId)&&(S==null?void 0:S.isGuest)&&jsxs(Box,{p:2,borderBottomWidth:1,borderColor:"whiteAlpha.300",children:[jsx(Button$1,{w:"full",colorScheme:"green",onClick:l,children:"Th\xEAm lead m\u1EDBi"}),LEAD_PERMISSION.create.includes(d==null?void 0:d.roleId)&&jsx(CreateLead,{isOpen:a,onClose:c})]}),(P==null?void 0:P.supporter)&&!ALLOW_STUDENT_ROLE.includes(d==null?void 0:d.roleId)&&((q=(N=_==null?void 0:_.participants)==null?void 0:N.filter(F=>F.roleId!=="5"))==null?void 0:q.map(F=>{var U;const $=(F==null?void 0:F.userId)===((U=P==null?void 0:P.supporter)==null?void 0:U.id.toString());return jsx(UserItemInfo,{username:F==null?void 0:F.fullName,positionName:`${$?"EC ch\xEDnh":"EC h\u1ED7 tr\u1EE3"}`},F==null?void 0:F.id)})),((V=P==null?void 0:P.supporter)==null?void 0:V.id)===Number(d==null?void 0:d.id)&&((Q=_==null?void 0:_.participants)==null?void 0:Q.length)&&jsx(Box,{w:"full",px:2,children:jsx(Button$1,{onClick:e,w:"full",rightIcon:jsx(TiUserAdd,{}),colorScheme:"blue",variant:"outline",children:"Th\xEAm EC h\u1ED7 tr\u1EE3"})})]}),A&&!(_==null?void 0:_.name)&&jsx(ListUserInfoForAdmin,{conversationDetail:_}),(_==null?void 0:_.name)&&jsx(GroupInfo,{conversationDetail:_}),C&&jsx(Notes,{isSuperAdmin:ALLOW_SUPER_ADMIN_ROLE.includes(d==null?void 0:d.roleId),allowForSupperAdmin:ALLOW_SUPER_ADMIN_ROLE.includes(d==null?void 0:d.roleId)&&((W=_==null?void 0:_.participants)==null?void 0:W.some(F=>(F==null?void 0:F.userId)===(d==null?void 0:d.id.toString())))}),((G=P==null?void 0:P.courseStudents)==null?void 0:G.length)>0&&!ALLOW_STUDENT_ROLE.includes(d==null?void 0:d.roleId)&&jsx(Courses,{data:(R=P==null?void 0:P.courseStudents)!=null?R:[]}),jsx(Attachments,{})]}),m&&jsx(Fragment,{children:jsx(Box,{bg:"blackAlpha.800",shadow:"md",pos:"fixed",top:14,bottom:0,zIndex:50,left:0,right:0,h:"screen"})}),jsx(CreateConversationModal,{isChannel:!1,isEdit:!0,isAddSupporter:!0,isOpen:r,onClose:n,listParticipants:(D=_==null?void 0:_.participants)==null?void 0:D.map(F=>F==null?void 0:F.userId),name:_==null?void 0:_.name,groupId:C})]})},useStore=create$1(e=>({conversationId:"",setConversationId:r=>e({conversationId:r}),userId:"",setUserId:r=>e({userId:r}),tab:"conversation",setTab:r=>e({tab:r}),channel:"",setChannel:r=>e({channel:r}),participants:[],setParticipants:r=>e({participants:r}),visibleUserProfile:!1,setVisibleUserProfile:r=>e({visibleUserProfile:r}),searchStringMessage:"",setSearchStringMessage:r=>e({searchStringMessage:r}),searchResult:[],setSearchResult:r=>e({searchResult:r}),nearSearchResult:[],setNearSearchResult:r=>e({nearSearchResult:r}),messageId:"",setMessageId:r=>e({messageId:r}),beforeId:"",setBeforeId:r=>e({beforeId:r}),afterId:"",setAfterId:r=>e({afterId:r})})),ChatPage=()=>{const e=useStore(S=>S.setChannel),r=useStore(S=>S.userId),n=useStore(S=>S.setUserId),a=useStore(S=>S.tab),l=useStore(S=>S.conversationId),c=useStore(S=>S.setConversationId),[u]=useUser(),[d,m]=useSearchParams({}),{height:E}=useGetWindowDimensions(),C=useBreakpointValue({base:"",md:"visible"});getEnviroment("ZIM_BUILD_MODE")==="module"&&useUpdateConversations();const _=d.get("qsConversationId"),A=d.get("qsUserId"),T=d.get("qsGroup");return react.exports.useEffect(()=>{(u==null?void 0:u.roleId)===6?e("guest"):e("my chats")},[u]),react.exports.useEffect(()=>{_||A?(_&&c(_),A&&n(A),T&&e(T)):(d.delete("qsConversationId"),d.delete("qsUserId"),d.delete("qsGroup"))},[_,A,T]),jsxs(VStack,{h:E-113,spacing:0,w:"full",children:[!C&&jsx(HeaderMobileChat,{}),jsxs(Box,{w:"100%",h:"100%",display:"flex",className:"divide-x",overflow:"hidden",children:[(C||!C&&a==="channel")&&jsx(ManageChannels,{}),(C||!C&&a==="conversation")&&jsx(MyListConversation,{}),(C||!C&&a==="message")&&jsx(BoxMessage,{}),(r||l)&&jsx(ManageParticipantDetail,{})]})]})};var index$1=Object.freeze({__proto__:null,[Symbol.toStringTag]:"Module",useStore,default:ChatPage});const useDeleteTagInConversation=()=>{const e=useToast(),r=useResetQueries(),n=useStore(u=>u.conversationId),[a,{loading:l}]=useMutation(DELETE_TAG_IN_CONVERSATION);return{onDeleteTag:react.exports.useCallback(async u=>{try{await a({variables:{tagId:u,conversationId:n}}),await r(["conversationDetail"])}catch(d){e({title:"L\u1ED7i !!!",description:d.message,status:"success",duration:2e3,isClosable:!0})}},[n,a,e,r]),loading:l}};function TagItem({id:e,name:r,color:n}){const{onDeleteTag:a,loading:l}=useDeleteTagInConversation(),c=useStore(u=>u.conversationId);return jsxs(Tag,{size:"md",rounded:"full",children:[jsx(Box,{bg:n,w:"5px",h:"5px",rounded:"full",mr:1}),jsx(TagLabel,{children:r}),l?jsx(Spinner,{color:"red.500",size:"xs",ml:2.5}):c&&jsx(TagCloseButton,{onClick:()=>a(e)})]})}const useParticipantDetail=(e,r)=>{const[n]=useUser();return react.exports.useMemo(()=>{var a,l;return[{icon:jsx(GoLocation,{}),value:(a=e==null?void 0:e.address)!=null?a:"Kh\xF4ng c\xF3 th\xF4ng tin",visible:ALLOW_VISIBLE_PROFILE_ROLE.includes(n.roleId)},{icon:jsx(BsTelephone,{}),value:(e==null?void 0:e.phone)||"Kh\xF4ng c\xF3 th\xF4ng tin",visible:ALLOW_VISIBLE_PROFILE_ROLE.includes(n.roleId)},{icon:jsx(FiFacebook,{}),value:((l=e==null?void 0:e.more_info)==null?void 0:l.facebook)||"Kh\xF4ng c\xF3 th\xF4ng tin",visible:ALLOW_VISIBLE_PROFILE_ROLE.includes(n.roleId)},{icon:jsx(FiMail,{}),value:(e==null?void 0:e.email)||"Kh\xF4ng c\xF3 th\xF4ng tin",visible:ALLOW_VISIBLE_PROFILE_ROLE.includes(n.roleId)},{icon:jsx(BsTags,{}),value:(r==null?void 0:r.length)?()=>jsx(Wrap,{flex:1,children:r==null?void 0:r.map((c,u)=>jsx(TagItem,B({},c),u))}):"Kh\xF4ng c\xF3 tag",visible:!0}]},[e,r,n])},zimRoles=["Admin","CC","CS","GV","EC","AM","KT","KTH","MK","BM","BP"],zimRolesNumber=[{id:1,name:"Super Admin"},{id:2,name:"CC"},{id:4,name:"Gi\xE1o vi\xEAn"},{id:6,name:"EC"},{id:8,name:"AM"},{id:11,name:"Marketer"},{id:12,name:"BM"}],ALLOW_SUPER_ADMIN_ROLE=[1],ALLOW_STUDENT_ROLE=[5],ALLOW_EC_ROLE=[5],ALLOW_VISIBLE_PROFILE_ROLE=[1,2,6],ERROR_FILE_SIZE="File c\u1EE7a b\u1EA1n v\u01B0\u1EE3t qu\xE1 10MB.",ERROR_FILE_NOT_SUPPORT="File c\u1EE7a b\u1EA1n kh\xF4ng \u0111\u01B0\u1EE3c h\u1ED7 tr\u1EE3.",tagColor=[{color:"#00DAA3"},{color:"#21A3F3"},{color:"#F51C50"},{color:"#006E7F"},{color:"#F8CB2E"}],ZimiansChannel=react.exports.memo(({searchString:e,overallMessageUnRead:r})=>{const[n]=useUser(),a=useStore(y=>y.setChannel),l=useStore(y=>y.setParticipants),c=useStore(y=>y.setConversationId),u=useStore(y=>y.setTab),d=useStore(y=>y.setUserId),[m,E]=useSearchParams({}),{listAccount:C,onLoadMore:_,hasNextPage:A,isLoadingMore:T,loading:S}=useGetListAccountChat({roles:zimRoles,searchString:e}),P=react.exports.useCallback(async()=>{await _()},[_]),k=react.exports.useMemo(()=>S?[1,2,3,4,5,6,7,8]:(C==null?void 0:C.filter(y=>{var I;return((I=y.user)==null?void 0:I.id)!==parseInt(n==null?void 0:n.id,10)}))||[],[n,S,C]),x=react.exports.useCallback(()=>{a("zimians"),u("conversation")},[]),b=react.exports.useCallback((y,I)=>{d(y),E({qsUserId:y,qsGroup:I}),m.delete("qsConversationId"),c(""),u("message"),a(I),l([])},[]),g=react.exports.useMemo(()=>jsx(ChannelItem,{canLoadMore:A,isLoading:T,onLoadMore:P,onClick:b,loading:S,conversations:k}),[k,S,A,T,b,P]);return jsx(MyAccordion,{onClick:x,title:"Zimians",overallMessageUnRead:r,children:g})}),useGetSupporterInfo=e=>{var a;const{data:r}=useQuery(GET_SUPPORTER_USER_INFO,{variables:{userId:e}}),n=r==null?void 0:r.getUserById;return{supporterInfo:n,id:(a=n==null?void 0:n.supporter)==null?void 0:a.id}},useGetListChannel=({search:e})=>{var E,C;const r=H(B({},pickBy_1({isChannel:!0,search:e},_=>_)),{limit:100,offset:0}),[n,{data:a,loading:l,fetchMore:c,called:u}]=useLazyQuery(CONVERSATIONS,{variables:r,fetchPolicy:"network-only",nextFetchPolicy:"cache-first"});react.exports.useEffect(()=>{u||(async()=>await n())()},[u,n]);const d=((E=a==null?void 0:a.conversations)==null?void 0:E.docs)||[],m=(C=a==null?void 0:a.conversations)==null?void 0:C.hasNextPage;return useLoadMore({variables:H(B({},r),{offset:(d==null?void 0:d.length)+1}),fetchMore:c,hasNextPage:m}),{listChannel:d,loading:l}},StudentChannel=react.exports.memo(({searchString:e,overallMessageUnRead:r})=>{const n=useStore(b=>b.setChannel),a=useStore(b=>b.setParticipants),l=useStore(b=>b.setConversationId),c=useStore(b=>b.setTab),u=useStore(b=>b.setUserId),[d,m]=useSearchParams({}),{listAccount:E,hasNextPage:C,isLoadingMore:_,onLoadMore:A,loading:T}=useGetListAccountChat({roles:["HV"],searchString:e}),S=react.exports.useMemo(()=>T?[1,2,3,4,5,6,7,8]:E,[T,E]),P=react.exports.useCallback(async()=>{await A()},[A]),k=react.exports.useCallback(()=>{n("student"),c("conversation")},[]),x=react.exports.useCallback((b,g)=>{u(b),m({qsUserId:b,qsGroup:g}),d.delete("qsConversationId"),l(""),c("message"),n(g),a([])},[]);return jsx(MyAccordion,{overallMessageUnRead:r,onClick:k,title:"Student",children:jsx(ChannelItem,{isLoading:_,canLoadMore:C,onLoadMore:P,onClick:x,loading:T,conversations:S})})}),NotiCount=react.exports.memo(({count:e})=>jsx(Badge$1,{w:6,h:6,variant:"solid",rounded:"full",bg:"blue.500",position:"absolute",right:8,alignItems:"center",justifyContent:"center",display:"flex",fontSize:11,children:jsx(Box,{alignItems:"center",justifyContent:"center",display:"flex",children:e>99?"99+":e})})),ManageChannels=react.exports.memo(()=>{var g,y;const[e]=useUser(),[r,n]=useSearchParams({}),a=useStore(I=>I.setParticipants),l=useStore(I=>I.setChannel),c=useStore(I=>I.setUserId),u=useStore(I=>I.setConversationId),d=useStore(I=>I.setTab),[m,E]=react.exports.useState(""),C=useDebounce$1(m,500),{overall:_,overallMyChats:A}=useGetOverallUnreadMessage(),{id:T}=useGetSupporterInfo(Number(e==null?void 0:e.id)),{listChannel:S}=useGetListChannel(m);S==null||S.filter(I=>I.name&&!["zimians","student"].includes(I.group));const P=react.exports.useMemo(()=>r,[]),k=react.exports.useCallback(async()=>{n({qsUserId:T}),P.delete("qsConversationId"),P.delete("qsGroup"),l("student"),c(T),d("message"),u(""),a([])},[T]),x=react.exports.useRef(null),b=react.exports.useCallback(()=>{l("guest"),d("conversation")},[]);return react.exports.useEffect(()=>{let I;return(x==null?void 0:x.current)&&(I=new PerfectScrollbar(x==null?void 0:x.current,{wheelSpeed:1,minScrollbarLength:10})),()=>{I&&I.destroy()}},[]),jsxs(Flex,{direction:"column",w:{base:"full",md:"200px",lg:"250px"},bg:useColorModeValue("white","#10172a"),h:"100%",children:[jsx(MyInfoSection,{searchString:m,setSearchString:E}),jsxs(Box,{flexGrow:1,ref:x,className:"scroll-container",overflow:"hidden",position:"relative",minH:0,children:[jsxs(Stack,{zIndex:50,spacing:0,position:"sticky",top:0,children:[(e==null?void 0:e.roleId)===5&&T&&jsx(Box,{display:"flex",alignItems:"center",px:4,h:10,cursor:"pointer",_hover:{backgroundColor:useColorModeValue("white","slate.500")},onClick:k,w:"100%",rounded:"none",borderWidth:"1px 0px 0px 0px",bg:useColorModeValue("white","#10172a"),fontSize:16,fontWeight:500,justifyContent:"start",children:jsx(Text,{children:"Chat v\u1EDBi supporter"})}),jsxs(Box,{display:"flex",alignItems:"center",px:4,h:10,cursor:"pointer",_hover:{backgroundColor:useColorModeValue("white","slate.500")},onClick:()=>{l("my chats"),d("conversation")},w:"100%",rounded:"none",borderWidth:"1px 0px 0px 0px",bg:useColorModeValue("white","#10172a"),fontSize:16,fontWeight:500,justifyContent:"start",children:["My Chats",A>0&&jsx(NotiCount,{count:A})]}),(e==null?void 0:e.roleId)!==4&&(e==null?void 0:e.roleId)!==5&&jsxs(Button$1,{display:"flex",alignItems:"center",px:4,h:10,cursor:"pointer",_hover:{backgroundColor:useColorModeValue("white","slate.500")},onClick:b,w:"100%",rounded:"none",borderWidth:"1px 0px 0px 0px",bg:useColorModeValue("white","#10172a"),fontSize:16,fontWeight:500,justifyContent:"start",children:["Guest",(_==null?void 0:_[3])+(_==null?void 0:_[4])>0&&jsx(NotiCount,{count:(_==null?void 0:_[3])+(_==null?void 0:_[4])})]})]}),jsxs(Stack,{display:{base:"block"},spacing:0,children:[(e==null?void 0:e.roleId)!==5&&jsx(ZimiansChannel,{overallMessageUnRead:(g=_==null?void 0:_[0])!=null?g:null,searchString:C}),jsx(StudentChannel,{overallMessageUnRead:(y=_==null?void 0:_[1])!=null?y:null,searchString:C})]})]})]})});function Header(){const{appState:{user:e},appSetLogout:r}=useAppContext(),n=useNavigate(),{colorMode:a,toggleColorMode:l}=useColorMode(),[c,u]=useToggle();getEnviroment("ZIM_BUILD_MODE")!=="module"&&useUpdateConversations();const{overallMyChats:d}=useGetOverallUnreadMessage(),m=_=>{_.preventDefault(),u()},E=_=>{const A=_.target;!A.closest(".main-menu")&&!A.closest("#js-toggle-menu")&&u(!1)},C=useBreakpointValue({base:"",md:"visible"});return useEventListener("click",E),jsxs(Fragment,{children:[jsx(Box,{zIndex:100,bg:useColorModeValue("white","slate.900"),px:4,borderBottom:"1px solid",borderBottomColor:useColorModeValue("gray.200","slate.800"),position:"sticky",top:0,children:jsxs(Flex,{h:14,alignItems:"center",justifyContent:"space-between",children:[jsxs(HStack,{cursor:"pointer",flexShrink:0,onClick:()=>n("/"),children:[jsx(Image$1,{src:logoSrc,objectFit:"contain",flexShrink:0,boxSize:"38px"}),jsx(Text,{fontSize:{lg:"xl",base:"md"},fontWeight:600,d:{sm:"none",md:"block"},children:"ZIM Academy"})]}),jsxs(Container,{maxW:"7xl",position:{base:"static",lg:"relative"},flexGrow:1,children:[jsxs(HStack,{spacing:4,flexGrow:1,children:[jsx(Button$1,{variant:"ghost",p:0,_hover:{backgroundColor:"transparent"},onClick:m,id:"js-toggle-menu",children:jsx(HamburgerIcon,{fontSize:32})}),jsx(Box,{flexGrow:1,children:jsx(SearchInput,{variant:"solid",bg:"gray.100"})})]}),jsx(Box,{children:jsx(Box,{position:"absolute",top:"100%",left:0,px:4,rounded:4,w:"full",id:"nav-menu",className:`${c?C:""} main-menu`,mt:2,children:jsx(Box,{bg:useColorModeValue("white","slate.800"),p:4,shadow:"sm",border:"1px solid",borderColor:useColorModeValue("gray.200","slate.700"),children:jsx(MegaMenu,{data:menuData,onClose:()=>u(!1)})})})})]}),jsx(Flex,{alignItems:"center",children:jsxs(Stack,{direction:"row",spacing:4,children:[jsxs(HStack,{spacing:1,children:[jsx(Button$1,{onClick:l,variant:"ghost",children:a==="light"?jsx(RiMoonClearLine,{}):jsx(SunIcon,{})}),jsxs(Button$1,{onClick:()=>n("/chat"),variant:"ghost",position:"relative",children:[jsx(Box,{position:"absolute",top:"-4px",right:"-32px",bg:"red",children:d>0&&jsx(NotiCount,{count:d})}),jsx(ChatIcon,{})]})]}),jsx(Box,{flexShrink:0,children:jsxs(Menu$1,{children:[jsx(MenuButton,{as:Button$1,rounded:"full",variant:"link",cursor:"pointer",minW:0,children:jsx(Avatar,{size:"sm",src:e==null?void 0:e.avatar})}),jsxs(MenuList,{alignItems:"center",py:4,children:[jsx(Center,{children:jsx(Avatar,{size:"xl",src:e==null?void 0:e.avatar})}),jsx(Center,{my:4,fontWeight:600,children:jsx("p",{children:e==null?void 0:e.fullName})}),jsx(MenuDivider,{}),jsx(MenuItem,{icon:jsx(Icon,{as:AiOutlineUser,w:4,h:4}),children:"T\xE0i kho\u1EA3n"}),jsx(MenuItem,{onClick:()=>r(),icon:jsx(Icon,{as:AiOutlineLogout,w:4,h:4}),children:"\u0110\u0103ng xu\u1EA5t"})]})]})})]})})]})}),jsx(Box,{w:"300px",shadow:"md",pos:"fixed",top:14,bottom:0,zIndex:101,h:"screen",overflowY:"auto",bg:useColorModeValue("white","slate.900"),className:"main-menu",display:{base:"block",md:"none"},transform:c?"none":"translateX(-100%)",transition:"all .3s ease",children:jsx(SideMenu,{data:menuData,onClose:()=>u(!1)})}),c&&jsx(Fragment,{children:jsx(Box,{bg:"blackAlpha.800",shadow:"md",pos:"fixed",top:14,bottom:0,zIndex:99,left:0,right:0,h:"screen"})})]})}const Breadcrumbs=({data:e=[]})=>jsx(Breadcrumb,{children:e&&e.map((r,n)=>jsx(BreadcrumbItem,{isCurrentPage:r.isActive,children:jsx(BreadcrumbLink,{as:Link$2,to:r.path,color:useColorModeValue("blue.500","blue.300"),children:r.breadcrumbText})},r.path+n))}),Home=loadable$2(()=>__vitePreload(()=>import("./index4.js"),["assets/index4.js","assets/vendor.js"])),ListCourse=loadable$2(()=>__vitePreload(()=>import("./index5.js"),["assets/index5.js","assets/vendor.js","assets/index6.js","assets/index7.js"])),CourseDetail=loadable$2(()=>__vitePreload(()=>import("./index8.js"),["assets/index8.js","assets/vendor.js","assets/useCreateOrUpdateCoursePlan.js","assets/useListSchools.js","assets/useListLevels.js","assets/useCourse.js","assets/index9.js","assets/test-utils.js","assets/index10.js"])),TicketDetailPage=loadable$2(()=>__vitePreload(()=>import("./index11.js"),["assets/index11.js","assets/vendor.js","assets/index9.js","assets/tickets.js"])),TicketListPage=loadable$2(()=>__vitePreload(()=>import("./index12.js"),["assets/index12.js","assets/tickets.js","assets/vendor.js","assets/index7.js","assets/index6.js"])),Chat=loadable$2(()=>__vitePreload(()=>Promise.resolve().then(function(){return index$1}),void 0)),LeadReportPage=loadable$2(()=>__vitePreload(()=>import("./index13.js"),["assets/index13.js","assets/index9.js","assets/vendor.js"])),AllSupportedUsersPage=loadable$2(()=>__vitePreload(()=>import("./index14.js"),["assets/index14.js","assets/vendor.js","assets/index7.js","assets/index6.js"])),ScheduleRegistration=loadable$2(()=>__vitePreload(()=>import("./index15.js"),["assets/index15.js","assets/index9.js","assets/vendor.js","assets/utils.js","assets/isSameOrAfter.js","assets/index16.js","assets/index6.js"])),MockTestOnlinePage=loadable$2(()=>__vitePreload(()=>import("./index17.js"),["assets/index17.js","assets/index18.js","assets/vendor.js","assets/index6.js","assets/groupByDate.js","assets/useListSubjectsOfMockTest.js","assets/index7.js"])),MockTestOfflinePage=loadable$2(()=>__vitePreload(()=>import("./index19.js"),["assets/index19.js","assets/index18.js","assets/vendor.js","assets/index6.js","assets/groupByDate.js","assets/useListSubjectsOfMockTest.js","assets/index7.js"])),MockTestDetailPage=loadable$2(()=>__vitePreload(()=>import("./index20.js").then(function(e){return e.i}),["assets/index20.js","assets/index6.js","assets/vendor.js","assets/groupByDate.js","assets/index16.js","assets/useGetIdOnRoute.js"])),ListSchedulesPage$1=loadable$2(()=>__vitePreload(()=>Promise.resolve().then(function(){return index}),void 0)),RegisterTestSchedulePage=loadable$2(()=>__vitePreload(()=>import("./index21.js"),["assets/index21.js","assets/index9.js","assets/vendor.js"])),LeadPage=loadable$2(()=>__vitePreload(()=>import("./index22.js"),["assets/index22.js","assets/vendor.js","assets/index6.js","assets/index7.js","assets/index24.js","assets/useListSchools.js","assets/index23.js","assets/test-utils.js"])),CoursePlannerPage=loadable$2(()=>__vitePreload(()=>import("./index25.js"),["assets/index25.js","assets/vendor.js","assets/index6.js","assets/index7.js","assets/useCreateOrUpdateCoursePlan.js","assets/useListSchools.js","assets/useListLevels.js","assets/generateCourseName.js","assets/useCourse.js","assets/index23.js","assets/index24.js"])),CoursePlanDetailPage=loadable$2(()=>__vitePreload(()=>import("./index10.js"),["assets/index10.js","assets/vendor.js","assets/useCreateOrUpdateCoursePlan.js","assets/useListSchools.js","assets/useListLevels.js","assets/index9.js"])),PaperBasedTestBank=loadable$2(()=>__vitePreload(()=>import("./index26.js"),["assets/index26.js","assets/index6.js","assets/vendor.js","assets/useListSubjectsOfMockTest.js","assets/index7.js"])),ExercisesPage=loadable$2(()=>__vitePreload(()=>import("./index27.js"),["assets/index27.js","assets/index6.js","assets/vendor.js","assets/CreateExerciseForm.js","assets/useListLevels.js","assets/index7.js"])),UpdateExercisePage=loadable$2(()=>__vitePreload(()=>import("./index28.js"),["assets/index28.js","assets/vendor.js","assets/useGetIdOnRoute.js","assets/CreateExerciseForm.js","assets/useListLevels.js"])),ListCoursePlan=loadable$2(()=>__vitePreload(()=>import("./index29.js"),["assets/index29.js","assets/vendor.js","assets/index6.js","assets/index7.js","assets/useCreateOrUpdateCoursePlan.js","assets/useListSchools.js","assets/useListLevels.js","assets/generateCourseName.js","assets/useCourse.js","assets/index23.js"])),routes=[{path:routePaths.home,exact:!0,component:Home,allowRoles:[],breadcrumbText:"Trang ch\u1EE7"},{path:`${routePaths.courseDetail}/:id`,exact:!0,component:CourseDetail,allowRoles:[],breadcrumbText:"Kh\xF3a h\u1ECDc"},{path:routePaths.listCourses,exact:!0,component:ListCourse,allowRoles:[],breadcrumbText:"Danh s\xE1ch kh\xF3a h\u1ECDc"},{path:`${routePaths.ticket}`,exact:!0,component:TicketListPage,allowRoles:[],breadcrumbText:"Danh s\xE1ch ticket"},{path:`${routePaths.ticket}/:id`,exact:!0,component:TicketDetailPage,allowRoles:[],breadcrumbText:"Chi ti\u1EBFt ticket"},{path:`${routePaths.leadReport}`,exact:!1,component:LeadReportPage,allowRoles:[],breadcrumbText:"Lead Report"},{path:`${routePaths.registerTestSchedule}`,exact:!1,component:RegisterTestSchedulePage,allowRoles:[],breadcrumbText:"\u0110\u0103ng k\xFD l\u1ECBch tr\u1EF1c test"},{path:`${routePaths.lead}`,exact:!1,component:LeadPage,allowRoles:[],breadcrumbText:"Ngu\u1ED3n kh\xE1ch h\xE0ng"},{path:`${routePaths.studentSupportByEc}`,exact:!1,component:AllSupportedUsersPage,allowRoles:[],breadcrumbText:"Danh s\xE1ch h\u1ECDc vi\xEAn"},{path:routePaths.chat,exact:!0,component:Chat,allowRoles:[],breadcrumbText:"Chat"},{path:routePaths.scheduleRegistration,exact:!0,component:ScheduleRegistration,allowRoles:[],breadcrumbText:"\u0110\u0103ng k\xFD l\u1ECBch l\xE0m vi\u1EC7c"},{path:routePaths.listSchedules,exact:!0,component:ListSchedulesPage$1,allowRoles:[],breadcrumbText:"Danh s\xE1ch l\u1ECBch l\xE0m vi\u1EC7c"},{path:routePaths.coursePlanner,exact:!0,component:CoursePlannerPage,allowRoles:[],breadcrumbText:"Course planner"},{path:`${routePaths.coursePlanDetail}/:id`,exact:!0,component:CoursePlanDetailPage,allowRoles:[],breadcrumbText:"Course plan detail"},{path:routePaths.paperBasedTestBank,exact:!0,component:PaperBasedTestBank,allowRoles:[],breadcrumbText:"Danh s\xE1ch \u0111\u1EC1 thi"},{path:routePaths.mockTestOnline,exact:!0,component:MockTestOnlinePage,allowRoles:[],breadcrumbText:"Mock test online"},{path:routePaths.mockTestOffline,exact:!0,component:MockTestOfflinePage,allowRoles:[],breadcrumbText:"Mock test offline"},{path:`${routePaths.mockTestDetail}/:id`,exact:!0,component:MockTestDetailPage,allowRoles:[],breadcrumbText:"Mock test detail"},{path:`${routePaths.listCoursesPlan}`,exact:!0,component:ListCoursePlan,allowRoles:[],breadcrumbText:"Kh\xF3a h\u1ECDc d\u1EF1 ki\u1EBFn"},{path:`${routePaths.exercises}`,exact:!0,component:ExercisesPage,allowRoles:[],breadcrumbText:"Danh s\xE1ch b\xE0i t\u1EADp"},{path:`${routePaths.updateExercise}/:id`,exact:!0,component:UpdateExercisePage,allowRoles:[],breadcrumbText:"C\u1EADp nh\u1EADt b\xE0i t\u1EADp"}];function Footer(){return jsx(Box,{bg:useColorModeValue("gray.50","gray.800"),color:useColorModeValue("gray.700","gray.200"),children:jsx(Container,{as:Stack,maxW:"full",py:2,direction:{base:"column",md:"row"},spacing:4,justify:{base:"center",md:"space-between"},align:{base:"center",md:"center"},children:jsx(Text,{children:"\xA9 2022 ZIM Academy"})})})}const Layout=({children:e,contentOnly:r=!1})=>{const{pathname:n}=useRouter(),a=useParams(),l=routes.filter(({path:c})=>n.includes(c)||matchPath(c,n)).map(d=>{var m=d,{path:c}=m,u=Ae(m,["path"]);return H(B({path:Object.keys(a).length?Object.keys(a).reduce((E,C)=>E.replace(`:${C}`,a==null?void 0:a[C]),c):c},u),{isActive:c===n})});return r?jsx(VStack,{w:"full",justifyContent:"stretch",alignItems:"stretch",spacing:0,minH:"full",bg:useColorModeValue("#f2f3f5","slate.900"),children:jsx(Box,{flexShrink:0,flexGrow:1,px:4,py:8,maxW:2560,className:"mx-auto",w:"full",children:e})}):jsxs(VStack,{w:"full",justifyContent:"stretch",alignItems:"stretch",spacing:0,minH:"full",bg:useColorModeValue("#f2f3f5","slate.900"),children:[jsx(Header,{}),jsx(Box,{bg:useColorModeValue("gray.50","slate.800"),px:4,py:2,children:(l==null?void 0:l.find(c=>c.path!=="/chat"&&c.path!=="/"))&&jsx(Breadcrumbs,{data:l})}),jsx(Box,{flexShrink:0,flexGrow:1,maxW:2560,className:"mx-auto",w:"full",children:e}),jsx(Footer,{})]})},ProtectedRoute=a=>{var l=a,{children:e,allowRoles:r=[]}=l,n=Ae(l,["children","allowRoles"]);const{appState:{isAuthenticated:c,user:u}}=useAppContext(),d=useLocation();if(c&&r.length>0&&u&&!r.includes(u==null?void 0:u.roleId))return jsx(Navigate,{to:"/error/403"});const m=d.search.replaceAll("?","");return c?e:jsx(Navigate,{to:`/auth/login?redirectUrl=${encodeURIComponent(`${d.pathname}?${m}`)}`,state:{from:d}})},useSignIn=()=>{const{appSetLogin:e}=useAppContext(),r=useResetQueries();return useMutation(SIGN_IN,{onCompleted:async n=>{n.signInV2&&(await e(n.signInV2.token),await r(["conversations"]))}})},schema=yup.object().shape({username:yup.string().required("Vui l\xF2ng nh\u1EADp t\xE0i kho\u1EA3n"),password:yup.string().required("Vui l\xF2ng nh\u1EADp m\u1EADt kh\u1EA9u")}),Login=()=>{var m,E;const{appState:e}=useAppContext(),{register:r,handleSubmit:n,formState:{errors:a}}=useForm({resolver:o$1(schema),mode:"onBlur",reValidateMode:"onChange"}),l=useRouter(),[c,{loading:u}]=useSignIn(),d=react.exports.useCallback(async C=>{const{username:_,password:A}=C;await c({variables:{username:_,password:A}})},[]);return e.isAuthenticated&&e.user?jsx(Navigate,{to:(E=decodeURIComponent((m=l.query)==null?void 0:m.redirectUrl))!=null?E:"/"}):jsx(Fragment,{children:jsxs(Stack,{minH:"100vh",direction:{base:"column",md:"row"},children:[jsx(Flex,{flex:1,children:jsx(Image$1,{alt:"Login Image",objectFit:"cover",src:"https://www.nikaiacours.fr/wp-content/uploads/2019/12/login-background.jpg"})}),jsx(Flex,{p:8,flex:1,align:"center",justify:"center",as:"form",onSubmit:n(d),children:jsxs(Stack,{spacing:4,w:"full",maxW:"md",children:[jsx(Heading,{fontSize:"2xl",children:"Sign in to your account"}),jsxs(FormControl,{id:"email",children:[jsx(FormLabel,{children:"Email address"}),jsx(Input$1,B({type:"text"},r("username")))]}),jsxs(FormControl,{id:"password",children:[jsx(FormLabel,{children:"Password"}),jsx(Input$1,B({type:"password"},r("password")))]}),jsxs(Stack,{spacing:6,children:[jsxs(Stack,{direction:{base:"column",sm:"row"},align:"start",justify:"space-between",children:[jsx(Checkbox,{children:"Remember me"}),jsx(Link$1,{color:"blue.500",as:Link$2,to:"/auth/forgot",children:"Forgot password?"})]}),jsx(Button$1,{isLoading:u,type:"submit",colorScheme:"blue",variant:"solid",children:"Sign in"})]})]})})]})})};function ErrorFallback({error:e,componentStack:r,resetErrorBoundary:n}){return jsxs(Alert,{role:"alert",status:"error",as:VStack,my:4,children:[jsxs(Code,{p:4,d:"block",children:[jsx("pre",{children:e.message}),jsx("pre",{children:r})]}),jsx(Button$1,{onClick:n,colorScheme:"facebook",children:"Try again"})]})}const dateToStringFormat=(e,r="DD/MM/YYYY")=>dayjs(e).isValid()?dayjs(e).format(r):"Ng\xE0y kh\xF4ng h\u1EE3p l\u1EC7",blankStateEventSelected={teachers:[],date:"",shift:{},type:0,registerScheduleId:0,requestsCancel:[],configFunction:{isAutoSchedule:!1,id:1},isReopenSchedule:!1,fieldReopenSchedule:{reopenScheduleId:0,type:0,students:[]}};var Components;(function(e){e.detail="detail",e.makeAnAppointment="makeAnAppointment",e.requestCancelSchedule="requestCancelSchedule",e.listCanceledSchedule="listCanceledSchedule",e.listRequestCancelSchedules="listRequestCancelSchedules"})(Components||(Components={}));var EnumRequestCancelStatus;(function(e){e.requested="requested",e.approval="approval",e.rejected="rejected"})(EnumRequestCancelStatus||(EnumRequestCancelStatus={}));const defaultColor="slate",toastDuration=15e3,RenderEvent=({eventInfo:e,eventClick:r,maxW:n="220px"})=>{var x,b;const{event:a}=e,l=`${dateToStringFormat(a.start,"HH:mm")} - ${dateToStringFormat(a.end,"HH:mm")}`,{teachers:c,bg:u=defaultColor,isFull:d,requestsCancel:m,isEmptyEvent:E,shift:C}=a.extendedProps,_=(c==null?void 0:c.length)>0?c==null?void 0:c.map(g=>g.full_name).join(", "):"-",A=(x=C.name)==null?void 0:x[0],T=d?"gray":u,S=(m==null?void 0:m.length)>0,P=(b=c==null?void 0:c.filter(g=>m.map(y=>y.teacherId).includes(g.id)))==null?void 0:b.map(g=>g.full_name).join(", "),k=E;return jsxs(HStack,{bg:`${T}.50`,w:"full",h:"45px",maxW:n,overflow:"hidden",rounded:"md",pos:"relative",as:"button",textAlign:"start",onClick:()=>r(e),disabled:k,cursor:k&&"not-allowed",children:[jsx(HStack,{align:"center",justify:"center",w:6,h:"full",flexShrink:0,bg:`${T}.700`,children:jsx(Text,{color:"white",children:A})}),jsxs(VStack,{spacing:0,align:"stretch",overflow:"hidden",w:"full",children:[jsxs(HStack,{justify:"space-between",pr:1,w:"full",children:[jsx(Text,{color:`${T}.800`,fontSize:"xs",children:l}),S&&jsx(Tooltip,{hasArrow:!0,label:`GV ${P} \u0111ang y\xEAu c\u1EA7u h\u1EE7y l\u1ECBch`,maxW:"200px",children:jsx(Box,{children:jsx(MdRefresh,{color:"#0085E4",size:18})})})]}),jsx(Text,{isTruncated:!0,color:useColorModeValue("black.900","slate.700"),fontSize:"xs",children:_})]})]})};dayjs.extend(customParseFormat);const stringToDate=(e,r="DD/MM/YYYY HH:mm")=>dayjs(e,r).toDate();dayjs.extend(customParseFormat);const dateAddTime=(e,r,n="DD/MM/YYYY "+r)=>stringToDate(dayjs(e).format(n)),generateMonthEvent=(e=[],r="",n=[],a=[])=>{const l=dayjs().daysInMonth(),c=e.map(m=>{const E=a.find(C=>C.id===m.type);return B({start:new Date(dateAddTime(m.date,m.shift.start_time)),end:new Date(dateAddTime(m.date,m.shift.end_time)),teachers:m.teachers,scheduleDetail:m.scheduleDetail,bg:E==null?void 0:E.color,date:m.date,eventDate:m.date},m)}),u=c.map(m=>({start:m.start,end:m.end}));let d=[];for(let m=1;m<=l;m+=1){const E=dayjs(r);n.map(C=>{const _=new Date(E.year(),E.month(),m,parseInt(C.start_time.split(":")[0]),parseInt(C.start_time.split(":")[1])),A=new Date(E.year(),E.month(),m,parseInt(C.end_time.split(":")[0]),parseInt(C.end_time.split(":")[1]));u.some(T=>dayjs(T.start).isSame(_)&&dayjs(T.end).isSame(A))||d.push({id:v4$1(),title:v4$1(),start:_,end:A,isEmptyEvent:!0,extendedProps:{schedule:null},shift:C,allDay:!1})})}return[...c,...d]},SCHEDULE_COPY_TYPE=gql`
  fragment AllScheduleCopyType on ScheduleCopyType {
    type
    id
    configFunction {
      isAutoSchedule
    }
    date
    shift {
      id
      start_time
      end_time
    }
    teachers {
      id
      full_name
    }
    school {
      id
      name
    }
  }
`;gql`
  query getTodoScheduleByEc($idEc: Int, $date: String) @api(name: "appZim") {
    getTodoScheduleByEc(idEc: $idEc, date: $date) {
      id
      type
      content
      customer {
        id
        role_id
        user_name
        full_name
        avatar
      }
      ec {
        id
        user_name
        full_name
        avatar
      }
      instructor {
        id
        role_id
        user_name
        full_name
        avatar
      }
      file_attached
      link
      start_time
      end_time
      date
      is_delete
      create_by
      create_date
      update_by
      update_date
    }
  }
`;const GET_TYPE_REGISTER=gql`
  query getConfigFunctionByType($type: EnumConfigFunctionType!)
  @api(name: "appZim") {
    getConfigFunctionByType(type: $type) {
      id
      name
      type
      description
      timePerShift
      color
      isAutoSchedule
      shiftType
    }
  }
`,GET_LIST_REGISTER_SCHEDULE_OF_TEACHER=gql`
  query getListRegisterSchedule(
    $fromDate: CustomDateInputType
    $toDate: CustomDateInputType
  ) @api(name: "appZim") {
    getListRegisterSchedule(fromDate: $fromDate, toDate: $toDate) {
      id
      type
      date
      shift {
        id
        start_time
        end_time
      }
      isFull
      requestsCancel {
        teacherId
        id
        reason
      }
      school {
        id
        name
      }
      configFunction {
        autoCancel
      }
    }
  }
`,GET_FUNCTION_ASSIGNED_TEACHER=gql`
  query ($teacherId: CustomUserInputType!) @api(name: "appZim") {
    getFunctionAssignedTeacher(teacherId: $teacherId) {
      id
      name
      color
      isAutoSchedule
      shiftType
      needToCenter
    }
  }
`,GET_LIST_REGISTER_SCHEDULE=gql`
  query getListRegisterSchedule(
    $fromDate: CustomDateInputType
    $toDate: CustomDateInputType
    $type: Int
    $schoolId: Int
    $teacherId: CustomUserInputType
    $status: EnumStatusScheduleType
  ) @api(name: "appZim") {
    getListRegisterSchedule(
      fromDate: $fromDate
      toDate: $toDate
      type: $type
      schoolId: $schoolId
      teacherId: $teacherId
      status: $status
    ) {
      id
      type
      date
      shift {
        id
        start_time
        end_time
        name
      }
      school {
        id
        name
      }
      teachers {
        id
        full_name
      }
      requestsCancel {
        teacherId
        id
        reason
      }
      configFunction {
        isAutoSchedule
        timePerShift
        id
        color
        name
      }
      isFull
    }
  }
`,CHECK_LIST_COPY_SCHEDULE=gql`
  query checkListCopySchedule($input: CopyRegisterScheduleInputType)
  @api(name: "appZim") {
    checkListCopySchedule(input: $input) {
      scheduleConflict {
        ...AllScheduleCopyType
      }
      scheduleApprove {
        ...AllScheduleCopyType
      }
    }
  }
  ${SCHEDULE_COPY_TYPE}
`,GET_LIST_SCHEDULE_BY_STATUS=gql`
  query getListScheduleByStatus(
    $fromDate: String
    $toDate: String
    $type: Int
    $teacherId: CustomUserInputType
    $schoolId: Int
    $status: EnumStatusScheduleType
  ) @api(name: "appZim") {
    getListScheduleByStatus(
      fromDate: $fromDate
      toDate: $toDate
      type: $type
      teacherId: $teacherId
      schoolId: $schoolId
      status: $status
    ) {
      id
      type
      date
      configFunction {
        id
        isAutoSchedule
      }
      teacher {
        id
        full_name
      }
      students {
        id
        full_name
      }
      startTime
      endTime
      description
      roomId
      linkMeeting
    }
  }
`;gql`
  query getRequestCancelByStatus(
    $fromDate: String
    $toDate: String
    $type: Int
    $teacherId: CustomUserInputType
    $schoolId: Int
    $status: EnumStatusScheduleType
  ) @api(name: "appZim") {
    getRequestCancelByStatus(
      fromDate: $fromDate
      toDate: $toDate
      type: $type
      teacherId: $teacherId
      schoolId: $schoolId
      status: $status
    ) {
      id
      type
      date
    }
  }
`;const ListSchedulesContext=t.createContext({eventSelected:{teachers:[],date:"",shift:{},registerScheduleId:0,requestsCancel:[],configFunction:{isAutoSchedule:!1,id:1},isReopenSchedule:!1,fieldReopenSchedule:{reopenScheduleId:0,type:0,students:[]}},events:[],setEvents:e=>{},setEventSelected:e=>{},typeRegisters:[],leftComponent:"",setLeftComponent:e=>{},dataScheduleInShift:{},setDataScheduleInShift:e=>{},onCloseDrawer:()=>{}}),useListScheduleContext=()=>t.useContext(ListSchedulesContext),MyBox=({title:e,children:r})=>jsxs(VStack,{py:2,align:"stretch",h:"full",children:[jsx(Text,{fontSize:"md",fontWeight:600,mx:4,children:e}),jsx(Divider,{h:"1px",bg:useColorModeValue("gray.400","gray.700")}),jsx(VStack,{px:4,align:"stretch",h:"full",children:r})]}),Shift=c=>{var u=c,{title:e,students:r,isBooked:n,onClick:a}=u,l=Ae(u,["title","students","isBooked","onClick"]);var m;const d=(m=r==null?void 0:r.map(E=>E.full_name).join(", "))!=null?m:"-";return jsxs(VStack,H(B({align:"stretch",borderWidth:1,borderColor:n?"transparent":useColorModeValue("black","white"),py:1,px:2,bg:n?useColorModeValue("gray.100","slate.600"):useColorModeValue("white","slate.700"),rounded:4,onClick:a,overflow:"hidden",textAlign:"left"},l),{cursor:n?"pointer":l==null?void 0:l.cursor,children:[jsx(Text,{fontSize:"sm",children:e}),jsx(Text,{fontSize:"sm",minW:0,isTruncated:!0,children:d})]}))},GET_TEACHER_ASSIGNED_FOR_FUNCTION=gql`
  query getTeacherAssignedForFunction(
    $functionId: Int!
    $registerScheduleId: Int!
    $registerDetailId: [Int]!
  ) @api(name: "appZim") {
    getTeacherAssignedForFunction(
      functionId: $functionId
      registerScheduleId: $registerScheduleId
      registerDetailId: $registerDetailId
    ) {
      teachersAvailable {
        id
        full_name
      }
      teachersBusy {
        id
        full_name
      }
      teachersUnregistered {
        id
        full_name
      }
    }
  }
`,CustomMenuList=r=>{var e=Ae(r,[]);return jsx(components$1.MenuList,H(B({},e),{children:e.children}))};function arrayToSelectOption(e,r,n,a=!0){return e.map(l=>a?H(B({},l),{label:l[r],id:l[n]}):{label:l[r],id:l[n]})}const CREATE_REGISTER_SCHEDULE=gql`
  mutation createRegisterSchedule($input: RegisterScheduleInputCreateType)
  @api(name: "appZim") {
    createRegisterSchedule(input: $input) {
      success
      error
      message
      dataString
    }
  }
`,CANCEL_REGISTER_SCHEDULE=gql`
  mutation cancelRegisterSchedule($input: CancelRegisterInputType)
  @api(name: "appZim") {
    cancelRegisterSchedule(input: $input) {
      success
      error
      message
    }
  }
`,CREATE_OR_UPDATE_REGISTER_SCHEDULE_DETAIL=gql`
  mutation createOrUpdateRegisterScheduleDetail(
    $input: CreateRegisterScheduleDetailInputType
  ) @api(name: "appZim") {
    createOrUpdateRegisterScheduleDetail(input: $input) {
      success
      error
      message
    }
  }
`,APPROVE_OR_REJECT_REQUEST_CANCEL=gql`
  mutation approveOrRejectRequestCancel(
    $input: RejectRequestCancelScheduleInputType
  ) @api(name: "appZim") {
    approveOrRejectRequestCancel(input: $input) {
      success
      message
      dataString
    }
  }
`,CHANGE_SCHEDULE_FOR_TEACHER=gql`
  mutation changeScheduleForTeacher($input: ChangeScheduleForTeacherInputType)
  @api(name: "appZim") {
    changeScheduleForTeacher(input: $input) {
      success
      error
      message
    }
  }
`,CANCEL_SCHEDULE_DETAIL=gql`
  mutation cancelScheduleDetail($input: ScheduleDetailInputType)
  @api(name: "appZim") {
    cancelScheduleDetail(input: $input) {
      message
      success
      dataString
      error
    }
  }
`,COPY_REGISTER_SCHEDULE=gql`
  mutation copyRegisterSchedule($input: CopyRegisterScheduleInputType)
  @api(name: "appZim") {
    copyRegisterSchedule(input: $input) {
      success
      message
    }
  }
`,REOPEN_SCHEDULE_DETAIL=gql`
  mutation reopenScheduleDetail($input: ReopenScheduleDetailInputType)
  @api(name: "appZim") {
    reopenScheduleDetail(input: $input) {
      success
      error
    }
  }
`,FORCE_CANCEL_REGISTER_SCHEDULE=gql`
  mutation forceCancelRegisterSchedule($input: CancelRegisterInputType)
  @api(name: "appZim") {
    forceCancelRegisterSchedule(input: $input) {
      success
      message
    }
  }
`,ConfirmCancelSchedule=({isOpen:e,onClose:r,onConfirm:n=()=>{}})=>jsxs(Modal$1,{isOpen:e,onClose:r,isCentered:!0,children:[jsx(ModalOverlay,{}),jsxs(ModalContent,{p:4,children:[jsx(ModalCloseButton,{}),jsx(Text,{fontWeight:600,fontSize:"lg",children:"Th\xF4ng b\xE1o"}),jsx(Text,{children:"B\u1EA1n c\xF3 ch\u1EAFc ch\u1EAFn mu\u1ED1n h\u1EE7y l\u1ECBch n\xE0y kh\xF4ng?"}),jsx(ModalFooter,{mt:4,p:0,children:jsxs(HStack,{children:[jsx(Button$1,{colorScheme:"gray",onClick:r,children:"\u0110\xF3ng"}),jsx(Button$1,{colorScheme:"blue",onClick:()=>n(),children:"X\xE1c nh\u1EADn"})]})})]})]}),GET_SHIFT_REGISTER_SCHEDULE=gql`
  query ($type: EnumShiftsType) @api(name: "appZim") {
    getShiftRegisterSchedule(type: $type) {
      id
      start_time
      end_time
      name
    }
  }
`;gql`
  query shifts($q: String, $status: String, $shift_minute: Int)
  @api(name: "appZim") {
    shifts(q: $q, status: $status, shift_minute: $shift_minute) {
      id
      start_time
      end_time
    }
  }
`;const GET_SCHEDULE_DETAIL=gql`
  query getScheduleDetail(
    $registerScheduleId: Int!
    $teacherId: CustomUserInputType
    $schoolId: Int
    $status: EnumStatusScheduleType
  ) @api(name: "appZim") {
    getScheduleDetail(
      registerScheduleId: $registerScheduleId
      teacherId: $teacherId
      schoolId: $schoolId
      status: $status
    ) {
      id
      type
      date
      schoolId
      startTime
      endTime
      students {
        id
        full_name
      }
      teacher {
        id
        full_name
      }
      eC {
        id
        full_name
      }
      linkMeeting
      description
    }
  }
`,ConfirmChangeTeacher=({isOpen:e,onClose:r,fromTeacher:n,toTeacher:a,onConfirm:l=()=>{}})=>jsxs(Modal$1,{isOpen:e,onClose:r,isCentered:!0,children:[jsx(ModalOverlay,{}),jsxs(ModalContent,{p:4,children:[jsx(ModalCloseButton,{}),jsx(Text,{fontWeight:600,fontSize:"lg",children:"Th\xF4ng b\xE1o"}),jsxs(Text,{children:["B\u1EA1n c\xF3 ch\u1EAFc ch\u1EAFn \u0111\xE3 li\xEAn h\u1EC7 v\xE0 \u0111\u01B0\u1EE3c s\u1EF1 \u0111\u1ED3ng \xFD c\u1EE7a ",a," \u0111\u1EC3 thay th\u1EBF cho ",n," kh\xF4ng?"]}),jsx(ModalFooter,{mt:4,p:0,children:jsxs(HStack,{children:[jsx(Button$1,{colorScheme:"gray",onClick:r,children:"\u0110\xF3ng"}),jsx(Button$1,{colorScheme:"blue",onClick:l,children:"X\xE1c nh\u1EADn"})]})})]})]}),useListScheduleStore=create$1(e=>({filterInput:{teacherId:0,schoolId:0,type:0,status:EnumStatusScheduleType.Active},setFilterInput:r=>e({filterInput:r})})),RequestCancelSchedule=({dataCancelSchedule:e})=>{var te;const{eventSelected:r,setLeftComponent:n,events:a,setEventSelected:l,onCloseDrawer:c}=useListScheduleContext(),{shift:u,date:d}=r,{teacherId:m}=e,[E,C]=react.exports.useState(!1),[_,{push:A,filter:T,reset:S}]=useList([]),[P,k]=react.exports.useState([]),[x,b]=react.exports.useState(!1),[g,y]=react.exports.useState([]),{isOpen:I,onOpen:M,onClose:j}=useDisclosure(),{isOpen:O,onOpen:L,onClose:N}=useDisclosure(),{filterInput:q}=useListScheduleStore(),V=useToast();useQuery(GET_SCHEDULE_DETAIL,{variables:{registerScheduleId:r.registerScheduleId,teacherId:m,schoolId:q.schoolId,status:EnumStatusScheduleType.RequestedCancel},skip:r.registerScheduleId===0,onError:X=>{var ce;V({title:"C\xF3 l\u1ED7i x\u1EA3y ra",description:(ce=X==null?void 0:X.message)!=null?ce:"L\u1EA5y d\u1EEF li\u1EC7u kh\xF4ng th\xE0nh c\xF4ng",status:"error",duration:3e3,isClosable:!0})},onCompleted:X=>{if(X.getScheduleDetail.length===0){const ce=[...r.teachers||[]],J=[...r.requestsCancel||[]],oe=ce.filter(de=>de.id!==e.teacherId);l(H(B({},r),{teachers:oe,requestsCancel:J.filter(de=>de.teacherId!==e.teacherId)})),(oe==null?void 0:oe.length)===0?(n(null),c()):n(Components.detail),$(["getListRegisterSchedule"])}y(X.getScheduleDetail)}});const{control:Q,handleSubmit:W,watch:G,reset:R}=useForm({defaultValues:{teacher:null}}),[D,{loading:F}]=useMutation(CHANGE_SCHEDULE_FOR_TEACHER),$=useResetQueries(),U=react.exports.useCallback(async X=>{var ce,J;try{const{data:oe}=await D({variables:{input:{registerScheduleId:r.registerScheduleId,toTeacherId:X.teacher.id,registerScheduleDetailId:_}}});V({title:"Th\xF4ng b\xE1o",description:(ce=oe.changeScheduleForTeacher.message)!=null?ce:"\u0110\u1EB7t l\u1ECBch th\xE0nh c\xF4ng",status:"success",duration:toastDuration,isClosable:!0});const de=[...r.teachers||[]];de.findIndex(pe=>pe.id===X.teacher.id)<0&&(de.push({id:X.teacher.id,full_name:X.teacher.full_name}),l(H(B({},r),{teachers:de}))),$(["getListRegisterSchedule","getScheduleDetail"]),C(!1),S(),N(),R({teacher:null})}catch(oe){V({title:"Th\xF4ng b\xE1o",description:(J=oe.message)!=null?J:"\u0110\u1ED5i l\u1ECBch th\u1EA5t b\u1EA1i",status:"error",duration:toastDuration,isClosable:!0})}},[_,g,r]);useQuery(GET_TEACHER_ASSIGNED_FOR_FUNCTION,{variables:{functionId:r.type,registerScheduleId:r.registerScheduleId,registerDetailId:_},onCompleted:X=>{var oe;const ce=X==null?void 0:X.getTeacherAssignedForFunction,J=(de=[])=>arrayToSelectOption(de,"full_name","id");k([{label:"Gi\xE1o vi\xEAn c\xF3 s\u1EB5n",options:J(ce.teachersAvailable)},{label:"Gi\xE1o vi\xEAn kh\xF4ng \u0111\u0103ng k\xFD l\u1ECBch",options:J(ce.teachersUnregistered)},{label:"Gi\xE1o vi\xEAn b\u1EADn",options:(oe=J(ce.teachersBusy))==null?void 0:oe.map(de=>H(B({},de),{isDisabled:!0}))}])},skip:!E});const z=react.exports.useCallback(({isSelected:X,id:ce})=>{E||(X?T(J=>J!==ce):A(ce))},[E]),Y=react.exports.useCallback(()=>{_.length===0?V({title:"Th\xF4ng b\xE1o",description:"Vui l\xF2ng ch\u1ECDn l\u1ECBch b\u1ECB \u1EA3nh h\u01B0\u1EDFng \u0111\u1EC3 \u0111\u1EB7t l\u1ECBch m\u1EDBi",status:"warning",duration:3e3,isClosable:!0}):C(!0)},[_]),Z=react.exports.useCallback(()=>{_.length===0?V({title:"Th\xF4ng b\xE1o",description:"Vui l\xF2ng ch\u1ECDn l\u1ECBch \u0111\u1EC3 h\u1EE7y",status:"warning",duration:3e3,isClosable:!0}):M()},[_]),K=react.exports.useCallback(()=>{E?C(!1):(x&&$(["getListRegisterSchedule"]),n(Components.detail))},[E,x]),[ue]=useMutation(CANCEL_SCHEDULE_DETAIL),ie=react.exports.useCallback(async()=>{var X,ce;try{const{data:J}=await ue({variables:{input:{registerScheduleId:r.registerScheduleId,registerScheduleDetailId:_}}});V({title:"Th\xF4ng b\xE1o",description:(X=J.cancelScheduleDetail.message)!=null?X:"H\u1EE7y l\u1ECBch th\xE0nh c\xF4ng",status:"success",duration:3e3,isClosable:!0}),j(),$(["getScheduleDetail","getListScheduleByStatus"]),b(!0),S()}catch(J){V({title:"Th\xF4ng b\xE1o",description:(ce=J.message)!=null?ce:"H\u1EE7y l\u1ECBch th\u1EA5t b\u1EA1i",status:"error",duration:3e3,isClosable:!0})}},[r,a,m,_]);return jsxs(Fragment,{children:[jsx(MyBox,{title:E?"\u0110\u1ED5i l\u1ECBch h\u1EB9n":"Y\xEAu c\u1EA7u \u0111\u1ED5i l\u1ECBch",children:jsxs(VStack,{spacing:4,align:"stretch",children:[jsx(Text,{children:e.teacherName}),jsxs(Text,{children:[dayjs(d).format("dddd, DD-MM-YYYY")," ",u.start_time," -"," ",u.end_time]}),jsx(Text,{fontWeight:700,fontSize:"md",children:E?"\u0110\u1ED5i l\u1ECBch khung gi\u1EDD":"C\xE1c l\u1ECBch b\u1ECB \u1EA3nh h\u01B0\u1EDFng"}),!E&&jsx(Text,{fontSize:"md",children:"Ch\u1ECDn l\u1ECBch b\xEAn d\u01B0\u1EDBi \u0111\u1EC3 h\u1EE7y l\u1ECBch ho\u1EB7c \u0111\u1EB7t l\u1ECBch m\u1EDBi"}),E?jsx(Text,{children:g.filter(X=>_.includes(X.id)).map(X=>`${X.startTime} - ${X.endTime}`).join(", ")}):jsx(SimpleGrid,{columns:3,gap:4,children:g==null?void 0:g.map(X=>{const ce=_.includes(X.id);return jsx(Shift,{students:X.students,title:`${X.startTime} - ${X.endTime}`,isBooked:ce,onClick:()=>z({isSelected:ce,id:X.id}),cursor:"pointer"},X.id)})}),jsxs(VStack,{align:"stretch",spacing:4,children:[E?jsxs(Fragment,{children:[jsxs(VStack,{align:"stretch",children:[jsxs(Text,{as:"span",fontSize:"md",children:["Ch\u1ECDn gi\u1EA3ng vi\xEAn"," ",jsx(Text,{as:"span",color:"red.500",children:"*"})]}),jsx(Controller,{rules:{required:"Vui l\xF2ng ch\u1ECDn gi\u1EA3ng vi\xEAn"},control:Q,render:({field:X,fieldState:{error:ce}})=>jsx(Select$1,H(B({components:{MenuList:CustomMenuList},options:P,getOptionLabel:J=>J.full_name,getOptionValue:J=>J.id},X),{error:ce})),name:"teacher"})]}),jsx(Text,{fontSize:"sm",fontStyle:"italic",children:"* Trong tr\u01B0\u1EDDng h\u1EE3p kh\xF4ng c\xF3 GV thay th\u1EBF th\xEC bu\u1ED9c ph\u1EA3i h\u1EE7y l\u1ECBch"}),jsx(Button$1,{isLoading:F,colorScheme:"blue",onClick:W(L),children:"\u0110\u1ED5i l\u1ECBch"})]}):jsxs(Fragment,{children:[jsx(Button$1,{isDisabled:(_==null?void 0:_.length)===0,colorScheme:"blue",onClick:Y,children:"\u0110\u1EB7t l\u1ECBch m\u1EDBi"}),jsx(Button$1,{isDisabled:(_==null?void 0:_.length)===0,colorScheme:"red",onClick:Z,children:"H\u1EE7y l\u1ECBch"})]}),jsx(Button$1,{colorScheme:"gray",onClick:K,children:"Quay l\u1EA1i"})]})]})}),jsx(ConfirmCancelSchedule,{isOpen:I,onClose:j,onConfirm:ie}),jsx(ConfirmChangeTeacher,{isOpen:O,onClose:N,onConfirm:W(U),toTeacher:(te=G("teacher"))==null?void 0:te.full_name,fromTeacher:e==null?void 0:e.teacherName})]})};var reactDatepicker="";newStyled(Box)`
  .react-datepicker-wrapper {
    display: block;
  }

  .react-datepicker {
    display: flex;
    border: 0 !important;
    box-shadow: 4px 0px 8px 0px rgba(0, 0, 0, 0.15);
    .react-datepicker__time-container {
      border-color: var(--chakra-colors-gray-200);
    }
  }

  .react-datepicker__header {
    border-bottom: 0;
    background: var(--chakra-colors-gray-100);
  }

  .react-datepicker__day--today {
    background: var(--chakra-colors-yellow-200);
    color: var(--chakra-colors-gray-900);
    border-radius: 0.3rem;
  }

  .react-datepicker__day--selected {
    background: var(--chakra-colors-primary);
    color: #fff;
  }
  react-datepicker__tab-loop {
    .react-datepicker-popper {
      z-index: 11;
    }
  }

  .react-datepicker-popper[data-placement^='bottom']
    .react-datepicker__triangle,
  .react-datepicker-popper[data-placement^='bottom']
    .react-datepicker__triangle:before {
    border-bottom-color: var(--chakra-colors-gray-100);
  }
  .react-datepicker__input-container {
    border-color: var(--chakra-colors-gray-100);
  }
`;const DateTimePicker=react.exports.forwardRef((l,a)=>{var c=l,{error:e,label:r}=c,n=Ae(c,["error","label"]);return jsxs(FormControl,{isInvalid:!!e,children:[r&&jsx(FormLabel,{htmlFor:n.id,children:r}),jsx(Wt,H(B({},n),{customInput:jsx(Input$1,{autoComplete:"off",background:useColorModeValue("white","slate.700")}),ref:a})),e&&jsx(FormErrorMessage,{children:e&&(e==null?void 0:e.message)})]})});var DateTimePicker$1=react.exports.memo(DateTimePicker);function groupBy(e){var n;const r=e.reduce(function(a,l){return a[l.date]=a[l.date]||[],a[l.date].push(l),a},Object.create(null));return(n=Object.entries(r))==null?void 0:n.map(([a,l])=>({date:a,children:l.map(c=>{var u;return{event:{id:c.id,start:new Date(dateAddTime(a,c.shift.start_time)),end:new Date(dateAddTime(a,c.shift.end_time)),extendedProps:{teachers:c.teachers,bg:c.configFunction.color,isFull:c.isFull,shift:c.shift,requestsCancel:(u=c.requestsCancel)!=null?u:[],configFunction:c.configFunction,type:c.type,eventDate:a}}}})}))}const SelectNewShift=({fieldReopenSchedule:e,goBack:r})=>{const n=new Date(Date.now()+3600*1e3*24),[a,l]=react.exports.useState(n),[c,u]=react.exports.useState(new Date(Date.now()+3600*1e3*8*24)),[d,m]=react.exports.useState([]),E=useToast();useQuery(GET_LIST_REGISTER_SCHEDULE,{variables:{fromDate:dayjs(a).format("YYYY/MM/DD HH:mm"),toDate:dayjs(c).format("YYYY/MM/DD HH:mm"),type:e.type},onCompleted:T=>{m(groupBy(T.getListRegisterSchedule))},onError:T=>{var S;E({title:"L\u1EA5y d\u1EEF li\u1EC7u th\u1EA5t b\u1EA1i.",description:(S=T==null?void 0:T.message)!=null?S:"L\u1EA5y d\u1EEF li\u1EC7u kh\xF4ng th\xE0nh c\xF4ng",status:"error",duration:3e3,isClosable:!0})}});const{setLeftComponent:C,setEventSelected:_}=useListScheduleContext(),A=react.exports.useCallback(T=>{const{event:S}=T,{teachers:P,eventDate:k,shift:x,type:b,requestsCancel:g,configFunction:y}=S.extendedProps;_({teachers:P,date:k,shift:x,type:b,registerScheduleId:parseInt(S.id,10),requestsCancel:g,configFunction:y,isReopenSchedule:!0,fieldReopenSchedule:e}),C(Components.detail)},[e]);return jsxs(VStack,{align:"stretch",children:[jsxs(HStack,{children:[jsx(DateTimePicker$1,{selected:a,onChange:l,maxDate:c,minDate:n,dateFormat:"dd/MM/yyyy"}),jsx(DateTimePicker$1,{selected:c,onChange:u,minDate:a,dateFormat:"dd/MM/yyyy"})]}),jsx(VStack,{align:"stretch",children:(d==null?void 0:d.length)?d.map(T=>{var S;return jsxs(VStack,{align:"stretch",children:[jsx(Text,{children:dayjs(T.date).format("dddd, DD/MM/YYYY")}),(S=T.children)==null?void 0:S.map(P=>jsx(RenderEvent,{eventInfo:P,maxW:"auto",eventClick:A},P.event.id))]},T.date)}):jsx(Text,{children:"Kh\xF4ng c\xF3 l\u1ECBch ph\xF9 h\u1EE3p"})}),jsx(Button$1,{colorScheme:"gray",onClick:r,children:"Quay l\u1EA1i"})]})},StyledTextContentTipTap=newStyled(Text)`
  table {
    display: table;
    margin-left: auto;
    margin-right: auto;
    width: 100%;
    border: 1px solid #666666;
    border-spacing: 5px;
    border-collapse: collapse;
    th,
    td {
      border: ${e=>`1px solid ${e.borderColor||"black"}`};
      min-width: 100px;
      padding: 2px;
    }
    p {
      margin: 0;
    }
  }
  .custom-image {
    max-width: ${e=>`${e.maxWidthImage||"100%"}`};
    height: ${e=>`${e.heightImage||"auto"}`};
  }
`,ContentScheduleDetail=({startTime:e,endTime:r,students:n,description:a,linkMeeting:l,teacher:c,date:u,id:d})=>jsxs(Fragment,{children:[jsxs(HStack,{justifyContent:"space-between",children:[jsx(Text,{children:dayjs(u).format("dddd, DD-MM-YYYY")}),jsxs(Text,{fontWeight:600,children:["ID: ",d]})]}),jsxs(Text,{children:[e," - ",r]}),jsxs(Text,{children:["H\u1ECDc vi\xEAn: ",n==null?void 0:n.map(m=>m.full_name).join(", ")]}),jsxs(Text,{children:["GV: ",c==null?void 0:c.full_name]}),jsxs(Text,{as:"span",children:["N\u1ED9i dung"," ",jsx(StyledTextContentTipTap,{dangerouslySetInnerHTML:{__html:a},noOfLines:2})]}),(l==null?void 0:l.length)&&jsx(Text,{as:"a",href:l,target:"_blank",textDecorationLine:"underline",children:"Room"})]}),ListCanceledSchedules=()=>{const e=new Date(Date.now()+3600*1e3*24),[r,n]=react.exports.useState(e),[a,l]=react.exports.useState(new Date(Date.now()+3600*1e3*8*24)),[c,u]=react.exports.useState([]),[d,m]=react.exports.useState(!1),[E,C]=react.exports.useState({reopenScheduleId:0,students:[],type:0}),_=useToast();return useQuery(GET_LIST_SCHEDULE_BY_STATUS,{variables:{fromDate:dayjs(r).format("YYYY/MM/DD HH:mm"),toDate:dayjs(a).format("YYYY/MM/DD HH:mm"),status:EnumStatusScheduleType.Cancel},onCompleted:A=>{u(A==null?void 0:A.getListScheduleByStatus)},onError:A=>{var T;_({title:"L\u1EA5y d\u1EEF li\u1EC7u th\u1EA5t b\u1EA1i.",description:(T=A==null?void 0:A.message)!=null?T:"L\u1EA5y d\u1EEF li\u1EC7u kh\xF4ng th\xE0nh c\xF4ng",status:"error",duration:3e3,isClosable:!0})}}),jsx(MyBox,{title:d?"Ch\u1ECDn ca m\u1EDBi":"L\u1ECBch \u0111\xE3 b\u1ECB h\u1EE7y",children:jsxs(Fragment,{children:[!d&&jsxs(Fragment,{children:[jsxs(HStack,{children:[jsx(DateTimePicker$1,{selected:r,onChange:n,maxDate:a,minDate:new Date,dateFormat:"dd/MM/yyyy"}),jsx(DateTimePicker$1,{selected:a,onChange:l,minDate:r,dateFormat:"dd/MM/yyyy"})]}),jsx(VStack,{align:"stretch",children:(c==null?void 0:c.length)>0?c.map(A=>jsx(ScheduleDetail$1,H(B({},A),{onRegisterSchedule:(T,S,P)=>{m(!0),C({reopenScheduleId:T,students:S,type:P})}}),A.id)):jsx(Text,{children:"Kh\xF4ng c\xF3 l\u1ECBch b\u1ECB h\u1EE7y"})})]}),d&&jsx(SelectNewShift,{fieldReopenSchedule:E,goBack:()=>m(!1)})]})})},ScheduleDetail$1=c=>{var u=c,{id:e,onRegisterSchedule:r,type:n,students:a}=u,l=Ae(u,["id","onRegisterSchedule","type","students"]);const[d]=useUser();return jsxs(VStack,{rounded:8,align:"stretch",bg:useColorModeValue("gray.100","slate.600"),p:4,spacing:2,fontSize:"15px",children:[jsx(ContentScheduleDetail,H(B({},l),{students:a,id:e})),GROUP_ROLES.canBookSchedule.includes(d.roleId)&&jsx(Button$1,{colorScheme:"blue",onClick:()=>r(e,a,n),children:"\u0110\u1EB7t l\u1EA1i l\u1ECBch"})]})},MakeAnAppointment=loadable$2(()=>__vitePreload(()=>import("./index30.js"),["assets/index30.js","assets/vendor.js","assets/isSameOrAfter.js"])),ScheduleDetail=loadable$2(()=>__vitePreload(()=>import("./index31.js"),["assets/index31.js","assets/vendor.js","assets/isSameOrAfter.js","assets/utils.js","assets/index16.js"])),LeftContent=()=>{const{leftComponent:e}=useListScheduleContext(),[r,n]=react.exports.useState({teacherName:"",startTime:"",endTime:"",teacherId:0}),[a,l]=react.exports.useState({teacherName:"",teacherId:0,scheduleDetail:[]});return jsx(Flex,{width:{xl:"410px",base:"full"},h:"full",flexGrow:1,flexShrink:0,borderWidth:{xl:1,base:0},borderColor:useColorModeValue("gray.400","gray.700"),overflow:"auto",bg:useColorModeValue("white","slate.800"),children:jsxs(Box,{flexGrow:1,overflow:"auto",children:[e===Components.detail&&jsx(ScheduleDetail,{setDataRegisterSchedule:n,setDataCancelSchedule:l}),e===Components.makeAnAppointment&&jsx(MakeAnAppointment,B({},r)),e===Components.requestCancelSchedule&&jsx(RequestCancelSchedule,{dataCancelSchedule:a}),e===Components.listCanceledSchedule&&jsx(ListCanceledSchedules,{})]})})};var LeftContent$1=react.exports.memo(LeftContent);const Menu=({setLeftComponent:e,onOpenDrawer:r})=>jsx(Box,{display:"flex",alignItems:"center",flexDirection:{xl:"column",base:"row"},bg:useColorModeValue("blue.500","blue.800"),w:{xl:"56px",base:"full"},flexShrink:0,py:2,experimental_spaceX:{xl:0,base:4},experimental_spaceY:{xl:2,base:0},px:{xl:0,base:4},justifyContent:{xl:"start",base:"space-around"},children:jsx(Tooltip,{hasArrow:!0,label:"L\u1ECBch \u0111\xE3 b\u1ECB h\u1EE7y",children:jsx(Box,{as:"button",onClick:()=>{e(Components.listCanceledSchedule),r()},children:jsx(MdOutlineCancel,{color:"white",size:24})})})});var style="";const SELF_STUDY_ROOM_ID=3,OFF_ID=4;var EnumConfigFunctionType;(function(e){e.register="register",e.function="function"})(EnumConfigFunctionType||(EnumConfigFunctionType={}));const ScheduleTypeNotes=({typeRegister:e})=>(e==null?void 0:e.length)?jsx(SimpleGrid,{columns:{xl:4,base:1},maxW:"xl",rowGap:2,columnGap:{xl:6,base:0},children:e.map(r=>jsxs(HStack,{spacing:2,children:[jsx(Circle,{size:3,bg:`${r.color}.500`}),jsx(Text,{whiteSpace:"nowrap",children:r.name})]},r.id))}):null,Wrapper=({children:e,split:r=!1})=>jsx(Box,{w:{base:r?"50%":"100%",md:r?"25%":"50%",lg:"33.33%",xl:"25%","2xl":"20%"},p:2,children:e}),useQueryOption=({query:e,options:r={}})=>{const{data:n,loading:a,refetch:l}=useQuery(e,B({fetchPolicy:"cache-and-network"},r));return[n,a,l]},allOption={label:"T\u1EA5t c\u1EA3",value:"all"},initialFilterState={school:allOption,subject:allOption,openStatuses:[],openDateFrom:null,openDateTo:null,finishDateFrom:null,finishDateTo:null,level:allOption,shifts:[],ec:allOption,dayOfWeeks:[],skills:[],preTeachers:[]},CourseFilter=({onSubmit:e,loading:r})=>{var O,L;const[n,a]=useToggle(),[l,c]=react.exports.useState(initialFilterState),[u,d]=react.exports.useState([]),[m,E]=react.exports.useState([]),[C,_]=react.exports.useState([]),[A,T]=useQueryOption({query:GET_SCHOOLS}),[S,P]=useQueryOption({query:GET_SUBJECTS$1,options:{variables:{status:"active"}}}),[k,x,b]=useQueryOption({query:GET_LEVELS$1,options:{variables:{subject_id:((O=l==null?void 0:l.subject)==null?void 0:O.value)==="all"?0:(L=l==null?void 0:l.subject)==null?void 0:L.value}}}),g=react.exports.useCallback(N=>{c(H(B({},l),{openDateFrom:N}))},[l]),y=react.exports.useCallback(N=>{c(H(B({},l),{finishDateTo:N}))},[l]),I=react.exports.useCallback((N,q)=>{(q==null?void 0:q.name)&&c(H(B({},l),{[q.name]:N}))},[l]),M=()=>{e(l)},j=()=>{c(initialFilterState),e(initialFilterState)};return react.exports.useEffect(()=>{(A==null?void 0:A.schools)&&!T&&d(A.schools.map(N=>H(B({},N),{label:N.name,value:N.id})))},[A,T]),react.exports.useEffect(()=>{(S==null?void 0:S.subjects)&&!P&&E(S.subjects.map(N=>H(B({},N),{label:N.name,value:N.id})))},[S,P]),react.exports.useEffect(()=>{(k==null?void 0:k.levels)&&!x&&_(k.levels.map(N=>H(B({},N),{label:N.graduation,value:N.id})))},[k,x]),react.exports.useEffect(()=>{var N,q;!((N=l.subject)==null?void 0:N.value)||((q=l.subject)==null?void 0:q.value)==="all"?(c(H(B({},l),{level:allOption})),_([])):(async()=>{var Q;return await b({subject_id:(Q=l.subject)==null?void 0:Q.value})})()},[l.subject.value]),react.exports.useEffect(()=>{JSON.stringify(l)===JSON.stringify(initialFilterState)?a(!1):a(!0)},[l]),jsxs(Box,{children:[jsxs(Flex,{mx:-2,flexWrap:"wrap",children:[jsx(Wrapper,{children:jsx(Select$1,{id:"f-school",name:"school",label:"Trung t\xE2m",value:l.school,options:u,isLoading:T,onChange:I,paramKey:"school",allOption:{label:"T\u1EA5t c\u1EA3",value:"all"}})}),jsx(Wrapper,{children:jsx(Select$1,{id:"f-subject",name:"subject",label:"M\xF4n h\u1ECDc",value:l.subject,options:m,isLoading:P,onChange:I,paramKey:"subject",allOption:{label:"T\u1EA5t c\u1EA3 ",value:"all"}})}),jsx(Wrapper,{children:jsx(Select$1,{name:"level",isLoading:x,label:"Tr\xECnh \u0111\u1ED9",value:l.level,options:C,onChange:I,allOption:{label:"T\u1EA5t c\u1EA3 ",value:"all"}})}),jsxs(Wrapper,{children:[jsx(FormControl,{children:jsx(FormLabel,{htmlFor:"f-startDate",children:"Khai gi\u1EA3ng"})}),jsx(DateTimePicker$1,{id:"f-startDate",selected:l.openDateFrom,onChange:g,dateFormat:"dd/MM/yyyy",placeholderText:"Ch\u1ECDn ng\xE0y"})]}),jsxs(Wrapper,{children:[jsx(FormControl,{children:jsx(FormLabel,{htmlFor:"f-endDate",children:"K\u1EBFt th\xFAc"})}),jsx(DateTimePicker$1,{id:"f-endDate",selected:l.finishDateTo,onChange:y,dateFormat:"dd/MM/yyyy",placeholderText:"Ch\u1ECDn ng\xE0y",monthsShown:1})]}),jsx(Wrapper,{children:jsx(Select$1,{label:"Tr\u1EA1ng th\xE1i",name:"openStatuses",isMulti:!0,options:courseStatusOptions,value:l.openStatuses,onChange:I})})]}),jsxs(HStack,{mt:4,spacing:4,children:[jsx(Button$1,{colorScheme:"purple",width:{base:"100%",md:"auto"},onClick:M,isLoading:r,children:"L\u1ECDc d\u1EEF li\u1EC7u"}),n&&jsx(Button$1,{leftIcon:jsx(MdClear,{}),variant:"ghost",colorScheme:"red",onClick:j,flexShrink:0,isDisabled:r,children:"X\xF3a b\u1ED9 l\u1ECDc"})]})]})};gql`
  query schools($city_id: Int, $status: String) @api(name: "appZim") {
    schools(city_id: $city_id, status: $status) {
      id
      city {
        id
      }
      district {
        id
      }
      name
      address
      phone
      color_code
      lat
      lon
      status
    }
  }
`;const GET_GRADES=gql`
  query cats @api(name: "appZim") {
    cats {
      id
      name
    }
  }
`,GET_SUBJECTS=gql`
  query subjects($name: String, $status: StatusEnum) @api(name: "appZim") {
    subjects(name: $name, status: $status) {
      id
      name
      status
      cats {
        id
        name
        status
        subject {
          id
          name
          status
        }
      }
    }
  }
`;gql`
  query subcatsBySubjectId($subject_id: Int!) @api(name: "appZim") {
    subcatsBySubjectId(subject_id: $subject_id) {
      id
      name
      status
      cat_id
    }
  }
`;const GET_CURRICULUMS=gql`
  query curriculums($name: String, $status: StatusEnum, $program_id: Int)
  @api(name: "appZim") {
    curriculums(name: $name, status: $status, program_id: $program_id) {
      id
      name
      status
      shift_minute
      total_lesson
    }
  }
`,GET_SHIFTS=gql`
  query shifts($q: String, $status: String, $shift_minute: Int)
  @api(name: "appZim") {
    shifts(q: $q, status: $status, shift_minute: $shift_minute) {
      id
      status
      shift_minute
      start_time
      end_time
    }
  }
`,GET_SIZES=gql`
  query sizes($q: String, $status: String) @api(name: "appZim") {
    sizes(q: $q, status: $status) {
      id
      status
      name
      size
    }
  }
`,GET_LEVELS=gql`
  query levels(
    $subject_id: Int!
    $graduation: Float
    $status: EnumLevelStatus
    $name: String
    $type: String
  ) @api(name: "appZim") {
    levels(
      subject_id: $subject_id
      graduation: $graduation
      status: $status
      name: $name
      type: $type
    ) {
      id
      name
      status
      graduation
    }
  }
`,GET_EXERCISE_TYPES=gql`
  query @api(name: "appZim") {
    getExerciseTypes {
      id
      name
    }
  }
`,GET_QUESTION_TYPE=gql`
  query @api(name: "appZim") {
    getQuestionType {
      id
      name
    }
  }
`,GET_SUB_CATS=gql`
  query subcatsBySubjectId($subject_id: Int!) @api(name: "appZim") {
    subcatsBySubjectId(subject_id: $subject_id) {
      id
      name
    }
  }
`;gql`
  query levels(
    $name: String
    $status: EnumLevelStatus
    $type: String
    $subject_id: Int
    $graduation: Float
  ) @api(name: "appZim") {
    levels(
      name: $name
      status: $status
      type: $type
      subject_id: $subject_id
      graduation: $graduation
    ) {
      id
      name
    }
  }
`;const GET_LESSON_BY_CONFIG=gql`
  query getLessonByConfig(
    $subjectId: Int!
    $catId: Int!
    $subcatId: Int!
    $levelIn: Int!
    $levelOut: Int!
  ) @api(name: "appZim") {
    getLessonByConfig(
      subjectId: $subjectId
      catId: $catId
      subcatId: $subcatId
      levelIn: $levelIn
      levelOut: $levelOut
    ) {
      id
      title
    }
  }
`,GET_SUB_CAT_BY_CAT_ID=gql`
  query subcats($cat_id: Int, $status: String) @api(name: "appZim") {
    subcats(cat_id: $cat_id, status: $status) {
      id
      name
    }
  }
`,GET_LIST_SCHOOL_ASSIGN_FOR_TEACHER=gql`
  query getListSchoolAssignForTeacher(
    $teacherId: CustomUserInputType
    $functionId: Int
  ) @api(name: "appZim") {
    getListSchoolAssignForTeacher(
      teacherId: $teacherId
      functionId: $functionId
    ) {
      id
      name
    }
  }
`,GET_ACCOUNTS_BY_ROLE=gql`
  query getAccountsByRole(
    $userId: CustomUserInputType
    $schoolId: Int
    $roleId: Int!
  ) @api(name: "appZim") {
    getAccountsByRole(userId: $userId, schoolId: $schoolId, roleId: $roleId) {
      id
      full_name
    }
  }
`,useGetCenterByUser=({onCompleted:e=()=>{},variables:r}={})=>{const n=useToast(),[a]=useUser();return useQuery(GET_LIST_SCHOOL_ASSIGN_FOR_TEACHER,{variables:B({teacherId:Number(a.id)},r),onCompleted:l=>{var u;const c=(u=l.getListSchoolAssignForTeacher)==null?void 0:u.map(d=>({label:d.name,value:d.id}));e(c)},onError:l=>{var c;n({title:"Failed",description:(c=l.message)!=null?c:"L\u1EA5y danh s\xE1ch trung t\xE2m kh\xF4ng th\xE0nh c\xF4ng !!",status:"error",duration:3e3,isClosable:!0})},skip:!a})},useListAccountsByRole=(e,r={})=>{const n=useToast();return useQuery(GET_ACCOUNTS_BY_ROLE,B({variables:e,onError:a=>{n({title:"L\u1EA5y d\u1EEF li\u1EC7u th\u1EA5t b\u1EA1i.",description:`GET account by role error: ${a==null?void 0:a.message}`,status:"error",duration:3e3,isClosable:!0})},fetchPolicy:"cache-first",errorPolicy:"all"},r))},statusOptions=[{label:"T\u1EA5t c\u1EA3",value:EnumStatusScheduleType.All},{label:"\u0110\xE3 \u0111\u01B0\u1EE3c ph\xE2n l\u1ECBch",value:EnumStatusScheduleType.Active},{label:"\u0110ang y\xEAu c\u1EA7u h\u1EE7y",value:EnumStatusScheduleType.RequestedCancel},{label:"\u0110\xE3 h\u1EE7y",value:EnumStatusScheduleType.Cancel},{label:"\u0110\xE3 \u0111\u01B0\u1EE3c \u0111\u1EB7t l\u1EA1i",value:EnumStatusScheduleType.Reopened}],blankState={type:null,center:null,teacher:{full_name:"T\u1EA5t c\u1EA3",id:0},status:statusOptions[0]},ListScheduleFilter=({typeRegisterOptions:e=[],onFilter:r=()=>{}})=>{const{control:n,handleSubmit:a,setValue:l,watch:c,reset:u,resetField:d}=useForm({defaultValues:B({},blankState)}),[m,E]=react.exports.useState({type:null,center:null}),C=c("center"),_=c("type"),A=c("teacher"),T=c("status"),{data:S}=useGetCenterByUser();react.exports.useEffect(()=>{if(!(e==null?void 0:e.length))return;const g=H(B({},e[0]),{label:e[0].name});l("type",g),E(H(B({},m),{type:g}))},[e]),react.exports.useEffect(()=>{if(!(S==null?void 0:S.getListSchoolAssignForTeacher))return;const g=H(B({},S.getListSchoolAssignForTeacher[0]),{label:S.getListSchoolAssignForTeacher[0].name});l("center",g),E(H(B({},m),{center:g}))},[S]);const{data:P}=useListAccountsByRole({roleId:4,schoolId:C==null?void 0:C.id},{skip:!C}),k=!lodash.exports.isEqual(B(B({},blankState),m),{type:_,center:C,teacher:A,status:T}),x=react.exports.useCallback(()=>{var g,y,I;u(H(B({},blankState),{center:m.center,type:m.type})),r({schoolId:(g=m.center.id)!=null?g:0,type:(I=(y=m.type)==null?void 0:y.id)!=null?I:0,teacherId:0,status:statusOptions[0].value})},[m]),b=react.exports.useCallback(g=>{var y,I,M,j,O,L;r({type:(I=(y=g.type)==null?void 0:y.id)!=null?I:0,schoolId:(j=(M=g.center)==null?void 0:M.id)!=null?j:0,teacherId:(L=(O=g.teacher)==null?void 0:O.id)!=null?L:0,status:g.status.value})},[]);return react.exports.useEffect(()=>{var g,y,I,M;r({type:(y=(g=m.type)==null?void 0:g.id)!=null?y:0,schoolId:(M=(I=m.center)==null?void 0:I.id)!=null?M:0,teacherId:0,status:statusOptions[0].value})},[m]),jsxs(Box,{children:[jsxs(Flex,{flexWrap:"wrap",children:[jsx(Wrapper,{children:jsx(Controller,{control:n,render:({field:g})=>jsx(Select$1,B({label:"Lo\u1EA1i l\u1ECBch",options:arrayToSelectOption([{name:"T\u1EA5t c\u1EA3",id:0},...e],"name","id"),getOptionLabel:y=>y.name,getOptionValue:y=>{var I;return(I=y.id)==null?void 0:I.toString()}},g)),name:"type"})}),jsx(Wrapper,{children:jsx(Controller,{control:n,render:({field:g})=>{var y;return jsx(Select$1,H(B({label:"Trung t\xE2m",options:arrayToSelectOption((y=S==null?void 0:S.getListSchoolAssignForTeacher)!=null?y:[],"name","id"),getOptionLabel:I=>I.name,getOptionValue:I=>{var M;return(M=I==null?void 0:I.id)==null?void 0:M.toString()}},g),{onChange:I=>{g.onChange(I),d("teacher")}}))},name:"center"})}),jsx(Wrapper,{children:jsx(Controller,{control:n,render:({field:g})=>jsx(Select$1,H(B({label:"Gi\xE1o vi\xEAn",options:arrayToSelectOption([{full_name:"T\u1EA5t c\u1EA3",id:0},...(P==null?void 0:P.getAccountsByRole)||[]],"full_name","id")},g),{getOptionLabel:y=>y.full_name,getOptionValue:y=>y.id.toString()})),name:"teacher"})}),jsx(Wrapper,{children:jsx(Controller,{control:n,render:({field:g})=>jsx(Select$1,B({label:"Tr\u1EA1ng th\xE1i",options:statusOptions},g)),name:"status"})})]}),jsxs(HStack,{mt:4,children:[jsx(Button$1,{mx:2,colorScheme:"green",onClick:a(b),children:"L\u1ECDc d\u1EEF li\u1EC7u"}),k&&jsx(Button$1,{leftIcon:jsx(MdClear,{}),variant:"ghost",colorScheme:"red",onClick:x,flexShrink:0,children:"X\xF3a b\u1ED9 l\u1ECDc"})]})]})},ListSchedulesPage=()=>{var G;const e=react.exports.useRef(null),[r,n]=react.exports.useState([]),[a,l]=react.exports.useState(dayjs()),[c,u]=react.exports.useState([]),[d,m]=react.exports.useState([]),[E,C]=react.exports.useState(null),[_,A]=react.exports.useState(null),[T,S]=react.exports.useState(B({},blankStateEventSelected)),{setFilterInput:P,filterInput:k}=useListScheduleStore(),{search:x}=useLocation(),{registerScheduleId:b}=queryString.parse(x),g=useToast(),{isOpen:y,onOpen:I,onClose:M}=useDisclosure(),j=useBreakpointValue({xl:!1,base:!0}),O=react.exports.useCallback(R=>{var F;const D=generateMonthEvent((F=R==null?void 0:R.getListRegisterSchedule)!=null?F:[],dayjs(a).format(),d,c);if(b){const $=parseInt(b,10),U=D.find(z=>z.id===$);U&&(S({teachers:U.teachers,date:U.eventDate,shift:U.shift,type:U.type,registerScheduleId:$,requestsCancel:U.requestsCancel,configFunction:U.configFunction,isReopenSchedule:!1,fieldReopenSchedule:{reopenScheduleId:0,type:0,students:[]}}),C(Components.detail))}n(D)},[a,d,b,c]);useQuery(GET_TYPE_REGISTER,{variables:{type:EnumConfigFunctionType.function},onCompleted:R=>{u(R.getConfigFunctionByType)},onError:R=>{var D;g({title:"L\u1EA5y d\u1EEF li\u1EC7u th\u1EA5t b\u1EA1i.",description:(D=R==null?void 0:R.message)!=null?D:"L\u1EA5y d\u1EEF li\u1EC7u kh\xF4ng th\xE0nh c\xF4ng",status:"error",duration:3e3,isClosable:!0})}}),useQuery(GET_SHIFT_REGISTER_SCHEDULE,{variables:{type:((G=c==null?void 0:c.find(R=>R.id===k.type))==null?void 0:G.shiftType)||EnumShiftsType.Other},onCompleted:R=>{m(R.getShiftRegisterSchedule)},onError:R=>{var D;g({title:"L\u1EA5y d\u1EEF li\u1EC7u th\u1EA5t b\u1EA1i.",description:(D=R==null?void 0:R.message)!=null?D:"L\u1EA5y d\u1EEF li\u1EC7u kh\xF4ng th\xE0nh c\xF4ng",status:"error",duration:3e3,isClosable:!0})}});const{loading:L,data:N}=useQuery(GET_LIST_REGISTER_SCHEDULE,{variables:{fromDate:dayjs(a).startOf("month").set("hour",0).set("minute",0).format("YYYY/MM/DD HH:mm"),toDate:dayjs(a).endOf("month").set("hour",0).set("minute",0).format("YYYY/MM/DD HH:mm"),type:k.type,schoolId:k.schoolId,teacherId:k.teacherId,status:k.status},onError:R=>{var D;g({title:"L\u1EA5y d\u1EEF li\u1EC7u th\u1EA5t b\u1EA1i.",description:(D=R==null?void 0:R.message)!=null?D:"L\u1EA5y d\u1EEF li\u1EC7u kh\xF4ng th\xE0nh c\xF4ng",status:"error",duration:3e3,isClosable:!0})}});react.exports.useEffect(()=>{!N||O(N)},[N]);const q=react.exports.useCallback(()=>{if(e.current){const D=e.current._calendarApi.getDate();l(dayjs(D))}},[]),V=react.exports.useCallback(R=>{const{event:D}=R,{teachers:F,eventDate:$,shift:U,type:z,requestsCancel:Y=[],configFunction:Z}=D.extendedProps;S({teachers:F,date:$,shift:U,type:z,registerScheduleId:parseInt(D.id,10),requestsCancel:Y,configFunction:Z,isReopenSchedule:!1,fieldReopenSchedule:{reopenScheduleId:0,type:0,students:[]}});const K=document.querySelector(".fc-popover-close");setTimeout(()=>{K&&K.click()},200),j&&I(),C(Components.detail),_&&A(null)},[_,j]),Q=react.exports.useMemo(()=>({eventSelected:T,typeRegisters:c,leftComponent:E,setLeftComponent:C,dataScheduleInShift:_,setDataScheduleInShift:A,setEventSelected:S,events:r,setEvents:n,onCloseDrawer:M}),[T,c,E,C,_,A,S,r,n]),{height:W}=useGetWindowDimensions();return jsx(Stack,{flexDirection:{xl:"row",base:"column"},display:"flex",h:W-146,flexGrow:1,align:"stretch",children:jsxs(ListSchedulesContext.Provider,{value:Q,children:[jsxs(HStack,{spacing:0,align:"stretch",display:{xl:"flex",base:"none"},flexGrow:1,h:"full",maxH:"full",children:[jsx(Menu,{setLeftComponent:C}),jsx(LeftContent$1,{})]}),jsxs(VStack,{align:"stretch",overflow:"auto",minW:0,py:4,px:{xl:8,base:4},h:"full",maxH:"full",children:[jsx(Card$1,{bg:useColorModeValue("gray.50","slate.800"),mt:4,children:jsx(ListScheduleFilter,{typeRegisterOptions:c,onFilter:R=>{P(B({},R)),S(B({},blankStateEventSelected)),C(null),M()}})}),jsx(ScheduleTypeNotes,{typeRegister:c}),jsxs(Stack,{className:"wrap-list-schedules",pos:"relative",children:[jsx(FullCalendar,{ref:e,plugins:[main,main$1],initialView:"dayGridMonth",selectable:!0,events:r,weekends:!0,eventContent:R=>jsx(RenderEvent,{eventInfo:R,eventClick:V}),height:"auto",stickyHeaderDates:!0,dayMaxEventRows:4,headerToolbar:{left:"title",right:"today prev,next"},datesSet:q}),L&&jsx(VStack,{justify:"center",pos:"absolute",bg:"whiteAlpha.500",top:0,w:"full",h:"full",zIndex:100,children:jsx(Loading,{})})]})]}),j&&jsxs(Drawer$1,{placement:"left",isOpen:y,onClose:M,size:"xl",children:[jsx(ModalOverlay,{}),jsxs(DrawerContent,{children:[jsx(ModalCloseButton,{}),jsx(ModalBody,{px:0,children:jsx(LeftContent$1,{})})]})]}),jsx(Box,{pos:"sticky",bottom:0,display:{xl:"none",base:"flex"},w:"full",zIndex:10,children:jsx(Menu,{setLeftComponent:C,onOpenDrawer:I})})]})})};var index=Object.freeze({__proto__:null,[Symbol.toStringTag]:"Module",default:ListSchedulesPage});const isModuleMode=getEnviroment("ZIM_BUILD_MODE")==="module",Forbidden403Page=loadable$2(()=>__vitePreload(()=>import("./index2.js"),["assets/index2.js","assets/vendor.js"])),NotFound404Page=loadable$2(()=>__vitePreload(()=>import("./index3.js"),["assets/index3.js","assets/vendor.js"]));function App(){const[e,r]=react.exports.useState(!0),{appSetLogin:n,appSetLogout:a,appState:l}=useAppContext(),c=useIsMounted(),{loaded:u}=l,d=()=>{};return react.exports.useEffect(()=>{if(!!c()){if(isModuleMode){n(getCookie("token")),Object.values(global.wsClients).forEach(m=>{m.connect()}),r(!1);return}fetchAccessToken().then(m=>{m===void 0||(m==null?void 0:m.token)===void 0?(a(),r(!1),Object.values(global.wsClients).forEach(C=>{C.close(!0,!0)})):(n(m==null?void 0:m.token),Object.values(global.wsClients).forEach(C=>{C.connect()}))}).catch(m=>{console.log({e:m}),a(),r(!1)}).finally(()=>{r(!1)})}},[c()]),jsx(ErrorBoundary,{FallbackComponent:ErrorFallback,onError:d,children:jsx(BrowserRouter,{children:e||!u?jsx(HStack,{w:"100%",h:"100vh",justifyContent:"center",alignItems:"center",children:jsx(Loading,{text:"\u0110ang l\u1EA5y d\u1EEF li\u1EC7u..."})}):isModuleMode?jsx(Routes,{children:jsx(Route,{path:"*",element:jsx(ListSchedulesPage,{})})}):jsxs(Routes,{children:[routes.map((m,E)=>jsx(Route,{path:m.path,element:jsx(ProtectedRoute,{allowRoles:m.allowRoles,children:jsx(Layout,{children:jsx(m.component,{})})})},`${m.path}-${E}`)),jsx(Route,{path:"/auth/login",element:jsx(Login,{})}),jsx(Route,{path:"/error/403",element:jsx(Forbidden403Page,{})}),jsx(Route,{path:"/error/404",element:jsx(NotFound404Page,{})})]})})})}const colors={secondary:"#c89934",primary:"#3463c8",gray:{50:"#F8F8F8",100:"#F0F0F0",200:"#E6E6E6",300:"#D5D5D5",400:"#B0B0B0",500:"#909090",600:"#686868",700:"#555555",800:"#373737",900:"#171717"},secondBrand:{50:"#f8fae8",100:"#eff2c5",200:"#e5e9a0",300:"#dce07d",400:"#d6da66",500:"#d1d551",600:"#cfc449",700:"#ccae3f",800:"#c89934",900:"#c07722"},brand:{50:"#e4f2ff",100:"#bedeff",200:"#94caff",300:"#6bb5ff",400:"#4fa4ff",500:"#3463c8",600:"#2e44a8",700:"#243b9c",800:"#1b3190",900:"#081f7c"},pink:{50:"#f9e4ec",100:"#f2bcd0",200:"#eb92b1",300:"#e36993",400:"#dd4d7c",500:"#d93767",600:"#c83463",700:"#b2305d",800:"#9d2c58",900:"#77264c"},blue:{50:"#E1F2FC",100:"#B7DDFA",200:"#88C8F7",300:"#55B3F4",400:"#21A3F3",500:"#0093F1",600:"#0085E4",700:"#0074D1",800:"#0063BF",900:"#0045A1"},green:{50:"#E8F5E9",100:"#C8E6C9",200:"#A5D6A7",300:"#81C784",400:"#66BB6A",500:"#4CAF50",600:"#43A047",700:"#388E3C",800:"#2E7D32",900:"#1B5E20"},red:{50:"#FFEAED",100:"#FFCBD5",200:"#F1959E",300:"#E9686B",400:"#F54258",500:"#FC243E",600:"#EC163D",700:"#DA0037",800:"#CD002F",900:"#BF0022"},slate:{50:"#f8fafc",100:"#f1f5f9",200:"#e2e8f0",300:"#cbd5e1",400:"#94a3b8",500:"#64748b",600:"#475569",700:"#334155",800:"#1e293b",900:"#0f172a"},orange:{50:"#FFF4E2",100:"#FFE1B6",200:"#FFCE88",300:"#FFBA5A",400:"#FFAB3A",500:"#FF9C27",600:"#FF9125",700:"#FA8123",800:"#F37121",900:"#E9581E"}},Card={baseStyle:({colorMode:e})=>{const r=e==="light";return{display:"block",background:r?"white":"slate.800",gap:6,border:"1px solid",borderColor:r?"gray.200":"slate.700"}},variants:{rounded:{padding:8,borderRadius:"xl"},smooth:{padding:4,borderRadius:"base"}},defaultProps:{variant:"smooth"}},Table={baseStyle:({colorMode:e})=>{const r=e==="dark";return{table:{th:{background:r?"slate.700":"gray.50",lineHeight:1.5},td:{background:r?"slate.800":"white",lineHeight:1.5}}}},variants:{simple:({colorMode:e})=>{const n=e==="dark"?"slate.700":"gray.300";return{td:{borderBottomColor:n,paddingTop:2,paddingBottom:2,paddingInlineStart:4,paddingInlineEnd:4},th:{borderBottomColor:n,paddingInlineStart:4,paddingInlineEnd:4,paddingTop:2,paddingBottom:2}}}},sizes:{xs:({colorMode:e})=>({td:{paddingTop:"var(--chakra-space-1)",paddingBottom:"var(--chakra-space-1)",fontSize:"var(--chakra-fontSizes-xs)"},th:{paddingTop:"var(--chakra-space-1)",paddingBottom:"var(--chakra-space-1)",fontSize:"var(--chakra-fontSizes-xs)"}})},defaultProps:{variant:"simple",size:"sm"}},Badge={baseStyle:({colorMode:e})=>({textTransform:"capitalize",padding:"0.25rem 0.5rem ",borderRadius:4}),variants:{},sizes:{xs:{padding:"0.125rem 0.5rem",fontSize:"var(--chakra-fontSizes-xs)"}},defaultProps:{variant:"subtle",size:"sm"}},Input={baseStyle:{_invalid:{boxShadow:0}},variants:{outline:{display:"none"}},defaultProps:{variant:"outline"}},Link={baseStyle:({colorMode:e})=>{const r=e==="light";return{color:r?"blue.500":"blue.300",_hover:{textDecoration:"none",color:r?"blue.700":"blue.500"}}}},Modal={baseStyle:({colorMode:e})=>({dialog:{background:e==="light"?"white":"slate.800"}})},Drawer={baseStyle:({colorMode:e})=>({dialog:{background:e==="light"?"white":"slate.800"}})};var components={Card,Table,Badge,Input,Link,Modal,Drawer};const config={initialColorMode:"light",useSystemColorMode:!1},breakpoints={sm:"320px",md:"768px",lg:"960px",xl:"1280px","2xl":"1536px"},theme=extendTheme({config,colors,breakpoints,styles:{global:e=>({"html, body":{color:e.colorMode==="dark"?"gray.100":"gray.900",lineHeight:1.5},a:{color:e.colorMode==="dark"?"blue.300":"blue.500"},ul:{paddingRight:1}})},semanticTokens:{text:{default:"gray.900",_dark:"slate.400"},colors:{error:{default:"red.500",_dark:"red.200"},"gray.500":{default:"gray.500",_dark:"slate.400"}}},fonts:{body:"'Roboto', sans-serif",heading:"'Arial', sans-serif",mono:"monospace"},components:H(B({},components),{FormLabel:{baseStyle:{fontSize:"sm",marginBottom:1}},Avatar:{defaultProps:{colorScheme:"gray"}}})},withDefaultColorScheme({colorScheme:"gray",components:["Avatar"]}));ReactDOM.render(jsx(t.StrictMode,{children:jsx(ProvideAuth,{children:jsxs(ChakraProvider,{theme,children:[jsx(ColorModeScript,{initialColorMode:theme.config.initialColorMode}),jsx(App,{})]})})}),document.getElementById("root"));export{ticketStatusOptions as $,GET_COURSE_FEE_SUGGESTION as A,EMPTY_ERROR as B,Card$1 as C,DATE_OF_WEEKS as D,EnumCoursePlanerTypeEnum as E,EnumConfigSystemNames as F,GET_COURSES as G,ACCOUNT_FRAGMENT as H,GET_COURSE_PLAN as I,GET_PERSONAL_INFO as J,GET_STUDENT_INVOICE as K,Loading as L,EnumInvoiceStatus as M,NavLink as N,OPEN_STATUSES as O,AutoResizeTextarea as P,GET_COURSE as Q,RequestStudentToCourseTypeEnumeration as R,SKILLS as S,TextInput as T,GET_SCHOOLS as U,GET_SUBJECTS as V,GET_LEVELS as W,TicketCard as X,TicketFragment as Y,TICKET_STATUS_LOWERCASE as Z,useUser as _,useRouter as a,ExerciseStatus as a$,useResetQueries as a0,GROUP_ROLES as a1,ticketFilterTypeOptions as a2,TicketFormModal as a3,GET_LEAD_REPORT_DATA as a4,UserStatusEnum as a5,GET_STUDENT_SUPPORTED_BY_EC_FOR_DATA_TABLE as a6,userStatusOptions as a7,CREATE_REGISTER_SCHEDULE as a8,GET_SHIFT_REGISTER_SCHEDULE as a9,__vitePreload as aA,GET_FB_COMMENT as aB,GET_FB_POST as aC,LEAD_STATUS as aD,GET_LEAD_NOTE as aE,GET_LEADS as aF,UPDATE_LEAD_STATUS as aG,CREATE_LEAD_NOTE as aH,GET_WARDS_BY_DISTRICT as aI,GET_DISTRICTS as aJ,GET_CITIES as aK,GET_STREETS_BY_DISTRICT as aL,GET_JOBS as aM,GET_ECS as aN,REGEX_PHONE as aO,useListSources as aP,GET_USER_BY_PHONE as aQ,CREATE_APOINTMENT as aR,UPDATE_LEAD_EC as aS,useToggle as aT,LEAD_PERMISSION as aU,CreateLead as aV,useCombinedRefs as aW,EnumOrder as aX,blankStateCreateExercise as aY,CREATE_EXERCISE as aZ,ExerciseOptions as a_,CANCEL_REGISTER_SCHEDULE as aa,GET_SCHEDULE_DETAIL as ab,StyledTextContent as ac,CHECK_LIST_COPY_SCHEDULE as ad,EnumConfigFunctionType as ae,GET_TYPE_REGISTER as af,arrayToSelectOption as ag,GET_FUNCTION_ASSIGNED_TEACHER as ah,GET_LIST_REGISTER_SCHEDULE_OF_TEACHER as ai,useGetCenterByUser as aj,ScheduleTypeNotes as ak,COPY_REGISTER_SCHEDULE as al,MOCK_TEST_STATUS as am,Wrapper as an,MockTestStatusOptions as ao,EnumMockTestType as ap,tableDisplayRowsOptions as aq,UPLOAD_EXAM_DOCUMENT as ar,useDropzone as as,ZimVnPaths as at,ERROR_FILE_SIZE as au,ERROR_FILE_NOT_SUPPORT as av,colors as aw,OrderActionInput as ax,MockTestStatusEnum as ay,MOCK_TEST_PERMISSION as az,useBuildModeValue as b,GET_EXERCISE_PAGINATION as b0,GET_EXERCISE_TYPES as b1,GET_GRADES as b2,GET_LESSON_BY_CONFIG as b3,GET_SUB_CAT_BY_CAT_ID as b4,GET_QUESTION_TYPE as b5,GET_QUESTION_GROUP_ID as b6,UPLOAD_S3 as b7,SubfolderS3Enum as b8,useActiveGroupStore as b9,APPROVE_OR_REJECT_REQUEST_CANCEL as bA,Shift as bB,blankStateEventSelected as bC,MockTestSubjectsType as bD,CREATE_QUESTION_GROUP as ba,UPDATE_QUESTION_GROUP as bb,TipTap$1 as bc,questionTypeNoAnswerQuestion as bd,TableQuestion as be,StyledTextContentTipTap as bf,REMOVE_QUESTION_GROUP as bg,useDebounce as bh,StatusExerciseGrapNet as bi,GET_EXERCISE_DETAIL as bj,UPDATE_STATUS_EXERCISE as bk,UPDATE_EXERCISE as bl,useListScheduleContext as bm,GET_LIST_STUDENT_BY_EC_BY_FIELD as bn,MyBox as bo,REGEX_LINK as bp,Components as bq,SELF_STUDY_ROOM_ID as br,ROOMS as bs,CREATE_OR_UPDATE_REGISTER_SCHEDULE_DETAIL as bt,REOPEN_SCHEDULE_DETAIL as bu,EnumRequestCancelStatus as bv,defaultColor as bw,FORCE_CANCEL_REGISTER_SCHEDULE as bx,useListScheduleStore as by,OFF_ID as bz,appZimPaths as c,routePaths as d,dateToStringFormat as e,SearchInput as f,CourseFilter as g,DataTable as h,Select$1 as i,dateAddTime as j,LoadingAnimated as k,DateTimePicker$1 as l,ErrorFallback as m,EnumCourseTypeEnum as n,COURSE_STATUSES as o,GET_ROOMS as p,GET_ECS_BY_SCHOOL_ID as q,routes as r,GET_CURRICULUMS as s,GET_SHIFTS as t,useIsMounted as u,GET_SIZES as v,GET_TEACHERS as w,GET_SUB_CATS as x,StatusEnum as y,EnumLevelStatus as z};
