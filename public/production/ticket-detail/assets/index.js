var Qo=Object.defineProperty,Zo=Object.defineProperties;var Yo=Object.getOwnPropertyDescriptors;var At=Object.getOwnPropertySymbols;var sa=Object.prototype.hasOwnProperty,ia=Object.prototype.propertyIsEnumerable;var la=(e,n,a)=>n in e?Qo(e,n,{enumerable:!0,configurable:!0,writable:!0,value:a}):e[n]=a,b=(e,n)=>{for(var a in n||(n={}))sa.call(n,a)&&la(e,a,n[a]);if(At)for(var a of At(n))ia.call(n,a)&&la(e,a,n[a]);return e},A=(e,n)=>Zo(e,Yo(n));var Re=(e,n)=>{var a={};for(var s in e)sa.call(e,s)&&n.indexOf(s)<0&&(a[s]=e[s]);if(e!=null&&At)for(var s of At(e))n.indexOf(s)<0&&ia.call(e,s)&&(a[s]=e[s]);return a};import{g as E,o as Ko,A as Qt,O as Xo,I as Jo,r as Zt,M as er,c as tr,b as nr,a as ca,W as ar,T as or,d as da,s as rr,e as sr,f as ir,S as lr,h as m,j as t,i as cr,L as Ve,k as dr,u as T,l as g,F as kt,m as Yt,n as vt,p as wt,B as k,q as ua,t as pa,v as Q,V as ur,w as he,x as Kt,y as pr,z as ha,C as ga,D as ma,R as at,E as Se,G as fa,H as D,J as N,K as hr,N as gr,P as mr,Q as ve,U as fr,X as xr,Y as br,Z as ot,_ as We,$ as Ae,a0 as ee,a1 as yr,a2 as Qe,a3 as Er,a4 as Cr,a5 as It,a6 as Xt,a7 as pe,a8 as xa,a9 as ht,aa as ba,ab as Jt,ac as ce,ad as $r,ae as ya,af as Ea,ag as Tt,ah as Pt,ai as Ca,aj as _t,ak as Ot,al as $a,am as Ft,an as Ze,ao as we,ap as en,aq as Sr,ar as Ar,as as kr,at as vr,au as wr,av as Ir,aw as Tr,ax as Lt,ay as tn,az as Ie,aA as Nt,aB as Mt,aC as Pr,aD as Rt,aE as nn,aF as Ee,aG as _r,aH as Or,aI as Fr,aJ as Lr,aK as Nr,aL as Mr,aM as Rr,aN as Dr,aO as Br,aP as zr,aQ as jr,aR as qr,aS as Ur,aT as Hr,aU as Gr,aV as Sa,aW as Aa,aX as Vr,aY as rt,aZ as Ye,a_ as st,a$ as Ke,b0 as it,b1 as lt,b2 as Wr,b3 as ka,b4 as Qr,b5 as Zr,b6 as gt,b7 as Yr,b8 as Kr,b9 as Xr,ba as Jr,bb as es,bc as ts,bd as ns,be as as,bf as os,bg as rs,bh as va,bi as wa,bj as qe,bk as ss,bl as an,bm as is,bn as ls,bo as cs,bp as ds,bq as us,br as ps,bs as hs,bt as gs,bu as ms,bv as Ia,bw as fs,bx as xs,by as Ta,bz as bs,bA as ys,bB as Es,bC as Dt,bD as Te,bE as on,bF as rn,bG as Pa,bH as mt,bI as Cs,bJ as $s,bK as Ss,bL as As,bM as ks,bN as Ce,bO as vs,bP as ws,bQ as Is,bR as sn,bS as ln,bT as Ts,bU as _a,bV as ft,bW as Oa,bX as Ps,bY as _s,bZ as Os,b_ as Fs,b$ as Ls,c0 as Ns,c1 as Ms,c2 as ct,c3 as Pe,c4 as cn,c5 as dn,c6 as Fa,c7 as La,c8 as Na,c9 as Rs,ca as Ds,cb as Ma,cc as Bs,cd as zs,ce as js,cf as qs,cg as Us,ch as Hs,ci as un,cj as Ra,ck as Gs,cl as Vs,cm as Ws,cn as Qs,co as Zs,cp as Ys,cq as Ks,cr as Xs,cs as Js,ct as ei,cu as pn,cv as ti,cw as ni,cx as ai,cy as oi,cz as ri,cA as si,cB as ii,cC as li,cD as Da,cE as ci,cF as Ba,cG as za,cH as ja,cI as di,cJ as ui,cK as pi,cL as hi,cM as gi,cN as mi,cO as fi,cP as xi,cQ as bi,cR as yi,cS as Ei,cT as Ci,cU as $i,cV as Si,cW as Ai,cX as ki,cY as vi,cZ as wi,c_ as Ii,c$ as Ti,d0 as Pi}from"./vendor.js";const _i=function(){const n=document.createElement("link").relList;if(n&&n.supports&&n.supports("modulepreload"))return;for(const r of document.querySelectorAll('link[rel="modulepreload"]'))s(r);new MutationObserver(r=>{for(const o of r)if(o.type==="childList")for(const l of o.addedNodes)l.tagName==="LINK"&&l.rel==="modulepreload"&&s(l)}).observe(document,{childList:!0,subtree:!0});function a(r){const o={};return r.integrity&&(o.integrity=r.integrity),r.referrerpolicy&&(o.referrerPolicy=r.referrerpolicy),r.crossorigin==="use-credentials"?o.credentials="include":r.crossorigin==="anonymous"?o.credentials="omit":o.credentials="same-origin",o}function s(r){if(r.ep)return;r.ep=!0;const o=a(r);fetch(r.href,o)}};_i();const Oi="modulepreload",qa={},Fi="/",$e=function(n,a){return!a||a.length===0?n():Promise.all(a.map(s=>{if(s=`${Fi}${s}`,s in qa)return;qa[s]=!0;const r=s.endsWith(".css"),o=r?'[rel="stylesheet"]':"";if(document.querySelector(`link[href="${s}"]${o}`))return;const l=document.createElement("link");if(l.rel=r?"stylesheet":Oi,r||(l.as="script",l.crossOrigin=""),l.href=s,document.head.appendChild(l),r)return new Promise((i,c)=>{l.addEventListener("load",i),l.addEventListener("error",c)})})).then(()=>n())};const xt=E`
  fragment Post on Post {
    id
    title
    slug
    excerpt
    content
    status
    numOfComments
    categories {
      id
      title
      slug
    }
    tags {
      id
      name
      slug
    }
    postGroups {
      id
      title
      slug
    }
    publishedAt
    featureMedias {
      id
      type
      path
      variants {
        id
        type
        width
        height
        path
      }
    }
    user {
      id
      role
      username
      fullName
      phone
      gender
      avatar
    }
    censor {
      id
      fullName
    }
    createdAt
    updatedAt
    numOfContextualLink
  }
`;E`
  query posts(
    $after: String
    $before: String
    $first: Int
    $last: Int
    $categorySlugs: [String]
    $relatedPost: ObjectID
  ) @api(name: "zim") {
    posts(
      after: $after
      before: $before
      first: $first
      last: $last
      categorySlugs: $categorySlugs
      relatedPost: $relatedPost
    ) {
      totalCount
      edges {
        cursor
        node {
          ...Post
        }
      }
      pageInfo {
        startCursor
        endCursor
        hasNextPage
        hasPreviousPage
      }
    }
  }
  ${xt}
`;E`
  query ($idOrSlug: String!) @api(name: "zim") {
    post(idOrSlug: $idOrSlug) {
      ...Post
      seo {
        title
        description
        ogDescription
        ogImage {
          id
          path
        }
        ogTitle
        publisher
        noIndex
        noFollow
        canonicalUrl
        customHeadHtml
      }
      rejectReason
    }
  }
  ${xt}
`;E`
  query postPagination(
    $searchString: String
    $page: Int
    $limit: Int
    $categoryIds: [ObjectID]
    $tagIds: [ObjectID]
    $postGroupIds: [ObjectID]
    $userIds: [Int]
    $statuses: [PostStatus]
    $censorUserIds: [Int]
    $orderBy: PostOrderBy
    $order: OrderDirection
  ) @api(name: "zim") {
    postPagination(
      searchString: $searchString
      page: $page
      limit: $limit
      categoryIds: $categoryIds
      tagIds: $tagIds
      postGroupIds: $postGroupIds
      userIds: $userIds
      statuses: $statuses
      censorUserIds: $censorUserIds
      orderBy: $orderBy
      order: $order
    ) {
      hasNextPage
      hasPrevPage
      limit
      nextPage
      prevPage
      totalDocs
      totalPages
      docs {
        ...Post
      }
    }
  }
  ${xt}
`;E`
  mutation updatePost($postId: ObjectID!, $input: UpdatePostData!)
  @api(name: "zim") {
    updatePost(input: $input, postId: $postId) {
      ...Post
    }
  }
  ${xt}
`;E`
  mutation createPost($input: CreatePostData!) @api(name: "zim") {
    createPost(input: $input) {
      ...Post
    }
  }
  ${xt}
`;E`
  mutation createTag($name: String!, $description: String) @api(name: "zim") {
    createTag(description: $description, name: $name) {
      id
      name
      slug
      description
    }
  }
`;E`
  mutation publishPost($postId: ObjectID!) @api(name: "zim") {
    publishPost(postId: $postId) {
      id
      title
      status
    }
  }
`;E`
  mutation unpublishPost($postId: ObjectID!) @api(name: "zim") {
    unpublishPost(postId: $postId) {
      id
      title
      status
    }
  }
`;E`
  mutation requestPostForApproval($postId: ObjectID!) @api(name: "zim") {
    requestPostForApproval(postId: $postId) {
      id
      title
      status
    }
  }
`;E`
  mutation approveOrRejectPost(
    $postId: ObjectID!
    $status: ApproveOrRejectPostStatus
    $rejectReason: String
    $updatedContent: String
  ) @api(name: "zim") {
    approveOrRejectPost(
      postId: $postId
      status: $status
      rejectReason: $rejectReason
      updatedContent: $updatedContent
    ) {
      id
      title
      status
      rejectReason
      content
    }
  }
`;E`
  mutation reassignPost($postId: ObjectID!, $userId: Int!) @api(name: "zim") {
    reassignPost(postId: $postId, userId: $userId) {
      id
      user {
        id
        birthday
        email
        fullName
        avatar
        roleId
        role
      }
    }
  }
`;E`
  mutation signInV2($username: String!, $password: String!) @api(name: "zim") {
    signInV2(username: $username, password: $password) {
      token
      expired_at
      refreshToken
    }
  }
`;const Li=E`
  query @api(name: "zim") {
    me {
      id
      role
      roleId
      username
      fullName
      phone
      birthday
      gender
      avatar
      email
      canCensor
    }
  }
`,Ni=E`
  mutation signOut @api(name: "zim") {
    signOut
  }
`;E`
  mutation createPostCategory($input: PostCategoryInput!) @api(name: "zim") {
    createPostCategory(input: $input) {
      id
      title
      slug
      description
    }
  }
`;const Ua=E`
  fragment CategoryFields on PostCategory {
    id
    title
    slug
    parentCategory {
      id
      title
      slug
    }
    description
  }
`;E`
  query postCategoriesTree @api(name: "zim") {
    postCategoriesTree {
      ...CategoryFields
      children {
        ...CategoryFields
        children {
          ...CategoryFields
          children {
            ...CategoryFields
          }
        }
      }
    }
  }
  ${Ua}
`;E`
  query postCategories($flatten: Boolean, $search: String, $parent: ObjectID)
  @api(name: "zim") {
    postCategories(search: $search, parent: $parent, flatten: $flatten) {
      ...CategoryFields
      children {
        ...CategoryFields
      }
    }
  }
  ${Ua}
`;E`
  mutation updatePostCategory($id: ObjectID!, $input: UpdatePostCategoryInput!)
  @api(name: "zim") {
    updatePostCategory(id: $id, input: $input) {
      id
      title
      slug
      description
    }
  }
`;E`
  mutation deletePostCategory($id: ObjectID!) @api(name: "zim") {
    deletePostCategory(id: $id)
  }
`;E`
  query comments(
    $type: CommentType!
    $after: String
    $before: String
    $first: Int
    $last: Int
    $refId: String!
    $parent: ObjectID
  ) @api(name: "zim") {
    comments(
      type: $type
      after: $after
      before: $before
      first: $first
      last: $last
      refId: $refId
      parent: $parent
    ) {
      totalCount
      edges {
        cursor
        node {
          ... on PostComment {
            id
            user {
              id
              role
              username
              fullName
              phone
              birthday
              gender
              avatar
            }
            content
            parent {
              id
              content
              user {
                id
                role
                username
                fullName
                phone
                birthday
                gender
                avatar
              }
            }
            liked
            numOfLikes
            createdAt
            updatedAt
          }
          __typename
        }
      }
      pageInfo {
        startCursor
        endCursor
        hasNextPage
        hasPreviousPage
      }
    }
  }
`;E`
  mutation postComment(
    $content: String!
    $type: CommentType
    $ref: ObjectID!
    $parent: ObjectID
  ) @api(name: "zim") {
    postComment(content: $content, type: $type, ref: $ref, parent: $parent) {
      ... on PostComment {
        id
        content
        user {
          id
          role
          username
          fullName
          phone
          birthday
          gender
          avatar
        }
        parent {
          id
          content
          user {
            id
            role
            username
            fullName
            phone
            birthday
            gender
            avatar
          }
          liked
          numOfLikes
          createdAt
          updatedAt
        }
        liked
        numOfLikes
        createdAt
        updatedAt
      }
      __typename
    }
  }
`;E`
  mutation likeOrUnlikeComment($commentId: ObjectID!, $liked: Boolean!)
  @api(name: "zim") {
    likeOrUnlikeComment(commentId: $commentId, liked: $liked) {
      ... on PostComment {
        id
        liked
        numOfLikes
      }
    }
  }
`;const Ha=E`
  fragment Course on CoursePlanType {
    id
    slug
    start_date
    program {
      id
      name
      cat {
        id
        name
      }
      subject {
        id
        name
      }
      program_type {
        id
        name
      }
      level {
        subject_id
        id
        name
        graduation
        status
      }
      fee
      status
      sub_cats {
        id
        cat_id
        name
        status
        parent_id
      }
    }
    subject {
      id
      name
      status
    }
    level_out {
      subject_id
      id
      name
      graduation
    }
    type
    size {
      id
      name
      size
      status
    }
    school {
      id
      city {
        id
      }
      district {
        id
      }
      name
      address
      phone
      color_code
      lat
      lon
      status
    }
    curriculum {
      id
      name
      program {
        id
      }
      shift_minute
      total_lesson
      status
    }
    shifts {
      id
      start_time
      end_time
      status
      shift_minute
    }
    day_of_weeks {
      id
      name
    }
    config_fee {
      detail_name
      detail_id
      type
      fee
    }
    fee
    status
    ec_id
    teacher_lead_id
    description
    featured_image
  }
`,Du=E`
  query getListCoursePagination(
    $order_by: String
    $order: String
    $q: String
    $school_id: Int
    $city_id: Int
    $program_id: Int
    $program_type_id: Int
    $limit: Int
    $cat_id: Int
    $level_id: Int
    $subject_id: Int
    $status: [CourseStatusEnum]
    $type: EnumCourseTypeEnum
    $fromdate: String
    $todate: String
    $page: Int
    $bussiness_partner_id: Int
  ) @api(name: "appZim") {
    getListCoursePagination(
      q: $q
      status: $status
      subject_id: $subject_id
      level_id: $level_id
      cat_id: $cat_id
      school_id: $school_id
      city_id: $city_id
      program_id: $program_id
      program_type_id: $program_type_id
      type: $type
      fromdate: $fromdate
      todate: $todate
      order_by: $order_by
      order: $order
      page: $page
      limit: $limit
      bussiness_partner_id: $bussiness_partner_id
    ) {
      hasNextPage
      hasPrevPage
      limit
      page
      pageNext
      pagePrev
      pagingCounter
      totalPages
      totalDocs
      docs {
        id
        slug
        name
        start_date
        subcats {
          id
          cat_id
          name
          status
          parent_id
        }
        subject {
          id
          name
          status
        }
        school {
          id
          name
        }

        shifts {
          id
          start_time
          end_time
        }
        level_out {
          subject_id
          id
          name
          graduation
        }
        day_of_weeks {
          id
          name
        }
        size {
          id
          name
          size
          status
        }
        fee
        status
        total_register
        total_paid
        config_fee {
          type
          fee
          detail_id
          detail_name
        }
        end_date
        type
        ec {
          id
          full_name
        }
        teachers {
          id
          user_name
          full_name
        }
      }
    }
  }
`;E`
  query coursesPlan($offset: Int, $limit: Int, $school_id: Int, $city_id: Int)
  @api(name: "appZim") {
    coursesPlan(
      school_id: $school_id
      city_id: $city_id
      offset: $offset
      limit: $limit
    ) {
      totalCount
      edges {
        node {
          ...Course
        }
      }
      pageInfo {
        hasNextPage
        hasPreviousPage
      }
    }
  }
  ${Ha}
`;E`
  query coursesPlan($limit: Int) @api(name: "appZim") {
    coursesPlan(status: incoming, limit: $limit) {
      edges {
        node {
          id
          slug
        }
      }
    }
  }
`;E`
  query coursePlan($idOrSlug: String!) @api(name: "appZim") {
    coursePlan(idOrSlug: $idOrSlug) {
      ...Course
    }
  }
  ${Ha}
`;E`
  mutation createAdvisory(
    $name: String
    $phone: String!
    $email: String!
    $city_id: Int!
    $gclid: String
    $fbclid: String
    $link: String
    $note: String
  ) @api(name: "appZim") {
    createAdvisory(
      name: $name
      phone: $phone
      email: $email
      city_id: $city_id
      gclid: $gclid
      fbclid: $fbclid
      link: $link
      note: $note
    ) {
      success
      error
      message
    }
  }
`;E`
  mutation checkout($input: CheckoutInput!) @api(name: "zim") {
    checkout(input: $input) {
      paymentUrl
      order {
        id
        status
        paymentMethod
        paymentStatus
        shippingMethod
        orderId
        email
        shippingAddress {
          recipientName
          recipientPhone
          email
          street1
          street2
          city
          state
          zip
          country
        }
        total
      }
    }
  }
`;const Ga=E`
  fragment OrderBaseFields on Order {
    id
    status
    paymentMethod
    paymentStatus
    shippingMethod
    orderId
    status
    email
    shippingAddress {
      recipientName
      recipientPhone
      email
      street1
      street2
      city
      state
      zip
      country
    }
    total
    createdAt
    updatedAt
  }
`,Mi=E`
  fragment OrderDetailFields on Order {
    ...OrderBaseFields
    user {
      id
      fullName
    }
    billingAddress {
      recipientName
      recipientPhone
      email
      street1
      street2
      city
      state
      zip
      country
    }
    attachments {
      id
      filename
      filepath
    }
    total
    totalPreDiscount
    discount
    coupons
    items {
      quantity
      total
      preTaxTotal
      price
      salePrice
      tax
      type
      item {
        ... on SimpleProduct {
          id
          name
          slug
          images {
            id
            path
          }
          sku
          attributes {
            isVariantAttribute
            attribute {
              id
              name
            }
            value
          }
        }
        ... on Post {
          id
          excerpt
          featureMedias {
            id
            path
          }
          title
          slug
        }
        ... on MockTestItem {
          data
        }
      }
    }
  }
  ${Ga}
`;E`
  query order($idOrCode: String!) @api(name: "zim") {
    order(idOrCode: $idOrCode) {
      ...OrderBaseFields
      ...OrderDetailFields
    }
  }
  ${Mi}
`;E`
  query orderPagination(
    $searchString: String
    $statuses: [OrderStatus]
    $userIds: [Int]
    $orderBy: OrderOrderBy
    $order: OrderDirection
    $page: Int
    $limit: Int
    $itemType: [CheckoutProduct]
  ) @api(name: "zim") {
    orderPagination(
      searchString: $searchString
      statuses: $statuses
      userIds: $userIds
      order: $order
      orderBy: $orderBy
      page: $page
      limit: $limit
      itemType: $itemType
    ) {
      totalPages
      totalDocs
      docs {
        ...OrderBaseFields
      }
    }
  }
  ${Ga}
`;E`
  query tags(
    $after: String
    $before: String
    $first: Int
    $last: Int
    $search: String
  ) @api(name: "zim") {
    tags(
      after: $after
      before: $before
      first: $first
      last: $last
      search: $search
    ) {
      edges {
        node {
          id
          name
          description
          slug
        }
      }
      pageInfo {
        hasNextPage
        hasPreviousPage
        startCursor
        endCursor
      }
    }
  }
`;E`
  query postCategories($search: String, $parent: ObjectID) @api(name: "zim") {
    postCategories(search: $search, parent: $parent) {
      id
      title
      slug
      parentCategory {
        id
        title
        slug
      }
      children {
        id
        title
        slug
        parentCategory {
          id
          title
          slug
        }
      }
    }
  }
`;const Xe=E`
  fragment ProductBaseField on BaseProduct {
    ... on SimpleProduct {
      id
      sku
      name
      slug
      regularPrice
      salePrice
      salePriceFrom
      salePriceTo
      description
      shortDescription
      numOfContextualLink
      attributes {
        id
        attribute {
          id
          name
          type
          options
        }
        value
        isVariantAttribute
      }
      parentProduct {
        id
      }
      price
      categories {
        id
        title
        slug
      }
      seo {
        title
        description
        ogDescription
        ogImage {
          id
          path
        }
        ogTitle
        publisher
        noIndex
        noFollow
        canonicalUrl
        customHeadHtml
      }
      images {
        id
        path
      }
      status
      productType
      tags {
        id
        name
        description
        slug
      }
      downloadable
      attributes {
        attribute {
          id
          type
          name
          options
        }
        value
      }
      inStock
      inventories {
        id
        inStock
        manageStock
        reserved
        storeId
        inventory
        store {
          id
          name
        }
      }
      createdAt
      updatedAt
    }
    ... on VariableProduct {
      id
      sku
      name
      slug
      categories {
        id
        title
        slug
      }
      images {
        id
        filename
        path
      }
      description
      shortDescription
      status
      minPrice
      maxPrice
      minRegularPrice
      maxRegularPrice
      productType
      inStock
      variationAttributes {
        id
        name
        type
        options
      }
      createdAt
      updatedAt
      seo {
        title
        description
        ogDescription
        ogImage {
          id
          path
        }
        ogTitle
        publisher
        noIndex
        noFollow
        canonicalUrl
        customHeadHtml
      }
    }
  }
`,bt=E`
  fragment ProductDetailFields on BaseProduct {
    ...ProductBaseField
    ... on SimpleProduct {
      tags {
        id
        name
      }
      downloadable
      downloads {
        id
        filename
        path
      }

      attributes {
        id
        attribute {
          id
          name
          type
          options
        }
        value
        isVariantAttribute
        media {
          id
          type
          filename
          path
        }
      }
      parentProduct {
        id
        slug
        name
        status
        variationAttributes {
          id
          name
          type
          options
        }
      }
    }
    ... on VariableProduct {
      childCount
      childPublishedCount
      attributes {
        id
        attribute {
          id
          name
          type
          options
        }
        value
        media {
          id
          type
          filename
          path
        }
      }
      tags {
        id
        name
      }
    }
  }
  ${Xe}
`;E`
  query product($id: ObjectID!) @api(name: "zim") {
    productAdmin(id: $id) {
      ...ProductBaseField
      ...ProductDetailFields
    }
  }
  ${bt}
`;E`
  query productPagination(
    $searchString: String
    $statuses: [ProductStatus]
    $orderBy: ProductOrderBy
    $order: OrderDirection
    $page: Int
    $limit: Int
    $parentProduct: ObjectID
    $tagSlugs: [String]
    $flatten: Boolean
    $simpleOnly: Boolean
    $tagIds: [ObjectID]
  ) @api(name: "zim") {
    productPagination(
      searchString: $searchString
      statuses: $statuses
      parentProduct: $parentProduct
      order: $order
      orderBy: $orderBy
      page: $page
      limit: $limit
      tagSlugs: $tagSlugs
      flatten: $flatten
      simpleOnly: $simpleOnly
      tagIds: $tagIds
    ) {
      totalPages
      totalDocs
      docs {
        ...ProductBaseField
      }
    }
  }
  ${Xe}
`;E`
  query publishedProducts(
    $search: String
    $after: String
    $before: String
    $first: Int
    $last: Int
  ) @api(name: "zim") {
    publishedProducts(
      after: $after
      before: $before
      first: $first
      last: $last
      search: $search
    ) {
      totalCount
      pageInfo {
        hasNextPage
        hasPreviousPage
        startCursor
        endCursor
      }
      edges {
        cursor
        node {
          ...ProductBaseField
        }
      }
    }
  }
  ${Xe}
`;E`
  query productVariants($parentProductId: ObjectID!) @api(name: "zim") {
    productVariants(parentProductId: $parentProductId) {
      ...ProductBaseField
    }
  }
  ${Xe}
`;E`
  query (
    $searchString: String
    $excludeIds: [ObjectID]
    $page: Int
    $limit: Int
  ) @api(name: "zim") {
    productAttributePagination(
      page: $page
      limit: $limit
      excludeIds: $excludeIds
      searchString: $searchString
    ) {
      totalDocs
      docs {
        id
        name
        options
        type
      }
    }
  }
`;E`
  query ($id: ObjectID!) @api(name: "zim") {
    productAttribute(id: $id) {
      id
      name
      options
      type
    }
  }
`;E`
  query @api(name: "zim") {
    productCategoriesTree {
      id
      title
      slug
      parentCategory {
        id
        title
        slug
      }
      children {
        id
        title
        slug
        parentCategory {
          id
          title
          slug
        }
      }
    }
  }
`;E`
  query productCategories($search: String, $parent: ObjectID)
  @api(name: "zim") {
    productCategories(search: $search, parent: $parent) {
      id
      title
      slug
      parentCategory {
        id
        title
        slug
      }
      children {
        id
        title
        slug
        parentCategory {
          id
          title
          slug
        }
      }
    }
  }
`;E`
  query slugExist($slug: String!, $type: SlugType, $refId: ObjectID)
  @api(name: "zim") {
    slugExists(type: $type, slug: $slug, refId: $refId)
  }
`;E`
  query titleExists($title: String!, $type: SlugType, $refId: ObjectID)
  @api(name: "zim") {
    titleExists(type: $type, title: $title, refId: $refId)
  }
`;E`
  query postViewReport(
    $startTime: DateTime
    $endTime: DateTime
    $page: Int
    $limit: Int
    $userId: Int
  ) @api(name: "zim") {
    postViewReport(
      startTime: $startTime
      endTime: $endTime
      page: $page
      limit: $limit
      userId: $userId
    ) {
      totalDocs
      docs {
        post {
          id
          title
          slug
          user {
            id
            fullName
          }
        }
        viewCount
      }
    }
  }
`;E`
  query generalPostViewByDate(
    $range: FromToDateInput
    $dateViewMode: DateViewMode
  ) @api(name: "zim") {
    generalPostViewByDate(range: $range, dateViewMode: $dateViewMode) {
      label
      viewCount
      postCount
    }
  }
`;E`
  query postViewReportByUser(
    $startTime: DateTime
    $endTime: DateTime
    $page: Int
    $limit: Int
  ) @api(name: "zim") {
    postViewReportByUser(
      startTime: $startTime
      endTime: $endTime
      page: $page
      limit: $limit
    ) {
      totalDocs
      docs {
        user {
          id
          fullName
        }
        postCount
        viewCount
      }
    }
  }
`;E`
  query postReportBySpecificUser(
    $userId: Int!
    $startTime: DateTime
    $endTime: DateTime
  ) @api(name: "zim") {
    postReportBySpecificUser(
      startTime: $startTime
      endTime: $endTime
      userId: $userId
    ) {
      totalLifetimePosts
      totalLifetimePublishedPosts
      totalLifetimeViews
      totalPosts
      totalPublishedPosts
    }
  }
`;E`
  query postReportByStatus($userId: Int) @api(name: "zim") {
    postReportByStatus(userId: $userId) {
      status
      count
    }
  }
`;E`
  query postByUser(
    $page: Int
    $limit: Int
    $order: OrderDirection
    $orderBy: PostByUserSort
    $userFilter: UserWithPostUserFilter
  ) @api(name: "zim") {
    postByUser(
      page: $page
      limit: $limit
      order: $order
      orderBy: $orderBy
      userFilter: $userFilter
    ) {
      limit
      page
      totalPages
      totalDocs
      docs {
        user {
          id
          email
          fullName
          avatar
          role
        }
        reviewsAverage
        postCount
        viewCount
        employeeInfo {
          id
          showInFrontPage
          position
          priority
          userId
          user {
            id
            avatar
            role
            username
            fullName
          }
        }
      }
    }
  }
`;E`
  query commentsPagination(
    $type: CommentType!
    $refId: String!
    $parent: ObjectID
    $page: Int
  ) @api(name: "zim") {
    commentsPagination(
      type: $type
      refId: $refId
      parent: $parent
      page: $page
    ) {
      totalDocs
      totalPages
      limit
      page
      docs {
        ... on PostComment {
          id
          content
          numOfLikes
          createdAt
          numOfChildComments
          user {
            id
            email
            avatar
            fullName
          }
        }
      }
    }
  }
`;E`
  query comments(
    $first: Int
    $type: CommentType!
    $refId: String!
    $parent: ObjectID
    $after: String
  ) @api(name: "zim") {
    comments(
      type: $type
      refId: $refId
      parent: $parent
      first: $first
      after: $after
    ) {
      pageInfo {
        hasNextPage
        hasPreviousPage
        endCursor
        startCursor
      }
      totalCount
      edges {
        node {
          ... on PostComment {
            id
            content
            numOfLikes
            createdAt
            numOfChildComments
            user {
              id
              email
              avatar
              fullName
            }
          }
        }
        cursor
      }
    }
  }
`;E`
  query getUserList($roleIds: [Int], $search: String) @api(name: "zim") {
    users(role_ids: $roleIds, search: $search) {
      totalDocs
      docs {
        id
        fullName
        role
      }
    }
  }
`;E`
  query user($userId: Int!) @api(name: "zim") {
    user(id: $userId) {
      id
      fullName
      avatar
      role
      roleId
      username
      birthday
      gender
    }
  }
`;E`
  query user($userId: Int!) @api(name: "zim") {
    user(id: $userId) {
      id
      fullName
      avatar
      role
      roleId
      username
      birthday
      gender
      email
      phone
      moreInfo
    }
  }
`;const Va=E`
  query getUserById($userId: Int!) @api(name: "appZim") {
    getUserById(id: $userId) {
      id
      full_name
      avatar
      role_name
      address
      email
      phone
      status_online
      supporter {
        id
        full_name
        avatar
        role_name
      }
      more_info {
        facebook
      }
      courseStudents {
        id
        course {
          status
          id
          slug
          name
        }
      }
    }
  }
`,Ri=E`
  query getUserById($userId: Int!) @api(name: "appZim") {
    getUserById(id: $userId) {
      id
      supporter {
        id
      }
    }
  }
`,Wa=E`
  query getAccountChatPagination(
    $searchString: String
    $roles: [EnumRoleType]
    $page: Int
    $limit: Int
  ) @api(name: "appZim") {
    getAccountChatPagination(
      input: { q: $searchString, roles: $roles, page: $page, limit: $limit }
    ) {
      hasNextPage
      hasPrevPage
      limit
      page
      pageNext
      pagePrev
      pagingCounter
      totalPages
      totalDocs
      docs {
        user {
          id
          full_name
          role_id
          role_name
          status
          avatar
          status_online
        }
      }
    }
  }
`;E`
  query getListStudentsPagination($input: TicketGetListStudentsInputType)
  @api(name: "appZim") {
    getListStudentsPagination(input: $input) {
      hasNextPage
      hasPrevPage
      limit
      page
      pageNext
      pagePrev
      pagingCounter
      totalPages
      totalDocs
      docs {
        supportUId
        userId
        schoolAddress
        course {
          name
        }
        status
        user {
          id
          user_name
          full_name
          phone
          address
          email
          role_id
          role_name
          status
          avatar
        }
      }
    }
  }
`;const Bu=E`
  query getListStudentsPagination($input: TicketGetListStudentsInputType)
  @api(name: "appZim") {
    getListStudentsPagination(input: $input) {
      hasNextPage
      hasPrevPage
      limit
      page
      pageNext
      pagePrev
      pagingCounter
      totalPages
      totalDocs
      docs {
        supportUId
        userId
        schoolAddress
        course {
          name
          code
          #          day_of_weeks {
          #            id
          #            name
          #          }
          #          shifts {
          #            id
          #            start_time
          #            end_time
          #          }
          ec {
            id
            full_name
          }
        }
        status
        user {
          id
          user_name
          full_name
          phone
          address
          email
          role_id
          role_name
        }
      }
    }
  }
`;E`
  query getListStudentByEc($input: AccountByEcInputType) @api(name: "appZim") {
    getListStudentByEc(input: $input) {
      page
      hasNextPage
      totalPages
      totalDocs
      docs {
        id
        full_name
        avatar
        phone
        address
        email
        status
        supporter
      }
    }
  }
`;const Di=(e=[])=>E`
  query getListStudentByEc($input: AccountByEcInputType)
  @api(name: "appZim") {
    getListStudentByEc(input: $input) {
      page
      hasNextPage
      docs {
        id
        ${e.join()}
      }
    }
  }
`;E`
  query getPersonalInfo($id: Int!) @api(name: "appZim") {
    getPersonalInfo(id: $id) {
      id
      userName
      sourceOfCustomer {
        id
        sourceOfCustomer
      }
      supporter {
        isRequestCancel
        id
        user_name
        full_name
        phone
        address
        email
      }
      city {
        id
        name
        status
      }
      district {
        id
        name
        status
      }
      ward {
        id
        wardName
      }
      street {
        id
        wardName
      }
      homeNumber
      fullName
      phone
      email
      birthday
      address
      identityCard
      placeOfIssue {
        id
        name
        status
      }
      identityCardDate
      job {
        id
        jobName
      }
      workPlace
      academicPurposes {
        id
        academicPurposesName
      }
      noteHome
      typeOfEducation
      scoreIn
      scoreOut
      status
      statusId
    }
  }
`;const zu=E`
  query searchPhone($phone: String!) @api(name: "appZim") {
    searchPhone(phone: $phone) {
      id
      user_name
      full_name
      phone
      address
      email
      role_id
      role_name
      status
      status_online
      avatar
      supporter
      student_more_info {
        identity_card_city_id
        note_home
        birthday
        city_id
        city_name
        district_id
        district_name
        ward_id
        ward_name
        street_id
        street_name
        home_number
        source_id
        source_name
        job_id
        job_name
        identity_card
        identity_card_city_name
        identity_card_date
        learning_status_id
        work_place
        learning_status_name
      }
    }
  }
`;E`
  query isUserExist($field: ExistFieldCheck!, $value: String!)
  @api(name: "zim") {
    isUserExist(field: $field, value: $value)
  }
`;const Qa=E`
  fragment AdvertisementBaseFields on Advertisement {
    id
    type
    status
    title
    description
    image {
      id
      filename
      path
      width
      height
    }
    url
    clicks
    showIn
    startDate
    endDate
    createdAt
    updatedAt
  }
`,Bi=E`
  fragment AdvertisementDetailFields on Advertisement {
    ...AdvertisementBaseFields
    postCategories {
      id
      title
    }
    productCategories {
      id
      title
    }
    posts {
      id
      title
    }
    tags {
      id
      name
    }
    products {
      ... on SimpleProduct {
        id
        name
      }
      ... on VariableProduct {
        id
        name
      }
    }
    excludeUrls
    position
    tagHtml
    tagPosition
  }
  ${Qa}
`;E`
  query advertisementPagination(
    $page: Int
    $limit: Int
    $order: OrderDirection
    $orderBy: AdvertisementSortBy
    $statuses: [AdvertisementStatus]
  ) @api(name: "zim") {
    advertisementPagination(
      page: $page
      limit: $limit
      order: $order
      orderBy: $orderBy
      statuses: $statuses
    ) {
      totalDocs
      totalPages
      docs {
        ...AdvertisementBaseFields
      }
    }
  }
  ${Qa}
`;E`
  query advertisement($id: ObjectID!) @api(name: "zim") {
    advertisement(id: $id) {
      ...AdvertisementDetailFields
    }
  }
  ${Bi}
`;const _e=E`
  query supportConversations(
    $status: ConversationStatus
    $search: String
    $offset: Int
    $limit: Int
  ) @api(name: "chat") {
    supportConversations(
      status: $status
      search: $search
      offset: $offset
      limit: $limit
    ) {
      hasNextPage
      hasPrevPage
      limit
      nextPage
      page
      prevPage
      docs {
        id
        name
        group
        participants {
          userId
          fullName
          nickname
          avatar
          isGuest
        }
        createdAt
        updatedAt
        typing
        usersTyping
        pinnedAt
        blockedBy
        isUnread
        lastMessage {
          id
          text
          from
          to
          type
          attachments {
            type
            attachmentId
            attachment {
              id
              path
              fullUrl
              type
            }
          }
          loading @client
          error @client
          seenBy
          createdAt
          updatedAt
          deletedAt
          callPayload
        }
      }
    }
  }
`,me=E`
  query conversations(
    $status: ConversationStatus
    $search: String
    $offset: Int
    $limit: Int
    $group: String
  ) @api(name: "chat") {
    conversations(
      status: $status
      search: $search
      offset: $offset
      limit: $limit
      group: $group
    ) {
      hasNextPage
      hasPrevPage
      limit
      nextPage
      page
      prevPage
      docs {
        id
        name
        group
        participants {
          userId
          fullName
          nickname
          avatar
          isGuest
          online
        }
        createdAt
        updatedAt
        typing
        usersTyping
        pinnedAt
        blockedBy
        isUnread
        lastMessage {
          id
          text
          from
          to
          type
          attachments {
            type
            attachmentId
            attachment {
              id
              path
              fullUrl
              type
            }
          }
          loading @client
          error @client
          seenBy
          createdAt
          updatedAt
          deletedAt
          callPayload
        }
      }
    }
  }
`,zi=E`
  query conversationDetail($id: ObjectID, $userId: String) @api(name: "chat") {
    conversationDetail(id: $id, userId: $userId) {
      id
      name
      group
      typing
      usersTyping
      isUnread
      participants {
        userId
        fullName
        nickname
        avatar
        isGuest
        online
        roleId
      }
      owner {
        userId
        fullName
      }
      tags {
        id
        name
        color
      }
    }
  }
`,Be=E`
  query messages(
    $conversationId: ObjectID
    $before: ObjectID
    $after: ObjectID
    $userId: String
    $offset: Int
    $limit: Int
  ) @api(name: "chat") {
    messages(
      conversationId: $conversationId
      before: $before
      after: $after
      userId: $userId
      offset: $offset
      limit: $limit
    ) {
      hasNextPage
      hasPrevPage
      limit
      nextPage
      page
      prevPage
      docs {
        id
        text
        from
        to
        type
        attachments {
          type
          attachmentId
          attachment {
            id
            path
            fullUrl
            type
          }
        }
        conversation {
          id
          participants {
            userId
            avatar
            fullName
          }
        }
        loading @client
        error @client
        seenBy
        createdAt
        updatedAt
        deletedAt
        callPayload
      }
    }
  }
`,ji=E`
  query notes(
    $conversationId: ObjectID
    $userId: String
    $page: Int
    $limit: Int
  ) @api(name: "chat") {
    notes(
      conversationId: $conversationId
      userId: $userId
      page: $page
      limit: $limit
    ) {
      hasNextPage
      hasPrevPage
      limit
      nextPage
      page
      pagingCounter
      prevPage
      totalDocs
      totalPages
      docs {
        id
        content
        createdAt
        createdBy
        owner {
          userId
          fullName
          avatar
        }
      }
    }
  }
`,qi=E`
  query getAttachmentsInConversation(
    $conversationId: ObjectID
    $userId: String
    $offset: Int
    $limit: Int
  ) @api(name: "chat") {
    getAttachmentsInConversation(
      conversationId: $conversationId
      userId: $userId
      offset: $offset
      limit: $limit
    ) {
      hasNextPage
      hasPrevPage
      limit
      nextPage
      page
      pagingCounter
      prevPage
      totalDocs
      totalPages
      docs {
        id
        attachments {
          type
          attachmentId
          attachment {
            id
            path
            fullUrl
            type
          }
        }
      }
    }
  }
`,Bt=E`
  query @api(name: "chat") {
    statisticMessageUnread {
      zimians
      student
      customer
      open
      following
      another
      isMarkSeen
    }
  }
`,Ui=E`
  query getListTag($page: Int, $limit: Int) @api(name: "chat") {
    getListTag(page: $page, limit: $limit) {
      docs {
        id
        name
        color
      }
    }
  }
`,Hi=E`
  query searchMessage($conversationId: ObjectID!, $search: String!)
  @api(name: "chat") {
    searchMessage(conversationId: $conversationId, search: $search) {
      hasNextPage
      hasPrevPage
      limit
      nextPage
      page
      pagingCounter
      prevPage
      totalDocs
      totalPages
      docs {
        id
        text
        from
        to
        type
        conversation {
          id
          participants {
            userId
            fullName
            avatar
          }
        }
        attachments {
          type
          attachmentId
          attachment {
            id
            path
            fullUrl
            type
          }
        }
        loading @client
        error @client
        seenBy
        createdAt
        updatedAt
        deletedAt
        callPayload
      }
    }
  }
`,zt=E`
  query nearbyMessages($messageId: ObjectID!, $limit: Int) @api(name: "chat") {
    nearbyMessages(messageId: $messageId, limit: $limit) {
      hasNextPage
      hasPrevPage
      limit
      nextPage
      page
      prevPage
      docs {
        id
        text
        from
        to
        type
        attachments {
          type
          attachmentId
          attachment {
            id
            path
            fullUrl
            type
          }
        }
        conversation {
          id
          participants {
            userId
            avatar
            fullName
          }
        }
        loading @client
        error @client
        seenBy
        createdAt
        updatedAt
        deletedAt
        callPayload
      }
    }
  }
`,Gi=E`
  query QAPostCategoriesTree @api(name: "zim") {
    QAPostCategoriesTree {
      id
      title
    }
  }
`,ju=E`
  query (
    $school_id: Int
    $q: String
    $name: String
    $description: String
    $status: String
  ) @api(name: "appZim") {
    rooms(
      school_id: $school_id
      q: $q
      name: $name
      description: $description
      status: $status
    ) {
      id
      name
    }
  }
`,qu=E`
  query schools($city_id: Int, $status: String) @api(name: "appZim") {
    schools(city_id: $city_id, status: $status) {
      id
      city {
        id
        name
      }
      district {
        id
        name
      }
      name
      address
      phone
      status
    }
  }
`,Uu=E`
  query @api(name: "appZim") {
    cities {
      id
      name
      status
    }
  }
`;E`
  query @api(name: "appZim") {
    cats {
      id
      name
    }
  }
`;const Hu=E`
  query subject($name: String, $status: StatusEnum) @api(name: "appZim") {
    subjects(name: $name, status: $status) {
      id
      name
      status
      cats {
        id
        name
        status
        subject {
          id
          name
          status
        }
      }
    }
  }
`,Gu=E`
  query levels($subject_id: Int!) @api(name: "appZim") {
    levels(subject_id: $subject_id, status: active) {
      id
      name
      status
      graduation
      subject_id
    }
  }
`;E`
  query shifts($q: String) @api(name: "appZim") {
    shifts(q: $q, status: "active", shift_minute: 0) {
      id
      start_time
      end_time
      status
      shift_minute
    }
  }
`;E`
  query getStatusAccount @api(name: "appZim") {
    getStatusAccount {
      key
      value
    }
  }
`;E`
  query getTypeOfEducation @api(name: "appZim") {
    getTypeOfEducation {
      key
      value
    }
  }
`;E`
  query getAcademicPurposesAll @api(name: "appZim") {
    getAcademicPurposesAll {
      id
      academicPurposesName
    }
  }
`;const Vu=E`
  query getEcs @api(name: "appZim") {
    getEcs {
      id
      user_name
      full_name
      phone
      address
      email
      role_id
      role_name
      status
      status_online
      avatar
      supporter
    }
  }
`,Wu=E`
  query getJobs @api(name: "appZim") {
    getJobs {
      id
      jobName
    }
  }
`,Vi=E`
  query getSourceOfCustomer @api(name: "appZim") {
    getSourceOfCustomer {
      id
      sourceOfCustomer
    }
  }
`,Qu=E`
  query districts($city_id: Int!, $streetName: String, $status: StatusEnum)
  @api(name: "appZim") {
    districts(city_id: $city_id, name: $streetName, status: $status) {
      id
      name
      status
    }
  }
`,Zu=E`
  query getStreets($districtid: Int!, $streetName: String, $isHide: Boolean)
  @api(name: "appZim") {
    getStreets(
      districtid: $districtid
      streetName: $streetName
      isHide: $isHide
    ) {
      id
      streetName
    }
  }
`,Yu=E`
  query getWards($districtid: Int!, $wardName: String, $isHide: Boolean)
  @api(name: "appZim") {
    getWards(districtid: $districtid, wardName: $wardName, isHide: $isHide) {
      id
      wardName
    }
  }
`;E`
  mutation createProductCategory($input: ProductCategoryInput!)
  @api(name: "zim") {
    createProductCategory(input: $input) {
      id
      title
      slug
    }
  }
`;E`
  mutation updateProductCategory(
    $id: ObjectID!
    $input: UpdateProductCategoryInput!
  ) @api(name: "zim") {
    updateProductCategory(id: $id, input: $input) {
      id
      title
    }
  }
`;E`
  mutation deleteProductCategory($id: ObjectID!) @api(name: "zim") {
    deleteProductCategory(id: $id)
  }
`;E`
  mutation ($name: String!, $type: ProductAttributeType!, $options: [String])
  @api(name: "zim") {
    createProductAttribute(name: $name, type: $type, options: $options) {
      id
      type
      name
      options
    }
  }
`;E`
  mutation (
    $id: ObjectID!
    $name: String
    $type: ProductAttributeType
    $options: [String]
  ) @api(name: "zim") {
    updateProductAttribute(
      id: $id
      name: $name
      type: $type
      options: $options
    ) {
      id
      type
      name
      options
    }
  }
`;E`
  mutation (
    $type: ProductType!
    $sku: String!
    $description: String!
    $shortDescription: String!
    $name: String!
    $featured: Boolean
    $categories: [ObjectID]
    $images: [ObjectID]
    $attributes: [ProductAttributeInput]
    $tags: [ObjectID]
    $variationAttributes: [ObjectID]
    $regularPrice: Int
    $salePrice: Int
    $salePriceFrom: DateTime
    $salePriceTo: DateTime
    $downloads: [ObjectID]
    $downloadable: Boolean
  ) @api(name: "zim") {
    createProduct(
      input: {
        type: $type
        sku: $sku
        description: $description
        shortDescription: $shortDescription
        name: $name
        featured: $featured
        categories: $categories
        images: $images
        attributes: $attributes
        tags: $tags
        variationAttributes: $variationAttributes
        regularPrice: $regularPrice
        salePrice: $salePrice
        salePriceFrom: $salePriceFrom
        salePriceTo: $salePriceTo
        downloads: $downloads
        downloadable: $downloadable
      }
    ) {
      ...ProductBaseField
      ...ProductDetailFields
    }
  }
  ${bt}
`;E`
  mutation (
    $id: ObjectID!
    $description: String!
    $shortDescription: String!
    $name: String!
    $featured: Boolean
    $categories: [ObjectID]
    $images: [ObjectID]
    $attributes: [ProductAttributeInput]
    $tags: [ObjectID]
    $regularPrice: Int
    $salePrice: Int
    $salePriceFrom: DateTime
    $salePriceTo: DateTime
    $downloads: [ObjectID]
    $downloadable: Boolean
    $variationAttributes: [ObjectID]
    $slug: String
    $sku: String
  ) @api(name: "zim") {
    updateProduct(
      id: $id
      input: {
        description: $description
        shortDescription: $shortDescription
        name: $name
        featured: $featured
        categories: $categories
        images: $images
        attributes: $attributes
        tags: $tags
        regularPrice: $regularPrice
        salePrice: $salePrice
        salePriceFrom: $salePriceFrom
        salePriceTo: $salePriceTo
        downloads: $downloads
        downloadable: $downloadable
        variationAttributes: $variationAttributes
        slug: $slug
        sku: $sku
      }
    ) {
      ...ProductBaseField
      ...ProductDetailFields
    }
  }
  ${bt}
`;E`
  mutation (
    $id: ObjectID!
    $description: String
    $shortDescription: String
    $name: String
    $categories: [ObjectID]
    $images: [ObjectID]
    $attributes: [ProductAttributeInput]
    $tags: [ObjectID]
    $regularPrice: Int
    $salePrice: Int
    $salePriceFrom: DateTime
    $salePriceTo: DateTime
    $downloads: [ObjectID]
    $downloadable: Boolean
    $slug: String
    $sku: String
  ) @api(name: "zim") {
    updateProductVariants(
      id: $id
      input: {
        description: $description
        shortDescription: $shortDescription
        name: $name
        categories: $categories
        images: $images
        attributes: $attributes
        tags: $tags
        regularPrice: $regularPrice
        salePrice: $salePrice
        salePriceFrom: $salePriceFrom
        salePriceTo: $salePriceTo
        downloads: $downloads
        downloadable: $downloadable
        slug: $slug
        sku: $sku
      }
    ) {
      ...ProductBaseField
      ...ProductDetailFields
    }
  }
  ${bt}
`;E`
  mutation ($parentProduct: ObjectID!, $input: AddVariantProductInput!)
  @api(name: "zim") {
    createProductVariants(parentProduct: $parentProduct, input: $input) {
      ...ProductBaseField
      ...ProductDetailFields
    }
  }
  ${bt}
`;E`
  mutation ($id: ObjectID!) @api(name: "zim") {
    publishProduct(id: $id) {
      ...ProductBaseField
    }
  }
  ${Xe}
`;E`
  mutation ($id: ObjectID!) @api(name: "zim") {
    unPublishProduct(id: $id) {
      ...ProductBaseField
    }
  }
  ${Xe}
`;E`
  mutation ($productId: ObjectID!, $inventories: [InventoryInput]!)
  @api(name: "zim") {
    bulkUpdateProductInventory(
      productId: $productId
      inventories: $inventories
    ) {
      id
      inStock
      inventories {
        id
        inStock
        inventory
        reserved
        manageStock
        store {
          id
          name
        }
      }
    }
  }
`;E`
  mutation ($id: ObjectID!) @api(name: "zim") {
    removeProductVariants(id: $id) {
      ...ProductBaseField
    }
  }
  ${Xe}
`;E`
  fragment RecruitmentPostBaseField on RecruitmentPost {
    id
    title
    area
    status
    position
    currentNumOfReplies
    maxCandidates
    startDate
    endDate
    createdAt
    updatedAt
    slug
  }
`;const Wi=E`
  fragment RecruitmentPostDetailsField on RecruitmentPost {
    id
    title
    area
    position
    numOfCandidates
    maxCandidates
    jobDescription
    currentNumOfReplies
    salaryFrom
    slug
    salaryType
    salaryTo
    status
    endDate
    createdAt
    updatedAt
    startDate
    seo {
      title
      description
      ogDescription
      ogImage {
        id
        path
      }
      ogTitle
      publisher
      noIndex
      noFollow
      canonicalUrl
      customHeadHtml
    }
  }
`;E`
  mutation ($postId: ObjectID!, $seo: PostSeoInput!) @api(name: "zim") {
    updatePostSeoConfig(postId: $postId, seo: $seo) {
      id
      title
      slug
      seo {
        title
        description
        ogDescription
        ogImage {
          id
          path
        }
        ogTitle
        publisher
        noIndex
        noFollow
        canonicalUrl
        customHeadHtml
      }
      createdAt
      updatedAt
    }
  }
`;E`
  mutation ($productId: ObjectID!, $seo: PostSeoInput!) @api(name: "zim") {
    updateProductSeoConfig(productId: $productId, seo: $seo) {
      ... on SimpleProduct {
        id
        createdAt
        updatedAt
        seo {
          title
          description
          ogDescription
          ogImage {
            id
            path
          }
          ogTitle
          publisher
          noIndex
          noFollow
          canonicalUrl
          customHeadHtml
        }
      }
      ... on VariableProduct {
        id
        createdAt
        updatedAt
        seo {
          title
          description
          ogDescription
          ogImage {
            id
            path
          }
          ogTitle
          publisher
          noIndex
          noFollow
          canonicalUrl
          customHeadHtml
        }
      }
    }
  }
`;E`
  mutation ($recruitmentPostId: ObjectID!, $seo: CommonSeoInput!)
  @api(name: "zim") {
    updateRecruitmentPostSeoConfig(
      recruitmentPostId: $recruitmentPostId
      seo: $seo
    ) {
      ...RecruitmentPostDetailsField
    }
  }
  ${Wi}
`;E`
  query redirectUrlPagination($page: Int, $limit: Int, $search: String)
  @api(name: "zim") {
    redirectUrlPagination(page: $page, limit: $limit, search: $search) {
      docs {
        id
        prevUrl
        newUrl
        redirectCode
        createdAt
        updatedAt
      }
      totalPages
      totalDocs
    }
  }
`;E`
  mutation updateRedirectUrl($id: ObjectID!, $input: UpdateRedirectUrlInput!)
  @api(name: "zim") {
    updateRedirectUrl(id: $id, input: $input) {
      id
      prevUrl
      newUrl
      redirectCode
      createdAt
      updatedAt
    }
  }
`;E`
  mutation deleteRedirectUrl($id: ObjectID!) @api(name: "zim") {
    deleteRedirectUrl(id: $id)
  }
`;E`
  mutation createRedirectUrl($input: CreateRedirectUrlInput!)
  @api(name: "zim") {
    createRedirectUrl(input: $input) {
      id
      prevUrl
      newUrl
      redirectCode
      createdAt
      updatedAt
    }
  }
`;function Za(e){const n=e+"=",s=decodeURIComponent(document.cookie).split(";");for(let r=0;r<s.length;r++){let o=s[r];for(;o.charAt(0)===" ";)o=o.substring(1);if(o.indexOf(n)===0)return o.substring(n.length,o.length)}return""}const Qi=`
    mutation {
        refreshToken {
            token
            refreshToken
        }
    }
`,Zi=e=>{},Yi=()=>{},Je=()=>Za("token"),Ki=Ko(({graphQLErrors:e,networkError:n})=>{e&&e.map(({message:a,locations:s,path:r})=>console.log(`[GraphQL error]: Message: ${a}, Location: ${s}, Path: ${r}`)),n&&console.log(`[Network error]: ${n}`)}),Xi=new Qt((e,n)=>new Xo(a=>{let s;return Promise.resolve(e).then(r=>{r.setContext({headers:{authorization:`Bearer ${Je()}`,"Access-Control-Allow-Credentials":!0}})}).then(()=>{s=n(e).subscribe({next:a.next.bind(a),error:a.error.bind(a),complete:a.complete.bind(a)})}).catch(a.error.bind(a)),()=>{s&&s.unsubscribe()}})),Ji=new Jo({possibleTypes:{BaseProduct:["VariableProduct","SimpleProduct"]},typePolicies:{Participant:{keyFields:["userId"]},AccountStudentInfoType:{keyFields:(e,n)=>`${e.source_name}-${e.district_name}-${e.job_name}-${e.ward_name}-${e.city_name}-${e.street_name}-${e.birthday}`},AccountType:{keyFields:(e,n)=>`${e.id}-${e.full_name}-${e.phone}`},Message:{fields:{loading:{read:()=>!1},error:{read:()=>!1}}},Query:{fields:{getAccountChatPagination:{keyArgs:["input",["q","limit","roles"]]},messages:{keyArgs:["conversationId","userId","offset","before","after"]},supportConversations:{keyArgs:["status"]},conversations:{keyArgs:["group"]},medias:Zt(["type","width","height","search"]),tags:Zt(["search"]),comments:Zt(["type","refId","parent"])}}}});global.wsClients={};const el=e=>{const n=new lr(e,{reconnect:!0,connectionParams:()=>({authToken:Je()})});return global.wsClients[e]=n,n},tl=e=>new ar(el(e)),Ya=new er({endpoints:{zim:"https://graph-api.zim.vn",appZim:"https://graphnet-api.zim.vn",analytics:{}.REACT_APP_ANALYTICS_API_ENDPOINT,chat:"https://chat.zim.vn",crawler:"https://crawler.zim.vn"},createHttpLink:()=>tr({credentials:"include",fetch:nr(ca,(e,n,a)=>A(b({},e),{withCredentials:!0,onUploadProgress:a.onUploadProgress}))}),wsSuffix:"/graphql",createWsLink:e=>tl(e)}),hn=async()=>{const e={operationName:null,variables:{},query:Qi};return fetch("https://graph-api.zim.vn/graphql",{method:"POST",credentials:"include",body:JSON.stringify(e),headers:{"Access-Control-Request-Headers":"content-type","Content-Type":"application/json; charset=utf-8"}}).then(async n=>{const a=await n.json();return a==null?void 0:a.data.refreshToken})},Ka=new or({accessTokenField:"token",isTokenValidOrUndefined:()=>{const e=Je();if(e.length===0)return!0;try{const{exp:n}=da(e);return Date.now()<n*1e3}catch{return!1}},fetchAccessToken:hn,handleFetch:e=>{e?localStorage.setItem("isAuthenticated","true"):localStorage.removeItem("isAuthenticated")},handleResponse:()=>{},handleError:e=>{console.log(`handleError: ${e}`)}}),nl=Qt.from([Ka,Xi,Ki,Ya]),al=rr(({query:e})=>{const n=sr(e);return n.kind==="OperationDefinition"&&n.operation==="subscription"},Qt.from([Ka,Ya]),nl),yt=new ir({ssrMode:typeof window=="undefined",link:al,credentials:"include",cache:Ji,connectToDevTools:!0}),gn=m.exports.createContext({appState:{isAuthenticated:!1,user:null,loaded:!1}}),ol=({children:e})=>{const[n,a]=m.exports.useState({isAuthenticated:!1,user:null,loaded:!1,buildMode:"module"}),s=async()=>{var l,i;try{const c=await yt.query({query:Li,fetchPolicy:"network-only"});((l=c==null?void 0:c.data)==null?void 0:l.me)?(a(A(b({},n),{user:(i=c==null?void 0:c.data)==null?void 0:i.me,isAuthenticated:!0,loaded:!0})),localStorage.setItem("isAuthenticated","true")):(localStorage.removeItem("isAuthenticated"),a(A(b({},n),{user:null,isAuthenticated:!1,loaded:!0})))}catch{a(A(b({},n),{user:null,isAuthenticated:!1,loaded:!0})),localStorage.removeItem("isAuthenticated")}},r=async l=>{await s()},o=async()=>{await yt.mutate({mutation:Ni}),a(A(b({},n),{user:null,isAuthenticated:!1,loaded:!0})),localStorage.removeItem("isAuthenticated")};return t(gn.Provider,{value:{getUser:s,appState:n,setAppState:a,appSetLogin:r,appSetLogout:o,appSetAuthToken:Zi,appClearAuthToken:Yi},children:t(cr,{client:yt,children:e})})};function rl(){return m.exports.useContext(gn)}const sl=()=>{const e=m.exports.useRef(!1);return m.exports.useEffect(()=>(e.current=!0,()=>{e.current=!1}),[]),m.exports.useCallback(()=>e.current,[])},il=e=>t(Ve,b({rounded:"base",_hover:{textDecoration:"none"},as:dr,color:T("blue.500","blue.300")},e)),Ne={home:"/",listCourses:"/courses",courseDetail:"/courses/:id",forbiddenError:"/error/403",ticket:"/ticket",chat:"/chat",report:"/report",leadReport:"/report/lead",studentSupportByEc:"/user/student-support-by-ec",scheduleRegistration:"/schedule/schedule-registration",listSchedules:"/schedule/list-schedules",registerTestSchedule:"/schedule/test/reg",lead:"/web-data/lead"},Ku={lead:"/Admin/ContactCustomer/ContactList",customerInfo:"/Admin/Customer/CustomerDetai",staffInfo:"/Admin/Staff/StaffDetail",courseDetail:"/Admin/CourseManage/CourseDetail",ticketList:"/Admin/Ticket",customerList:"/Admin/Customer/CustomersList"},Et=(e=!1)=>{const[n,a]=m.exports.useState(e),s=m.exports.useCallback(r=>a(o=>r!==void 0?r:!o),[]);return[n,s]},Xa=(e,n,a=window)=>{const s=m.exports.useRef();m.exports.useEffect(()=>{s.current=n},[n]),m.exports.useEffect(()=>{if(!(a&&a.addEventListener))return;const o=l=>s&&typeof s.current=="function"?s.current(l):null;return typeof o=="function"&&a.addEventListener(e,o),()=>{typeof o=="function"&&a.removeEventListener(e,o)}},[e,a])},ll=(...e)=>{const n=m.exports.useRef();return m.exports.useEffect(()=>{e.forEach(a=>{!a||(typeof a=="function"?a(n.current):a.current=n.current)})},[e]),n},Me=m.exports.forwardRef((s,a)=>{var r=s,{error:e}=r,n=Re(r,["error"]);return g(kt,{isInvalid:!!e,children:[(n==null?void 0:n.label)&&t(Yt,{htmlFor:n.id,children:n.label}),t(vt,A(b({bg:T("white","slate.700"),borderColor:T("gray.300","slate.600"),_hover:{borderColor:T("gray.400","slate.700")},color:T("gray.900","slate.300"),_placeholder:{color:T("gray.400","slate.500")}},n),{ref:a})),e&&t(wt,{children:e&&(e==null?void 0:e.message)})]})}),Xu=m.exports.forwardRef((r,s)=>{var o=r,{placeholder:e,onSubmitSearch:n}=o,a=Re(o,["placeholder","onSubmitSearch"]);const l=m.exports.useRef(null),i=ll(s,l);return t(k,{as:"form",onSubmit:f=>{f.preventDefault();const{value:p}=i.current;typeof n=="function"&&n(p)},id:"form-header-search",w:"full",children:g(ua,{flexGrow:1,w:"100%",children:[t(pa,{children:t(Q,{p:0,variant:"ghost",_hover:{backgroundColor:"transparent"},type:"submit",children:t(ur,{fontSize:18})})}),t(Me,A(b({placeholder:"T\xECm ki\u1EBFm...",variant:"filled"},a),{ref:i,pr:10}))]})})}),Ja=E`
  subscription ($conversationId: ObjectID, $status: String) @api(name: "chat") {
    conversationUpdated(conversationId: $conversationId, status: $status) {
      id
      name
      isDeleteMessage
      group
      status
      createdAt
      updatedAt
      pinnedAt
      blockedBy
      isUnread
      typing
      usersTyping
      participants {
        userId
        fullName
        nickname
        avatar
        isGuest
        online
      }
      lastMessage {
        id
        from
        to
        text
        type
        attachments {
          type
          attachmentId
          attachment {
            id
            path
            fullUrl
            type
          }
        }
        loading @client
        error @client
        seenBy
        createdAt
        updatedAt
        deletedAt
        callPayload
      }
    }
  }
`,cl=E`
  subscription @api(name: "chat") {
    messageAdded {
      zimians
      student
      customer
      open
      following
      another
      isMarkSeen
    }
  }
`;function re(){const{appState:{user:e},setUser:n}=m.exports.useContext(gn);return[e,n]}var dl="/assets/ring.mp3";const ul=()=>{const[e]=re(),n=new Audio(dl);n.volume=1;const{data:a,loading:s}=he(Bt),r=a==null?void 0:a.statisticMessageUnread;return Kt(cl,{onSubscriptionData:({client:o,subscriptionData:l})=>{var c,f;const i=o.cache.readQuery({query:Bt});if(l.data){const{messageAdded:p}=l.data;p?(o.writeQuery({query:Bt,data:A(b({},i),{statisticMessageUnread:b(b({},i.statisticMessageUnread),p)})}),((f=(c=l==null?void 0:l.data)==null?void 0:c.messageAdded)==null?void 0:f.isMarkSeen)||n.play()):o.writeQuery({query:Bt,data:b({},i)})}},shouldResubscribe:!!e}),{listMessageUnreadOverall:r,loading:s}},Ju={editor:[1,2,11],approve:[1,8],author:[1,8,11,4],manager:[1,2,12,8,11],productManager:[1,2],censor:[1,2,8],canEdit:[1,2,4,8,11],canConfigSeo:[1,2,11],admin:[1],adminAndCC:[1,2],canCreateTicket:[5,6,12,1,2,11,12],canCancelSchedule:[2],canBookSchedule:[6]},mn=[{id:1,name:"Super Admin"},{id:2,name:"CC"},{id:4,name:"Gi\xE1o vi\xEAn"},{id:5,name:"Student"},{id:6,name:"EC"},{id:8,name:"AM"},{id:9,name:"K\u1EBF to\xE1n"},{id:11,name:"Marketer"},{id:12,name:"BM"},{id:13,name:"BP"},{id:14,name:"Guest"}],pl=[5],hl=[1,6],gl={apiUrl:"https://app.zim.vn",apiVersion:"v1",access_token:"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJqc29uX3dlYl90b2tlbiIsImp0aSI6ImM5ZDFmNzA3LTk4ZmYtNGNmMC1iMGUyLTAwN2RhY2MzYTVhNSIsImlhdCI6IjEvNC8yMDIyIDM6MDE6MzIgUE0iLCJpZCI6IjM0NDg4IiwidXNlcl9uYW1lIjoiemltIiwiZnVsbF9uYW1lIjoiWklNIEFjYWRlbXkiLCJyb2xlX2lkIjoiMSIsInJvbGVfbmFtZSI6IlN1cGVyIEFkbWluIiwiZXhwIjoxNjY3NTc0MDkyLCJpc3MiOiJhcHAuemltLnZuIiwiYXVkIjoiYXBwLnppbS52biJ9.RPOVJ-aana5NoeyUEZ_WYI_i0f2gBqU0o1W68WO51Ro"},ml=()=>{var f;const[e]=re(),n=e.roleId,{listMessageUnreadOverall:a}=ul(),s=(a==null?void 0:a.another)?Object==null?void 0:Object.values(JSON==null?void 0:JSON.parse(a==null?void 0:a.another)):[],r=[a==null?void 0:a.zimians,a==null?void 0:a.student,a==null?void 0:a.following,a==null?void 0:a.open,(f=s==null?void 0:s.map(p=>parseInt(p,10)))==null?void 0:f.reduce((p,d)=>p+d,0)],o=r[1]+r[4],l=r[0]+r[1]+r[4],i=r==null?void 0:r.reduce((p,d)=>p+d,0),c=pl.includes(n)?o:hl.includes(n)?i:l;return m.exports.useEffect(()=>{const p=document.getElementById("noti-chat");p&&(p.innerHTML=`${c}`)},[c]),{overall:r,overallMyChats:c}},fl=({searchString:e,setSearchString:n})=>g(k,{flex:1,maxH:"35px",borderWidth:1,borderColor:"#e4e4e4",rounded:"full",display:"flex",flexDirection:"row",alignItems:"center",px:2,overflow:"hidden",bg:"white",children:[t(vt,{variant:"unstyled",placeholder:"T\xECm ki\u1EBFm",p:2,value:e,onChange:s=>{s.target&&n(s.target.value)},color:"#373737"}),t(pr,{className:"h-5 w-5 text-gray-400"})]}),xl=e=>{const{isMultiple:n}=e,{getInputProps:a,getCheckboxProps:s}=n?ha(e):ga(e),r=a(),o=s();return g(k,{as:"label",rounded:"md",d:"block",bg:T("#1e293b","#1e293b50"),children:[t("input",A(b({},r),{style:{display:"none"}})),g(k,A(b({},o),{cursor:"pointer",opacity:.8,pos:"relative",children:[r.checked&&t(ma,{color:"green.500",fontSize:"xl",pos:"absolute",top:2,right:2,zIndex:2}),e.children]}))]})},ep=e=>`${e} is required.`,jt="00000000-0000-0000-0000-000000000000",tp={0:{short:"CN",long:"CN"},1:{short:"2",long:"T2"},2:{short:"3",long:"T3"},3:{short:"4",long:"T4"},4:{short:"5",long:"T5"},5:{short:"6",long:"T6"},6:{short:"7",long:"T7"}},np={LISTENING:{short:"L",long:"Listening"},READING:{short:"R",long:"Reading"},SPEAKING:{short:"S",long:"Speaking"},WRITING:{short:"W",long:"Writing"},"WRITING TASK 1":{short:"WT1",long:"WT1"},"WRITING TASK 2":{short:"WT2",long:"WT2"}},fn={ALL:"all",INCOMING:"incoming",ONGOING:"ongoing",CLOSED:"closed"},ap={ALL:{label:"All",colorScheme:"gray"},INCOMING:{label:"Incoming",colorScheme:"green"},ONGOING:{label:"Ongoing",colorScheme:"orange"},FINISHED:{label:"Finished",colorScheme:"red"}},op=[{label:"Incoming",value:fn.INCOMING},{label:"Ongoing",value:fn.ONGOING},{label:"Closed",value:fn.CLOSED}],rp=[{id:1,label:"Th\u1EE9 Hai",value:1},{id:2,label:"Th\u1EE9 Ba",value:1},{id:3,label:"Th\u1EE9 T\u01B0",value:3},{id:4,label:"Th\u1EE9 N\u0103m",value:4},{id:5,label:"Th\u1EE9 S\xE1u",value:5},{id:6,label:"Th\u1EE9 B\u1EA3y",value:6},{id:0,label:"Ch\u1EE7 nh\u1EADt",value:7}];var dt;(function(e){e.PROCESSING="Processing",e.CLOSED="Closed",e.WAITING="Waiting",e.DELETED="Deleted"})(dt||(dt={}));var qt;(function(e){e.send="send",e.received="received"})(qt||(qt={}));var Ut;(function(e){e.active="active",e.deactivated="deactivated"})(Ut||(Ut={}));const et={All:{label:"All",value:"All",colorScheme:"gray"},Processing:{label:"Processing",value:"Processing",colorScheme:"green"},Waiting:{label:"Waiting",value:"Waiting",colorScheme:"orange"},Closed:{label:"Closed",value:"Closed",colorScheme:"red"},Deleted:{label:"Deleted",value:"Deleted",colorScheme:"black"}},sp={0:{label:"Ch\u01B0a x\u1EED l\xFD",value:0,colorScheme:"red",filterValue:"pending"},1:{label:"\u0110ang theo d\xF5i",value:1,colorScheme:"orange",filterValue:"following"},2:{label:"\u0110\xE3 h\u1EB9n test",value:2,colorScheme:"blue",filterValue:"appoiment"},4:{label:"\u0110\xE3 \u0111\u1EBFn test",value:4,colorScheme:"teal",filterValue:"called"},6:{label:"\u0110\xE3 \u0111\u0103ng k\xFD",value:6,colorScheme:"green",filterValue:"registered"},3:{label:"\u0110\xE3 h\u1EE7y",value:3,colorScheme:"gray",filterValue:"cancelled"},5:{label:"\u0110\xE3 \u0111\u1ED5i EC",value:5,colorScheme:"purple",filterValue:"changeEc"}},ip=[{label:"Processing",value:dt.PROCESSING},{label:"Waiting",value:dt.WAITING},{label:"Closed",value:dt.CLOSED},{label:"Deleted",value:dt.DELETED}],lp=[{label:"\u0110\xE3 g\u1EEDi",value:qt.send},{label:"\u0110\xE3 nh\u1EADn",value:qt.received}],cp=[{label:"Active",value:Ut.active},{label:"Deactivated",value:Ut.deactivated}];var eo;(function(e){e.Other="Other",e.TeacherOff="TeacherOff"})(eo||(eo={}));const dp=[{id:1,label:"3 bu\u1ED5i/tu\u1EA7n"},{id:2,label:"4-6 bu\u1ED5i/tu\u1EA7n"},{id:3,label:"Tr\xEAn 6 bu\u1ED5i/tu\u1EA7n"}],ze="https://demo.zim.vn/app-assets/zimv2/images/default-logo1.png",bl=at.memo(function({avatarUrl:n,visibleLastOnline:a,lastOnline:s}){return t(Se,{zIndex:"10",boxSize:"2em",src:n===ze?"":n,children:a&&t(fa,{boxSize:"15px",bg:s==="online"?"green.500":"gray.300"})})}),Ct=e=>{const p=e,{onClick:n,avatarUrl:a,username:s,usernameClassName:r,positionName:o,lastOnline:l,visibleLastOnline:i=!0,isOwner:c}=p,f=Re(p,["onClick","avatarUrl","username","usernameClassName","positionName","lastOnline","visibleLastOnline","isOwner"]);return g(k,A(b({w:"100%",display:"flex",alignItems:"start",cursor:"pointer",onClick:n,maxW:{base:"full"},className:" space-x-2.5",bg:T("white",""),p:3,rounded:"lg"},f),{children:[t(bl,{avatarUrl:a,lastOnline:l,visibleLastOnline:i}),g(k,{flex:1,children:[g(D,{children:[t(N,{noOfLines:1,fontSize:14,lineHeight:"18px",fontWeight:"medium",color:T("gray.800","slate.300"),className:`${r}`,children:s}),c&&t(hr,{})]}),t(N,{noOfLines:1,fontSize:12,lineHeight:"24px",color:"gray.400",children:o})]})]}))},yl=E`
  mutation sendMessage(
    $recipient: RecipientInput!
    $message: MessageInput
    $senderAction: SenderAction
  ) @api(name: "chat") {
    sendMessage(
      recipient: $recipient
      message: $message
      senderAction: $senderAction
    ) {
      id
      createdAt
      updatedAt
      lastMessage {
        id
        text
        from
        to
        type
        attachments {
          type
          attachmentId
          attachment {
            id
            path
            fullUrl
            type
          }
        }
        loading @client
        error @client
        seenBy
        createdAt
        updatedAt
        deletedAt
        callPayload
      }
    }
  }
`,El=E`
  mutation removeMessage($id: ObjectID!) @api(name: "chat") {
    removeMessage(id: $id) {
      id
      name
      createdAt
      updatedAt
      isDeleteMessage
      lastMessage {
        id
        text
        from
        to
        type
        attachments {
          type
          attachmentId
          attachment {
            id
            path
            fullUrl
            type
          }
        }
        loading @client
        error @client
        seenBy
        createdAt
        updatedAt
        deletedAt
        callPayload
      }
    }
  }
`,Cl=E`
  mutation createConversation(
    $name: String
    $group: String
    $userIds: [String!]!
  ) @api(name: "chat") {
    createConversation(name: $name, group: $group, userIds: $userIds) {
      id
      name
    }
  }
`,$l=E`
  mutation updateConversation(
    $name: String
    $id: ObjectID!
    $userIds: [String!]!
  ) @api(name: "chat") {
    updateConversation(name: $name, id: $id, userIds: $userIds) {
      id
      name
    }
  }
`,Sl=E`
  mutation takeNoteForConversation(
    $conversationId: ObjectID!
    $content: String!
  ) @api(name: "chat") {
    takeNoteForConversation(
      conversationId: $conversationId
      content: $content
    ) {
      id
      content
      conversationId
      createdAt
      createdBy
    }
  }
`,Al=E`
  mutation removeMemberFromParticipant($userId: String!, $id: ObjectID!)
  @api(name: "chat") {
    removeMemberFromParticipant(userId: $userId, id: $id) {
      id
    }
  }
`,kl=E`
  mutation createTagForConversation(
    $tagId: ObjectID
    $conversationId: ObjectID
    $name: String
    $color: String
  ) @api(name: "chat") {
    createTagForConversation(
      tagId: $tagId
      conversationId: $conversationId
      name: $name
      color: $color
    ) {
      id
      tags {
        id
        name
        color
      }
    }
  }
`,vl=E`
  mutation markDoneConversation($conversationId: ObjectID) @api(name: "chat") {
    markDoneConversation(id: $conversationId) {
      id
    }
  }
`,wl=E`
  mutation deleteTagInConversation(
    $conversationId: ObjectID!
    $tagId: ObjectID!
  ) @api(name: "chat") {
    deleteTagInConversation(conversationId: $conversationId, tagId: $tagId) {
      id
      tags {
        id
      }
    }
  }
`,Il=E`
  mutation createQAPost($input: QAPostInput!, $userId: String)
  @api(name: "zim") {
    createQAPost(input: $input, userId: $userId) {
      id
      title
      content
    }
  }
`,Tl=E`
  mutation postComment($content: String!, $type: CommentType, $ref: ObjectID!)
  @api(name: "zim") {
    postComment(content: $content, type: $type, ref: $ref) {
      ... on PostComment {
        id
      }
      ... on QAPostComment {
        id
      }
    }
  }
`,to=()=>{const{innerWidth:e,innerHeight:n}=window,[a,s]=m.exports.useState(e),[r,o]=m.exports.useState(n),l=m.exports.useCallback(()=>{s(window.innerWidth),o(window.innerHeight)},[]);return Xa("resize",l),{width:a,height:r}},Pl=({item:e,onClick:n,isActive:a})=>t(k,{id:"js-toggle-profile",onClick:n,w:10,h:10,p:1,rounded:"full",bg:T(a?"blue.400":"gray.100",a?"blue.400":"#1e293b"),overflow:"hidden",display:"flex",alignItems:"center",justifyContent:"center",children:e.icon}),_l=()=>{const e=F(a=>a.setTab),n=F(a=>a.tab);return t(D,{w:"full",bg:T("white","#10172a"),p:2,justifyContent:"flex-end",alignItems:"center",borderBottomWidth:1,spacing:6,children:[{icon:t(gr,{}),key:"channel",onClick:()=>e("channel")},{icon:t(mr,{}),key:"conversation",onClick:()=>e("conversation")}].map((a,s)=>{const r=a.key===n;return t(Pl,{isActive:r,item:a,onClick:a.onClick},s)})})},no=(e,n=new Date)=>{var c,f;const a=Math.abs(Math.max(Date.parse(n),Date.parse(e))-Math.min(Date.parse(e),Date.parse(n))),s=1e3,r=60*s,o=60*r,l=(c=Math.floor(a/o))!=null?c:0,i=(f=Math.floor(a%o/r).toLocaleString("en-US",{minimumIntegerDigits:1}))!=null?f:"0";return l&&l<24?`${l} gi\u1EDD tr\u01B0\u1EDBc.`:l===0?`${i} ph\xFAt tr\u01B0\u1EDBc.`:l>=24?l/24>31?ve(e).format("DD/MM/YYYY"):`${Math.floor(l/24)} ng\xE0y tr\u01B0\u1EDBc.`:"v\u1EEBa xong."},ao=()=>g(D,{p:3,py:2,rounded:"lg",boxShadow:"md",bg:T("white","#1e293b50"),children:[t(fr,{size:"10"}),t(xr,{flex:1,noOfLines:2,spacing:"2",skeletonHeight:5})]}),Ol="5.5.8",Fl=60,Ll=0,Nl=110,Ml=144,Rl=105,Dl="typing indicator",Bl=0,zl=[],jl=[{ddd:0,ind:1,ty:3,nm:"\u25BD Dots",sr:1,ks:{o:{a:0,k:100,ix:11},r:{a:0,k:0,ix:10},p:{a:0,k:[72,51,0],ix:2},a:{a:0,k:[36,9,0],ix:1},s:{a:0,k:[100,100,100],ix:6}},ao:0,ip:0,op:110,st:0,bm:0},{ddd:0,ind:2,ty:4,nm:"dot 1",parent:1,sr:1,ks:{o:{a:0,k:100,ix:11},r:{a:0,k:0,ix:10},p:{a:1,k:[{i:{x:.667,y:1},o:{x:.333,y:0},t:0,s:[9,9,0],to:[0,0,0],ti:[0,0,0]},{i:{x:.667,y:1},o:{x:.333,y:0},t:3.053,s:[9,10,0],to:[0,0,0],ti:[0,0,0]},{i:{x:.667,y:1},o:{x:.333,y:0},t:15.273,s:[9,-9,0],to:[0,0,0],ti:[0,0,0]},{i:{x:.667,y:1},o:{x:.333,y:0},t:25.174,s:[9,12,0],to:[0,0,0],ti:[0,0,0]},{t:32.744140625,s:[9,9,0]}],ix:2},a:{a:0,k:[0,0,0],ix:1},s:{a:0,k:[100,100,100],ix:6}},ao:0,shapes:[{ty:"gr",it:[{d:1,ty:"el",s:{a:0,k:[6,6],ix:2},p:{a:0,k:[0,0],ix:3},nm:"Ellipse Path 1",mn:"ADBE Vector Shape - Ellipse",hd:!1},{ty:"fl",c:{a:1,k:[{i:{x:[.472],y:[1]},o:{x:[1],y:[0]},t:0,s:[.690196078431,.725490196078,.752941176471,1]},{i:{x:[.49],y:[1]},o:{x:[1],y:[0]},t:15.273,s:[.349019616842,.392156869173,.427450984716,1]},{t:32.072265625,s:[.690196078431,.725490196078,.752941176471,1]}],ix:4},o:{a:0,k:100,ix:5},r:1,bm:0,nm:"Fill 1",mn:"ADBE Vector Graphic - Fill",hd:!1},{ty:"tr",p:{a:0,k:[0,0],ix:2},a:{a:0,k:[0,0],ix:1},s:{a:0,k:[300,300],ix:3},r:{a:0,k:0,ix:6},o:{a:0,k:100,ix:7},sk:{a:0,k:0,ix:4},sa:{a:0,k:0,ix:5},nm:"Transform"}],nm:"dot 1",np:2,cix:2,bm:0,ix:1,mn:"ADBE Vector Group",hd:!1}],ip:0,op:110,st:0,bm:0},{ddd:0,ind:3,ty:4,nm:"dot 2",parent:1,sr:1,ks:{o:{a:0,k:100,ix:11},r:{a:0,k:0,ix:10},p:{a:1,k:[{i:{x:.667,y:1},o:{x:.333,y:0},t:10.691,s:[36,9,0],to:[0,0,0],ti:[0,0,0]},{i:{x:.667,y:1},o:{x:.333,y:0},t:15.273,s:[36,10,0],to:[0,0,0],ti:[0,0,0]},{i:{x:.667,y:1},o:{x:.333,y:0},t:25.963,s:[36,-9,0],to:[0,0,0],ti:[0,0,0]},{i:{x:.667,y:1},o:{x:.333,y:0},t:36.984,s:[36,14.326,0],to:[0,0,0],ti:[0,0,0]},{t:43.97265625,s:[36,9,0]}],ix:2},a:{a:0,k:[0,0,0],ix:1},s:{a:0,k:[100,100,100],ix:6}},ao:0,shapes:[{ty:"gr",it:[{d:1,ty:"el",s:{a:0,k:[6,6],ix:2},p:{a:0,k:[0,0],ix:3},nm:"Ellipse Path 1",mn:"ADBE Vector Shape - Ellipse",hd:!1},{ty:"fl",c:{a:1,k:[{i:{x:[.472],y:[1]},o:{x:[1],y:[0]},t:10.691,s:[.690196078431,.725490196078,.752941176471,1]},{i:{x:[.49],y:[1]},o:{x:[1],y:[0]},t:25.963,s:[.349019616842,.392156869173,.427450984716,1]},{t:42.763671875,s:[.690196078431,.725490196078,.752941176471,1]}],ix:4},o:{a:0,k:100,ix:5},r:1,bm:0,nm:"Fill 1",mn:"ADBE Vector Graphic - Fill",hd:!1},{ty:"tr",p:{a:0,k:[0,0],ix:2},a:{a:0,k:[0,0],ix:1},s:{a:0,k:[300,300],ix:3},r:{a:0,k:0,ix:6},o:{a:0,k:100,ix:7},sk:{a:0,k:0,ix:4},sa:{a:0,k:0,ix:5},nm:"Transform"}],nm:"dot 2",np:2,cix:2,bm:0,ix:1,mn:"ADBE Vector Group",hd:!1}],ip:0,op:110,st:0,bm:0},{ddd:0,ind:4,ty:4,nm:"dot 3",parent:1,sr:1,ks:{o:{a:0,k:100,ix:11},r:{a:0,k:0,ix:10},p:{a:1,k:[{i:{x:.667,y:1},o:{x:.333,y:0},t:21.383,s:[63,9,0],to:[0,0,0],ti:[0,0,0]},{i:{x:.667,y:1},o:{x:.333,y:0},t:25.963,s:[63,10,0],to:[0,0,0],ti:[0,0,0]},{i:{x:.667,y:1},o:{x:.333,y:0},t:38.184,s:[63,-9,0],to:[0,0,0],ti:[0,0,0]},{i:{x:.667,y:1},o:{x:.333,y:0},t:49.594,s:[63,13.139,0],to:[0,0,0],ti:[0,0,0]},{t:56,s:[63,9,0]}],ix:2},a:{a:0,k:[0,0,0],ix:1},s:{a:0,k:[100,100,100],ix:6}},ao:0,shapes:[{ty:"gr",it:[{d:1,ty:"el",s:{a:0,k:[6,6],ix:2},p:{a:0,k:[0,0],ix:3},nm:"Ellipse Path 1",mn:"ADBE Vector Shape - Ellipse",hd:!1},{ty:"fl",c:{a:1,k:[{i:{x:[.472],y:[1]},o:{x:[1],y:[0]},t:21.383,s:[.690196078431,.725490196078,.752941176471,1]},{i:{x:[.49],y:[1]},o:{x:[1],y:[0]},t:36.656,s:[.349019616842,.392156869173,.427450984716,1]},{t:53.45703125,s:[.690196078431,.725490196078,.752941176471,1]}],ix:4},o:{a:0,k:100,ix:5},r:1,bm:0,nm:"Fill 1",mn:"ADBE Vector Graphic - Fill",hd:!1},{ty:"tr",p:{a:0,k:[0,0],ix:2},a:{a:0,k:[0,0],ix:1},s:{a:0,k:[300,300],ix:3},r:{a:0,k:0,ix:6},o:{a:0,k:100,ix:7},sk:{a:0,k:0,ix:4},sa:{a:0,k:0,ix:5},nm:"Transform"}],nm:"dot 3",np:2,cix:2,bm:0,ix:1,mn:"ADBE Vector Group",hd:!1}],ip:0,op:110,st:0,bm:0}],ql=[];var Ul={v:Ol,fr:Fl,ip:Ll,op:Nl,w:Ml,h:Rl,nm:Dl,ddd:Bl,assets:zl,layers:jl,markers:ql};const oo=()=>{const e={animationData:Ul,loop:!0,autoplay:!0},{View:n}=br(e);return n},ro=({groupInfo:e})=>g(ot,{columns:2,spacing:1,children:[e==null?void 0:e.slice(0,3).map((n,a)=>t(Se,{boxSize:"18px",src:(n==null?void 0:n.avatar)===ze?"":n==null?void 0:n.avatar},a)),(e==null?void 0:e.length)>3&&t(k,{display:"flex",alignItems:"center",justifyContent:"center",w:"18px",h:"18px",rounded:"full",bg:"orange.500",children:t(N,{color:"white",fontSize:12,children:(e==null?void 0:e.length)-3<=9?`+${(e==null?void 0:e.length)-3}`:"..."})})]}),Hl=({onClick:e,username:n,usernameClassName:a,lastMessage:s,seen:r,isActive:o,updatedAt:l,groupInfo:i,seenByCount:c,isTyping:f,avatarUrl:p,online:d,userTypingName:$})=>{const y=T("gray.500","slate.300"),u=()=>{if(c===(i==null?void 0:i.length)||!i)return null;if(i)return g(N,{color:y,fontSize:12,children:[c,"/",i==null?void 0:i.length]})};return g(D,{_hover:{backgroundColor:"#e4e4e450"},w:"100%",align:"start",p:3,py:2,shadow:"md",cursor:"pointer",rounded:"lg",onClick:e,bg:T(o?"gray.100":"bg-white",o?"#1e293b":"#1e293b50"),children:[We.exports.isEmpty(i)?t(Se,{boxSize:"2em",src:p===ze?"":p,children:t(fa,{boxSize:"14px",bg:d?"green.500":"gray.300"})}):t(ro,{groupInfo:i}),g(Ae,{flex:1,spacing:1,children:[t(N,{noOfLines:2,fontSize:15,lineHeight:"18px",color:T(r?"gray.700":"gray.900",r?"#ffffff50":"white"),className:`${a}`,textTransform:"capitalize",children:n}),f?g(D,{flex:1,children:[t(N,{noOfLines:1,fontSize:13,color:T("gray.700","white"),children:$==null?void 0:$.map(h=>h||"Someone is typing").join(",")}),t(k,{display:"flex",alignItems:"center",justifyContent:"center",w:10,h:5,children:t(oo,{})})]}):t(N,{noOfLines:2,fontSize:12,lineHeight:"24px",fontWeight:r?"400":"600",color:T(r?"gray.700":"gray.900",r?"#ffffff50":"white"),children:s})]}),g(ee,{spacing:2,alignItems:"flex-end",minW:"50px",children:[t(N,{color:T(r?"gray.700":"gray.900",r?"#ffffff50":"white"),fontSize:12,children:l}),g(D,{justifyContent:"space-between",children:[r&&t(yr,{}),u()]})]})]})},xn=[{key:"open",value:"Open"},{key:"following",value:"Following"},{key:"done",value:"Done"}],Gl=({data:e,isSelected:n,setParticipants:a,setUserId:s,loading:r,setLocalTab:o})=>{var u;const l=F(h=>h.setTab),i=F(h=>h.channel),c=F(h=>h.conversationId),f=F(h=>h.setConversationId),[p]=re(),[d,$]=Qe({}),y=m.exports.useCallback(h=>{c===h.id&&l("message"),f(h.id),a(h==null?void 0:h.participants),s(""),l("message"),$({qsConversationId:h.id}),d.delete("qsUserId"),d.delete("qsGroup")},[c,d]);return g(Er,{variant:"enclosed",onChange:h=>{var x;return o((x=xn[h])==null?void 0:x.key)},defaultIndex:xn.findIndex(h=>h.key===n),children:[g(D,{flexDirection:{base:"row",md:"column"},pt:6,position:"sticky",top:0,zIndex:50,bg:T("white","#10172a"),alignItems:"center",justifyContent:"center",spacing:0,px:1,children:[t(k,{w:{base:"25%",md:"full"},display:"flex",alignItems:"center",justifyContent:"center",children:t(N,{fontSize:13,fontWeight:"bold",textColor:T("#444","slate.300"),children:(u=i==null?void 0:i.toString())==null?void 0:u.toLocaleUpperCase()})}),i==="guest"&&t(D,{spacing:0,w:"full",children:xn.map((h,x)=>t(Cr,{flex:1,_dark:{color:"slate.300"},w:"full",fontSize:11,px:0,children:h.value},h.key))})]}),t(It,{in:!0,children:t(k,{p:2,children:t(Ae,{children:(e==null?void 0:e.length)>0?e.map((h,x)=>{var P,L,H,W,G,ne,te,J,de,se,z,O;const S=h==null?void 0:h.participants,w=!(S==null?void 0:S.find(Z=>Z.userId===(p==null?void 0:p.id)))&&(S==null?void 0:S.length)>=2||(S==null?void 0:S.find(Z=>Z.userId===(p==null?void 0:p.id)))&&(S==null?void 0:S.length)>=3,I=(h==null?void 0:h.name)||w?S:S==null?void 0:S.filter(Z=>Z.userId!==(p==null?void 0:p.id))[0],v=(I==null?void 0:I.isGuest)?I==null?void 0:I.fullName:(h==null?void 0:h.name)?h==null?void 0:h.name:w?S==null?void 0:S.map(Z=>Z.fullName).join(","):I==null?void 0:I.fullName,R=((L=(P=h==null?void 0:h.usersTyping)==null?void 0:P.filter(Z=>Z!==(p==null?void 0:p.id)))==null?void 0:L.length)>0,_=(h==null?void 0:h.lastMessage)?(W=(H=h==null?void 0:h.lastMessage)==null?void 0:H.seenBy)==null?void 0:W.includes(p.id):!0,j=no(ve(h==null?void 0:h.updatedAt).toDate()),C=((ne=(G=h==null?void 0:h.lastMessage)==null?void 0:G.attachments)==null?void 0:ne.length)>0?["[T\u1EC7p \u0111\xEDnh k\xE8m]"]:(te=h==null?void 0:h.lastMessage)==null?void 0:te.text;return r?t(ao,{},x):m.exports.createElement(Hl,A(b({},I),{isTyping:R,isActive:h.id===c,key:h.id,onClick:()=>{y(h)},username:v,lastMessage:C,updatedAt:j,seen:_,seenByCount:(se=(de=(J=h==null?void 0:h.lastMessage)==null?void 0:J.seenBy)==null?void 0:de.length)!=null?se:0,avatarUrl:I==null?void 0:I.avatar,groupInfo:(h==null?void 0:h.name)||w?I:null,userTypingName:(O=(z=h==null?void 0:h.usersTyping)==null?void 0:z.filter(Z=>Z!==p.id))==null?void 0:O.map(Z=>{var ge;return(ge=S.find(ye=>ye.userId===Z))==null?void 0:ge.fullName})}))}):t(N,{textAlign:"center",children:`Hi\u1EC7n t\u1EA1i kh\xF4ng c\xF3 ${i==="guest"?"y\xEAu c\u1EA7u h\u1ED7 tr\u1EE3":"cu\u1ED9c tr\xF2 chuy\u1EC7n"} n\xE0o !!!`})})})})]})};function bn(...e){const n=a=>a&&typeof a=="object";return e.reduce((a,s)=>(Object.keys(s).forEach(r=>{const o=a[r],l=s[r];Array.isArray(o)&&Array.isArray(l)?a[r]=Xt(o.concat(...l),"id"):n(o)&&n(l)?a[r]=bn(o,l):a[r]=l}),a),{})}function so(...e){const n=a=>a&&typeof a=="object";return e.reduce((a,s)=>(Object.keys(s).forEach(r=>{const o=a[r],l=s[r];Array.isArray(o)&&Array.isArray(l)?a[r]=Xt(o.concat(...l),"user"):n(o)&&n(l)?a[r]=so(o,l):a[r]=l}),a),{})}const ut=({hasNextPage:e,fetchMore:n,variables:a={},isNormalLoadMore:s=!0})=>{const[r,o]=m.exports.useState(!1),l=s?bn:so;return{onLoadMore:m.exports.useCallback(async()=>{r||!e||(await o(!0),await n({variables:a,updateQuery:(c,{fetchMoreResult:f})=>f?l(c,f):c}),await o(!1))},[r,e,o,n,a]),isLoadingMore:r}},Vl=({status:e="",search:n,channel:a})=>{var u,h,x;const s=A(b({},pe({status:e,search:n},S=>S)),{limit:20,offset:0}),[r,{data:o,loading:l,fetchMore:i,called:c}]=xa(_e,{variables:s,fetchPolicy:"network-only",nextFetchPolicy:"cache-first"});m.exports.useEffect(()=>{a&&a==="guest"&&!c&&(async()=>await r())()},[a,c]);const f=((u=o==null?void 0:o.supportConversations)==null?void 0:u.docs)||[],p=(h=o==null?void 0:o.supportConversations)==null?void 0:h.hasNextPage,{onLoadMore:d,isLoadingMore:$}=ut({variables:A(b({},s),{offset:(f==null?void 0:f.length)+1}),fetchMore:i,hasNextPage:p});return{listSupportConversations:((x=o==null?void 0:o.supportConversations)==null?void 0:x.docs)||[],loading:l,onLoadMore:d,hasNextPage:p,isLoadingMore:$}},Wl=({search:e,channel:n})=>{var $,y;const a=A(b({},pe({group:n==="my chats"?"":n,search:e},u=>u)),{limit:20,offset:0}),[s,{data:r,loading:o,fetchMore:l,called:i}]=xa(me,{variables:a,fetchPolicy:"network-only",nextFetchPolicy:"cache-first"});m.exports.useEffect(()=>{n&&n!=="guest"&&!i&&(async()=>await s())()},[n,i,s]);const c=(($=r==null?void 0:r.conversations)==null?void 0:$.docs)||[],f=(y=r==null?void 0:r.conversations)==null?void 0:y.hasNextPage,{onLoadMore:p,isLoadingMore:d}=ut({variables:A(b({},a),{offset:(c==null?void 0:c.length)+1}),fetchMore:l,hasNextPage:f});return{listConversations:c,loading:o,onLoadMore:p,hasNextPage:f,isLoadingMore:d}},Ql=e=>e.scrollHeight-e.scrollTop<=e.clientHeight+10,$t=({text:e="\u0110ang l\u1EA5y d\u1EEF li\u1EC7u...",color:n="gray.900"})=>g(D,{justifyContent:"center",pointerEvents:"none",userSelect:"none",children:[t(ht,{color:n}),t(N,{textAlign:"center",color:n,children:e})]}),Zl=({onClick:e,username:n,lastMessage:a,isActive:s,createdAt:r,avatarUrl:o})=>{const l=F(i=>i.searchStringMessage);return g(D,{_hover:{backgroundColor:"#e4e4e450"},w:"100%",align:"start",p:3,py:2,shadow:"md",cursor:"pointer",rounded:"lg",onClick:e,bg:T(s?"gray.100":"bg-white",s?"#1e293b":"#1e293b50"),children:[t(Se,{boxSize:"2em",src:o}),g(Ae,{flex:1,spacing:1,children:[t(N,{noOfLines:2,fontSize:15,lineHeight:"18px",color:T("gray.900","slate.300"),children:n}),t(N,{noOfLines:2,fontSize:12,lineHeight:"24px",fontWeight:"400",color:T("gray.900","slate.300"),children:t(ba,{searchWords:[l],autoEscape:!0,textToHighlight:a})})]}),t(ee,{spacing:2,alignItems:"flex-end",minW:"50px",children:t(N,{color:T("gray.900","slate.300"),fontSize:12,children:r})})]})},Yl=()=>{const e=F(r=>r.searchStringMessage),n=F(r=>r.searchResult),a=F(r=>r.setMessageId),s=F(r=>r.messageId);return g(Ae,{w:"full",h:"full",p:2,spacing:2,children:[t(D,{position:"sticky",top:0,zIndex:50,bg:T("white","#10172a"),alignItems:"center",children:t(N,{fontSize:16,fontWeight:"bold",textColor:T("#444","slate.300"),children:`C\xF3 ${n==null?void 0:n.length} k\u1EBFt qu\u1EA3 t\xECm ki\u1EBFm cho t\u1EEB kh\xF3a "${e}"`})}),n==null?void 0:n.map(r=>{var c,f;const o=(f=(c=r==null?void 0:r.conversation)==null?void 0:c.participants)==null?void 0:f.find(p=>p.userId===(r==null?void 0:r.from)),l=no(ve(r==null?void 0:r.createdAt).toDate()),i=r.id===s;return t(Zl,{isActive:i,onClick:()=>a(r.id),lastMessage:r==null?void 0:r.text,avatarUrl:o==null?void 0:o.avatar,username:o==null?void 0:o.fullName,createdAt:l},r.id)})]})},Kl=()=>{const e=F(v=>v.setParticipants),n=F(v=>v.searchStringMessage),a=F(v=>v.channel),s=F(v=>v.setUserId),[r,o]=m.exports.useState(!1),[l,i]=m.exports.useState("open");to();const{listSupportConversations:c,onLoadMore:f,isLoadingMore:p,hasNextPage:d,loading:$}=Vl({status:l,search:"",channel:a}),{listConversations:y,onLoadMore:u,hasNextPage:h,isLoadingMore:x,loading:S}=Wl({channel:a,search:""}),w=a==="guest"?{onLoadMore:f,isLoadingMore:p,hasNextPage:d,loading:$}:{onLoadMore:u,hasNextPage:h,isLoadingMore:x,loading:S},I=m.exports.useCallback(v=>{o(Ql(v.target))},[]);return Jt(()=>{r&&w.hasNextPage&&!w.isLoadingMore&&w.onLoadMore()},[r,w]),g(k,{w:{base:"full",md:"260px",lg:"350px"},h:"100%",overflowY:"auto",bg:T("white","#10172a"),onScroll:I,children:[n?t(Yl,{}):t(Gl,{setParticipants:e,setLocalTab:i,setUserId:s,loading:w.loading,data:w.loading?[1,2,3,4,5,6,7,8]:a==="guest"?c:y,isSelected:l}),w.isLoadingMore&&t($t,{color:T("","white")})]})};var io="/assets/weCanDoItWomen.png";const Xl=()=>({onPush:m.exports.useCallback((n,a,s)=>{if(!Notification)return;Notification.permission!=="granted"&&Notification.requestPermission();let r=new Notification("B\u1EA1n c\xF3 tin nh\u1EAFn m\u1EDBi !!",{icon:io,body:`${s}: ${a}`});r.onclick=function(){window.open(`${gl.apiUrl}/Admin/Chat?qsConversationId=${n}`)}},[])}),Jl=()=>{const[e]=re(),{onPush:n}=Xl(),a=F(r=>r.conversationId),s=F(r=>r.userId);Kt(Ja,{onSubscriptionData:({client:r,subscriptionData:o})=>{var ne,te,J,de,se,z,O,Z,ge,ye,Oe,De,Y,oe,Fe,Le,U,ae,fe,He,Ge,q,B,xe,be,nt,ue,pt,Wt,vn,wn,In,Tn,Pn,_n,On,Fn,Ln,Nn,Mn,Rn,Dn,Bn,zn,jn,qn,Un,Hn,Gn,Vn,Wn,Qn,Zn,Yn,Kn,Xn,Jn,ea,ta,na,aa,oa,ra;const l=r.cache.readQuery({query:Be,variables:A(b({},pe({conversationId:a,userId:a?"":s==null?void 0:s.toString(),limit:30},M=>M)),{offset:0})}),i=(te=(ne=l==null?void 0:l.messages)==null?void 0:ne.docs)!=null?te:[],{conversationUpdated:c}=o.data,{lastMessage:f,typing:p}=c,{id:d,from:$}=f,y=i==null?void 0:i.find(M=>(M==null?void 0:M.id)===d);if(!a&&!y&&p===null||a&&!y&&e.id!==$&&p===null){const M=(de=(J=c==null?void 0:c.participants)==null?void 0:J.find(K=>K.userId===$))==null?void 0:de.fullName;n((se=o==null?void 0:o.data)==null?void 0:se.conversationUpdated.id,(z=o==null?void 0:o.data)==null?void 0:z.conversationUpdated.lastMessage.text,M)}const u=(Z=(O=o==null?void 0:o.data)==null?void 0:O.conversationUpdated)==null?void 0:Z.group,h=(ye=(ge=o==null?void 0:o.data)==null?void 0:ge.conversationUpdated)==null?void 0:ye.status,x=r.cache.readQuery({query:me,variables:{offset:0,limit:20}}),S=(De=(Oe=x==null?void 0:x.conversations)==null?void 0:Oe.docs)!=null?De:[];if((x==null?void 0:x.conversations)&&h!=="open"){let M=[...S];const K=M==null?void 0:M.find(V=>{var ie,le;return(V==null?void 0:V.id)===((le=(ie=o==null?void 0:o.data)==null?void 0:ie.conversationUpdated)==null?void 0:le.id)});K&&((Y=o==null?void 0:o.data)==null?void 0:Y.conversationUpdated.typing)===null?(M=M.filter(V=>V.id!==K.id),r.writeQuery({query:me,variables:{offset:0,limit:20},data:A(b({},x),{conversations:A(b({},(oe=x.conversations)!=null?oe:[]),{docs:[(Fe=o==null?void 0:o.data)==null?void 0:Fe.conversationUpdated,...M]})})})):K?r.writeQuery({query:me,variables:{offset:0,limit:20},data:b({},x)}):r.writeQuery({query:me,variables:{offset:0,limit:20},data:A(b({},x),{conversations:A(b({},(Le=x.conversations)!=null?Le:[]),{docs:[(U=o==null?void 0:o.data)==null?void 0:U.conversationUpdated,...M]})})})}const w=r.cache.readQuery({query:me,variables:{group:"zimians",offset:0,limit:20}}),I=(fe=(ae=w==null?void 0:w.conversations)==null?void 0:ae.docs)!=null?fe:[];if((w==null?void 0:w.conversations)&&u==="zimians"){let M=[...I];const K=M==null?void 0:M.find(V=>{var ie,le;return(V==null?void 0:V.id)===((le=(ie=o==null?void 0:o.data)==null?void 0:ie.conversationUpdated)==null?void 0:le.id)});K&&((He=o==null?void 0:o.data)==null?void 0:He.conversationUpdated.typing)===null?(M=M.filter(V=>V.id!==K.id),r.writeQuery({query:me,variables:{group:"zimians",offset:0,limit:20},data:A(b({},w),{conversations:A(b({},(Ge=w.conversations)!=null?Ge:[]),{docs:[(q=o==null?void 0:o.data)==null?void 0:q.conversationUpdated,...M]})})})):K?r.writeQuery({query:me,variables:{group:"zimians",offset:0,limit:20},data:b({},w)}):r.writeQuery({query:me,variables:{group:"zimians",offset:0,limit:20},data:A(b({},w),{conversations:A(b({},(B=w.conversations)!=null?B:[]),{docs:[(xe=o==null?void 0:o.data)==null?void 0:xe.conversationUpdated,...M]})})})}const v=r.cache.readQuery({query:me,variables:{group:"student",offset:0,limit:20}}),R=(nt=(be=v==null?void 0:v.conversations)==null?void 0:be.docs)!=null?nt:[];if((v==null?void 0:v.conversations)&&u==="student"){let M=[...R];const K=M==null?void 0:M.find(V=>{var ie,le;return(V==null?void 0:V.id)===((le=(ie=o==null?void 0:o.data)==null?void 0:ie.conversationUpdated)==null?void 0:le.id)});K&&((ue=o==null?void 0:o.data)==null?void 0:ue.conversationUpdated.typing)===null?(M=M.filter(V=>V.id!==K.id),r.writeQuery({query:me,variables:{group:"student",offset:0,limit:20},data:A(b({},v),{conversations:A(b({},(pt=v.conversations)!=null?pt:[]),{docs:[(Wt=o==null?void 0:o.data)==null?void 0:Wt.conversationUpdated,...M]})})})):K?r.writeQuery({query:me,variables:{group:"student",offset:0,limit:20},data:b({},v)}):r.writeQuery({query:me,variables:{group:"student",offset:0,limit:20},data:A(b({},v),{conversations:A(b({},(vn=v.conversations)!=null?vn:[]),{docs:[(wn=o==null?void 0:o.data)==null?void 0:wn.conversationUpdated,...M]})})})}const _=r.cache.readQuery({query:me,variables:{group:"customer",offset:0,limit:20}}),j=(Tn=(In=_==null?void 0:_.conversations)==null?void 0:In.docs)!=null?Tn:[];if((_==null?void 0:_.conversations)&&u==="customer"){let M=[...j];const K=M==null?void 0:M.find(V=>{var ie,le;return(V==null?void 0:V.id)===((le=(ie=o==null?void 0:o.data)==null?void 0:ie.conversationUpdated)==null?void 0:le.id)});K&&((Pn=o==null?void 0:o.data)==null?void 0:Pn.conversationUpdated.typing)===null?(M=M.filter(V=>V.id!==K.id),r.writeQuery({query:me,variables:{group:"customer",offset:0,limit:20},data:A(b({},_),{conversations:A(b({},(_n=_.conversations)!=null?_n:[]),{docs:[(On=o==null?void 0:o.data)==null?void 0:On.conversationUpdated,...M]})})})):K?r.writeQuery({query:me,variables:{group:"customer",offset:0,limit:20},data:b({},_)}):r.writeQuery({query:me,variables:{group:"customer",offset:0,limit:20},data:A(b({},_),{conversations:A(b({},(Fn=_.conversations)!=null?Fn:[]),{docs:[(Ln=o==null?void 0:o.data)==null?void 0:Ln.conversationUpdated,...M]})})})}const C=r.cache.readQuery({query:_e,variables:{status:"open",offset:0,limit:20}}),P=(Mn=(Nn=C==null?void 0:C.supportConversations)==null?void 0:Nn.docs)!=null?Mn:[];if((C==null?void 0:C.supportConversations)&&u==="support"){let M=[...P];const K=M==null?void 0:M.find(V=>{var ie,le;return(V==null?void 0:V.id)===((le=(ie=o==null?void 0:o.data)==null?void 0:ie.conversationUpdated)==null?void 0:le.id)});!K&&h==="open"?r.writeQuery({query:_e,variables:{status:"open",offset:0,limit:20},data:A(b({},C),{supportConversations:A(b({},(Rn=C.supportConversations)!=null?Rn:[]),{docs:[(Dn=o==null?void 0:o.data)==null?void 0:Dn.conversationUpdated,...M]})})}):K&&((Bn=o==null?void 0:o.data)==null?void 0:Bn.conversationUpdated.typing)===null&&((qn=(jn=(zn=o==null?void 0:o.data)==null?void 0:zn.conversationUpdated)==null?void 0:jn.lastMessage)==null?void 0:qn.from)===e.id?(M=M.filter(V=>V.id!==K.id),r.writeQuery({query:_e,variables:{status:"open",offset:0,limit:20},data:A(b({},C),{supportConversations:A(b({},(Un=C.supportConversations)!=null?Un:[]),{docs:[...M]})})})):r.writeQuery({query:_e,variables:{status:"open",offset:0,limit:20},data:A(b({},C),{supportConversations:b({},(Hn=C.supportConversations)!=null?Hn:[])})})}const L=r.cache.readQuery({query:_e,variables:{status:"following",offset:0,limit:20}}),H=(Vn=(Gn=L==null?void 0:L.supportConversations)==null?void 0:Gn.docs)!=null?Vn:[];if((L==null?void 0:L.supportConversations)&&u==="support"){let M=[...H];const K=M==null?void 0:M.find(V=>{var ie,le;return(V==null?void 0:V.id)===((le=(ie=o==null?void 0:o.data)==null?void 0:ie.conversationUpdated)==null?void 0:le.id)});!K&&h==="following"?r.writeQuery({query:_e,variables:{status:"following",offset:0,limit:20},data:A(b({},L),{supportConversations:A(b({},(Wn=L.supportConversations)!=null?Wn:[]),{docs:[(Qn=o==null?void 0:o.data)==null?void 0:Qn.conversationUpdated,...M]})})}):K&&((Zn=o==null?void 0:o.data)==null?void 0:Zn.conversationUpdated.typing)===null?(M=M.filter(V=>V.id!==K.id),r.writeQuery({query:_e,variables:{status:"following",offset:0,limit:20},data:A(b({},L),{supportConversations:A(b({},(Yn=L.supportConversations)!=null?Yn:[]),{docs:h==="following"?[(Kn=o==null?void 0:o.data)==null?void 0:Kn.conversationUpdated,...M]:[...M]})})})):r.writeQuery({query:_e,variables:{status:"following",offset:0,limit:20},data:A(b({},L),{supportConversations:b({},(Xn=L.supportConversations)!=null?Xn:[])})})}const W=r.cache.readQuery({query:_e,variables:{status:"done",offset:0,limit:20}}),G=(ea=(Jn=W==null?void 0:W.supportConversations)==null?void 0:Jn.docs)!=null?ea:[];if((W==null?void 0:W.supportConversations)&&u==="support"){let M=[...G];const K=M==null?void 0:M.find(V=>{var ie,le;return(V==null?void 0:V.id)===((le=(ie=o==null?void 0:o.data)==null?void 0:ie.conversationUpdated)==null?void 0:le.id)});K&&((ta=o==null?void 0:o.data)==null?void 0:ta.conversationUpdated.typing)===null?(M=M.filter(V=>V.id!==K.id),r.writeQuery({query:_e,variables:{status:"done",offset:0,limit:20},data:A(b({},G),{supportConversations:A(b({},(na=G.supportConversations)!=null?na:[]),{docs:[...M]})})})):!K&&h==="done"?r.writeQuery({query:_e,variables:{status:"done",offset:0,limit:20},data:A(b({},W),{supportConversations:A(b({},(aa=W.supportConversations)!=null?aa:[]),{docs:[(oa=o==null?void 0:o.data)==null?void 0:oa.conversationUpdated,...M]})})}):r.writeQuery({query:_e,variables:{status:"done",offset:0,limit:20},data:A(b({},G),{supportConversations:b({},(ra=G.supportConversations)!=null?ra:[])})})}},shouldResubscribe:!!e})},ec="_boxChatContainer_1to48_1",tc="_containerMessage_1to48_8",nc="_boxShadow_1to48_13",ac="_messageLoading_1to48_17";var oc={boxChatContainer:ec,containerMessage:tc,boxShadow:nc,messageLoading:ac};const rc="_sm_183st_1",sc="_base_183st_6",ic="_md_183st_10";var yn={sm:rc,base:sc,md:ic};const lo={sm:yn.sm,base:yn.base,md:yn.md},co={primary:"bg-red-700 border-red-700 dark:bg-red-700 hover:border-red-900 hover:bg-red-900 dark:hover:bg-red-900 dark:border-red-500  hover:text-white ring-red-400 text-white ",info:"bg-blue-500 border-blue-500 hover:border-blue-700 hover:bg-blue-700 hover:text-white ring-blue-400 text-white",white:"bg-white border-transparent hover:border-red-700 hover:bg-red-700 hover:text-white ring-red-400 text-black",warning:"bg-yellow-500 hover:bg-yellow-700 border-yellow-500 hover:border-yellow-700 ring-yellow-400 text-black",success:"bg-green-500 hover:bg-green-700 ring-green-400 text-white border-green-500 hover:border-green-700","primary-outline":"border border-red-700 hover:bg-red-700 hover:text-white ring-red-400 text-red-700 dark:text-red-300 dark:border-red-300 dark:hover:bg-red-300 dark:hover:text-slate-300","info-outline":"border border-blue-700 hover:bg-blue-700 hover:text-white ring-blue-400 text-blue-700","success-outline":"border border-green-700 hover:bg-green-700 hover:text-white ring-green-400 text-green-700","warning-outline":"border border-yellow-700 text-yellow-700 hover:bg-yellow-700 hover:text-black ring-yellow-400","black-outline":"border border-black hover:bg-gray-50 text-black hover:text-black ring-gray-700 dark:border-slate-500 dark:text-slate-300 dark:hover:text-slate-900",gray:"bg-gray-200 hover:bg-gray-600 ring-gray-400 text-gray-900 hover:text-white dark:bg-slate-600 dark:text-slate-300 dark:hover:bg-slate-700 dark:border-slate-700",ghost:"hover:bg-gray-100 ring-gray-400 border-transparent hover:border-transparent dark:bg-slate-600 dark:text-slate-300 dark:hover:bg-slate-700"},uo=(e,n)=>e.match(n),lc=m.exports.forwardRef((c,i)=>{var f=c,{className:e="",buttonType:n=co.primary,buttonSize:a=lo.base,isLoading:s=!1,isDisabled:r=!1,children:o}=f,l=Re(f,["className","buttonType","buttonSize","isLoading","isDisabled","children"]);const p=m.exports.useMemo(()=>co[n],[n]),d=m.exports.useMemo(()=>lo[a],[a]),$=m.exports.useMemo(()=>s||r,[s,r]);return g("button",A(b({disabled:r||s,ref:i,className:`whitespace-nowrap border inline-flex items-center justify-center transition-colors transition-background duration-300 ${uo(e,"p-")?"":"py-2 px-4"} rounded focus:outline-none ring-opacity-75  focus:ring ${uo(e,"text-")?"":"text-base"} font-medium ${$&&"opacity-60 cursor-not-allowed"} ${p} ${e} ${d}`},l),{children:[s&&g("svg",{className:"animate-spin -ml-1 mr-3 h-5 w-5 text-current",xmlns:"http://www.w3.org/2000/svg",fill:"none",viewBox:"0 0 24 24",children:[t("circle",{className:"opacity-25",cx:"12",cy:"12",r:"10",stroke:"currentColor",strokeWidth:"4"}),t("path",{className:"opacity-100",fill:"currentColor",d:"M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"})]}),o]}))});function cc(e,n){const a=e.split(" ");return a.map((s,r)=>{if(s.length>1&&s.includes(":")){if(!n&&r===a.length-1)return s;const o=$r.search(s);return o.length>0?o[0].native:s}return s}).join(" ")}const En=({setContent:e,userId:n,group:a,setConversationId:s})=>{const[r]=re(),o=r==null?void 0:r.id,[l,{client:i,loading:c}]=ce(yl),f=m.exports.useCallback(async(y,u,h,x)=>{var I;const S=h==null?void 0:h.map(({type:v,fileId:R})=>({type:v,attachmentId:R})),{data:w}=await l({variables:{recipient:pe({recipientType:x?"conversation":"user",conversationId:x||null,userId:x?null:n==null?void 0:n.toString(),group:x||a==="my chats"?"":a},v=>v),message:pe({text:y.trim(),attachments:We.exports.isEmpty(S)?null:S},v=>v)},update:(v,R)=>{var j;const _=v.readQuery({query:Be,variables:A(b({},pe({conversationId:x,userId:x?"":n==null?void 0:n.toString(),limit:30},C=>C)),{offset:0})});if(_==null?void 0:_.messages){const C=((j=_==null?void 0:_.messages)==null?void 0:j.docs)||[],P=C==null?void 0:C.map(L=>L.id===u?A(b(b({},L),R==null?void 0:R.data.sendMessage.lastMessage),{loading:!1}):L);v.writeQuery({query:Be,variables:A(b({},pe({conversationId:x,userId:x?"":n==null?void 0:n.toString(),limit:30},L=>L)),{offset:0}),data:A(b({},_),{messages:A(b({},_==null?void 0:_.messages),{docs:P})})})}}});x||await s((I=w==null?void 0:w.sendMessage)==null?void 0:I.id)},[l,n,a,o,r,s]),p=m.exports.useCallback(async(y,u)=>{if(u)try{await l({variables:{recipient:{conversationId:u,recipientType:"conversation"},senderAction:y}})}catch(h){console.log(h)}},[l]),d=m.exports.useCallback(async y=>{try{await l({variables:{recipient:pe({recipientType:"conversation",conversationId:y||"",userId:y?"":n==null?void 0:n.toString()},u=>u),senderAction:"markSeen"}})}catch(u){console.log(u)}},[l,n]);return{onSendMessage:m.exports.useCallback(async({nativeEvent:y,conversationId:u,file:h})=>{var R,_;const x=A(b({},pe({conversationId:u,userId:u?"":n==null?void 0:n.toString(),limit:30},j=>j)),{offset:0}),S=cc(y.text,!0),w=ya(),I=i.cache.readQuery({query:Be,variables:b({},x)}),v=(_=(R=I==null?void 0:I.messages)==null?void 0:R.docs)!=null?_:[];try{e(""),(I==null?void 0:I.messages)&&i.cache.writeQuery({query:Be,variables:b({},x),data:A(b({},I),{messages:A(b({},I==null?void 0:I.messages),{docs:[{id:w,text:S,from:o,to:null,type:"update",attachments:[],seenBy:[],createdAt:ve().format(),updatedAt:ve().format(),deletedAt:null,callPayload:null,loading:!0,error:!1,conversation:{id:u,participants:[{userId:o,avatar:r.avatar,fullName:r.fullName}]}},...v]})})}),await f(S,w,h,u),setTimeout(()=>{const j=document.getElementById("chat-box");j&&(j.scrollTop=j==null?void 0:j.scrollHeight)},50)}catch{i.cache.writeQuery({query:Be,variables:b({},x),data:A(b({},I),{messages:A(b({},I==null?void 0:I.messages),{docs:[{id:w,text:S,from:o,to:null,type:"update",attachments:[],seenBy:[],createdAt:ve().format(),updatedAt:ve().format(),deletedAt:null,callPayload:null,loading:!1,error:!0},...v]})})})}},[e,f,n,i]),onTyping:p,onMarkSeen:d,loading:c}},Ht=e=>{if(!e)return;const n=(e==null?void 0:e.search("-"))+1;return e==null?void 0:e.slice(n)},dc=(e,n=null)=>{e.forEach(a=>{n==null||n.evict({id:"ROOT_QUERY",fieldName:a})})};function Ue(){const e=Ea();return n=>{dc(n,e.cache)}}const uc=({time:e})=>t(k,{py:6,display:"flex",alignItems:"center",justifyContent:"center",w:"full",children:t(N,{fontSize:14,textAlign:"center",color:T("gray.400","slate.300"),children:Tt(e).format("HH:mm - DD-MM-YYYY")})}),pc=({files:e,onPress:n,isMyMessage:a})=>t(ot,{maxW:{base:"250px"},columns:1,spacing:"8px",children:e==null?void 0:e.map((s,r)=>{var o,l,i,c;return t(k,{rounded:"lg",borderColor:T("gray.100","gray.300"),cursor:"pointer",overflow:"hidden",bg:a?"#cbe9fc":"gray.100",display:"flex",alignItems:"center",justifyContent:"center",children:((o=s==null?void 0:s.attachment)==null?void 0:o.type)!=="file"?t(Pt,{objectFit:"contain",w:"80px",h:"80px",onClick:()=>n(r),src:(l=s==null?void 0:s.attachment)==null?void 0:l.fullUrl},r):t(Ve,{href:(i=s==null?void 0:s.attachment)==null?void 0:i.fullUrl,target:"_blank",children:g(D,{m:2,children:[t(k,{w:6,h:6,rounded:"full",display:"flex",alignItems:"center",justifyContent:"center",borderWidth:1,children:t(Ca,{className:"w-6 h-6 text-gray-700"})}),t(N,{flex:1,color:"gray.800",fontSize:14,children:Ht((c=s==null?void 0:s.attachment)==null?void 0:c.path)})]})})},r)})}),po=e=>{if(typeof e!="string"||!e)return!1;let n;try{n=new URL(e)}catch{return!1}return n.protocol==="http:"||n.protocol==="https:"},hc="https://getopengraph.herokuapp.com/",gc=new RegExp("^(?!mailto:)(?:(?:http|https|ftp)://)(?:\\S+(?::\\S*)?@)?(?:(?:(?:[1-9]\\d?|1\\d\\d|2[01]\\d|22[0-3])(?:\\.(?:1?\\d{1,2}|2[0-4]\\d|25[0-5])){2}(?:\\.(?:[0-9]\\d?|1\\d\\d|2[0-4]\\d|25[0-4]))|(?:(?:[a-z\\u00a1-\\uffff0-9]+-?)*[a-z\\u00a1-\\uffff0-9]+)(?:\\.(?:[a-z\\u00a1-\\uffff0-9]+-?)*[a-z\\u00a1-\\uffff0-9]+)*(?:\\.(?:[a-z\\u00a1-\\uffff]{2,})))|localhost)(?::\\d{2,5})?(?:(/|\\?|#)[^\\s]*)?$"),mc=({url:e,isMyMessage:n})=>{const[a,s]=m.exports.useState(null),[r,o]=m.exports.useState(!1);m.exports.useEffect(()=>{e&&e.match(gc)!==null&&l(e)},[e]);const l=async c=>{o(!0);let f=await ca.get(`${hc}?url=${c}`,{method:"GET"});s(f.data),o(!1)},i=m.exports.useMemo(()=>{var c;return g(D,{flexDirection:"column",rounded:"8px",children:[t(Ve,{_hover:{color:n?"white":void 0},fontSize:14,target:"_blank",href:e,cursor:"pointer",color:n?"white":"blue.600",textDecoration:"underline",onClick:()=>window.open(e),rel:"noreferrer",children:(a==null?void 0:a.url)||e}),((c=a==null?void 0:a.img)==null?void 0:c.length)>0&&t(Pt,{w:"100%",h:"80px",mt:4,alt:"img",src:a==null?void 0:a.img,rounded:"8px"}),t(Ve,{_hover:{color:n?"white":void 0},fontSize:14,target:"_blank",href:e,cursor:"pointer",color:n?"white":"blue.600",textDecoration:"underline",onClick:()=>window.open(e),rel:"noreferrer",children:(a==null?void 0:a.title)||"kh\xF4ng t\xECm th\u1EA5y"})]})},[a,e,n]);return r?t(Ve,{_hover:{color:n?"white":void 0},fontSize:14,target:"_blank",href:e,cursor:"pointer",color:n?"white":"blue.600",textDecoration:"underline",onClick:()=>window.open(e),rel:"noreferrer",children:e}):(a==null?void 0:a.status)===200&&a&&i},fc=({text:e,isMyMessage:n,deletedAt:a,createdAt:s})=>{const r=F(i=>i.searchStringMessage),o=T("gray.100","slate.300"),l=T("gray.700","slate.900");return m.exports.useMemo(()=>(e==null?void 0:e.length)>0||a?t(k,{pointerEvents:a?"none":"auto",py:{sm:1.5,base:3},px:3,rounded:"lg",maxW:{sm:"2/3",base:"210px",lg:"250px",xl:"280px","2xl":"400px"},bg:n?"#cbe9fc":o,children:a?t(N,{letterSpacing:.1,fontWeight:"500",fontSize:{base:"13px"},color:l,children:"Tin nh\u1EAFn \u0111\xE3 b\u1ECB x\xF3a"}):po(e)?t(mc,{url:e,isMyMessage:n}):t(ee,{alignItems:"stretch",spacing:0,children:t(N,{letterSpacing:.1,fontWeight:"500",fontSize:{base:"15px"},textAlign:"left",color:l,children:t(ba,{searchWords:[r],autoEscape:!0,textToHighlight:e})})})}):null,[po,n,e,s,a,r,l])},xc=()=>{const[e,{loading:n}]=ce(El),a=Ue();return{onRemoveMessage:m.exports.useCallback(async r=>{try{await e({variables:{id:r}}),await a(["messages"])}catch(o){console.log(o)}},[e,a]),loading:n}},bc=({isMyMessage:e,isHoverMessage:n,setIsHoverMessage:a,id:s})=>{const{onRemoveMessage:r}=xc();return e&&n&&g(_t,{strategy:"fixed",offset:[-200,-100],children:[t(Ot,{display:"flex",alignItems:"center",justifyContent:"center",rounded:"full",cursor:"pointer",borderWidth:1,borderColor:T("gray.300","white"),w:5,h:5,children:t(k,{display:"flex",alignItems:"center",justifyContent:"center",children:t($a,{})})}),t(Ft,{alignItems:"center",py:4,children:t(Ze,{onClick:async()=>{await r(s),a(!1)},icon:t(we,{as:en,w:4,h:4}),children:"X\xF3a tin nh\u1EAFn n\xE0y"})})]})},yc=({lastSeenMessageId:e,id:n,userData:a,isMyMessage:s,notSeen:r,loading:o,error:l,isGroup:i})=>{const c=T("white","slate.300");return m.exports.useMemo(()=>e===n&&!i?t(k,{w:4,h:4,rounded:"full",alignItems:"center",justifyContent:"center",display:"flex",flexDirection:"column",overflow:"hidden",bg:c,children:t(Se,{src:(a==null?void 0:a.avatar)===ze?"":a==null?void 0:a.avatar,size:"xs"})}):s&&r||o||e===n&&i?t(k,{w:4,h:4,rounded:"lg",alignItems:"center",justifyContent:"center",display:"flex",flexDirection:"column",bg:o||l?"transparent":"gray.400",border:o||l?"1px solid #838b8b":void 0,children:t(Sr,{fill:"#fff",className:"w-3 h-3"})}):null,[e,n,l,o,r,s,i,a])},ho=({attachments:e,attachmentIndex:n,setAttachmentIndex:a,showImageViewer:s,setShowImageViewer:r})=>t(k,{w:"100%",h:"100%",position:"relative",children:t(Ar,{imgs:e.map(o=>{var l;return{src:(l=o==null?void 0:o.attachment)==null?void 0:l.fullUrl}}),currImg:n,isOpen:s,onClose:()=>r(!1),showImgCount:!1,backdropCloseable:!0,onClickPrev:()=>{n>1&&a(n-1)},onClickNext:()=>{n<(e==null?void 0:e.length)-1&&a(n+1)}})}),Ec=kr.extend({addAttributes(){var e,n,a,s,r;return A(b({},(e=this.parent)==null?void 0:e.call(this)),{backgroundColor:{default:(a=(n=this.options)==null?void 0:n.backgroundColor)!=null?a:null,parseHTML:o=>o.getAttribute("data-background-color"),renderHTML:o=>({"data-background-color":o.backgroundColor,style:`background-color: ${o.backgroundColor}`})},style:{default:(r=(s=this.options)==null?void 0:s.style)!=null?r:null,parseHTML:o=>o.getAttribute("colwidth"),renderHTML:o=>({style:`${o.style?`width: ${o.colwidth}px`:null}`,colspan:o.colspan,rowspan:o.rowspan,colwidth:o.colwidth})}})}}),Cc=vr.extend({renderHTML({HTMLAttributes:e}){return["div",{class:"table-tiptap"},["table",e,["tbody",0]]]}}),$c=wr.extend({content:"paragraph*",addAttributes(){var e;return A(b({},(e=this.parent)==null?void 0:e.call(this)),{cite:{default:this.options.cite},class:{default:this.options.class},title:{default:this.options.title}})}}),Sc=Ir.extend({content:"paragraph*",addAttributes(){var e,n,a,s,r,o,l,i;return A(b({},(e=this.parent)==null?void 0:e.call(this)),{rel:{default:(a=(n=this==null?void 0:this.options)==null?void 0:n.rel)!=null?a:"noopener nofollow noreferrer"},target:{default:(r=(s=this==null?void 0:this.options)==null?void 0:s.target)!=null?r:"_blank"},"data-contextual":{default:((o=this==null?void 0:this.options)==null?void 0:o["data-contextual"])||void 0},"data-contextual-tag":{default:((l=this==null?void 0:this.options)==null?void 0:l["data-contextual-tag"])||void 0},"data-contextual-tag-id":{default:((i=this==null?void 0:this.options)==null?void 0:i["data-contextual-tag-id"])||void 0}})}}),Ac=Tr.extend({addAttributes(){var e,n,a,s,r;return A(b({},(e=this.parent)==null?void 0:e.call(this)),{alt:{default:(a=(n=this==null?void 0:this.options)==null?void 0:n.alt)!=null?a:"image-alt"},title:{default:(r=(s=this==null?void 0:this.options)==null?void 0:s.title)!=null?r:"image-title"}})}}),kc=m.exports.forwardRef((i,l)=>{var c=i,{activeKey:e="",activeOptions:n={},isActive:a=!1,icon:s,label:r=""}=c,o=Re(c,["activeKey","activeOptions","isActive","icon","label"]);return s?t(Lt,{label:r,children:t(tn,b({ref:l,colorScheme:a?"brand":"gray",variant:"solid",fontSize:"24px",icon:s},o))}):t(Lt,{label:r,children:t(Q,A(b({ref:l,colorScheme:a?"brand":"gray",variant:"solid"},o),{children:o.children}))})});var X=m.exports.memo(kc);const vc=[{name:"H1",value:1},{name:"H2",value:2},{name:"H3",value:3},{name:"H4",value:4},{name:"H5",value:5},{name:"H6",value:6}],go=m.exports.memo(()=>{const{editor:e}=m.exports.useContext(Co);return e?t(Ie,{children:g(Nt,{children:[t(Mt,{children:t(X,{label:"H tag",icon:t(Pr,{})})}),t(Rt,{zIndex:99,p:0,children:t(nn,{p:2,children:t(Ee,{gap:2,children:vc.map(n=>t(X,{isActive:e.isActive("heading",{level:n.value}),activeKey:"heading",activeOptions:{level:n.value},onClick:()=>e.chain().focus().toggleHeading({level:n.value}).run(),children:n.name},n.value))})})})]})}):null}),wc=m.exports.memo(({editor:e})=>{if(!e)return null;const n=m.exports.useCallback(()=>{e.chain().focus().insertTable({rows:3,cols:3,withHeaderRow:!0}).run()},[e]),a=m.exports.useCallback(()=>{e.chain().focus().deleteTable().run()},[e]),s=m.exports.useCallback(()=>{e.chain().focus().addColumnAfter().run()},[e]),r=m.exports.useCallback(()=>{e.chain().focus().addColumnBefore().run()},[e]),o=m.exports.useCallback(()=>{e.chain().focus().deleteColumn().run()},[e]),l=m.exports.useCallback(()=>{e.chain().focus().addRowBefore().run()},[e]),i=m.exports.useCallback(()=>{e.chain().focus().addRowAfter().run()},[e]),c=m.exports.useCallback(()=>{e.chain().focus().deleteRow().run()},[e]),f=m.exports.useCallback(()=>{e.chain().focus().mergeCells().run()},[e]),p=m.exports.useCallback(()=>{e.chain().focus().splitCell().run()},[e]),d=m.exports.useCallback(()=>{e.chain().focus().toggleHeaderColumn().run()},[e]);return g(ot,{minChildWidth:38,gap:2,p:2,bg:"gray.50",w:"full",maxW:350,children:[t(X,{onClick:n,icon:t(_r,{}),label:"Th\xEAm table"}),t(X,{onClick:a,icon:t(Or,{}),label:"X\xF3a table"}),t(X,{onClick:s,icon:t(Fr,{}),label:"Th\xEAm c\u1ED9t b\xEAn ph\u1EA3i"}),t(X,{onClick:r,icon:t(Lr,{}),label:"Th\xEAm c\u1ED9t b\xEAn tr\xE1i"}),t(X,{onClick:o,icon:t(Nr,{}),label:"X\xF3a c\u1ED9t"}),t(X,{onClick:l,icon:t(Mr,{}),label:"Th\xEAm h\xE0ng ph\xEDa tr\xEAn"}),t(X,{onClick:i,icon:t(Rr,{}),label:"Th\xEAm h\xE0ng b\xEAn d\u01B0\u1EDBi"}),t(X,{onClick:c,icon:t(Dr,{}),label:"X\xF3a h\xE0ng"}),t(X,{onClick:f,icon:t(Br,{}),label:"G\u1ED9p c\xE1c \xF4"}),t(X,{onClick:p,icon:t(zr,{}),label:"T\xE1ch c\xE1c \xF4"}),t(X,{onClick:d,icon:t(jr,{}),label:"Toggle header c\u1ED9t"}),t(X,{onClick:()=>e.chain().focus().toggleHeaderRow().run(),icon:t(qr,{}),label:"Toggle header h\xE0ng"}),t(X,{onClick:()=>e.chain().focus().toggleHeaderCell().run(),icon:t(Ur,{}),label:"Toggle header cell"})]})}),mo=m.exports.memo(({editor:e})=>e?g(Nt,{children:[t(Mt,{children:t(X,{label:"Table",icon:t(Hr,{})})}),t(Rt,{zIndex:99,p:0,children:t(nn,{p:0,children:t(wc,{editor:e})})})]}):null),fo=e=>{const c=e,{isMultiple:n,children:a}=c,s=Re(c,["isMultiple","children"]),{getInputProps:r,getCheckboxProps:o}=n?ha(s):ga(s),l=r(),i=o();return g(k,{as:"label",rounded:"base",overflow:"hidden",d:"block",children:[t("input",A(b({},l),{style:{display:"none"}})),g(k,A(b({},i),{cursor:"pointer",opacity:.7,_checked:{borderColor:"red.500",opacity:1},_focus:{boxShadow:"none"},pos:"relative",children:[l.checked&&t(ma,{color:"green.500",fontSize:"2xl",pos:"absolute",top:2,left:2,zIndex:2}),a]}))]})},Ic=async()=>{const e=Je(),n=da(e);(!Je()||Je()&&Date.now()>=n*1e3)&&await hn()};async function Tc(e){const a="https://graph-api.zim.vn/upload";try{if(e)return await Ic(),await(await fetch(a,{method:"POST",credentials:"include",headers:{Authorization:`Bearer ${Je()}`,Accept:"*/*","x-no-compression":"true"},body:e})).json()}catch(s){console.log("uploadFiles : "+s)}}const Pc=E`
  fragment Media on Media {
    id
    type
    path
    variants {
      id
      width
      height
      path
      type
    }
    filename
    title
    visibility
    width
    height
  }
`,_c=E`
  query medias(
    $first: Int
    $after: String
    $type: [MediaType]
    $width: Int
    $height: Int
    $search: String
    $userId: Int
  ) @api(name: "zim") {
    medias(
      first: $first
      after: $after
      type: $type
      width: $width
      height: $height
      search: $search
      userId: $userId
    ) {
      edges {
        node {
          ...Media
        }
      }
      pageInfo {
        startCursor
        endCursor
        hasNextPage
      }
    }
  }
  ${Pc}
`,Oc=e=>e.scrollHeight-e.scrollTop<=e.clientHeight+10,Fc={control:e=>A(b({},e),{minHeight:42,backgroundColor:"white",borderColor:"var(--chakra-colors-gray-200)",borderRadius:"var(--chakra-radii-md)",":hover":{borderColor:"var(--chakra-colors-gray-300)"}}),menuPortal:e=>A(b({},e),{zIndex:2e7}),container:e=>A(b({},e),{fontSize:"16px",width:"100%",":focus":{borderWidth:2,borderColor:"var(--chakra-colors-blue-500)"},zIndex:1e5}),IndicatorsContainer:e=>A(b({},e),{minHeight:40}),option:(e,{isDisabled:n,isSelected:a})=>A(b({},e),{color:n?"#ccc":a?"white":"black",cursor:n?"not-allowed":"default",fontSize:"15px",":active":b({},e[":active"])}),multiValue:e=>A(b({},e),{backgroundColor:"#ccc"}),multiValueLabel:e=>A(b({},e),{color:"var(--chakra-colors-gray-700)",backgroundColor:"var(--chakra-colors-gray-200)"}),multiValueRemove:e=>A(b({},e),{color:"var(--chakra-colors-red-200)",backgroundColor:"var(--chakra-colors-gray-200)",borderRadius:0,":hover":{color:"var(--chakra-colors-red-500)",backgroundColor:"var(--chakra-colors-red-100)"}})},Lc=(e,n)=>{const[a,s]=m.exports.useState(e);return m.exports.useEffect(()=>{const r=setTimeout(()=>s(e),n);return()=>clearTimeout(r)},[e,n]),a},xo=[{label:"T\u1EC7p tin",value:"file"},{label:"H\xECnh \u1EA3nh",value:"image"}],Nc=({selectedCallback:e,onClose:n,isOpen:a,isMultiple:s=!0,defaultTypes:r=["image"],enableTypeFilter:o,enableSizeFilter:l,width:i,height:c})=>{var ge,ye,Oe,De,Y,oe,Fe,Le;const[f,p]=m.exports.useState(!1),[d,$]=m.exports.useState([]),[y,u]=m.exports.useState(!1),[h,x]=Gr(!0),[S,w]=m.exports.useState(""),I=Lc(S,500),[v,R]=m.exports.useState([...r]),[_]=re(),j={first:20,type:Sa(v),search:I,height:0,width:0,userId:parseInt(_.id,10)};h&&i&&c&&(j.width=i,j.height=c);const{data:C,fetchMore:P,refetch:L,loading:H}=he(_c,{variables:j,notifyOnNetworkStatusChange:!0,skip:!a}),W=async()=>{p(!0);try{await L()}catch(U){console.log({e:U})}p(!1)},G=async()=>{const U=document.createElement("input");U.setAttribute("type","file"),v.includes("file")||U.setAttribute("accept","image/*"),U.click(),U.onchange=async()=>{const ae=U.files[0],fe=new FormData;fe.append("file",ae),fe.append("visibility","public"),await Tc(fe),await W()}},{getCheckboxProps:ne,setValue:te}=Aa({onChange:$}),{getRootProps:J,getRadioProps:de}=Vr({name:"images",onChange:(...U)=>$(U)}),se=J(),z=()=>{var U;e((U=C==null?void 0:C.medias)==null?void 0:U.edges.filter(ae=>d.includes(ae.node.id))),te([]),n()},O=()=>{n(),te([])},Z=U=>{u(Oc(U.target))};return m.exports.useEffect(()=>{var U;y&&typeof P=="function"&&((U=C.medias)==null?void 0:U.pageInfo.hasNextPage)&&!H&&(async()=>{var ae;await P({variables:{after:(ae=C.medias)==null?void 0:ae.pageInfo.endCursor,first:20,type:Sa(v)}})})()},[y]),t(Ie,{children:g(rt,{closeOnOverlayClick:!1,isCentered:!0,isOpen:a,onClose:O,size:"6xl",children:[t(Ye,{}),g(st,{children:[t(Ke,{children:"Th\u01B0 vi\u1EC7n media"}),t(it,{}),g(lt,{children:[g(D,{mb:4,justifyContent:"space-between",children:[t(Q,{as:Ve,variant:"outline",colorScheme:"black",onClick:G,leftIcon:t(Wr,{}),isLoading:f,children:"Select from computer"}),o&&t(k,{width:300,children:t(ka,{placeholder:"Ch\u1ECDn lo\u1EA1i media",styles:Fc,isMulti:!0,value:v.map(U=>xo.find(ae=>ae.value===U)).filter(U=>U),onChange:U=>{R(U.map(ae=>ae.value))},options:xo})}),l&&i&&c&&g(D,{children:[g(N,{children:["Ch\u1EC9 filter nh\u1EEFng \u1EA3nh size ",i," x ",c]}),t(Qr,{isChecked:h,onChange:x})]})]}),g(k,{width:350,pos:"relative",role:"group",children:[t(vt,{_focus:{color:"gray.900",borderColor:"gray.900"},_groupHover:{color:"gray.900",borderColor:"gray.900"},type:"text",placeholder:"T\xECm ki\u1EBFm media",size:"lg",pr:12,mb:4,value:S,onChange:U=>w(U.target.value)}),t(Zr,{_groupHover:{color:"gray.900"},pos:"absolute",right:4,top:"50%",color:"gray.400",transform:"translateY(-50%)"})]}),g(k,{position:"relative",h:"calc(65vh - 50px)",overflow:f?"hidden":"auto",w:"full",flexGrow:1,p:3,onScroll:Z,children:[s?t(D,{alignItems:"stretch",flexWrap:"wrap",mx:"-0.75rem",spacing:0,children:((ye=(ge=C==null?void 0:C.medias)==null?void 0:ge.edges)==null?void 0:ye.length)>0&&((De=(Oe=C==null?void 0:C.medias)==null?void 0:Oe.edges)==null?void 0:De.map(({node:U})=>{const ae=ne({value:U.id});return t(k,{w:{lg:"25%",base:"50%"},p:3,children:t(fo,A(b({isMultiple:s},ae),{children:t(bo,b({},U))}))},`${U.id}`)}))}):t(D,A(b({alignItems:"stretch",flexWrap:"wrap",mx:"-0.75rem",spacing:0},se),{children:((oe=(Y=C==null?void 0:C.medias)==null?void 0:Y.edges)==null?void 0:oe.length)>0&&((Le=(Fe=C==null?void 0:C.medias)==null?void 0:Fe.edges)==null?void 0:Le.map(({node:U})=>{const ae=de({value:U.id});return t(k,{w:{lg:"25%",base:"50%"},p:3,children:t(fo,A(b({isMultiple:!1},ae),{children:t(bo,b({},U))}))},`${U.id}`)}))})),f&&g(D,{bgColor:"whiteAlpha.800",pos:"absolute",top:0,bottom:0,right:0,left:0,spacing:2,justifyContent:"center",children:[t(ht,{color:"black",size:"xl"}),t(N,{fontSize:"2xl",color:"black",children:"\u0110ang ti\u1EBFn h\xE0nh upload"})]})]})]}),g(gt,{children:[t(Q,{colorScheme:"blue",mr:3,disabled:!d.length,onClick:z,children:"X\xE1c nh\u1EADn"}),t(Q,{colorScheme:"gray",mr:0,onClick:O,children:"\u0110\xF3ng l\u1EA1i"})]})]})]})})};function bo({type:e,path:n,filename:a}){return e==="file"?t(k,{display:"flex",flexDirection:"column",justifyContent:"center",borderWidth:2,borderColor:"#ccc",borderRadius:10,paddingX:"12px",w:"full",h:"150",children:g(ee,{spacing:6,width:"full",overflow:"hidden",children:[t(Yr,{fontSize:"40"}),t(k,{w:"full",display:"table",style:{tableLayout:"fixed"},children:t(N,{display:"table-cell",textAlign:"center",whiteSpace:"nowrap",textOverflow:"ellipsis",overflow:"hidden",children:a})})]})}):t(k,{borderColor:"#ccc",borderRadius:10,borderWidth:2,w:"full",h:"150",backgroundSize:"cover",backgroundRepeat:"no-repeat",backgroundImage:`url('${Kr("https://graph-api.zim.vn",n)}')`})}const yo=m.exports.memo(({editor:e})=>{const n=i=>{i&&e.chain().focus().setImage({src:i}).run()},[a,s]=m.exports.useState(!1);return g(Ie,{children:[t(X,{activeKey:"image",onClick:()=>{s(!0)},icon:t(Xr,{}),label:"H\xECnh \u1EA3nh"}),t(Nc,{isOpen:a,onClose:()=>{s(!1)},selectedCallback:i=>{i.map(c=>{n(`https://graph-api.zim.vn/${c.node.path}`)})},isMultiple:!0})]})}),Mc=m.exports.memo(({editor:e,stickyMenuBar:n})=>{const a=T("white","slate.700"),s=T("gray.200","slate.600");return t(Ie,{children:g(ot,{minChildWidth:"38px",gap:2,w:"full",bg:a,zIndex:10,px:2,py:2,pos:n?"sticky":"static",top:0,borderBottom:"1px solid",borderBottomColor:s,children:[t(go,{}),t(X,{onClick:()=>e.chain().focus().toggleBold().run(),activeKey:"bold",label:"In \u0111\u1EADm",icon:t(Jr,{})}),t(X,{onClick:()=>e.chain().focus().toggleItalic().run(),activeKey:"italic",label:"In nghi\xEAng",icon:t(es,{})}),t(X,{onClick:()=>e.chain().focus().toggleUnderline().run(),activeKey:"underline",label:"G\u1EA1ch ch\xE2n",icon:t(ts,{})}),t(X,{onClick:()=>e.chain().focus().toggleStrike().run(),activeKey:"strike",label:"G\u1EA1ch ngang",icon:t(ns,{})}),t(X,{onClick:()=>e.chain().focus().setTextAlign("left").run(),label:"C\u0103n l\u1EC1 tr\xE1i",icon:t(as,{})}),t(X,{onClick:()=>e.chain().focus().setTextAlign("center").run(),label:"C\u0103n gi\u1EEFa",icon:t(os,{})}),t(X,{onClick:()=>e.chain().focus().setTextAlign("right").run(),label:"C\u0103n l\u1EC1 ph\u1EA3i",icon:t(rs,{})}),t(X,{onClick:()=>e.chain().focus().toggleBulletList().run(),label:"Danh s\xE1ch bullet",icon:t(va,{})}),t(X,{onClick:()=>e.chain().focus().toggleOrderedList().run(),label:"Danh s\xE1ch s\u1ED1 th\u1EE9 t\u1EF1",icon:t(wa,{})}),t(mo,{editor:e}),t(yo,{editor:e})]})})});m.exports.memo(({editor:e})=>g(ot,{minChildWidth:38,gap:2,p:2,bg:"gray.50",w:{base:300,sm:300,md:450},children:[t(go,{}),t(X,{onClick:()=>e.chain().focus().toggleBulletList().run(),label:"Danh s\xE1ch bullet",icon:t(va,{}),isActive:e.isActive("bulletList")}),t(X,{onClick:()=>e.chain().focus().toggleOrderedList().run(),label:"Danh s\xE1ch s\u1ED1 th\u1EE9 t\u1EF1",icon:t(wa,{}),isActive:e.isActive("orderedList")}),t(mo,{editor:e}),t(yo,{editor:e})]}));const Rc=m.exports.memo(({editor:e})=>{const{onOpen:n,onClose:a,isOpen:s}=qe(),r=m.exports.useRef(null),[o,l]=m.exports.useState(""),[i,c]=m.exports.useState(""),f=async()=>{const p=e.getAttributes("image").src;e.chain().focus().setImage({src:p,alt:i,title:o}).run(),a()};return m.exports.useEffect(()=>{e.isActive("image")?(c(e.getAttributes("image").alt),l(e.getAttributes("image").title)):(c(""),l(""))},[s]),g(D,{bg:"white",shadow:"base",p:2,spacing:4,rounded:4,children:[g(k,{fontSize:"sm",children:[g(N,{as:"div",children:[t("strong",{children:"Title:"})," ",e.getAttributes("image").title]}),g(N,{as:"div",children:[t("strong",{children:"Alt:"})," ",e.getAttributes("image").alt]})]}),g(Nt,{isOpen:s,initialFocusRef:r,onOpen:n,onClose:a,placement:"top",closeOnBlur:!0,children:[t(Mt,{children:t(X,{activeKey:"image",label:"Ch\u1EC9nh s\u1EEDa",icon:t(ss,{})})}),t(Rt,{children:t(k,{p:4,bg:"white",shadow:"base",children:g(ee,{spacing:4,alignItems:"flex-start",children:[t(Me,{ref:r,label:"Title",id:"title-url",value:o,onChange:p=>l(p.target.value),autoComplete:"off"}),t(Me,{label:"Alt",id:"alt-url",value:i,onChange:p=>c(p.target.value),autoComplete:"off"}),t(Q,{colorScheme:"teal",onClick:f,children:"C\u1EADp nh\u1EADt"})]})})})]})]})});var Dc=an.create({name:"iframe",group:"block",atom:!0,defaultOptions:{allowFullscreen:!0,HTMLAttributes:{class:"iframe-wrapper"}},addAttributes(){return{class:{default:null},src:{default:null},frameborder:{default:0},allowfullscreen:{default:this.options.allowFullscreen,parseHTML:()=>this.options.allowFullscreen},width:{default:this.options.width},height:{default:this.options.height}}},parseHTML(){return[{tag:"iframe"}]},renderHTML({HTMLAttributes:e}){return["div",this.options.HTMLAttributes,["iframe",e]]},addCommands(){return{setIframe:e=>({tr:n,dispatch:a})=>{const{selection:s}=n,r=this.type.create(e);return a&&n.replaceRangeWith(s.from,s.to,r),!0}}}});an.create({name:"video",group:"block",atom:!0,defaultOptions:{HTMLAttributes:{class:"iframe-wrapper"}},addAttributes(){return{class:{default:this.options.class},src:{default:null},autoplay:{default:this.options.width},loop:{default:this.options.loop},muted:{default:this.options.muted},preload:{default:this.options.preload},width:{default:this.options.width},height:{default:this.options.height},controls:{default:this.options.controls}}},parseHTML(){return[{tag:"video"}]},renderHTML({HTMLAttributes:e}){return["div",this.options.HTMLAttributes,["video",e]]},addCommands(){return{Video:e=>({tr:n,dispatch:a})=>{const{selection:s}=n,r=this.type.create(e);return a&&n.replaceRangeWith(s.from,s.to,r),!0}}}});var Bc=an.create({name:"spacing",content:"inline*",atom:!0,draggable:!0,defaultOptions:{HTMLAttributes:{class:"space-wrapper","data-question":"","data-question-group":"","data-label":"","data-type":""}},addOptions(){return{inline:!0,HTMLAttributes:{}}},inline(){return this.options.inline},group(){return this.options.inline?"inline":"block"},addAttributes(){var e,n,a,s,r,o,l;return{class:{default:((e=this==null?void 0:this.options)==null?void 0:e.class)||void 0},"data-question":{default:(a=(n=this==null?void 0:this.options)==null?void 0:n["data-question"])!=null?a:"",parseHTML:i=>i.getAttribute("data-question")},"data-question-group":{default:(r=(s=this==null?void 0:this.options)==null?void 0:s["data-question-group"])!=null?r:"",parseHTML:i=>i.getAttribute("data-question-group")},"data-type":{default:(l=(o=this==null?void 0:this.options)==null?void 0:o["data-type"])!=null?l:"",parseHTML:i=>i.getAttribute("data-type")},"data-label":{default:"",parseHTML:i=>i.getAttribute("data-label"),renderHTML:i=>i["data-label"]?{"data-label":i["data-label"]}:{"data-label":""}}}},parseHTML(){return[{tag:"span.space-wrapper"}]},renderHTML({HTMLAttributes:e}){return["span",is(this.options.HTMLAttributes,e)]},renderText({HTMLAttributes:e}){var n;return(n=e==null?void 0:e["data-label"])!=null?n:"r\u1ED7ng"},addCommands(){return{addSpace:e=>({commands:n})=>n.insertContent({type:this.name,attrs:e})}}});function Eo(e,n){const[a,s]=m.exports.useState(e);return m.exports.useEffect(()=>{const r=setTimeout(()=>{s(e)},n);return()=>{clearTimeout(r)}},[e,n]),a}const Co=m.exports.createContext({editor:null}),zc=m.exports.forwardRef(({onChange:e,placeholder:n="Nh\u1EADp n\u1ED9i dung",defaultValue:a="",extensions:s=[],enableSpaceMenu:r=!1,showWordCount:o=!0,isDisabled:l=!1,stickyMenuBar:i=!1},c)=>{const[f,p]=m.exports.useState(0),d=ls({extensions:[cs,Cc.configure({resizable:!0}),ds,us,Ec,$c,ps,hs,Ac.configure({inline:!0,HTMLAttributes:{class:"custom-image"}}),Sc.configure({openOnClick:!1}),gs.configure({placeholder:n||"Nh\u1EADp n\u1ED9i dung",emptyEditorClass:"is-editor-empty"}),Dc.configure({inline:!0}),ms.configure({types:["heading","paragraph"],alignments:["left","right","center"]}),Bc.configure({inline:!0}),...s],content:a,onCreate:async({editor:h})=>{const x=h.state.doc.textContent.split(" ").length;p(x)},onUpdate:({editor:h})=>{const x=h.state.doc.textContent.split(" ").length;p(x);const S=h.getHTML();typeof e=="function"&&e(S)},editable:!l}),$=T("white","slate.700"),y=T("gray.200","slate.700"),u=Eo(d,1e3);return g(Co.Provider,{value:{editor:u},children:[o&&g(k,{mb:4,children:["Word count: ",t("strong",{children:f})]}),t(k,{p:2,pt:0,borderColor:y,borderRadius:4,borderWidth:1,h:"full",bg:$,children:d&&g(Ie,{children:[!l&&g(Ie,{children:[t(Mc,{editor:d,enableSpaceMenu:r,stickyMenuBar:i}),t(Ia,{editor:d,pluginKey:"bubbleImageMenu",shouldShow:({editor:h})=>h.isActive("image"),children:t(Rc,{editor:d})}),t(Ia,{editor:d,tippyOptions:{placement:"bottom"},pluginKey:"bubbleTextSelection",shouldShow:({editor:h,view:x,state:S})=>h.isActive("link",{"data-contextual":!0})})]}),t(fs,{editor:d,ref:c})]})})]})});var St=m.exports.memo(zc);function $o(){const e=Ta(),n=bs(),a=ys();return m.exports.useMemo(()=>({navigate:a,pathname:n.pathname,query:b(b({},xs.parse(n.search)),e),location:n}),[e,n,a])}const jc=m.exports.forwardRef((l,o)=>{var i=l,{onChange:e,paramKey:n,options:a,allOption:s}=i,r=Re(i,["onChange","paramKey","options","allOption"]);var $;Es();const c=$o();Qe();const f=m.exports.useMemo(()=>({menuPortal:y=>A(b({},y),{zIndex:1400}),container:(y,u)=>A(b({},y),{color:T("var(--chakra-colors-gray-900)","var(--chakra-colors-slate-400)")}),placeholder:(y,u)=>A(b({},y),{color:T("var(--chakra-colors-gray-400)","var(--chakra-colors-slate-500)")}),input:(y,u)=>A(b({},y),{minHeight:30,color:T("var(--chakra-colors-gray-900)","var(--chakra-colors-slate-400)")}),control:(y,u)=>A(b({},y),{backgroundColor:u.isDisabled?"var(--chakra-colors-gray.500)":T("var(--chakra-colors-white)","var(--chakra-colors-slate-700)"),borderRadius:"var(--chakra-radii-md)",borderColor:T("var(--chakra-colors-gray.300)","var(--chakra-colors-slate-600)")}),menu:(y,u)=>A(b({},y),{zIndex:3}),menuList:(y,u)=>A(b({},y),{backgroundColor:T("var(--chakra-colors-white)","var(--chakra-colors-slate-700)")}),option:(y,u)=>(u.isFocused&&console.log({state:u}),A(b({},y),{color:u.isFocused?T("var(--chakra-colors-gray.900)","var(--chakra-colors-slate-900)"):T("var(--chakra-colors-gray.900)","currentColor"),"&:hover":{color:u.isSelected?T("var(--chakra-colors-white)","var(--chakra-colors-white)"):T("var(--chakra-colors-gray.900)","var(--chakra-colors-slate-900)")}})),singleValue:(y,u)=>A(b({},y),{color:T("var(--chakra-colors-gray.900)","var(--chakra-colors-slate-300)")})}),[]),p=m.exports.useCallback((y,u)=>{e(y,u)},[r,c.query,n]),d=m.exports.useMemo(()=>s?[s,...a]:a,[r,a,s]);return g(kt,{isInvalid:!!(r==null?void 0:r.error),width:"100%",children:[(r==null?void 0:r.label)&&t(Yt,{htmlFor:r.id,children:r==null?void 0:r.label}),t(ka,A(b({ref:o,id:r.id},r),{styles:f,onChange:p,options:d,menuPortalTarget:document.body})),(r==null?void 0:r.error)&&t(wt,{children:($=r==null?void 0:r.error)==null?void 0:$.message})]})});var Gt=m.exports.memo(jc);const qc=({questionText:e})=>{var o;const{data:n}=he(Gi),{register:a,formState:{errors:s}}=Pa(),r=(o=n==null?void 0:n.QAPostCategoriesTree)!=null?o:[];return g(Ee,{direction:"column",w:"full",justifyContent:"stretch",alignItems:"stretch",className:"reply-editor",gap:4,children:[g(Ae,{spacing:1,children:[t(N,{fontSize:14,children:"C\xE2u h\u1ECFi"}),t(k,{bg:"gray.100",borderWidth:1,w:"100%",p:4,color:"black",rounded:"md",children:e})]}),t(Me,A(b({label:"Ti\xEAu \u0111\u1EC1",placeholder:"Nh\u1EADp ti\xEAu \u0111\u1EC1..."},a("title",{required:!0})),{error:s==null?void 0:s.name})),t(mt,{rules:{required:"Vui l\xF2ng ch\u1ECDn ch\u1EE7 \u0111\u1EC1"},name:"categories",render:({field:l,fieldState:{error:i}})=>t(Gt,b({error:i,label:"Ch\u1EE7 \u0111\u1EC1",options:r,getOptionLabel:c=>c.title,getOptionValue:c=>c.id+"",formatOptionLabel:c=>t(N,{children:c.title})},l))}),g(Ae,{spacing:1,children:[t(N,{fontSize:14,children:"C\xE2u tr\u1EA3 l\u1EDDi"}),t(mt,{name:"answer",render:({field:l})=>t(St,b({showWordCount:!1,placeholder:"Nh\u1EADp c\xE2u tr\u1EA3 l\u1EDDi..."},l))})]})]})},Uc=({onClose:e,isOpen:n,questionText:a,messageQAOwnerId:s,setIsHoverMessage:r})=>{const o=m.exports.useRef(),l=Dt({mode:"onBlur",reValidateMode:"onChange"}),i=Te(),[c]=ce(Il),[f,{loading:p}]=ce(Tl),d=m.exports.useCallback(async({title:$,categories:y,answer:u})=>{var h;try{const{data:x}=await c({variables:{input:pe({title:$,content:a,type:"QUESTION",categoryIds:(y==null?void 0:y.id)?[y==null?void 0:y.id]:""},S=>S),userId:s}});await f({variables:{ref:(h=x==null?void 0:x.createQAPost)==null?void 0:h.id,content:u,type:"QAPost"}}),e(),r(!1),await i({title:"Success.",description:"\u0110\u1EA9y c\xE2u h\u1ECFi l\xEAn forum th\xE0nh c\xF4ng",status:"success",duration:3e3,isClosable:!0})}catch(x){console.log(x)}},[a,s,e,r]);return t(on,A(b({},l),{children:g(rt,{finalFocusRef:o,isOpen:n,onClose:e,size:"3xl",children:[t(Ye,{}),g(st,{children:[t(Ke,{children:"T\u1EA1o QA "}),t(it,{}),g("form",{onSubmit:l.handleSubmit(d),children:[t(lt,{children:t(qc,{questionText:a})}),g(gt,{children:[t(Q,{leftIcon:t(we,{as:rn}),colorScheme:"green",mr:4,type:"submit",isLoading:p,children:"X\xE1c nh\u1EADn"}),t(Q,{variant:"ghost",onClick:e,children:"H\u1EE7y"})]})]})]})]})}))},Hc=({isParticipant:e,isHoverMessage:n,messageQAText:a,messageQAOwnerId:s,setIsHoverMessage:r})=>{const{isOpen:o,onOpen:l,onClose:i}=qe();return e&&n&&g(_t,{strategy:"fixed",offset:[0,10],children:[t(Ot,{display:"flex",alignItems:"center",justifyContent:"center",rounded:"full",cursor:"pointer",borderWidth:1,borderColor:T("gray.300","white"),w:5,h:5,mt:"-20px",children:t(k,{display:"flex",alignItems:"center",justifyContent:"center",children:t($a,{})})}),t(Ft,{alignItems:"center",py:4,children:t(Ze,{onClick:()=>{l()},icon:t(we,{as:Cs,w:4,h:4}),children:"\u0110\xE1nh d\u1EA5u c\xE2u h\u1ECFi"})}),o&&t(Uc,{isOpen:o,onClose:i,questionText:a,messageQAOwnerId:s,setIsHoverMessage:r})]})};function Gc({isMyMessage:e,text:n,loading:a,notSeen:s,error:r,createdAt:o,lastSeenMessageId:l,id:i,userData:c,attachments:f,deletedAt:p,from:d,isGroup:$,conversation:y}){var j,C;const[u]=re(),[h,x]=m.exports.useState(!1),[S,w]=m.exports.useState(null),[I,v]=m.exports.useState(!1),R=T("white","slate.300"),_=m.exports.useMemo(()=>{var P,L,H,W;return t(k,{as:"div",w:6,h:6,rounded:"full",alignItems:"center",justifyContent:"center",display:"flex",flexDirection:"column",position:"absolute",overflow:"hidden",bg:R,className:`${e?"-right-8 top-3":"-left-[30px] bottom-6"}`,children:t(Se,{size:"xs",src:((L=(P=y==null?void 0:y.participants)==null?void 0:P.find(G=>(G==null?void 0:G.userId)===d))==null?void 0:L.avatar)===ze?"":(W=(H=y==null?void 0:y.participants)==null?void 0:H.find(G=>(G==null?void 0:G.userId)===d))==null?void 0:W.avatar})})},[e,R,y,d]);return t(D,{onMouseEnter:()=>!p&&x(!0),onMouseLeave:()=>!p&&x(!1),w:"full",position:"relative",alignItems:"center",justifyContent:e?"flex-end":"flex-start",children:t(D,{alignItems:"center",children:t(k,{position:"relative",display:"flex",justifyContent:e?"flex-end":"flex-start ",children:g(k,{display:"flex",flexDirection:"column",as:"div",alignItems:e?"flex-end":"flex-start",children:[!e&&t(N,{color:"gray.400",fontSize:12,children:((C=(j=y==null?void 0:y.participants)==null?void 0:j.find(P=>(P==null?void 0:P.userId)===d))==null?void 0:C.fullName)||"\u1EA8n danh"}),g(D,{align:"center",children:[t(bc,{isMyMessage:e,isHoverMessage:h,setIsHoverMessage:x,id:i}),t(fc,{isMyMessage:e,text:n,deletedAt:p,createdAt:o}),!p&&(f==null?void 0:f.length)>0&&t(pc,{isMyMessage:e,files:f,onPress:P=>{w(P),v(!0)}}),$n.map(P=>P.id).includes(u.roleId)&&n&&t(Hc,{messageQAOwnerId:d,messageQAText:n,isHoverMessage:h,isParticipant:!e,setIsHoverMessage:x})]}),!(f==null?void 0:f.length)&&t($s,{className:`${e?"text-[#cbe9fc]":T("text-gray-100","text-slate-300")} absolute ${e?"top-4 -right-2.5":"bottom-6 -left-2.5"} transform ${e?void 0:"rotate-180"}`}),g(D,{justify:"flex-end",mt:1,alignItems:"center",children:[t(N,{letterSpacing:.1,fontSize:12,color:"gray.500",children:ve(o).format("HH:mm")}),t(yc,{isGroup:$,isMyMessage:e,lastSeenMessageId:l,notSeen:s,id:i,error:r,loading:a,userData:c})]}),_,t(ho,{setAttachmentIndex:w,attachments:f,attachmentIndex:S,showImageViewer:I,setShowImageViewer:v})]})})})})}const Vc=({conversationId:e,setConversationId:n,ownerId:a,messages:s,participants:r,userId:o,isGroup:l,loading:i,channel:c,isLoadMore:f,onLoadBeforeMessage:p,hasBeforeLoadMoreButton:d,searchLoading:$,onLoadAfterMessage:y,hasAfterLoadMoreButton:u})=>{var C;const[h,x]=m.exports.useState(""),{onMarkSeen:S}=En({setContent:()=>{},userId:o,group:c,setConversationId:n}),w=m.exports.useMemo(()=>We.exports.uniqBy(We.exports.reverse([].concat(s)),P=>P.id),[s]),I=(C=s.find(P=>{var L;if(P.from===a&&((L=P.seenBy)==null?void 0:L.length)>=2)return!0}))==null?void 0:C.id,v=r,R=v==null?void 0:v.filter(P=>P.userId!==a)[0],_=w[w.length-1],j=Ss();return Jt(()=>{var P;e&&(r==null?void 0:r.find(L=>L.userId===a))&&!i&&!((P=_==null?void 0:_.seenBy)==null?void 0:P.includes(a==null?void 0:a.toString()))&&async function(){await j(S(e))}()},[j,a,_,r,i,e]),g(ee,{alignItems:"stretch",w:"full",px:10,py:4,spacing:4,children:[d&&t(ee,{children:t(Q,{w:"100px",h:"30px",onClick:p,isLoading:$,loadingText:"Loading",colorScheme:"teal",variant:"outline",spinnerPlacement:"end",children:"Load more"})}),f&&t(As,{in:f,animateOpacity:!0,children:t(k,{pt:2,children:t($t,{color:"white",text:"loading"})})}),w==null?void 0:w.map((P,L)=>{var de,se,z,O,Z;const H=P.from===a,W=Tt((de=w[L])==null?void 0:de.createdAt),G=Tt((se=w[L-1])==null?void 0:se.createdAt),ne=Tt.duration(W.diff(G)),te=ne.asMinutes(),J=te>15&&t(uc,{time:P==null?void 0:P.createdAt});return g(at.Fragment,{children:[J,t(Gc,A(b({},P),{QAId:h,setQAId:x,isGroup:l,attachments:(z=P==null?void 0:P.attachments)!=null?z:[],loading:P==null?void 0:P.loading,error:P==null?void 0:P.error,isMyMessage:H,text:P==null?void 0:P.text,id:P==null?void 0:P.id,userData:R,lastSeenMessageId:I,createdAt:P==null?void 0:P.createdAt,notSeen:((O=P==null?void 0:P.seenBy)==null?void 0:O.length)===1&&((Z=P==null?void 0:P.seenBy)==null?void 0:Z.includes(a))}))]},L)}),u&&t(ee,{pb:2,children:t(Q,{w:"100px",h:"30px",onClick:y,isLoading:$,loadingText:"Loading",colorScheme:"teal",variant:"outline",spinnerPlacement:"end",children:"Load more"})})]})},Wc=({userId:e})=>{var x,S;const[n]=re(),a=Ue(),[s,r]=m.exports.useState(!1),o=F(w=>w.conversationId),l=F(w=>w.setConversationId),i=A(b({},pe({conversationId:o,userId:o?"":e==null?void 0:e.toString(),limit:10},w=>w)),{offset:0}),{data:c,loading:f,fetchMore:p,error:d}=he(Be,{variables:b({},i),skip:!o&&!e});Kt(Ja,{onSubscriptionData:({client:w,subscriptionData:I})=>{var R,_;const v=w.cache.readQuery({query:Be,variables:i});if(v==null?void 0:v.messages){let C=[...(_=(R=v==null?void 0:v.messages)==null?void 0:R.docs)!=null?_:[]];const{conversationUpdated:P}=I.data,{lastMessage:L,isDeleteMessage:H,id:W}=P,{id:G,from:ne}=L,te=C==null?void 0:C.find(J=>(J==null?void 0:J.id)===G);H&&!s&&o===W&&r(!0),!te&&n.id!==ne&&o===W&&(w.writeQuery({query:Be,variables:i,data:A(b({},v),{messages:A(b({},v.messages),{docs:[L,...C]})})}),setTimeout(()=>{const J=document.getElementById("chat-box");J&&(J.scrollTop=J.scrollHeight)},300))}},shouldResubscribe:!!o});const $=((x=c==null?void 0:c.messages)==null?void 0:x.docs)||[],y=(S=c==null?void 0:c.messages)==null?void 0:S.hasNextPage,{onLoadMore:u,isLoadingMore:h}=ut({hasNextPage:y,variables:A(b({},i),{offset:$.length+1}),fetchMore:p});return m.exports.useEffect(()=>{s&&(a(["messages","getAttachmentsInConversation"]),r(!1))},[s,a,r]),m.exports.useEffect(()=>{var w,I;($==null?void 0:$.length)>0&&l((I=(w=$==null?void 0:$[0])==null?void 0:w.conversation)==null?void 0:I.id)},[$]),{data:c,loading:f,isLoadMore:h,loadMore:u,hasNextPage:y,messages:$,error:d}},Qc=({userId:e,options:n})=>{var $,y,u;const a=F(h=>h.conversationId),s=F(h=>h.beforeId),r=F(h=>h.afterId),o=b({},pe({conversationId:a,userId:a?"":e==null?void 0:e.toString(),limit:10,before:s,after:r},h=>h)),{data:l,loading:i,error:c}=he(Be,A(b({variables:b({},o)},n),{skip:!s&&!r})),f=(($=l==null?void 0:l.messages)==null?void 0:$.docs)||[],p=(y=l==null?void 0:l.messages)==null?void 0:y.hasNextPage,d=(u=l==null?void 0:l.messages)==null?void 0:u.hasPrevPage;return{data:l,loading:i,hasNextPage:p,hasPrevPage:d,messages:f,error:c}},So=({conversationId:e="",search:n})=>{var y,u;const a=F(h=>h.setSearchResult),s=F(h=>h.messageId),r=F(h=>h.beforeId),[o,l]=m.exports.useState(!1),{refetch:i,called:c}=he(Hi,{variables:{conversationId:e,search:n},onCompleted:h=>{var x;a((x=h==null?void 0:h.searchMessage)==null?void 0:x.docs),l(!0)},onError:()=>{a([])},skip:e.length===0||!!s||!!r||!n}),{data:f,refetch:p}=he(zt,{variables:{messageId:s,limit:10},skip:!s}),d=(y=f==null?void 0:f.nearbyMessages)==null?void 0:y.hasPrevPage,$=(u=f==null?void 0:f.nearbyMessages)==null?void 0:u.hasNextPage;return m.exports.useEffect(()=>{(c&&n.length||c&&o&&(n==null?void 0:n.length)===0)&&i()},[n,o,c]),m.exports.useEffect(()=>{c&&s&&p()},[s,c]),{hasPrevPageNearbyMessages:d,hasNextPageNearbyMessages:$}},Zc=({conversationId:e,ownerId:n,userId:a,onSendMessage:s,participants:r,isGroup:o,setConversationId:l,channel:i,isShowSayHi:c,sendMessageLoading:f})=>{var ye,Oe,De;const p=Ea(),d=m.exports.useRef(0),$=m.exports.useRef(0),y=m.exports.useRef(null),u=m.exports.useRef(null),h=m.exports.useCallback(async()=>{f||await s({nativeEvent:{text:"Hi !!"},conversationId:e})},[s,e,f]),[x,S]=m.exports.useState(!1),w=F(Y=>Y.searchStringMessage),I=F(Y=>Y.setBeforeId),v=F(Y=>Y.setAfterId),R=F(Y=>Y.messageId),_=F(Y=>Y.beforeId),j=F(Y=>Y.afterId),{messages:C,loading:P,error:L,loadMore:H,isLoadMore:W,hasNextPage:G}=Wc({userId:a}),{hasPrevPageNearbyMessages:ne,hasNextPageNearbyMessages:te}=So({conversationId:e,search:w}),{hasNextPage:J,hasPrevPage:de}=Qc({userId:a,options:{onCompleted:Y=>{var fe,He,Ge,q,B;S(!0);const oe=(fe=p.cache.readQuery({query:zt,variables:{messageId:R,limit:10}}))!=null?fe:{},Fe=(Ge=(He=oe==null?void 0:oe.nearbyMessages)==null?void 0:He.docs)!=null?Ge:[],Le=(B=(q=Y==null?void 0:Y.messages)==null?void 0:q.docs)!=null?B:[],U=_?[...Fe,...Le]:[...Le,...Fe],ae=Xt(U,"id");$.current=u.current.scrollHeight,p.cache.writeQuery({query:zt,variables:{messageId:R,limit:10},data:A(b({},oe),{nearbyMessages:A(b({},oe==null?void 0:oe.nearbyMessages),{docs:ae})})}),S(!1),_&&(u.current.scrollTop=u.current.scrollHeight-$.current)}}}),se=(ye=p.cache.readQuery({query:zt,variables:{messageId:R,limit:10}}))!=null?ye:{},z=(De=(Oe=se==null?void 0:se.nearbyMessages)==null?void 0:Oe.docs)!=null?De:[],O=(w==null?void 0:w.length)>0&&(R==null?void 0:R.length)>0?z:C,Z=m.exports.useCallback(()=>{var Y;v(""),I((Y=z==null?void 0:z[z.length-1])==null?void 0:Y.id)},[z]),ge=m.exports.useCallback(()=>{var Y;I(""),v((Y=z==null?void 0:z[0])==null?void 0:Y.id)},[z]);return L?t(D,{bg:"blackAlpha.400",h:"100%",justifyContent:"center",children:t($t,{color:"white"})}):(m.exports.useEffect(()=>{const Y={attributes:!0,childList:!0,subtree:!0},oe=new MutationObserver((Fe,Le)=>{var U;for(const ae of Fe)if(ae.type==="childList"&&(u==null?void 0:u.current)&&!w){const fe=u.current;fe&&y.current!==e&&(fe.scrollTop=(U=fe.scrollHeight)!=null?U:5e3,y.current=e)}});return oe.observe(u.current,Y),()=>{oe.disconnect()}},[e,C,y.current,w]),m.exports.useEffect(()=>()=>{y.current&&(y.current=null)},[]),t(k,{ref:u,bg:T("white","#10172a"),h:"full",flex:1,overflowY:"scroll",position:"relative",id:"chat-box",onScroll:async Y=>{const{scrollTop:oe}=Y.nativeEvent.target;oe<=20&&oe<d.current&&G&&!W&&!w&&(await H(),W||(document.documentElement.scrollTop=u.current.scrollTop=oe+35)),d.current=oe},children:(O==null?void 0:O.length)===0&&!P||!e&&!a&&!f?g(ee,{w:"100%",h:"100%",display:"flex",flexDirection:"column",alignItems:"center",justifyContent:"center",spacing:8,children:[t(ee,{w:"40%",spacing:4,maxW:"360px",maxH:"400px",children:t(Pt,{src:io,objectFit:"contain"})}),c&&t(k,{h:10,px:2,bg:"red.400",rounded:"lg",as:"button",onClick:h,fontSize:14,color:"white",fontWeight:500,children:"G\u1EEDi l\u1EDDi ch\xE0o"})]}):t(ks,{in:!P,initialScale:.95,children:t(Vc,{hasBeforeLoadMoreButton:(!_&&ne||de)&&(w==null?void 0:w.length)>0,hasAfterLoadMoreButton:(!j&&te||J)&&(w==null?void 0:w.length)>0,onLoadBeforeMessage:Z,onLoadAfterMessage:ge,isLoadMore:W,searchLoading:x,loading:P,isGroup:o,ownerId:n,messages:O,conversationId:e,participants:r,userId:a,setConversationId:l,channel:i})})}))};E`
  mutation updateMedia($id: ObjectID!, $input: UpdateMediaInput!)
  @api(name: "zim") {
    updateMedia(id: $id, input: $input) {
      id
      title
    }
  }
`;E`
  mutation deleteMedia($id: ObjectID!) @api(name: "zim") {
    deleteMedia(id: $id)
  }
`;E`
  mutation uploadImages($file: [Upload]) @api(name: "appZim") {
    uploadImages(file: $file)
  }
`;const Yc=E`
  mutation singleUpload($file: Upload!) @api(name: "chat") {
    singleUpload(file: $file) {
      id
      path
      fullUrl
      type
    }
  }
`,Kc=({setMediaUrls:e})=>{const[n,{loading:a}]=ce(Yc),s=m.exports.useCallback(async o=>{var i,c,f;const l=ya();try{await e.push({fileId:l,url:URL.createObjectURL(o),name:o.name,loading:!0,progress:0,type:((i=o==null?void 0:o.type)==null?void 0:i.includes("video"))?"video":((c=o==null?void 0:o.type)==null?void 0:c.includes("image"))?"image":"file"});const{data:p}=await n({variables:{file:o},context:{fetchOptions:{onUploadProgress:d=>{var $,y;e.update(u=>u.fileId===l,{fileId:l,url:URL.createObjectURL(o),name:o.name,loading:!0,progress:d.loaded/d.total,type:(($=o==null?void 0:o.type)==null?void 0:$.includes("video"))?"video":((y=o==null?void 0:o.type)==null?void 0:y.includes("image"))?"image":"file"})}}}});await e.update(d=>d.fileId===l,{fileId:p.singleUpload.id,name:p.singleUpload.path,url:URL.createObjectURL(o),type:o.type.includes("video")?"video":((f=o==null?void 0:o.type)==null?void 0:f.includes("image"))?"image":"file"})}catch(p){console.log(p)}},[n]);return{doUploadImage:m.exports.useCallback(async o=>{await Promise.all(o.map(s))},[s]),loading:a}},Xc=Ce(()=>$e(()=>import("./index18.js").then(function(e){return e.i}),["assets/index18.js","assets/vendor.js"])),Jc=1048576*2,ed=({conversationId:e,setConversationId:n,userId:a,group:s,textAreaRef:r,file:o,setFile:l,conversationDetail:i,isMyTyping:c})=>{var f,p,d,$;{const y=Te(),[u]=re(),h=m.exports.useRef(null),x=m.exports.useRef(null),S=T("gray.900","slate.300"),[w,I]=m.exports.useState(!1),[v,R]=m.exports.useState(!1),[_,j]=m.exports.useState(""),[C,P]=m.exports.useState(!1),{onSendMessage:L,onTyping:H}=En({setContent:j,userId:a,group:s,setConversationId:n}),W=((p=(f=i==null?void 0:i.usersTyping)==null?void 0:f.filter(z=>z!==(u==null?void 0:u.id)))==null?void 0:p.length)>0,G=($=(d=i==null?void 0:i.usersTyping)==null?void 0:d.filter(z=>z!==u.id))==null?void 0:$.map(z=>{var O;return(O=i==null?void 0:i.participants.find(Z=>Z.userId===z))==null?void 0:O.fullName}),{doUploadImage:ne}=Kc({setMediaUrls:l}),{getRootProps:te,getInputProps:J}=vs({accept:["image/*","application/vnd.ms-excel","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet","application/vnd.openxmlformats-officedocument.presentationml.presentation","application/pdf","application/xhtml+xml","application/xml","application/msword","application/vnd.openxmlformats-officedocument.wordprocessingml.document","application/zip"],multiple:!1,onDrop:(z,O)=>{var ye;const Z=(O==null?void 0:O.length)>0,ge=O.length>0&&((ye=O[0].errors)==null?void 0:ye.find(Oe=>Oe.code==="file-too-large"));Z?y({title:"Th\u1EA5t b\u1EA1i!",description:ge?zd:jd,status:"error",duration:3e3,isClosable:!0}):ne(z).then()},minSize:0,maxSize:Jc}),de=m.exports.useCallback(async z=>{const O=z.target.value.trim();if(z.key==="Enter"&&!z.shiftKey)(O.length!==0||(o==null?void 0:o.length)>0)&&(C&&P(!1),await L({nativeEvent:{text:O},conversationId:e}),P(!1),j(""));else return!1},[e,_,C,o,r]),se=()=>v&&t(sn,{_focus:{outline:"none"},onFocus:()=>{I(!0)},onBlur:()=>{I(!1)},as:ln,style:{resize:"none"},ref:r,minRows:1,maxRows:5,rows:1,value:_,onChange:async z=>{z.target.value!==`
`&&await j(z.target.value)},border:"none",placeholder:"Nh\u1EADp n\u1ED9i dung ...",onKeyPress:de,color:S,bg:"transparent",mr:4});return ws(()=>{v&&async function(){w&&await H("typingOn",e),w||await H("typingOff",e)}()},[w,e,H,v,r,x]),m.exports.useEffect(()=>{if(c){const z=setTimeout(()=>{I(!1)},15e3);clearTimeout(z)}},[c]),m.exports.useEffect(()=>{R(!0)},[]),m.exports.useEffect(()=>{const z=O=>{h.current&&!h.current.contains(O.target)&&P(!1)};return document.addEventListener("click",z,!0),()=>{document.removeEventListener("click",z,!0)}},[P]),g(k,{bg:T("white",""),position:"sticky",bottom:0,w:"100%",display:"flex",flexDirection:"column",borderTopWidth:1,borderColor:"var(--chakra-colors-whiteAlpha-300)",p:2,children:[W&&g(D,{w:"50%",px:2,rounded:4,mb:2,children:[t(N,{fontSize:13,color:T("gray.700","white"),children:G==null?void 0:G.map(z=>z||"Someone is Typing").join(",")}),t(k,{display:"flex",alignItems:"center",justifyContent:"center",w:10,h:5,children:t(oo,{})})]}),g(D,{w:"full",children:[g(k,A(b({as:"div"},te({className:"dropzone"})),{children:[t("input",A(b({},J()),{style:{display:"none"},type:"file"})),t(Is,{color:T("#DA0037","white"),className:"w-5 h-5"})]})),g(k,{w:"100%",display:"flex",flexDirection:"row",borderWidth:1,rounded:"lg",borderColor:T("gray.300","var(--chakra-colors-whiteAlpha-300)"),p:2,children:[se(),g(D,{display:"flex",alignItems:"center",children:[t(k,{as:"button",onClick:()=>P(!C),children:t(Ts,{color:T("#DA0037","white"),className:"w-5 h-5 "})}),t(k,{cursor:"pointer",pointerEvents:!_&&(o==null?void 0:o.length)===0?"none":"auto",onClick:async()=>{_.trim().length!==0||(o==null?void 0:o.length)>0?(C&&P(!1),await L({nativeEvent:{text:_},conversationId:e})):j("")},children:t(_a,{fill:T((_==null?void 0:_.length)>0||(o==null?void 0:o.length)>0?"#DA0037":"#686868","white"),className:"w-5 h-5"})})]})]}),C&&t(k,{ref:h,color:"slate.900",children:t(Xc,{disableSkinTonePicker:!0,pickerStyle:{position:"absolute",bottom:"70px",right:"20px",overflow:"hidden",backgroundColor:"white",zIndex:"1000"},onEmojiClick:(z,O)=>{j(`${_} ${O.emoji}`)}})})]})]})}},td=e=>{const n=Te(),[a,{loading:s}]=ce(vl);return{onMarkDone:m.exports.useCallback(async()=>{try{await a({variables:{conversationId:e}}),await n({title:"Th\xE0nh c\xF4ng.",description:"\u0110\xE3 chuy\u1EC3n user sang tr\u1EA1ng th\xE1i done",status:"success",duration:2e3,isClosable:!0})}catch(o){n({title:"L\u1ED7i !!!",description:o.message,status:"success",duration:2e3,isClosable:!0})}},[e,a,n]),loading:s}},Ao=e=>{const o=e,{Icon:n,onClick:a,disabled:s}=o,r=Re(o,["Icon","onClick","disabled"]);return t(k,A(b({_hover:{backgroundColor:"#0085E4"},as:"button",display:"flex",alignItems:"center",justifyContent:"center",width:10,rounded:"4px",h:"100%",onClick:a,disabled:s},r),{children:t(n,{})}))},nd=({userInfo:e,groupInfo:n,loading:a,participants:s})=>{var j;const[r]=re(),[o,l]=m.exports.useState(!1),[i,c]=m.exports.useState(""),f=F(C=>C.searchStringMessage),p=F(C=>C.setSearchStringMessage),d=F(C=>C.visibleUserProfile),$=F(C=>C.conversationId),y=F(C=>C.setSearchResult),u=F(C=>C.setNearSearchResult),h=F(C=>C.setVisibleUserProfile),x=F(C=>C.setBeforeId),S=F(C=>C.setAfterId),w=F(C=>C.setMessageId),I=ft({base:"",xl:"visible"}),v=ft({base:"",md:"visible"}),{onMarkDone:R,loading:_}=td($);return So({conversationId:$,search:i}),Oa(()=>{c(f)},1e3,[f]),g(k,{w:"100%",minH:"35px",maxH:"50px",display:"flex",flexDirection:"row",alignItems:"center",borderBottomWidth:1,pl:2,py:3,zIndex:1,children:[!o&&g(D,{id:"js-toggle-profile",flex:1,alignItems:"center",cursor:"pointer",onClick:()=>!I&&h(!d),children:[e&&We.exports.isEmpty(n)?t(Se,{boxSize:"30px",src:(e==null?void 0:e.avatar)===ze?"":e==null?void 0:e.avatar}):t(ro,{groupInfo:n==null?void 0:n.participants}),t(k,{flex:1,maxW:"300px",children:t(N,{noOfLines:1,fontSize:14,lineHeight:"24px",fontWeight:"medium",color:T("gray.800","white"),textTransform:"capitalize",children:a?"\u0110ang t\u1EA3i ...":(n==null?void 0:n.name)?n==null?void 0:n.name:(e==null?void 0:e.full_name)||(e==null?void 0:e.fullName)||""})})]}),g(D,{w:o?"full":"",h:"100%",pr:o?2:4,spacing:0,minH:10,children:[[6].includes(parseInt(r==null?void 0:r.roleId,10))&&(s==null?void 0:s.find(C=>C.userId===r.id.toString()))&&!o&&((e==null?void 0:e.isGuest)||((j=n==null?void 0:n.participants)==null?void 0:j.find(C=>C.isGuest)))&&t(Ao,{onClick:R,disabled:_,Icon:Ps}),!o&&v&&t(Ao,{onClick:()=>{l(!0)},Icon:_s}),o&&g(ua,{children:[t(vt,{type:"text",placeholder:"Nh\u1EADp g\xEC \u0111\xF3 ...",value:f,onChange:C=>p(C.target.value),pr:"4.5rem"}),t(pa,{width:"4.5rem",children:t(Q,{h:"1.75rem",size:"sm",onClick:()=>{l(!1),p(""),y([]),u([]),w(""),x(""),S("")},children:o?"Close":"Open"})})]})]})]})},ko=(e,n)=>{var l;const{data:a,loading:s,error:r}=he(zi,{variables:pe({id:e,userId:e?"":n?n==null?void 0:n.toString():""},i=>i),skip:!e&&!n});return{conversationDetail:(l=a==null?void 0:a.conversationDetail)!=null?l:{},loading:s,error:r}},vo=(e="",n)=>{var o;const{data:a,loading:s}=he(Va,{variables:{userId:Number(e)},skip:n||!e||isNaN(Number(e))});return{userData:(o=a==null?void 0:a.getUserById)!=null?o:{},loading:s}},wo=({conversationDetail:e})=>{var a,s,r,o;const[n]=re();return!((a=e==null?void 0:e.participants)==null?void 0:a.find(l=>l.userId===(n==null?void 0:n.id)))&&((s=e==null?void 0:e.participants)==null?void 0:s.length)>=2||((r=e==null?void 0:e.participants)==null?void 0:r.find(l=>l.userId===(n==null?void 0:n.id)))&&((o=e==null?void 0:e.participants)==null?void 0:o.length)>=3},ad=(e=[],n)=>{if((e==null?void 0:e.length)<2)return!0;if((e==null?void 0:e.length)>=2)return!!(e==null?void 0:e.find(a=>a.userId===n))},od=()=>{var v,R,_,j;const e=F(C=>C.channel),n=F(C=>C.userId),a=F(C=>C.conversationId),s=F(C=>C.setConversationId),r=F(C=>C.searchStringMessage),o=Ue(),l=m.exports.useRef(null),[i]=re(),[c,f]=Os([]),[p,d]=m.exports.useState(!1),{userData:$,loading:y}=vo(n,!n),{conversationDetail:u,loading:h}=ko(a,n),x=((v=u==null?void 0:u.participants)==null?void 0:v.filter(C=>(C==null?void 0:C.userId)!==(i==null?void 0:i.id.toString()))[0])||$,S=wo({conversationDetail:u}),{onSendMessage:w,loading:I}=En({setContent:()=>{},userId:n,group:e,setConversationId:s});return m.exports.useEffect(()=>{(c==null?void 0:c.length)>0?d(!0):d(!1)},[c]),g(k,{bg:T("white","#10172a"),overflow:"auto",display:"flex",flexDirection:"column",flex:1,w:"full",position:"relative",h:"full",children:[(a||n)&&t(nd,{participants:u==null?void 0:u.participants,userInfo:x,groupInfo:S&&a?{name:(u==null?void 0:u.name)?u==null?void 0:u.name:S&&a?(_=(R=u==null?void 0:u.participants)==null?void 0:R.map(C=>C.fullName))==null?void 0:_.join(","):"",participants:u==null?void 0:u.participants}:null,loading:y||h}),t(Zc,{isGroup:u==null?void 0:u.name,ownerId:i==null?void 0:i.id,conversationId:a,userId:n,onSendMessage:w,sendMessageLoading:I,participants:u==null?void 0:u.participants,setConversationId:s,channel:e,isShowSayHi:a||n}),t(Fs,{appear:!0,show:p,as:"div",enter:"transition-opacity duration-75",enterFrom:"opacity-0",enterTo:"opacity-100",leave:"transition-opacity duration-150",leaveFrom:"opacity-100",leaveTo:"opacity-0",children:t(k,{p:2,zIndex:1e3,position:"absolute",bg:T("white","slate.600"),rounded:"lg",display:"flex",flexDirection:"column",justifyContent:"space-between",w:"50%",h:"40%",className:`translate-y-2/4 translate-x-[50%] inset-0 ${oc.boxShadow} `,children:c==null?void 0:c.map((C,P)=>g(k,{flexDirection:"column",display:"flex",justifyContent:"space-between",w:"full",h:"100%",overflow:"hidden",rounded:"lg",children:[(C==null?void 0:C.type)!=="file"?t(k,{maxH:"75%",children:t(Pt,{w:"full",h:"full",src:C==null?void 0:C.url,alt:"file",objectFit:"contain"})}):g(ee,{children:[t(k,{w:14,h:14,rounded:"full",display:"flex",alignItems:"center",justifyContent:"center",borderWidth:1,children:t(Ca,{className:"w-[40px] h-[40px]"})}),t(N,{textAlign:"center",flex:1,color:T("gray.800","white"),fontSize:14,children:Ht(C==null?void 0:C.name)})]}),t(k,{as:"button",onClick:()=>{f.removeAt(P),d(!1)},top:-2,right:-1,className:"cursor-pointer absolute z-20",children:t(Ls,{onClick:()=>{f.removeAt(P),d(!1)},width:32,className:"text-gray-400 hover:text-red-400"})}),g(Ae,{display:"flex",flexDirection:"column",height:"25%",justifyContent:"flex-end",children:[t(k,{h:"18%",w:"100%",children:t(Ns,{colorScheme:(C==null?void 0:C.progress)<1?"":"green",hasStripe:(C==null?void 0:C.progress)<1,mt:2,value:(C==null?void 0:C.progress)*100})}),t(Ms,{}),t(lc,{disabled:(C==null?void 0:C.progress)<1,onClick:async()=>{await w({nativeEvent:{text:""},conversationId:a,file:c}),o(["getAttachmentsInConversation"]),await f.clear()},buttonType:"info",className:"w-full text-center py-2 rounded-lg text-white mt-2 max-h-[40px]",children:"G\u1EEDi"})]})]},P))})}),(ad(u==null?void 0:u.participants,i==null?void 0:i.id)&&(a||n)||S&&(i==null?void 0:i.roleId)===6)&&!r&&t(ed,{conversationDetail:u,file:c,setFile:f,textAreaRef:l,conversationId:a,setConversationId:s,userId:n,group:e,isMyTyping:(j=u==null?void 0:u.usersTyping)==null?void 0:j.includes(i==null?void 0:i.id)}),(c==null?void 0:c.length)>0&&t(k,{opacity:.4,zIndex:10,position:"absolute",bg:T("black","slate.300"),className:"inset-0"})]})},rd=({participantAvatarUrl:e,fullName:n="",onClose:a,loading:s})=>g(k,{flex:1,display:"flex",alignItems:"center",cursor:"pointer",onClick:()=>a?a():()=>{},children:[t(Se,{size:"sm",src:e===ze?"":e}),t(N,{ml:2,flex:1,noOfLines:1,fontSize:14,lineHeight:"24px",fontWeight:"medium",color:T("gray.800","slate.300"),children:s?"\u0110ang t\u1EA3i ...":n||"\u1EA8n danh"})]}),sd=/^(?=.*\d)(?=.*[a-zA-Z])[\da-zA-Z_.\-@]{8,}$/,id=/^\d+$/,Io=/(84|03|05|07|08|09|01[2|6|8|9])+([0-9]{8})\b/,up=/^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w.-]+)+[\w\-._~:/?#[\]@!$&'()*+,;=.]+$/gm,ld=e=>{if(typeof e!="string")return"";const n=e.split(".");return n.length>0?n[n.length-1]:""};ct(cn,"password",function(e){return this.matches(sd,{message:e,excludeEmptyString:!0})});ct(cn,"onlyNumber",function(e){return this.matches(id,{message:e,excludeEmptyString:!0})});ct(cn,"phone",function(e){return this.matches(Io,{message:e,excludeEmptyString:!0})});ct(dn,"fileSize",function(e,n){return this.test("fileSize",n,a=>Array.isArray(a)&&a.length>0&&a[0]?!a.map(r=>r.size>e):!0)});ct(dn,"fileType",function(e=[],n){return this.test("fileType",n,a=>e.length===0?!0:Array.isArray(a)&&a.length>0&&a[0]?!a.map(r=>e.includes(r.type)):!0)});ct(dn,"file",function({size:e,type:n},a){return this.test({name:"file",test:function(s){var l,i,c,f,p,d;const r=(l=n==null?void 0:n.value)!=null?l:[],o=(i=e==null?void 0:e.value)!=null?i:5*1024*1e3;if(Array.isArray(s)&&s.length>0&&s[0]){const $=s.find(u=>!r.includes(u.type)),y=s.find(u=>u.size>o);return $?this.createError({message:`${(c=n==null?void 0:n.message)!=null?c:"Kh\xF4ng h\u1ED7 tr\u1EE3 \u0111\u1ECBnh d\u1EA1ng"} ${ld((f=$==null?void 0:$.name)!=null?f:"")}`,path:this.path}):y?this.createError({message:(d=(p=e==null?void 0:e.message)!=null?p:`Dung l\u01B0\u1EE3ng v\u01B0\u1EE3t qu\xE1 ${o/1024e3}`+(y==null?void 0:y.name))!=null?d:"",path:this.path}):!0}else return!0}})});const tt=E`
  fragment Ticket on TicketType {
    id
    refId
    title
    description
    createDate
    status
    createBy {
      id
      user_name
      full_name
      status
      avatar
      supporter
      role_id
      phone
      address
      supporter
    }
    receiver {
      id
      user_name
      full_name
      avatar
      role_id
      phone
      address
      supporter
    }
    reactions {
      id
      ticketId
      reaction
      count
    }
  }
`,To=E`
  mutation createTicket($input: TicketInputType) @api(name: "appZim") {
    createTicket(input: $input) {
      ...Ticket
    }
  }
  ${tt}
`,Po=E`
  mutation updateTicket($input: TicketInputType) @api(name: "appZim") {
    updateTicket(input: $input) {
      ...Ticket
    }
  }
  ${tt}
`,_o=E`
  mutation updateTicketStatus($input: TicketUpdateStatusType)
  @api(name: "appZim") {
    updateTicketStatus(input: $input) {
      ...Ticket
    }
  }
  ${tt}
`,cd=E`
  mutation createTicketReaction($input: TicketReactionInputType)
  @api(name: "appZim") {
    createTicketReaction(input: $input) {
      id
      ticketId
      reaction
      count
    }
  }
  ${tt}
`,dd=Pe.object().shape({title:Pe.string().required("Vui l\xF2ng nh\u1EADp ti\xEAu \u0111\u1EC1"),description:Pe.string().min(10,"T\u1ED1i thi\u1EC3u 10 k\xFD t\u1EF1").required("Vui l\xF2ng nh\u1EADp m\xF4 t\u1EA3"),receiver:Pe.mixed()}),ud=({enableReceiver:e,users:n,onInputChange:a})=>{var l;const{register:s,formState:{errors:r},resetField:o}=Pa();return g(Ee,{direction:"column",w:"full",justifyContent:"stretch",alignItems:"stretch",className:"reply-editor",gap:4,children:[t(Me,A(b({label:"Ti\xEAu \u0111\u1EC1",placeholder:"Nh\u1EADp..."},s("title")),{error:r==null?void 0:r.title})),e&&t(mt,{name:"receiver",render:({field:i})=>t(Gt,b({label:"Ng\u01B0\u1EDDi nh\u1EADn",options:n||[],getOptionLabel:c=>`${c.full_name} - ${c.phone}`,getOptionValue:c=>c.id+"",formatOptionLabel:c=>g(D,{children:[t(Se,{src:c.avatar,objectFit:"cover",boxSize:6,flexShrink:0}),t(N,{children:c.full_name})]}),error:r==null?void 0:r.receiver,onInputChange:a},i))}),t(k,{children:g(kt,{isInvalid:r==null?void 0:r.description,children:[t(mt,{name:"description",render:({field:i})=>t(St,b({showWordCount:!1,placeholder:"N\u1ED9i dung ticket..."},i))}),t(wt,{children:(l=r==null?void 0:r.description)==null?void 0:l.message})]})})]})},pd=({onClose:e,isOpen:n,submitCallback:a=null})=>{const s=m.exports.useRef(),r=Dt({resolver:Fa(dd),mode:"onBlur",reValidateMode:"onChange"}),o=Te();$o();const[l,i]=Qe(),c=Ue(),[f,p]=m.exports.useState(""),[d,$]=m.exports.useState(f);Oa(()=>{p(d)},500,[d]);const[y]=re(),u=y.roleId===6,[h,{loading:x}]=ce(To,{onCompleted:v=>{const{createTicket:R}=v;R&&o({title:"Th\xE0nh c\xF4ng!",description:"Ticket \u0111\xE3 \u0111\u01B0\u1EE3c g\u1EEDi.",status:"success",duration:3e3,isClosable:!0}),e(),a&&typeof a=="function"&&a(),c(["getTicketsPagination"])},onError:v=>{var R;o({title:"G\u1EEDi kh\xF4ng th\xE0nh c\xF4ng!",description:`${(R=v==null?void 0:v.message)!=null?R:"L\u1ED7i"}`,status:"error",duration:3e3,isClosable:!0})},notifyOnNetworkStatusChange:!0}),{data:S}=he(Di(["avatar","full_name","phone"]),{variables:{input:{limit:100,q:f}},skip:!u}),w=S==null?void 0:S.getListStudentByEc.docs,I=m.exports.useCallback(async v=>{var _;if(u&&!(v==null?void 0:v.receiver)){r.setError("receiver",{type:"manual",message:"Vui l\xF2ng ch\u1ECDn ng\u01B0\u1EDDi nh\u1EADn"});return}const R={refId:jt,title:v==null?void 0:v.title,description:v==null?void 0:v.description,receiverId:u?parseInt((_=v==null?void 0:v.receiver)==null?void 0:_.id):null};u||delete R.receiverId,await h({variables:{input:R}})},[u]);return m.exports.useEffect(()=>{if(n){const v=l.get("receiverId");if(!v||!(w==null?void 0:w.length))return;const R=w.find(_=>_.id===Number(v));r.reset({title:"",description:"",receiver:R||null})}else l.delete("receiverId"),i(l),r.reset({title:"",description:"",receiver:null})},[n,l,w]),t(on,A(b({},r),{children:g(rt,{finalFocusRef:s,isOpen:n,onClose:e,size:"3xl",children:[t(Ye,{}),g(st,{children:[t(Ke,{children:"T\u1EA1o ticket"}),t(it,{}),g("form",{onSubmit:r.handleSubmit(I),children:[t(lt,{children:t(ud,{enableReceiver:u,users:w,onInputChange:$})}),g(gt,{children:[t(Q,{leftIcon:t(we,{as:rn}),colorScheme:"green",mr:4,type:"submit",isLoading:x,children:"G\u1EEDi ticket"}),t(Q,{variant:"ghost",onClick:e,children:"H\u1EE7y"})]})]})]})]})}))};function hd({color:e,onClick:n,isActive:a}){return t(k,{borderWidth:a?3:1,borderColor:a?"white":"",as:"button",onClick:n,rounded:"sm",w:4,h:4,bg:`${e}`})}function gd({firstFieldRef:e,onClose:n}){var S,w;const a=Te(),[s,r]=m.exports.useState("1"),[o,l]=m.exports.useState(null),i=F(I=>I.conversationId),[c,f]=m.exports.useState("#00DAA3"),[p,d]=m.exports.useState(""),{data:$}=he(Ui,{skip:s==="2"}),[y,{loading:u}]=ce(kl),h=m.exports.useCallback(()=>{f("#00DAA3"),d(""),l(null)},[]),x=m.exports.useCallback(async()=>{var R;const I={conversationId:i,tagId:o==null?void 0:o.id},v={conversationId:i,name:p,color:c};try{await y({variables:(o==null?void 0:o.id)?I:v}),n(),h(),a({title:"Th\xE0nh c\xF4ng!",description:"G\u1EAFn tag th\xE0nh c\xF4ng",status:"success",duration:3e3,isClosable:!0})}catch(_){a({title:"G\u1EAFn tag kh\xF4ng th\xE0nh c\xF4ng!",description:`${(R=_==null?void 0:_.message)!=null?R:"L\u1ED7i"}`,status:"error",duration:3e3,isClosable:!0})}},[p,c,i,o,h]);return g(Ae,{spacing:4,children:[t(La,{defaultValue:"1",children:g(D,{spacing:5,align:"center",children:[t(Na,{isChecked:s==="1",colorScheme:"red",value:"1",onChange:()=>{r("1"),d(""),f("#00DAA3")},children:"Tag c\xF3 s\u1EB5n"}),t(Na,{isChecked:s==="2",colorScheme:"green",value:"2",onChange:()=>{r("2"),l(null)},children:"Th\xEAm tag m\u1EDBi"})]})}),s==="1"&&t(Gt,{value:o,label:"Tags",onChange:l,options:(w=(S=$==null?void 0:$.getListTag)==null?void 0:S.docs)!=null?w:[],getOptionLabel:I=>I.name,getOptionValue:I=>I.id+"",formatOptionLabel:I=>g(D,{children:[t(k,{bg:I.color,w:"5px",h:"5px",rounded:"full",mr:1}),t(N,{children:I.name})]})}),s==="2"&&g(Ae,{spacing:4,children:[t(La,{onChange:f,value:c,children:t(D,{spacing:3,children:qd.map((I,v)=>{const R=I.color===c;return t(hd,{color:I.color,onClick:()=>f(I.color),isActive:R},I.color)})})}),t(Me,{label:"Tag name",id:"tag-name",ref:e,placeholder:"Nh\u1EADp t\xEAn tag",value:p,onChange:I=>d(I.target.value)})]}),g(Rs,{d:"flex",justifyContent:"flex-end",children:[t(Q,{onClick:()=>{n(),h()},variant:"outline",children:"Cancel"}),t(Q,{onClick:x,disabled:!p&&!o||u,colorScheme:"teal",children:"Save"})]})]})}const md=({item:e})=>g(D,{children:[t(k,{w:6,h:6,p:1,rounded:"full",bg:T("gray.100","#1e293b"),overflow:"hidden",display:"flex",alignItems:"center",justifyContent:"center",children:e.icon}),typeof e.value=="function"?e==null?void 0:e.value():t(N,{flex:1,fontSize:14,color:T("gray.800","slate.300"),noOfLines:2,children:e.value||"Kh\xF4ng c\xF3 th\xF4ng tin"})]}),fd=({participantInfo:e,participantAvatarUrl:n,fullName:a,loading:s,isStudent:r,tags:o,participants:l})=>{const{isOpen:i,onClose:c,onOpen:f}=qe(),p=at.useRef(null),[d]=re(),$=$n.map(S=>S.id),y=F(S=>S.setVisibleUserProfile),u=F(S=>S.conversationId),h=Bd(e,o),x=ft({sm:"visible",xl:""});return!r&&g(ee,{alignItems:"stretch",spacing:2.5,px:4,py:2,borderBottomWidth:1,children:[g(Ae,{spacing:2.5,children:[g(D,{justifyContent:"space-between",children:[t(rd,{onClose:()=>y(!1),participantAvatarUrl:n,fullName:a,loading:s}),x&&t(k,{onClick:()=>y(!1),display:"flex",cursor:"pointer",w:6,h:6,alignItems:"center",justifyContent:"center",children:t(Ds,{className:"w-full h-full"})})]}),h.map((S,w)=>t(md,{item:S},`${S.value}-${w}`))]}),(d==null?void 0:d.roleId)!==5&&u&&(l==null?void 0:l.find(S=>S.userId===(d==null?void 0:d.id.toString())))&&g(D,{spacing:10,children:[t(Nt,{children:({onClose:S})=>g(Ie,{children:[t(Mt,{children:t(Q,{bg:"transparent",_hover:{backgroundColor:"transparent"},_focus:{backgroundColor:"transparent"},_active:{backgroundColor:"transparent"},children:$.includes(d==null?void 0:d.roleId)&&t(Oo,{iconContainerClassName:"bg-blue-500",label:"G\u1EAFn tag"})})}),g(Rt,{children:[t(Ma,{}),t(Bs,{children:"G\u1EAFn tag"}),g(nn,{children:[t(Ma,{}),t(gd,{firstFieldRef:p,onClose:S})]})]})]})}),(e==null?void 0:e.role_name)==="H\u1ECDc vi\xEAn"&&$.includes(d==null?void 0:d.roleId)&&u&&t(Oo,{onClick:f,label:"T\u1EA1o ticket",iconContainerClassName:"bg-red-500"})]}),t(pd,{onClose:c,isOpen:i})]})},Oo=({label:e="T\u1EA1o ticket",onClick:n,iconContainerClassName:a})=>g(D,{onClick:n,display:"flex",alignItems:"center",cursor:"pointer",children:[t(k,{className:a,w:5,h:5,rounded:"full",display:"flex",alignItems:"center",justifyContent:"center",children:t(zs,{color:"white"})}),t(N,{fontWeight:"400",color:T("gray.800","slate.300"),children:e})]}),xd=({createdAt:e,content:n,owner:a})=>{var s;return g(D,{display:"flex",alignItems:"start",children:[t(Se,{size:"sm",src:a==null?void 0:a.avatar}),g(ee,{flex:1,alignItems:"stretch",spacing:1,children:[g(D,{spacing:3,justifyContent:{base:"start","2xl":"space-between"},children:[t(N,{noOfLines:1,fontSize:14,lineHeight:"24px",fontWeight:"medium",color:T("gray.800","slate.400"),maxW:{base:"90px","2xl":"120px"},children:(s=a==null?void 0:a.fullName)!=null?s:"Kh\xF4ng c\xF3"}),t(N,{fontSize:12,lineHeight:"24px",className:"text-gray-400",children:ve(e).format("DD-MM-YYYY, HH:mm")})]}),t(N,{fontSize:12,lineHeight:"16px",color:T("gray.800","slate.200"),children:n})]})]})},bd=({conversationId:e})=>{const n=m.exports.useRef(null),a=Ue(),[s,r]=m.exports.useState(!1),[o,l]=m.exports.useState(""),[i]=ce(Sl),c=m.exports.useCallback(async()=>{try{await i({variables:{conversationId:e,content:o}}),await a(["notes"])}catch(p){console.log(p)}},[e,o]),f=m.exports.useCallback(async p=>{const d=p.target.value.trim();if(p.key==="Enter"&&!p.shiftKey)d.length!==0&&(c().then(),l(""));else return!1},[c]);return m.exports.useEffect(()=>{r(!0)},[]),g(k,{w:"100%",display:"flex",flexDirection:"column",borderWidth:1,rounded:"lg",borderColor:T("gray.300","var(--chakra-colors-whiteAlpha-300)"),p:2,children:[s&&t(sn,{_focus:{outline:"none"},as:ln,style:{resize:"none"},ref:n,minRows:1,maxRows:5,rows:1,value:o,onChange:p=>{p.target.value!==`
`&&l(p.target.value)},border:"none",placeholder:"Nh\u1EADp n\u1ED9i dung ghi ch\xFA ...",onKeyPress:f,color:T("gray.900","slate.300"),bg:"transparent"}),g(D,{justifyContent:"space-between",children:[t(D,{spacing:3,children:t(k,{onClick:()=>l(o.concat("@")),as:"button",className:"flex items-center"})}),t(k,{as:"button",disabled:!o,onClick:async()=>{o.trim().length!==0&&(c(),l(""))},children:t(_a,{fill:T((o==null?void 0:o.length)>0?"#DA0037":"#686868","white"),className:"w-5 h-5"})})]})]})},yd=({conversationId:e,userId:n})=>{var p,d,$;const{data:a,loading:s,fetchMore:r}=he(ji,{variables:pe({conversationId:e||"",userId:e?"":n?n==null?void 0:n.toString():"",limit:5},y=>y),skip:!e}),o=(p=a==null?void 0:a.notes)==null?void 0:p.docs,l=(d=a==null?void 0:a.notes)==null?void 0:d.page,i=($=a==null?void 0:a.notes)==null?void 0:$.hasNextPage,{onLoadMore:c,isLoadingMore:f}=ut({variables:{conversationId:e,limit:5,page:l+1},fetchMore:r,hasNextPage:i});return{notes:o,loading:s,onLoadMore:c,isLoadingMore:f,hasNextPage:i}},Ed=({conversationId:e,isSuperAdmin:n,allowForSupperAdmin:a,userId:s})=>{const{notes:r,onLoadMore:o,hasNextPage:l,isLoadingMore:i}=yd({conversationId:e,userId:s});return g(ee,{alignItems:"stretch",spacing:2.5,px:4,py:2,borderBottomWidth:1,children:[g(N,{fontSize:14,color:T("gray.800","slate.300"),children:["Notes ",r==null?void 0:r.length]}),t(ee,{alignItems:"stretch",spacing:2.5,maxH:"200px",overflow:"auto",children:r==null?void 0:r.map(c=>t(xd,b({},c),c.id))}),l&&t(Q,{h:"32px",isLoading:i,loadingText:"\u0110ang t\u1EA3i th\xEAm",onClick:o,w:"100%",rounded:"none",borderWidth:"1px 0px 0px 0px",bg:T("white","slate.600"),fontSize:14,fontWeight:400,justifyContent:"center",children:"Xem th\xEAm"}),(!n||a)&&t(bd,{conversationId:e})]})},Cd=({conversationId:e,userId:n})=>{var d,$,y;const[a,s]=m.exports.useState(!1),{data:r,loading:o,fetchMore:l}=he(qi,{variables:A(b({},pe({conversationId:e||"",userId:e?"":n?n==null?void 0:n.toString():""},u=>u)),{offset:0})}),i=($=(d=r==null?void 0:r.getAttachmentsInConversation)==null?void 0:d.docs)==null?void 0:$.map(u=>u.attachments),c=[].concat.apply([],i),f=(y=r==null?void 0:r.getAttachmentsInConversation)==null?void 0:y.hasNextPage,p=m.exports.useCallback(async()=>{var u;!f||a||(await s(!0),await l({variables:{conversationId:e,offset:((u=r==null?void 0:r.getAttachmentsInConversation)==null?void 0:u.docs.length)+1},updateQuery:(h,{fetchMoreResult:x})=>x?bn(h,x):h}),await s(!1))},[r,l,a,e]);return{attachments:c,loading:o,onLoadMore:p,isLoadingMore:a,hasNextPage:f}},$d=({conversationId:e,userId:n})=>{const[a,s]=m.exports.useState(null),[r,o]=m.exports.useState(!1),{attachments:l,isLoadingMore:i,onLoadMore:c,hasNextPage:f}=Cd({conversationId:e,userId:n});return g(ee,{alignItems:"stretch",spacing:2.5,px:4,py:2,borderBottomWidth:1,children:[g(N,{fontSize:14,color:T("gray.800","slate.300"),children:["File \u0111\xEDnh k\xE8m ",l==null?void 0:l.length]}),l==null?void 0:l.map((p,d)=>{var $,y,u;return g(D,{spacing:3,children:[t(js,{className:"w-5 h-5 transform rotate-[30deg]"}),(p==null?void 0:p.type)==="image"&&t(N,{onClick:()=>{s(d),o(!0)},flex:1,noOfLines:2,fontSize:14,lineHeight:"24px",color:T("gray.800","slate.300"),children:Ht(($=p==null?void 0:p.attachment)==null?void 0:$.path)}),(p==null?void 0:p.type)==="file"&&t(Ve,{target:"_blank",href:(y=p==null?void 0:p.attachment)==null?void 0:y.fullUrl,children:t(N,{flex:1,noOfLines:2,fontSize:14,lineHeight:"24px",color:T("gray.800","slate.300"),children:Ht((u=p==null?void 0:p.attachment)==null?void 0:u.path)})})]},d)}),t(ho,{attachments:l,attachmentIndex:a,setAttachmentIndex:s,showImageViewer:r,setShowImageViewer:o}),f&&t(Q,{isLoading:i,loadingText:"\u0110ang t\u1EA3i th\xEAm",onClick:c,w:"100%",rounded:"none",borderWidth:"1px 0px 0px 0px",bg:T("white","slate.600"),fontSize:14,fontWeight:400,justifyContent:"center",children:"Xem th\xEAm"})]})},Sd=({isOpen:e,onClose:n,userInfo:a,isOwner:s,groupId:r,isSupperAdmin:o,visibleRemoveMemberButton:l})=>{var h;const i=Ue(),{isOpen:c,onOpen:f,onClose:p}=qe(),d=at.useRef(),[$,{loading:y}]=ce(Al),u=m.exports.useCallback(async()=>{var x;try{await $({variables:{userId:(x=a==null?void 0:a.userId)==null?void 0:x.toString(),id:r}}),i(["conversationDetail"]),p(),n()}catch(S){console.log(S)}},[a,r]);return g(Ie,{children:[g(rt,{isOpen:e,onClose:n,children:[t(Ye,{}),g(st,{children:[t(Ke,{textAlign:"center",children:"Th\xF4ng tin th\xE0nh vi\xEAn"}),t(it,{}),g(lt,{pb:4,children:[t(ee,{p:4,pt:0,children:t(Se,{size:"2xl",src:a.avatar===ze?"":a.avatar})}),t(ee,{alignItems:"stretch",children:g(D,{justifyContent:s||o?"space-between":"center",alignItems:"center",children:[g(N,{textAlign:s||o?"left":"center",fontSize:"xl",children:[a.fullName," ",`(${(h=mn.find(x=>x.id===Number(a==null?void 0:a.roleId)))==null?void 0:h.name})`]}),l&&t(Q,{onClick:f,size:"sm",colorScheme:"red",variant:"solid",fontSize:13,textColor:"white",children:"M\u1EDDi r\u1EDDi nh\xF3m"})]})})]})]})]}),t(qs,{isOpen:c,leastDestructiveRef:d,onClose:p,children:t(Ye,{children:g(Us,{children:[t(Ke,{fontSize:"lg",fontWeight:"bold",children:"C\u1EA3nh b\xE1o !!!"}),t(lt,{children:"B\u1EA1n c\xF3 ch\u1EAFc kh\xF4ng? B\u1EA1n kh\xF4ng th\u1EC3 ho\xE0n t\xE1c h\xE0nh \u0111\u1ED9ng n\xE0y sau khi x\xF3a."}),g(gt,{children:[t(Q,{disabled:y,ref:d,onClick:p,children:"Tr\u1EDF l\u1EA1i"}),t(Q,{disabled:y,colorScheme:"red",onClick:u,ml:3,color:"white",loadingText:"\u0110ang x\u1EED l\xFD",children:"X\xE1c nh\u1EADn"})]})]})})})]})},Ad=({conversationId:e,conversationDetail:n})=>{var d,$,y,u,h;const[a]=re(),{isOpen:s,onClose:r,onOpen:o}=qe(),{isOpen:l,onClose:i,onOpen:c}=qe(),[f,p]=m.exports.useState(null);return g(ee,{alignItems:"stretch",spacing:2.5,px:4,py:2,borderBottomWidth:1,children:[g(D,{justifyContent:"space-between",alignItems:"center",children:[t(N,{fontSize:14,color:T("gray.800","slate.300"),children:"Th\xE0nh vi\xEAn"}),((d=n==null?void 0:n.owner)==null?void 0:d.userId)===(a==null?void 0:a.id)&&t(k,{onClick:o,as:"button",w:6,h:6,display:"flex",alignItems:"center",justifyContent:"center",children:t(Hs,{className:"w-full h-full"})})]}),t(ee,{alignItems:"stretch",spacing:0,maxH:"400px",overflow:"auto",children:($=n==null?void 0:n.participants)==null?void 0:$.map(x=>{var w,I;const S=((w=n==null?void 0:n.owner)==null?void 0:w.userId)===(x==null?void 0:x.userId);return t(Ct,A(b({},x),{isOwner:S,lastOnline:x==null?void 0:x.online,avatarUrl:x==null?void 0:x.avatar,username:x==null?void 0:x.fullName,positionName:(I=mn.find(v=>v.id===Number(x==null?void 0:x.roleId)))==null?void 0:I.name,onClick:async()=>{await p(x),await c()},px:0}),x.userId)})}),t(Sn,{isAddSupporter:!1,isEdit:!0,isOpen:s,onClose:r,listParticipants:(y=n==null?void 0:n.participants)==null?void 0:y.map(x=>x==null?void 0:x.userId),name:n==null?void 0:n.name,groupId:e}),l&&f&&t(Sd,{visibleRemoveMemberButton:(a==null?void 0:a.roleId)===1||a.id===((u=n==null?void 0:n.owner)==null?void 0:u.userId),isSupperAdmin:(a==null?void 0:a.roleId)===1,groupId:n==null?void 0:n.id,isOwner:((h=n==null?void 0:n.owner)==null?void 0:h.userId)===(f==null?void 0:f.userId),userInfo:f!=null?f:{},isOpen:l,onClose:i})]})},Cn={ongoing:{backgroundColor:"green.500"},incoming:{backgroundColor:"orange.400"},finished:{backgroundColor:"gray.300"}},kd=({status:e,title:n})=>g(D,{children:[t(k,{w:2.5,h:2.5,rounded:"full",bg:Cn==null?void 0:Cn[e].backgroundColor}),t(N,{flex:1,lineHeight:"20px",fontSize:14,color:T("#444","slate.300"),children:n})]}),vd=({data:e})=>g(ee,{alignItems:"stretch",spacing:2.5,px:4,py:2,borderBottomWidth:1,maxH:"200px",children:[g(N,{fontSize:14,color:T("gray.800","slate.300"),children:["Kh\xF3a h\u1ECDc ",e.length]}),e.map((n,a)=>{var o;const{status:s,name:r}=(o=n==null?void 0:n.course)!=null?o:{};return t(kd,A(b({},n),{status:s,title:r}),a)})]}),Fo={create:[6],changeEC:[12,1,2],makeApointment:[6]},wd=at.forwardRef((e,n)=>{var a;return g(kt,{isInvalid:!!(e==null?void 0:e.error),width:"100%",children:[(e==null?void 0:e.label)&&t(Yt,{htmlFor:e.id,children:e==null?void 0:e.label}),t(sn,b({minH:"unset",overflow:"hidden",w:"100%",resize:"none",ref:n,minRows:1,as:ln,transition:"height none",bg:T("white","slate.700")},e)),(e==null?void 0:e.error)&&t(wt,{children:(a=e==null?void 0:e.error)==null?void 0:a.message})]})}),Id=(e={},n={})=>{const a=Te();return he(Vi,b({variables:e,onError:s=>{a({title:"L\u1EA5y d\u1EEF li\u1EC7u th\u1EA5t b\u1EA1i.",description:`GET list source status error: ${s==null?void 0:s.message}`,status:"error",duration:3e3,isClosable:!0})},fetchPolicy:"cache-first",errorPolicy:"all"},n))},pp=E`
  mutation saveNote($rawId: Int, $note: String) @api(name: "appZim") {
    saveNote(rawId: $rawId, note: $note) {
      note
      createBy
      createDate
    }
  }
`,hp=E`
  mutation updateStatusLeads($input: updateStatusLeadsInputType)
  @api(name: "appZim") {
    updateStatusLeads(input: $input) {
      success
      error
      message
      dataString
    }
  }
`,gp=E`
  mutation makeApointment($input: MakeApointmentInputType)
  @api(name: "appZim") {
    makeApointment(input: $input) {
      success
      error
      message
      dataString
    }
  }
`,mp=E`
  mutation changeEcLeads($input: ChangeEcLeadsInputType) @api(name: "appZim") {
    changeEcLeads(input: $input) {
      success
      error
      message
      dataString
    }
  }
`,Td=E`
  mutation addCustomerLeads($input: AddCustomerLeadInputType)
  @api(name: "appZim") {
    addCustomerLeads(input: $input) {
      success
      error
      message
      dataString
    }
  }
`,fp=E`
  query getReportLeads($startDate: String, $endDate: String)
  @api(name: "appZim") {
    getReportLeads(startDate: $startDate, endDate: $endDate) {
      date
      totalView
      dataLeads {
        date
        sourseName
        data
      }
    }
  }
`,Lo=E`
  query getRawDataPagination(
    $q: String
    $status: RawDataStatusEnum
    $fromDate: DateTime
    $toDate: DateTime
    $orderBy: String
    $order: String
    $page: Int
    $limit: Int
    $ecId: Int
    $schoolId: Int
    $tags: [String]
    $date: DateTime
    $sourceName: String
    $isOnlyDataFacebook: Boolean
  ) @api(name: "appZim") {
    getRawDataPagination(
      q: $q
      status: $status
      fromDate: $fromDate
      toDate: $toDate
      orderBy: $orderBy
      order: $order
      page: $page
      limit: $limit
      ecId: $ecId
      schoolId: $schoolId
      tags: $tags
      date: $date
      sourceName: $sourceName
      isOnlyDataFacebook: $isOnlyDataFacebook
    ) {
      hasNextPage
      hasPrevPage
      limit
      page
      pageNext
      pagePrev
      pagingCounter
      totalPages
      totalDocs
      docs {
        id
        student {
          id
          user_name
          full_name
          phone
          address
          email
          role_id
          role_name
          supporter
          student_more_info {
            identity_card_city_id
            note_home
            birthday
            city_id
            city_name
            district_id
            district_name
            ward_id
            ward_name
            street_id
            street_name
            home_number
            source_id
            source_name
            job_id
            job_name
            identity_card
            identity_card_city_name
            identity_card_date
            learning_status_id
            work_place
            learning_status_name
          }
        }
        fb_user_id
        source_id
        ec_name
        ec_id
        source_name
        form_name
        link
        note
        created_date
        appoinment_test_id
        status
        tags {
          level_name
          name
        }
      }
    }
  }
`,xp=E`
  query getRawDataNote($rawId: Int) @api(name: "appZim") {
    getRawDataNote(rawId: $rawId) {
      note
      createBy
      createDate
    }
  }
`,bp=E`
  query facebookCommentsPagination(
    $page: Int
    $limit: Int
    $fbProfileId: String
    $postId: ObjectID
    $search: String
  ) @api(name: "crawler") {
    facebookCommentsPagination(
      page: $page
      limit: $limit
      search: $search
      postId: $postId
      fbProfileId: $fbProfileId
    ) {
      totalDocs
      totalPages
      docs {
        id
        post {
          id
          link
        }
        content
        facebookId
        createdAt
        updatedAt
        owner
        linkProfile
        inDemand
        fbProfileId
      }
    }
  }
`,yp=E`
  query facebookPostsPagination(
    $page: Int
    $limit: Int
    $fbProfileId: String
    $search: String
  ) @api(name: "crawler") {
    facebookPostsPagination(
      page: $page
      limit: $limit
      search: $search
      fbProfileId: $fbProfileId
    ) {
      totalDocs
      totalPages
      docs {
        id
        content
        facebookId
        numOfComments
        numOfLikes
        numOfShares
        numOfCmtInDemand
        link
        featured
        createdAt
        updatedAt
        inDemand
        postOwnerId
        postOwnerName
      }
    }
  }
`,Pd=async(e,n)=>{var a,s,r,o,l;if(e==="")return!1;try{const i=await yt.query({query:Lo,variables:{limit:1,q:e,page:1}});return!(!((s=(a=i.data)==null?void 0:a.getRawDataPagination)==null?void 0:s.docs)||((l=(o=(r=i.data)==null?void 0:r.getRawDataPagination)==null?void 0:o.docs)==null?void 0:l.length)>0)}catch{return!1}},_d=Pe.object().shape({phone:Pe.string().matches(Io,"S\u1ED1 \u0111i\u1EC7n tho\u1EA1i kh\xF4ng h\u1EE3p l\u1EC7").required("S\u1ED1 \u0111i\u1EC7n tho\u1EA1i l\xE0 b\u1EAFt bu\u1ED9c").test("phone","\u0110\xE3 c\xF3 EC ch\u0103m s\xF3c S\u0110T n\xE0y. Vui l\xF2ng ki\u1EC3m tra l\u1EA1i trong lead",Pd),name:Pe.string().required("Vui l\xF2ng nh\u1EADp h\u1ECD t\xEAn"),email:Pe.string().email("\u0110\u1ECBa ch\u1EC9 email kh\xF4ng h\u1EE3p l\u1EC7").required("Vui l\xF2ng nh\u1EADp email"),source:Pe.mixed().nullable(!0).required("M\u1EE5c n\xE0y l\xE0 b\u1EAFt bu\u1ED9c"),note:Pe.string().required("Vui l\xF2ng nh\u1EADp ghi ch\xFA ")}),Vt=e=>t(Ra,b({w:{base:"100%",md:"50%"},p:2},e)),Od=({onClose:e,isOpen:n})=>{const{handleSubmit:a,register:s,control:r,formState:{errors:o},reset:l}=Dt({mode:"onSubmit",reValidateMode:"onBlur",resolver:Fa(_d)}),{data:i,loading:c}=Id(),f=Te(),[p,{loading:d}]=ce(Td,{refetchQueries:[Lo],onCompleted:({addCustomerLeads:y})=>{var u;y.success?(f({title:"Th\xE0nh c\xF4ng !",description:"T\u1EA1o lead th\xE0nh c\xF4ng !",status:"success",duration:3e3,isClosable:!0}),l(),e()):f({title:"T\u1EA1o lead th\u1EA5t b\u1EA1i !",description:(u=y==null?void 0:y.message)!=null?u:"Vui l\xF2ng ki\u1EC3m tra l\u1EA1i th\xF4ng tin \u0111\xE3 nh\u1EADp",status:"error",duration:3e3,isClosable:!0})},onError:y=>{var u;f({title:"T\u1EA1o lead th\u1EA5t b\u1EA1i !",description:(u=y==null?void 0:y.message)!=null?u:"Vui l\xF2ng ki\u1EC3m tra l\u1EA1i th\xF4ng tin",status:"error",duration:3e3,isClosable:!0})}});return g(rt,{isOpen:n,onClose:()=>{},size:"3xl",children:[t(Ye,{}),t(st,{children:g("form",{onSubmit:a(async y=>{try{await p({variables:{input:{phoneNumber:y.phone,fullName:y.name,email:y.email,sourceId:y.source.id,note:y.note}}})}catch(u){console.log({e:u})}}),children:[t(Ke,{children:"T\u1EA1o Lead m\u1EDBi"}),t(it,{onClick:e}),g(k,{px:4,pb:8,children:[g(un,{spacing:0,children:[t(Vt,{children:t(Me,A(b({label:"S\u1ED1 \u0111i\u1EC7n tho\u1EA1i (*)",placeholder:"Nh\u1EADp...",variant:"outline",pr:10},s("phone")),{error:o==null?void 0:o.phone}))}),t(Vt,{children:t(Me,A(b({},s("name")),{label:"H\u1ECD t\xEAn (*)",placeholder:"Nh\u1EADp...",error:o==null?void 0:o.name}))}),t(Vt,{children:t(Me,A(b({label:"Email (*)",placeholder:"Nh\u1EADp..."},s("email")),{error:o==null?void 0:o.email}))}),t(Vt,{children:t(mt,{name:"source",control:r,render:({field:y})=>t(Gt,b({label:"Ngu\u1ED3n (*)",error:o==null?void 0:o.source,getOptionLabel:u=>u.sourceOfCustomer,getOptionValue:u=>u.id,options:i?i.getSourceOfCustomer:[],isLoading:c},y))})}),t(Ra,{w:"full",p:2,children:t(wd,A(b({label:"Ghi ch\xFA (*)",bg:"white",minRows:2,placeholder:"N\u1ED9i dung..."},s("note")),{error:o==null?void 0:o.note}))})]}),g(D,{px:2,mt:4,spacing:4,children:[t(Q,{type:"submit",colorScheme:"green",isLoading:d,children:"T\u1EA1o m\u1EDBi"}),t(Q,{variant:"outline",onClick:e,children:"H\u1EE7y b\u1ECF"})]})]})]})})]})},Fd=({conversationDetail:e})=>{var n;return t(k,{borderBottomWidth:"1px",borderColor:"whiteAlpha.300",children:(n=e==null?void 0:e.participants)==null?void 0:n.map(a=>{var s;return t(Ct,{avatarUrl:a==null?void 0:a.avatar,username:a==null?void 0:a.fullName,positionName:(s=mn.find(r=>{var o;return r.id===Number((o=a.roleId)!=null?o:14)}))==null?void 0:s.name},a==null?void 0:a.id)})})},Ld=()=>{var v,R,_,j,C,P,L,H,W,G,ne,te,J,de,se,z;const{onOpen:e,isOpen:n,onClose:a}=qe(),{isOpen:s,onOpen:r,onClose:o}=qe(),l=F(O=>O.userId),[i]=re(),c=F(O=>O.visibleUserProfile),f=F(O=>O.setVisibleUserProfile),p=F(O=>O.conversationId),{conversationDetail:d}=ko(p,l),$=wo({conversationDetail:d}),y=(v=d==null?void 0:d.participants)==null?void 0:v.find(O=>(O==null?void 0:O.roleId)==="5"||(O==null?void 0:O.roleId)===null),u=$?y:(R=d==null?void 0:d.participants)==null?void 0:R.filter(O=>(O==null?void 0:O.userId)!==(i==null?void 0:i.id.toString()))[0],{userData:h,loading:x}=vo(l||(u==null?void 0:u.userId),d.name),S=$?(u==null?void 0:u.fullName)||((_=d==null?void 0:d.owner)==null?void 0:_.fullName):(h==null?void 0:h.full_name)?h==null?void 0:h.full_name:(u==null?void 0:u.isGuest)?`${u==null?void 0:u.fullName}`:u==null?void 0:u.fullName;Xa("click",O=>{const Z=O.target;!Z.closest(".profile")&&!Z.closest("#js-toggle-profile")&&f(!1)});const I=ft({base:"",xl:"visible"});return!l&&!p?null:g(Ie,{children:[g(Ae,{pos:I?void 0:"fixed",top:I?0:"56px",right:0,bottom:0,zIndex:51,h:"full",overflowY:"auto",bg:T("white","slate.900"),w:{base:"320px","2xl":"380px"},transform:I||c?"none":"translateX(100%)",transition:"all .3s ease",className:"profile",children:[!(d==null?void 0:d.name)&&(((j=d==null?void 0:d.participants)==null?void 0:j.length)===2&&((C=d==null?void 0:d.participants)==null?void 0:C.find(O=>O.userId===i.id.toString()))||$&&(i==null?void 0:i.roleId)===6)&&g(It,{in:!(d==null?void 0:d.name)&&((P=d==null?void 0:d.participants)==null?void 0:P.length)===2&&((L=d==null?void 0:d.participants)==null?void 0:L.find(O=>O.userId===i.id.toString()))||$&&(i==null?void 0:i.roleId)===6,children:[t(fd,{tags:(H=d==null?void 0:d.tags)!=null?H:[],isStudent:Ro.includes(i==null?void 0:i.roleId),participantAvatarUrl:u==null?void 0:u.avatar,fullName:S,loading:x,participants:d==null?void 0:d.participants,participantInfo:h}),Fo.create.includes(i==null?void 0:i.roleId)&&(u==null?void 0:u.isGuest)&&g(k,{p:2,borderBottomWidth:1,borderColor:"whiteAlpha.300",children:[t(Q,{w:"full",colorScheme:"green",onClick:r,children:"Th\xEAm lead m\u1EDBi"}),Fo.create.includes(i==null?void 0:i.roleId)&&t(Od,{isOpen:s,onClose:o})]}),(h==null?void 0:h.supporter)&&!Ro.includes(i==null?void 0:i.roleId)&&((G=(W=d==null?void 0:d.participants)==null?void 0:W.filter(O=>O.roleId!=="5"))==null?void 0:G.map(O=>{var ge;const Z=(O==null?void 0:O.userId)===((ge=h==null?void 0:h.supporter)==null?void 0:ge.id.toString());return t(Ct,{username:O==null?void 0:O.fullName,positionName:`${Z?"EC ch\xEDnh":"EC h\u1ED7 tr\u1EE3"}`},O==null?void 0:O.id)})),((ne=h==null?void 0:h.supporter)==null?void 0:ne.id)===Number(i==null?void 0:i.id)&&((te=d==null?void 0:d.participants)==null?void 0:te.length)&&g(k,{w:"full",px:2,children:[t(Q,{onClick:e,w:"full",rightIcon:t(Gs,{}),colorScheme:"blue",variant:"outline",children:"Th\xEAm EC h\u1ED7 tr\u1EE3"}),t(Sn,{isEdit:!0,isAddSupporter:!0,isOpen:n,onClose:a,listParticipants:(J=d==null?void 0:d.participants)==null?void 0:J.map(O=>O==null?void 0:O.userId),name:d==null?void 0:d.name,groupId:p})]})]}),$&&!(d==null?void 0:d.name)&&t(Fd,{conversationDetail:d}),(d==null?void 0:d.name)&&t(It,{in:d==null?void 0:d.name,children:t(Ad,{conversationId:p,conversationDetail:d})}),p&&t(Ed,{userId:l,conversationId:p,isSuperAdmin:Mo.includes(i==null?void 0:i.roleId),allowForSupperAdmin:Mo.includes(i==null?void 0:i.roleId)&&((de=d==null?void 0:d.participants)==null?void 0:de.some(O=>(O==null?void 0:O.userId)===(i==null?void 0:i.id.toString())))}),((se=h==null?void 0:h.courseStudents)==null?void 0:se.length)>0&&(i==null?void 0:i.roleId)!==5&&t(vd,{data:(z=h==null?void 0:h.courseStudents)!=null?z:[]}),t($d,{userId:l,conversationId:p})]}),c&&t(Ie,{children:t(k,{bg:"blackAlpha.800",shadow:"md",pos:"fixed",top:14,bottom:0,zIndex:50,left:0,right:0,h:"screen"})})]})},F=Vs(e=>({conversationId:"",setConversationId:n=>e({conversationId:n}),userId:"",setUserId:n=>e({userId:n}),tab:"conversation",setTab:n=>e({tab:n}),channel:"",setChannel:n=>e({channel:n}),participants:[],setParticipants:n=>e({participants:n}),visibleUserProfile:!1,setVisibleUserProfile:n=>e({visibleUserProfile:n}),searchStringMessage:"",setSearchStringMessage:n=>e({searchStringMessage:n}),searchResult:[],setSearchResult:n=>e({searchResult:n}),nearSearchResult:[],setNearSearchResult:n=>e({nearSearchResult:n}),messageId:"",setMessageId:n=>e({messageId:n}),beforeId:"",setBeforeId:n=>e({beforeId:n}),afterId:"",setAfterId:n=>e({afterId:n})})),Nd=()=>{const e=F(u=>u.setChannel),n=F(u=>u.userId),a=F(u=>u.setUserId),s=F(u=>u.tab),r=F(u=>u.conversationId),o=F(u=>u.setConversationId),[l]=re(),[i,c]=Qe({}),{height:f}=to(),p=ft({base:"",md:"visible"});Jl();const d=i.get("qsConversationId"),$=i.get("qsUserId"),y=i.get("qsGroup");return m.exports.useEffect(()=>{(l==null?void 0:l.roleId)===6?e("guest"):e("my chats")},[l]),m.exports.useEffect(()=>{d||$?(d&&o(d),$&&a($),y&&e(y)):(i.delete("qsConversationId"),i.delete("qsUserId"),i.delete("qsGroup"))},[d,$,y]),g(ee,{h:f-113,spacing:0,w:"full",children:[!p&&t(_l,{}),g(k,{w:"100%",h:"100%",display:"flex",className:"divide-x",children:[(p||!p&&s==="channel")&&t(eu,{}),(p||!p&&s==="conversation")&&t(Kl,{}),(p||!p&&s==="message")&&t(od,{}),(n||r)&&t(Ld,{})]})]})};var Md=Object.freeze({__proto__:null,[Symbol.toStringTag]:"Module",useStore:F,default:Nd});const Rd=()=>{const e=Te(),n=Ue(),a=F(l=>l.conversationId),[s,{loading:r}]=ce(wl);return{onDeleteTag:m.exports.useCallback(async l=>{try{await s({variables:{tagId:l,conversationId:a}}),await n(["conversationDetail"])}catch(i){e({title:"L\u1ED7i !!!",description:i.message,status:"success",duration:2e3,isClosable:!0})}},[a,s,e,n]),loading:r}};function Dd({id:e,name:n,color:a}){const{onDeleteTag:s,loading:r}=Rd(),o=F(l=>l.conversationId);return g(Xs,{size:"md",rounded:"full",children:[t(k,{bg:a,w:"5px",h:"5px",rounded:"full",mr:1}),t(Js,{children:n}),r?t(ht,{color:"red.500",size:"xs",ml:2.5}):o&&t(ei,{onClick:()=>s(e)})]})}const Bd=(e,n)=>{var a,s;return[{icon:t(Ws,{}),value:(a=e==null?void 0:e.address)!=null?a:"Kh\xF4ng c\xF3 th\xF4ng tin"},{icon:t(Qs,{}),value:(e==null?void 0:e.phone)||"Kh\xF4ng c\xF3 th\xF4ng tin"},{icon:t(Zs,{}),value:((s=e==null?void 0:e.more_info)==null?void 0:s.facebook)||"Kh\xF4ng c\xF3 th\xF4ng tin"},{icon:t(Ys,{}),value:(e==null?void 0:e.email)||"Kh\xF4ng c\xF3 th\xF4ng tin"},{icon:t(Ks,{}),value:(n==null?void 0:n.length)?()=>t(un,{flex:1,children:n==null?void 0:n.map((r,o)=>t(Dd,b({},r),o))}):"Kh\xF4ng c\xF3 tag"}]},No=["Admin","CC","CS","GV","EC","AM","KT","KTH","MK","BM","BP"],$n=[{id:1,name:"Super Admin"},{id:2,name:"CC"},{id:4,name:"Gi\xE1o vi\xEAn"},{id:6,name:"EC"},{id:8,name:"AM"},{id:11,name:"Marketer"},{id:12,name:"BM"}],Mo=[1],Ro=[5],zd="File c\u1EE7a b\u1EA1n v\u01B0\u1EE3t qu\xE1 10MB.",jd="File c\u1EE7a b\u1EA1n kh\xF4ng \u0111\u01B0\u1EE3c h\u1ED7 tr\u1EE3.",qd=[{color:"#00DAA3"},{color:"#21A3F3"},{color:"#F51C50"},{color:"#006E7F"},{color:"#F8CB2E"}],Ud=[{key:"",value:"All"},{key:"zimians",value:"Zimians"},{key:"student",value:"Student"}],Hd=e=>e.scrollHeight-e.scrollTop<=e.clientHeight+10,Gd=({getCheckboxProps:e,listParticipants:n,usersInfo:a,isAddSupporter:s})=>{var v,R,_,j;const[r,o]=m.exports.useState(""),[l,i]=m.exports.useState(!1),[c,f]=m.exports.useState(""),p=C=>{i(Hd(C.target))},d=pe({roles:s?["EC"]:c==="all"?"":c==="zimians"?No:c==="student"?["HV"]:"",searchString:r,limit:20},C=>C),{data:$,loading:y,fetchMore:u}=he(Wa,{variables:d}),h=(R=(v=$==null?void 0:$.getAccountChatPagination)==null?void 0:v.docs)==null?void 0:R.filter(C=>{var P,L;return!(n==null?void 0:n.includes((L=(P=C==null?void 0:C.user)==null?void 0:P.id)==null?void 0:L.toString()))}),x=(_=$==null?void 0:$.getAccountChatPagination)==null?void 0:_.page,S=(j=$==null?void 0:$.getAccountChatPagination)==null?void 0:j.hasNextPage,{onLoadMore:w,isLoadingMore:I}=ut({variables:A(b({},d),{page:x+1}),fetchMore:u,hasNextPage:S,isNormalLoadMore:!1});return m.exports.useEffect(()=>{l&&S&&!I&&w()},[l,S,I]),g(Ie,{children:[t(D,{children:Ud.map((C,P)=>{const L=C.key===c;return t(Q,{onClick:()=>f(C.key),colorScheme:"teal",variant:L?"solid":"outline",disabled:s&&P!==1,children:C.value},P)})}),t(un,{children:a==null?void 0:a.map((C,P)=>t(pn,{children:C.full_name},P))}),t(Me,{type:"text",placeholder:"Nh\u1EADp t\xEAn ng\u01B0\u1EDDi d\xF9ng",size:"md",value:r,onChange:C=>o(C.target.value)}),g(k,{position:"relative",h:"calc(65vh - 140px)",overflow:"auto",w:"full",flexGrow:1,onScroll:p,children:[g(ot,{columns:2,spacing:2,children:[(h==null?void 0:h.length)>0&&(h==null?void 0:h.map(({user:C})=>{const P=e({value:C.id});return t(k,{rounded:"lg",overflow:"hidden",children:t(xl,A(b({isMultiple:!0},P),{children:t(Ct,A(b({},C),{username:C==null?void 0:C.full_name,positionName:C==null?void 0:C.role_name,lastOnline:C==null?void 0:C.status_online}))}))},`${C.id}`)})),y&&g(D,{rounded:"lg",bg:"blackAlpha.300",opacity:.5,pos:"absolute",top:0,bottom:0,right:0,left:0,justifyContent:"center",children:[t(ht,{color:"#319795",size:"lg"}),t(N,{fontSize:"xl",color:"white",children:"\u0110ang t\u1EA3i ..."})]})]}),I&&g(D,{w:"full",alignItems:"center",justifyContent:"center",py:2,children:[t(ht,{color:"#319795",size:"lg"}),t(N,{fontSize:"xl",color:T("gray.700","white"),children:"\u0110ang t\u1EA3i ..."})]})]})]})},Vd=({onClose:e,userId:n,isEdit:a,groupId:s,setUserId:r,isAddSupporter:o})=>{const[l]=re(),i=Te(),c=Ue(),f=F(x=>x.setConversationId),p=F(x=>x.setChannel),[d,{loading:$}]=ce(Cl,{onCompleted:x=>{const{createConversation:S}=x;S&&(i({title:"Th\xE0nh c\xF4ng!",description:`T\u1EA1o nh\xF3m chat ${S==null?void 0:S.name} th\xE0nh c\xF4ng`,status:"success",duration:3e3,isClosable:!0}),e(),f(S==null?void 0:S.id),p("my chats")),r([]),c(["conversations","conversationDetail"])},onError:x=>{var S;i({title:"T\u1EA1o nh\xF3m chat kh\xF4ng th\xE0nh c\xF4ng!",description:`${(S=x==null?void 0:x.message)!=null?S:"L\u1ED7i"}`,status:"error",duration:3e3,isClosable:!0})}}),[y,{loading:u}]=ce($l,{onCompleted:x=>{const{updateConversation:S}=x;S&&i({title:"Th\xE0nh c\xF4ng!",description:o?"Th\xEAm EC h\u1ED7 tr\u1EE3 th\xE0nh c\xF4ng":`C\u1EADp nh\u1EADt nh\xF3m chat ${S==null?void 0:S.name} th\xE0nh c\xF4ng`,status:"success",duration:3e3,isClosable:!0}),e(),r([]),c(["conversations","conversationDetail"])},onError:x=>{var S;i({title:"C\u1EADp nh\u1EADt nh\xF3m chat kh\xF4ng th\xE0nh c\xF4ng!",description:`${(S=x==null?void 0:x.message)!=null?S:"L\u1ED7i"}`,status:"error",duration:3e3,isClosable:!0})}});return{handleSubmitForm:m.exports.useCallback(async x=>{try{a?await y({variables:pe({name:x==null?void 0:x.name,id:s,userIds:n==null?void 0:n.filter(S=>S!==(l==null?void 0:l.id))})}):await d({variables:pe({name:x==null?void 0:x.name,group:s,userIds:n})})}catch(S){console.log(S)}},[n,s,a,l]),loading:$||u}},Wd=e=>typeof e=="object"?Promise.resolve(e):yt.query({query:Va,variables:{userId:parseInt(e,10)}}).then(({data:n})=>n.getUserById),Qd=(e,n)=>We.exports.isEmpty(e||n)?[]:Promise.allSettled(e==null?void 0:e.map(Wd)).then(a=>a.filter(s=>s.status==="fulfilled").map(s=>s==null?void 0:s.value)),Sn=({onClose:e,isOpen:n,groupId:a="",listParticipants:s,name:r,isEdit:o,isAddSupporter:l})=>{const[i,c]=m.exports.useState([]),f=m.exports.useRef(),[p,d]=m.exports.useState([]),{getCheckboxProps:$,setValue:y}=Aa({onChange:d}),{handleSubmitForm:u,loading:h}=Vd({onClose:e,userId:[...s!=null?s:[],...p!=null?p:[]],isEdit:o,groupId:a,setUserId:d,isAddSupporter:l}),x=Dt({mode:"onBlur",reValidateMode:"onChange"}),{register:S,formState:{errors:w}}=x,I=m.exports.useRef(null),v=x.watch("name"),R=!We.exports.isEqual(v,I.current)||(p==null?void 0:p.length)>0;return Jt(()=>{o&&!l&&(x.reset({name:r}),I.current=r)},[n,o,I,x,r]),m.exports.useEffect(()=>{(async function(){try{const _=await Qd(p,!n);c(_)}catch(_){console.log(_)}})()},[p]),t(on,A(b({},x),{children:g(rt,{finalFocusRef:f,isOpen:n,onClose:()=>{e(),y([])},size:"2xl",children:[t(Ye,{}),g(st,{bg:T(void 0,"slate.900"),children:[t(Ke,{children:l?"Th\xEAm EC h\u1ED7 tr\u1EE3":o?"C\u1EADp nh\u1EADt nh\xF3m chat":"T\u1EA1o nh\xF3m chat"}),t(it,{}),g("form",{onSubmit:x.handleSubmit(u),children:[t(lt,{children:g(Ae,{spacing:6,children:[!l&&t(Me,A(b({label:"T\xEAn nh\xF3m",placeholder:"Nh\u1EADp t\xEAn nh\xF3m"},S("name",{required:!0})),{error:w==null?void 0:w.name})),t(Gd,{isAddSupporter:l,usersInfo:i,listParticipants:s,getCheckboxProps:$})]})}),g(gt,{children:[t(Q,{disabled:!o&&We.exports.isEmpty(p)||h||o&&!R,leftIcon:t(we,{as:rn}),bg:"#319795",mr:4,type:"submit",isLoading:!1,color:"white",children:o?"C\u1EADp nh\u1EADt":"T\u1EA1o nh\xF3m"}),t(Q,{disabled:h,variant:"outline",onClick:()=>{e(),y([]),d([])},children:"H\u1EE7y"})]})]})]})]})}))},Zd=({searchString:e,setSearchString:n})=>{const[a]=re(),{isOpen:s,onOpen:r,onClose:o}=qe();return g(ee,{w:"100%",p:{base:3},position:"sticky",top:0,zIndex:50,bg:T("white","#10172a"),alignItems:"stretch",children:[g(k,{w:"100%",display:"flex",alignItems:"start",maxW:{base:"full"},className:" space-x-2.5",bg:T("white",""),p:{base:2,md:0},rounded:"lg",children:[t(Se,{display:{base:"block"},boxSize:"2em",src:(a==null?void 0:a.avatar)===ze?"":a==null?void 0:a.avatar}),g(k,{flex:1,children:[t(N,{noOfLines:1,fontSize:14,lineHeight:"18px",fontWeight:"medium",color:T("gray.800","slate.300"),children:a==null?void 0:a.fullName}),t(N,{display:{base:"block"},noOfLines:1,fontSize:14,fontWeight:500,lineHeight:"24px",color:"gray.400",children:a==null?void 0:a.role})]})]}),g(D,{children:[t(fl,{searchString:e,setSearchString:n}),$n.map(l=>l.id).includes(a==null?void 0:a.roleId)&&t(k,{onClick:r,w:6,h:6,cursor:"pointer",children:t(ti,{className:"w-full h-full"})})]}),s&&t(Sn,{isAddSupporter:!1,isEdit:!1,name:"",listParticipants:[],isOpen:s,onClose:o})]})},Do=({conversations:e,onClick:n,isLoading:a,canLoadMore:s,onLoadMore:r,loading:o})=>t(It,{in:!0,children:g(ee,{alignItems:"stretch",children:[e==null?void 0:e.map((l,i)=>{const c=l==null?void 0:l.user,f=(c==null?void 0:c.role_id)===5?"student":(c==null?void 0:c.role_id)===14?"customer":"zimians";return o?t(ao,{},`loading_${i}`):m.exports.createElement(Ct,A(b({},c),{lastOnline:c==null?void 0:c.status_online,avatarUrl:(c==null?void 0:c.avatar)===ze?"":c==null?void 0:c.avatar,username:c==null?void 0:c.full_name,positionName:c==null?void 0:c.role_name,key:i,onClick:()=>n(c==null?void 0:c.id,f)}))}),s&&t(Q,{isLoading:a,loadingText:"\u0110ang t\u1EA3i th\xEAm",onClick:r,w:"100%",rounded:"none",borderWidth:"1px 0px 0px 0px",bg:T("white","slate.600"),fontSize:14,fontWeight:400,justifyContent:"center",children:"Xem th\xEAm"})]})}),Yd=({title:e,onClick:n,children:a,overallMessageUnRead:s})=>g(ni,{borderBottom:1,borderColor:"",children:[g(D,{position:"relative",spacing:0,pl:4,_hover:{backgroundColor:T("#f0f0f0","slate.600")},children:[t(k,{onClick:n,display:"flex",justifyContent:"start",flex:1,as:"button",children:t(N,{color:T("#444","white"),fontSize:16,fontWeight:"medium",children:e})}),t(ai,{display:"flex",alignItems:"center",justifyContent:"center",w:10,children:t(oi,{})}),s&&t(An,{count:s})]}),t(ri,{p:0,children:a})]}),Bo=({title:e="Accordion",children:n,onClick:a,overallMessageUnRead:s})=>t(si,{defaultIndex:[0],allowMultiple:!0,children:t(Yd,{overallMessageUnRead:s,children:n,title:e,onClick:a})}),zo=({roles:e,searchString:n})=>{var d,$,y;const a=pe({roles:e,searchString:n,limit:10,page:1},u=>u),{data:s,loading:r,fetchMore:o}=he(Wa,{variables:a}),l=(d=s==null?void 0:s.getAccountChatPagination)==null?void 0:d.docs,i=($=s==null?void 0:s.getAccountChatPagination)==null?void 0:$.page,c=(y=s==null?void 0:s.getAccountChatPagination)==null?void 0:y.hasNextPage,{onLoadMore:f,isLoadingMore:p}=ut({variables:A(b({},a),{page:i+1}),fetchMore:o,hasNextPage:c,isNormalLoadMore:!1});return{listAccount:l,loading:r,onLoadMore:f,hasNextPage:c,isLoadingMore:p}},Kd=({searchString:e,setUserId:n,setTab:a,setChannel:s,setConversationId:r,setParticipants:o,overallMessageUnRead:l})=>{const[i]=re(),[c,f]=Qe({}),{listAccount:p,onLoadMore:d,hasNextPage:$,isLoadingMore:y,loading:u}=zo({roles:No,searchString:e});return t(Ie,{children:t(Bo,{onClick:()=>s("zimians"),title:"Zimians",overallMessageUnRead:l,children:t(Do,{canLoadMore:$,isLoading:y,onLoadMore:d,onClick:(h,x)=>{n(h),f({qsUserId:h,qsGroup:x}),c.delete("qsConversationId"),r(""),a("message"),s(x),o([])},loading:u,conversations:u?[1,2,3,4,5,6,7,8]:(p==null?void 0:p.filter(h=>{var x;return((x=h.user)==null?void 0:x.id)!==parseInt(i==null?void 0:i.id,10)}))||[]})})})},Xd=({searchString:e,setUserId:n,setTab:a,setChannel:s,setConversationId:r,setParticipants:o,overallMessageUnRead:l})=>{const[i,c]=Qe({}),{listAccount:f,hasNextPage:p,isLoadingMore:d,onLoadMore:$,loading:y}=zo({roles:["HV"],searchString:e});return t(Bo,{overallMessageUnRead:l,onClick:()=>s("student"),title:"Student",children:t(Do,{isLoading:d,canLoadMore:p,onLoadMore:$,onClick:(u,h)=>{n(u),r(""),c({qsUserId:u,qsGroup:h}),i.delete("qsConversationId"),a("message"),s(h),o([])},loading:y,conversations:y?[1,2,3,4,5,6,7,8]:f})})},Jd=e=>{var s;const{data:n}=he(Ri,{variables:{userId:e}}),a=n==null?void 0:n.getUserById;return{supporterInfo:a,id:(s=a==null?void 0:a.supporter)==null?void 0:s.id}};function An({count:e}){return t(pn,{w:6,h:6,variant:"solid",rounded:"full",bg:"blue.500",position:"absolute",right:8,alignItems:"center",justifyContent:"center",display:"flex",fontSize:11,children:t(k,{alignItems:"center",justifyContent:"center",display:"flex",children:e>99?"99+":e})})}const eu=()=>{var u,h;const[e]=re(),[n,a]=Qe({}),s=F(x=>x.setParticipants),r=F(x=>x.setChannel),o=F(x=>x.setUserId),l=F(x=>x.setConversationId),i=F(x=>x.setTab),[c,f]=m.exports.useState(""),p=Eo(c,500),{overall:d,overallMyChats:$}=ml(),{id:y}=Jd(Number(e==null?void 0:e.id));return g(k,{w:{base:"full",md:"200px",lg:"250px"},bg:T("white","#10172a"),maxH:"100%",overflow:"hidden",overflowY:"scroll",children:[t(Zd,{searchString:c,setSearchString:f}),g(Ae,{zIndex:50,spacing:0,position:"sticky",top:{sm:"125px",md:"109px"},children:[(e==null?void 0:e.roleId)===5&&y&&t(k,{display:"flex",alignItems:"center",px:4,h:10,cursor:"pointer",_hover:{backgroundColor:T("white","slate.500")},onClick:async()=>{a({qsUserId:y}),n.delete("qsConversationId"),n.delete("qsGroup"),r("student"),o(y),i("message"),l(""),s([])},w:"100%",rounded:"none",borderWidth:"1px 0px 0px 0px",bg:T("white","#10172a"),fontSize:16,fontWeight:500,justifyContent:"start",children:t(N,{children:"Chat v\u1EDBi supporter"})}),g(k,{display:"flex",alignItems:"center",px:4,h:10,cursor:"pointer",_hover:{backgroundColor:T("white","slate.500")},onClick:()=>{r("my chats"),i("conversation")},w:"100%",rounded:"none",borderWidth:"1px 0px 0px 0px",bg:T("white","#10172a"),fontSize:16,fontWeight:500,justifyContent:"start",children:["My Chats",$>0&&t(An,{count:$})]}),(e==null?void 0:e.roleId)!==4&&(e==null?void 0:e.roleId)!==5&&g(Q,{display:"flex",alignItems:"center",px:4,h:10,cursor:"pointer",_hover:{backgroundColor:T("white","slate.500")},onClick:()=>{r("guest"),i("conversation")},w:"100%",rounded:"none",borderWidth:"1px 0px 0px 0px",bg:T("white","#10172a"),fontSize:16,fontWeight:500,justifyContent:"start",children:["Guest",(d==null?void 0:d[3])+(d==null?void 0:d[4])>0&&t(An,{count:(d==null?void 0:d[3])+(d==null?void 0:d[4])})]})]}),g(Ae,{display:{base:"block"},spacing:0,children:[(e==null?void 0:e.roleId)!==5&&t(Kd,{overallMessageUnRead:(u=d==null?void 0:d[0])!=null?u:null,setUserId:o,setTab:i,setChannel:r,setConversationId:l,setParticipants:s,searchString:p}),t(Xd,{overallMessageUnRead:(h=d==null?void 0:d[1])!=null?h:null,setUserId:o,setTab:i,setChannel:r,setConversationId:l,searchString:p,setParticipants:s})]})]})},tu=Ce(()=>$e(()=>import("./index4.js"),["assets/index4.js","assets/vendor.js"])),nu=Ce(()=>$e(()=>import("./index5.js"),["assets/index5.js","assets/vendor.js","assets/index6.js","assets/index7.js","assets/dateToStringFormat.js","assets/index8.js","assets/index9.js"])),au=Ce(()=>$e(()=>import("./index10.js"),["assets/index10.js","assets/vendor.js","assets/dateToStringFormat.js","assets/main.js","assets/customParseFormat.js","assets/index9.js"])),ou=Ce(()=>$e(()=>Promise.resolve().then(function(){return Su}),void 0)),ru=Ce(()=>$e(()=>import("./index11.js"),["assets/index11.js","assets/vendor.js","assets/index7.js","assets/index8.js","assets/index6.js"])),su=Ce(()=>$e(()=>Promise.resolve().then(function(){return Md}),void 0)),iu=Ce(()=>$e(()=>import("./index12.js"),["assets/index12.js","assets/vendor.js","assets/index7.js","assets/customParseFormat.js","assets/index9.js","assets/dateToStringFormat.js"])),lu=Ce(()=>$e(()=>import("./index13.js"),["assets/index13.js","assets/vendor.js","assets/index8.js","assets/index7.js","assets/index6.js"])),cu=Ce(()=>$e(()=>import("./index14.js"),["assets/index14.js","assets/vendor.js","assets/main.js","assets/customParseFormat.js","assets/useGetCenterByUser.js","assets/utils.js","assets/isSameOrAfter.js","assets/dateToStringFormat.js","assets/index6.js"])),du=Ce(()=>$e(()=>import("./index15.js").then(function(e){return e.i}),["assets/index15.js","assets/main.js","assets/vendor.js","assets/customParseFormat.js","assets/dateToStringFormat.js","assets/useGetCenterByUser.js","assets/index9.js"])),uu=Ce(()=>$e(()=>import("./index16.js"),["assets/index16.js","assets/vendor.js"])),pu=Ce(()=>$e(()=>import("./index17.js"),["assets/index17.js","assets/vendor.js","assets/index6.js","assets/index7.js","assets/index8.js","assets/index9.js"])),Ep=[{path:Ne.home,exact:!0,component:tu,allowRoles:[],breadcrumbText:"Trang ch\u1EE7"},{path:Ne.courseDetail,exact:!0,component:au,allowRoles:[],breadcrumbText:"Kh\xF3a h\u1ECDc"},{path:Ne.listCourses,exact:!0,component:nu,allowRoles:[],breadcrumbText:"Danh s\xE1ch kh\xF3a h\u1ECDc"},{path:`${Ne.ticket}`,exact:!0,component:ru,allowRoles:[],breadcrumbText:"Danh s\xE1ch ticket"},{path:`${Ne.ticket}/:id`,exact:!0,component:ou,allowRoles:[],breadcrumbText:"Chi ti\u1EBFt ticket"},{path:`${Ne.leadReport}`,exact:!1,component:iu,allowRoles:[],breadcrumbText:"Lead Report"},{path:`${Ne.registerTestSchedule}`,exact:!1,component:uu,allowRoles:[],breadcrumbText:"\u0110\u0103ng k\xFD l\u1ECBch tr\u1EF1c test"},{path:`${Ne.lead}`,exact:!1,component:pu,allowRoles:[],breadcrumbText:"Ngu\u1ED3n kh\xE1ch h\xE0ng"},{path:`${Ne.studentSupportByEc}`,exact:!1,component:lu,allowRoles:[],breadcrumbText:"Danh s\xE1ch h\u1ECDc vi\xEAn"},{path:Ne.chat,exact:!0,component:su,allowRoles:[]},{path:Ne.scheduleRegistration,exact:!0,component:cu,allowRoles:[],breadcrumbText:"\u0110\u0103ng k\xFD l\u1ECBch l\xE0m vi\u1EC7c"},{path:Ne.listSchedules,exact:!0,component:du,allowRoles:[],breadcrumbText:"Danh s\xE1ch l\u1ECBch l\xE0m vi\u1EC7c"}];Pe.object().shape({username:Pe.string().required("Vui l\xF2ng nh\u1EADp t\xE0i kho\u1EA3n"),password:Pe.string().required("Vui l\xF2ng nh\u1EADp m\u1EADt kh\u1EA9u")});const kn={waiting:{tooltip:"Ticket waiting",icon:ii,color:"red"},longtime:{tooltip:"Longtime no see",icon:li,color:"blue"}},hu=({type:e="waiting"})=>{const n=m.exports.useMemo(()=>kn==null?void 0:kn[e],[e]);return t(Lt,{hasArrow:!0,bg:T("slate.700","slate.700"),label:n==null?void 0:n.tooltip,"aria-label":"status tool tip",isDisabled:!(n==null?void 0:n.tooltip),color:T("slate.300","slate.300"),children:t(D,{spacing:1,color:`${n==null?void 0:n.color}.500`,children:t(k,{as:"span",children:(n==null?void 0:n.icon)&&t(we,{as:n==null?void 0:n.icon})})})})},gu=Da(D)`
  .flip-card {
    perspective: 1000px;
    background: transparent;
  }
  .flip-card-inner {
    position: relative;
    width: 100%;
    height: 100%;
    text-align: center;
    transition: transform 0.6s;
    transform-style: preserve-3d;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
  }

  .flip-card:hover .flip-card-inner {
    transform: rotateY(180deg);
  }

  .flip-card-front,
  .flip-card-back {
    position: absolute;
    width: 100%;
    height: 100%;
    -webkit-backface-visibility: hidden;
    backface-visibility: hidden;
  }

  .flip-card-front {
    background-color: #bbb;
    color: black;
  }

  .flip-card-back {
    background-color: #2980b9;
    color: white;
    transform: rotateY(180deg);
    display: flex;
    align-items: center;
    justify-content: center;
  }
`,jo=({name:e,avatar:n,meta:a,lastContact:s="",badgeTitle:r="",badgeProps:o,isFlipTicketAvatarCard:l=!1,onClickFlipCard:i=null,avatarSize:c=10,badgeStatusType:f=null})=>g(gu,{flexGrow:1,alignItems:"flex-start",spacing:4,children:[t(k,{className:"flip-card",cursor:i?"pointer":"normal",width:c,height:c,rounded:"full",onClick:i,flexShrink:0,overflow:"hidden",children:l?t(Lt,{label:"G\u1EEDi ticket ch\u0103m s\xF3c",children:g(k,{className:"flip-card-inner",children:[t(k,{className:"flip-card-front",children:t(Se,{src:n,objectFit:"cover",boxSize:c,flexShrink:0})}),t(k,{className:"flip-card-back",children:t(we,{as:ci,width:6,height:6})})]})}):t(Se,{src:n,objectFit:"cover",boxSize:c,flexShrink:0})}),g(ee,{justifyContent:"stretch",alignItems:"flex-start",spacing:0,children:[g(Ee,{alignItems:"flex-start",gap:2,children:[t(il,{to:"/",children:t(N,{fontWeight:600,children:e||""})}),r&&t(pn,A(b({size:"sm",colorScheme:"green",variant:"subtle",rounded:4},o),{children:r})),f&&t(hu,{type:f})]}),g(Ee,{gap:0,fontSize:"sm",direction:"column",color:"gray.500",children:[t(N,{children:a||""}),s&&g(N,{fontSize:"xs",children:["Last contact ",s||""]})]})]})]}),qo=({data:e,onTicketUpdate:n,canOperationComment:a})=>{var P;const[s,r]=Et(),C=e,{name:o,avatar:l,time:i,status:c,meta:f,question:p}=C,d=Re(C,["name","avatar","time","status","meta","question"]),[$,y]=m.exports.useState(e.question.content),u=m.exports.useRef(),h=Te(),[x,{loading:S}]=ce(Po,{onCompleted:L=>{const{updateTicket:H}=L;n(A(b({},e),{question:{title:H==null?void 0:H.title,content:H==null?void 0:H.description}})),h({title:"Th\xE0nh c\xF4ng!",description:"C\u1EADp nh\u1EADt n\u1ED9i dung th\xE0nh c\xF4ng.",status:"success",duration:3e3,isClosable:!0}),r()},onError:L=>{var H;h({title:"Th\u1EA5t b\u1EA1i!",description:`${(H=L==null?void 0:L.message)!=null?H:"L\u1ED7i c\u1EADp nh\u1EADt"}`,status:"error",duration:3e3,isClosable:!0})}}),[w]=re(),I=parseInt(w.id,10)===((P=e==null?void 0:e.createBy)==null?void 0:P.id),[v,{loading:R}]=ce(_o,{onCompleted:L=>{const{updateTicketStatus:H}=L;H&&(n(A(b({},e),{status:H.status})),h({title:"Th\xE0nh c\xF4ng!",description:H.status===ke.DELETED?"\u0110\xE3 x\xF3a ticket":"C\u1EADp nh\u1EADt tr\u1EA1ng th\xE1i th\xE0nh c\xF4ng.",status:"success",duration:3e3,isClosable:!0}))}}),_=async()=>{var L,H,W;if(u.current){const G=(H=(L=u==null?void 0:u.current)==null?void 0:L.props)==null?void 0:H.editor;if(!G||!(G==null?void 0:G.getText()))return;const te=G==null?void 0:G.getHTML();te&&y(te),await x({variables:{input:{refId:(W=e==null?void 0:e.id)!=null?W:jt,title:"",description:te}}})}},j=m.exports.useCallback(async()=>{await v({variables:{input:{refId:e.id,status:ke.DELETED}}})},[e]);return g(k,{w:"full",children:[t(Ee,{gap:4,children:g(D,{justifyContent:"space-between",flexGrow:1,alignItems:"flex-start",children:[t(jo,{avatar:l,name:o,meta:f,avatarSize:10}),I&&a&&g(_t,{children:[t(Ot,{as:tn,variant:"ghost",icon:t(Ba,{fontSize:24})}),g(Ft,{children:[t(Ze,{icon:t(we,{as:za,w:4,h:4}),onClick:()=>r(),children:"Ch\u1EC9nh s\u1EEDa"}),t(Ze,{icon:t(en,{}),onClick:j,children:"X\xF3a b\xECnh lu\u1EADn"})]})]})]})}),t(k,{mt:2,w:"full",children:s?g(k,{mt:4,w:"full",children:[t(k,{w:"full",className:"reply-editor",mt:2,children:t(St,{ref:u,showWordCount:!1,placeholder:"N\u1ED9i dung ph\u1EA3n h\u1ED3i ticket...",defaultValue:$})}),g(D,{mt:2,children:[t(Q,{colorScheme:"orange",size:"sm",onClick:_,isLoading:S,children:"C\u1EADp nh\u1EADt"}),t(Q,{colorScheme:"gray",onClick:()=>r(),size:"sm",children:"H\u1EE7y"})]})]}):t(k,{mt:2,overflow:"auto",w:"full",children:t(Ho,{borderColor:T("black","white"),dangerouslySetInnerHTML:{__html:p==null?void 0:p.content}})})})]})};var je;(function(e){e.Ok="Ok",e.NotOk="NotOk",e.Love="Love",e.Report="Report"})(je||(je={}));const mu=[{reaction:je.Love,count:0},{reaction:je.Ok,count:0},{reaction:je.NotOk,count:0},{reaction:je.Report,count:0}],fu=({bg:e,icon:n,text:a,count:s,allowVote:r})=>{const[o,l]=m.exports.useState(s);return g(D,{className:"emoji","aria-label":"vote emoji",spacing:{base:1,md:2},onClick:f=>{const{target:p}=f,d=p.closest(".emoji");d&&r&&(d.classList.add("animate-emoji"),l(o+1))},onAnimationEnd:f=>{const{target:p}=f,d=p.closest(".emoji");d&&d.classList.remove("animate-emoji")},transition:"all .2s ease",_hover:{color:e},children:[t(Ee,{bg:e,rounded:"full",width:6,height:6,d:"inline-flex",alignItems:"center",justifyContent:"center",color:"white",p:1,gap:2,className:"icon",children:t(we,{as:n,w:4,h:4})}),g(D,{as:"span",spacing:1,children:[t(k,{as:"span",d:{base:"none",md:"inline"},children:a}),g(k,{as:"span",children:["(",o,")"]})]})]})},xu=e=>{switch(e){case je.Ok:return{icon:pi,colorScheme:"green"};case je.NotOk:return{icon:ui,colorScheme:"orange"};case je.Report:return{icon:di,colorScheme:"slate"};case je.Love:return{icon:ja,colorScheme:"red"};default:return{icon:ja,colorScheme:"blue"}}},bu=({data:e,allowVote:n=!1,ticketId:a})=>{const[s,{loading:r}]=ce(cd),o=Te(),l=m.exports.useCallback(async f=>{var p;try{await s({variables:{input:{refId:a,reaction:f}}})}catch(d){o({title:"Th\u1EA5t b\u1EA1i!",description:`${(p=d==null?void 0:d.message)!=null?p:"L\u1ED7i reaction"}`,status:"error",duration:3e3,isClosable:!0})}},[a]),i=mu.map(f=>{const p=e==null?void 0:e.find(d=>d.reaction===f.reaction);return p?b({},p):b({},f)}),c=n?i:i.filter(f=>f.count>0);return c?t(D,{spacing:4,fontSize:"sm",children:c.map(f=>{const p=xu(f.reaction);return t(k,{pointerEvents:r?"none":"inherit",as:"span",role:n?"button":"text",onClick:n?()=>l(f.reaction):null,children:t(fu,{icon:p.icon,bg:`${p.colorScheme}.500`,text:f.reaction,count:f.count,allowVote:n})},f.reaction)})}):null};var ke;(function(e){e.PROCESSING="Processing",e.CLOSED="Closed",e.WAITING="Waiting",e.DELETED="Deleted"})(ke||(ke={}));const Uo=1,Ho=Da(N)`
  table {
    display: table;
    margin-left: auto;
    margin-right: auto;
    width: 100%;
    border: 1px solid #666666;
    border-spacing: 5px;
    border-collapse: collapse;
    th,
    td {
      border: ${e=>`1px solid ${e.borderColor||"black"}`};
      min-width: 100px;
      padding: 2px;
    }
    p {
      margin: 0;
    }
  }
  .custom-image {
    max-width: ${e=>`${e.maxWidthImage||"100%"}`};
    height: ${e=>`${e.heightImage||"auto"}`};
  }
`,yu=({data:e,defaultShowEditor:n=!1,onTicketUpdate:a})=>{var Le,U,ae,fe,He,Ge;const[s,r]=Et(),[o,l]=m.exports.useState(e),[i,c]=m.exports.useState(((Le=o.question)==null?void 0:Le.title)||""),[f,p]=m.exports.useState(((U=o.question)==null?void 0:U.content)||""),[d,$]=m.exports.useState([]),[y,u]=Et(!1),[h,x]=Et(n),[S,w]=Et(!1),I=m.exports.useRef(null),v=Te(),{name:R,avatar:_,time:j,status:C,meta:P,question:L,createDate:H}=o;Ue();const[W]=re(),G=parseInt(W.id,10)===((ae=e==null?void 0:e.createBy)==null?void 0:ae.id),ne=(W==null?void 0:W.roleId)===5,[te,{loading:J}]=ce(To,{onCompleted:q=>{var xe,be;const{createTicket:B}=q;o.status===ke.WAITING&&l(A(b({},o),{status:ke.PROCESSING})),$([A(b({},B),{id:B.id,name:(xe=B==null?void 0:B.createBy)==null?void 0:xe.full_name,avatar:(be=B==null?void 0:B.createBy)==null?void 0:be.avatar,status:B==null?void 0:B.status,time:(B==null?void 0:B.createDate)?ve(B.createDate).format("DD/MM/YYYY HH:mm"):"",meta:" [ZIM - 308 Tr\u1EA7n Ph\xFA] - Online - Advanced (10 h\u1ECDc vi\xEAn), 16/11, 19:30-21:30",question:{title:B==null?void 0:B.title,content:B==null?void 0:B.description},comments:[]}),...d]),v({title:"Th\xE0nh c\xF4ng!",description:"B\xECnh lu\u1EADn c\u1EE7a b\u1EA1n \u0111\xE3 \u0111\u01B0\u1EE3c g\u1EEDi.",status:"success",duration:3e3,isClosable:!0})},onError:q=>{var B;v({title:"T\u1EA1o kh\xF4ng th\xE0nh c\xF4ng!",description:`${(B=q==null?void 0:q.message)!=null?B:"L\u1ED7i"}`,status:"error",duration:3e3,isClosable:!0})}}),[de,{loading:se}]=ce(Po,{onCompleted:q=>{const{updateTicket:B}=q;l(A(b({},e),{question:{title:B==null?void 0:B.title,content:B==null?void 0:B.description}})),v({title:"Th\xE0nh c\xF4ng!",description:"C\u1EADp nh\u1EADt n\u1ED9i dung th\xE0nh c\xF4ng.",status:"success",duration:3e3,isClosable:!0})},onError:q=>{var B;v({title:"Th\u1EA5t b\u1EA1i!",description:`${(B=q==null?void 0:q.message)!=null?B:"L\u1ED7i c\u1EADp nh\u1EADt"}`,status:"error",duration:3e3,isClosable:!0})}}),[z,{loading:O}]=ce(_o,{onCompleted:q=>{const{updateTicketStatus:B}=q;B&&(l(A(b({},o),{status:B.status})),a(A(b({},o),{status:B.status})),v({title:"Th\xE0nh c\xF4ng!",description:B.status===ke.DELETED?"\u0110\xE3 x\xF3a ticket":"C\u1EADp nh\u1EADt tr\u1EA1ng th\xE1i th\xE0nh c\xF4ng.",status:"success",duration:3e3,isClosable:!0}))}}),Z=m.exports.useCallback(q=>{q.preventDefault(),x()},[]),ge=m.exports.useCallback(q=>{q.preventDefault(),u()},[]);m.exports.useCallback(q=>{q.preventDefault(),w()},[]);const ye=m.exports.useCallback(async()=>{const q=o.status;await z({variables:{input:{refId:o.id,status:q===et.Closed.value?ke.PROCESSING:ke.CLOSED}}})},[o]),Oe=m.exports.useCallback(async()=>{await z({variables:{input:{refId:o.id,status:ke.DELETED}}})},[o]),De=m.exports.useCallback(()=>{r()},[]),Y=m.exports.useCallback(q=>{const B=q.status===ke.DELETED?d.filter(xe=>xe.id!==q.id):d.map(xe=>xe.id===q.id?q:xe);$(B)},[d]),oe=async()=>{var q,B,xe;if(I.current){const be=(B=(q=I==null?void 0:I.current)==null?void 0:q.props)==null?void 0:B.editor;if(!be||!(be==null?void 0:be.getText()))return;const ue=be==null?void 0:be.getHTML();ue&&p(ue),await de({variables:{input:{refId:(xe=o==null?void 0:o.id)!=null?xe:jt,title:i,description:ue}}})}r()},Fe=async()=>{var q,B,xe,be,nt;try{if(I.current){const ue=(B=(q=I==null?void 0:I.current)==null?void 0:q.props)==null?void 0:B.editor;if(ue){const pt=ue==null?void 0:ue.getHTML();if(!(ue==null?void 0:ue.getText())&&!pt)return;await te({variables:{input:{refId:(xe=e==null?void 0:e.id)!=null?xe:jt,title:"",description:pt,receiverId:(be=e==null?void 0:e.createBy)==null?void 0:be.id}}}),(nt=ue==null?void 0:ue.commands)==null||nt.clearContent()}}}catch(ue){console.log({e:ue})}};return m.exports.useEffect(()=>{$(e.comments||[])},[e]),e?g(Go,{className:"animate__animated animate__fadeIn",children:[t(Ee,{gap:4,children:g(D,{justifyContent:"space-between",flexGrow:1,alignItems:"flex-start",children:[t(jo,{avatar:_,name:R,meta:ve(H).format("DD/MM/YYYY HH:mm"),badgeTitle:(fe=et==null?void 0:et[o==null?void 0:o.status])==null?void 0:fe.label,badgeProps:{colorScheme:(He=et==null?void 0:et[o==null?void 0:o.status])==null?void 0:He.colorScheme}}),!ne&&g(_t,{children:[t(Ot,{as:tn,variant:"ghost",icon:t(Ba,{fontSize:24})}),g(Ft,{children:[t(Ze,{icon:t(we,{as:za,w:4,h:4}),onClick:De,children:"Ch\u1EC9nh s\u1EEDa"}),t(Ze,{icon:t(we,{as:hi,w:4,h:4}),onClick:ye,children:t("span",{children:o.status!==ke.CLOSED?"\u0110\xF3ng ticket":"M\u1EDF ticket"})}),!ne&&t(Ze,{icon:t(we,{as:en,w:4,h:4}),onClick:Oe,children:"X\xF3a ticket"})]})]})]})}),g(k,{mt:4,children:[s?g(k,{mt:4,w:"full",children:[g(k,{w:"full",className:"reply-editor",mt:2,children:[t(Me,{label:"Ti\xEAu \u0111\u1EC1",value:i,onChange:q=>c(q.target.value)}),t(k,{mt:2,children:t(St,{ref:I,showWordCount:!1,placeholder:"N\u1ED9i dung ticket...",defaultValue:f})})]}),g(D,{mt:2,children:[t(Q,{colorScheme:"orange",size:"sm",onClick:oe,isLoading:se,children:"C\u1EADp nh\u1EADt"}),t(Q,{colorScheme:"gray",onClick:()=>r(),size:"sm",children:"H\u1EE7y"})]})]}):g(k,{children:[t(N,{fontWeight:600,children:(Ge=L==null?void 0:L.title)!=null?Ge:""}),t(k,{mt:2,overflow:"auto",w:"full",children:t(Ho,{borderColor:T("black","white"),dangerouslySetInnerHTML:{__html:L==null?void 0:L.content}})})]}),t(ee,{spacing:4,mx:-4,children:d&&d.map((q,B)=>y?t(k,{borderTop:"1px solid",borderColor:T("gray.300","slate.700"),mt:4,pt:4,px:4,w:"full",children:t(qo,{data:A(b({},q),{meta:ve(q.createDate).format("DD/MM/YYYY HH:mm")}),onTicketUpdate:Y,canOperationComment:!ne})},q.id):B<Uo&&t(k,{borderTop:"1px solid",borderColor:T("gray.300","slate.700"),mt:4,pt:4,px:4,w:"full",children:t(qo,{onTicketUpdate:Y,data:A(b({},q),{meta:ve(q.createDate).format("DD/MM/YYYY HH:mm")}),canOperationComment:!ne})},q.id))}),!y&&(d==null?void 0:d.length)>Uo&&g(Ve,{href:"#",color:"blue.500",d:"flex",gap:2,mt:2,alignItems:"center",onClick:ge,children:[t(k,{as:"span",className:"animate__animated animate__pulse animate__infinite",children:t(gi,{})}),t(N,{role:"button","aria-label":"load more replies",children:"Xem c\xE1c ph\u1EA3n h\u1ED3i tr\u01B0\u1EDBc..."})]}),h&&g(k,{mt:4,w:"full",className:"animate__animated animate__fadeIn",children:[t(N,{fontWeight:600,children:"Ph\u1EA3n h\u1ED3i"}),t(k,{width:"full",className:"reply-editor",mt:2,children:t(St,{ref:I,showWordCount:!1,placeholder:"N\u1ED9i dung ph\u1EA3n h\u1ED3i ticket..."})}),t(Q,{colorScheme:"blue",mt:4,mb:0,size:"sm",onClick:Fe,isLoading:J,children:"G\u1EEDi ph\u1EA3n h\u1ED3i"})]})]}),t(k,{my:4,children:t(bu,{data:e.reactions,allowVote:G,ticketId:e.id})}),g(Ee,{justifyContent:"space-between",alignItems:"center",gap:2,mt:4,flexWrap:"wrap",children:[t(D,{spacing:4,children:t(Q,{leftIcon:t(mi,{}),variant:h?"solid":"outline",size:"sm",colorScheme:"gray",onClick:Z,isDisabled:e.status===ke.CLOSED||e.status===ke.DELETED,children:"Ph\u1EA3n h\u1ED3i"})}),t(N,{color:"gray.500",fontSize:"sm",children:j||""})]})]}):null},Go=e=>{const o=e,{variant:n,children:a}=o,s=Re(o,["variant","children"]),r=fi("Card",{variant:n});return t(k,A(b({__css:r},s),{children:a}))},Eu=e=>t(xi,b({px:0,maxW:"7xl"},e)),Cp=E`
  query getTicketsPagination($input: TicketPaginationInputType)
  @api(name: "appZim") {
    getTicketsPagination(input: $input) {
      hasNextPage
      hasPrevPage
      limit
      page
      pageNext
      pagePrev
      pagingCounter
      totalPages
      totalDocs
      docs {
        ...Ticket
        comments {
          ...Ticket
        }
      }
    }
  }
  ${tt}
`;E`
  query getTicketsPagination($input: TicketPaginationInputType)
  @api(name: "appZim") {
    getTicketsPagination(input: $input) {
      hasNextPage
      hasPrevPage
      limit
      page
      pageNext
      pagePrev
      pagingCounter
      totalPages
      totalDocs
      docs {
        status
      }
    }
  }
  ${tt}
`;const Cu=E`
  query getTicketDetail($idTicket: Guid!) @api(name: "appZim") {
    getTicketDetail(idTicket: $idTicket) {
      userId
      fullName
      ticketId
      createDateTicket
      schoolAddress
      course {
        id
        name
        ec {
          id
          full_name
        }
      }
      ticket {
        ...Ticket
        comments {
          ...Ticket
        }
      }
      user {
        id
        full_name
        phone
        email
        address
        avatar
      }
    }
  }
  ${tt}
`,$u=(e,n)=>n,Vo=()=>{var h;const e=Te(),{id:n}=Ta(),s=window.location.pathname.split("/"),r=s==null?void 0:s[s.length-1],o=$u(n,r),{data:l,loading:i}=he(Cu,{variables:{idTicket:o},onError:x=>{var S;e({title:"L\u1EA5y d\u1EEF li\u1EC7u th\u1EA5t b\u1EA1i.",description:(S=x==null?void 0:x.message)!=null?S:"L\u1EA5y d\u1EEF li\u1EC7u kh\xF4ng th\xE0nh c\xF4ng",status:"error",duration:3e3,isClosable:!0})},skip:!o}),c=l==null?void 0:l.getTicketDetail,f=c==null?void 0:c.ticket,p=f==null?void 0:f.createBy,$=(p==null?void 0:p.role_id)===5?p:f==null?void 0:f.receiver,y=m.exports.useMemo(()=>{if(!f)return null;const x=S=>{var w,I,v;return S?A(b({},S),{name:(w=S==null?void 0:S.createBy)==null?void 0:w.full_name,avatar:(I=S==null?void 0:S.createBy)==null?void 0:I.avatar,question:{title:S==null?void 0:S.title,content:(S==null?void 0:S.description)||""},comments:((v=S.comments)==null?void 0:v.map(x))||[]}):{}};return b({},x(f))},[f]),u=m.exports.useMemo(()=>{var x;return[{icon:bi,value:(x=$==null?void 0:$.full_name)!=null?x:"Ch\u01B0a c\xF3 th\xF4ng tin"},{icon:yi,value:($==null?void 0:$.phone)||"Ch\u01B0a c\xF3 th\xF4ng tin"},{icon:Ei,value:($==null?void 0:$.email)||"Ch\u01B0a c\xF3 th\xF4ng tin"},{icon:Ci,value:($==null?void 0:$.address)||"Ch\u01B0a c\xF3 th\xF4ng tin"}]},[$]);return t(Eu,{py:4,children:i?t($t,{}):g(Ee,{gap:8,flexWrap:{base:"wrap",lg:"nowrap"},children:[t(k,{flexShrink:0,width:{base:"full",lg:"300px"},children:g(Go,{fontSize:"sm",children:[t($i,{size:"sm",mb:4,children:"Th\xF4ng tin h\u1ECDc vi\xEAn"}),g(Ee,{direction:"column",gap:{base:4,sm:2},children:[u.map(x=>{const{icon:S,value:w}=x;return w?g(Ee,{gap:2,alignItems:"center",children:[t(we,{w:4,h:4,as:S}),t(N,{children:w})]}):null}),(c==null?void 0:c.course)&&g(Ie,{children:[t(Si,{my:2}),g(Ee,{gap:{base:1,sm:2},flexWrap:{base:"wrap",sm:"nowrap"},children:[t(N,{fontWeight:600,w:20,flexShrink:0,children:"Trung t\xE2m:"}),t(N,{children:c==null?void 0:c.schoolAddress})]}),g(Ee,{gap:{base:1,sm:2},flexWrap:{base:"wrap",sm:"nowrap"},children:[t(N,{fontWeight:600,w:20,flexShrink:0,children:"Kh\xF3a h\u1ECDc:"}),t(N,{children:(h=c==null?void 0:c.course)==null?void 0:h.name})]}),g(Ee,{gap:{base:1,sm:2},flexWrap:{base:"wrap",sm:"nowrap"},children:[t(N,{fontWeight:600,w:20,flexShrink:0,children:"EC ch\u1EE7 nhi\u1EC7m:"}),t(N,{children:$.supporter})]})]})]})]})}),t(k,{flexGrow:1,minWidth:0,children:y&&t(yu,{data:y,defaultShowEditor:!0})})]})})};var Su=Object.freeze({__proto__:null,[Symbol.toStringTag]:"Module",default:Vo});Ce(()=>$e(()=>import("./index2.js"),["assets/index2.js","assets/vendor.js"]));Ce(()=>$e(()=>import("./index3.js"),["assets/index3.js","assets/vendor.js"]));function Au(){const[e,n]=m.exports.useState(!0),{appSetLogin:a,appSetLogout:s,appState:r}=rl(),o=sl(),{loaded:l}=r;return m.exports.useEffect(()=>{!o()||hn().then(i=>{{a(Za("token")),Object.values(global.wsClients).forEach(c=>{c.connect()});return}}).catch(i=>{console.log({e:i}),s(),n(!1)}).finally(()=>{n(!1)})},[o()]),t(Ai,{children:e||!l?t(D,{w:"100%",h:"100vh",justifyContent:"center",alignItems:"center",children:t($t,{text:"\u0110ang l\u1EA5y d\u1EEF li\u1EC7u..."})}):t(ki,{children:t(vi,{path:"*",element:t(Vo,{})})})})}const ku={secondary:"#c89934",primary:"#3463c8",gray:{50:"#F8F8F8",100:"#F0F0F0",200:"#E6E6E6",300:"#D5D5D5",400:"#B0B0B0",500:"#909090",600:"#686868",700:"#555555",800:"#373737",900:"#171717"},secondBrand:{50:"#f8fae8",100:"#eff2c5",200:"#e5e9a0",300:"#dce07d",400:"#d6da66",500:"#d1d551",600:"#cfc449",700:"#ccae3f",800:"#c89934",900:"#c07722"},brand:{50:"#e4f2ff",100:"#bedeff",200:"#94caff",300:"#6bb5ff",400:"#4fa4ff",500:"#3463c8",600:"#2e44a8",700:"#243b9c",800:"#1b3190",900:"#081f7c"},pink:{50:"#f9e4ec",100:"#f2bcd0",200:"#eb92b1",300:"#e36993",400:"#dd4d7c",500:"#d93767",600:"#c83463",700:"#b2305d",800:"#9d2c58",900:"#77264c"},blue:{50:"#E1F2FC",100:"#B7DDFA",200:"#88C8F7",300:"#55B3F4",400:"#21A3F3",500:"#0093F1",600:"#0085E4",700:"#0074D1",800:"#0063BF",900:"#0045A1"},green:{50:"#E8F5E9",100:"#C8E6C9",200:"#A5D6A7",300:"#81C784",400:"#66BB6A",500:"#4CAF50",600:"#43A047",700:"#388E3C",800:"#2E7D32",900:"#1B5E20"},red:{50:"#FFEAED",100:"#FFCBD5",200:"#F1959E",300:"#E9686B",400:"#F54258",500:"#FC243E",600:"#EC163D",700:"#DA0037",800:"#CD002F",900:"#BF0022"},slate:{50:"#f8fafc",100:"#f1f5f9",200:"#e2e8f0",300:"#cbd5e1",400:"#94a3b8",500:"#64748b",600:"#475569",700:"#334155",800:"#1e293b",900:"#0f172a"},orange:{50:"#FFF4E2",100:"#FFE1B6",200:"#FFCE88",300:"#FFBA5A",400:"#FFAB3A",500:"#FF9C27",600:"#FF9125",700:"#FA8123",800:"#F37121",900:"#E9581E"}},vu={baseStyle:({colorMode:e})=>{const n=e==="light";return{display:"block",background:n?"white":"slate.800",gap:6,border:"1px solid",borderColor:n?"gray.200":"slate.700"}},variants:{rounded:{padding:8,borderRadius:"xl"},smooth:{padding:4,borderRadius:"base"}},defaultProps:{variant:"smooth"}},wu={baseStyle:({colorMode:e})=>{const n=e==="dark";return{table:{th:{background:n?"slate.700":"gray.50",lineHeight:1.5},td:{background:n?"slate.800":"white",lineHeight:1.5}}}},variants:{simple:({colorMode:e})=>{const a=e==="dark"?"slate.700":"gray.300";return{td:{borderBottomColor:a,paddingTop:2,paddingBottom:2,paddingInlineStart:4,paddingInlineEnd:4},th:{borderBottomColor:a,paddingInlineStart:4,paddingInlineEnd:4,paddingTop:2,paddingBottom:2}}}},defaultProps:{variant:"simple",size:"sm"}},Iu={baseStyle:({colorMode:e})=>({textTransform:"capitalize",padding:"0.25rem 0.5rem ",borderRadius:4}),variants:{},defaultProps:{variant:"subtle",size:"sm"}},Tu={baseStyle:{_invalid:{boxShadow:0}},variants:{outline:{display:"none"}},defaultProps:{variant:"outline"}},Pu={baseStyle:{_hover:{textDecoration:"none",color:"brand.500"}}},_u={baseStyle:({colorMode:e})=>({dialog:{background:e==="light"?"white":"slate.800"}})},Ou={baseStyle:({colorMode:e})=>({dialog:{background:e==="light"?"white":"slate.800"}})};var Fu={Card:vu,Table:wu,Badge:Iu,Input:Tu,Link:Pu,Modal:_u,Drawer:Ou};const Lu={initialColorMode:"light",useSystemColorMode:!1},Nu={sm:"320px",md:"768px",lg:"960px",xl:"1280px","2xl":"1536px"},Wo=wi({config:Lu,colors:ku,breakpoints:Nu,styles:{global:e=>({"html, body":{color:e.colorMode==="dark"?"gray.100":"gray.900",lineHeight:1.5},a:{color:e.colorMode==="dark"?"blue.300":"blue.500"},ul:{paddingRight:1}})},semanticTokens:{text:{default:"gray.900",_dark:"slate.400"},colors:{error:{default:"red.500",_dark:"red.200"},"gray.500":{default:"gray.500",_dark:"slate.400"}}},fonts:{body:"'Roboto', sans-serif",heading:"'Arial', sans-serif",mono:"monospace"},components:A(b({},Fu),{FormLabel:{baseStyle:{fontSize:"sm",marginBottom:1}}})});Ii.render(t(at.StrictMode,{children:t(ol,{children:g(Ti,{theme:Wo,children:[t(Pi,{initialColorMode:Wo.config.initialColorMode}),t(Au,{})]})})}),document.getElementById("root"));export{Zu as $,fp as A,Bu as B,Go as C,tp as D,ep as E,cp as F,Du as G,Ho as H,eo as I,bp as J,yp as K,$t as L,sp as M,il as N,ap as O,xp as P,Lo as Q,hp as R,Gt as S,Me as T,Ut as U,wd as V,pp as W,Yu as X,Qu as Y,Uu as Z,$e as _,Et as a,Wu as a0,Vu as a1,Io as a2,Id as a3,zu as a4,gp as a5,mp as a6,Fo as a7,Od as a8,Di as a9,St as aa,up as ab,ju as ac,Gu as b,op as c,qu as d,Hu as e,$o as f,$u as g,Ku as h,Ne as i,np as j,Xu as k,rp as l,dp as m,re as n,Za as o,gl as p,et as q,Ep as r,Cp as s,ip as t,sl as u,Ue as v,Ju as w,lp as x,pd as y,Eu as z};
