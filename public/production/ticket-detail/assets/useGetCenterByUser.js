import{g as e,j as n,Z as m,l as h,H as S,dH as E,J as _,bD as g,h as T}from"./vendor.js";import{A as p,a8 as $,p as y}from"./main.js";import{n as I}from"./index.js";const C={getStaffSchool:(t="")=>{p.defaults.baseURL=$.baseURL;const s=`${y.STAFF_SCHOOL}?staffId=${t}`;return p.get(s)}},A=e`
  query ($type: EnumShiftsType) @api(name: "appZim") {
    getShiftRegisterSchedule(type: $type) {
      id
      start_time
      end_time
      name
    }
  }
`;e`
  query shifts($q: String, $status: String, $shift_minute: Int)
  @api(name: "appZim") {
    shifts(q: $q, status: $status, shift_minute: $shift_minute) {
      id
      start_time
      end_time
    }
  }
`;const U=e`
  query getScheduleDetail(
    $registerScheduleId: Int!
    $teacherId: CustomUserInputType
  ) @api(name: "appZim") {
    getScheduleDetail(
      registerScheduleId: $registerScheduleId
      teacherId: $teacherId
    ) {
      id
      type
      date
      schoolId
      startTime
      endTime
      students {
        id
        full_name
      }
      teacher {
        id
        full_name
      }
      eC {
        id
        full_name
      }
      linkMeeting
      description
    }
  }
`,F=e`
  mutation createRegisterSchedule($input: RegisterScheduleInputCreateType)
  @api(name: "appZim") {
    createRegisterSchedule(input: $input) {
      success
      error
      message
      dataString
    }
  }
`,q=e`
  mutation cancelRegisterSchedule($input: CancelRegisterInputType)
  @api(name: "appZim") {
    cancelRegisterSchedule(input: $input) {
      success
      error
      message
    }
  }
`,G=e`
  mutation createOrUpdateRegisterScheduleDetail(
    $input: CreateRegisterScheduleDetailInputType
  ) @api(name: "appZim") {
    createOrUpdateRegisterScheduleDetail(input: $input) {
      success
      error
      message
    }
  }
`,H=e`
  mutation approveOrRejectRequestCancel(
    $input: RejectRequestCancelScheduleInputType
  ) @api(name: "appZim") {
    approveOrRejectRequestCancel(input: $input) {
      success
      message
      dataString
    }
  }
`,Z=e`
  mutation changeScheduleForTeacher($input: ChangeScheduleForTeacherInputType)
  @api(name: "appZim") {
    changeScheduleForTeacher(input: $input) {
      success
      error
      message
    }
  }
`,O=e`
  mutation cancelScheduleDetail($input: ScheduleDetailInputType)
  @api(name: "appZim") {
    cancelScheduleDetail(input: $input) {
      message
      success
      dataString
      error
    }
  }
`;e`
  mutation copyRegisterSchedule($input: CopyRegisterScheduleInputType)
  @api(name: "appZim") {
    copyRegisterSchedule(input: $input) {
      success
      message
    }
  }
`;const x=e`
  mutation reopenScheduleDetail($input: ReopenScheduleDetailInputType)
  @api(name: "appZim") {
    reopenScheduleDetail(input: $input) {
      success
      error
    }
  }
`,P=2,B=3,N=4;var d;(function(t){t.register="register",t.function="function"})(d||(d={}));const f=e`
  fragment AllScheduleCopyType on ScheduleCopyType {
    type
    id
    configFunction {
      isAutoSchedule
    }
    date
    shift {
      id
      start_time
      end_time
    }
    teachers {
      id
      full_name
    }
    school {
      id
      name
    }
  }
`;e`
  query getTodoScheduleByEc($idEc: Int, $date: String) @api(name: "appZim") {
    getTodoScheduleByEc(idEc: $idEc, date: $date) {
      id
      type
      content
      customer {
        id
        role_id
        user_name
        full_name
        avatar
      }
      ec {
        id
        user_name
        full_name
        avatar
      }
      instructor {
        id
        role_id
        user_name
        full_name
        avatar
      }
      file_attached
      link
      start_time
      end_time
      date
      is_delete
      create_by
      create_date
      update_by
      update_date
    }
  }
`;const b=e`
  query getConfigFunctionByType($type: EnumConfigFunctionType!)
  @api(name: "appZim") {
    getConfigFunctionByType(type: $type) {
      id
      name
      type
      description
      timePerShift
      color
      isAutoSchedule
    }
  }
`,j=e`
  query getListRegisterSchedule($fromDate: String, $toDate: String)
  @api(name: "appZim") {
    getListRegisterSchedule(fromDate: $fromDate, toDate: $toDate) {
      id
      type
      date
      shift {
        id
        start_time
        end_time
      }
      isFull
      requestsCancel {
        teacherId
        id
        reason
      }
      school {
        id
        name
      }
    }
  }
`,v=e`
  query ($teacherId: CustomUserInputType!) @api(name: "appZim") {
    getFunctionAssignedTeacher(teacherId: $teacherId) {
      id
      name
      color
      isAutoSchedule
      shiftType
      needToCenter
    }
  }
`,k=e`
  query getListRegisterSchedule(
    $fromDate: String
    $toDate: String
    $type: Int
    $schoolId: Int
  ) @api(name: "appZim") {
    getListRegisterSchedule(
      fromDate: $fromDate
      toDate: $toDate
      type: $type
      schoolId: $schoolId
    ) {
      id
      type
      date
      shift {
        id
        start_time
        end_time
        name
      }
      school {
        id
        name
      }
      teachers {
        id
        full_name
      }
      requestsCancel {
        teacherId
        id
        reason
      }
      configFunction {
        isAutoSchedule
        timePerShift
        id
        color
        name
      }
      isFull
    }
  }
`;e`
  query checkListCopySchedule($input: CopyRegisterScheduleInputType)
  @api(name: "appZim") {
    checkListCopySchedule(input: $input) {
      scheduleConflict {
        ...AllScheduleCopyType
      }
      scheduleApprove {
        ...AllScheduleCopyType
      }
    }
  }
  ${f}
`;const Y=e`
  query getListScheduleByStatus(
    $fromDate: String
    $toDate: String
    $type: Int
    $teacherId: CustomUserInputType
    $schoolId: Int
    $status: EnumBaseStatus
  ) @api(name: "appZim") {
    getListScheduleByStatus(
      fromDate: $fromDate
      toDate: $toDate
      type: $type
      teacherId: $teacherId
      schoolId: $schoolId
      status: $status
    ) {
      id
      type
      date
      configFunction {
        id
        isAutoSchedule
      }
      teacher {
        id
        full_name
      }
      students {
        id
        full_name
      }
      startTime
      endTime
      description
      roomId
      linkMeeting
    }
  }
`,w=e`
  query getRequestCancelByStatus(
    $fromDate: String
    $toDate: String
    $type: Int
    $teacherId: CustomUserInputType
    $schoolId: Int
    $status: EnumRequestCancelStatus
  ) @api(name: "appZim") {
    getRequestCancelByStatus(
      fromDate: $fromDate
      toDate: $toDate
      type: $type
      teacherId: $teacherId
      schoolId: $schoolId
      status: $status
    ) {
      id
      type
      date
    }
  }
`,M=({typeRegister:t})=>(t==null?void 0:t.length)?n(m,{columns:{xl:4,base:1},maxW:"xl",rowGap:2,columnGap:{xl:6,base:0},children:t.map(s=>h(S,{spacing:2,children:[n(E,{size:3,bg:`${s.color}.500`}),n(_,{whiteSpace:"nowrap",children:s.name})]},s.id))}):null,J=({onCompleted:t=()=>{}}={})=>{const s=g(),[i]=I();T.exports.useEffect(()=>{(async()=>{var c,u,r;if(!!i)try{const{data:a}=await C.getStaffSchool(i==null?void 0:i.id);if((a==null?void 0:a.length)>0){const l=a.map(o=>({label:o.SchoolName,value:o.ID}));t(l)}}catch(a){console.log({e:a}),s({title:"Failed",description:(r=(u=(c=a==null?void 0:a.response)==null?void 0:c.data)==null?void 0:u.message)!=null?r:"L\u1EA5y danh s\xE1ch trung t\xE2m kh\xF4ng th\xE0nh c\xF4ng !!",status:"error",duration:3e3,isClosable:!0})}})()},[i])};export{H as A,F as C,d as E,A as G,N as O,P,x as R,B as S,q as a,U as b,b as c,v as d,j as e,M as f,Z as g,O as h,k as i,Y as j,w as k,G as l,J as u};
