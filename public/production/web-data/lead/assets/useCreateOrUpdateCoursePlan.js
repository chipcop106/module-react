var dn=Object.defineProperty,En=Object.defineProperties;var hn=Object.getOwnPropertyDescriptors;var Ze=Object.getOwnPropertySymbols;var Zt=Object.prototype.hasOwnProperty,Yt=Object.prototype.propertyIsEnumerable;var Kt=(s,e,u)=>e in s?dn(s,e,{enumerable:!0,configurable:!0,writable:!0,value:u}):s[e]=u,l=(s,e)=>{for(var u in e||(e={}))Zt.call(e,u)&&Kt(s,u,e[u]);if(Ze)for(var u of Ze(e))Yt.call(e,u)&&Kt(s,u,e[u]);return s},C=(s,e)=>En(s,hn(e));var qe=(s,e)=>{var u={};for(var i in s)Zt.call(s,i)&&e.indexOf(i)<0&&(u[i]=s[i]);if(s!=null&&Ze)for(var i of Ze(s))e.indexOf(i)<0&&Yt.call(s,i)&&(u[i]=s[i]);return u};import{g as p,a2 as D,av as y,h as _,m as E,U as nt,j as n,X as it,_ as pn,Y as _n,u as X,e6 as mn,Z as gn,e7 as bn,e1 as x,e3 as Ee,e4 as Cn,cr as yn,ay as Jt,ct as Xt,F as he,B as S,V as Qt,w as pe,c7 as A,af as st,b7 as $e,H as _e,L as Ge,cR as xn,aw as Wt,cq as $,q as v,ap as ut,cJ as fe,cv as vn,cw as Sn,bH as An,d5 as $n,a0 as at,aK as fn,aB as Fn,aC as Tn,aD as Ln,aE as In,aF as On,aG as Dn,e8 as wn,e9 as Ye}from"./vendor.js";import{o as ot,p as kn,q as Pn,s as Bn,t as Vt,v as Rn,w as L,D as Nn,g as k,j as Mn,x as lt,E as qn,A as Ke,y as Gn,z as Un,B as zn,b as Fe,c as Te,d as ae,f as en,N as Hn,F as jn,L as Zn}from"./index.js";import"./dateAddTime.js";import{E as oe,a as rt,S as Je,b as tn,c as Yn,R as le,d as Kn}from"./grapql-types.js";const Ue=(s,e=",")=>{let u=/\D+/g,i=(s+"").replace(u,"");return(Number(i)+"").replace(/(\d)(?=(\d{3})+(?!\d))/g,`$1${e}`)},ct=s=>Number(s.replaceAll(",",".").replaceAll(".","")),Zi=[{label:"Ch\xEDnh th\u1EE9c",value:oe.Course},{label:"D\u1EF1 ki\u1EBFn",value:oe.CoursePlan}],ze=[{label:"Online",value:rt.Online},{label:"Classroom",value:rt.ClassRoom}],Yi=[{label:"In coming",value:ot.INCOMING},{label:"On coming",value:ot.ONGOING},{label:"Closed",value:ot.CLOSED}],Jn=[{label:"Th\u1EE9 2",value:1},{label:"Th\u1EE9 3",value:2},{label:"Th\u1EE9 4",value:3},{label:"Th\u1EE9 5",value:4},{label:"Th\u1EE9 6",value:5},{label:"Th\u1EE9 7",value:6},{label:"Ch\u1EE7 nh\u1EADt",value:0}],Xn=[{id:1,label:"3 bu\u1ED5i/tu\u1EA7n",value:1},{id:2,label:"4-6 bu\u1ED5i/tu\u1EA7n",value:2},{id:3,label:"Tr\xEAn 6 bu\u1ED5i/tu\u1EA7n",value:3}],Ki=[{label:"5 rows",value:5},{label:"10 rows",value:10},{label:"15 rows",value:15},{label:"20 rows",value:20},{label:"25 rows",value:25},{label:"30 rows",value:30},{label:"50 rows",value:50},{label:"100 rows",value:100}];p`
  query schools($city_id: Int, $status: String) @api(name: "appZim") {
    schools(city_id: $city_id, status: $status) {
      id
      city {
        id
      }
      district {
        id
      }
      name
      address
      phone
      color_code
      lat
      lon
      status
    }
  }
`;p`
  query cats @api(name: "appZim") {
    cats {
      id
      name
    }
  }
`;const Qn=p`
  query subjects($name: String, $status: StatusEnum) @api(name: "appZim") {
    subjects(name: $name, status: $status) {
      id
      name
      status
      cats {
        id
        name
        status
        subject {
          id
          name
          status
        }
      }
    }
  }
`,Wn=p`
  query subcatsBySubjectId($subject_id: Int!) @api(name: "appZim") {
    subcatsBySubjectId(subject_id: $subject_id) {
      id
      name
      status
      cat_id
    }
  }
`,Vn=p`
  query curriculums($name: String, $status: StatusEnum, $program_id: Int)
  @api(name: "appZim") {
    curriculums(name: $name, status: $status, program_id: $program_id) {
      id
      name
      status
      shift_minute
      total_lesson
    }
  }
`,ei=p`
  query shifts($q: String, $status: String, $shift_minute: Int)
  @api(name: "appZim") {
    shifts(q: $q, status: $status, shift_minute: $shift_minute) {
      id
      status
      shift_minute
      start_time
      end_time
    }
  }
`,ti=p`
  query sizes($q: String, $status: String) @api(name: "appZim") {
    sizes(q: $q, status: $status) {
      id
      status
      name
      size
    }
  }
`,ni=p`
  query levels(
    $subject_id: Int!
    $graduation: Float!
    $status: EnumLevelStatus
    $name: String
    $type: String
  ) @api(name: "appZim") {
    levels(
      subject_id: $subject_id
      graduation: $graduation
      status: $status
      name: $name
      type: $type
    ) {
      id
      name
      status
      graduation
    }
  }
`,ii=(s,e={})=>{const u=y();return D(Qn,l({variables:s,onError:i=>{u({title:"L\u1EA5y d\u1EEF li\u1EC7u th\u1EA5t b\u1EA1i.",description:`GET list subject status error: ${i==null?void 0:i.message}`,status:"error",duration:3e3,isClosable:!0})},fetchPolicy:"cache-first",errorPolicy:"all"},e))},si=(s,e={})=>{const u=y();return D(kn,l({variables:l({status:"active"},s),onError:i=>{u({title:"L\u1EA5y d\u1EEF li\u1EC7u th\u1EA5t b\u1EA1i.",description:`GET list rooms status error: ${i==null?void 0:i.message}`,status:"error",duration:3e3,isClosable:!0})},fetchPolicy:"cache-first",errorPolicy:"all"},e))},ui=(s={},e={})=>{const u=y();return D(Pn,l({variables:s,onError:i=>{u({title:"L\u1EA5y d\u1EEF li\u1EC7u th\u1EA5t b\u1EA1i.",description:`GET list EC status error: ${i==null?void 0:i.message}`,status:"error",duration:3e3,isClosable:!0})},fetchPolicy:"cache-first",errorPolicy:"all"},e))},ai=(s,e={})=>{const u=y();return D(Vn,l({variables:s,onError:i=>{u({title:"L\u1EA5y d\u1EEF li\u1EC7u th\u1EA5t b\u1EA1i.",description:`GET list curriculums status error: ${i==null?void 0:i.message}`,status:"error",duration:3e3,isClosable:!0})},fetchPolicy:"cache-first",errorPolicy:"all"},e))},oi=(s,e={})=>{const u=y();return D(ei,l({variables:s,onError:i=>{u({title:"L\u1EA5y d\u1EEF li\u1EC7u th\u1EA5t b\u1EA1i.",description:`GET list shifts status error: ${i==null?void 0:i.message}`,status:"error",duration:3e3,isClosable:!0})},fetchPolicy:"cache-first",errorPolicy:"all"},e))},li=(s,e={})=>{const u=y();return D(Wn,l({variables:s,onError:i=>{u({title:"L\u1EA5y d\u1EEF li\u1EC7u th\u1EA5t b\u1EA1i.",description:`GET list sub cats status error: ${i==null?void 0:i.message}`,status:"error",duration:3e3,isClosable:!0})},fetchPolicy:"cache-first",errorPolicy:"all"},e))},ri=(s,e={})=>{const u=y();return D(ti,l({variables:s,onError:i=>{u({title:"L\u1EA5y d\u1EEF li\u1EC7u th\u1EA5t b\u1EA1i.",description:`GET list sizes status error: ${i==null?void 0:i.message}`,status:"error",duration:3e3,isClosable:!0})},fetchPolicy:"cache-first",errorPolicy:"all"},e))},nn=(s,e={})=>{const u=y();return D(ni,l({variables:s,onError:i=>{u({title:"L\u1EA5y d\u1EEF li\u1EC7u th\u1EA5t b\u1EA1i.",description:`GET list levels status error: ${i==null?void 0:i.message}`,status:"error",duration:3e3,isClosable:!0})},fetchPolicy:"cache-first",errorPolicy:"all"},e))},re=s=>({hour:parseInt(s.split(":")[0]),minute:parseInt(s.split(":")[1])}),ci=(s,e)=>{let u=!1;if(!e||!s)return u;const i=new Date().setHours(re(s.start_time).hour,re(s.start_time).minute),o=new Date().setHours(re(s.end_time).hour,re(s.end_time).minute);return e&&e.map(d=>{const f=new Date().setHours(re(d.start_time).hour,re(d.start_time).minute),b=new Date().setHours(re(d.end_time).hour,re(d.end_time).minute);(f<=i&&i<=b||f<=o&&o<=b)&&(u=!0)}),u},di=(s,e={})=>{const u=y();return D(Bn,l({variables:l({},s),onError:i=>{u({title:"L\u1EA5y d\u1EEF li\u1EC7u th\u1EA5t b\u1EA1i.",description:`GET list teachers status error: ${i==null?void 0:i.message}`,status:"error",duration:3e3,isClosable:!0})},fetchPolicy:"cache-first",errorPolicy:"all"},e))},Ei=({schoolWatch:s,subjectWatch:e,shiftsWatch:u,levelInWatch:i,levelOutWatch:o,skillsWatch:d,typeWatch:f,configFeeWatch:b},g,F)=>{var c,w,z,H;const{data:Q,loading:ee}=Vt(),{data:O,loading:j}=ii({status:Je.Active}),{data:a,loading:G}=si({status:"active",school_id:s==null?void 0:s.value},{skip:!(s==null?void 0:s.value)}),{data:Z,loading:Ie}=ui({schoolId:s==null?void 0:s.value},{skip:!(s==null?void 0:s.value)}),{data:P,loading:W}=di({input:{type:(c=f==null?void 0:f.value)!=null?c:rt.All,page:1,limit:99999}}),{data:te,loading:Oe}=ai({status:Je.Active}),{data:m,loading:ce}=oi({status:Je.Active}),{data:Y,loading:be}=li({subject_id:(w=e==null?void 0:e.value)!=null?w:0},{onCompleted:({subcatsBySubjectId:r})=>{if(r)if(d){const ie=r.filter(K=>d.find(V=>V.value===K.id));if(ie.length>0){const K=b.filter(V=>ie.find(Re=>Re.id===V.detail_id));F("skills",d),F("config_fee",K)}else F("skills",[]),F("config_fee",[])}else F("levelIn",[])},skip:!(e==null?void 0:e.value)}),{data:U,loading:de}=ri({status:Je.Active}),{data:M,loading:ne}=nn({subject_id:(z=e==null?void 0:e.value)!=null?z:0,graduation:0,type:"in",status:tn.Active},{onCompleted:({levels:r})=>{r&&(i&&r.find(K=>K.id===i.value)?F("levelIn",i):F("levelIn",null))},skip:!(e==null?void 0:e.value)}),{data:q,loading:De}=nn({subject_id:(H=e==null?void 0:e.value)!=null?H:0,graduation:0,type:"out",status:tn.Active},{onCompleted:({levels:r})=>{r&&(o&&r.find(K=>K.id===o.value)?F("levelOut",o):F("levelOut",null))},skip:!(e==null?void 0:e.value)}),we=_.exports.useMemo(()=>O&&O.subjects?O.subjects.map(r=>({label:r.name,value:r.id})):[],[O]),Ce=_.exports.useMemo(()=>a&&a.rooms?a.rooms.map(r=>({label:r.name,value:r.id})):[],[a]),ke=_.exports.useMemo(()=>Z&&Z.getListEcForCenter?Z.getListEcForCenter.map(r=>({label:r.full_name,value:r.id})):[],[Z]),B=_.exports.useMemo(()=>P&&P.getListTeacherPagination?P.getListTeacherPagination.docs.map(r=>({label:r.full_name,value:r.id})):[],[P]),Pe=_.exports.useMemo(()=>te&&te.curriculums?te.curriculums.map(r=>({label:r.name,value:r.id})):[],[te]),ye=_.exports.useMemo(()=>U&&U.sizes?U.sizes.map(r=>({label:r.size,value:r.id})):[],[U]),N=_.exports.useMemo(()=>m&&m.shifts?m.shifts.map(r=>((m==null?void 0:m.shifts)&&ci(r,u),C(l({},r),{label:`${r==null?void 0:r.start_time} - ${r==null?void 0:r.end_time}`,value:r.id,isDisabled:!1}))):[],[m,u]),Be=_.exports.useMemo(()=>M&&M.levels?M.levels.map(r=>({label:r.graduation,value:r.id})):[],[M]),xe=_.exports.useMemo(()=>q&&q.levels?q.levels.map(r=>({label:r.graduation,value:r.id})):[],[q]),ve=_.exports.useMemo(()=>Q&&Q.schools?Q.schools.map(r=>({label:r.name,value:r.id})):[],[Q]);return{options:{subcatOptions:_.exports.useMemo(()=>Y&&Y.subcatsBySubjectId?Y.subcatsBySubjectId.map(r=>({label:r.name,value:r.id})):[],[Y]),schoolOptions:ve,levelOutOptions:xe,levelInOptions:Be,shiftOptions:N,sizeOptions:ye,curriculumOptions:Pe,ecOptions:ke,roomOptions:Ce,subjectOptions:we,teacherOptions:B},loadings:{ecLoading:Ie,levelOutLoading:De,levelInLoading:ne,subcatsLoading:be,subjectLoading:j,roomLoading:G,sizeLoading:de,curriculumLoading:Oe,shiftLoading:ce,schoolLoading:ee,teacherLoading:W}}},hi=p`
  query getConfigSystemByName($name: EnumConfigSystemNames)
  @api(name: "appZim") {
    getConfigSystemByName(name: $name) {
      name
      value
      description
    }
  }
`,pi=(s,e={})=>{const u=y();return D(hi,l({variables:s,onError:i=>{var o;u({title:"L\u1EA5y d\u1EEF li\u1EC7u th\u1EA5t b\u1EA1i.",description:`GET config ${(o=s==null?void 0:s.name)!=null?o:""} status error: ${i==null?void 0:i.message}`,status:"error",duration:3e3,isClosable:!0})},fetchPolicy:"network-only",errorPolicy:"all"},e))},_i=_.exports.forwardRef((i,u)=>{var o=i,{addonValue:s}=o,e=qe(o,["addonValue"]);var d;return E(nt,{isInvalid:e.isInvalid,children:[n(it,{htmlFor:e.id,fontSize:"sm",color:"gray.500",children:e.label}),E(pn,{children:[n(_n,C(l({bg:X("white","slate.700"),borderColor:X("gray.300","slate.600"),_hover:{borderColor:X("gray.400","slate.700")},color:X("gray.900","slate.300"),_placeholder:{color:X("gray.400","slate.500")}},e),{ref:u})),n(mn,{flexGrow:1,children:s})]}),n(gn,{children:(d=e==null?void 0:e.error)==null?void 0:d.message})]})}),mi=(s={},e={})=>{const u=y();return D(Rn,l({variables:s,onError:i=>{var o;u({title:"L\u1EA5y d\u1EEF li\u1EC7u th\u1EA5t b\u1EA1i.",description:(o=i==null?void 0:i.message)!=null?o:"L\u1EA5y d\u1EEF li\u1EC7u kh\xF4ng th\xE0nh c\xF4ng",status:"error",duration:3e3,isClosable:!0})},notifyOnNetworkStatusChange:!0},e))},me=(s,e)=>s?s.toString().replace(/\B(?=(\d{3})+(?!\d))/g,e||"."):"0",ge=7,I=u=>{var i=u,{children:s}=i,e=qe(i,["children"]);return n(S,C(l({p:2,width:{base:"100%",md:"33%",sm:"50%"}},e),{children:s}))},Le=i=>{var o=i,{children:s,title:e}=o,u=qe(o,["children","title"]);return n(S,C(l({w:"full",borderBottom:"1px solid",borderBottomColor:X("gray.500","slate.500"),pos:"relative"},u),{children:n(S,{mx:-2,pb:4,children:s})}))},gi=({onSubmit:s,defaultValues:e,isCreateMode:u=!0,courseType:i=oe.Course,formRef:o})=>{var Et,ht,pt,_t,mt,gt,bt,Ct,yt,xt,vt,St,At,$t,ft,Ft,Tt,Lt,It,Ot,Dt,wt,kt,Pt,Bt,Rt;const d=i===oe.Course,f=_.exports.useMemo(()=>x().shape({school:x().nullable(!0).required(L("School")),rooms:d?Ee().of(x()).required(L("Rooms")).min(1,L("Rooms")).nullable(!0):Ee().nullable(!0),levelIn:x().nullable(!0).when("subject",(t,T)=>t&&(t==null?void 0:t.value)===ge?x().nullable(!0):x().nullable(!0).required(L("Level out"))),levelOut:x().nullable(!0).when("subject",(t,T)=>t&&(t==null?void 0:t.value)===ge?x().nullable(!0):x().nullable(!0).required(L("Level out"))),shifts:Ee().of(x()).required(L("Shifts")).min(1,L("Shifts")).nullable(!0),skills:x().nullable(!0).when("subject",(t,T)=>t&&(t==null?void 0:t.value)===ge?Ee().of(x()).nullable(!0):Ee().of(x()).required(L("Skills")).min(1,L("Skills")).nullable(!0)),dayOfWeeks:Ee().of(x()).required(L("Day Study")).min(1,L("Day Study")).nullable(!0),subject:x().nullable(!0).required(L("Subject")),curriculum:x().nullable(!0).required(L("Curriculum")),dateOpen:Cn().nullable(!0).required(L("Date open")),size:x().nullable(!0).required(L("Size")),fee:yn().required(),type:x().nullable(!0).required(L("Course type")),ec:d?x().nullable(!0).required(L("EC")):x().nullable(!0),teachers:Ee().of(x()).required(L("Teachers")).min(1,L("Teachers")).nullable(!0),coefficientRange:x().nullable(!0).required(L("Coefficient"))}),[i]),{handleSubmit:b,control:g,setValue:F,getValues:Q,trigger:ee,register:O,resetField:j,formState:{errors:a},watch:G,setError:Z}=Jt({resolver:Xt(f),defaultValues:{subject:(e==null?void 0:e.subject)&&{label:(Et=e.subject)==null?void 0:Et.name,value:(ht=e.subject)==null?void 0:ht.id},program:(e==null?void 0:e.program)&&{label:(pt=e.program)==null?void 0:pt.name,value:(_t=e.program)==null?void 0:_t.id},rooms:(e==null?void 0:e.rooms)&&e.rooms.map(t=>({label:t==null?void 0:t.name,value:t==null?void 0:t.id})),size:(e==null?void 0:e.size)&&{label:(mt=e.size)==null?void 0:mt.name,value:(gt=e.size)==null?void 0:gt.id},shifts:(e==null?void 0:e.shifts)&&(e==null?void 0:e.shifts.map(t=>C(l({},t),{label:`${t==null?void 0:t.start_time} - ${t==null?void 0:t.end_time}`,value:t==null?void 0:t.id}))),skills:(e==null?void 0:e.subcats)&&e.subcats.map(t=>({label:t==null?void 0:t.name,value:t==null?void 0:t.id})),curriculum:(e==null?void 0:e.curriculum)&&{label:(bt=e.curriculum)==null?void 0:bt.name,value:(Ct=e.curriculum)==null?void 0:Ct.id},school:(e==null?void 0:e.school)&&{label:(yt=e.school)==null?void 0:yt.name,value:(xt=e.school)==null?void 0:xt.id},dayOfWeeks:(e==null?void 0:e.day_of_weeks)&&e.day_of_weeks.map(t=>({label:Nn[t.id].full,value:t==null?void 0:t.id})),ec:(e==null?void 0:e.ec)&&{label:(vt=e.ec)==null?void 0:vt.full_name,value:(St=e.ec)==null?void 0:St.id},levelIn:(e==null?void 0:e.level_in)&&{label:(At=e.level_in)==null?void 0:At.graduation,value:($t=e.level_in)==null?void 0:$t.id},levelOut:(e==null?void 0:e.level_out)&&{label:(ft=e.level_out)==null?void 0:ft.graduation,value:(Ft=e.level_out)==null?void 0:Ft.id},teachers:(e==null?void 0:e.teachers)&&e.teachers.map(t=>({label:t==null?void 0:t.full_name,value:t==null?void 0:t.id})),dateOpen:(e==null?void 0:e.start_date)?new Date(e==null?void 0:e.start_date):null,fee:(Tt=e==null?void 0:e.fee)!=null?Tt:"",config_fee:(e==null?void 0:e.config_fee)&&e.config_fee.map(t=>C(l({},t),{label:t==null?void 0:t.detail_name,value:t==null?void 0:t.detail_id})),type:(e==null?void 0:e.type)?ze.find(t=>t.value.toLowerCase()===e.type.toLowerCase()):(Lt=ze==null?void 0:ze[1])!=null?Lt:null,coefficientRange:(e==null?void 0:e.coefficient_range)&&{label:(It=e.coefficient_range)==null?void 0:It.name,value:(Ot=e.coefficient_range)==null?void 0:Ot.id},config_fee_month:(e==null?void 0:e.config_fee)&&(wt=(Dt=e==null?void 0:e.config_fee.find(t=>(t==null?void 0:t.type)==="forMonth"))==null?void 0:Dt.detail_id)!=null?wt:1}}),Ie=y(),[P,W,te,Oe,m,ce,Y,be,U,de,M,ne,q,De,we]=G(["school","curriculum","program","skills","subject","shifts","fee","config_fee_month","teachers","size","coefficientRange","type","config_fee","levelIn","levelOut"]),{options:{subcatOptions:Ce,schoolOptions:ke,levelOutOptions:B,levelInOptions:Pe,shiftOptions:ye,sizeOptions:N,curriculumOptions:Be,ecOptions:xe,roomOptions:ve,subjectOptions:h,teacherOptions:c},loadings:{ecLoading:w,levelOutLoading:z,levelInLoading:H,subcatsLoading:r,subjectLoading:ie,roomLoading:K,sizeLoading:V,curriculumLoading:Re,shiftLoading:Xe,schoolLoading:J,teacherLoading:Qe}}=Ei({schoolWatch:P,subjectWatch:m,shiftsWatch:ce,levelInWatch:De,levelOutWatch:we,skillsWatch:Oe,typeWatch:ne,configFeeWatch:q},j,F),{data:Se,loading:qi}=pi({name:Yn.PercentageForFeeByMonth}),{data:We,loading:an}=mi({input:{coefficient_range_id:(kt=M==null?void 0:M.value)!=null?kt:0,curriculum_id:(Pt=W==null?void 0:W.value)!=null?Pt:0,subject_id:(Bt=m==null?void 0:m.value)!=null?Bt:0,size_id:(Rt=de==null?void 0:de.value)!=null?Rt:0,shift_ids:ce?ce.map(t=>t.value):[],teacher_ids:U?U.map(t=>t.value):[],type:ne==null?void 0:ne.value}},{fetchPolicy:"network-only",skip:!M||!W||!m||!de||!ce||!U||!ne}),Ne=Se==null?void 0:Se.getConfigSystemByName,je=We==null?void 0:We.getCourseFeeSuggestion,on=_.exports.useCallback(t=>t?t.isDisabled:!1,[]),ln=_.exports.useCallback(t=>{let T=ct(Ue(Y)+""),se=isNaN(Number(t))?0:Number(t);se<=0&&(se=1);let Me=Ne?parseInt(Ne==null?void 0:Ne.value):0,ue=T;return se>1&&(ue=T*(100+Me)/100),Math.round(ue/se/1e3)*1e3},[be,Y,Ne]),rn=async t=>{var T,se,Me,ue,Nt,Mt,qt;try{let Ae=!1,Ve=[];const Gt=t.config_fee?t.config_fee.filter(R=>R.type=="forSubcat"):[],et=je/Gt.length,Ut=ct(t.fee);if(Gt.map(R=>{var Ht,jt;const zt=ct((Ht=t["config_fee_subcat_"+R.detail_id])!=null?Ht:0);((jt=t.subject)==null?void 0:jt.value)!==ge&&zt<et&&(Z(`config_fee_subcat_${R.detail_id}`,{type:"custom",message:`Ph\xED t\u1ED1i thi\u1EC3u l\xE0: ${et?me(et):0}`}),Ae=!0),Ve.push({detail_id:R.value,type:"forSubcat",detail_name:R.label,fee:zt})}),Ve.push({type:"forMonth",detail_id:Number(t.config_fee_month),fee:0,detail_name:"Fee For Month"}),je>Ut){Z("fee",{type:"custom",message:"H\u1ECDc ph\xED kh\xF4ng \u0111\u01B0\u1EE3c th\u1EA5p h\u01A1n h\u1ECDc ph\xED \u0111\u1EC1 xu\u1EA5t"});return}if(Ae)return;const tt={fee:Ut,config_fee:Ve,school_id:t==null?void 0:t.school.value,teacher_ids:(t==null?void 0:t.teachers)?t.teachers.map(R=>R.value):[],courseType:t.type.value,subcat_ids:(t==null?void 0:t.skills)?t.skills.map(R=>R.value):[],ec_id:(se=(T=t==null?void 0:t.ec)==null?void 0:T.value)!=null?se:null,day_of_week_ids:(t==null?void 0:t.dayOfWeeks)?t.dayOfWeeks.map(R=>R.value):[],level_in_id:(ue=(Me=t==null?void 0:t.levelIn)==null?void 0:Me.value)!=null?ue:0,level_out_id:(Mt=(Nt=t==null?void 0:t.levelOut)==null?void 0:Nt.value)!=null?Mt:0,room_ids:(t==null?void 0:t.rooms)?(qt=t==null?void 0:t.rooms)==null?void 0:qt.map(R=>R.value):[],shift_ids:(t==null?void 0:t.shifts)?t.shifts.map(R=>R.value):[],start_date:st(t.dateOpen).toISOString(),size_id:t.size.value,coefficient_range_id:t.coefficientRange.value,curriculum_id:t.curriculum.value,subject_id:t.subject.value};i===oe.CoursePlan&&(delete tt.ec_id,delete tt.room_ids),s(tt)}catch(Ae){Ie({title:"Failed",description:Ae==null?void 0:Ae.message,status:"error",duration:3e3,isClosable:!0})}},cn=_.exports.useCallback(t=>{F("skills",t),F("config_fee",t.map(T=>C(l({},T),{detail_id:T.value,type:"forSubcat",fee:0,detail_name:T.label})))},[]);return n(he,{children:n(S,{children:n("form",{onSubmit:b(rn),ref:o,children:E(Qt,{spacing:4,children:[n(Le,{title:"\u0110\u1ECBa \u0111i\u1EC3m",children:E(pe,{flexWrap:"wrap",w:"full",pb:{base:4,md:0},children:[n(I,{width:{base:"100%",md:"50%"},children:n(A,{name:"school",control:g,render:({field:t})=>n(k,l({label:"Trung t\xE2m",options:ke,isSearchable:!0,isLoading:J,name:"school",placeholder:"Ch\u1ECDn trung t\xE2m",id:"school",error:a==null?void 0:a.school,isDisabled:!u},t))})}),d&&n(I,{width:{base:"100%",md:"50%"},children:n(A,{name:"rooms",control:g,render:({field:t})=>n(k,C(l({label:"Ph\xF2ng h\u1ECDc",options:ve,isSearchable:!0,isLoading:K,name:"rooms",placeholder:"Ch\u1ECDn ph\xF2ng",id:"rooms",error:a==null?void 0:a.rooms},t),{isMulti:!0}))})})]})}),n(Le,{title:"Th\xF4ng tin kh\xF3a",children:E(pe,{flexWrap:"wrap",w:"full",pb:{base:4,md:0},children:[n(I,{width:{base:"100%",md:"50%",lg:"25%"},children:n(A,{name:"type",control:g,render:({field:t})=>n(k,l({label:"Lo\u1EA1i kh\xF3a h\u1ECDc",options:ze,isSearchable:!0,isLoading:!1,name:"type",placeholder:"Ch\u1ECDn lo\u1EA1i kh\xF3a h\u1ECDc",id:"type",error:a==null?void 0:a.type,isDisabled:!u},t))})}),n(I,{width:{base:"100%",md:"50%",lg:"25%"},children:n(A,{name:"size",control:g,render:({field:t})=>n(k,l({label:"S\u0129 s\u1ED1",options:N,isSearchable:!0,isLoading:V,name:"size",placeholder:"Ch\u1ECDn s\u1ED1 l\u01B0\u1EE3ng",id:"size",error:a==null?void 0:a.size,noOptionsMessage:()=>"Kh\xF4ng c\xF3 s\u1ED1 l\u01B0\u1EE3ng ph\xF9 h\u1EE3p",isDisabled:!u},t))})}),n(I,{width:{base:"100%",md:"50%"},children:n(A,{name:"subject",control:g,render:({field:t})=>n(k,C(l({label:"M\xF4n h\u1ECDc",options:h,isSearchable:!0,isLoading:ie,name:"subject",placeholder:"Ch\u1ECDn m\xF4n h\u1ECDc",id:"program",error:a==null?void 0:a.subject},t),{isDisabled:!u}))})}),n(I,{width:{base:"100%",md:"50%"},children:n(A,{name:"curriculum",control:g,render:({field:t})=>n(k,C(l({label:"L\u1ED9 tr\xECnh",options:Be,isSearchable:!0,isLoading:Re,name:"curriculum",placeholder:"Ch\u1ECDn l\u1ED9 tr\xECnh",id:"curriculum",error:a==null?void 0:a.curriculum},t),{isDisabled:!u}))})}),(m==null?void 0:m.value)!==ge&&n(I,{width:{base:"100%",md:"50%"},children:n(A,{name:"skills",control:g,render:({field:t})=>n(k,C(l({label:"K\u0129 n\u0103ng",options:Ce,isSearchable:!0,isLoading:r,name:"skills",placeholder:"Ch\u1ECDn k\u0129 n\u0103ng",id:"skills",error:a==null?void 0:a.skills},t),{isMulti:!0,isDisabled:!u,onChange:cn}))})})]})}),(m==null?void 0:m.value)!==ge&&n(Le,{title:"Tr\xECnh \u0111\u1ED9",children:E(pe,{flexWrap:"wrap",w:"full",pb:{base:4,md:0},children:[n(I,{width:{base:"100%",md:"50%"},children:n(A,{name:"levelIn",control:g,render:({field:t})=>n(k,l({label:"\u0110\u1EA7u v\xE0o",options:Pe,isSearchable:!0,isLoading:H,name:"levelIn",placeholder:"Ch\u1ECDn \u0111\u1EA7u v\xE0o",id:"levelIn",error:a==null?void 0:a.levelIn},t))})}),n(I,{width:{base:"100%",md:"50%"},children:n(A,{name:"levelOut",control:g,render:({field:t})=>n(k,l({label:"\u0110\u1EA7u ra",options:B,isSearchable:!0,isLoading:z,name:"levelOut",placeholder:"Ch\u1ECDn \u0111\u1EA7u ra",id:"levelOut",error:a==null?void 0:a.levelOut},t))})})]})}),n(Le,{title:"Th\u1EDDi gian",children:E(pe,{flexWrap:"wrap",w:"full",pb:{base:4,md:0},children:[n(I,{width:{base:"100%",md:"50%"},children:n(A,{name:"dateOpen",control:g,render:Me=>{var ue=Me,{field:{onChange:t,value:T}}=ue,se=qe(ue,["field"]);return n(Mn,{label:"Ng\xE0y m\u1EDF",selected:T,onChange:t,"data-value":T,autoComplete:"off",dateFormat:"dd/MM/yyyy",placeholderText:"Ng\xE0y b\u1EAFt \u0111\u1EA7u",disabled:!u,minDate:new Date,error:a==null?void 0:a.dateOpen})}})}),n(I,{width:{base:"100%",md:"50%"},children:n(A,{name:"shifts",control:g,render:({field:t})=>n(k,l({label:"Ca h\u1ECDc",options:ye,isSearchable:!0,isLoading:Xe,name:"shifts",placeholder:"Ch\u1ECDn ca h\u1ECDc",id:"shifts",error:a==null?void 0:a.shifts,isMulti:!0,isOptionDisabled:on},t))})}),n(I,{width:{base:"100%",md:"50%"},children:n(A,{name:"dayOfWeeks",control:g,render:({field:t})=>n(k,C(l({label:"Ng\xE0y h\u1ECDc",options:Jn,isSearchable:!0,isLoading:!1,placeholder:"Ch\u1ECDn ng\xE0y h\u1ECDc",id:"day_of_week",error:a==null?void 0:a.dayOfWeeks},t),{isMulti:!0}))})}),n(I,{width:{base:"100%",md:"50%"},children:n(A,{name:"coefficientRange",control:g,render:({field:t})=>n(k,l({label:"C\u01B0\u1EDDng \u0111\u1ED9",options:Xn,isSearchable:!0,isLoading:!1,name:"coefficientRange",placeholder:"Ch\u1ECDn",id:"coefficientRange",error:a==null?void 0:a.coefficientRange},t))})})]})}),n(Le,{title:"Nh\xE2n s\u1EF1",children:E(pe,{flexWrap:"wrap",w:"full",pb:{base:4,md:0},children:[d&&n(I,{width:{base:"100%",md:"50%"},children:n(A,{name:"ec",control:g,render:({field:t})=>n(k,l({label:"EC ch\u1EE7 nhi\u1EC7m",options:xe,isSearchable:!0,isLoading:w,name:"ec",placeholder:"Ch\u1ECDn EC",id:"ec",error:a==null?void 0:a.ec,noOptionsMessage:()=>P?"Vui l\xF2ng ch\u1ECDn trung t\xE2m tr\u01B0\u1EDBc":"Kh\xF4ng c\xF3 EC ph\xF9 h\u1EE3p"},t))})}),n(I,{width:{base:"100%",md:"50%"},children:n(A,{name:"teachers",control:g,render:({field:t})=>n(k,C(l({label:"Gi\u1EA3ng vi\xEAn d\u1EF1 ki\u1EBFn",options:c,isSearchable:!0,isLoading:Qe,placeholder:"Ch\u1ECDn gi\u1EA3ng vi\xEAn",id:"teachers",error:a==null?void 0:a.teachers,isMulti:!0},t),{isDisabled:!u&&d}))})})]})}),n(Le,{title:"H\u1ECDc ph\xED",borderBottom:0,children:E(pe,{flexWrap:"wrap",w:"full",children:[n(I,{width:{base:"100%",md:"50%",xl:"25%"},children:n(A,{control:g,render:({field:t})=>{var T;return n(lt,C(l({label:"H\u1ECDc ph\xED",placeholder:"Nh\u1EADp h\u1ECDc ph\xED",id:"fee"},t),{style:{boxShadow:"0 0 0 0px #f44336"},error:a==null?void 0:a.fee,isInvalid:!!(a==null?void 0:a.fee),value:Ue((T=t==null?void 0:t.value)!=null?T:0)}))},name:"fee"})}),n(I,{width:{base:"100%",md:"50%",xl:"25%"},children:n(lt,{label:"H\u1ECDc ph\xED \u0111\u1EC1 xu\u1EA5t",placeholder:"H\u1ECDc ph\xED \u0111\u1EC1 xu\u1EA5t",id:"suggestion-fee",style:{boxShadow:"0 0 0 0px #f44336"},error:a==null?void 0:a.fee_suggestion,isInvalid:!!(a==null?void 0:a.fee_suggestion),value:an?"\u0110ang \u0111\u1EC1 xu\u1EA5t...":je?`${Ue(je)}`:"0",isReadOnly:!0})}),n(I,{flex:1,minW:{base:"50%",md:"50%"},maxW:{base:"100%",md:"50%"},children:n(_i,C(l({label:"H\u1ECDc ph\xED theo th\xE1ng",placeholder:"Nh\u1EADp th\xE1ng",id:"config_fee_month",style:{boxShadow:"0 0 0 0px #f44336"},error:a==null?void 0:a.config_fee_month,isInvalid:!!(a==null?void 0:a.config_fee_month)},O("config_fee_month",{required:!0})),{addonValue:Ue(ln(be))+" \u0111/th\xE1ng",width:125}))}),(m==null?void 0:m.value)!==ge&&n(pe,{px:{base:0,md:4},flexGrow:1,mx:-4,flexWrap:"wrap",w:"full",children:q&&(q==null?void 0:q.filter(t=>t.type=="forSubcat").map(t=>{const T=O(`config_fee_subcat_${t.detail_id}`,{required:!0});return n(I,{flex:1,minW:{base:"100%",sm:"50%",md:"33%",lg:"25%"},maxW:{base:"100%",sm:"50%",md:"33%",lg:"25%"},children:n(lt,C(l({label:`H\u1ECDc ph\xED ${t.detail_name}`,placeholder:"Nh\u1EADp h\u1ECDc ph\xED",id:`config_fee_subcat_${t.detail_id}`,style:{boxShadow:"0 0 0 0px #f44336"},error:a==null?void 0:a[`config_fee_subcat_${t.detail_id}`],isInvalid:!!(a==null?void 0:a[`config_fee_subcat_${t.detail_id}`])},T),{value:Ue(G(`config_fee_subcat_${t.detail_id}`)||t.fee)}))},`subcat_${t.detail_id}`)}))})]})})]})})})})},bi=bn(_.exports.memo(gi),{FallbackComponent:qn});var Ji=_.exports.forwardRef((s,e)=>n(bi,C(l({},s),{formRef:e})));const Xi=p`
  mutation upsertCoursePlanerNote(
    $coursePlanerId: Int!
    $id: Int
    $note: String!
    $coursePlanerType: EnumCoursePlanerTypeEnum!
  ) @api(name: "appZim") {
    upsertCoursePlanerNote(
      coursePlanerId: $coursePlanerId
      note: $note
      id: $id
      coursePlanerType: $coursePlanerType
    ) {
      note
      createBy {
        id
        user_name
        full_name
      }
      refId
      refTable
      createDate
    }
  }
`,Qi=p`
  mutation upsertCourse($input: UpsertCourseInput) @api(name: "appZim") {
    upsertCourse(input: $input) {
      id
      name
    }
  }
`,Ci=p`
  mutation upsertCoursePlan($input: UpsertCoursePlanInput)
  @api(name: "appZim") {
    upsertCoursePlan(input: $input) {
      message
      success
      error
      coursePlan {
        id
        name
      }
    }
  }
`,yi=p`
  mutation createRequestStudentToCourse(
    $student_id: Int
    $course_id: Int
    $type: RequestStudentToCourseTypeEnumeration
    $note: String
    $new_invoice_ids: [Int]
    $old_invoice_ids: [Int]
    $refTable: EnumCoursePlanerTypeEnum
  ) @api(name: "appZim") {
    createRequestStudentToCourse(
      student_id: $student_id
      course_id: $course_id
      type: $type
      note: $note
      new_invoice_ids: $new_invoice_ids
      old_invoice_ids: $old_invoice_ids
      refTable: $refTable
    ) {
      id
      type
      status
      course {
        name
      }
      student {
        ...AccountField
      }
      note
      update_by
      update_date
      create_by
      create_date
    }
  }

  ${Ke}
`,Wi=p`
  mutation updateCourseSchedule(
    $event_id: String
    $date: String
    $lesson_id: Int
    $subject_ids: [Int]
    $course_id: Int
    $room_id: Int
    $shift_id: Int
    $teacher_id: Int
    $subcat_type_id: Int
  ) @api(name: "appZim") {
    updateCourseSchedule(
      event_id: $event_id
      date: $date
      lesson_id: $lesson_id
      subject_ids: $subject_ids
      course_id: $course_id
      room_id: $room_id
      shift_id: $shift_id
      teacher_id: $teacher_id
      subcat_type_id: $subcat_type_id
    ) {
      is_additional
      event_id
      course {
        id
        slug
      }
      lesson
      teacher_id
      teacher {
        id
        user_name
        full_name
      }
      alt_teacher_id
      date
      room {
        id
        name
      }
      shift {
        id
        start_time
        end_time
      }
      subcats {
        id
        name
      }
      status
      subcat_type_id
      subcat_type {
        id
        name
      }
      document
    }
  }
`,Vi=p`
  mutation createCourseSchedule(
    $type: String
    $date: String
    $lesson_id: Int
    $subject_ids: [Int]
    $course_id: Int
    $room_id: Int
    $shift_id: Int
    $teacher_id: Int
    $subcat_type_id: Int
  ) @api(name: "appZim") {
    createCourseSchedule(
      type: $type
      date: $date
      lesson_id: $lesson_id
      subject_ids: $subject_ids
      course_id: $course_id
      room_id: $room_id
      shift_id: $shift_id
      teacher_id: $teacher_id
      subcat_type_id: $subcat_type_id
    ) {
      is_additional
      event_id
      course {
        id
        slug
      }
      lesson
      teacher_id
      teacher {
        id
        user_name
        full_name
      }
      alt_teacher_id
      date
      room {
        id
        name
      }
      shift {
        id
        start_time
        end_time
      }
      subcats {
        id
        name
      }
      status
      subcat_type_id
      subcat_type {
        id
        name
      }
      document
    }
  }
`,es=p`
  mutation deleteCourseSchedule($event_id: String, $id: Int)
  @api(name: "appZim") {
    deleteCourseSchedule(event_id: $event_id, id: $id) {
      success
      error
      message
    }
  }
`,sn=p`
  fragment LevelField on LevelType {
    subject_id
    id
    name
    graduation
    status
    type
  }
`,xi=p`
  fragment SubjectField on SubjectType {
    id
    name
    status
    cats {
      id
      name
      create_date
      status
    }
    levels {
      ...LevelField
    }
  }
  ${sn}
`,vi=p`
  fragment ShiftField on ShiftType {
    id
    start_time
    end_time
    status
    shift_minute
  }
`,Si=p`
  fragment SchoolField on SchoolType {
    id
    city {
      id
      name
      status
    }
    district {
      id
      name
      status
    }
    name
    address
    phone
    status
  }
`,Ai=p`
  fragment CurriculumField on CurriculumType {
    id
    name
    shift_minute
    total_lesson
    status
    curriculum_details {
      id
      lesson
    }
    update_by
    update_date
  }
`,$i=p`
  query coursePlanerPagination($input: CoursePlanerPaginationInputType)
  @api(name: "appZim") {
    coursePlanerPagination(input: $input) {
      hasNextPage
      hasPrevPage
      limit
      page
      pageNext
      pagePrev
      pagingCounter
      totalPages
      totalDocs
      docs {
        id
        code
        ref_table
        start_date
        type
        fee
        total_registed
        total_new
        total_enroll
        total_paid
        percent_occupancy
        day_left
        subject {
          ...SubjectField
        }
        school {
          ...SchoolField
        }
        status
        size {
          id
          name
          size
          status
        }
        level_in {
          ...LevelField
        }
        level_out {
          ...LevelField
        }
        day_of_weeks {
          id
          name
        }
        subcats {
          id
          cat_id
          cat {
            id
            name
            create_date
            status
          }
          name
          status
          parent_id
        }
        shifts {
          ...ShiftField
        }
        curriculum {
          ...CurriculumField
        }
        ec {
          id
          full_name
        }
        teachers {
          ...AccountField
        }
        note
      }
    }
  }
  ${Ke}
  ${sn}
  ${vi}
  ${xi}
  ${Si}
  ${Ai}
`,fi=p`
  query getCoursePlanerStudents(
    $refId: Int!
    $type: EnumCoursePlanerTypeEnum!
    $studentType: String
  ) @api(name: "appZim") {
    getCoursePlanerStudents(
      refId: $refId
      type: $type
      studentType: $studentType
    ) {
      student_id
      student_name
      student_type
      ec_source_name
      ec_support_name
      invoices
      listInvoice {
        id
        student {
          id
          user_name
          full_name
          phone
          email
        }
        code
        number_invoice
        paid
        status
        payment
        qr_code
        ec_id
        ec_name
        type
        create_date
        create_by
        update_by
        update_date
        note
        appointment_date
        discount_price
        invoice_detail {
          id
          detail_id
          detail_type
          detail_name
          detail_fee
          subcat_name
        }
      }
      invoiceStudent {
        id
        student {
          id
          user_name
          full_name
          phone
          email
        }
        code
        number_invoice
        paid
        status
        payment
        qr_code
        ec_id
        ec_name
        type
        create_date
        create_by
        update_by
        update_date
        note
        appointment_date
        discount_price
        invoice_detail {
          id
          detail_id
          detail_type
          detail_name
          detail_fee
          subcat_name
        }
      }
    }
  }
`,Fi=p`
  query getAllCourseBySTudentById($studentId: Int!) @api(name: "appZim") {
    getAllCourseBySTudentById(studentId: $studentId) {
      courseNew {
        tags {
          level_name
          name
        }
        id
        slug
        name
        start_date
        school {
          id
          name
          address
          phone
          color_code
          lat
          lon
          status
        }
        curriculum {
          id
          name
          shift_minute
          total_lesson
          status
          update_by
          update_date
        }
        size {
          id
          name
          size
          status
        }
        fee
        status
        code
        rooms {
          id
          name
          status
          description
        }
        shifts {
          id
          start_time
          end_time
          status
          shift_minute
        }
        day_of_weeks {
          id
          name
        }
        ec_id
        ec {
          id
          user_name
          full_name
          phone
          address
          email
          role_id
        }
        teacher_lead_id
        teacher_lead {
          id
          user_name
          full_name
          phone
          address
          email
          role_id
        }
        description
        featured_image
        total_register
        total_paid
        config_fee {
          type
          fee
          detail_id
          detail_name
        }
        end_date
        subcats {
          id
          name
          status
          parent_id
        }
        type
        level_in {
          subject_id
          id
          name
          graduation
          status
          type
        }
        level_out {
          subject_id
          id
          name
          graduation
          status
          type
        }
        subject {
          id
          name
          status
        }
        coefficient_range {
          id
          min
          max
          operator
          name
        }
        teachers {
          id
          user_name
          full_name
          phone
          address
          email
          role_id
          role_name
        }
      }
      courseOld {
        id
        name
        grade_id
        grade_name
        class_id
        class_name
        open_date
        fee
        curriculum_id
        curriculum_name
        total_schedule
        school {
          name
        }
      }
    }
  }
`,ts=p`
  query teacherAvailable($course_id: Int!, $date: String, $shift_id: Int)
  @api(name: "appZim") {
    teacherAvailable(course_id: $course_id, date: $date, shift_id: $shift_id) {
      ...AccountField
    }
  }
  ${Ke}
`,ns=p`
  query courseLesson($course_id: Int!) @api(name: "appZim") {
    courseLesson(course_id: $course_id) {
      id
      name
      subcat_type {
        id
        name
      }
      subcats {
        id
        name
      }
    }
  }
`,is=p`
  query roomsAvalable(
    $course_id: Int!
    $date: String
    $shift_id: Int
    $room_ids: [Int]
  ) @api(name: "appZim") {
    roomsAvalable(
      course_id: $course_id
      date: $date
      shift_id: $shift_id
      room_ids: $room_ids
    ) {
      id
      name
      status
      description
    }
  }
  ${Ke}
`,Ti=p`
  query coursePlanbyStudentId($studentId: Int!) @api(name: "appZim") {
    coursePlanbyStudentId(studentId: $studentId) {
      id
      name
      school {
        name
        id
      }
    }
  }
`,ss=p`
  query getCoursePlanerNotes(
    $coursePlanerId: Int!
    $coursePlanerType: EnumCoursePlanerTypeEnum!
  ) @api(name: "appZim") {
    getCoursePlanerNotes(
      coursePlanerId: $coursePlanerId
      coursePlanerType: $coursePlanerType
    ) {
      id
      refId
      refTable
      note
      createBy {
        id
        user_name
        full_name
      }
      createDate
    }
  }
`,us=p`
  query courseSchedule(
    $course_id: Int
    $subcat_type_id: Int
    $month: Int
    $year: Int
  ) @api(name: "appZim") {
    courseSchedule(
      course_id: $course_id
      subcat_type_id: $subcat_type_id
      month: $month
      year: $year
    ) {
      is_additional
      event_id
      course {
        id
        slug
      }
      lesson
      teacher_id
      teacher {
        id
        user_name
        full_name
      }
      alt_teacher_id
      date
      room {
        id
        name
      }
      shift {
        id
        start_time
        end_time
      }
      subcats {
        id
        name
      }
      status
      subcat_type_id
      subcat_type {
        id
        name
      }
      document
    }
  }
`,as=p`
  query getCourseSummary($course_id: Int) @api(name: "appZim") {
    getCourseSummary(course_id: $course_id) {
      total

      total_registered

      total_additional
    }
  }
`,os=(s={},e={})=>{const u=y();return D(Gn,l({variables:s,onError:i=>{var o;u({title:"L\u1EA5y d\u1EEF li\u1EC7u th\u1EA5t b\u1EA1i.",description:(o=i==null?void 0:i.message)!=null?o:"L\u1EA5y d\u1EEF li\u1EC7u kh\xF4ng th\xE0nh c\xF4ng",status:"error",duration:3e3,isClosable:!0})},notifyOnNetworkStatusChange:!0},e))},ls=(s,e={})=>{const u=y();return D(fi,l({variables:s,onError:i=>{u({title:"L\u1EA5y d\u1EEF li\u1EC7u th\u1EA5t b\u1EA1i.",description:`GET course's student error: ${i==null?void 0:i.message}`,status:"error",duration:3e3,isClosable:!0})}},e))},Li=(s,e={})=>{const u=y();return D(Un,l({variables:s,onError:i=>{var o;u({title:"L\u1EA5y d\u1EEF li\u1EC7u th\u1EA5t b\u1EA1i.",description:(o=i==null?void 0:i.message)!=null?o:"L\u1EA5y d\u1EEF li\u1EC7u kh\xF4ng th\xE0nh c\xF4ng",status:"error",duration:3e3,isClosable:!0})},notifyOnNetworkStatusChange:!0,skip:!(s==null?void 0:s.id)},e))},Ii=(s,e={})=>{const u=y();return D(zn,l({variables:s,onError:i=>{var o;u({title:"L\u1EA5y d\u1EEF li\u1EC7u th\u1EA5t b\u1EA1i.",description:(o=i==null?void 0:i.message)!=null?o:"L\u1EA5y d\u1EEF li\u1EC7u kh\xF4ng th\xE0nh c\xF4ng",status:"error",duration:3e3,isClosable:!0})},notifyOnNetworkStatusChange:!0},e))},Oi=({children:s,limit:e,onReadMore:u=null})=>{var ee;const i=_.exports.useRef(null),[o,d]=_.exports.useState(!1),[f,b]=_.exports.useState({trim:null,full:null}),[g,F]=_.exports.useState(!0),Q=()=>{u&&typeof u=="function"?u():F(!g)};return _.exports.useEffect(()=>{if(i.current){const O=i.current.textContent;O!==null&&b({full:O,trim:O.slice(0,e)})}},[e,s,o]),_.exports.useEffect(()=>{f.full&&!o&&d(!0)},[f.full,s]),_.exports.useEffect(()=>{d(!1)},[s]),E(he,{children:[n("div",{className:"text hidden",ref:i,children:s}),E("p",{children:[g?f.trim:f.full,((ee=f==null?void 0:f.full)==null?void 0:ee.length)>e&&n("span",{onClick:Q,className:"whitespace-nowrap cursor-pointer read-or-hide text-blue-700 hover:text-blue-900 dark:text-blue-500 dark:hover:text-blue-700 duration-200 transition-colors",children:g?"... Xem th\xEAm":" \u1EA8n b\u1EDBt"})]})]})};var He;(function(s){s.OLD="invoiceold",s.NEW="invoicenew"})(He||(He={}));const Di=({data:s,onSelectedRows:e})=>{const u=_.exports.useMemo(()=>[{Header:"M\xE3 phi\u1EBFu",accessor:"number_invoice",nowrap:!0,Cell:({value:i,row:{original:o}})=>n($e,{label:"B\u1EA5m \u0111\u1EC3 xem phi\u1EBFu thu",children:E(_e,{alignItems:"center",children:[n(Ge,{href:Fe(`${ae.invoice}/${o.code}`,`${Te.invoice}${o.type==="invoicenew"?`/${o.code}`:`?code=${o.code}`}`),isExternal:!0,children:i}),n(xn,{colorScheme:"gray",size:"xs",fontSize:"xs",p:1,children:o.type===He.NEW?"M\u1EDBi":"C\u0169"})]})}),disableSortBy:!0},{Header:"\u0110\xE3 thanh to\xE1n",accessor:"paid",Cell:({value:i,row:{original:o}})=>i!==0?`${me(i)}`:"0",disableSortBy:!0,nowrap:!0},{Header:"Ghi ch\xFA",accessor:"note",Cell:({value:i})=>n(Oi,{limit:50,children:i}),disableSortBy:!0},{Header:"Ng\u01B0\u1EDDi t\u1EA1o",accessor:"create_by",disableSortBy:!0,minWidth:100},{Header:"Ng\xE0y t\u1EA1o",accessor:"create_date",Cell:({value:i,row:{original:o}})=>n("span",{children:st(i).format("DD/MM/YYYY HH:mm")}),disableSortBy:!0,minWidth:100}],[]);return n(en,{columns:u,data:s,isSelection:!0,onSelectedRows:e})},wi=(s,e={})=>{const u=y();return D($i,l({variables:s,onError:i=>{var o;u({title:"L\u1EA5y d\u1EEF li\u1EC7u th\u1EA5t b\u1EA1i.",description:(o=i==null?void 0:i.message)!=null?o:"L\u1EA5y d\u1EEF li\u1EC7u kh\xF4ng th\xE0nh c\xF4ng",status:"error",duration:3e3,isClosable:!0})},notifyOnNetworkStatusChange:!0},e))},ki=(s,e={})=>{const u=y();return Wt(yi,l({variables:s,onError:i=>{var o;u({title:"T\u1EA1o y\xEAu c\u1EA7u th\u1EA5t b\u1EA1i",description:(o=i==null?void 0:i.message)!=null?o:"Vui l\xF2ng ki\u1EC3m tra l\u1EA1i d\u1EEF li\u1EC7u nh\u1EADp...",status:"error",duration:3e3,isClosable:!0})}},e))},Pi=(s,e={})=>{const u=y();return D(Ti,l({variables:s,onError:i=>{var o;u({title:"L\u1EA5y d\u1EEF li\u1EC7u th\u1EA5t b\u1EA1i.",description:(o=i==null?void 0:i.message)!=null?o:"L\u1EA5y d\u1EEF li\u1EC7u kh\xF4ng th\xE0nh c\xF4ng",status:"error",duration:3e3,isClosable:!0})},notifyOnNetworkStatusChange:!0},e))},Bi=(s,e={})=>{const u=y();return D(Fi,l({variables:s,onError:i=>{var o;u({title:"L\u1EA5y d\u1EEF li\u1EC7u th\u1EA5t b\u1EA1i.",description:(o=i==null?void 0:i.message)!=null?o:"L\u1EA5y d\u1EEF li\u1EC7u kh\xF4ng th\xE0nh c\xF4ng",status:"error",duration:3e3,isClosable:!0})},notifyOnNetworkStatusChange:!0},e))},dt=[{label:"Chuy\u1EC3n v\xE0o kh\xF3a",value:le.JoinCourse},{label:"X\xF3a h\u1ECDc vi\xEAn kh\u1ECFi kh\xF3a",value:le.LeftCourse},{label:"H\u1ECDc l\u1EA1i",value:le.ReEnroll}],un=[{label:"Ch\xEDnh th\u1EE9c",value:oe.Course},{label:"D\u1EF1 ki\u1EBFn",value:oe.CoursePlan}],Ri=$.object().shape({requestType:$.string().required("M\u1EE5c n\xE0y l\xE0 b\u1EAFt bu\u1ED9c"),center:$.mixed().nullable(!0).required("M\u1EE5c n\xE0y l\xE0 b\u1EAFt bu\u1ED9c"),note:$.string().required("M\u1EE5c n\xE0y l\xE0 b\u1EAFt bu\u1ED9c"),isAddonCourse:$.boolean(),invoices:$.array().of($.object().shape({number_invoice:$.string(),note:$.string(),paid:$.number(),create_by:$.string(),create_date:$.string()})).when("requestType",{is:s=>s!==le.LeftCourse,then:$.array().of($.object().shape({number_invoice:$.string(),note:$.string(),paid:$.number(),create_by:$.string(),create_date:$.string()})).min(1,"Vui l\xF2ng ch\u1ECDn \xEDt nh\u1EA5t m\u1ED9t phi\u1EBFu thu").required("M\u1EE5c n\xE0y l\xE0 b\u1EAFt bu\u1ED9c")}),course:$.mixed().nullable(!0).required("M\u1EE5c n\xE0y l\xE0 b\u1EAFt bu\u1ED9c"),courseType:$.mixed().nullable(!0).required("M\u1EE5c n\xE0y l\xE0 b\u1EAFt bu\u1ED9c")}),Ni=({studentId:s,onClose:e})=>{var xe,ve;const{handleSubmit:u,register:i,control:o,formState:{errors:d},resetField:f,setValue:b,watch:g,getValues:F,setError:Q,reset:ee}=Jt({reValidateMode:"onBlur",defaultValues:{requestType:le.JoinCourse,invoices:[],course:null,note:"",center:null,courseType:un[0],isAddonCourse:!1},resolver:Xt(Ri)}),[O,j,a,G,Z,Ie]=g(["courseType","center","invoices","course","requestType","isAddonCourse"]),P=(O==null?void 0:O.value)===oe.Course,W=y(),{data:te,loading:Oe}=Vt(),{data:m,loading:ce}=Li({id:s}),{data:Y,loading:be}=wi({input:{refTypes:O?[O.value]:[],schoolIds:(j==null?void 0:j.id)?[j==null?void 0:j.id]:[],limit:9999}},{onCompleted:({coursePlanerPagination:h})=>{h&&b("course",null)},skip:!j||!O}),{data:U,loading:de}=Pi({studentId:s}),{data:M,loading:ne}=Bi({studentId:s}),{data:q,loading:De}=Ii({student_id:s,status:Kn.Paid},{skip:!s}),[we,{loading:Ce}]=ki({},{onCompleted:({createRequestStudentToCourse:h})=>{var c,w,z;h?(W({title:"Th\xE0nh c\xF4ng !",description:`T\u1EA1o y\xEAu c\u1EA7u: ${(c=dt.find(H=>H.value===Z))==null?void 0:c.label} th\xE0nh c\xF4ng !!`,status:"success",duration:3e3,isClosable:!0}),e(),ee()):W({title:`T\u1EA1o y\xEAu c\u1EA7u: ${(w=dt.find(H=>H.value===Z))==null?void 0:w.label} th\u1EA5t b\u1EA1i !!`,description:(z=h==null?void 0:h.message)!=null?z:"Kh\xF4ng x\xE1c \u0111\u1ECBnh",status:"error",duration:3e3,isClosable:!0})}}),ke=async h=>{try{const{requestType:c,invoices:w,course:z,note:H,center:r,courseType:ie,isAddonCourse:K}=h;let V={student_id:s,course_id:z.id,type:c,note:H,refTable:ie.value,new_invoice_ids:[],old_invoice_ids:[],is_free:K};const Re=w.filter(J=>J.type===He.OLD),Xe=w.filter(J=>J.type===He.NEW);if(c===le.JoinCourse&&w.reduce((Qe,Se)=>Qe+Se.paid+Se.discount_price,0)<z.fee){Q("invoices",{type:"custom",message:"T\u1ED5ng phi\u1EBFu thu kh\xF4ng \u0111\u01B0\u1EE3c th\u1EA5p h\u01A1n s\u1ED1 ti\u1EC1n kh\xF3a chuy\u1EC3n v\xE0o"});return}V=C(l({},V),{new_invoice_ids:Xe.map(J=>J.id),old_invoice_ids:Re.map(J=>J.id)}),await we({variables:V})}catch(c){console.log("Error submit form",c)}},B=m==null?void 0:m.getPersonalInfo,Pe=(xe=Y==null?void 0:Y.coursePlanerPagination)==null?void 0:xe.docs,ye=q==null?void 0:q.getStudentInvoices,N=_.exports.useMemo(()=>P?M==null?void 0:M.getAllCourseBySTudentById:U==null?void 0:U.coursePlanbyStudentId,[M,U,P]),Be=_.exports.useMemo(()=>{if(!N)return null;let h=[];return P?Array.isArray(N==null?void 0:N.courseNew)&&Array.isArray(N==null?void 0:N.courseOld)?(h=[...N.courseNew,...N.courseOld],console.log({courses:h},"1")):h=[]:h=N,h.length>0?h.map(c=>{var w;return n(v,{children:E(Ge,{isExternal:!0,href:Fe(`${P?ae.courseDetail:ae.coursePlanDetail}/${c.id}`,`${P?Te.courseDetail:ae.coursePlanDetail}/${c.id}`),children:[`${P?"":`${(w=c==null?void 0:c.school)==null?void 0:w.name} | `}`,c.name]})},c.id)}):n(v,{color:"red.500",children:"Kh\xF4ng c\xF3 kh\xF3a h\u1ECDc n\xE0o"})},[P,N]);return _.exports.useEffect(()=>{ee()},[]),n(he,{children:E(S,{as:"form",onSubmit:u(ke),fontSize:"sm",children:[B&&n(S,{bg:"blue.50",rounded:4,p:2,children:E(ut,{children:[n(fe,{pr:4,children:E(v,{children:["H\u1ECD v\xE0 t\xEAn:"," ",n($e,{label:"Xem th\xF4ng tin h\u1ECDc vi\xEAn",children:n(S,{as:"span",children:n(Hn,{to:`${Fe(`${ae.account}/${B.id}`,`${Te.customerInfo}/${B.id}`)}`,children:n("strong",{children:B==null?void 0:B.fullName})})})})]})}),n(fe,{pr:4,children:E(v,{children:["S\u0110T: ",n("strong",{children:B==null?void 0:B.phone})]})}),n(fe,{pr:4,children:E(v,{children:["Email: ",E("strong",{children:[" ",B==null?void 0:B.email]})]})})]})}),n(S,{w:"full",mt:4,children:E(_e,{justifyContent:"stretch",children:[n(S,{w:200,children:n(A,{name:"courseType",control:o,render:({field:h})=>n(k,l({label:"Lo\u1EA1i kho\xE1",error:d==null?void 0:d.course,options:un||[]},h))})}),n(S,{flexGrow:1,children:n(A,{name:"center",control:o,render:({field:h})=>n(k,l({label:"Trung t\xE2m (*)",error:d==null?void 0:d.center,getOptionLabel:c=>c.name,getOptionValue:c=>c.id,options:te?te.schools:[],isLoading:Oe,loadingMessage:()=>"\u0110ang l\u1EA5y th\xF4ng tin center"},h))})})]})}),E(Qt,{spacing:4,mt:4,children:[n(A,{name:"requestType",control:o,render:({field:h})=>E(nt,{children:[n(it,{children:"Lo\u1EA1i y\xEAu c\u1EA7u:"}),n(vn,C(l({},h),{children:n(ut,{spacing:4,children:dt.map(c=>n(fe,{pr:4,children:n(Sn,{value:c.value,children:c.label})},c.value))})}))]})}),E(S,{w:"full",children:[n(A,{name:"course",control:o,render:({field:h})=>n(k,l({label:"Kho\xE1 h\u1ECDc",placeholder:"Ch\u1ECDn kh\xF3a h\u1ECDc",error:d==null?void 0:d.course,getOptionLabel:c=>{var w,z,H,r;return`${c.type} ${c.size.size<4?"Solo":c.subject.name} | ${(z=(w=c.level_in)==null?void 0:w.graduation.toFixed(1))!=null?z:"unset"} \u2192
 ${(r=(H=c.level_out)==null?void 0:H.graduation.toFixed(1))!=null?r:"unset"} | ${st(c.start_date).format("DD/MM/YYYY")} | ${c.size.size} h\u1ECDc vi\xEAn | ${me(c.fee)}`},getOptionValue:c=>c.id,options:Pe||[],isLoading:be,loadingMessage:()=>"\u0110ang l\u1EA5y d\u1EEF li\u1EC7u kh\xF3a h\u1ECDc..."},h))}),!j&&n(v,{color:"red.500",mt:2,children:"Vui l\xF2ng ch\u1ECDn trung t\xE2m tr\u01B0\u1EDBc"})]}),E(S,{w:"full",children:[E(v,{children:["C\xE1c kho\xE1 ",P?"ch\xEDnh th\u1EE9c":"d\u1EF1 ki\u1EBFn"," hi\u1EC7n t\u1EA1i c\u1EE7a h\u1ECDc vi\xEAn:"]}),Be]}),n(jn,C(l({label:"Ghi ch\xFA",bg:"white",minRows:2,placeholder:"N\u1ED9i dung..."},i("note")),{error:d==null?void 0:d.note}))]}),E(S,{mt:4,children:[Z!==le.LeftCourse&&n(S,{mb:4,children:n($e,{label:"N\u1EBFu enable th\xEC c\xE1c phi\u1EBFu thu \u0111\xE3 ch\u1ECDn s\u1EBD kh\xF4ng \u0111\u01B0\u1EE3c t\xEDnh v\xE0o t\u1ED5ng doanh thu c\u1EE7a kh\xF3a n\xE0y",children:E(nt,{display:"flex",alignItems:"center",children:[n(it,{htmlFor:"is-addon-course",mb:"0",d:"flex",children:n(v,{as:"span",children:" Kh\xF3a h\u1ECDc t\u1EB7ng k\xE8m ?"})}),n(An,C(l({},i("isAddonCourse")),{isChecked:Ie}))]})})}),n($n,{size:"base",mb:2,children:"Ch\u1ECDn phi\u1EBFu thu:"}),De?n(Zn,{text:"\u0110ang l\u1EA5y th\xF4ng tin h\xF3a \u0111\u01A1n"}):ye&&n(A,{control:o,render:({field:h})=>n(Di,{data:ye,onSelectedRows:h.onChange}),name:"invoices"}),Z!==le.LeftCourse&&n(he,{children:n(v,{color:"slate.500",my:2,children:"Note: T\u1ED5ng ti\u1EC1n phi\u1EBFu thu ph\u1EA3i l\u1EDBn h\u01A1n ho\u1EB7c b\u1EB1ng s\u1ED1 ti\u1EC1n kh\xF3a h\u1ECDc \u0111\u01B0\u1EE3c chuy\u1EC3n v\xE0o"})}),d.invoices&&n(v,{color:X("red.500","red.300"),mt:2,children:(ve=d==null?void 0:d.invoices)==null?void 0:ve.message}),E(S,{mt:4,children:[G&&E(v,{children:["H\u1ECDc ph\xED kh\xF3a \u0111\xE3 ch\u1ECDn:"," ",n("strong",{children:(G==null?void 0:G.fee)?me(G.fee):0})," "]}),(a==null?void 0:a.length)>0&&E(v,{children:["T\u1ED5ng ti\u1EC1n \u0111\xE3 thanh to\xE1n:"," ",n("strong",{children:me(a.reduce((h,c)=>h+c.paid+c.discount_price,0))})]}),(a==null?void 0:a.length)>0&&G&&G&&E(v,{children:["S\u1ED1 ti\u1EC1n ch\xEAnh l\u1EC7ch:"," ",n("strong",{children:me(a.reduce((h,c)=>h+c.paid+c.discount_price,0)-G.fee)})]})]})]}),E(_e,{mt:8,spacing:4,children:[n(at,{type:"submit",colorScheme:"green",isLoading:Ce,children:"G\u1EEDi y\xEAu c\u1EA7u"}),n(at,{variant:"outline",onClick:e,isDisabled:Ce,children:"H\u1EE7y b\u1ECF"})]})]})})},Mi=({children:s,studentId:e})=>{const{onOpen:u,onClose:i,isOpen:o}=fn();return E(he,{children:[n(S,{onClick:u,role:"button","aria-label":"Show modal dialog",children:s}),E(Fn,{isOpen:o,onClose:()=>{},size:"3xl",children:[n(Tn,{}),E(Ln,{children:[n(In,{children:"G\u1EEDi y\xEAu c\u1EA7u"}),n(On,{onClick:i}),n(Dn,{children:n(S,{pb:4,children:n(Ni,{studentId:e,onClose:i})})})]})]})]})},rs=({data:s})=>{const e=_.exports.useMemo(()=>[{Header:"H\u1ECDc vi\xEAn",accessor:"student_name",nowrap:!0,Cell:({value:u,row:{original:i}})=>n($e,{label:"Xem th\xF4ng tin h\u1ECDc vi\xEAn",children:n(S,{children:n(Ge,{href:`${Fe(`${ae.account}/${i.student_id}`,`${Te.customerInfo}/${i.student_id}`)}`,isExternal:!0,children:u})})})},{Header:"Tr\u1EA1ng th\xE1i",accessor:"student_type",nowrap:!0,Cell:({value:u})=>u==="new"?"\u0110\u0103ng k\xFD m\u1EDBi":"H\u1ECDc l\u1EA1i"},{Header:()=>n(v,{align:"right",children:"\u0110\xE3 thanh to\xE1n"}),accessor:"paid",nowrap:!0,Cell:({value:u,row:{original:i}})=>{const{listInvoice:o,invoiceStudent:d}=i,b=((o==null?void 0:o.length)>0?o:d).reduce((g,F)=>F.paid+g,0);return n(v,{children:`${b?me(b):"0"}`})}},{Header:"Thao t\xE1c",accessor:"",nowrap:!0,Cell:({value:u,row:{original:i}})=>n(Mi,{studentId:i.student_id,children:n(at,{size:"sm",leftIcon:n(wn,{}),children:"T\u1EA1o y\xEAu c\u1EA7u"})})},{Header:"Phi\u1EBFu thu",accessor:"invoices",disableSortBy:!0,isSticky:!0,stickyRightOffset:0,Cell:({value:u,row:{original:i}})=>{const{listInvoice:o,invoiceStudent:d}=i,f=(o==null?void 0:o.length)===0;return E(he,{children:[n(_e,{children:o.map(b=>n(Ge,{href:Fe(`${ae.invoice}/${b.code}`,`${Te.invoice}/${b.code}`),isExternal:!0,children:n(S,{as:"span",children:n($e,{label:b.note,children:n(v,{fontSize:24,color:X("blue.500","blue.300"),children:n(Ye,{})})})})},b.code))}),f&&E(he,{children:[n(_e,{children:d.map(b=>n(Ge,{href:Fe(`${ae.invoice}/${b.code}`,`${Te.invoice}?code=${b.code}`),isExternal:!0,children:n(S,{as:"span",children:n($e,{label:b.note,children:n(v,{color:"yellow.500",fontSize:24,children:n(Ye,{})})})})},b.code))}),n(v,{fontSize:12,mt:1,color:X("gray.500","slate.500"),children:"H\u1ECDc vi\xEAn kh\xF4ng c\xF3 phi\u1EBFu thu trong kh\xF3a n\xE0y, ki\u1EC3m tra l\u1EA1i trong c\xE1c phi\u1EBFu thu tr\xEAn."})]})]})}}],[]);return E(S,{children:[n(en,{columns:e,data:s}),E(ut,{mt:4,gap:4,children:[n(fe,{children:E(_e,{children:[n(v,{fontSize:24,color:X("blue.500","blue.300"),children:n(Ye,{})}),n(v,{children:"Phi\u1EBFu thu kh\xF3a hi\u1EC7n t\u1EA1i"})]})}),n(fe,{children:E(_e,{children:[n(v,{fontSize:24,color:"yellow.500",children:n(Ye,{})}),n(v,{children:"Phi\u1EBFu thu kh\xE1c c\u1EE7a h\u1ECDc vi\xEAn kh\xF4ng thu\u1ED9c kh\xF3a hi\u1EC7n t\u1EA1i"})]})})]})]})},cs=(s={},e={})=>{const u=y();return Wt(Ci,l({variables:s,onError:i=>{var o;u({title:"Th\u1EA5t b\u1EA1i !!!",description:(o=i==null?void 0:i.message)!=null?o:"T\u1EA1o ho\u1EB7c c\u1EADp nh\u1EADt kh\xF4ng th\xE0nh c\xF4ng !",status:"error",duration:5e3,isClosable:!0})},context:{headers:{"x-app-token":"0aAUfvzhIZVSDIF0yBk53U8Vokij6xv7"}},notifyOnNetworkStatusChange:!0},e))};export{Ji as C,es as D,us as G,rs as S,Wi as U,as as a,ts as b,is as c,ns as d,Vi as e,os as f,ls as g,cs as h,Qi as i,ss as j,Xi as k,wi as l,Ki as m,ii as n,ai as o,oi as p,ri as q,nn as r,Zi as s,me as t,li as u,ze as v,Yi as w};
