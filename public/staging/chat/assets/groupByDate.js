var Ut=Object.defineProperty,qt=Object.defineProperties;var wt=Object.getOwnPropertyDescriptors;var g=Object.getOwnPropertySymbols;var Nt=Object.prototype.hasOwnProperty,xt=Object.prototype.propertyIsEnumerable;var m=(e,a,i)=>a in e?Ut(e,a,{enumerable:!0,configurable:!0,writable:!0,value:i}):e[a]=i,l=(e,a)=>{for(var i in a||(a={}))Nt.call(a,i)&&m(e,i,a[i]);if(g)for(var i of g(a))xt.call(a,i)&&m(e,i,a[i]);return e},u=(e,a)=>qt(e,wt(a));import{g as t,Q as f}from"./vendor.js";const Bt={province:null,center:{label:"T\u1EA5t c\u1EA3",value:0},status:{label:"T\u1EA5t c\u1EA3",value:0},startDate:null,endDate:null,subject:{name:"T\u1EA5t c\u1EA3",id:0}},Wt=2,Yt=1,Xt=1,yt=6,p=t`
  fragment QuestionField on IeltsPracticeQuestion {
    id
    testId
    questionGroupId
    question
    questionIndex
    questionCount
    sampleAnswers
    allowToBreakPoint

    answers {
      answer
      correct
      alternatives
    }
    explanation
    type
  }
`,d=t`
  fragment QuestionGroupField on IeltsPracticeQuestionGroup {
    id
    title
    testId
    question
    groupIndex
    audioFiles {
      id
      url
    }
    questions {
      ...QuestionField
    }
  }
  ${p}
`,T=t`
  fragment ModuleField on IeltsPracticeTest {
    id
    title
    type
    level
    status
    format
    parts {
      question
      questionGroupId
    }
    audio {
      id
      url
    }
    questionGroups {
      ...QuestionGroupField
    }
  }
  ${d}
`,r=t`
  fragment PracticeTestField on IeltsPracticeTestGroup {
    id
    title
    slug
    level
    status
    practiceTests {
      id
      title
      type
      level
      status
    }
    active
  }
`;t`
  mutation createIELTSPracticeTest(
    $type: IeltsPracticeTestType!
    $level: IeltsPracticeTestLevel!
    $title: String
    $audio: Upload
    $parts: [IeltsPracticeTestPartInput]
    $format: IeltsPracticeTestFormat
  ) @api(name: "zim") {
    createIELTSPracticeTest(
      type: $type
      level: $level
      title: $title
      audio: $audio
      parts: $parts
      format: $format
    ) {
      id
      title
      type
      level
      status
      parts {
        question
        questionGroupId
      }
    }
  }
`;t`
  mutation updateIELTSPracticeTest(
    $id: ObjectID!
    $type: IeltsPracticeTestType!
    $level: IeltsPracticeTestLevel!
    $title: String
    $audio: Upload
    $parts: [IeltsPracticeTestPartInput]
    $format: IeltsPracticeTestFormat
  ) @api(name: "zim") {
    updateIELTSPracticeTest(
      id: $id
      type: $type
      level: $level
      title: $title
      audio: $audio
      parts: $parts
      format: $format
    ) {
      id
      type
      level
      status
      parts {
        question
        questionGroupId
      }
    }
  }
`;t`
  mutation publishIELTSPracticeTest($id: ObjectID!) @api(name: "zim") {
    publishIELTSPracticeTest(id: $id) {
      id
      type
      level
      status
      questionGroups {
        ...QuestionGroupField
      }
    }
  }
  ${d}
`;t`
  mutation createIELTSPracticeTestQuestionGroup(
    $testId: ObjectID!
    $question: String!
    $groupIndex: Int
    $title: String
  ) @api(name: "zim") {
    createIELTSPracticeTestQuestionGroup(
      testId: $testId
      question: $question
      groupIndex: $groupIndex
      title: $title
    ) {
      ...QuestionGroupField
    }
  }
  ${d}
`;t`
  mutation updateIELTSPracticeTestQuestionGroup(
    $id: ObjectID!
    $question: String!
    $title: String
    $groupIndex: Int
  ) @api(name: "zim") {
    updateIELTSPracticeTestQuestionGroup(
      id: $id
      question: $question
      title: $title
      groupIndex: $groupIndex
    ) {
      ...QuestionGroupField
    }
  }
  ${d}
`;t`
  mutation removeIELTSPracticeTestQuestionGroup($id: ObjectID!)
  @api(name: "zim") {
    removeIELTSPracticeTestQuestionGroup(id: $id)
  }
`;t`
  mutation removeIELTSPracticeTestQuestion($id: ObjectID!) @api(name: "zim") {
    removeIELTSPracticeTestQuestion(id: $id)
  }
`;t`
  mutation createIELTSPracticeTestQuestion(
    $questionGroupId: ObjectID!
    $input: IELTSPracticeTestQuestionInput!
    $questionIndex: Int
  ) @api(name: "zim") {
    createIELTSPracticeTestQuestion(
      questionGroupId: $questionGroupId
      input: $input
      questionIndex: $questionIndex
    ) {
      ...QuestionField
    }
  }
  ${p}
`;t`
  mutation updateIELTSPracticeTestQuestion(
    $id: ObjectID!
    $input: IELTSPracticeTestQuestionInput!
  ) @api(name: "zim") {
    updateIELTSPracticeTestQuestion(id: $id, input: $input) {
      ...QuestionField
    }
  }
  ${p}
`;t`
  mutation swapIELTSPracticeTestQuestionIndex(
    $firstId: ObjectID!
    $secondId: ObjectID!
  ) @api(name: "zim") {
    swapIELTSPracticeTestQuestionIndex(firstId: $firstId, secondId: $secondId) {
      id
      testId
    }
  }
`;t`
  mutation swapIELTSPracticeTestQuestionGroupIndex(
    $firstId: ObjectID!
    $secondId: ObjectID!
  ) @api(name: "zim") {
    swapIELTSPracticeTestQuestionGroupIndex(
      firstId: $firstId
      secondId: $secondId
    ) {
      id
      testId
    }
  }
`;t`
  mutation createIELTSPracticeTestGroup(
    $title: String!
    $slug: String!
    $level: IeltsPracticeTestLevel!
    $practiceTests: [ObjectID]!
    $featureMediaIds: [ObjectID]!
    $description: String
  ) @api(name: "zim") {
    createIELTSPracticeTestGroup(
      title: $title
      slug: $slug
      level: $level
      practiceTests: $practiceTests
      featureMediaIds: $featureMediaIds
      description: $description
    ) {
      ...PracticeTestField
    }
  }
  ${r}
`;t`
  mutation updateIELTSPracticeTestGroup(
    $id: ObjectID!
    $title: String!
    $slug: String!
    $level: IeltsPracticeTestLevel!
    $practiceTests: [ObjectID]!
    $featureMediaIds: [ObjectID]!
    $description: String
    $categoryIds: [ObjectID]
  ) @api(name: "zim") {
    updateIELTSPracticeTestGroup(
      id: $id
      title: $title
      slug: $slug
      level: $level
      practiceTests: $practiceTests
      featureMediaIds: $featureMediaIds
      description: $description
      categoryIds: $categoryIds
    ) {
      ...PracticeTestField
    }
  }
  ${r}
`;t`
  mutation publishIELTSPracticeTestGroup($id: ObjectID!) @api(name: "zim") {
    publishIELTSPracticeTestGroup(id: $id) {
      ...PracticeTestField
    }
  }
  ${r}
`;t`
  mutation unpublishIELTSPracticeTestGroup($id: ObjectID!) @api(name: "zim") {
    unpublishIELTSPracticeTestGroup(id: $id) {
      ...PracticeTestField
    }
  }
  ${r}
`;t`
  mutation setActiveIELTSPracticeTestGroup($id: ObjectID!) @api(name: "zim") {
    setActiveIELTSPracticeTestGroup(id: $id) {
      ...PracticeTestField
    }
  }
  ${r}
`;t`
  mutation updateIELTSPracticeTestGroupSeoConfig(
    $IELTSPracticeTestGroupId: ObjectID!
    $seo: CommonSeoInput!
  ) @api(name: "zim") {
    updateIELTSPracticeTestGroupSeoConfig(
      IELTSPracticeTestGroupId: $IELTSPracticeTestGroupId
      seo: $seo
    ) {
      seo {
        title
        description
        ogDescription
        ogImage {
          id
          type
          path
          filename
          title
          visibility
          width
          height
          usage
          createdAt
          updatedAt
        }
        ogTitle
        publisher
        noIndex
        noFollow
        canonicalUrl
        customHeadHtml
      }
    }
  }
`;const Jt=t`
  mutation createMockTest(
    $dates: [String]
    $school_id: Int!
    $type: EnumMockTestType
    $subjectId: Int!
  ) @api(name: "appZim") {
    createMockTest(
      dates: $dates
      school_id: $school_id
      type: $type
      subjectId: $subjectId
    ) {
      id
      slug
      type
      status
      fee
      total_slot
    }
  }
`,ei=t`
  mutation updateMockTest(
    $id: Int
    $status: EnumBaseStatus
    $dates: [String]
    $school_id: Int
  ) @api(name: "appZim") {
    updateMockTest(
      id: $id
      status: $status
      dates: $dates
      school_id: $school_id
    ) {
      id
    }
  }
`,ti=t`
  mutation updateMockTestDetailV2(
    $mockTestId: Int!
    $mockTestDetail: [MockTestDetailInputV2Type]!
  ) @api(name: "appZim") {
    updateMockTestDetailV2(
      mockTestDetail: $mockTestDetail
      mockTestId: $mockTestId
    )
  }
`,ii=t`
  mutation updateMockTestAttendanceV2(
    $mock_test_detail_id: Int
    $student_id: Int
    $is_missing: Boolean
  ) @api(name: "appZim") {
    updateMockTestAttendanceV2(
      mock_test_detail_id: $mock_test_detail_id
      student_id: $student_id
      is_missing: $is_missing
    ) {
      id
    }
  }
`,ai=t`
  mutation uploadResultStudent($input: UploadResultMockTestOrderInputType)
  @api(name: "appZim") {
    uploadResultStudent(input: $input) {
      success
    }
  }
`,ni=t`
  mutation updateResultMockTest(
    $orderId: Int
    $input: UpdateOrderDetailInputType
  ) @api(name: "appZim") {
    updateResultMockTest(orderId: $orderId, input: $input) {
      message
    }
  }
`,oi=t`
  mutation sentEmailResultForStudent($orderId: Int) @api(name: "appZim") {
    sentEmailResultForStudent(orderId: $orderId) {
      message
    }
  }
`,si=t`
  mutation createMockTetOfStudentSchedule(
    $input: CreateMockTestOfStudentScheduleInputType
  ) @api(name: "appZim") {
    createMockTestOfStudentSchedule(input: $input) {
      message
    }
  }
`;t`
  mutation confirmOrCancelOrder(
    $orderId: ObjectID!
    $action: OrderActionInput!
    $cancelReason: String
    $confirmPayment: Boolean
  ) @api(name: "zim") {
    confirmOrCancelOrder(
      orderId: $orderId
      action: $action
      cancelReason: $cancelReason
      confirmPayment: $confirmPayment
    ) {
      id
    }
  }
`;var I;(function(e){e.ClickRatio="clickRatio",e.Clicks="clicks",e.Displays="displays"})(I||(I={}));var v;(function(e){e.Post="Post",e.Product="Product"})(v||(v={}));var $;(function(e){e.Clicks="clicks",e.CreatedAt="createdAt",e.UpdatedAt="updatedAt"})($||($={}));var _;(function(e){e.Draft="draft",e.Publish="publish"})(_||(_={}));var P;(function(e){e.DesktopBannerXs="desktopBannerXS",e.FreeStyle="freeStyle",e.LargeRectangle="largeRectangle",e.Leaderboard="leaderboard",e.LeaderboardMd="leaderboardMD",e.MediumRectangle="mediumRectangle",e.MobileBanner="mobileBanner",e.MobileBannerMd="mobileBannerMD",e.MobileSquare="mobileSquare",e.MobileSquareSm="mobileSquareSM",e.Sidebar="sidebar",e.SidebarXl="sidebarXL"})(P||(P={}));var S;(function(e){e.Approved="approved",e.Rejected="rejected"})(S||(S={}));var h;(function(e){e.File="file",e.Image="image",e.Video="video"})(h||(h={}));var k;(function(e){e.Private="PRIVATE",e.Public="PUBLIC"})(k||(k={}));var A;(function(e){e.IeltsCorrection="IeltsCorrection",e.MockTest="MockTest",e.Post="Post",e.Product="Product",e.VideoCourse="VideoCourse"})(A||(A={}));var C;(function(e){e.Post="Post",e.PracticeTestGroup="PracticeTestGroup",e.QaPost="QAPost"})(C||(C={}));var b;(function(e){e.Approved="approved",e.Draft="draft",e.PendingApproval="pendingApproval",e.Published="published",e.Rejected="rejected"})(b||(b={}));var O;(function(e){e.Paid="paid",e.Trial="trial"})(O||(O={}));var L;(function(e){e.ForAll="forAll",e.ForMonth="forMonth",e.ForSubcat="forSubcat"})(L||(L={}));var M;(function(e){e.Customer="customer",e.Student="student",e.Zimians="zimians"})(M||(M={}));var D;(function(e){e.Done="done",e.Following="following",e.Open="open"})(D||(D={}));var E;(function(e){e.CreatedAt="createdAt",e.UpdatedAt="updatedAt"})(E||(E={}));var F;(function(e){e.Draft="draft",e.Publish="publish"})(F||(F={}));var G;(function(e){e.Fixed="fixed",e.Percentage="percentage"})(G||(G={}));var R;(function(e){e.Fee="fee",e.Id="id",e.ProgramType="program_type",e.StartDate="start_date",e.Status="status"})(R||(R={}));var j;(function(e){e.All="all",e.Closed="closed",e.Incoming="incoming",e.Ongoing="ongoing"})(j||(j={}));var U;(function(e){e.MockTest="MockTest",e.Other="Other"})(U||(U={}));var q;(function(e){e.Daily="daily",e.Monthly="monthly",e.Yearly="yearly"})(q||(q={}));var w;(function(e){e.CreatedAt="createdAt",e.Priority="priority",e.UpdatedAt="updatedAt"})(w||(w={}));var N;(function(e){e.Active="active",e.Deactivated="deactivated"})(N||(N={}));var x;(function(e){e.MockTestOrder="MockTestOrder"})(x||(x={}));var V;(function(e){e.Function="function"})(V||(V={}));var Q;(function(e){e.PercentageForFeeByMonth="PercentageForFeeByMonth"})(Q||(Q={}));var K;(function(e){e.Course="course",e.CoursePlan="coursePlan",e.Program="program"})(K||(K={}));var Z;(function(e){e.Course="course",e.CoursePlan="coursePlan",e.Program="program"})(Z||(Z={}));var H;(function(e){e.Active="active",e.Deactivated="deactivated"})(H||(H={}));var z;(function(e){e.Fixed="fixed",e.Percentage="percentage"})(z||(z={}));var B;(function(e){e.Course="Course",e.CoursePlan="CoursePlan"})(B||(B={}));var W;(function(e){e.All="all",e.Classroom="classroom",e.Online="online"})(W||(W={}));var Y;(function(e){e.All="all",e.Done="done",e.Registed="registed",e.Unregistration="unregistration"})(Y||(Y={}));var X;(function(e){e.Fri="Fri",e.Mon="Mon",e.Sat="Sat",e.Sun="Sun",e.Thu="Thu",e.Tue="Tue",e.Wed="Wed"})(X||(X={}));var y;(function(e){e.Active="active",e.Deactivated="deactivated"})(y||(y={}));var J;(function(e){e.All="All",e.Accepted="accepted",e.Confirmed="confirmed",e.Pending="pending",e.Rejected="rejected"})(J||(J={}));var ee;(function(e){e.All="all",e.Paid="paid",e.Refund="refund"})(ee||(ee={}));var te;(function(e){e.Active="active",e.Deactivated="deactivated",e.Deleted="deleted",e.Inactive="inactive"})(te||(te={}));var ie;(function(e){e.All="all",e.Finished="finished",e.Incoming="incoming",e.Marked="marked",e.Ongoing="ongoing"})(ie||(ie={}));var ae;(function(e){e.Listening="Listening",e.Reading="Reading",e.Speaking="Speaking",e.Writing="Writing"})(ae||(ae={}));var ne;(function(e){e.Offline="offline",e.Online="online"})(ne||(ne={}));var oe;(function(e){e.Academic="Academic",e.General="General"})(oe||(oe={}));var se;(function(e){e.Asc="asc",e.Desc="desc"})(se||(se={}));var ce;(function(e){e.Atm="atm",e.Banking="banking",e.Cost="cost",e.Online="online",e.Other="other"})(ce||(ce={}));var re;(function(e){e.Accept="accept",e.Reject="reject"})(re||(re={}));var de;(function(e){e.Facebook="facebook",e.Google="google"})(de||(de={}));var le;(function(e){e.Approval="approval",e.Rejected="rejected",e.Requested="requested"})(le||(le={}));var ue;(function(e){e.Am="AM",e.Admin="Admin",e.Bm="BM",e.Bp="BP",e.Cc="CC",e.Cs="CS",e.Ec="EC",e.Gv="GV",e.Guest="Guest",e.Hv="HV",e.HVu="HVu",e.Kt="KT",e.Kth="KTH",e.Mk="MK"})(ue||(ue={}));var pe;(function(e){e.Instructor="INSTRUCTOR",e.MockTestOffline="MOCK_TEST_OFFLINE",e.MockTestOnline="MOCK_TEST_ONLINE",e.Other="OTHER",e.PlacementTest="PLACEMENT_TEST",e.SelfStudyRoom="SELF_STUDY_ROOM",e.TeacherOff="TEACHER_OFF"})(pe||(pe={}));var ge;(function(e){e.Listening="Listening",e.Reading="Reading"})(ge||(ge={}));var me;(function(e){e.Active="active",e.Deactivated="deactivated",e.Notactive="notactive"})(me||(me={}));var fe;(function(e){e.Academic="Academic",e.General="General"})(fe||(fe={}));var Te;(function(e){e.Email="email",e.Phone="phone",e.Username="username"})(Te||(Te={}));var Ie;(function(e){e.CreatedAt="createdAt",e.InteractiveScore="interactiveScore",e.NumOfComments="numOfComments",e.NumOfLikes="numOfLikes",e.NumOfShares="numOfShares"})(Ie||(Ie={}));var ve;(function(e){e.Admin="admin",e.Manager="manager",e.Mod="mod"})(ve||(ve={}));var $e;(function(e){e.CreatedAt="createdAt",e.Title="title",e.UpdatedAt="updatedAt"})($e||($e={}));var _e;(function(e){e.CreatedAt="createdAt",e.Title="title",e.UpdatedAt="updatedAt"})(_e||(_e={}));var Pe;(function(e){e.CouponsBook="COUPONS_BOOK",e.CouponsIeltsCorrect="COUPONS_IELTS_CORRECT",e.CouponsMocktest="COUPONS_MOCKTEST"})(Pe||(Pe={}));var Se;(function(e){e.GapsFilling="gapsFilling",e.IdentifyInformation="identifyInformation",e.MapLabelling="mapLabelling",e.Matching="matching",e.MultipleChoice="multipleChoice",e.MultipleQuestions="multipleQuestions",e.SingleChoice="singleChoice",e.Speaking="speaking",e.Writing="writing"})(Se||(Se={}));var he;(function(e){e.Exercise="Exercise",e.MockTest="MockTest",e.PracticeTest="PracticeTest"})(he||(he={}));var ke;(function(e){e.Draft="draft",e.Publish="publish"})(ke||(ke={}));var Ae;(function(e){e.Academic="Academic",e.Exercise="Exercise",e.General="General"})(Ae||(Ae={}));var Ce;(function(e){e.MockTest="MockTest"})(Ce||(Ce={}));var be;(function(e){e.Draft="draft",e.Publish="publish"})(be||(be={}));var Oe;(function(e){e.Listening="Listening",e.Other="Other",e.Reading="Reading",e.Speaking="Speaking",e.Writing="Writing"})(Oe||(Oe={}));var Le;(function(e){e.After="after",e.Before="before"})(Le||(Le={}));var Me;(function(e){e.Div="div",e.H1="h1",e.H2="h2",e.H3="h3",e.H4="h4",e.H5="h5",e.H6="h6",e.P="p",e.Span="span"})(Me||(Me={}));var De;(function(e){e.Crawling="crawling",e.Pending="pending",e.Waiting="waiting"})(De||(De={}));var Ee;(function(e){e.Group="group",e.Page="page"})(Ee||(Ee={}));var Fe;(function(e){e.CreatedAt="createdAt",e.UpdatedAt="updatedAt"})(Fe||(Fe={}));var Ge;(function(e){e.File="file",e.Image="image",e.Video="video"})(Ge||(Ge={}));var Re;(function(e){e.Private="private",e.Public="public"})(Re||(Re={}));var je;(function(e){e.Call="call",e.Response="response",e.Update="update"})(je||(je={}));var Ue;(function(e){e.Draft="draft",e.Published="published"})(Ue||(Ue={}));var qe;(function(e){e.CreateDate="create_date",e.Id="id",e.UpdateDate="update_date"})(qe||(qe={}));var we;(function(e){e.All="all",e.Cancelled="cancelled",e.Confirmed="confirmed",e.Reserved="reserved"})(we||(we={}));var Ne;(function(e){e.CommentAdded="commentAdded",e.CommentReplied="commentReplied",e.NewPost="newPost",e.PostApproved="postApproved",e.PostPublished="postPublished",e.PostRejected="postRejected",e.PostRequestApproval="postRequestApproval"})(Ne||(Ne={}));var xe;(function(e){e.Cancel="cancel",e.Confirm="confirm"})(xe||(xe={}));var Ve;(function(e){e.Asc="asc",e.Desc="desc"})(Ve||(Ve={}));var Qe;(function(e){e.CreatedAt="createdAt",e.OrderId="orderId",e.UpdatedAt="updatedAt"})(Qe||(Qe={}));var Ke;(function(e){e.PurchaseCount="purchaseCount",e.Total="total"})(Ke||(Ke={}));var Ze;(function(e){e.Cancelled="cancelled",e.Confirmed="confirmed",e.Draft="draft",e.Dropped="dropped",e.Finished="finished",e.OnShipping="onShipping",e.Pending="pending",e.PendingShipping="pendingShipping",e.Picked="picked",e.PreparingFiles="preparingFiles"})(Ze||(Ze={}));var He;(function(e){e.Deleted="DELETED",e.Updated="UPDATED"})(He||(He={}));var ze;(function(e){e.AtStore="atStore",e.BankTransfer="bankTransfer",e.Cod="cod",e.Momo="momo",e.Onepay="onepay"})(ze||(ze={}));var Be;(function(e){e.Paid="paid",e.Pending="pending",e.Refund="refund"})(Be||(Be={}));var We;(function(e){e.Cancelled="cancelled",e.Confirmed="confirmed",e.Pending="pending"})(We||(We={}));var Ye;(function(e){e.PostCount="postCount",e.ReviewsAverage="reviewsAverage",e.ViewCount="viewCount"})(Ye||(Ye={}));var Xe;(function(e){e.CreatedAt="createdAt",e.Usage="usage"})(Xe||(Xe={}));var ye;(function(e){e.CreatedAt="createdAt",e.Title="title",e.UpdatedAt="updatedAt"})(ye||(ye={}));var Je;(function(e){e.CreatedAt="createdAt",e.NumOfComments="numOfComments",e.NumOfContextualLink="numOfContextualLink",e.PublishedAt="publishedAt",e.Title="title",e.UpdatedAt="updatedAt",e.Views="views"})(Je||(Je={}));var et;(function(e){e.Approved="approved",e.Deleted="deleted",e.Draft="draft",e.PendingApproval="pendingApproval",e.Published="published",e.Rejected="rejected"})(et||(et={}));var tt;(function(e){e.CreatedAt="createdAt",e.Name="name",e.UpdatedAt="updatedAt",e.Usage="usage"})(tt||(tt={}));var it;(function(e){e.Media="media",e.Number="number",e.Select="select",e.String="string"})(it||(it={}));var at;(function(e){e.CreatedAt="createdAt",e.Name="name",e.NumOfContextualLink="numOfContextualLink",e.Price="price",e.UpdatedAt="updatedAt"})(at||(at={}));var nt;(function(e){e.Deleted="deleted",e.Draft="draft",e.Publish="publish"})(nt||(nt={}));var ot;(function(e){e.SimpleProduct="SimpleProduct",e.VariableProduct="VariableProduct"})(ot||(ot={}));var st;(function(e){e.Question="QUESTION",e.Voting="VOTING"})(st||(st={}));var ct;(function(e){e.All="all",e.Appoiment="appoiment",e.Arrived="arrived",e.Cancelled="cancelled",e.ChangeEc="changeEc",e.Following="following",e.Pending="pending",e.Registered="registered"})(ct||(ct={}));var rt;(function(e){e.Conversation="conversation",e.Support="support",e.User="user"})(rt||(rt={}));var dt;(function(e){e.CreatedAt="createdAt",e.UpdatedAt="updatedAt"})(dt||(dt={}));var lt;(function(e){e.Pending="pending",e.Processed="processed"})(lt||(lt={}));var ut;(function(e){e.Hidden="hidden",e.Public="public"})(ut||(ut={}));var pt;(function(e){e.CreatedAt="createdAt",e.UpdatedAt="updatedAt"})(pt||(pt={}));var gt;(function(e){e.Draft="draft",e.Publish="publish"})(gt||(gt={}));var mt;(function(e){e.Accepted="accepted",e.Pending="pending",e.Rejected="rejected"})(mt||(mt={}));var ft;(function(e){e.JoinCourse="joinCourse",e.LeftCourse="leftCourse",e.ReEnroll="reEnroll"})(ft||(ft={}));var Tt;(function(e){e.CreatedAt="createdAt",e.Rating="rating"})(Tt||(Tt={}));var It;(function(e){e.Level="Level",e.MockTest="MockTest",e.Other="Other",e.Post="Post",e.Product="Product",e.Writing="Writing"})(It||(It={}));var vt;(function(e){e.MarkSeen="markSeen",e.TypingOff="typingOff",e.TypingOn="typingOn"})(vt||(vt={}));var $t;(function(e){e.Delivery="delivery",e.StorePickup="storePickup",e.Virtual="virtual"})($t||($t={}));var _t;(function(e){e.IeltsPracticeTestGroup="IeltsPracticeTestGroup",e.Post="Post",e.PostGroup="PostGroup",e.Product="Product",e.RecruitmentPost="RecruitmentPost",e.Tag="Tag"})(_t||(_t={}));var Pt;(function(e){e.App="App",e.Zim="Zim",e.ZimMobile="ZimMobile"})(Pt||(Pt={}));var St;(function(e){e.Active="active",e.All="all",e.Deactivated="deactivated",e.Inactive="inactive"})(St||(St={}));var ht;(function(e){e.Active="active",e.Deactivated="deactivated",e.Notactive="notactive"})(ht||(ht={}));var kt;(function(e){e.Active="active",e.Deactivated="deactivated",e.Inactive="inactive"})(kt||(kt={}));var At;(function(e){e.InStock="inStock",e.OutOfStock="outOfStock"})(At||(At={}));var Ct;(function(e){e.MultipleChoice="multipleChoice",e.SingleChoice="singleChoice",e.Text="text"})(Ct||(Ct={}));var bt;(function(e){e.Finished="finished",e.Pending="pending",e.Scanning="scanning"})(bt||(bt={}));var Ot;(function(e){e.Post="Post",e.Product="Product"})(Ot||(Ot={}));var Lt;(function(e){e.CreatedAt="createdAt",e.Name="name",e.PostUsage="postUsage",e.ProductUsage="productUsage",e.UpdatedAt="updatedAt",e.Usage="usage"})(Lt||(Lt={}));var Mt;(function(e){e.Received="received",e.Send="send"})(Mt||(Mt={}));var Dt;(function(e){e.Love="Love",e.NotOk="NotOk",e.Ok="Ok",e.Report="Report"})(Dt||(Dt={}));var Et;(function(e){e.Closed="Closed",e.Deleted="Deleted",e.Processing="Processing",e.Waiting="Waiting"})(Et||(Et={}));var Ft;(function(e){e.Apointment="Apointment",e.FinalTest="FinalTest",e.ToDo="ToDo"})(Ft||(Ft={}));var Gt;(function(e){e.FillInTheBlank="fillInTheBlank",e.MultipleChoice="multipleChoice",e.SingleChoice="singleChoice"})(Gt||(Gt={}));const Vt=t`
  fragment SEOField on CommonSeoConfig {
    title
    description
    ogDescription
    ogImage {
      id
      type
      path
      variants {
        id
        width
        height
        path
        type
      }
      filename
      title
      visibility
      width
      height
      usage
      createdAt
      updatedAt
    }
    ogTitle
    publisher
    noIndex
    noFollow
    canonicalUrl
    customHeadHtml
  }
`,Qt=t`
  fragment MockTestField on MockTestV2Type {
    id
    slug
    status
    fee
    total_slot
    available_slot
    school {
      id
      name
    }
    mockTestDetails {
      id
      date
      shift {
        id
        name
        start_time
        end_time
      }
      subject {
        id
        name
      }
      mock_test_shift_id
      mock_test_subject_id
      ecs {
        id
        full_name
      }
      teachers {
        id
        full_name
      }
    }
    subject {
      id
      name
    }
  }
`,Kt=t`
  fragment MockTestFieldForDetail on MockTestV2Type {
    id
    slug
    status
    fee
    type
    total_slot
    available_slot
    sent_email_status
    school {
      id
      name
    }
    mockTestDetails {
      id
      date
      shift {
        id
        name
        start_time
        end_time
      }
      subject {
        id
        name
      }
      mock_test_shift_id
      mock_test_subject_id
      available_slot
      total_slot
      currentSlot
      ecs {
        id
        full_name
      }
      teachers {
        id
        full_name
      }
    }
    examQuestionMockTest {
      id
      cat {
        id
        name
      }
      configMockTestBank {
        id
        questionLink
        answerLink
        referenceLink
        mockTestSubCat {
          id
          name
        }
      }
    }
  }
`;t`
  query IELTSPracticeTestGroupPagination(
    $page: Int
    $limit: Int
    $status: [IeltsPracticeTestGroupStatus]
    $level: [IeltsPracticeTestLevel]
  ) @api(name: "zim") {
    IELTSPracticeTestGroupPagination(
      page: $page
      limit: $limit
      status: $status
      level: $level
    ) {
      hasNextPage
      hasPrevPage
      limit
      nextPage
      page
      pagingCounter
      prevPage
      totalDocs
      totalPages
      docs {
        id
        slug
        title
        level
        status
        active
        categories {
          id
          title
          description
        }
        practiceTests {
          id
          title
        }
      }
    }
  }
`;t`
  query IELTSPracticeTestPagination(
    $page: Int
    $limit: Int
    $type: [IeltsPracticeTestType]
    $status: [IeltsPracticeTestStatus]
    $level: [IeltsPracticeTestLevel]
    $format: [IeltsPracticeTestFormat]
  ) @api(name: "zim") {
    IELTSPracticeTestPagination(
      page: $page
      limit: $limit
      type: $type
      status: $status
      level: $level
      format: $format
    ) {
      hasNextPage
      hasPrevPage
      limit
      nextPage
      page
      pagingCounter
      prevPage
      totalDocs
      totalPages
      docs {
        ...ModuleField
      }
    }
  }
  ${T}
`;t`
  query IELTSPracticeTest($id: ObjectID!) @api(name: "zim") {
    IELTSPracticeTest(id: $id) {
      ...ModuleField
    }
  }
  ${T}
`;t`
  query IELTSPracticeTestGroup($id: ObjectID!) @api(name: "zim") {
    IELTSPracticeTestGroup(id: $id) {
      id
      title
      slug
      description
      level
      status
      active
      practiceTests {
        id
        title
        type
        status
      }
      categories {
        id
        title
        description
        slug
      }
      featureMedias {
        id
        type
        path
        variants {
          id
          width
          height
          path
          type
        }
        filename
        title
        visibility
        width
        height
        usage
        createdAt
        updatedAt
      }
      seo {
        ...SEOField
      }
    }
  }
  ${Vt}
`;const ci=t`
  query getMockTestDetailV2($idOrSlug: String) @api(name: "appZim") {
    getMockTestDetailV2(idOrSlug: $idOrSlug) {
      id
      status
      details {
        date
      }
      fee
      school {
        id
        name
      }
      subject {
        id
        name
      }
    }
  }
`,ri=t`
  query getMockTestDetailV2($idOrSlug: String) @api(name: "appZim") {
    getMockTestDetailV2(idOrSlug: $idOrSlug) {
      ...MockTestFieldForDetail
    }
  }
  ${Kt}
`,di=(e=[])=>t`
  query getMockTestDetailByField($idOrSlug: String) @api(name: "appZim") {
    getMockTestDetailV2(idOrSlug: $idOrSlug) {
      id
      ${e.join()}
    }
  }
`,li=t`
  query getListMockTestByRoleV2(
    $q: String
    $type: EnumMockTestType
    $status: EnumBaseStatus
    $schoolId: Int
    $fromDate: String
    $toDate: String
    $offset: Int
    $limit: Int
    $subjectId: Int
  ) @api(name: "appZim") {
    getListMockTestByRoleV2(
      q: $q
      type: $type
      status: $status
      schoolId: $schoolId
      fromDate: $fromDate
      toDate: $toDate
      offset: $offset
      limit: $limit
      subjectId: $subjectId
    ) {
      edges {
        node {
          ...MockTestField
        }
      }
      totalCount
      pageInfo {
        hasNextPage
        hasPreviousPage
      }
    }
  }
  ${Qt}
`,ui=t`
  query getListTeacherRegisterForFunction(
    $type: Int!
    $date: String!
    $schoolId: Int
    $shift: Int!
  ) @api(name: "appZim") {
    getListTeacherRegisterForFunction(
      type: $type
      date: $date
      schoolId: $schoolId
      shift: $shift
    ) {
      id
      full_name
    }
  }
`,pi=t`
  query getListEcForCenter($schoolId: Int) @api(name: "appZim") {
    getListEcForCenter(schoolId: $schoolId) {
      id
      full_name
    }
  }
`,gi=t`
  query getListMockTestOrderByRoles(
    $mocktestId: String
    $status: MockTestStatusEnum
  ) @api(name: "appZim") {
    getListMockTestOrderByRoles(mocktestId: $mocktestId, status: $status) {
      id
      id_number
      status
      order_id
      student {
        id
        full_name
        phone
        email
      }
      owner {
        name
        email
        phone
      }
      moduleV2 {
        id
        name
        create_date
        status
      }
      mockTestDetailV2 {
        date
        id
        shift {
          id
          start_time
          end_time
        }
        subject {
          id
          name
        }
      }
      schedulesV2 {
        is_missing
        subject {
          id
        }
        timePreview
        platform_id
      }
      feedback_writing1
      feedback_writing2
      teacherV2 {
        id
        full_name
      }
      catV2 {
        id
        name
      }
      mockTestResultv2 {
        id
        reading
        writing1
        writing2
        writing_note
        overall
        listening
        speaking
        send_email_status
        speakingNote
        sessions {
          sessionIdReading
          sessionIdWriting
          sessionIdSpeaking
          sessionIdListening
          sessionIdWritingTask1
          sessionIdWritingTask2
        }
        mock_test_writings {
          id
          essay_correction {
            id
            title
          }
          score
          status
        }
      }
      imageMockTestOrder {
        id
        mockTestOrderId
        mockTestSubCatId
        mockTestSubCat {
          id
          name
        }
        url
      }
    }
  }
`,mi=t`
  query ($catId: Int!) @api(name: "appZim") {
    getSubCatMockTestForStudent(catId: $catId) {
      id
      name
    }
  }
`,fi=t`
  query getImageMockTestOrderBySubCat($orderId: Int!, $mockTestSubCatId: Int)
  @api(name: "appZim") {
    getImageMockTestOrderBySubCat(
      mockTestSubCatId: $mockTestSubCatId
      orderId: $orderId
    ) {
      id
      mockTestOrderId
      mockTestSubCatId
      mockTestSubCat {
        id
        name
      }
      url
    }
  }
`;t`
  query calcWritingAverage($writingTask1: Float, $writingTask2: Float)
  @api(name: "appZim") {
    calcWritingAverage(writingTask1: $writingTask1, writingTask2: $writingTask2)
  }
`;const Ti=t`
  query calcOverall(
    $listening: Float!
    $reading: Float!
    $writing: Float
    $writingTask1: Float
    $writingTask2: Float
    $speaking: Float!
  ) @api(name: "appZim") {
    calcOverall(
      listening: $listening
      reading: $reading
      writing: $writing
      speaking: $speaking
      writingTask1: $writingTask1
      writingTask2: $writingTask2
    )
  }
`,Ii=t`
  query getResultMockTestStudent($mocktestId: String) @api(name: "appZim") {
    getResultMockTestStudent(mocktestId: $mocktestId) {
      id
      listening
      reading
      writing1
      writing2
      writing_note
      send_email_status
      speaking
      speakingNote
    }
  }
`,vi=t`
  query getListTeacherAssignedMockTest($mockTestId: Int!) @api(name: "appZim") {
    getListTeacherAssignedMockTest(mockTestId: $mockTestId) {
      id
      full_name
    }
  }
`,$i=t`
  query getListTeacherOfMockTestWithSchedule($mockTestId: Int, $orderId: Int)
  @api(name: "appZim") {
    getListTeacherOfMockTestWithSchedule(
      mockTestId: $mockTestId
      orderId: $orderId
    ) {
      teacher_id
      teacher_name
      schedule {
        mock_test_detail_id
        isChange
        id_number
        lRW
        speaking
      }
    }
  }
`,Zt=(e=[])=>f(e.map(i=>i.shift),"id").map(i=>{const o=e.filter(s=>s.shift.id===i.id)||[],c=o.reduce((s,n)=>(s.push(u(l({},n.subject||{}),{mockTestDetailId:n.id,ecs:n.ecs,total_slot:n.total_slot,available_slot:n.available_slot,currentSlot:n.currentSlot})),s),[]).sort((s,n)=>s.id-n.id),Rt=o.reduce((s,n)=>s.concat(n.ecs),[]),jt=o.reduce((s,n)=>s.concat(n.teachers),[]);return u(l({},i),{subjects:f(c,"id"),ecs:Rt,teachers:jt})});function _i(e=[]){var i;const a=e.reduce(function(o,c){return o[c.date]=o[c.date]||[],o[c.date].push(c),o},Object.create(null));return(i=Object.entries(a))==null?void 0:i.map(([o,c])=>({date:o,shifts:Zt(c)}))}export{Jt as C,ne as E,ci as G,we as M,xe as O,oi as S,ei as U,li as a,Bt as b,ii as c,mi as d,fi as e,ai as f,_i as g,Ii as h,Ti as i,ni as j,$i as k,si as l,gi as m,Yt as n,di as o,ri as p,ui as q,pi as r,Wt as s,Xt as t,yt as u,vi as v,ti as w};
