var sa=Object.defineProperty,oa=Object.defineProperties;var ia=Object.getOwnPropertyDescriptors;var us=Object.getOwnPropertySymbols;var ci=Object.prototype.hasOwnProperty,mi=Object.prototype.propertyIsEnumerable;var di=(e,s,o)=>s in e?sa(e,s,{enumerable:!0,configurable:!0,writable:!0,value:o}):e[s]=o,x=(e,s)=>{for(var o in s||(s={}))ci.call(s,o)&&di(e,o,s[o]);if(us)for(var o of us(s))mi.call(s,o)&&di(e,o,s[o]);return e},y=(e,s)=>oa(e,ia(s));var Ue=(e,s)=>{var o={};for(var l in e)ci.call(e,l)&&s.indexOf(l)<0&&(o[l]=e[l]);if(e!=null&&us)for(var l of us(e))s.indexOf(l)<0&&mi.call(e,l)&&(o[l]=e[l]);return o};import{g as f,o as la,A as Os,O as aa,I as ra,r as Ls,M as na,c as ua,b as ca,a as pi,W as ma,T as da,d as bi,s as pa,e as ba,f as ga,S as ha,h as c,j as t,i as fa,F as cs,k as Bs,l as ms,u as j,m as ds,B as S,n as gi,p as hi,q as Y,V as xa,t as De,v as Rs,w as Na,x as fi,y as ie,z as M,H as q,R as St,C as xi,D as Ea,E as va,G as Aa,J as Ni,K as Ei,L as vi,N as Ai,P as Ti,Q as ps,U as be,X as Ta,Y as ya,Z as Ke,_ as Da,$ as xt,a0 as at,a1 as ve,a2 as ja,a3 as yi,a4 as Di,a5 as ji,a6 as Us,a7 as Ci,a8 as $i,a9 as Si,aa as Gt,ab as Vi,ac as Je,ad as Pe,ae as ki,af as bs,ag as Gs,ah as Hs,ai as Vt,aj as Nt,ak as kt,al as Et,am as wt,an as It,ao as Ht,ap as Wt,aq as Ws,ar as Xe,as as vt,at as Ca,au as At,av as $a,aw as wi,ax as Sa,ay as Qs,az as Ii,aA as Va,aB as Pi,aC as gs,aD as Pt,aE as rt,aF as _i,aG as ka,aH as Zs,aI as Ys,aJ as Fi,aK as hs,aL as Ks,aM as Mi,aN as wa,aO as Ia,aP as Pa,aQ as _a,aR as qi,aS as Js,aT as Fa,aU as Ma,aV as qa,aW as za,aX as Oa,aY as La,aZ as Qt,a_ as Zt,a$ as Ba,b0 as Yt,b1 as Xs,b2 as Ra,b3 as Ua,b4 as Ga,b5 as Ha,b6 as Wa,b7 as Qa,b8 as Za,b9 as Ya,ba as Ka,bb as Ja,bc as Xa,bd as er,be as tr,bf as sr,bg as or,bh as zi,bi as ir,bj as lr,bk as Oi,bl as ar,bm as rr,bn as nr,bo as ur,bp as cr,bq as mr,br as dr,bs as pr,bt as br,bu as gr,bv as hr,bw as fr,bx as Li,by as Bi,bz as xr,bA as eo,bB as Nr,bC as Er,bD as vr,bE as Ar,bF as Tr,bG as yr,bH as Dr,bI as jr,bJ as Cr,bK as Ri,bL as $r,bM as Sr,bN as Vr,bO as kr,bP as wr,bQ as Ir,bR as Ui,bS as Kt,bT as Pr,bU as _r,bV as xe,bW as Fr,bX as Mr,bY as qr,bZ as to,b_ as so,b$ as Gi,c0 as Jt,c1 as zr,c2 as Or,c3 as Lr,c4 as Br,c5 as Rr,c6 as Ur,c7 as Gr,c8 as _t,c9 as Le,ca as oo,cb as io,cc as Hi,cd as Hr,ce as Wi,cf as Qi,cg as Wr,ch as Qr,ci as Zi,cj as Zr,ck as Yr,cl as fs,cm as Kr,cn as Jr,co as Xr,cp as en,cq as tn,cr as sn,cs as Yi,ct as on,cu as ln,cv as an,cw as rn,cx as nn,cy as un,cz as cn,cA as mn,cB as dn,cC as pn,cD as bn,cE as gn,cF as hn,cG as fn,cH as xn,cI as Nn,cJ as En,cK as vn,cL as An}from"./vendor.js";const Tn=function(){const s=document.createElement("link").relList;if(s&&s.supports&&s.supports("modulepreload"))return;for(const a of document.querySelectorAll('link[rel="modulepreload"]'))l(a);new MutationObserver(a=>{for(const i of a)if(i.type==="childList")for(const n of i.addedNodes)n.tagName==="LINK"&&n.rel==="modulepreload"&&l(n)}).observe(document,{childList:!0,subtree:!0});function o(a){const i={};return a.integrity&&(i.integrity=a.integrity),a.referrerpolicy&&(i.referrerPolicy=a.referrerpolicy),a.crossorigin==="use-credentials"?i.credentials="include":a.crossorigin==="anonymous"?i.credentials="omit":i.credentials="same-origin",i}function l(a){if(a.ep)return;a.ep=!0;const i=o(a);fetch(a.href,i)}};Tn();const yn="modulepreload",Ki={},Dn="/",Ne=function(s,o){return!o||o.length===0?s():Promise.all(o.map(l=>{if(l=`${Dn}${l}`,l in Ki)return;Ki[l]=!0;const a=l.endsWith(".css"),i=a?'[rel="stylesheet"]':"";if(document.querySelector(`link[href="${l}"]${i}`))return;const n=document.createElement("link");if(n.rel=a?"stylesheet":yn,a||(n.as="script",n.crossOrigin=""),n.href=l,document.head.appendChild(n),a)return new Promise((r,u)=>{n.addEventListener("load",r),n.addEventListener("error",u)})})).then(()=>s())};const Xt=f`
  fragment Post on Post {
    id
    title
    slug
    excerpt
    content
    status
    numOfComments
    categories {
      id
      title
      slug
    }
    tags {
      id
      name
      slug
    }
    postGroups {
      id
      title
      slug
    }
    publishedAt
    featureMedias {
      id
      type
      path
      variants {
        id
        type
        width
        height
        path
      }
    }
    user {
      id
      role
      username
      fullName
      phone
      gender
      avatar
    }
    censor {
      id
      fullName
    }
    createdAt
    updatedAt
    numOfContextualLink
  }
`;f`
  query posts(
    $after: String
    $before: String
    $first: Int
    $last: Int
    $categorySlugs: [String]
    $relatedPost: ObjectID
  ) @api(name: "zim") {
    posts(
      after: $after
      before: $before
      first: $first
      last: $last
      categorySlugs: $categorySlugs
      relatedPost: $relatedPost
    ) {
      totalCount
      edges {
        cursor
        node {
          ...Post
        }
      }
      pageInfo {
        startCursor
        endCursor
        hasNextPage
        hasPreviousPage
      }
    }
  }
  ${Xt}
`;f`
  query ($idOrSlug: String!) @api(name: "zim") {
    post(idOrSlug: $idOrSlug) {
      ...Post
      seo {
        title
        description
        ogDescription
        ogImage {
          id
          path
        }
        ogTitle
        publisher
        noIndex
        noFollow
        canonicalUrl
        customHeadHtml
      }
      rejectReason
    }
  }
  ${Xt}
`;f`
  query postPagination(
    $searchString: String
    $page: Int
    $limit: Int
    $categoryIds: [ObjectID]
    $tagIds: [ObjectID]
    $postGroupIds: [ObjectID]
    $userIds: [Int]
    $statuses: [PostStatus]
    $censorUserIds: [Int]
    $orderBy: PostOrderBy
    $order: OrderDirection
  ) @api(name: "zim") {
    postPagination(
      searchString: $searchString
      page: $page
      limit: $limit
      categoryIds: $categoryIds
      tagIds: $tagIds
      postGroupIds: $postGroupIds
      userIds: $userIds
      statuses: $statuses
      censorUserIds: $censorUserIds
      orderBy: $orderBy
      order: $order
    ) {
      hasNextPage
      hasPrevPage
      limit
      nextPage
      prevPage
      totalDocs
      totalPages
      docs {
        ...Post
      }
    }
  }
  ${Xt}
`;f`
  mutation updatePost($postId: ObjectID!, $input: UpdatePostData!)
  @api(name: "zim") {
    updatePost(input: $input, postId: $postId) {
      ...Post
    }
  }
  ${Xt}
`;f`
  mutation createPost($input: CreatePostData!) @api(name: "zim") {
    createPost(input: $input) {
      ...Post
    }
  }
  ${Xt}
`;f`
  mutation createTag($name: String!, $description: String) @api(name: "zim") {
    createTag(description: $description, name: $name) {
      id
      name
      slug
      description
    }
  }
`;f`
  mutation publishPost($postId: ObjectID!) @api(name: "zim") {
    publishPost(postId: $postId) {
      id
      title
      status
    }
  }
`;f`
  mutation unpublishPost($postId: ObjectID!) @api(name: "zim") {
    unpublishPost(postId: $postId) {
      id
      title
      status
    }
  }
`;f`
  mutation requestPostForApproval($postId: ObjectID!) @api(name: "zim") {
    requestPostForApproval(postId: $postId) {
      id
      title
      status
    }
  }
`;f`
  mutation approveOrRejectPost(
    $postId: ObjectID!
    $status: ApproveOrRejectPostStatus
    $rejectReason: String
    $updatedContent: String
  ) @api(name: "zim") {
    approveOrRejectPost(
      postId: $postId
      status: $status
      rejectReason: $rejectReason
      updatedContent: $updatedContent
    ) {
      id
      title
      status
      rejectReason
      content
    }
  }
`;f`
  mutation reassignPost($postId: ObjectID!, $userId: Int!) @api(name: "zim") {
    reassignPost(postId: $postId, userId: $userId) {
      id
      user {
        id
        birthday
        email
        fullName
        avatar
        roleId
        role
      }
    }
  }
`;f`
  mutation signInV2($username: String!, $password: String!) @api(name: "zim") {
    signInV2(username: $username, password: $password) {
      token
      expired_at
      refreshToken
    }
  }
`;const jn=f`
  query @api(name: "zim") {
    me {
      id
      role
      roleId
      username
      fullName
      phone
      birthday
      gender
      avatar
      email
      canCensor
    }
  }
`,Cn=f`
  mutation signOut @api(name: "zim") {
    signOut
  }
`;f`
  mutation createPostCategory($input: PostCategoryInput!) @api(name: "zim") {
    createPostCategory(input: $input) {
      id
      title
      slug
      description
    }
  }
`;const Ji=f`
  fragment CategoryFields on PostCategory {
    id
    title
    slug
    parentCategory {
      id
      title
      slug
    }
    description
  }
`;f`
  query postCategoriesTree @api(name: "zim") {
    postCategoriesTree {
      ...CategoryFields
      children {
        ...CategoryFields
        children {
          ...CategoryFields
          children {
            ...CategoryFields
          }
        }
      }
    }
  }
  ${Ji}
`;f`
  query postCategories($flatten: Boolean, $search: String, $parent: ObjectID)
  @api(name: "zim") {
    postCategories(search: $search, parent: $parent, flatten: $flatten) {
      ...CategoryFields
      children {
        ...CategoryFields
      }
    }
  }
  ${Ji}
`;f`
  mutation updatePostCategory($id: ObjectID!, $input: UpdatePostCategoryInput!)
  @api(name: "zim") {
    updatePostCategory(id: $id, input: $input) {
      id
      title
      slug
      description
    }
  }
`;f`
  mutation deletePostCategory($id: ObjectID!) @api(name: "zim") {
    deletePostCategory(id: $id)
  }
`;f`
  query comments(
    $type: CommentType!
    $after: String
    $before: String
    $first: Int
    $last: Int
    $refId: String!
    $parent: ObjectID
  ) @api(name: "zim") {
    comments(
      type: $type
      after: $after
      before: $before
      first: $first
      last: $last
      refId: $refId
      parent: $parent
    ) {
      totalCount
      edges {
        cursor
        node {
          ... on PostComment {
            id
            user {
              id
              role
              username
              fullName
              phone
              birthday
              gender
              avatar
            }
            content
            parent {
              id
              content
              user {
                id
                role
                username
                fullName
                phone
                birthday
                gender
                avatar
              }
            }
            liked
            numOfLikes
            createdAt
            updatedAt
          }
          __typename
        }
      }
      pageInfo {
        startCursor
        endCursor
        hasNextPage
        hasPreviousPage
      }
    }
  }
`;f`
  mutation postComment(
    $content: String!
    $type: CommentType
    $ref: ObjectID!
    $parent: ObjectID
  ) @api(name: "zim") {
    postComment(content: $content, type: $type, ref: $ref, parent: $parent) {
      ... on PostComment {
        id
        content
        user {
          id
          role
          username
          fullName
          phone
          birthday
          gender
          avatar
        }
        parent {
          id
          content
          user {
            id
            role
            username
            fullName
            phone
            birthday
            gender
            avatar
          }
          liked
          numOfLikes
          createdAt
          updatedAt
        }
        liked
        numOfLikes
        createdAt
        updatedAt
      }
      __typename
    }
  }
`;f`
  mutation likeOrUnlikeComment($commentId: ObjectID!, $liked: Boolean!)
  @api(name: "zim") {
    likeOrUnlikeComment(commentId: $commentId, liked: $liked) {
      ... on PostComment {
        id
        liked
        numOfLikes
      }
    }
  }
`;const Xi=f`
  fragment Course on CoursePlanType {
    id
    slug
    start_date
    program {
      id
      name
      cat {
        id
        name
      }
      subject {
        id
        name
      }
      program_type {
        id
        name
      }
      level {
        subject_id
        id
        name
        graduation
        status
      }
      fee
      status
      sub_cats {
        id
        cat_id
        name
        status
        parent_id
      }
    }
    subject {
      id
      name
      status
    }
    level_out {
      subject_id
      id
      name
      graduation
    }
    type
    size {
      id
      name
      size
      status
    }
    school {
      id
      city {
        id
      }
      district {
        id
      }
      name
      address
      phone
      color_code
      lat
      lon
      status
    }
    curriculum {
      id
      name
      program {
        id
      }
      shift_minute
      total_lesson
      status
    }
    shifts {
      id
      start_time
      end_time
      status
      shift_minute
    }
    day_of_weeks {
      id
      name
    }
    config_fee {
      detail_name
      detail_id
      type
      fee
    }
    fee
    status
    ec_id
    teacher_lead_id
    description
    featured_image
  }
`,wd=f`
  query getListCoursePagination(
    $order_by: String
    $order: String
    $q: String
    $school_id: Int
    $city_id: Int
    $program_id: Int
    $program_type_id: Int
    $limit: Int
    $cat_id: Int
    $level_id: Int
    $subject_id: Int
    $status: [CourseStatusEnum]
    $type: EnumCourseTypeEnum
    $fromdate: String
    $todate: String
    $page: Int
    $bussiness_partner_id: Int
  ) @api(name: "appZim") {
    getListCoursePagination(
      q: $q
      status: $status
      subject_id: $subject_id
      level_id: $level_id
      cat_id: $cat_id
      school_id: $school_id
      city_id: $city_id
      program_id: $program_id
      program_type_id: $program_type_id
      type: $type
      fromdate: $fromdate
      todate: $todate
      order_by: $order_by
      order: $order
      page: $page
      limit: $limit
      bussiness_partner_id: $bussiness_partner_id
    ) {
      hasNextPage
      hasPrevPage
      limit
      page
      pageNext
      pagePrev
      pagingCounter
      totalPages
      totalDocs
      docs {
        id
        slug
        name
        start_date
        subcats {
          id
          cat_id
          name
          status
          parent_id
        }
        subject {
          id
          name
          status
        }
        school {
          id
          name
        }

        shifts {
          id
          start_time
          end_time
        }
        level_out {
          subject_id
          id
          name
          graduation
        }
        day_of_weeks {
          id
          name
        }
        size {
          id
          name
          size
          status
        }
        fee
        status
        total_register
        total_paid
        config_fee {
          type
          fee
          detail_id
          detail_name
        }
        end_date
        type
        ec {
          id
          full_name
        }
        teachers {
          id
          user_name
          full_name
        }
      }
    }
  }
`;f`
  query coursesPlan($offset: Int, $limit: Int, $school_id: Int, $city_id: Int)
  @api(name: "appZim") {
    coursesPlan(
      school_id: $school_id
      city_id: $city_id
      offset: $offset
      limit: $limit
    ) {
      totalCount
      edges {
        node {
          ...Course
        }
      }
      pageInfo {
        hasNextPage
        hasPreviousPage
      }
    }
  }
  ${Xi}
`;f`
  query coursesPlan($limit: Int) @api(name: "appZim") {
    coursesPlan(status: incoming, limit: $limit) {
      edges {
        node {
          id
          slug
        }
      }
    }
  }
`;f`
  query coursePlan($idOrSlug: String!) @api(name: "appZim") {
    coursePlan(idOrSlug: $idOrSlug) {
      ...Course
    }
  }
  ${Xi}
`;f`
  mutation createAdvisory(
    $name: String
    $phone: String!
    $email: String!
    $city_id: Int!
    $gclid: String
    $fbclid: String
    $link: String
    $note: String
  ) @api(name: "appZim") {
    createAdvisory(
      name: $name
      phone: $phone
      email: $email
      city_id: $city_id
      gclid: $gclid
      fbclid: $fbclid
      link: $link
      note: $note
    ) {
      success
      error
      message
    }
  }
`;f`
  mutation checkout($input: CheckoutInput!) @api(name: "zim") {
    checkout(input: $input) {
      paymentUrl
      order {
        id
        status
        paymentMethod
        paymentStatus
        shippingMethod
        orderId
        email
        shippingAddress {
          recipientName
          recipientPhone
          email
          street1
          street2
          city
          state
          zip
          country
        }
        total
      }
    }
  }
`;const el=f`
  fragment OrderBaseFields on Order {
    id
    status
    paymentMethod
    paymentStatus
    shippingMethod
    orderId
    status
    email
    shippingAddress {
      recipientName
      recipientPhone
      email
      street1
      street2
      city
      state
      zip
      country
    }
    total
    createdAt
    updatedAt
  }
`,$n=f`
  fragment OrderDetailFields on Order {
    ...OrderBaseFields
    user {
      id
      fullName
    }
    billingAddress {
      recipientName
      recipientPhone
      email
      street1
      street2
      city
      state
      zip
      country
    }
    attachments {
      id
      filename
      filepath
    }
    total
    totalPreDiscount
    discount
    coupons
    items {
      quantity
      total
      preTaxTotal
      price
      salePrice
      tax
      type
      item {
        ... on SimpleProduct {
          id
          name
          slug
          images {
            id
            path
          }
          sku
          attributes {
            isVariantAttribute
            attribute {
              id
              name
            }
            value
          }
        }
        ... on Post {
          id
          excerpt
          featureMedias {
            id
            path
          }
          title
          slug
        }
        ... on MockTestItem {
          data
        }
      }
    }
  }
  ${el}
`;f`
  query order($idOrCode: String!) @api(name: "zim") {
    order(idOrCode: $idOrCode) {
      ...OrderBaseFields
      ...OrderDetailFields
    }
  }
  ${$n}
`;f`
  query orderPagination(
    $searchString: String
    $statuses: [OrderStatus]
    $userIds: [Int]
    $orderBy: OrderOrderBy
    $order: OrderDirection
    $page: Int
    $limit: Int
    $itemType: [CheckoutProduct]
  ) @api(name: "zim") {
    orderPagination(
      searchString: $searchString
      statuses: $statuses
      userIds: $userIds
      order: $order
      orderBy: $orderBy
      page: $page
      limit: $limit
      itemType: $itemType
    ) {
      totalPages
      totalDocs
      docs {
        ...OrderBaseFields
      }
    }
  }
  ${el}
`;f`
  query tags(
    $after: String
    $before: String
    $first: Int
    $last: Int
    $search: String
  ) @api(name: "zim") {
    tags(
      after: $after
      before: $before
      first: $first
      last: $last
      search: $search
    ) {
      edges {
        node {
          id
          name
          description
          slug
        }
      }
      pageInfo {
        hasNextPage
        hasPreviousPage
        startCursor
        endCursor
      }
    }
  }
`;f`
  query postCategories($search: String, $parent: ObjectID) @api(name: "zim") {
    postCategories(search: $search, parent: $parent) {
      id
      title
      slug
      parentCategory {
        id
        title
        slug
      }
      children {
        id
        title
        slug
        parentCategory {
          id
          title
          slug
        }
      }
    }
  }
`;const Tt=f`
  fragment ProductBaseField on BaseProduct {
    ... on SimpleProduct {
      id
      sku
      name
      slug
      regularPrice
      salePrice
      salePriceFrom
      salePriceTo
      description
      shortDescription
      numOfContextualLink
      attributes {
        id
        attribute {
          id
          name
          type
          options
        }
        value
        isVariantAttribute
      }
      parentProduct {
        id
      }
      price
      categories {
        id
        title
        slug
      }
      seo {
        title
        description
        ogDescription
        ogImage {
          id
          path
        }
        ogTitle
        publisher
        noIndex
        noFollow
        canonicalUrl
        customHeadHtml
      }
      images {
        id
        path
      }
      status
      productType
      tags {
        id
        name
        description
        slug
      }
      downloadable
      attributes {
        attribute {
          id
          type
          name
          options
        }
        value
      }
      inStock
      inventories {
        id
        inStock
        manageStock
        reserved
        storeId
        inventory
        store {
          id
          name
        }
      }
      createdAt
      updatedAt
    }
    ... on VariableProduct {
      id
      sku
      name
      slug
      categories {
        id
        title
        slug
      }
      images {
        id
        filename
        path
      }
      description
      shortDescription
      status
      minPrice
      maxPrice
      minRegularPrice
      maxRegularPrice
      productType
      inStock
      variationAttributes {
        id
        name
        type
        options
      }
      createdAt
      updatedAt
      seo {
        title
        description
        ogDescription
        ogImage {
          id
          path
        }
        ogTitle
        publisher
        noIndex
        noFollow
        canonicalUrl
        customHeadHtml
      }
    }
  }
`,es=f`
  fragment ProductDetailFields on BaseProduct {
    ...ProductBaseField
    ... on SimpleProduct {
      tags {
        id
        name
      }
      downloadable
      downloads {
        id
        filename
        path
      }

      attributes {
        id
        attribute {
          id
          name
          type
          options
        }
        value
        isVariantAttribute
        media {
          id
          type
          filename
          path
        }
      }
      parentProduct {
        id
        slug
        name
        status
        variationAttributes {
          id
          name
          type
          options
        }
      }
    }
    ... on VariableProduct {
      childCount
      childPublishedCount
      attributes {
        id
        attribute {
          id
          name
          type
          options
        }
        value
        media {
          id
          type
          filename
          path
        }
      }
      tags {
        id
        name
      }
    }
  }
  ${Tt}
`;f`
  query product($id: ObjectID!) @api(name: "zim") {
    productAdmin(id: $id) {
      ...ProductBaseField
      ...ProductDetailFields
    }
  }
  ${es}
`;f`
  query productPagination(
    $searchString: String
    $statuses: [ProductStatus]
    $orderBy: ProductOrderBy
    $order: OrderDirection
    $page: Int
    $limit: Int
    $parentProduct: ObjectID
    $tagSlugs: [String]
    $flatten: Boolean
    $simpleOnly: Boolean
    $tagIds: [ObjectID]
  ) @api(name: "zim") {
    productPagination(
      searchString: $searchString
      statuses: $statuses
      parentProduct: $parentProduct
      order: $order
      orderBy: $orderBy
      page: $page
      limit: $limit
      tagSlugs: $tagSlugs
      flatten: $flatten
      simpleOnly: $simpleOnly
      tagIds: $tagIds
    ) {
      totalPages
      totalDocs
      docs {
        ...ProductBaseField
      }
    }
  }
  ${Tt}
`;f`
  query publishedProducts(
    $search: String
    $after: String
    $before: String
    $first: Int
    $last: Int
  ) @api(name: "zim") {
    publishedProducts(
      after: $after
      before: $before
      first: $first
      last: $last
      search: $search
    ) {
      totalCount
      pageInfo {
        hasNextPage
        hasPreviousPage
        startCursor
        endCursor
      }
      edges {
        cursor
        node {
          ...ProductBaseField
        }
      }
    }
  }
  ${Tt}
`;f`
  query productVariants($parentProductId: ObjectID!) @api(name: "zim") {
    productVariants(parentProductId: $parentProductId) {
      ...ProductBaseField
    }
  }
  ${Tt}
`;f`
  query (
    $searchString: String
    $excludeIds: [ObjectID]
    $page: Int
    $limit: Int
  ) @api(name: "zim") {
    productAttributePagination(
      page: $page
      limit: $limit
      excludeIds: $excludeIds
      searchString: $searchString
    ) {
      totalDocs
      docs {
        id
        name
        options
        type
      }
    }
  }
`;f`
  query ($id: ObjectID!) @api(name: "zim") {
    productAttribute(id: $id) {
      id
      name
      options
      type
    }
  }
`;f`
  query @api(name: "zim") {
    productCategoriesTree {
      id
      title
      slug
      parentCategory {
        id
        title
        slug
      }
      children {
        id
        title
        slug
        parentCategory {
          id
          title
          slug
        }
      }
    }
  }
`;f`
  query productCategories($search: String, $parent: ObjectID)
  @api(name: "zim") {
    productCategories(search: $search, parent: $parent) {
      id
      title
      slug
      parentCategory {
        id
        title
        slug
      }
      children {
        id
        title
        slug
        parentCategory {
          id
          title
          slug
        }
      }
    }
  }
`;f`
  query slugExist($slug: String!, $type: SlugType, $refId: ObjectID)
  @api(name: "zim") {
    slugExists(type: $type, slug: $slug, refId: $refId)
  }
`;f`
  query titleExists($title: String!, $type: SlugType, $refId: ObjectID)
  @api(name: "zim") {
    titleExists(type: $type, title: $title, refId: $refId)
  }
`;f`
  query postViewReport(
    $startTime: DateTime
    $endTime: DateTime
    $page: Int
    $limit: Int
    $userId: Int
  ) @api(name: "zim") {
    postViewReport(
      startTime: $startTime
      endTime: $endTime
      page: $page
      limit: $limit
      userId: $userId
    ) {
      totalDocs
      docs {
        post {
          id
          title
          slug
          user {
            id
            fullName
          }
        }
        viewCount
      }
    }
  }
`;f`
  query generalPostViewByDate(
    $range: FromToDateInput
    $dateViewMode: DateViewMode
  ) @api(name: "zim") {
    generalPostViewByDate(range: $range, dateViewMode: $dateViewMode) {
      label
      viewCount
      postCount
    }
  }
`;f`
  query postViewReportByUser(
    $startTime: DateTime
    $endTime: DateTime
    $page: Int
    $limit: Int
  ) @api(name: "zim") {
    postViewReportByUser(
      startTime: $startTime
      endTime: $endTime
      page: $page
      limit: $limit
    ) {
      totalDocs
      docs {
        user {
          id
          fullName
        }
        postCount
        viewCount
      }
    }
  }
`;f`
  query postReportBySpecificUser(
    $userId: Int!
    $startTime: DateTime
    $endTime: DateTime
  ) @api(name: "zim") {
    postReportBySpecificUser(
      startTime: $startTime
      endTime: $endTime
      userId: $userId
    ) {
      totalLifetimePosts
      totalLifetimePublishedPosts
      totalLifetimeViews
      totalPosts
      totalPublishedPosts
    }
  }
`;f`
  query postReportByStatus($userId: Int) @api(name: "zim") {
    postReportByStatus(userId: $userId) {
      status
      count
    }
  }
`;f`
  query postByUser(
    $page: Int
    $limit: Int
    $order: OrderDirection
    $orderBy: PostByUserSort
    $userFilter: UserWithPostUserFilter
  ) @api(name: "zim") {
    postByUser(
      page: $page
      limit: $limit
      order: $order
      orderBy: $orderBy
      userFilter: $userFilter
    ) {
      limit
      page
      totalPages
      totalDocs
      docs {
        user {
          id
          email
          fullName
          avatar
          role
        }
        reviewsAverage
        postCount
        viewCount
        employeeInfo {
          id
          showInFrontPage
          position
          priority
          userId
          user {
            id
            avatar
            role
            username
            fullName
          }
        }
      }
    }
  }
`;f`
  query commentsPagination(
    $type: CommentType!
    $refId: String!
    $parent: ObjectID
    $page: Int
  ) @api(name: "zim") {
    commentsPagination(
      type: $type
      refId: $refId
      parent: $parent
      page: $page
    ) {
      totalDocs
      totalPages
      limit
      page
      docs {
        ... on PostComment {
          id
          content
          numOfLikes
          createdAt
          numOfChildComments
          user {
            id
            email
            avatar
            fullName
          }
        }
      }
    }
  }
`;f`
  query comments(
    $first: Int
    $type: CommentType!
    $refId: String!
    $parent: ObjectID
    $after: String
  ) @api(name: "zim") {
    comments(
      type: $type
      refId: $refId
      parent: $parent
      first: $first
      after: $after
    ) {
      pageInfo {
        hasNextPage
        hasPreviousPage
        endCursor
        startCursor
      }
      totalCount
      edges {
        node {
          ... on PostComment {
            id
            content
            numOfLikes
            createdAt
            numOfChildComments
            user {
              id
              email
              avatar
              fullName
            }
          }
        }
        cursor
      }
    }
  }
`;f`
  query getUserList($roleIds: [Int], $search: String) @api(name: "zim") {
    users(role_ids: $roleIds, search: $search) {
      totalDocs
      docs {
        id
        fullName
        role
      }
    }
  }
`;f`
  query user($userId: Int!) @api(name: "zim") {
    user(id: $userId) {
      id
      fullName
      avatar
      role
      roleId
      username
      birthday
      gender
    }
  }
`;f`
  query user($userId: Int!) @api(name: "zim") {
    user(id: $userId) {
      id
      fullName
      avatar
      role
      roleId
      username
      birthday
      gender
      email
      phone
      moreInfo
    }
  }
`;const tl=f`
  query getUserById($userId: Int!) @api(name: "appZim") {
    getUserById(id: $userId) {
      id
      full_name
      avatar
      role_name
      address
      email
      phone
      status_online
      supporter {
        id
        full_name
        avatar
        role_name
      }
      more_info {
        facebook
      }
      courseStudents {
        id
        course {
          status
          id
          slug
          name
        }
      }
    }
  }
`,Sn=f`
  query getUserById($userId: Int!) @api(name: "appZim") {
    getUserById(id: $userId) {
      id
      supporter {
        id
      }
    }
  }
`,sl=f`
  query getAccountChatPagination(
    $searchString: String
    $roles: [EnumRoleType]
    $page: Int
    $limit: Int
  ) @api(name: "appZim") {
    getAccountChatPagination(
      input: { q: $searchString, roles: $roles, page: $page, limit: $limit }
    ) {
      hasNextPage
      hasPrevPage
      limit
      page
      pageNext
      pagePrev
      pagingCounter
      totalPages
      totalDocs
      docs {
        user {
          id
          full_name
          role_id
          role_name
          status
          avatar
          status_online
        }
      }
    }
  }
`;f`
  query getListStudentsPagination($input: TicketGetListStudentsInputType)
  @api(name: "appZim") {
    getListStudentsPagination(input: $input) {
      hasNextPage
      hasPrevPage
      limit
      page
      pageNext
      pagePrev
      pagingCounter
      totalPages
      totalDocs
      docs {
        supportUId
        userId
        schoolAddress
        course {
          name
        }
        status
        user {
          id
          user_name
          full_name
          phone
          address
          email
          role_id
          role_name
          status
          avatar
        }
      }
    }
  }
`;const Id=f`
  query getListStudentsPagination($input: TicketGetListStudentsInputType)
  @api(name: "appZim") {
    getListStudentsPagination(input: $input) {
      hasNextPage
      hasPrevPage
      limit
      page
      pageNext
      pagePrev
      pagingCounter
      totalPages
      totalDocs
      docs {
        supportUId
        userId
        schoolAddress
        course {
          name
          code
          #          day_of_weeks {
          #            id
          #            name
          #          }
          #          shifts {
          #            id
          #            start_time
          #            end_time
          #          }
          ec {
            id
            full_name
          }
        }
        status
        user {
          id
          user_name
          full_name
          phone
          address
          email
          role_id
          role_name
        }
      }
    }
  }
`;f`
  query getListStudentByEc($input: AccountByEcInputType) @api(name: "appZim") {
    getListStudentByEc(input: $input) {
      page
      hasNextPage
      totalPages
      totalDocs
      docs {
        id
        full_name
        avatar
        phone
        address
        email
        status
        supporter
      }
    }
  }
`;const Vn=(e=[])=>f`
  query getListStudentByEc($input: AccountByEcInputType)
  @api(name: "appZim") {
    getListStudentByEc(input: $input) {
      page
      hasNextPage
      docs {
        id
        ${e.join()}
      }
    }
  }
`;f`
  query getPersonalInfo($id: Int!) @api(name: "appZim") {
    getPersonalInfo(id: $id) {
      id
      userName
      sourceOfCustomer {
        id
        sourceOfCustomer
      }
      supporter {
        isRequestCancel
        id
        user_name
        full_name
        phone
        address
        email
      }
      city {
        id
        name
        status
      }
      district {
        id
        name
        status
      }
      ward {
        id
        wardName
      }
      street {
        id
        wardName
      }
      homeNumber
      fullName
      phone
      email
      birthday
      address
      identityCard
      placeOfIssue {
        id
        name
        status
      }
      identityCardDate
      job {
        id
        jobName
      }
      workPlace
      academicPurposes {
        id
        academicPurposesName
      }
      noteHome
      typeOfEducation
      scoreIn
      scoreOut
      status
      statusId
    }
  }
`;const Pd=f`
  query searchPhone($phone: String!) @api(name: "appZim") {
    searchPhone(phone: $phone) {
      id
      user_name
      full_name
      phone
      address
      email
      role_id
      role_name
      status
      status_online
      avatar
      supporter
      student_more_info {
        identity_card_city_id
        note_home
        birthday
        city_id
        city_name
        district_id
        district_name
        ward_id
        ward_name
        street_id
        street_name
        home_number
        source_id
        source_name
        job_id
        job_name
        identity_card
        identity_card_city_name
        identity_card_date
        learning_status_id
        work_place
        learning_status_name
      }
    }
  }
`;f`
  query isUserExist($field: ExistFieldCheck!, $value: String!)
  @api(name: "zim") {
    isUserExist(field: $field, value: $value)
  }
`;const ol=f`
  fragment AdvertisementBaseFields on Advertisement {
    id
    type
    status
    title
    description
    image {
      id
      filename
      path
      width
      height
    }
    url
    clicks
    showIn
    startDate
    endDate
    createdAt
    updatedAt
  }
`,kn=f`
  fragment AdvertisementDetailFields on Advertisement {
    ...AdvertisementBaseFields
    postCategories {
      id
      title
    }
    productCategories {
      id
      title
    }
    posts {
      id
      title
    }
    tags {
      id
      name
    }
    products {
      ... on SimpleProduct {
        id
        name
      }
      ... on VariableProduct {
        id
        name
      }
    }
    excludeUrls
    position
    tagHtml
    tagPosition
  }
  ${ol}
`;f`
  query advertisementPagination(
    $page: Int
    $limit: Int
    $order: OrderDirection
    $orderBy: AdvertisementSortBy
    $statuses: [AdvertisementStatus]
  ) @api(name: "zim") {
    advertisementPagination(
      page: $page
      limit: $limit
      order: $order
      orderBy: $orderBy
      statuses: $statuses
    ) {
      totalDocs
      totalPages
      docs {
        ...AdvertisementBaseFields
      }
    }
  }
  ${ol}
`;f`
  query advertisement($id: ObjectID!) @api(name: "zim") {
    advertisement(id: $id) {
      ...AdvertisementDetailFields
    }
  }
  ${kn}
`;const Be=f`
  query supportConversations(
    $status: ConversationStatus
    $search: String
    $offset: Int
    $limit: Int
  ) @api(name: "chat") {
    supportConversations(
      status: $status
      search: $search
      offset: $offset
      limit: $limit
    ) {
      hasNextPage
      hasPrevPage
      limit
      nextPage
      page
      prevPage
      docs {
        id
        name
        group
        participants {
          userId
          fullName
          nickname
          avatar
          isGuest
        }
        createdAt
        updatedAt
        typing
        usersTyping
        pinnedAt
        blockedBy
        isUnread
        lastMessage {
          id
          text
          from
          to
          type
          attachments {
            type
            attachmentId
            attachment {
              id
              path
              fullUrl
              type
            }
          }
          loading @client
          error @client
          seenBy
          createdAt
          updatedAt
          deletedAt
          callPayload
        }
      }
    }
  }
`,Ae=f`
  query conversations(
    $status: ConversationStatus
    $search: String
    $offset: Int
    $limit: Int
    $group: String
  ) @api(name: "chat") {
    conversations(
      status: $status
      search: $search
      offset: $offset
      limit: $limit
      group: $group
    ) {
      hasNextPage
      hasPrevPage
      limit
      nextPage
      page
      prevPage
      docs {
        id
        name
        group
        participants {
          userId
          fullName
          nickname
          avatar
          isGuest
          online
        }
        createdAt
        updatedAt
        typing
        usersTyping
        pinnedAt
        blockedBy
        isUnread
        lastMessage {
          id
          text
          from
          to
          type
          attachments {
            type
            attachmentId
            attachment {
              id
              path
              fullUrl
              type
            }
          }
          loading @client
          error @client
          seenBy
          createdAt
          updatedAt
          deletedAt
          callPayload
        }
      }
    }
  }
`,wn=f`
  query conversationDetail($id: ObjectID, $userId: String) @api(name: "chat") {
    conversationDetail(id: $id, userId: $userId) {
      id
      name
      group
      typing
      usersTyping
      isUnread
      participants {
        userId
        fullName
        nickname
        avatar
        isGuest
        online
        roleId
      }
      owner {
        userId
        fullName
      }
      tags {
        id
        name
        color
      }
    }
  }
`,Ze=f`
  query messages(
    $conversationId: ObjectID
    $before: ObjectID
    $after: ObjectID
    $userId: String
    $offset: Int
    $limit: Int
  ) @api(name: "chat") {
    messages(
      conversationId: $conversationId
      before: $before
      after: $after
      userId: $userId
      offset: $offset
      limit: $limit
    ) {
      hasNextPage
      hasPrevPage
      limit
      nextPage
      page
      prevPage
      totalDocs
      docs {
        id
        text
        from
        to
        type
        attachments {
          type
          attachmentId
          attachment {
            id
            path
            fullUrl
            type
          }
        }
        conversation {
          id
          participants {
            userId
            avatar
            fullName
          }
        }
        loading @client
        error @client
        seenBy
        createdAt
        updatedAt
        deletedAt
        callPayload
        emojis {
          userId
          emoji
        }
      }
    }
  }
`,In=f`
  query notes(
    $conversationId: ObjectID
    $userId: String
    $page: Int
    $limit: Int
  ) @api(name: "chat") {
    notes(
      conversationId: $conversationId
      userId: $userId
      page: $page
      limit: $limit
    ) {
      hasNextPage
      hasPrevPage
      limit
      nextPage
      page
      pagingCounter
      prevPage
      totalDocs
      totalPages
      docs {
        id
        content
        createdAt
        createdBy
        owner {
          userId
          fullName
          avatar
        }
      }
    }
  }
`,Pn=f`
  query getAttachmentsInConversation(
    $conversationId: ObjectID
    $userId: String
    $offset: Int
    $limit: Int
  ) @api(name: "chat") {
    getAttachmentsInConversation(
      conversationId: $conversationId
      userId: $userId
      offset: $offset
      limit: $limit
    ) {
      hasNextPage
      hasPrevPage
      limit
      nextPage
      page
      pagingCounter
      prevPage
      totalDocs
      totalPages
      docs {
        id
        attachments {
          type
          attachmentId
          attachment {
            id
            path
            fullUrl
            type
          }
        }
      }
    }
  }
`,xs=f`
  query @api(name: "chat") {
    statisticMessageUnread {
      zimians
      student
      customer
      open
      following
      another
      isMarkSeen
    }
  }
`,_n=f`
  query getListTag($page: Int, $limit: Int) @api(name: "chat") {
    getListTag(page: $page, limit: $limit) {
      docs {
        id
        name
        color
      }
    }
  }
`,Fn=f`
  query searchMessage($conversationId: ObjectID!, $search: String!)
  @api(name: "chat") {
    searchMessage(conversationId: $conversationId, search: $search) {
      hasNextPage
      hasPrevPage
      limit
      nextPage
      page
      pagingCounter
      prevPage
      totalDocs
      totalPages
      docs {
        id
        text
        from
        to
        type
        conversation {
          id
          participants {
            userId
            fullName
            avatar
          }
        }
        attachments {
          type
          attachmentId
          attachment {
            id
            path
            fullUrl
            type
          }
        }
        loading @client
        error @client
        seenBy
        createdAt
        updatedAt
        deletedAt
        callPayload
      }
    }
  }
`,Ns=f`
  query nearbyMessages($messageId: ObjectID!, $limit: Int) @api(name: "chat") {
    nearbyMessages(messageId: $messageId, limit: $limit) {
      hasNextPage
      hasPrevPage
      limit
      nextPage
      page
      prevPage
      docs {
        id
        text
        from
        to
        type
        attachments {
          type
          attachmentId
          attachment {
            id
            path
            fullUrl
            type
          }
        }
        conversation {
          id
          participants {
            userId
            avatar
            fullName
          }
        }
        loading @client
        error @client
        seenBy
        createdAt
        updatedAt
        deletedAt
        callPayload
      }
    }
  }
`,Mn=f`
  query QAPostCategoriesTree @api(name: "zim") {
    QAPostCategoriesTree {
      id
      title
    }
  }
`,_d=f`
  query (
    $school_id: Int
    $q: String
    $name: String
    $description: String
    $status: String
  ) @api(name: "appZim") {
    rooms(
      school_id: $school_id
      q: $q
      name: $name
      description: $description
      status: $status
    ) {
      id
      name
    }
  }
`,Fd=f`
  query schools($city_id: Int, $status: String) @api(name: "appZim") {
    schools(city_id: $city_id, status: $status) {
      id
      city {
        id
        name
      }
      district {
        id
        name
      }
      name
      address
      phone
      status
    }
  }
`,Md=f`
  query @api(name: "appZim") {
    cities {
      id
      name
      status
    }
  }
`;f`
  query @api(name: "appZim") {
    cats {
      id
      name
    }
  }
`;const qd=f`
  query subject($name: String, $status: StatusEnum) @api(name: "appZim") {
    subjects(name: $name, status: $status) {
      id
      name
      status
      cats {
        id
        name
        status
        subject {
          id
          name
          status
        }
      }
    }
  }
`,zd=f`
  query levels($subject_id: Int!) @api(name: "appZim") {
    levels(subject_id: $subject_id, status: active) {
      id
      name
      status
      graduation
      subject_id
    }
  }
`;f`
  query shifts($q: String) @api(name: "appZim") {
    shifts(q: $q, status: "active", shift_minute: 0) {
      id
      start_time
      end_time
      status
      shift_minute
    }
  }
`;f`
  query getStatusAccount @api(name: "appZim") {
    getStatusAccount {
      key
      value
    }
  }
`;f`
  query getTypeOfEducation @api(name: "appZim") {
    getTypeOfEducation {
      key
      value
    }
  }
`;f`
  query getAcademicPurposesAll @api(name: "appZim") {
    getAcademicPurposesAll {
      id
      academicPurposesName
    }
  }
`;const Od=f`
  query getEcs @api(name: "appZim") {
    getEcs {
      id
      user_name
      full_name
      phone
      address
      email
      role_id
      role_name
      status
      status_online
      avatar
      supporter
    }
  }
`,Ld=f`
  query getJobs @api(name: "appZim") {
    getJobs {
      id
      jobName
    }
  }
`,qn=f`
  query getSourceOfCustomer @api(name: "appZim") {
    getSourceOfCustomer {
      id
      sourceOfCustomer
    }
  }
`,Bd=f`
  query districts($city_id: Int!, $streetName: String, $status: StatusEnum)
  @api(name: "appZim") {
    districts(city_id: $city_id, name: $streetName, status: $status) {
      id
      name
      status
    }
  }
`,Rd=f`
  query getStreets($districtid: Int!, $streetName: String, $isHide: Boolean)
  @api(name: "appZim") {
    getStreets(
      districtid: $districtid
      streetName: $streetName
      isHide: $isHide
    ) {
      id
      streetName
    }
  }
`,Ud=f`
  query getWards($districtid: Int!, $wardName: String, $isHide: Boolean)
  @api(name: "appZim") {
    getWards(districtid: $districtid, wardName: $wardName, isHide: $isHide) {
      id
      wardName
    }
  }
`;f`
  mutation createProductCategory($input: ProductCategoryInput!)
  @api(name: "zim") {
    createProductCategory(input: $input) {
      id
      title
      slug
    }
  }
`;f`
  mutation updateProductCategory(
    $id: ObjectID!
    $input: UpdateProductCategoryInput!
  ) @api(name: "zim") {
    updateProductCategory(id: $id, input: $input) {
      id
      title
    }
  }
`;f`
  mutation deleteProductCategory($id: ObjectID!) @api(name: "zim") {
    deleteProductCategory(id: $id)
  }
`;f`
  mutation ($name: String!, $type: ProductAttributeType!, $options: [String])
  @api(name: "zim") {
    createProductAttribute(name: $name, type: $type, options: $options) {
      id
      type
      name
      options
    }
  }
`;f`
  mutation (
    $id: ObjectID!
    $name: String
    $type: ProductAttributeType
    $options: [String]
  ) @api(name: "zim") {
    updateProductAttribute(
      id: $id
      name: $name
      type: $type
      options: $options
    ) {
      id
      type
      name
      options
    }
  }
`;f`
  mutation (
    $type: ProductType!
    $sku: String!
    $description: String!
    $shortDescription: String!
    $name: String!
    $featured: Boolean
    $categories: [ObjectID]
    $images: [ObjectID]
    $attributes: [ProductAttributeInput]
    $tags: [ObjectID]
    $variationAttributes: [ObjectID]
    $regularPrice: Int
    $salePrice: Int
    $salePriceFrom: DateTime
    $salePriceTo: DateTime
    $downloads: [ObjectID]
    $downloadable: Boolean
  ) @api(name: "zim") {
    createProduct(
      input: {
        type: $type
        sku: $sku
        description: $description
        shortDescription: $shortDescription
        name: $name
        featured: $featured
        categories: $categories
        images: $images
        attributes: $attributes
        tags: $tags
        variationAttributes: $variationAttributes
        regularPrice: $regularPrice
        salePrice: $salePrice
        salePriceFrom: $salePriceFrom
        salePriceTo: $salePriceTo
        downloads: $downloads
        downloadable: $downloadable
      }
    ) {
      ...ProductBaseField
      ...ProductDetailFields
    }
  }
  ${es}
`;f`
  mutation (
    $id: ObjectID!
    $description: String!
    $shortDescription: String!
    $name: String!
    $featured: Boolean
    $categories: [ObjectID]
    $images: [ObjectID]
    $attributes: [ProductAttributeInput]
    $tags: [ObjectID]
    $regularPrice: Int
    $salePrice: Int
    $salePriceFrom: DateTime
    $salePriceTo: DateTime
    $downloads: [ObjectID]
    $downloadable: Boolean
    $variationAttributes: [ObjectID]
    $slug: String
    $sku: String
  ) @api(name: "zim") {
    updateProduct(
      id: $id
      input: {
        description: $description
        shortDescription: $shortDescription
        name: $name
        featured: $featured
        categories: $categories
        images: $images
        attributes: $attributes
        tags: $tags
        regularPrice: $regularPrice
        salePrice: $salePrice
        salePriceFrom: $salePriceFrom
        salePriceTo: $salePriceTo
        downloads: $downloads
        downloadable: $downloadable
        variationAttributes: $variationAttributes
        slug: $slug
        sku: $sku
      }
    ) {
      ...ProductBaseField
      ...ProductDetailFields
    }
  }
  ${es}
`;f`
  mutation (
    $id: ObjectID!
    $description: String
    $shortDescription: String
    $name: String
    $categories: [ObjectID]
    $images: [ObjectID]
    $attributes: [ProductAttributeInput]
    $tags: [ObjectID]
    $regularPrice: Int
    $salePrice: Int
    $salePriceFrom: DateTime
    $salePriceTo: DateTime
    $downloads: [ObjectID]
    $downloadable: Boolean
    $slug: String
    $sku: String
  ) @api(name: "zim") {
    updateProductVariants(
      id: $id
      input: {
        description: $description
        shortDescription: $shortDescription
        name: $name
        categories: $categories
        images: $images
        attributes: $attributes
        tags: $tags
        regularPrice: $regularPrice
        salePrice: $salePrice
        salePriceFrom: $salePriceFrom
        salePriceTo: $salePriceTo
        downloads: $downloads
        downloadable: $downloadable
        slug: $slug
        sku: $sku
      }
    ) {
      ...ProductBaseField
      ...ProductDetailFields
    }
  }
  ${es}
`;f`
  mutation ($parentProduct: ObjectID!, $input: AddVariantProductInput!)
  @api(name: "zim") {
    createProductVariants(parentProduct: $parentProduct, input: $input) {
      ...ProductBaseField
      ...ProductDetailFields
    }
  }
  ${es}
`;f`
  mutation ($id: ObjectID!) @api(name: "zim") {
    publishProduct(id: $id) {
      ...ProductBaseField
    }
  }
  ${Tt}
`;f`
  mutation ($id: ObjectID!) @api(name: "zim") {
    unPublishProduct(id: $id) {
      ...ProductBaseField
    }
  }
  ${Tt}
`;f`
  mutation ($productId: ObjectID!, $inventories: [InventoryInput]!)
  @api(name: "zim") {
    bulkUpdateProductInventory(
      productId: $productId
      inventories: $inventories
    ) {
      id
      inStock
      inventories {
        id
        inStock
        inventory
        reserved
        manageStock
        store {
          id
          name
        }
      }
    }
  }
`;f`
  mutation ($id: ObjectID!) @api(name: "zim") {
    removeProductVariants(id: $id) {
      ...ProductBaseField
    }
  }
  ${Tt}
`;f`
  fragment RecruitmentPostBaseField on RecruitmentPost {
    id
    title
    area
    status
    position
    currentNumOfReplies
    maxCandidates
    startDate
    endDate
    createdAt
    updatedAt
    slug
  }
`;const zn=f`
  fragment RecruitmentPostDetailsField on RecruitmentPost {
    id
    title
    area
    position
    numOfCandidates
    maxCandidates
    jobDescription
    currentNumOfReplies
    salaryFrom
    slug
    salaryType
    salaryTo
    status
    endDate
    createdAt
    updatedAt
    startDate
    seo {
      title
      description
      ogDescription
      ogImage {
        id
        path
      }
      ogTitle
      publisher
      noIndex
      noFollow
      canonicalUrl
      customHeadHtml
    }
  }
`;f`
  mutation ($postId: ObjectID!, $seo: PostSeoInput!) @api(name: "zim") {
    updatePostSeoConfig(postId: $postId, seo: $seo) {
      id
      title
      slug
      seo {
        title
        description
        ogDescription
        ogImage {
          id
          path
        }
        ogTitle
        publisher
        noIndex
        noFollow
        canonicalUrl
        customHeadHtml
      }
      createdAt
      updatedAt
    }
  }
`;f`
  mutation ($productId: ObjectID!, $seo: PostSeoInput!) @api(name: "zim") {
    updateProductSeoConfig(productId: $productId, seo: $seo) {
      ... on SimpleProduct {
        id
        createdAt
        updatedAt
        seo {
          title
          description
          ogDescription
          ogImage {
            id
            path
          }
          ogTitle
          publisher
          noIndex
          noFollow
          canonicalUrl
          customHeadHtml
        }
      }
      ... on VariableProduct {
        id
        createdAt
        updatedAt
        seo {
          title
          description
          ogDescription
          ogImage {
            id
            path
          }
          ogTitle
          publisher
          noIndex
          noFollow
          canonicalUrl
          customHeadHtml
        }
      }
    }
  }
`;f`
  mutation ($recruitmentPostId: ObjectID!, $seo: CommonSeoInput!)
  @api(name: "zim") {
    updateRecruitmentPostSeoConfig(
      recruitmentPostId: $recruitmentPostId
      seo: $seo
    ) {
      ...RecruitmentPostDetailsField
    }
  }
  ${zn}
`;f`
  query redirectUrlPagination($page: Int, $limit: Int, $search: String)
  @api(name: "zim") {
    redirectUrlPagination(page: $page, limit: $limit, search: $search) {
      docs {
        id
        prevUrl
        newUrl
        redirectCode
        createdAt
        updatedAt
      }
      totalPages
      totalDocs
    }
  }
`;f`
  mutation updateRedirectUrl($id: ObjectID!, $input: UpdateRedirectUrlInput!)
  @api(name: "zim") {
    updateRedirectUrl(id: $id, input: $input) {
      id
      prevUrl
      newUrl
      redirectCode
      createdAt
      updatedAt
    }
  }
`;f`
  mutation deleteRedirectUrl($id: ObjectID!) @api(name: "zim") {
    deleteRedirectUrl(id: $id)
  }
`;f`
  mutation createRedirectUrl($input: CreateRedirectUrlInput!)
  @api(name: "zim") {
    createRedirectUrl(input: $input) {
      id
      prevUrl
      newUrl
      redirectCode
      createdAt
      updatedAt
    }
  }
`;function il(e){const s=e+"=",l=decodeURIComponent(document.cookie).split(";");for(let a=0;a<l.length;a++){let i=l[a];for(;i.charAt(0)===" ";)i=i.substring(1);if(i.indexOf(s)===0)return i.substring(s.length,i.length)}return""}const On=`
    mutation {
        refreshToken {
            token
            refreshToken
        }
    }
`,Ln=e=>{},Bn=()=>{},yt=()=>il("token"),Rn=la(({graphQLErrors:e,networkError:s})=>{e&&e.map(({message:o,locations:l,path:a})=>console.log(`[GraphQL error]: Message: ${o}, Location: ${l}, Path: ${a}`)),s&&console.log(`[Network error]: ${s}`)}),Un=new Os((e,s)=>new aa(o=>{let l;return Promise.resolve(e).then(a=>{a.setContext({headers:{authorization:`Bearer ${yt()}`,"Access-Control-Allow-Credentials":!0,"x-app-token":"0aAUfvzhIZVSDIF0yBk53U8Vokij6xv7"}})}).then(()=>{l=s(e).subscribe({next:o.next.bind(o),error:o.error.bind(o),complete:o.complete.bind(o)})}).catch(o.error.bind(o)),()=>{l&&l.unsubscribe()}})),Gn=new ra({possibleTypes:{BaseProduct:["VariableProduct","SimpleProduct"]},typePolicies:{Participant:{keyFields:["userId"]},AccountStudentInfoType:{keyFields:(e,s)=>`${e.source_name}-${e.district_name}-${e.job_name}-${e.ward_name}-${e.city_name}-${e.street_name}-${e.birthday}`},AccountType:{keyFields:(e,s)=>`${e.id}-${e.full_name}-${e.phone}`},Message:{fields:{loading:{read:()=>!1},error:{read:()=>!1}}},Query:{fields:{getAccountChatPagination:{keyArgs:["input",["q","limit","roles"]]},messages:{keyArgs:["conversationId","userId","offset","before","after"]},supportConversations:{keyArgs:["status"]},conversations:{keyArgs:["group"]},medias:Ls(["type","width","height","search"]),tags:Ls(["search"]),comments:Ls(["type","refId","parent"])}}}});global.wsClients={};const Hn=e=>{const s=new ha(e,{reconnect:!0,connectionParams:()=>({authToken:yt()})});return global.wsClients[e]=s,s},Wn=e=>new ma(Hn(e)),ll=new na({endpoints:{zim:"https://stg-graph-api.zim.vn",appZim:"https://stg-graphnet-api.zim.vn",analytics:{}.REACT_APP_ANALYTICS_API_ENDPOINT,chat:"https://stg-chat.zim.vn",crawler:{}.ZIM_CRAWLER_ENDPOINT},createHttpLink:()=>ua({credentials:"include",fetch:ca(pi,(e,s,o)=>y(x({},e),{withCredentials:!0,onUploadProgress:o.onUploadProgress}))}),wsSuffix:"/graphql",createWsLink:e=>Wn(e)}),al=async()=>{const e={operationName:null,variables:{},query:On};return fetch("https://stg-graph-api.zim.vn/graphql",{method:"POST",credentials:"include",body:JSON.stringify(e),headers:{"Access-Control-Request-Headers":"content-type","Content-Type":"application/json; charset=utf-8"}}).then(async s=>{const o=await s.json();return o==null?void 0:o.data.refreshToken})},rl=new da({accessTokenField:"token",isTokenValidOrUndefined:()=>{const e=yt();if(e.length===0)return!0;try{const{exp:s}=bi(e);return Date.now()<s*1e3}catch{return!1}},fetchAccessToken:al,handleFetch:e=>{e?localStorage.setItem("isAuthenticated","true"):localStorage.removeItem("isAuthenticated")},handleResponse:()=>{},handleError:e=>{console.log(`handleError: ${e}`)}}),Qn=Os.from([rl,Un,Rn,ll]),Zn=pa(({query:e})=>{const s=ba(e);return s.kind==="OperationDefinition"&&s.operation==="subscription"},Os.from([rl,ll]),Qn),ts=new ga({ssrMode:typeof window=="undefined",link:Zn,credentials:"include",cache:Gn,connectToDevTools:!0});var nl="/Users/zimdev/work/zim-app/src/components/ProviderAuth/index.tsx";const lo=c.exports.createContext({appState:{isAuthenticated:!1,user:null,loaded:!1}}),Yn=({children:e})=>{const[s,o]=c.exports.useState({isAuthenticated:!1,user:null,loaded:!1,buildMode:"module"}),l=async()=>{var n,r;try{const u=await ts.query({query:jn,fetchPolicy:"network-only"});((n=u==null?void 0:u.data)==null?void 0:n.me)?(o(y(x({},s),{user:(r=u==null?void 0:u.data)==null?void 0:r.me,isAuthenticated:!0,loaded:!0})),localStorage.setItem("isAuthenticated","true")):(localStorage.removeItem("isAuthenticated"),o(y(x({},s),{user:null,isAuthenticated:!1,loaded:!0})))}catch{o(y(x({},s),{user:null,isAuthenticated:!1,loaded:!0})),localStorage.removeItem("isAuthenticated")}},a=async n=>{await l()},i=async()=>{await ts.mutate({mutation:Cn}),o(y(x({},s),{user:null,isAuthenticated:!1,loaded:!0})),localStorage.removeItem("isAuthenticated")};return t.exports.jsxDEV(lo.Provider,{value:{getUser:l,appState:s,setAppState:o,appSetLogin:a,appSetLogout:i,appSetAuthToken:Ln,appClearAuthToken:Bn},children:t.exports.jsxDEV(fa,{client:ts,children:e},void 0,!1,{fileName:nl,lineNumber:114,columnNumber:7},globalThis)},void 0,!1,{fileName:nl,lineNumber:103,columnNumber:5},globalThis)};function Kn(){return c.exports.useContext(lo)}const Jn=()=>{const e=c.exports.useRef(!1);return c.exports.useEffect(()=>(e.current=!0,()=>{e.current=!1}),[]),c.exports.useCallback(()=>e.current,[])},Se={home:"/",listCourses:"/courses",courseDetail:"/courses/:id",forbiddenError:"/error/403",ticket:"/ticket",chat:"/chat",report:"/report",leadReport:"/report/lead",studentSupportByEc:"/user/student-support-by-ec",scheduleRegistration:"/schedule/schedule-registration",listSchedules:"/schedule/list-schedules",registerTestSchedule:"/schedule/test/reg",lead:"/web-data/lead",paperBasedTestBank:"/paper-based-test-bank",mockTestOnline:"/mock-test/mock-test-online",mockTestOffline:"/mock-test/mock-test-offline",mockTestDetail:"/mock-test/mock-test-detail"},Gd={lead:"/Admin/ContactCustomer/ContactList",customerInfo:"/Admin/Customer/CustomerDetai",staffInfo:"/Admin/Staff/StaffDetail",courseDetail:"/Admin/CourseManage/CourseDetail",ticketList:"/Admin/Ticket",customerList:"/Admin/Customer/CustomersList",listStudentsMockTest:"/Admin/MockTest/ListStudent",mockTestDetail:"/Admin/MockTest/MockTest",ieltsCorrecting:"/Admin/Ieltscorrect/Correcting",newsFeed:"/news-feed"},Hd={testResult:"/test/result"},ul=(e,s,o=window)=>{const l=c.exports.useRef();c.exports.useEffect(()=>{l.current=s},[s]),c.exports.useEffect(()=>{if(!(o&&o.addEventListener))return;const i=n=>l&&typeof l.current=="function"?l.current(n):null;return typeof i=="function"&&o.addEventListener(e,i),()=>{typeof i=="function"&&o.removeEventListener(e,i)}},[e,o])},Xn=(...e)=>{const s=c.exports.useRef();return c.exports.useEffect(()=>{e.forEach(o=>{!o||(typeof o=="function"?o(s.current):o.current=s.current)})},[e]),s};var Es="/Users/zimdev/work/zim-app/src/components/TextInput/index.tsx";const Ge=c.exports.forwardRef((l,o)=>{var a=l,{error:e}=a,s=Ue(a,["error"]);return t.exports.jsxDEV(cs,{isInvalid:!!e,children:[(s==null?void 0:s.label)&&t.exports.jsxDEV(Bs,{htmlFor:s.id,children:s.label},void 0,!1,{fileName:Es,lineNumber:22,columnNumber:24},globalThis),t.exports.jsxDEV(ms,y(x({bg:j("white","slate.700"),borderColor:j("gray.300","slate.600"),_hover:{borderColor:j("gray.400","slate.700")},color:j("gray.900","slate.300"),_placeholder:{color:j("gray.400","slate.500")}},s),{ref:o}),void 0,!1,{fileName:Es,lineNumber:23,columnNumber:7},globalThis),e&&t.exports.jsxDEV(ds,{children:e&&(e==null?void 0:e.message)},void 0,!1,{fileName:Es,lineNumber:34,columnNumber:17},globalThis)]},void 0,!0,{fileName:Es,lineNumber:21,columnNumber:5},globalThis)});var Ft="/Users/zimdev/work/zim-app/src/components/SearchInput/index.tsx";const Wd=c.exports.forwardRef((a,l)=>{var i=a,{placeholder:e,onSubmitSearch:s}=i,o=Ue(i,["placeholder","onSubmitSearch"]);const n=c.exports.useRef(null),r=Xn(l,n),u=g=>{g.preventDefault();const{value:b}=r.current;typeof s=="function"&&s(b)};return t.exports.jsxDEV(S,{as:"form",onSubmit:u,id:"form-header-search",w:"full",children:t.exports.jsxDEV(gi,{flexGrow:1,w:"100%",children:[t.exports.jsxDEV(hi,{children:t.exports.jsxDEV(Y,{p:0,variant:"ghost",_hover:{backgroundColor:"transparent"},type:"submit",children:t.exports.jsxDEV(xa,{fontSize:18},void 0,!1,{fileName:Ft,lineNumber:58,columnNumber:15},globalThis)},void 0,!1,{fileName:Ft,lineNumber:50,columnNumber:13},globalThis)},void 0,!1,{fileName:Ft,lineNumber:48,columnNumber:9},globalThis),t.exports.jsxDEV(Ge,y(x({placeholder:"T\xECm ki\u1EBFm...",variant:"filled"},o),{ref:r,pr:10}),void 0,!1,{fileName:Ft,lineNumber:62,columnNumber:9},globalThis)]},void 0,!0,{fileName:Ft,lineNumber:47,columnNumber:7},globalThis)},void 0,!1,{fileName:Ft,lineNumber:41,columnNumber:5},globalThis)}),cl=f`
  subscription ($conversationId: ObjectID, $status: String) @api(name: "chat") {
    conversationUpdated(conversationId: $conversationId, status: $status) {
      id
      name
      isDeleteMessage
      group
      status
      createdAt
      updatedAt
      pinnedAt
      blockedBy
      isUnread
      typing
      usersTyping
      participants {
        userId
        fullName
        nickname
        avatar
        isGuest
        online
      }
      lastMessage {
        id
        text
        from
        to
        type
        attachments {
          type
          attachmentId
          attachment {
            id
            path
            fullUrl
            type
          }
        }
        conversation {
          id
          participants {
            userId
            avatar
            fullName
          }
        }
        loading @client
        error @client
        seenBy
        createdAt
        updatedAt
        deletedAt
        callPayload
        emojis {
          userId
          emoji
        }
      }
    }
  }
`,eu=f`
  subscription @api(name: "chat") {
    messageAdded {
      zimians
      student
      customer
      open
      following
      another
      isMarkSeen
    }
  }
`;function ne(){const{appState:{user:e},setUser:s}=c.exports.useContext(lo);return[e,s]}var tu="/assets/notificationSound.mp3";const su=()=>{const[e]=ne(),s=new Audio(tu);s.volume=1;const{data:o,loading:l}=De(xs),a=o==null?void 0:o.statisticMessageUnread;return Rs(eu,{onSubscriptionData:({client:i,subscriptionData:n})=>{var u,g;const r=i.cache.readQuery({query:xs});if(n.data){const{messageAdded:b}=n.data;b?(i.writeQuery({query:xs,data:y(x({},r),{statisticMessageUnread:x(x({},r.statisticMessageUnread),b)})}),((g=(u=n==null?void 0:n.data)==null?void 0:u.messageAdded)==null?void 0:g.isMarkSeen)||s.play()):i.writeQuery({query:xs,data:x({},r)})}},shouldResubscribe:!!e}),{listMessageUnreadOverall:a,loading:l}},Qd={editor:[1,2,11],approve:[1,8],author:[1,8,11,4],manager:[1,2,12,8,11],productManager:[1,2],censor:[1,2,8],canEdit:[1,2,4,8,11],canConfigSeo:[1,2,11],admin:[1],adminAndCC:[1,2],canCreateTicket:[5,6,12,1,2,11,12],canCancelSchedule:[2],canEnterMockTestScores:[4],canUploadMockTestTask:[6],canCreateOrEditMockTest:[1,2],assignTeacherAndEC:[1,2],viewTableMockTestDetail:[1,2,6],canBookSchedule:[6]},ao=[{id:1,name:"Super Admin"},{id:2,name:"CC"},{id:4,name:"Gi\xE1o vi\xEAn"},{id:5,name:"Student"},{id:6,name:"EC"},{id:8,name:"AM"},{id:9,name:"K\u1EBF to\xE1n"},{id:11,name:"Marketer"},{id:12,name:"BM"},{id:13,name:"BP"},{id:14,name:"Guest"}],ou=[5],iu=[1,6],lu={apiUrl:"https://demo.zim.vn",apiVersion:"v1",access_token:"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJqc29uX3dlYl90b2tlbiIsImp0aSI6ImM5ZDFmNzA3LTk4ZmYtNGNmMC1iMGUyLTAwN2RhY2MzYTVhNSIsImlhdCI6IjEvNC8yMDIyIDM6MDE6MzIgUE0iLCJpZCI6IjM0NDg4IiwidXNlcl9uYW1lIjoiemltIiwiZnVsbF9uYW1lIjoiWklNIEFjYWRlbXkiLCJyb2xlX2lkIjoiMSIsInJvbGVfbmFtZSI6IlN1cGVyIEFkbWluIiwiZXhwIjoxNjY3NTc0MDkyLCJpc3MiOiJhcHAuemltLnZuIiwiYXVkIjoiYXBwLnppbS52biJ9.RPOVJ-aana5NoeyUEZ_WYI_i0f2gBqU0o1W68WO51Ro"},au=()=>{var g;const[e]=ne(),s=e.roleId,{listMessageUnreadOverall:o}=su(),l=(o==null?void 0:o.another)?Object==null?void 0:Object.values(JSON==null?void 0:JSON.parse(o==null?void 0:o.another)):[],a=[o==null?void 0:o.zimians,o==null?void 0:o.student,o==null?void 0:o.following,o==null?void 0:o.open,(g=l==null?void 0:l.map(b=>parseInt(b,10)))==null?void 0:g.reduce((b,m)=>b+m,0)],i=a[1]+a[4],n=a[0]+a[1]+a[4],r=a==null?void 0:a.reduce((b,m)=>b+m,0),u=ou.includes(s)?i:iu.includes(s)?r:n;return c.exports.useEffect(()=>{const b=document.getElementById("noti-chat");b&&(b.innerHTML=`${u}`)},[u]),{overall:a,overallMyChats:u}};var ro="/Users/zimdev/work/zim-app/src/components/SearchBox/index.tsx";const ru=({searchString:e,setSearchString:s})=>{const o=l=>{l.target&&s(l.target.value)};return t.exports.jsxDEV(S,{flex:1,maxH:"35px",borderWidth:1,borderColor:"#e4e4e4",rounded:"full",display:"flex",flexDirection:"row",alignItems:"center",px:2,overflow:"hidden",bg:"white",children:[t.exports.jsxDEV(ms,{variant:"unstyled",placeholder:"T\xECm ki\u1EBFm",p:2,value:e,onChange:o,color:"#373737"},void 0,!1,{fileName:ro,lineNumber:24,columnNumber:7},globalThis),t.exports.jsxDEV(Na,{className:"h-5 w-5 text-gray-400"},void 0,!1,{fileName:ro,lineNumber:32,columnNumber:7},globalThis)]},void 0,!0,{fileName:ro,lineNumber:11,columnNumber:5},globalThis)},Zd=e=>`${e} is required.`,nu="00000000-0000-0000-0000-000000000000",Yd={0:{short:"CN",long:"CN"},1:{short:"2",long:"T2"},2:{short:"3",long:"T3"},3:{short:"4",long:"T4"},4:{short:"5",long:"T5"},5:{short:"6",long:"T6"},6:{short:"7",long:"T7"}},Kd={LISTENING:{short:"L",long:"Listening"},READING:{short:"R",long:"Reading"},SPEAKING:{short:"S",long:"Speaking"},WRITING:{short:"W",long:"Writing"},"WRITING TASK 1":{short:"WT1",long:"WT1"},"WRITING TASK 2":{short:"WT2",long:"WT2"}},no={ALL:"all",INCOMING:"incoming",ONGOING:"ongoing",CLOSED:"closed"},Jd={ALL:{label:"All",colorScheme:"gray"},INCOMING:{label:"Incoming",colorScheme:"green"},ONGOING:{label:"Ongoing",colorScheme:"orange"},FINISHED:{label:"Finished",colorScheme:"red"}},Xd=[{label:"Incoming",value:no.INCOMING},{label:"Ongoing",value:no.ONGOING},{label:"Closed",value:no.CLOSED}],ep=[{id:1,label:"Th\u1EE9 Hai",value:1},{id:2,label:"Th\u1EE9 Ba",value:1},{id:3,label:"Th\u1EE9 T\u01B0",value:3},{id:4,label:"Th\u1EE9 N\u0103m",value:4},{id:5,label:"Th\u1EE9 S\xE1u",value:5},{id:6,label:"Th\u1EE9 B\u1EA3y",value:6},{id:0,label:"Ch\u1EE7 nh\u1EADt",value:7}];var Mt;(function(e){e.PROCESSING="Processing",e.CLOSED="Closed",e.WAITING="Waiting",e.DELETED="Deleted",e.LONGTIME="Longtime"})(Mt||(Mt={}));var vs;(function(e){e.send="send",e.received="received"})(vs||(vs={}));var As;(function(e){e.active="active",e.deactivated="deactivated"})(As||(As={}));const tp={All:{label:"All",value:"All",colorScheme:"gray"},Processing:{label:"Processing",value:"Processing",colorScheme:"green"},Waiting:{label:"Waiting",value:"Waiting",colorScheme:"orange"},Closed:{label:"Closed",value:"Closed",colorScheme:"red"},Deleted:{label:"Deleted",value:"Deleted",colorScheme:"black"}},sp={all:{label:"All",value:"All",colorScheme:"gray"},processing:{label:"Processing",value:"Processing",colorScheme:"green"},waiting:{label:"Waiting",value:"Waiting",colorScheme:"orange"},closed:{label:"Closed",value:"Closed",colorScheme:"red"},deleted:{label:"Deleted",value:"Deleted",colorScheme:"black"},longtime:{label:"Longtime",value:"Longtime",colorScheme:"gray"}},op={0:{label:"Ch\u01B0a x\u1EED l\xFD",value:0,colorScheme:"red",filterValue:"pending"},1:{label:"\u0110ang theo d\xF5i",value:1,colorScheme:"orange",filterValue:"following"},2:{label:"\u0110\xE3 h\u1EB9n test",value:2,colorScheme:"blue",filterValue:"appoiment"},4:{label:"\u0110\xE3 \u0111\u1EBFn test",value:4,colorScheme:"teal",filterValue:"called"},6:{label:"\u0110\xE3 \u0111\u0103ng k\xFD",value:6,colorScheme:"green",filterValue:"registered"},3:{label:"\u0110\xE3 h\u1EE7y",value:3,colorScheme:"gray",filterValue:"cancelled"},5:{label:"\u0110\xE3 \u0111\u1ED5i EC",value:5,colorScheme:"purple",filterValue:"changeEc"}},ip=[{label:"Processing",value:Mt.PROCESSING},{label:"Waiting",value:Mt.WAITING},{label:"Closed",value:Mt.CLOSED},{label:"Deleted",value:Mt.DELETED}],lp=[{label:"\u0110\xE3 g\u1EEDi",value:vs.send},{label:"\u0110\xE3 nh\u1EADn",value:vs.received}],ap=[{label:"Active",value:As.active},{label:"Deactivated",value:As.deactivated}];var ml;(function(e){e.Other="Other",e.TeacherOff="TeacherOff",e.MOCK_TEST_ONLINE="MOCK_TEST_ONLINE",e.MOCK_TEST_OFFLINE="MOCK_TEST_OFFLINE",e.OTHER="OTHER",e.TEACHER_OFF="TEACHER_OFF"})(ml||(ml={}));const rp=[{id:1,label:"3 bu\u1ED5i/tu\u1EA7n"},{id:2,label:"4-6 bu\u1ED5i/tu\u1EA7n"},{id:3,label:"Tr\xEAn 6 bu\u1ED5i/tu\u1EA7n"}],uu="https://app.zim.vn/app-assets/zimv2/images/default-logo1.png",np={inactive:{label:"inactive",value:"inactive",colorScheme:"gray"},active:{label:"active",value:"active",colorScheme:"green"},deactivated:{label:"deactivated",value:"deactivated",colorScheme:"red"}},up=[{label:"Active",value:"active"},{label:"Deactivated",value:"deactivated"}],cp={"Listening-Reading-Writing":{long:"Listening-Reading-Writing",short:"L-R-W"},Speaking:{long:"Speaking",short:"Speaking"}};var nt;(function(e){e.confirmed="confirmed",e.cancel="cancel",e.pending="pending"})(nt||(nt={}));nt.confirmed,nt.cancel,nt.pending;nt.confirmed,nt.cancel,nt.pending;const mp=[{label:"5 rows",value:5},{label:"10 rows",value:10},{label:"15 rows",value:15},{label:"20 rows",value:20},{label:"25 rows",value:25},{label:"30 rows",value:30}];var cu="/Users/zimdev/work/zim-app/src/pages/chat/components/myAvatar.tsx";const He=c.exports.memo(l=>{var a=l,{src:e="",name:s=""}=a,o=Ue(a,["src","name"]);return t.exports.jsxDEV(fi,x({src:e===uu?"":e,name:s,zIndex:10},o),void 0,!1,{fileName:cu,lineNumber:11,columnNumber:7},globalThis)});var ut="/Users/zimdev/work/zim-app/src/pages/chat/manageChannels/myInfoSection.tsx";const mu=({searchString:e,setSearchString:s})=>{const[o]=ne();return t.exports.jsxDEV(ie,{w:"100%",p:{base:3},position:"sticky",top:0,zIndex:50,bg:j("white","#10172a"),alignItems:"stretch",children:[t.exports.jsxDEV(S,{w:"100%",display:"flex",alignItems:"start",maxW:{base:"full"},className:" space-x-2.5",bg:j("white",""),p:{base:2,md:0},rounded:"lg",children:[t.exports.jsxDEV(He,{boxSize:"2em",src:o==null?void 0:o.avatar,name:o==null?void 0:o.fullName},void 0,!1,{fileName:ut,lineNumber:35,columnNumber:9},globalThis),t.exports.jsxDEV(S,{flex:1,children:[t.exports.jsxDEV(M,{noOfLines:1,fontSize:14,lineHeight:"18px",fontWeight:"medium",color:j("gray.800","slate.300"),children:o==null?void 0:o.fullName},void 0,!1,{fileName:ut,lineNumber:37,columnNumber:11},globalThis),t.exports.jsxDEV(M,{display:{base:"block"},noOfLines:1,fontSize:14,fontWeight:500,lineHeight:"24px",color:"gray.400",children:o==null?void 0:o.role},void 0,!1,{fileName:ut,lineNumber:46,columnNumber:11},globalThis)]},void 0,!0,{fileName:ut,lineNumber:36,columnNumber:9},globalThis)]},void 0,!0,{fileName:ut,lineNumber:20,columnNumber:7},globalThis),t.exports.jsxDEV(q,{children:t.exports.jsxDEV(ru,{searchString:e,setSearchString:s},void 0,!1,{fileName:ut,lineNumber:61,columnNumber:9},globalThis)},void 0,!1,{fileName:ut,lineNumber:60,columnNumber:7},globalThis)]},void 0,!0,{fileName:ut,lineNumber:9,columnNumber:5},globalThis)};var et="/Users/zimdev/work/zim-app/src/pages/chat/components/userItemInfo.tsx";const du=St.memo(function({avatarUrl:s,visibleLastOnline:o,lastOnline:l,username:a}){return t.exports.jsxDEV(He,{zIndex:"10",boxSize:"2em",src:s,name:a,children:o&&t.exports.jsxDEV(xi,{boxSize:"15px",bg:l==="online"?"green.500":"gray.300"},void 0,!1,{fileName:et,lineNumber:38,columnNumber:9},this)},void 0,!1,{fileName:et,lineNumber:36,columnNumber:5},this)}),ss=c.exports.memo(e=>{const d=e,{onClick:s,avatarUrl:o,username:l,usernameClassName:a,positionName:i,lastOnline:n,visibleLastOnline:r=!0,isOwner:u,channel:g,id:b}=d,m=Ue(d,["onClick","avatarUrl","username","usernameClassName","positionName","lastOnline","visibleLastOnline","isOwner","channel","id"]),v=c.exports.useCallback(()=>{s(b,g)},[g,b]);return t.exports.jsxDEV(S,y(x({w:"100%",display:"flex",alignItems:"start",cursor:"pointer",onClick:v,maxW:{base:"full"},className:" space-x-2.5",bg:j("white",""),p:3,rounded:"lg"},m),{children:[t.exports.jsxDEV(du,{avatarUrl:o,lastOnline:n,visibleLastOnline:r,username:l},void 0,!1,{fileName:et,lineNumber:82,columnNumber:7},globalThis),t.exports.jsxDEV(S,{flex:1,children:[t.exports.jsxDEV(q,{children:[t.exports.jsxDEV(M,{noOfLines:1,fontSize:14,lineHeight:"18px",fontWeight:"medium",color:j("gray.800","slate.300"),className:`${a}`,children:l},void 0,!1,{fileName:et,lineNumber:90,columnNumber:11},globalThis),u&&t.exports.jsxDEV(Ea,{},void 0,!1,{fileName:et,lineNumber:100,columnNumber:23},globalThis)]},void 0,!0,{fileName:et,lineNumber:89,columnNumber:9},globalThis),t.exports.jsxDEV(M,{noOfLines:1,fontSize:12,lineHeight:"24px",color:"gray.400",children:i},void 0,!1,{fileName:et,lineNumber:102,columnNumber:9},globalThis)]},void 0,!0,{fileName:et,lineNumber:88,columnNumber:7},globalThis)]}),void 0,!0,{fileName:et,lineNumber:67,columnNumber:5},globalThis)});var uo="/Users/zimdev/work/zim-app/src/pages/chat/components/conversationLoading.tsx";const dl=()=>t.exports.jsxDEV(q,{px:3,py:2,borderBottomWidth:1,bg:j("white","#1e293b50"),children:[t.exports.jsxDEV(va,{size:"10"},void 0,!1,{fileName:uo,lineNumber:16,columnNumber:7},globalThis),t.exports.jsxDEV(Aa,{flex:1,noOfLines:2,spacing:"2",skeletonHeight:5},void 0,!1,{fileName:uo,lineNumber:17,columnNumber:7},globalThis)]},void 0,!0,{fileName:uo,lineNumber:10,columnNumber:5},globalThis);var Ts="/Users/zimdev/work/zim-app/src/pages/chat/components/channelItem.tsx";const pl=c.exports.memo(({conversations:e,onClick:s,isLoading:o,canLoadMore:l,onLoadMore:a,loading:i})=>{const n=c.exports.useMemo(()=>e==null?void 0:e.map((r,u)=>{const g=r==null?void 0:r.user,b=(g==null?void 0:g.role_id)===5?"student":(g==null?void 0:g.role_id)===14?"customer":"zimians";return i?t.exports.jsxDEV(dl,{},`loading_${u}`,!1,{fileName:Ts,lineNumber:18,columnNumber:18},globalThis):c.exports.createElement(ss,y(x({},g),{lastOnline:g==null?void 0:g.status_online,avatarUrl:g==null?void 0:g.avatar,username:g==null?void 0:g.full_name,positionName:g==null?void 0:g.role_name,key:u,onClick:s,channel:b,__self:globalThis,__source:{fileName:Ts,lineNumber:21,columnNumber:11}}))}),[e]);return t.exports.jsxDEV(ie,{alignItems:"stretch",children:[n,l&&t.exports.jsxDEV(Y,{isLoading:o,loadingText:"\u0110ang t\u1EA3i th\xEAm",onClick:a,w:"100%",rounded:"none",borderWidth:"1px 0px 0px 0px",bg:j("white","slate.600"),fontSize:14,fontWeight:400,justifyContent:"center",children:"Xem th\xEAm"},void 0,!1,{fileName:Ts,lineNumber:39,columnNumber:11},globalThis)]},void 0,!0,{fileName:Ts,lineNumber:36,columnNumber:7},globalThis)});var ct="/Users/zimdev/work/zim-app/src/components/Accordion/item.tsx";const pu=({title:e,onClick:s,children:o,overallMessageUnRead:l})=>t.exports.jsxDEV(Ni,{borderBottom:1,borderColor:"",children:[t.exports.jsxDEV(q,{position:"relative",spacing:0,pl:4,_hover:{backgroundColor:j("#f0f0f0","slate.600")},children:[t.exports.jsxDEV(S,{onClick:s,display:"flex",justifyContent:"start",flex:1,as:"button",children:t.exports.jsxDEV(M,{color:j("#444","white"),fontSize:16,fontWeight:"medium",children:e},void 0,!1,{fileName:ct,lineNumber:34,columnNumber:11},globalThis)},void 0,!1,{fileName:ct,lineNumber:27,columnNumber:9},globalThis),t.exports.jsxDEV(Ei,{display:"flex",alignItems:"center",justifyContent:"center",w:10,children:t.exports.jsxDEV(vi,{},void 0,!1,{fileName:ct,lineNumber:48,columnNumber:11},globalThis)},void 0,!1,{fileName:ct,lineNumber:42,columnNumber:9},globalThis),l&&t.exports.jsxDEV(yo,{count:l},void 0,!1,{fileName:ct,lineNumber:50,columnNumber:34},globalThis)]},void 0,!0,{fileName:ct,lineNumber:21,columnNumber:7},globalThis),t.exports.jsxDEV(Ai,{p:0,children:o},void 0,!1,{fileName:ct,lineNumber:52,columnNumber:7},globalThis)]},void 0,!0,{fileName:ct,lineNumber:20,columnNumber:5},globalThis);var bl="/Users/zimdev/work/zim-app/src/components/Accordion/index.tsx";const gl=c.exports.memo(({title:e="Accordion",children:s,onClick:o,overallMessageUnRead:l})=>t.exports.jsxDEV(Ti,{defaultIndex:[0],allowMultiple:!0,children:t.exports.jsxDEV(pu,{overallMessageUnRead:l,title:e,onClick:o,children:s},void 0,!1,{fileName:bl,lineNumber:21,columnNumber:9},globalThis)},void 0,!1,{fileName:bl,lineNumber:20,columnNumber:7},globalThis));function co(...e){const s=o=>o&&typeof o=="object";return e.reduce((o,l)=>(Object.keys(l).forEach(a=>{const i=o[a],n=l[a];Array.isArray(i)&&Array.isArray(n)?o[a]=ps(i.concat(...n),"id"):s(i)&&s(n)?o[a]=co(i,n):o[a]=n}),o),{})}function hl(...e){const s=o=>o&&typeof o=="object";return e.reduce((o,l)=>(Object.keys(l).forEach(a=>{const i=o[a],n=l[a];Array.isArray(i)&&Array.isArray(n)?o[a]=ps(i.concat(...n),"user"):s(i)&&s(n)?o[a]=hl(i,n):o[a]=n}),o),{})}const Dt=({hasNextPage:e,fetchMore:s,variables:o={},isNormalLoadMore:l=!0})=>{const[a,i]=c.exports.useState(!1),n=l?co:hl,r=c.exports.useCallback(async()=>{a||!e||(await i(!0),await s({variables:o,updateQuery:(u,{fetchMoreResult:g})=>g?n(u,g):u}),await i(!1))},[a,e,i,s,o]);return c.exports.useMemo(()=>({onLoadMore:r,isLoadingMore:a}),[r,a])},fl=({roles:e,searchString:s})=>{var m,v,d;const o=be({roles:e,searchString:s,limit:10,page:1},h=>h),{data:l,loading:a,fetchMore:i}=De(sl,{variables:o}),n=(m=l==null?void 0:l.getAccountChatPagination)==null?void 0:m.docs,r=(v=l==null?void 0:l.getAccountChatPagination)==null?void 0:v.page,u=(d=l==null?void 0:l.getAccountChatPagination)==null?void 0:d.hasNextPage,{onLoadMore:g,isLoadingMore:b}=Dt({variables:y(x({},o),{page:r+1}),fetchMore:i,hasNextPage:u,isNormalLoadMore:!1});return c.exports.useMemo(()=>({listAccount:n,loading:a,onLoadMore:g,hasNextPage:u,isLoadingMore:b}),[n,a,g,u,b])},bu=f`
  mutation sendMessage(
    $recipient: RecipientInput!
    $message: MessageInput
    $senderAction: SenderAction
  ) @api(name: "chat") {
    sendMessage(
      recipient: $recipient
      message: $message
      senderAction: $senderAction
    ) {
      id
      createdAt
      updatedAt
      lastMessage {
        id
        text
        from
        to
        type
        attachments {
          type
          attachmentId
          attachment {
            id
            path
            fullUrl
            type
          }
        }
        loading @client
        error @client
        seenBy
        createdAt
        updatedAt
        deletedAt
        callPayload
      }
    }
  }
`,gu=f`
  mutation removeMessage($id: ObjectID!) @api(name: "chat") {
    removeMessage(id: $id) {
      id
      name
      createdAt
      updatedAt
      isDeleteMessage
      lastMessage {
        id
        text
        from
        to
        type
        attachments {
          type
          attachmentId
          attachment {
            id
            path
            fullUrl
            type
          }
        }
        loading @client
        error @client
        seenBy
        createdAt
        updatedAt
        deletedAt
        callPayload
      }
    }
  }
`,hu=f`
  mutation createConversation(
    $name: String
    $group: String
    $userIds: [String!]!
    $isChannel: Boolean
  ) @api(name: "chat") {
    createConversation(
      name: $name
      group: $group
      userIds: $userIds
      isChannel: $isChannel
    ) {
      id
      name
    }
  }
`,fu=f`
  mutation updateConversation(
    $name: String
    $id: ObjectID!
    $userIds: [String!]!
  ) @api(name: "chat") {
    updateConversation(name: $name, id: $id, userIds: $userIds) {
      id
      name
    }
  }
`,xu=f`
  mutation takeNoteForConversation(
    $conversationId: ObjectID!
    $content: String!
  ) @api(name: "chat") {
    takeNoteForConversation(
      conversationId: $conversationId
      content: $content
    ) {
      id
      content
      conversationId
      createdAt
      createdBy
    }
  }
`,Nu=f`
  mutation removeMemberFromParticipant($userId: String!, $id: ObjectID!)
  @api(name: "chat") {
    removeMemberFromParticipant(userId: $userId, id: $id) {
      id
    }
  }
`,Eu=f`
  mutation createTagForConversation(
    $tagId: ObjectID
    $conversationId: ObjectID
    $name: String
    $color: String
  ) @api(name: "chat") {
    createTagForConversation(
      tagId: $tagId
      conversationId: $conversationId
      name: $name
      color: $color
    ) {
      id
      tags {
        id
        name
        color
      }
    }
  }
`,vu=f`
  mutation markDoneConversation($conversationId: ObjectID) @api(name: "chat") {
    markDoneConversation(id: $conversationId) {
      id
    }
  }
`,Au=f`
  mutation deleteTagInConversation(
    $conversationId: ObjectID!
    $tagId: ObjectID!
  ) @api(name: "chat") {
    deleteTagInConversation(conversationId: $conversationId, tagId: $tagId) {
      id
      tags {
        id
      }
    }
  }
`,Tu=f`
  mutation createQAPost($input: QAPostInput!, $userId: String)
  @api(name: "zim") {
    createQAPost(input: $input, userId: $userId) {
      id
      title
      content
    }
  }
`,yu=f`
  mutation postComment($content: String!, $type: CommentType, $ref: ObjectID!)
  @api(name: "zim") {
    postComment(content: $content, type: $type, ref: $ref) {
      ... on PostComment {
        id
      }
      ... on QAPostComment {
        id
      }
    }
  }
`,Du=f`
  mutation sendReactionMessage($messageId: ObjectID, $emoji: String)
  @api(name: "chat") {
    sendReactionMessage(messageId: $messageId, emoji: $emoji) {
      id
      emojis {
        userId
        emoji
      }
    }
  }
`,ju=()=>{const{innerWidth:e,innerHeight:s}=window,[o,l]=c.exports.useState(e),[a,i]=c.exports.useState(s),n=c.exports.useCallback(()=>{l(window.innerWidth),i(window.innerHeight)},[]);return ul("resize",n),{width:o,height:a}};var os="/Users/zimdev/work/zim-app/src/pages/chat/components/headerMobileChat.tsx";const Cu=({item:e,onClick:s,isActive:o})=>t.exports.jsxDEV(S,{id:"js-toggle-profile",onClick:s,w:10,h:10,p:1,rounded:"full",bg:j(o?"blue.400":"gray.100",o?"blue.400":"#1e293b"),overflow:"hidden",display:"flex",alignItems:"center",justifyContent:"center",color:o?"white":"black",children:e.icon},void 0,!1,{fileName:os,lineNumber:15,columnNumber:5},globalThis),$u=()=>{const e=V(o=>o.setTab),s=V(o=>o.tab);return t.exports.jsxDEV(q,{w:"full",bg:j("white","#10172a"),p:2,justifyContent:"flex-end",alignItems:"center",borderBottomWidth:1,spacing:6,children:[{icon:t.exports.jsxDEV(Ta,{},void 0,!1,{fileName:os,lineNumber:51,columnNumber:17},globalThis),key:"channel",onClick:()=>e("channel")},{icon:t.exports.jsxDEV(ya,{},void 0,!1,{fileName:os,lineNumber:53,columnNumber:17},globalThis),key:"conversation",onClick:()=>e("conversation")}].map((o,l)=>{const a=o.key===s;return t.exports.jsxDEV(Cu,{isActive:a,item:o,onClick:o.onClick},l,!1,{fileName:os,lineNumber:60,columnNumber:11},globalThis)})},void 0,!1,{fileName:os,lineNumber:41,columnNumber:5},globalThis)},xl=(e,s=new Date)=>{var u,g;const o=Math.abs(Math.max(Date.parse(s),Date.parse(e))-Math.min(Date.parse(e),Date.parse(s))),l=1e3,a=60*l,i=60*a,n=(u=Math.floor(o/i))!=null?u:0,r=(g=Math.floor(o%i/a).toLocaleString("en-US",{minimumIntegerDigits:1}))!=null?g:"0";return n&&n<24?`${n} gi\u1EDD tr\u01B0\u1EDBc.`:n===0?`${r} ph\xFAt tr\u01B0\u1EDBc.`:n>=24?n/24>31?Ke(e).format("DD/MM/YYYY"):`${Math.floor(n/24)} ng\xE0y tr\u01B0\u1EDBc.`:"v\u1EEBa xong."},Su="5.5.8",Vu=60,ku=0,wu=110,Iu=144,Pu=105,_u="typing indicator",Fu=0,Mu=[],qu=[{ddd:0,ind:1,ty:3,nm:"\u25BD Dots",sr:1,ks:{o:{a:0,k:100,ix:11},r:{a:0,k:0,ix:10},p:{a:0,k:[72,51,0],ix:2},a:{a:0,k:[36,9,0],ix:1},s:{a:0,k:[100,100,100],ix:6}},ao:0,ip:0,op:110,st:0,bm:0},{ddd:0,ind:2,ty:4,nm:"dot 1",parent:1,sr:1,ks:{o:{a:0,k:100,ix:11},r:{a:0,k:0,ix:10},p:{a:1,k:[{i:{x:.667,y:1},o:{x:.333,y:0},t:0,s:[9,9,0],to:[0,0,0],ti:[0,0,0]},{i:{x:.667,y:1},o:{x:.333,y:0},t:3.053,s:[9,10,0],to:[0,0,0],ti:[0,0,0]},{i:{x:.667,y:1},o:{x:.333,y:0},t:15.273,s:[9,-9,0],to:[0,0,0],ti:[0,0,0]},{i:{x:.667,y:1},o:{x:.333,y:0},t:25.174,s:[9,12,0],to:[0,0,0],ti:[0,0,0]},{t:32.744140625,s:[9,9,0]}],ix:2},a:{a:0,k:[0,0,0],ix:1},s:{a:0,k:[100,100,100],ix:6}},ao:0,shapes:[{ty:"gr",it:[{d:1,ty:"el",s:{a:0,k:[6,6],ix:2},p:{a:0,k:[0,0],ix:3},nm:"Ellipse Path 1",mn:"ADBE Vector Shape - Ellipse",hd:!1},{ty:"fl",c:{a:1,k:[{i:{x:[.472],y:[1]},o:{x:[1],y:[0]},t:0,s:[.690196078431,.725490196078,.752941176471,1]},{i:{x:[.49],y:[1]},o:{x:[1],y:[0]},t:15.273,s:[.349019616842,.392156869173,.427450984716,1]},{t:32.072265625,s:[.690196078431,.725490196078,.752941176471,1]}],ix:4},o:{a:0,k:100,ix:5},r:1,bm:0,nm:"Fill 1",mn:"ADBE Vector Graphic - Fill",hd:!1},{ty:"tr",p:{a:0,k:[0,0],ix:2},a:{a:0,k:[0,0],ix:1},s:{a:0,k:[300,300],ix:3},r:{a:0,k:0,ix:6},o:{a:0,k:100,ix:7},sk:{a:0,k:0,ix:4},sa:{a:0,k:0,ix:5},nm:"Transform"}],nm:"dot 1",np:2,cix:2,bm:0,ix:1,mn:"ADBE Vector Group",hd:!1}],ip:0,op:110,st:0,bm:0},{ddd:0,ind:3,ty:4,nm:"dot 2",parent:1,sr:1,ks:{o:{a:0,k:100,ix:11},r:{a:0,k:0,ix:10},p:{a:1,k:[{i:{x:.667,y:1},o:{x:.333,y:0},t:10.691,s:[36,9,0],to:[0,0,0],ti:[0,0,0]},{i:{x:.667,y:1},o:{x:.333,y:0},t:15.273,s:[36,10,0],to:[0,0,0],ti:[0,0,0]},{i:{x:.667,y:1},o:{x:.333,y:0},t:25.963,s:[36,-9,0],to:[0,0,0],ti:[0,0,0]},{i:{x:.667,y:1},o:{x:.333,y:0},t:36.984,s:[36,14.326,0],to:[0,0,0],ti:[0,0,0]},{t:43.97265625,s:[36,9,0]}],ix:2},a:{a:0,k:[0,0,0],ix:1},s:{a:0,k:[100,100,100],ix:6}},ao:0,shapes:[{ty:"gr",it:[{d:1,ty:"el",s:{a:0,k:[6,6],ix:2},p:{a:0,k:[0,0],ix:3},nm:"Ellipse Path 1",mn:"ADBE Vector Shape - Ellipse",hd:!1},{ty:"fl",c:{a:1,k:[{i:{x:[.472],y:[1]},o:{x:[1],y:[0]},t:10.691,s:[.690196078431,.725490196078,.752941176471,1]},{i:{x:[.49],y:[1]},o:{x:[1],y:[0]},t:25.963,s:[.349019616842,.392156869173,.427450984716,1]},{t:42.763671875,s:[.690196078431,.725490196078,.752941176471,1]}],ix:4},o:{a:0,k:100,ix:5},r:1,bm:0,nm:"Fill 1",mn:"ADBE Vector Graphic - Fill",hd:!1},{ty:"tr",p:{a:0,k:[0,0],ix:2},a:{a:0,k:[0,0],ix:1},s:{a:0,k:[300,300],ix:3},r:{a:0,k:0,ix:6},o:{a:0,k:100,ix:7},sk:{a:0,k:0,ix:4},sa:{a:0,k:0,ix:5},nm:"Transform"}],nm:"dot 2",np:2,cix:2,bm:0,ix:1,mn:"ADBE Vector Group",hd:!1}],ip:0,op:110,st:0,bm:0},{ddd:0,ind:4,ty:4,nm:"dot 3",parent:1,sr:1,ks:{o:{a:0,k:100,ix:11},r:{a:0,k:0,ix:10},p:{a:1,k:[{i:{x:.667,y:1},o:{x:.333,y:0},t:21.383,s:[63,9,0],to:[0,0,0],ti:[0,0,0]},{i:{x:.667,y:1},o:{x:.333,y:0},t:25.963,s:[63,10,0],to:[0,0,0],ti:[0,0,0]},{i:{x:.667,y:1},o:{x:.333,y:0},t:38.184,s:[63,-9,0],to:[0,0,0],ti:[0,0,0]},{i:{x:.667,y:1},o:{x:.333,y:0},t:49.594,s:[63,13.139,0],to:[0,0,0],ti:[0,0,0]},{t:56,s:[63,9,0]}],ix:2},a:{a:0,k:[0,0,0],ix:1},s:{a:0,k:[100,100,100],ix:6}},ao:0,shapes:[{ty:"gr",it:[{d:1,ty:"el",s:{a:0,k:[6,6],ix:2},p:{a:0,k:[0,0],ix:3},nm:"Ellipse Path 1",mn:"ADBE Vector Shape - Ellipse",hd:!1},{ty:"fl",c:{a:1,k:[{i:{x:[.472],y:[1]},o:{x:[1],y:[0]},t:21.383,s:[.690196078431,.725490196078,.752941176471,1]},{i:{x:[.49],y:[1]},o:{x:[1],y:[0]},t:36.656,s:[.349019616842,.392156869173,.427450984716,1]},{t:53.45703125,s:[.690196078431,.725490196078,.752941176471,1]}],ix:4},o:{a:0,k:100,ix:5},r:1,bm:0,nm:"Fill 1",mn:"ADBE Vector Graphic - Fill",hd:!1},{ty:"tr",p:{a:0,k:[0,0],ix:2},a:{a:0,k:[0,0],ix:1},s:{a:0,k:[300,300],ix:3},r:{a:0,k:0,ix:6},o:{a:0,k:100,ix:7},sk:{a:0,k:0,ix:4},sa:{a:0,k:0,ix:5},nm:"Transform"}],nm:"dot 3",np:2,cix:2,bm:0,ix:1,mn:"ADBE Vector Group",hd:!1}],ip:0,op:110,st:0,bm:0}],zu=[];var Ou={v:Su,fr:Vu,ip:ku,op:wu,w:Iu,h:Pu,nm:_u,ddd:Fu,assets:Mu,layers:qu,markers:zu};const Nl=()=>{const e={animationData:Ou,loop:!0,autoplay:!0},{View:s}=Da(e);return s};var ys="/Users/zimdev/work/zim-app/src/pages/chat/components/groupAvatar.tsx";const El=({groupInfo:e})=>t.exports.jsxDEV(xt,{columns:2,spacing:1,children:[e==null?void 0:e.slice(0,3).map((s,o)=>t.exports.jsxDEV(He,{size:"2xs",src:s==null?void 0:s.avatar,name:s==null?void 0:s.fullName},o,!1,{fileName:ys,lineNumber:18,columnNumber:11},globalThis)),(e==null?void 0:e.length)>3&&t.exports.jsxDEV(S,{display:"flex",alignItems:"center",justifyContent:"center",w:"16px",h:"16px",rounded:"full",bg:"orange.500",children:t.exports.jsxDEV(M,{color:"white",fontSize:12,children:(e==null?void 0:e.length)-3<=9?`+${(e==null?void 0:e.length)-3}`:"..."},void 0,!1,{fileName:ys,lineNumber:36,columnNumber:11},globalThis)},void 0,!1,{fileName:ys,lineNumber:27,columnNumber:9},globalThis)]},void 0,!0,{fileName:ys,lineNumber:15,columnNumber:5},globalThis);var Ve="/Users/zimdev/work/zim-app/src/pages/chat/components/conversationItem.tsx";const Lu=({onClick:e,username:s,usernameClassName:o,lastMessage:l,seen:a,isActive:i,updatedAt:n,groupInfo:r,seenByCount:u,isTyping:g,avatarUrl:b,online:m,userTypingName:v})=>{const d=j("gray.500","slate.300"),h=()=>{if(u===(r==null?void 0:r.length)||!r)return null;if(r)return t.exports.jsxDEV(M,{color:d,fontSize:12,children:[u,"/",r==null?void 0:r.length]},void 0,!0,{fileName:Ve,lineNumber:54,columnNumber:9},globalThis)};return t.exports.jsxDEV(q,{_hover:{backgroundColor:"#e4e4e450"},w:"100%",align:"start",px:3,py:2,borderBottomWidth:1,cursor:"pointer",onClick:e,bg:j(i?"gray.100":"bg-white",i?"#1e293b":"#1e293b50"),children:[at.exports.isEmpty(r)?t.exports.jsxDEV(He,{boxSize:"2em",src:b,name:s,children:t.exports.jsxDEV(xi,{boxSize:"14px",bg:m?"green.500":"gray.300"},void 0,!1,{fileName:Ve,lineNumber:77,columnNumber:11},globalThis)},void 0,!1,{fileName:Ve,lineNumber:76,columnNumber:9},globalThis):t.exports.jsxDEV(El,{groupInfo:r},void 0,!1,{fileName:Ve,lineNumber:80,columnNumber:9},globalThis),t.exports.jsxDEV(ve,{flex:1,spacing:1,children:[t.exports.jsxDEV(M,{noOfLines:2,fontSize:15,lineHeight:"18px",color:j(a?"gray.700":"gray.900",a?"#ffffff50":"white"),className:`${o}`,textTransform:"capitalize",children:s},void 0,!1,{fileName:Ve,lineNumber:83,columnNumber:9},globalThis),g?t.exports.jsxDEV(q,{flex:1,children:[t.exports.jsxDEV(M,{noOfLines:1,fontSize:13,color:j("gray.700","white"),children:v==null?void 0:v.map(A=>A||"Someone is typing").join(",")},void 0,!1,{fileName:Ve,lineNumber:98,columnNumber:13},globalThis),t.exports.jsxDEV(S,{display:"flex",alignItems:"center",justifyContent:"center",w:10,h:5,children:t.exports.jsxDEV(Nl,{},void 0,!1,{fileName:Ve,lineNumber:114,columnNumber:15},globalThis)},void 0,!1,{fileName:Ve,lineNumber:107,columnNumber:13},globalThis)]},void 0,!0,{fileName:Ve,lineNumber:97,columnNumber:11},globalThis):t.exports.jsxDEV(M,{noOfLines:2,fontSize:12,lineHeight:"24px",fontWeight:a?"400":"600",color:j(a?"gray.700":"gray.900",a?"#ffffff50":"white"),children:l},void 0,!1,{fileName:Ve,lineNumber:118,columnNumber:11},globalThis)]},void 0,!0,{fileName:Ve,lineNumber:82,columnNumber:7},globalThis),t.exports.jsxDEV(ie,{spacing:2,alignItems:"flex-end",minW:"50px",children:[t.exports.jsxDEV(M,{color:j(a?"gray.700":"gray.900",a?"#ffffff50":"white"),fontSize:12,children:n},void 0,!1,{fileName:Ve,lineNumber:133,columnNumber:9},globalThis),t.exports.jsxDEV(q,{justifyContent:"space-between",children:[a&&t.exports.jsxDEV(ja,{},void 0,!1,{fileName:Ve,lineNumber:143,columnNumber:20},globalThis),h()]},void 0,!0,{fileName:Ve,lineNumber:142,columnNumber:9},globalThis)]},void 0,!0,{fileName:Ve,lineNumber:132,columnNumber:7},globalThis)]},void 0,!0,{fileName:Ve,lineNumber:61,columnNumber:5},globalThis)};var Ds="/Users/zimdev/work/zim-app/src/components/Forms/CreateConversationForm/components/userCheckBox.tsx";const Bu=e=>{const{isMultiple:s}=e,{getInputProps:o,getCheckboxProps:l}=s?yi(e):Di(e),a=o(),i=l();return t.exports.jsxDEV(S,{as:"label",rounded:"md",d:"block",bg:j("#1e293b","#1e293b50"),children:[t.exports.jsxDEV("input",y(x({},a),{style:{display:"none"}}),void 0,!1,{fileName:Ds,lineNumber:18,columnNumber:7},globalThis),t.exports.jsxDEV(S,y(x({},i),{cursor:"pointer",opacity:.8,pos:"relative",children:[a.checked&&t.exports.jsxDEV(ji,{color:"green.500",fontSize:"xl",pos:"absolute",top:2,right:2,zIndex:2},void 0,!1,{fileName:Ds,lineNumber:21,columnNumber:11},globalThis),e.children]}),void 0,!0,{fileName:Ds,lineNumber:19,columnNumber:7},globalThis)]},void 0,!0,{fileName:Ds,lineNumber:12,columnNumber:5},globalThis)},Ru=[{key:"",value:"All"},{key:"zimians",value:"Zimians"},{key:"student",value:"Student"}];function is(e,s){const[o,l]=c.exports.useState(e);return c.exports.useEffect(()=>{const a=setTimeout(()=>{l(e)},s);return()=>{clearTimeout(a)}},[e,s]),o}var ge="/Users/zimdev/work/zim-app/src/components/Forms/CreateConversationForm/components/listUser.tsx";const Uu=e=>e.scrollHeight-e.scrollTop<=e.clientHeight+10,Gu=({getCheckboxProps:e,listParticipants:s,usersInfo:o,isAddSupporter:l,value:a,setValue:i})=>{var F,Z,z,G;const[n]=ne(),[r,u]=c.exports.useState(""),[g,b]=c.exports.useState(!1),[m,v]=c.exports.useState(""),d=is(r,500),h=P=>{b(Uu(P.target))},A=be({roles:l?["EC"]:m==="all"?"":m==="zimians"?Yl:m==="student"?["HV"]:"",searchString:d,limit:20},P=>P),{data:E,loading:T,fetchMore:p}=De(sl,{variables:A}),D=(Z=(F=E==null?void 0:E.getAccountChatPagination)==null?void 0:F.docs)==null?void 0:Z.filter(P=>{var H,I,B,W;return!(s==null?void 0:s.includes((I=(H=P==null?void 0:P.user)==null?void 0:H.id)==null?void 0:I.toString()))&&((W=(B=P==null?void 0:P.user)==null?void 0:B.id)==null?void 0:W.toString())!==(n==null?void 0:n.id)}),N=(z=E==null?void 0:E.getAccountChatPagination)==null?void 0:z.page,$=(G=E==null?void 0:E.getAccountChatPagination)==null?void 0:G.hasNextPage,{onLoadMore:k,isLoadingMore:C}=Dt({variables:y(x({},A),{page:N+1}),fetchMore:p,hasNextPage:$,isNormalLoadMore:!1});return c.exports.useEffect(()=>{g&&$&&!C&&k()},[g,$,C]),t.exports.jsxDEV(ve,{spacing:3,children:[t.exports.jsxDEV(M,{fontSize:14,children:"Danh s\xE1ch th\xE0nh vi\xEAn"},void 0,!1,{fileName:ge,lineNumber:88,columnNumber:7},globalThis),t.exports.jsxDEV(q,{children:Ru.map((P,H)=>{const I=P.key===m;return t.exports.jsxDEV(Y,{onClick:()=>v(P.key),colorScheme:"teal",variant:I?"solid":"outline",disabled:l&&H!==1,children:P.value},H,!1,{fileName:ge,lineNumber:93,columnNumber:13},globalThis)})},void 0,!1,{fileName:ge,lineNumber:89,columnNumber:7},globalThis),t.exports.jsxDEV(Us,{my:2,children:o==null?void 0:o.map((P,H)=>t.exports.jsxDEV(Ci,{size:"md",rounded:"md",children:[t.exports.jsxDEV($i,{children:P.full_name},void 0,!1,{fileName:ge,lineNumber:109,columnNumber:15},globalThis),t.exports.jsxDEV(Si,{onClick:()=>i(a==null?void 0:a.filter(I=>{var B;return I!==((B=P==null?void 0:P.id)==null?void 0:B.toString())}))},void 0,!1,{fileName:ge,lineNumber:110,columnNumber:15},globalThis)]},H,!0,{fileName:ge,lineNumber:108,columnNumber:13},globalThis))},void 0,!1,{fileName:ge,lineNumber:105,columnNumber:7},globalThis),t.exports.jsxDEV(Ge,{type:"text",placeholder:"Nh\u1EADp t\xEAn ng\u01B0\u1EDDi d\xF9ng",size:"md",value:r,onChange:P=>u(P.target.value)},void 0,!1,{fileName:ge,lineNumber:119,columnNumber:7},globalThis),t.exports.jsxDEV(S,{position:"relative",h:"250px",overflow:"auto",w:"full",flexGrow:1,onScroll:h,children:[t.exports.jsxDEV(xt,{columns:2,spacing:2,children:[(D==null?void 0:D.length)>0&&(D==null?void 0:D.map(({user:P})=>{const H=e({value:P.id});return t.exports.jsxDEV(S,{rounded:"lg",overflow:"hidden",children:t.exports.jsxDEV(Bu,y(x({isMultiple:!0},H),{children:t.exports.jsxDEV(ss,y(x({},P),{username:P==null?void 0:P.full_name,positionName:P==null?void 0:P.role_name,lastOnline:P==null?void 0:P.status_online,px:3,py:2}),void 0,!1,{fileName:ge,lineNumber:143,columnNumber:21},globalThis)}),void 0,!1,{fileName:ge,lineNumber:142,columnNumber:19},globalThis)},`${P.id}`,!1,{fileName:ge,lineNumber:141,columnNumber:17},globalThis)})),T&&t.exports.jsxDEV(q,{rounded:"lg",bg:"blackAlpha.300",opacity:.5,pos:"absolute",top:0,bottom:0,right:0,left:0,justifyContent:"center",children:[t.exports.jsxDEV(Gt,{color:"#319795",size:"lg"},void 0,!1,{fileName:ge,lineNumber:167,columnNumber:15},globalThis),t.exports.jsxDEV(M,{fontSize:"xl",color:"white",children:"\u0110ang t\u1EA3i ..."},void 0,!1,{fileName:ge,lineNumber:168,columnNumber:15},globalThis)]},void 0,!0,{fileName:ge,lineNumber:156,columnNumber:13},globalThis)]},void 0,!0,{fileName:ge,lineNumber:134,columnNumber:9},globalThis),C&&t.exports.jsxDEV(q,{w:"full",alignItems:"center",justifyContent:"center",py:2,children:[t.exports.jsxDEV(Gt,{color:"#319795",size:"lg"},void 0,!1,{fileName:ge,lineNumber:181,columnNumber:13},globalThis),t.exports.jsxDEV(M,{fontSize:"xl",color:j("gray.700","white"),children:"\u0110ang t\u1EA3i ..."},void 0,!1,{fileName:ge,lineNumber:182,columnNumber:13},globalThis)]},void 0,!0,{fileName:ge,lineNumber:175,columnNumber:11},globalThis)]},void 0,!0,{fileName:ge,lineNumber:126,columnNumber:7},globalThis)]},void 0,!0,{fileName:ge,lineNumber:87,columnNumber:5},globalThis)},Hu=(e,s=null)=>{e.forEach(o=>{s==null||s.evict({id:"ROOT_QUERY",fieldName:o})})};function mt(){const e=Vi();return s=>{Hu(s,e.cache)}}const Wu=({onClose:e,userId:s,isEdit:o,groupId:l,setUserId:a,isAddSupporter:i,isChannel:n})=>{const[r]=ne(),u=Je(),g=mt(),b=V(E=>E.setConversationId),[m,{loading:v}]=Pe(hu,{onCompleted:E=>{const{createConversation:T}=E;T&&(u({title:"Th\xE0nh c\xF4ng!",description:`T\u1EA1o ${n?"channel":"nh\xF3m chat"} ${T==null?void 0:T.name} th\xE0nh c\xF4ng`,status:"success",duration:3e3,isClosable:!0}),e(),b(T==null?void 0:T.id)),a([]),g(["conversations","conversationDetail"])},onError:E=>{var T;u({title:`T\u1EA1o ${n?"channel":"nh\xF3m chat"} kh\xF4ng th\xE0nh c\xF4ng!`,description:`${(T=E==null?void 0:E.message)!=null?T:"L\u1ED7i"}`,status:"error",duration:3e3,isClosable:!0})}}),[d,{loading:h}]=Pe(fu,{onCompleted:E=>{const{updateConversation:T}=E;T&&u({title:"Th\xE0nh c\xF4ng!",description:i?"Th\xEAm EC h\u1ED7 tr\u1EE3 th\xE0nh c\xF4ng":`C\u1EADp nh\u1EADt ${n?"channel":"nh\xF3m chat"} ${T==null?void 0:T.name} th\xE0nh c\xF4ng`,status:"success",duration:3e3,isClosable:!0}),e(),a([]),g(["conversations","conversationDetail"])},onError:E=>{var T;u({title:`C\u1EADp nh\u1EADt ${n?"channel":"nh\xF3m chat"} kh\xF4ng th\xE0nh c\xF4ng!`,description:`${(T=E==null?void 0:E.message)!=null?T:"L\u1ED7i"}`,status:"error",duration:3e3,isClosable:!0})}});return{handleSubmitForm:c.exports.useCallback(async E=>{try{o?await d({variables:be({name:E==null?void 0:E.name,id:l,userIds:s==null?void 0:s.filter(T=>T!==(r==null?void 0:r.id))})}):await m({variables:be({isChannel:n,name:E==null?void 0:E.name,group:l,userIds:s})})}catch(T){console.log(T)}},[s,l,o,r,n]),loading:v||h}},Qu=e=>typeof e=="object"?Promise.resolve(e):ts.query({query:tl,variables:{userId:parseInt(e,10)}}).then(({data:s})=>s.getUserById),vl=(e,s)=>at.exports.isEmpty(e||s)?[]:Promise.allSettled(e==null?void 0:e.map(Qu)).then(o=>o.filter(l=>l.status==="fulfilled").map(l=>l==null?void 0:l.value));var _e="/Users/zimdev/work/zim-app/src/components/Forms/CreateConversationForm/index.tsx";const mo=({onClose:e,isOpen:s,groupId:o="",listParticipants:l,name:a,isEdit:i,isAddSupporter:n,isChannel:r})=>{const[u,g]=c.exports.useState([]),b=c.exports.useRef(),[m,v]=c.exports.useState([]),[d,h]=c.exports.useState(o),{getCheckboxProps:A,value:E,setValue:T}=ki({onChange:v}),{handleSubmitForm:p,loading:D}=Wu({onClose:e,userId:[...l!=null?l:[],...m!=null?m:[]],isEdit:i,groupId:d,setUserId:v,isAddSupporter:n,isChannel:r}),N=bs({mode:"onBlur",reValidateMode:"onChange"}),{register:$,formState:{errors:k}}=N,C=c.exports.useRef(null),F=N.watch("name"),Z=!at.exports.isEqual(F,C.current)||(m==null?void 0:m.length)>0;return Gs(()=>{i&&!n&&(N.reset({name:a}),C.current=a)},[s,i,C,N,a]),c.exports.useEffect(()=>{(async function(){try{const z=await vl(m,!s);g(z)}catch(z){console.log(z)}})()},[m]),c.exports.useEffect(()=>{h(o)},[o]),t.exports.jsxDEV(Hs,y(x({},N),{children:t.exports.jsxDEV(Vt,{finalFocusRef:b,isOpen:s,onClose:()=>{e(),T([])},size:"2xl",children:[t.exports.jsxDEV(Nt,{},void 0,!1,{fileName:_e,lineNumber:98,columnNumber:9},globalThis),t.exports.jsxDEV(kt,{bg:j(void 0,"slate.900"),children:[t.exports.jsxDEV(Et,{children:n?"Th\xEAm EC h\u1ED7 tr\u1EE3":i?"C\u1EADp nh\u1EADt nh\xF3m chat":r?"T\u1EA1o channel":"T\u1EA1o nh\xF3m chat"},void 0,!1,{fileName:_e,lineNumber:100,columnNumber:11},globalThis),t.exports.jsxDEV(wt,{},void 0,!1,{fileName:_e,lineNumber:109,columnNumber:11},globalThis),t.exports.jsxDEV("form",{onSubmit:N.handleSubmit(p),children:[t.exports.jsxDEV(It,{children:t.exports.jsxDEV(ve,{spacing:6,children:[!n&&t.exports.jsxDEV(Ge,y(x({label:r?"T\xEAn channel":"T\xEAn nh\xF3m",placeholder:"Nh\u1EADp t\xEAn nh\xF3m"},$("name",{required:!0})),{error:k==null?void 0:k.name}),void 0,!1,{fileName:_e,lineNumber:114,columnNumber:19},globalThis),t.exports.jsxDEV(Gu,{value:E,setValue:T,isAddSupporter:n,usersInfo:u,listParticipants:l,getCheckboxProps:A},void 0,!1,{fileName:_e,lineNumber:124,columnNumber:17},globalThis)]},void 0,!0,{fileName:_e,lineNumber:112,columnNumber:15},globalThis)},void 0,!1,{fileName:_e,lineNumber:111,columnNumber:13},globalThis),t.exports.jsxDEV(Ht,{children:[t.exports.jsxDEV(Y,{disabled:!i&&at.exports.isEmpty(m)||D||i&&!Z,leftIcon:t.exports.jsxDEV(Wt,{as:Ws},void 0,!1,{fileName:_e,lineNumber:141,columnNumber:27},globalThis),bg:"#319795",mr:4,type:"submit",isLoading:!1,color:"white",children:i?"C\u1EADp nh\u1EADt":"T\u1EA1o nh\xF3m"},void 0,!1,{fileName:_e,lineNumber:135,columnNumber:15},globalThis),t.exports.jsxDEV(Y,{disabled:D,variant:"outline",onClick:()=>{e(),T([]),v([])},children:"H\u1EE7y"},void 0,!1,{fileName:_e,lineNumber:150,columnNumber:15},globalThis)]},void 0,!0,{fileName:_e,lineNumber:134,columnNumber:13},globalThis)]},void 0,!0,{fileName:_e,lineNumber:110,columnNumber:11},globalThis)]},void 0,!0,{fileName:_e,lineNumber:99,columnNumber:9},globalThis)]},void 0,!0,{fileName:_e,lineNumber:89,columnNumber:7},globalThis)}),void 0,!1,{fileName:_e,lineNumber:88,columnNumber:5},globalThis)};var Fe="/Users/zimdev/work/zim-app/src/pages/chat/components/dataTabs.tsx";const po=[{key:"open",value:"Open"},{key:"following",value:"Following"},{key:"done",value:"Done"}],Zu=({data:e,isSelected:s,setParticipants:o,setUserId:l,loading:a,setLocalTab:i})=>{var T;const{isOpen:n,onOpen:r,onClose:u}=Xe(),g=V(p=>p.setTab),b=V(p=>p.channel),m=V(p=>p.conversationId),v=V(p=>p.setConversationId),[d]=ne(),[h,A]=vt({}),E=c.exports.useCallback(p=>{m===p.id&&g("message"),v(p.id),o(p==null?void 0:p.participants),l(""),g("message"),A({qsConversationId:p.id}),h.delete("qsUserId"),h.delete("qsGroup")},[m,h]);return t.exports.jsxDEV(Ca,{onChange:p=>{var D;return i((D=po[p])==null?void 0:D.key)},defaultIndex:po.findIndex(p=>p.key===s),children:[t.exports.jsxDEV(At,{flexDirection:{base:"row",md:"column"},position:"sticky",top:0,zIndex:20,bg:j("white","#10172a"),alignItems:"center",justifyContent:"center",children:[t.exports.jsxDEV(q,{w:{base:["my chats","guest"].includes(b)?"25%":"100%",md:"full"},alignItems:"center",justify:"space-between",spacing:4,px:2,borderBottomWidth:1,minH:"50px",children:[t.exports.jsxDEV(M,{fontSize:13,fontWeight:"bold",textColor:j("#444","slate.300"),children:(T=b==null?void 0:b.toString())==null?void 0:T.toLocaleUpperCase()},void 0,!1,{fileName:Fe,lineNumber:98,columnNumber:11},globalThis),!["my chats","guest"].includes(b)&&To.map(p=>p.id).includes(d==null?void 0:d.roleId)&&t.exports.jsxDEV(S,{onClick:r,"data-tip":"React-tooltip",display:"flex",alignItems:"center",justifyContent:"center",cursor:"pointer",children:[t.exports.jsxDEV($a,{fontSize:20},void 0,!1,{fileName:Fe,lineNumber:115,columnNumber:17},globalThis),t.exports.jsxDEV(wi,{children:t.exports.jsxDEV("p",{className:"text-base font-medium",children:"T\u1EA1o group chat"},void 0,!1,{fileName:Fe,lineNumber:118,columnNumber:21},globalThis),place:"bottom",backgroundColor:"green",effect:"float"},void 0,!1,{fileName:Fe,lineNumber:116,columnNumber:17},globalThis)]},void 0,!0,{fileName:Fe,lineNumber:107,columnNumber:15},globalThis)]},void 0,!0,{fileName:Fe,lineNumber:86,columnNumber:9},globalThis),b==="guest"&&t.exports.jsxDEV(q,{spacing:0,w:"full",minH:"50px",children:po.map((p,D)=>t.exports.jsxDEV(Sa,{minH:"50px",flex:1,_dark:{color:"slate.300"},w:"full",fontSize:{sm:14},px:0,borderBottomColor:j("gray.200","whiteAlpha.300"),mb:0,children:p.value},p.key,!1,{fileName:Fe,lineNumber:131,columnNumber:17},globalThis))},void 0,!1,{fileName:Fe,lineNumber:128,columnNumber:11},globalThis)]},void 0,!0,{fileName:Fe,lineNumber:74,columnNumber:7},globalThis),t.exports.jsxDEV(ve,{spacing:0,children:(e==null?void 0:e.length)>0?e.map((p,D)=>{var P,H,I,B,W,re,w,O,se,ze,$e,Ie;const N=p==null?void 0:p.participants,$=!(N==null?void 0:N.find(oe=>oe.userId===(d==null?void 0:d.id)))&&(N==null?void 0:N.length)>=2||(N==null?void 0:N.find(oe=>oe.userId===(d==null?void 0:d.id)))&&(N==null?void 0:N.length)>=3,k=(p==null?void 0:p.name)||$?N:N==null?void 0:N.filter(oe=>oe.userId!==(d==null?void 0:d.id))[0],C=(k==null?void 0:k.isGuest)?k==null?void 0:k.fullName:(p==null?void 0:p.name)?p==null?void 0:p.name:$?N==null?void 0:N.map(oe=>oe.fullName).join(","):k==null?void 0:k.fullName,F=((H=(P=p==null?void 0:p.usersTyping)==null?void 0:P.filter(oe=>oe!==(d==null?void 0:d.id)))==null?void 0:H.length)>0,Z=(p==null?void 0:p.lastMessage)?(B=(I=p==null?void 0:p.lastMessage)==null?void 0:I.seenBy)==null?void 0:B.includes(d.id):!0,z=xl(Ke(p==null?void 0:p.updatedAt).toDate()),G=((re=(W=p==null?void 0:p.lastMessage)==null?void 0:W.attachments)==null?void 0:re.length)>0?["[T\u1EC7p \u0111\xEDnh k\xE8m]"]:(w=p==null?void 0:p.lastMessage)==null?void 0:w.text;return a?t.exports.jsxDEV(dl,{},D,!1,{fileName:Fe,lineNumber:189,columnNumber:22},globalThis):c.exports.createElement(Lu,y(x({},k),{isTyping:F,isActive:p.id===m,key:p.id,onClick:()=>{E(p)},username:C,lastMessage:G,updatedAt:z,seen:Z,seenByCount:(ze=(se=(O=p==null?void 0:p.lastMessage)==null?void 0:O.seenBy)==null?void 0:se.length)!=null?ze:0,avatarUrl:k==null?void 0:k.avatar,groupInfo:(p==null?void 0:p.name)||$?k:null,userTypingName:(Ie=($e=p==null?void 0:p.usersTyping)==null?void 0:$e.filter(oe=>oe!==d.id))==null?void 0:Ie.map(oe=>{var qe;return(qe=N.find(Oe=>Oe.userId===oe))==null?void 0:qe.fullName}),__self:globalThis,__source:{fileName:Fe,lineNumber:192,columnNumber:15}}))}):t.exports.jsxDEV(M,{textAlign:"center",mt:4,children:`Hi\u1EC7n t\u1EA1i kh\xF4ng c\xF3 ${b==="guest"?"y\xEAu c\u1EA7u h\u1ED7 tr\u1EE3":"cu\u1ED9c tr\xF2 chuy\u1EC7n"} n\xE0o !!!`},void 0,!1,{fileName:Fe,lineNumber:220,columnNumber:11},globalThis)},void 0,!1,{fileName:Fe,lineNumber:154,columnNumber:7},globalThis),t.exports.jsxDEV(mo,{isChannel:!1,isAddSupporter:!1,isEdit:!1,name:"",listParticipants:[],isOpen:n,onClose:u,groupId:b},void 0,!1,{fileName:Fe,lineNumber:227,columnNumber:7},globalThis)]},void 0,!0,{fileName:Fe,lineNumber:70,columnNumber:5},globalThis)},Yu=({status:e="",search:s,channel:o})=>{var h,A,E;const l=y(x({},be({status:e,search:s},T=>T)),{limit:20,offset:0}),[a,{data:i,loading:n,fetchMore:r,called:u}]=Qs(Be,{variables:l,fetchPolicy:"network-only",nextFetchPolicy:"cache-first"});c.exports.useEffect(()=>{o&&o==="guest"&&!u&&(async()=>await a())()},[o,u]);const g=((h=i==null?void 0:i.supportConversations)==null?void 0:h.docs)||[],b=(A=i==null?void 0:i.supportConversations)==null?void 0:A.hasNextPage,{onLoadMore:m,isLoadingMore:v}=Dt({variables:y(x({},l),{offset:(g==null?void 0:g.length)+1}),fetchMore:r,hasNextPage:b});return{listSupportConversations:((E=i==null?void 0:i.supportConversations)==null?void 0:E.docs)||[],loading:n,onLoadMore:m,hasNextPage:b,isLoadingMore:v}},Ku=({search:e,channel:s})=>{var v,d;const o=y(x({},be({group:s==="my chats"?"":s,search:e},h=>h)),{limit:20,offset:0}),[l,{data:a,loading:i,fetchMore:n,called:r}]=Qs(Ae,{variables:o,fetchPolicy:"network-only",nextFetchPolicy:"cache-first"});c.exports.useEffect(()=>{s&&s!=="guest"&&!r&&(async()=>await l())()},[s,r,l]);const u=((v=a==null?void 0:a.conversations)==null?void 0:v.docs)||[],g=(d=a==null?void 0:a.conversations)==null?void 0:d.hasNextPage,{onLoadMore:b,isLoadingMore:m}=Dt({variables:y(x({},o),{offset:(u==null?void 0:u.length)+1}),fetchMore:n,hasNextPage:g});return{listConversations:u,loading:i,onLoadMore:b,hasNextPage:g,isLoadingMore:m}},Ju=e=>e.scrollHeight-e.scrollTop<=e.clientHeight+10;var bo="/Users/zimdev/work/zim-app/src/components/Loading/index.tsx";const ls=({text:e="\u0110ang l\u1EA5y d\u1EEF li\u1EC7u...",color:s="gray.900"})=>t.exports.jsxDEV(q,{justifyContent:"center",pointerEvents:"none",userSelect:"none",children:[t.exports.jsxDEV(Gt,{color:s},void 0,!1,{fileName:bo,lineNumber:5,columnNumber:5},globalThis),t.exports.jsxDEV(M,{textAlign:"center",color:s,children:e},void 0,!1,{fileName:bo,lineNumber:6,columnNumber:5},globalThis)]},void 0,!0,{fileName:bo,lineNumber:4,columnNumber:3},globalThis);var dt="/Users/zimdev/work/zim-app/src/pages/chat/components/messageSearchResultItem.tsx";const Xu=({onClick:e,username:s,lastMessage:o,isActive:l,createdAt:a,avatarUrl:i})=>{const n=V(r=>r.searchStringMessage);return t.exports.jsxDEV(q,{_hover:{backgroundColor:"#e4e4e450"},w:"100%",align:"start",p:3,py:2,shadow:"md",cursor:"pointer",rounded:"lg",onClick:e,bg:j(l?"gray.100":"bg-white",l?"#1e293b":"#1e293b50"),children:[t.exports.jsxDEV(He,{boxSize:"2em",src:i,name:s},void 0,!1,{fileName:dt,lineNumber:45,columnNumber:7},globalThis),t.exports.jsxDEV(ve,{flex:1,spacing:1,children:[t.exports.jsxDEV(M,{noOfLines:2,fontSize:15,lineHeight:"18px",color:j("gray.900","slate.300"),children:s},void 0,!1,{fileName:dt,lineNumber:47,columnNumber:9},globalThis),t.exports.jsxDEV(M,{noOfLines:2,fontSize:12,lineHeight:"24px",fontWeight:"400",color:j("gray.900","slate.300"),children:t.exports.jsxDEV(Ii,{searchWords:[n],autoEscape:!0,textToHighlight:o},void 0,!1,{fileName:dt,lineNumber:62,columnNumber:11},globalThis)},void 0,!1,{fileName:dt,lineNumber:55,columnNumber:9},globalThis)]},void 0,!0,{fileName:dt,lineNumber:46,columnNumber:7},globalThis),t.exports.jsxDEV(ie,{spacing:2,alignItems:"flex-end",minW:"50px",children:t.exports.jsxDEV(M,{color:j("gray.900","slate.300"),fontSize:12,children:a},void 0,!1,{fileName:dt,lineNumber:70,columnNumber:9},globalThis)},void 0,!1,{fileName:dt,lineNumber:69,columnNumber:7},globalThis)]},void 0,!0,{fileName:dt,lineNumber:30,columnNumber:5},globalThis)};var js="/Users/zimdev/work/zim-app/src/pages/chat/components/listItemSearchMessage.tsx";const ec=()=>{const e=V(r=>r.searchStringMessage),s=V(r=>r.searchResult),o=V(r=>r.setMessageId),l=V(r=>r.messageId),a=V(r=>r.setBeforeId),i=V(r=>r.setAfterId),n=r=>{o(r),a(""),i("")};return t.exports.jsxDEV(ve,{w:"full",h:"full",p:2,spacing:2,children:[t.exports.jsxDEV(q,{position:"sticky",top:0,zIndex:50,bg:j("white","#10172a"),alignItems:"center",children:t.exports.jsxDEV(M,{fontSize:16,fontWeight:"bold",textColor:j("#444","slate.300"),children:`C\xF3 ${s==null?void 0:s.length} k\u1EBFt qu\u1EA3 t\xECm ki\u1EBFm cho t\u1EEB kh\xF3a "${e}"`},void 0,!1,{fileName:js,lineNumber:29,columnNumber:9},globalThis)},void 0,!1,{fileName:js,lineNumber:22,columnNumber:7},globalThis),s==null?void 0:s.map(r=>{var m,v;const u=(v=(m=r==null?void 0:r.conversation)==null?void 0:m.participants)==null?void 0:v.find(d=>d.userId===(r==null?void 0:r.from)),g=xl(Ke(r==null?void 0:r.createdAt).toDate()),b=r.id===l;return t.exports.jsxDEV(Xu,{isActive:b,onClick:()=>n(r.id),lastMessage:r==null?void 0:r.text,avatarUrl:u==null?void 0:u.avatar,username:u==null?void 0:u.fullName,createdAt:g},r.id,!1,{fileName:js,lineNumber:46,columnNumber:11},globalThis)})]},void 0,!0,{fileName:js,lineNumber:21,columnNumber:5},globalThis)};var Cs="/Users/zimdev/work/zim-app/src/pages/chat/myListConversation/index.tsx";const tc=()=>{const e=V(N=>N.setParticipants),s=V(N=>N.searchStringMessage),o=V(N=>N.channel),l=V(N=>N.setUserId),[a,i]=c.exports.useState(!1),[n,r]=c.exports.useState("open"),{listSupportConversations:u,onLoadMore:g,isLoadingMore:b,hasNextPage:m,loading:v}=Yu({status:n,search:"",channel:o}),{listConversations:d,onLoadMore:h,hasNextPage:A,isLoadingMore:E,loading:T}=Ku({channel:o,search:""}),p=o==="guest"?{onLoadMore:g,isLoadingMore:b,hasNextPage:m,loading:v}:{onLoadMore:h,hasNextPage:A,isLoadingMore:E,loading:T},D=c.exports.useCallback(N=>{i(Ju(N.target))},[]);return Gs(()=>{a&&p.hasNextPage&&!p.isLoadingMore&&p.onLoadMore()},[a,p]),t.exports.jsxDEV(At,{direction:"column",w:{base:"full",md:"260px",lg:"350px"},h:"100%",bg:j("white","#10172a"),onScroll:D,overflow:"auto",position:"relative",children:[s?t.exports.jsxDEV(ec,{},void 0,!1,{fileName:Cs,lineNumber:77,columnNumber:9},globalThis):t.exports.jsxDEV(Zu,{setParticipants:e,setLocalTab:r,setUserId:l,loading:p.loading,data:p.loading?[1,2,3,4,5,6,7,8]:o==="guest"?u:d,isSelected:n},void 0,!1,{fileName:Cs,lineNumber:79,columnNumber:9},globalThis),p.isLoadingMore&&t.exports.jsxDEV(ls,{text:"\u0110ang t\u1EA3i cu\u1ED9c tr\xF2 chuy\u1EC7n",color:j("","white")},void 0,!1,{fileName:Cs,lineNumber:95,columnNumber:9},globalThis)]},void 0,!0,{fileName:Cs,lineNumber:63,columnNumber:5},globalThis)};var Al="/assets/weCanDoItWomen.png";const sc=()=>({onPush:c.exports.useCallback((s,o,l)=>{if(!Notification)return;Notification.permission!=="granted"&&Notification.requestPermission();let a=new Notification("B\u1EA1n c\xF3 tin nh\u1EAFn m\u1EDBi !!",{icon:Al,body:`${l}: ${o}`});a.onclick=function(){window.open(`${lu.apiUrl}/Admin/Chat?qsConversationId=${s}`)}},[])}),oc=()=>{const[e]=ne(),{onPush:s}=sc(),o=V(a=>a.conversationId),l=V(a=>a.userId);Rs(cl,{onSubscriptionData:({client:a,subscriptionData:i})=>{var I,B,W,re,w,O,se,ze,$e,Ie,oe,qe,Oe,J,Ee,it,R,fe,lt,Ot,Lt,Bt,Rt,Ut,Do,jo,Co,$o,So,Vo,ko,wo,Io,Po,_o,Fo,Mo,qo,zo,Oo,Lo,Bo,Ro,Uo,Go,Ho,Wo,Qo,Zo,Yo,Ko,Jo,Xo,ei,ti,si,oi,ii,li,ai,ri,ni,ui;const n=a.cache.readQuery({query:Ze,variables:y(x({},be({conversationId:o,userId:o?"":l==null?void 0:l.toString(),limit:30},_=>_)),{offset:0})}),r=(B=(I=n==null?void 0:n.messages)==null?void 0:I.docs)!=null?B:[],{conversationUpdated:u}=i.data,{lastMessage:g,typing:b}=u,{id:m,from:v}=g;if(!(r==null?void 0:r.find(_=>(_==null?void 0:_.id)===m))&&e.id!==v&&b===null){const _=(re=(W=u==null?void 0:u.participants)==null?void 0:W.find(X=>X.userId===v))==null?void 0:re.fullName;s((w=i==null?void 0:i.data)==null?void 0:w.conversationUpdated.id,(O=i==null?void 0:i.data)==null?void 0:O.conversationUpdated.lastMessage.text,_)}const h=(ze=(se=i==null?void 0:i.data)==null?void 0:se.conversationUpdated)==null?void 0:ze.group,A=(Ie=($e=i==null?void 0:i.data)==null?void 0:$e.conversationUpdated)==null?void 0:Ie.status,E=a.cache.readQuery({query:Ae,variables:{offset:0,limit:20}}),T=(qe=(oe=E==null?void 0:E.conversations)==null?void 0:oe.docs)!=null?qe:[];if((E==null?void 0:E.conversations)&&A!=="open"){let _=[...T];const X=_==null?void 0:_.find(L=>{var de,pe;return(L==null?void 0:L.id)===((pe=(de=i==null?void 0:i.data)==null?void 0:de.conversationUpdated)==null?void 0:pe.id)});X&&((Oe=i==null?void 0:i.data)==null?void 0:Oe.conversationUpdated.typing)===null?(_=_.filter(L=>L.id!==X.id),a.writeQuery({query:Ae,variables:{offset:0,limit:20},data:y(x({},E),{conversations:y(x({},(J=E.conversations)!=null?J:[]),{docs:[(Ee=i==null?void 0:i.data)==null?void 0:Ee.conversationUpdated,..._]})})})):X?a.writeQuery({query:Ae,variables:{offset:0,limit:20},data:x({},E)}):a.writeQuery({query:Ae,variables:{offset:0,limit:20},data:y(x({},E),{conversations:y(x({},(it=E.conversations)!=null?it:[]),{docs:[(R=i==null?void 0:i.data)==null?void 0:R.conversationUpdated,..._]})})})}const p=a.cache.readQuery({query:Ae,variables:{group:"zimians",offset:0,limit:20}}),D=(lt=(fe=p==null?void 0:p.conversations)==null?void 0:fe.docs)!=null?lt:[];if((p==null?void 0:p.conversations)&&h==="zimians"){let _=[...D];const X=_==null?void 0:_.find(L=>{var de,pe;return(L==null?void 0:L.id)===((pe=(de=i==null?void 0:i.data)==null?void 0:de.conversationUpdated)==null?void 0:pe.id)});X&&((Ot=i==null?void 0:i.data)==null?void 0:Ot.conversationUpdated.typing)===null?(_=_.filter(L=>L.id!==X.id),a.writeQuery({query:Ae,variables:{group:"zimians",offset:0,limit:20},data:y(x({},p),{conversations:y(x({},(Lt=p.conversations)!=null?Lt:[]),{docs:[(Bt=i==null?void 0:i.data)==null?void 0:Bt.conversationUpdated,..._]})})})):X?a.writeQuery({query:Ae,variables:{group:"zimians",offset:0,limit:20},data:x({},p)}):a.writeQuery({query:Ae,variables:{group:"zimians",offset:0,limit:20},data:y(x({},p),{conversations:y(x({},(Rt=p.conversations)!=null?Rt:[]),{docs:[(Ut=i==null?void 0:i.data)==null?void 0:Ut.conversationUpdated,..._]})})})}const N=a.cache.readQuery({query:Ae,variables:{group:"student",offset:0,limit:20}}),$=(jo=(Do=N==null?void 0:N.conversations)==null?void 0:Do.docs)!=null?jo:[];if((N==null?void 0:N.conversations)&&h==="student"){let _=[...$];const X=_==null?void 0:_.find(L=>{var de,pe;return(L==null?void 0:L.id)===((pe=(de=i==null?void 0:i.data)==null?void 0:de.conversationUpdated)==null?void 0:pe.id)});X&&((Co=i==null?void 0:i.data)==null?void 0:Co.conversationUpdated.typing)===null?(_=_.filter(L=>L.id!==X.id),a.writeQuery({query:Ae,variables:{group:"student",offset:0,limit:20},data:y(x({},N),{conversations:y(x({},($o=N.conversations)!=null?$o:[]),{docs:[(So=i==null?void 0:i.data)==null?void 0:So.conversationUpdated,..._]})})})):X?a.writeQuery({query:Ae,variables:{group:"student",offset:0,limit:20},data:x({},N)}):a.writeQuery({query:Ae,variables:{group:"student",offset:0,limit:20},data:y(x({},N),{conversations:y(x({},(Vo=N.conversations)!=null?Vo:[]),{docs:[(ko=i==null?void 0:i.data)==null?void 0:ko.conversationUpdated,..._]})})})}const k=a.cache.readQuery({query:Ae,variables:{group:"customer",offset:0,limit:20}}),C=(Io=(wo=k==null?void 0:k.conversations)==null?void 0:wo.docs)!=null?Io:[];if((k==null?void 0:k.conversations)&&h==="customer"){let _=[...C];const X=_==null?void 0:_.find(L=>{var de,pe;return(L==null?void 0:L.id)===((pe=(de=i==null?void 0:i.data)==null?void 0:de.conversationUpdated)==null?void 0:pe.id)});X&&((Po=i==null?void 0:i.data)==null?void 0:Po.conversationUpdated.typing)===null?(_=_.filter(L=>L.id!==X.id),a.writeQuery({query:Ae,variables:{group:"customer",offset:0,limit:20},data:y(x({},k),{conversations:y(x({},(_o=k.conversations)!=null?_o:[]),{docs:[(Fo=i==null?void 0:i.data)==null?void 0:Fo.conversationUpdated,..._]})})})):X?a.writeQuery({query:Ae,variables:{group:"customer",offset:0,limit:20},data:x({},k)}):a.writeQuery({query:Ae,variables:{group:"customer",offset:0,limit:20},data:y(x({},k),{conversations:y(x({},(Mo=k.conversations)!=null?Mo:[]),{docs:[(qo=i==null?void 0:i.data)==null?void 0:qo.conversationUpdated,..._]})})})}const F=a.cache.readQuery({query:Be,variables:{status:"open",offset:0,limit:20}}),Z=(Oo=(zo=F==null?void 0:F.supportConversations)==null?void 0:zo.docs)!=null?Oo:[];if((F==null?void 0:F.supportConversations)&&h==="support"){let _=[...Z];const X=_==null?void 0:_.find(L=>{var de,pe;return(L==null?void 0:L.id)===((pe=(de=i==null?void 0:i.data)==null?void 0:de.conversationUpdated)==null?void 0:pe.id)});!X&&A==="open"?a.writeQuery({query:Be,variables:{status:"open",offset:0,limit:20},data:y(x({},F),{supportConversations:y(x({},(Lo=F.supportConversations)!=null?Lo:[]),{docs:[(Bo=i==null?void 0:i.data)==null?void 0:Bo.conversationUpdated,..._]})})}):X&&((Ro=i==null?void 0:i.data)==null?void 0:Ro.conversationUpdated.typing)===null&&((Ho=(Go=(Uo=i==null?void 0:i.data)==null?void 0:Uo.conversationUpdated)==null?void 0:Go.lastMessage)==null?void 0:Ho.from)===e.id?(_=_.filter(L=>L.id!==X.id),a.writeQuery({query:Be,variables:{status:"open",offset:0,limit:20},data:y(x({},F),{supportConversations:y(x({},(Wo=F.supportConversations)!=null?Wo:[]),{docs:[..._]})})})):a.writeQuery({query:Be,variables:{status:"open",offset:0,limit:20},data:y(x({},F),{supportConversations:x({},(Qo=F.supportConversations)!=null?Qo:[])})})}const z=a.cache.readQuery({query:Be,variables:{status:"following",offset:0,limit:20}}),G=(Yo=(Zo=z==null?void 0:z.supportConversations)==null?void 0:Zo.docs)!=null?Yo:[];if((z==null?void 0:z.supportConversations)&&h==="support"){let _=[...G];const X=_==null?void 0:_.find(L=>{var de,pe;return(L==null?void 0:L.id)===((pe=(de=i==null?void 0:i.data)==null?void 0:de.conversationUpdated)==null?void 0:pe.id)});!X&&A==="following"?a.writeQuery({query:Be,variables:{status:"following",offset:0,limit:20},data:y(x({},z),{supportConversations:y(x({},(Ko=z.supportConversations)!=null?Ko:[]),{docs:[(Jo=i==null?void 0:i.data)==null?void 0:Jo.conversationUpdated,..._]})})}):X&&((Xo=i==null?void 0:i.data)==null?void 0:Xo.conversationUpdated.typing)===null?(_=_.filter(L=>L.id!==X.id),a.writeQuery({query:Be,variables:{status:"following",offset:0,limit:20},data:y(x({},z),{supportConversations:y(x({},(ei=z.supportConversations)!=null?ei:[]),{docs:A==="following"?[(ti=i==null?void 0:i.data)==null?void 0:ti.conversationUpdated,..._]:[..._]})})})):a.writeQuery({query:Be,variables:{status:"following",offset:0,limit:20},data:y(x({},z),{supportConversations:x({},(si=z.supportConversations)!=null?si:[])})})}const P=a.cache.readQuery({query:Be,variables:{status:"done",offset:0,limit:20}}),H=(ii=(oi=P==null?void 0:P.supportConversations)==null?void 0:oi.docs)!=null?ii:[];if((P==null?void 0:P.supportConversations)&&h==="support"){let _=[...H];const X=_==null?void 0:_.find(L=>{var de,pe;return(L==null?void 0:L.id)===((pe=(de=i==null?void 0:i.data)==null?void 0:de.conversationUpdated)==null?void 0:pe.id)});X&&((li=i==null?void 0:i.data)==null?void 0:li.conversationUpdated.typing)===null?(_=_.filter(L=>L.id!==X.id),a.writeQuery({query:Be,variables:{status:"done",offset:0,limit:20},data:y(x({},H),{supportConversations:y(x({},(ai=H.supportConversations)!=null?ai:[]),{docs:[..._]})})})):!X&&A==="done"?a.writeQuery({query:Be,variables:{status:"done",offset:0,limit:20},data:y(x({},P),{supportConversations:y(x({},(ri=P.supportConversations)!=null?ri:[]),{docs:[(ni=i==null?void 0:i.data)==null?void 0:ni.conversationUpdated,..._]})})}):a.writeQuery({query:Be,variables:{status:"done",offset:0,limit:20},data:y(x({},H),{supportConversations:x({},(ui=H.supportConversations)!=null?ui:[])})})}},shouldResubscribe:!!e})};function ic(e,s){const o=e.split(" ");return o.map((l,a)=>{if(l.length>1&&l.includes(":")){if(!s&&a===o.length-1)return l;const i=Va.search(l);return i.length>0?i[0].native:l}return l}).join(" ")}const go=({setContent:e,userId:s,group:o,setConversationId:l})=>{const[a]=ne(),i=a==null?void 0:a.id,[n,{client:r,loading:u}]=Pe(bu),g=c.exports.useCallback(async(d,h,A,E)=>{var D;const T=A==null?void 0:A.map(({type:N,fileId:$})=>({type:N,attachmentId:$})),{data:p}=await n({variables:{recipient:be({recipientType:E?"conversation":"user",conversationId:E||null,userId:E?null:s==null?void 0:s.toString(),group:E||o==="my chats"?"":o},N=>N),message:be({text:d.trim(),attachments:at.exports.isEmpty(T)?null:T},N=>N)},update:(N,$)=>{var C;const k=N.readQuery({query:Ze,variables:y(x({},be({conversationId:E,userId:E?"":s==null?void 0:s.toString(),limit:30},F=>F)),{offset:0})});if(k==null?void 0:k.messages){const F=((C=k==null?void 0:k.messages)==null?void 0:C.docs)||[],Z=F==null?void 0:F.map(z=>z.id===h?y(x(x({},z),$==null?void 0:$.data.sendMessage.lastMessage),{loading:!1}):z);N.writeQuery({query:Ze,variables:y(x({},be({conversationId:E,userId:E?"":s==null?void 0:s.toString(),limit:30},z=>z)),{offset:0}),data:y(x({},k),{messages:y(x({},k==null?void 0:k.messages),{docs:Z})})})}}});E||await l((D=p==null?void 0:p.sendMessage)==null?void 0:D.id)},[n,s,o,i,a,l]),b=c.exports.useCallback(async(d,h)=>{if(h)try{await n({variables:{recipient:{conversationId:h,recipientType:"conversation"},senderAction:d}})}catch(A){console.log(A)}},[n]),m=c.exports.useCallback(async d=>{try{await n({variables:{recipient:be({recipientType:"conversation",conversationId:d||"",userId:d?"":s==null?void 0:s.toString()},h=>h),senderAction:"markSeen"}})}catch(h){console.log(h)}},[n,s]);return{onSendMessage:c.exports.useCallback(async({nativeEvent:d,conversationId:h,file:A})=>{var $,k;const E=y(x({},be({conversationId:h,userId:h?"":s==null?void 0:s.toString(),limit:30},C=>C)),{offset:0}),T=ic(d.text,!0),p=Pi(),D=r.cache.readQuery({query:Ze,variables:x({},E)}),N=(k=($=D==null?void 0:D.messages)==null?void 0:$.docs)!=null?k:[];try{e(""),(D==null?void 0:D.messages)&&r.cache.writeQuery({query:Ze,variables:x({},E),data:y(x({},D),{messages:y(x({},D==null?void 0:D.messages),{docs:[{id:p,text:T,from:i,to:null,type:"update",attachments:[],seenBy:[],createdAt:Ke().format(),updatedAt:Ke().format(),deletedAt:null,callPayload:null,loading:!0,error:!1,conversation:{id:h,participants:[{userId:i,avatar:a.avatar,fullName:a.fullName}]},emojis:{userId:a==null?void 0:a.id,emoji:""}},...N]})})}),await g(T,p,A,h);const C=document.getElementById("chat-box");C&&(C.scrollTop=C==null?void 0:C.scrollHeight)}catch{r.cache.writeQuery({query:Ze,variables:x({},E),data:y(x({},D),{messages:y(x({},D==null?void 0:D.messages),{docs:[{id:p,text:T,from:i,to:null,type:"update",attachments:[],seenBy:[],createdAt:Ke().format(),updatedAt:Ke().format(),deletedAt:null,callPayload:null,loading:!1,error:!0,conversation:{id:h,participants:[{userId:i,avatar:a.avatar,fullName:a.fullName}]},emojis:{userId:a==null?void 0:a.id,emoji:""}},...N]})})})}},[e,g,s,r]),onTyping:b,onMarkSeen:m,loading:u}};var Tl="/Users/zimdev/work/zim-app/src/pages/chat/boxMessage/components/spaceTime.tsx";const lc=({time:e})=>t.exports.jsxDEV(S,{py:6,display:"flex",alignItems:"center",justifyContent:"center",w:"full",children:t.exports.jsxDEV(M,{fontSize:14,textAlign:"center",color:j("gray.400","slate.300"),children:gs(e).format("HH:mm - DD-MM-YYYY")},void 0,!1,{fileName:Tl,lineNumber:16,columnNumber:7},globalThis)},void 0,!1,{fileName:Tl,lineNumber:9,columnNumber:5},globalThis),yl=e=>{if(typeof e!="string"||!e)return!1;let s;try{s=new URL(e)}catch{return!1}return s.protocol==="http:"||s.protocol==="https:"};var as="/Users/zimdev/work/zim-app/src/pages/chat/components/previewLink.tsx";const ac="https://getopengraph.herokuapp.com/",rc=new RegExp("^(?!mailto:)(?:(?:http|https|ftp)://)(?:\\S+(?::\\S*)?@)?(?:(?:(?:[1-9]\\d?|1\\d\\d|2[01]\\d|22[0-3])(?:\\.(?:1?\\d{1,2}|2[0-4]\\d|25[0-5])){2}(?:\\.(?:[0-9]\\d?|1\\d\\d|2[0-4]\\d|25[0-4]))|(?:(?:[a-z\\u00a1-\\uffff0-9]+-?)*[a-z\\u00a1-\\uffff0-9]+)(?:\\.(?:[a-z\\u00a1-\\uffff0-9]+-?)*[a-z\\u00a1-\\uffff0-9]+)*(?:\\.(?:[a-z\\u00a1-\\uffff]{2,})))|localhost)(?::\\d{2,5})?(?:(/|\\?|#)[^\\s]*)?$"),nc=({url:e,isMyMessage:s})=>{const[o,l]=c.exports.useState(null),[a,i]=c.exports.useState(!1);c.exports.useEffect(()=>{e&&e.match(rc)!==null&&n(e)},[e]);const n=async u=>{i(!0);let g=await pi.get(`${ac}?url=${u}`,{method:"GET"});l(g.data),i(!1)},r=c.exports.useMemo(()=>{var u;return t.exports.jsxDEV(q,{flexDirection:"column",rounded:"8px",children:[t.exports.jsxDEV(Pt,{_hover:{color:s?"white":void 0},fontSize:14,target:"_blank",href:e,cursor:"pointer",color:"black",textDecoration:"underline",onClick:()=>window.open(e),rel:"noreferrer",children:(o==null?void 0:o.url)||e},void 0,!1,{fileName:as,lineNumber:34,columnNumber:9},globalThis),((u=o==null?void 0:o.img)==null?void 0:u.length)>0&&t.exports.jsxDEV(rt,{w:"100%",h:"150px",mt:4,alt:"img",src:o==null?void 0:o.img,rounded:"8px"},void 0,!1,{fileName:as,lineNumber:48,columnNumber:11},globalThis),t.exports.jsxDEV(Pt,{_hover:{color:s?"white":void 0},fontSize:14,target:"_blank",href:e,cursor:"pointer",color:"black",textDecoration:"underline",onClick:()=>window.open(e),rel:"noreferrer",children:(o==null?void 0:o.title)||"kh\xF4ng t\xECm th\u1EA5y"},void 0,!1,{fileName:as,lineNumber:57,columnNumber:9},globalThis)]},void 0,!0,{fileName:as,lineNumber:33,columnNumber:7},globalThis)},[o,e,s]);return a?t.exports.jsxDEV(Pt,{_hover:{color:s?"white":void 0},fontSize:14,target:"_blank",href:e,cursor:"pointer",color:"black",textDecoration:"underline",onClick:()=>window.open(e),rel:"noreferrer",children:e},void 0,!1,{fileName:as,lineNumber:76,columnNumber:5},globalThis):(o==null?void 0:o.status)===200&&o&&r},ho=e=>{if(!e)return;const s=(e==null?void 0:e.search("-"))+1;return e==null?void 0:e.slice(s)};var pt="/Users/zimdev/work/zim-app/src/pages/chat/boxMessage/components/imageMessage.tsx";const uc=({files:e,onPress:s,isMyMessage:o})=>(console.log(e,"?????"),t.exports.jsxDEV(xt,{maxW:{base:"250px"},columns:(e==null?void 0:e.length)>3?3:e==null?void 0:e.length,spacing:"8px",children:e==null?void 0:e.map((l,a)=>{var i,n,r,u;return t.exports.jsxDEV(S,{rounded:"lg",borderColor:j("gray.100","gray.300"),cursor:"pointer",overflow:"hidden",bg:o?"blue.50":"gray.100",display:"flex",alignItems:"center",justifyContent:"center",children:((i=l==null?void 0:l.attachment)==null?void 0:i.type)!=="file"?t.exports.jsxDEV(rt,{objectFit:"cover",w:"80px",h:"80px",onClick:()=>s(a),src:(n=l==null?void 0:l.attachment)==null?void 0:n.fullUrl},a,!1,{fileName:pt,lineNumber:38,columnNumber:15},globalThis):t.exports.jsxDEV(Pt,{href:(r=l==null?void 0:l.attachment)==null?void 0:r.fullUrl,target:"_blank",children:t.exports.jsxDEV(q,{m:2,children:[t.exports.jsxDEV(S,{w:6,h:6,rounded:"full",display:"flex",alignItems:"center",justifyContent:"center",borderWidth:1,children:t.exports.jsxDEV(_i,{className:"w-6 h-6 text-gray-700"},void 0,!1,{fileName:pt,lineNumber:58,columnNumber:21},globalThis)},void 0,!1,{fileName:pt,lineNumber:49,columnNumber:19},globalThis),t.exports.jsxDEV(M,{flex:1,color:"gray.800",fontSize:14,children:ho((u=l==null?void 0:l.attachment)==null?void 0:u.path)},void 0,!1,{fileName:pt,lineNumber:60,columnNumber:19},globalThis)]},void 0,!0,{fileName:pt,lineNumber:48,columnNumber:17},globalThis)},void 0,!1,{fileName:pt,lineNumber:47,columnNumber:15},globalThis)},a,!1,{fileName:pt,lineNumber:26,columnNumber:11},globalThis)})},void 0,!1,{fileName:pt,lineNumber:17,columnNumber:5},globalThis));var bt="/Users/zimdev/work/zim-app/src/pages/chat/boxMessage/components/textMessage.tsx";const cc=({text:e,isMyMessage:s,deletedAt:o,createdAt:l,attachments:a,setAttachmentIndex:i,setShowImageViewer:n})=>{const r=V(m=>m.searchStringMessage),u=j("gray.100","slate.300"),g=j("gray.900","slate.900"),b=j("text-gray-100","text-slate-300");return c.exports.useMemo(()=>t.exports.jsxDEV(S,{pointerEvents:o?"none":"auto",py:{sm:1.5,base:3},px:3,rounded:"lg",maxW:{sm:"2/3",base:"210px",lg:"250px",xl:"280px","2xl":"400px"},bg:s?"blue.50":u,pos:"relative",children:[t.exports.jsxDEV(ka,{className:`${s?"text-blue-50":b} absolute ${s?"top-4 -right-2.5":"bottom-1 -left-2.5"} transform ${s?void 0:"rotate-180"}`},void 0,!1,{fileName:bt,lineNumber:42,columnNumber:9},globalThis),o?t.exports.jsxDEV(M,{letterSpacing:.1,fontWeight:"500",fontSize:{base:"13px"},color:g,children:"Tin nh\u1EAFn \u0111\xE3 b\u1ECB x\xF3a"},void 0,!1,{fileName:bt,lineNumber:50,columnNumber:11},globalThis):yl(e)?t.exports.jsxDEV(nc,{url:e,isMyMessage:s},void 0,!1,{fileName:bt,lineNumber:61,columnNumber:11},globalThis):t.exports.jsxDEV(S,{children:[!!(a==null?void 0:a.length)&&t.exports.jsxDEV(uc,{isMyMessage:s,files:a,onPress:m=>{i(m),n(!0)}},void 0,!1,{fileName:bt,lineNumber:65,columnNumber:15},globalThis),!!e&&t.exports.jsxDEV(M,{pt:(a==null?void 0:a.length)?2:0,letterSpacing:.1,fontSize:{base:"15px"},textAlign:"left",color:g,children:t.exports.jsxDEV(Ii,{searchWords:[r],autoEscape:!0,textToHighlight:e||"Tin nh\u1EAFn \u0111\xE3 b\u1ECB x\xF3a"},void 0,!1,{fileName:bt,lineNumber:84,columnNumber:17},globalThis)},void 0,!1,{fileName:bt,lineNumber:75,columnNumber:15},globalThis)]},void 0,!0,{fileName:bt,lineNumber:63,columnNumber:11},globalThis)]},void 0,!0,{fileName:bt,lineNumber:24,columnNumber:7},globalThis),[yl,s,e,l,o,r,g,a])},mc=()=>{const[e,{loading:s}]=Pe(gu),o=mt();return{onRemoveMessage:c.exports.useCallback(async a=>{try{await e({variables:{id:a}}),await o(["messages"])}catch(i){console.log(i)}},[e,o]),loading:s}};var gt="/Users/zimdev/work/zim-app/src/pages/chat/boxMessage/components/messageDeleted.tsx";const dc=({isMyMessage:e,isHoverMessage:s,setIsHoverMessage:o,id:l})=>{const{onRemoveMessage:a}=mc(),i=async()=>{await a(l),o(!1)};return e&&s&&t.exports.jsxDEV(Zs,{isLazy:!0,boundary:document.getElementById("chat-box"),children:[t.exports.jsxDEV(Ys,{display:"flex",alignItems:"center",justifyContent:"center",rounded:"full",cursor:"pointer",w:4,h:4,borderWidth:1,borderColor:j("blackAlpha.800","white"),children:t.exports.jsxDEV(S,{display:"flex",alignItems:"center",justifyContent:"center",children:t.exports.jsxDEV(Fi,{},void 0,!1,{fileName:gt,lineNumber:43,columnNumber:13},globalThis)},void 0,!1,{fileName:gt,lineNumber:42,columnNumber:11},globalThis)},void 0,!1,{fileName:gt,lineNumber:31,columnNumber:9},globalThis),t.exports.jsxDEV(hs,{children:t.exports.jsxDEV(Ks,{alignItems:"center",py:4,children:t.exports.jsxDEV(Mi,{onClick:i,icon:t.exports.jsxDEV(Wt,{as:wa,w:4,h:4},void 0,!1,{fileName:gt,lineNumber:50,columnNumber:21},globalThis),children:"X\xF3a tin nh\u1EAFn n\xE0y"},void 0,!1,{fileName:gt,lineNumber:48,columnNumber:13},globalThis)},void 0,!1,{fileName:gt,lineNumber:47,columnNumber:11},globalThis)},void 0,!1,{fileName:gt,lineNumber:46,columnNumber:9},globalThis)]},void 0,!0,{fileName:gt,lineNumber:30,columnNumber:7},globalThis)};var $s="/Users/zimdev/work/zim-app/src/pages/chat/boxMessage/components/messageStatus.tsx";const pc=({lastSeenMessageId:e,id:s,userData:o,isMyMessage:l,notSeen:a,loading:i,error:n,isGroup:r})=>{const u=j("white","slate.300");return c.exports.useMemo(()=>e===s&&!r?t.exports.jsxDEV(S,{w:4,h:4,rounded:"full",alignItems:"center",justifyContent:"center",display:"flex",flexDirection:"column",overflow:"hidden",bg:u,children:t.exports.jsxDEV(He,{src:o==null?void 0:o.avatar,name:o==null?void 0:o.fullName,size:"xs"},void 0,!1,{fileName:$s,lineNumber:31,columnNumber:11},globalThis)},void 0,!1,{fileName:$s,lineNumber:20,columnNumber:9},globalThis):l&&a||i||e===s&&r?t.exports.jsxDEV(S,{w:4,h:4,rounded:"lg",alignItems:"center",justifyContent:"center",display:"flex",flexDirection:"column",bg:i||n?"transparent":"gray.400",border:i||n?"1px solid #838b8b":void 0,children:t.exports.jsxDEV(Ia,{fill:"#fff",className:"w-3 h-3"},void 0,!1,{fileName:$s,lineNumber:56,columnNumber:11},globalThis)},void 0,!1,{fileName:$s,lineNumber:45,columnNumber:9},globalThis):null,[e,s,n,i,a,l,r,o])};var Dl="/Users/zimdev/work/zim-app/src/pages/chat/boxMessage/components/imageViewer.tsx";const jl=({attachments:e,attachmentIndex:s,setAttachmentIndex:o,showImageViewer:l,setShowImageViewer:a})=>t.exports.jsxDEV(S,{w:"100%",h:"100%",position:"relative",children:t.exports.jsxDEV(Pa,{imgs:e.map(i=>{var n;return{src:(n=i==null?void 0:i.attachment)==null?void 0:n.fullUrl}}),currImg:s,isOpen:l,onClose:()=>a(!1),showImgCount:!1,backdropCloseable:!0,onClickPrev:()=>{s>1&&o(s-1)},onClickNext:()=>{s<(e==null?void 0:e.length)-1&&o(s+1)}},void 0,!1,{fileName:Dl,lineNumber:13,columnNumber:7},globalThis)},void 0,!1,{fileName:Dl,lineNumber:12,columnNumber:5},globalThis);var ht="/Users/zimdev/work/zim-app/src/pages/chat/boxMessage/components/markAsQuestion.tsx";const bc=({isParticipant:e,onOpen:s})=>e&&t.exports.jsxDEV(Zs,{isLazy:!0,boundary:document.getElementById("chat-box"),children:[t.exports.jsxDEV(Ys,{display:"flex",alignItems:"center",justifyContent:"center",rounded:"full",cursor:"pointer",borderWidth:1,borderColor:j("blackAlpha.800","white"),w:4,h:4,mt:"-20px",children:t.exports.jsxDEV(S,{display:"flex",alignItems:"center",justifyContent:"center",children:t.exports.jsxDEV(Fi,{},void 0,!1,{fileName:ht,lineNumber:32,columnNumber:13},globalThis)},void 0,!1,{fileName:ht,lineNumber:31,columnNumber:11},globalThis)},void 0,!1,{fileName:ht,lineNumber:19,columnNumber:9},globalThis),t.exports.jsxDEV(hs,{children:t.exports.jsxDEV(Ks,{alignItems:"center",py:4,children:t.exports.jsxDEV(Mi,{onClick:()=>{s()},icon:t.exports.jsxDEV(Wt,{as:_a,w:4,h:4},void 0,!1,{fileName:ht,lineNumber:41,columnNumber:21},globalThis),children:"\u0110\xE1nh d\u1EA5u c\xE2u h\u1ECFi"},void 0,!1,{fileName:ht,lineNumber:37,columnNumber:13},globalThis)},void 0,!1,{fileName:ht,lineNumber:36,columnNumber:11},globalThis)},void 0,!1,{fileName:ht,lineNumber:35,columnNumber:9},globalThis)]},void 0,!0,{fileName:ht,lineNumber:18,columnNumber:7},globalThis);var gc="data:audio/mpeg;base64,SUQzBAAAAAACbFRFTkMAAAANAAADTG9naWMgUHJvIFgAVERSQwAAAAwAAAMyMDE0LTExLTA5AFRYWFgAAAARAAADY29kaW5nX2hpc3RvcnkAAFRYWFgAAAAaAAADdGltZV9yZWZlcmVuY2UAMTU4NzYwMDAwAFRYWFgAAAEJAAADdW1pZAAweDAwMDAwMDAwMTU2NDhEODQyMUZBMDBGQzcwQjY0MDU4RkY3RjAwMDAwMEZBODMwMDAwNjAwMDAwODA0RjEyMDAwMDAwMDAwMDM2QjI1NzA4MDEwMDAwMDA3MDcwRDI3OEZGN0YwMDAwQjBBRTQwNThGRjdGMDAwMEVBRTZEQzhFAFRTU0UAAAAPAAADTGF2ZjU4Ljc2LjEwMABDVE9DAAAACgAAdG9jAAMBY2gwAENIQVAAAAAsAABjaDAAAAAAAAAAAH3//////////1RJVDIAAAAOAAADVGVtcG86IDEyMC4wAAAAAAAAAAAAAAD/+5AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABJbmZvAAAADwAAAAYAAAtsAElJSUlJSUlJSUlJSUlJSUltbW1tbW1tbW1tbW1tbW1tbZKSkpKSkpKSkpKSkpKSkpK2tra2tra2tra2tra2tra2ttvb29vb29vb29vb29vb29vb/////////////////////wAAAABMYXZjNTguMTMAAAAAAAAAAAAAAAAkAzgAAAAAAAALbGkpEGgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD/+5BkAA/wAABpAAAACAAADSAAAAEAAAGkAAAAIAAANIAAAAQCAQFAIBgQMEAAAD/BAACCQB4xqZo+NkGUSf8wEYAgMBqAFDu03xU4g5WM/zAPQD8vkYrwEFmIthKuDAAhjAD9baEDbOW3iUxkyYAxiDgA5U0HA5jr79i4Yk+BiZFIBjpJOBgxO8BrYNx8ky5wMsouwMAYWgMOALAMT4GwMKwTv0G3hk4GBYIAGGAKQAwTA3AGAIAX/+KIAUBAqpgFAMIRL//9AskTWZm9Bv//////////5o1BaczN3V/////////sTZPuAgIAwGBAGGAAAAB/1Y8YLg3xjUCp/7iM0NXU5cz8CdP9/8jC+A5MJMEPPoAcVYoGbQ/03gYEBAGn1aBqMu/4ZGAJAINQgBigHf+AMIxahOAceJQ//WQwWQVFIf/7GmXGoFAoFAoFAoEAZKKAAAMIKGKjez/1FfxgTQqGYAGAHGALCRpsh0VoYNGG+mIkABBCATmJ5ADxCAgmH3AahiGRZsJAOBgAIHEJALhgCIDgcOyDxiTgHGHwAQ3/+5JkvoAAAABpBQAACAAADSCgAAEYfhEPmfsAAYCYorM9UAA0FEcwgwow4DCuVbNhMXqLiIEkZAI6DAITBqAAMMMCQwFwFjAIIoMg0feI0j9DwOTeeVQBRGAMYGQExgWgJmB2D2ZF4EZfSAK7OFbpbRyZYIaARvLNFASDARBJZKz8lAC76rs94aqrU5vxICyAaq52E/Ua/Yzqxr/bJP45Rn9VpN388P/+W//9//43h4z////////+z/+3AcDN9rI5Ta25/gAAAAwHAGCMflQT/gn5KZWweGGBTgusm+TeYHcAQGCABaHJJysBueN0AoN4kJagYFQSAYFimgZfQmV9gGAdCCwIA9AYcw299aguADbwDQFgND3IYqgl1HpVDWBbs1MyU3+hyQaF5/UAAC8GIwCEKjAUBLMJAL0wzgszEkLhO/oIgyAg+TDWCxMNUEYwOQkDAMA8QjSgfhfRUAAMD4J0DiM1A2ARQMEAMUIFzwpZEumJYDLgX6AwaXQNfCwGC4QVI4dykEiyKRDGoGAhOCygCw8R+NM6RNRFDYyI8G6Q//uSZOuAB347zu5/wARxZPttz9mAl12jLn3qgAC3CyfPttACKBYAoQBqgtGSS1IJGQyw0wuSLnK5gzv1FkWSOA4zJbrmQ5xiXzek/SMiaOvdm+kqm6Leup/1///////6kv9SkDb+vY1SAICgA12UspYiFBMbRAwJXSzZ9pjvP/TogOcL6jf9Q/hwHv9ZiWB/w6c+76P/sQAIAAAwFKU3CUBMwBAGAYE+YVgvBicpImFsEYYfgsY3DQdT1WbnmeZig0GEyGBOmYBgHKBIMYD1DJQDArq0UzZp4y4S5hRCa4kZOqSYzlqx4Unw1x05PjynnASMM+mAga9dpFKk9y+ihpaNAAkg1hY6iDX4fzz/Hf1Iaf5YZscMz0MRiUUljdqtGq9jPe/1reH9wrc1nb0xgKg24Waz///////9BAACAoAANAAsoR6UQAbayyXP4Z54kG31F1kzwbIRnsv500H8iku7f0i8cioqDjkfV8tBE7LoFXsGAKYfkcZZiwYTCeFAMMIQgMyH7NlsqMU0BMRCgMRgHSSdKhUFMCkNWWU3fiB52v/7kmSFioUjM0tL3dHAMcLZ7Q8tGBHg2Syu6S3QvgompBysYDxwn4FSOSSBAsAGjutAeROE8NUgMsCcl55RvsqpFMmncuYyhh0bcKHyzqAguOgwxRd665BK5+pdbkkpUsiaIR0VCIhNkCC/63/c9bLKWt0otwAjrv//////9ECAFAHdlaSazo1I0CGzQ6FQVVIFuNpKHU+r//hyJQDoaQHg6CZvw7nmAoFgG/+gcgIs63QPWQgOqgQ0vMcUGCIC+YPY7Bmlo4GWSbGYzYfZhXgemAAAQoG09fip4hhLpc/0klkttx6XwQ7agBn2RiABoTQxBPERD0xvnRoBhihgOENIg2Buy6tlczorfzUFuaj0XiROBgEOAl5ky09Gsqbv3Xhh/L3cOf3LeVWzKo1Go1PSqjltNet4WEGBOpCCwijf/////9B48AABwIYB9AIKYCcERkDZzDTtQMXXvUzun/e7/j9g3HCcRRKNQeCYaHI/QDQMgsLF/6gMYIF76tal+StyaykKXSMFAPMNyQMRzpNzGPOEU8NPDEMagoJUjYSDIGL/+5JkegLUxTFJCx7SIDJC6XkLS0QREMcgtdyAAMOMpIqycACWaGfpKszQV7FuWxrHe9xhXQMLDgDbqOtYYxCSzkUAKJpjlr2Jy+3bt51abHLHWd+l1nehqPsNWFZ1G12vQ+7oSl343Y3+H/zvOfv8t45VgVBUiEhdX///////8aKgI9Bn////6xxtRlI4RjnI6d6BoALUEwOBY1X1Tsd84UkABgDgNDYOBHEsmf/dgqeCQsXqDMZAAAiUKAuZAAAvVC2ixZp3oZLJGBwCYUAM25RgITmyKmDguYvei5R0AAFSUXKDioAEAwwMG/FgfJAwGgWGLw4MNlOitSJjHk4OoNsEfCCBNksTJMC5i8DQIE+buNgPSC94uMhxkbENMSdTTdM4kFlAhEI0EFw+oavI5iaJ4pG5RIsRBuXhS45AsAyhFRZZFTE1JpReWy/5dJ8uFsijE4WSfrRRYySSMf/mZsTheJxMqF83L5yjWpJTmK0f/+Thw8XzUvqK5gaFxEvmJ9VJ1LRWySkl1v////zQ2Ln/8aWAAEAAA/8Mfy/Knnfy//uSZHaABwJ2SeZyoABAw2kVzMgAi0Q43lyTAAiPhJnLhhAAZCa28XC8FWaBdURVEXKRtzc6fRo1so0SQ9KPz7GRXTYtHCADAfuwsML/6UnBr/+uhH//6YAGukSJEilcVURM8qCIImSIEgk4GARKnIkaimjQXEFNhDAoKbBWwU3I7//+BT8TYoL4VwK////i9BTcjoQV0F6C////hXAo6KbFBfCvO//4FeC4goqEMhBXgoiwBszMfqzGoCAkGAgkDQdKnUdYKuLHiwdiVYK/+s6JQ1BqHUxBTUUzLjEwMKqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqg==";const hc=e=>{const s=new Audio(gc);s.volume=1;const[o]=Pe(Du);return{onReactMessage:c.exports.useCallback(async(a,i)=>{try{await o({variables:{messageId:i,emoji:a}}),await e(!1),await s.play()}catch(n){console.log(n)}},[])}};var tt="/Users/zimdev/work/zim-app/src/pages/chat/boxMessage/components/messageReact.tsx";const fc=["http://chat-media.zim.vn/628dd69bb37316154a08c0ee/like-removebg-preview.png","http://chat-media.zim.vn/628dd6c1b37316154a08c0f6/heart-removebg-preview.png","http://chat-media.zim.vn/628dd718b37316154a08c113/emoji__1_-removebg-preview.png","http://chat-media.zim.vn/628dd733b37316154a08c11c/emoji-removebg-preview.png","http://chat-media.zim.vn/628dd751b37316154a08c128/sad-removebg-preview.png","http://chat-media.zim.vn/628dd76bb37316154a08c12d/happy__1_-removebg-preview.png","http://chat-media.zim.vn/628dd6e4b37316154a08c107/angry-removebg-preview.png"],xc=({setIsHoverMessage:e,id:s})=>{const{onReactMessage:o}=hc(e);return t.exports.jsxDEV(Zs,{isLazy:!0,boundary:document.getElementById("chat-box"),children:[t.exports.jsxDEV(Ys,{display:"flex",alignItems:"center",justifyContent:"center",rounded:"full",cursor:"pointer",w:4,h:4,children:t.exports.jsxDEV(S,{display:"flex",alignItems:"center",justifyContent:"center",children:t.exports.jsxDEV(qi,{className:"w-full h-full"},void 0,!1,{fileName:tt,lineNumber:38,columnNumber:11},globalThis)},void 0,!1,{fileName:tt,lineNumber:37,columnNumber:9},globalThis)},void 0,!1,{fileName:tt,lineNumber:28,columnNumber:7},globalThis),t.exports.jsxDEV(hs,{children:t.exports.jsxDEV(Ks,{display:"flex",alignItems:"center",justifyContent:"center",bg:"white",rounded:"md",shadow:"sm",zIndex:1e3,children:t.exports.jsxDEV(q,{alignItems:"center",children:fc.map((l,a)=>t.exports.jsxDEV(S,{w:5,h:5,rounded:"full",bg:"white",display:"flex",alignItems:"center",justifyContent:"center",cursor:"pointer",onClick:()=>o(l,s),children:t.exports.jsxDEV(rt,{_hover:{transition:"all 0.3s ease-in-out",transform:"scale(1.3)"},flex:1,src:l},void 0,!1,{fileName:tt,lineNumber:66,columnNumber:19},globalThis)},a,!1,{fileName:tt,lineNumber:54,columnNumber:17},globalThis))},void 0,!1,{fileName:tt,lineNumber:51,columnNumber:11},globalThis)},void 0,!1,{fileName:tt,lineNumber:42,columnNumber:9},globalThis)},void 0,!1,{fileName:tt,lineNumber:41,columnNumber:7},globalThis)]},void 0,!0,{fileName:tt,lineNumber:27,columnNumber:5},globalThis)};var We="/Users/zimdev/work/zim-app/src/pages/chat/boxMessage/components/renderReactionMessage.tsx";const Nc=c.exports.memo(({emojis:e})=>{var l,a;const[s,o]=c.exports.useState([]);return c.exports.useEffect(()=>{(async function(){try{const i=await vl(e==null?void 0:e.map(n=>n.userId),!1);o(i)}catch(i){console.log(i)}})()},[e]),t.exports.jsxDEV(q,{alignItems:"center",spacing:2,children:(a=(l=ps(e,"emoji"))==null?void 0:l.slice(0,3))==null?void 0:a.map(i=>t.exports.jsxDEV(Js,{openDelay:300,bg:"green.400",p:2,hasArrow:!0,label:t.exports.jsxDEV(ie,{maxW:"150px",children:t.exports.jsxDEV(q,{children:[t.exports.jsxDEV(ve,{children:e==null?void 0:e.map((n,r)=>t.exports.jsxDEV(S,{display:"flex",alignItems:"center",justifyContent:"center",w:4,h:4,cursor:"pointer",fontSize:"16px",children:t.exports.jsxDEV(rt,{flex:1,src:n==null?void 0:n.emoji},void 0,!1,{fileName:We,lineNumber:59,columnNumber:31},globalThis)},r,!1,{fileName:We,lineNumber:49,columnNumber:29},globalThis))},void 0,!1,{fileName:We,lineNumber:46,columnNumber:23},globalThis),t.exports.jsxDEV(ve,{children:s==null?void 0:s.map((n,r)=>t.exports.jsxDEV(M,{color:"white",fontWeight:600,children:n==null?void 0:n.full_name},r,!1,{fileName:We,lineNumber:67,columnNumber:29},globalThis))},void 0,!1,{fileName:We,lineNumber:64,columnNumber:23},globalThis)]},void 0,!0,{fileName:We,lineNumber:45,columnNumber:21},globalThis)},void 0,!1,{fileName:We,lineNumber:44,columnNumber:19},globalThis),children:t.exports.jsxDEV(S,{display:"flex",alignItems:"center",justifyContent:"center",w:4,h:4,cursor:"pointer",fontSize:"16px",children:t.exports.jsxDEV(rt,{flex:1,src:i==null?void 0:i.emoji},void 0,!1,{fileName:We,lineNumber:87,columnNumber:19},globalThis)},void 0,!1,{fileName:We,lineNumber:78,columnNumber:17},globalThis)},i==null?void 0:i.emoji,!1,{fileName:We,lineNumber:38,columnNumber:15},globalThis))},void 0,!1,{fileName:We,lineNumber:33,columnNumber:7},globalThis)}),Ec=Fa.extend({addAttributes(){var e,s,o,l,a;return y(x({},(e=this.parent)==null?void 0:e.call(this)),{backgroundColor:{default:(o=(s=this.options)==null?void 0:s.backgroundColor)!=null?o:null,parseHTML:i=>i.getAttribute("data-background-color"),renderHTML:i=>({"data-background-color":i.backgroundColor,style:`background-color: ${i.backgroundColor}`})},style:{default:(a=(l=this.options)==null?void 0:l.style)!=null?a:null,parseHTML:i=>i.getAttribute("colwidth"),renderHTML:i=>({style:`${i.style?`width: ${i.colwidth}px`:null}`,colspan:i.colspan,rowspan:i.rowspan,colwidth:i.colwidth})}})}}),vc=Ma.extend({renderHTML({HTMLAttributes:e}){return["div",{class:"table-tiptap"},["table",e,["tbody",0]]]}}),Ac=qa.extend({content:"paragraph*",addAttributes(){var e;return y(x({},(e=this.parent)==null?void 0:e.call(this)),{cite:{default:this.options.cite},class:{default:this.options.class},title:{default:this.options.title}})}}),Tc=za.extend({content:"paragraph*",addAttributes(){var e,s,o,l,a,i,n,r;return y(x({},(e=this.parent)==null?void 0:e.call(this)),{rel:{default:(o=(s=this==null?void 0:this.options)==null?void 0:s.rel)!=null?o:"noopener nofollow noreferrer"},target:{default:(a=(l=this==null?void 0:this.options)==null?void 0:l.target)!=null?a:"_blank"},"data-contextual":{default:((i=this==null?void 0:this.options)==null?void 0:i["data-contextual"])||void 0},"data-contextual-tag":{default:((n=this==null?void 0:this.options)==null?void 0:n["data-contextual-tag"])||void 0},"data-contextual-tag-id":{default:((r=this==null?void 0:this.options)==null?void 0:r["data-contextual-tag-id"])||void 0}})}}),yc=Oa.extend({addAttributes(){var e,s,o,l,a;return y(x({},(e=this.parent)==null?void 0:e.call(this)),{alt:{default:(o=(s=this==null?void 0:this.options)==null?void 0:s.alt)!=null?o:"image-alt"},title:{default:(a=(l=this==null?void 0:this.options)==null?void 0:l.title)!=null?a:"image-title"}})}});var Ss="/Users/zimdev/work/zim-app/src/components/TipTap/menu/buttons/ButtonIcon.tsx";const Dc=c.exports.forwardRef((r,n)=>{var u=r,{activeKey:e="",activeOptions:s={},isActive:o=!1,icon:l,label:a=""}=u,i=Ue(u,["activeKey","activeOptions","isActive","icon","label"]);return l?t.exports.jsxDEV(Js,{label:a,children:t.exports.jsxDEV(La,x({ref:n,colorScheme:o?"brand":"gray",variant:"solid",fontSize:"24px",icon:l},i),void 0,!1,{fileName:Ss,lineNumber:19,columnNumber:11},globalThis)},void 0,!1,{fileName:Ss,lineNumber:18,columnNumber:9},globalThis):t.exports.jsxDEV(Js,{label:a,children:t.exports.jsxDEV(Y,y(x({ref:n,colorScheme:o?"brand":"gray",variant:"solid"},i),{children:i.children}),void 0,!1,{fileName:Ss,lineNumber:32,columnNumber:9},globalThis)},void 0,!1,{fileName:Ss,lineNumber:31,columnNumber:7},globalThis)});var ee=c.exports.memo(Dc),ft="/Users/zimdev/work/zim-app/src/components/TipTap/menu/buttons/heading.tsx";const jc=[{name:"H1",value:1},{name:"H2",value:2},{name:"H3",value:3},{name:"H4",value:4},{name:"H5",value:5},{name:"H6",value:6}],Cl=c.exports.memo(()=>{const{editor:e}=c.exports.useContext(Il);return e?t.exports.jsxDEV(t.exports.Fragment,{children:t.exports.jsxDEV(Qt,{children:[t.exports.jsxDEV(Zt,{children:t.exports.jsxDEV(ee,{label:"H tag",icon:t.exports.jsxDEV(Ba,{},void 0,!1,{fileName:ft,lineNumber:31,columnNumber:45},globalThis)},void 0,!1,{fileName:ft,lineNumber:31,columnNumber:11},globalThis)},void 0,!1,{fileName:ft,lineNumber:30,columnNumber:9},globalThis),t.exports.jsxDEV(Yt,{zIndex:99,p:0,children:t.exports.jsxDEV(Xs,{p:2,children:t.exports.jsxDEV(At,{gap:2,children:jc.map(s=>t.exports.jsxDEV(ee,{isActive:e.isActive("heading",{level:s.value}),activeKey:"heading",activeOptions:{level:s.value},onClick:()=>e.chain().focus().toggleHeading({level:s.value}).run(),children:s.name},s.value,!1,{fileName:ft,lineNumber:37,columnNumber:17},globalThis))},void 0,!1,{fileName:ft,lineNumber:35,columnNumber:13},globalThis)},void 0,!1,{fileName:ft,lineNumber:34,columnNumber:11},globalThis)},void 0,!1,{fileName:ft,lineNumber:33,columnNumber:9},globalThis)]},void 0,!0,{fileName:ft,lineNumber:29,columnNumber:7},globalThis)},void 0,!1):null});var Q="/Users/zimdev/work/zim-app/src/components/TipTap/menu/buttons/table.tsx";const Cc=c.exports.memo(({editor:e})=>{if(!e)return null;const s=c.exports.useCallback(()=>{e.chain().focus().insertTable({rows:3,cols:3,withHeaderRow:!0}).run()},[e]),o=c.exports.useCallback(()=>{e.chain().focus().deleteTable().run()},[e]),l=c.exports.useCallback(()=>{e.chain().focus().addColumnAfter().run()},[e]),a=c.exports.useCallback(()=>{e.chain().focus().addColumnBefore().run()},[e]),i=c.exports.useCallback(()=>{e.chain().focus().deleteColumn().run()},[e]),n=c.exports.useCallback(()=>{e.chain().focus().addRowBefore().run()},[e]),r=c.exports.useCallback(()=>{e.chain().focus().addRowAfter().run()},[e]),u=c.exports.useCallback(()=>{e.chain().focus().deleteRow().run()},[e]),g=c.exports.useCallback(()=>{e.chain().focus().mergeCells().run()},[e]),b=c.exports.useCallback(()=>{e.chain().focus().splitCell().run()},[e]),m=c.exports.useCallback(()=>{e.chain().focus().toggleHeaderColumn().run()},[e]);return t.exports.jsxDEV(xt,{minChildWidth:38,gap:2,p:2,bg:"gray.50",w:"full",maxW:350,children:[t.exports.jsxDEV(ee,{onClick:s,icon:t.exports.jsxDEV(Ra,{},void 0,!1,{fileName:Q,lineNumber:85,columnNumber:15},globalThis),label:"Th\xEAm table"},void 0,!1,{fileName:Q,lineNumber:83,columnNumber:7},globalThis),t.exports.jsxDEV(ee,{onClick:o,icon:t.exports.jsxDEV(Ua,{},void 0,!1,{fileName:Q,lineNumber:92,columnNumber:15},globalThis),label:"X\xF3a table"},void 0,!1,{fileName:Q,lineNumber:89,columnNumber:7},globalThis),t.exports.jsxDEV(ee,{onClick:l,icon:t.exports.jsxDEV(Ga,{},void 0,!1,{fileName:Q,lineNumber:98,columnNumber:15},globalThis),label:"Th\xEAm c\u1ED9t b\xEAn ph\u1EA3i"},void 0,!1,{fileName:Q,lineNumber:95,columnNumber:7},globalThis),t.exports.jsxDEV(ee,{onClick:a,icon:t.exports.jsxDEV(Ha,{},void 0,!1,{fileName:Q,lineNumber:104,columnNumber:15},globalThis),label:"Th\xEAm c\u1ED9t b\xEAn tr\xE1i"},void 0,!1,{fileName:Q,lineNumber:101,columnNumber:7},globalThis),t.exports.jsxDEV(ee,{onClick:i,icon:t.exports.jsxDEV(Wa,{},void 0,!1,{fileName:Q,lineNumber:110,columnNumber:15},globalThis),label:"X\xF3a c\u1ED9t"},void 0,!1,{fileName:Q,lineNumber:107,columnNumber:7},globalThis),t.exports.jsxDEV(ee,{onClick:n,icon:t.exports.jsxDEV(Qa,{},void 0,!1,{fileName:Q,lineNumber:116,columnNumber:15},globalThis),label:"Th\xEAm h\xE0ng ph\xEDa tr\xEAn"},void 0,!1,{fileName:Q,lineNumber:113,columnNumber:7},globalThis),t.exports.jsxDEV(ee,{onClick:r,icon:t.exports.jsxDEV(Za,{},void 0,!1,{fileName:Q,lineNumber:122,columnNumber:15},globalThis),label:"Th\xEAm h\xE0ng b\xEAn d\u01B0\u1EDBi"},void 0,!1,{fileName:Q,lineNumber:119,columnNumber:7},globalThis),t.exports.jsxDEV(ee,{onClick:u,icon:t.exports.jsxDEV(Ya,{},void 0,!1,{fileName:Q,lineNumber:128,columnNumber:15},globalThis),label:"X\xF3a h\xE0ng"},void 0,!1,{fileName:Q,lineNumber:125,columnNumber:7},globalThis),t.exports.jsxDEV(ee,{onClick:g,icon:t.exports.jsxDEV(Ka,{},void 0,!1,{fileName:Q,lineNumber:135,columnNumber:15},globalThis),label:"G\u1ED9p c\xE1c \xF4"},void 0,!1,{fileName:Q,lineNumber:132,columnNumber:7},globalThis),t.exports.jsxDEV(ee,{onClick:b,icon:t.exports.jsxDEV(Ja,{},void 0,!1,{fileName:Q,lineNumber:142,columnNumber:15},globalThis),label:"T\xE1ch c\xE1c \xF4"},void 0,!1,{fileName:Q,lineNumber:139,columnNumber:7},globalThis),t.exports.jsxDEV(ee,{onClick:m,icon:t.exports.jsxDEV(Xa,{},void 0,!1,{fileName:Q,lineNumber:149,columnNumber:15},globalThis),label:"Toggle header c\u1ED9t"},void 0,!1,{fileName:Q,lineNumber:146,columnNumber:7},globalThis),t.exports.jsxDEV(ee,{onClick:()=>e.chain().focus().toggleHeaderRow().run(),icon:t.exports.jsxDEV(er,{},void 0,!1,{fileName:Q,lineNumber:155,columnNumber:15},globalThis),label:"Toggle header h\xE0ng"},void 0,!1,{fileName:Q,lineNumber:152,columnNumber:7},globalThis),t.exports.jsxDEV(ee,{onClick:()=>e.chain().focus().toggleHeaderCell().run(),icon:t.exports.jsxDEV(tr,{},void 0,!1,{fileName:Q,lineNumber:161,columnNumber:15},globalThis),label:"Toggle header cell"},void 0,!1,{fileName:Q,lineNumber:158,columnNumber:7},globalThis)]},void 0,!0,{fileName:Q,lineNumber:75,columnNumber:5},globalThis)}),$l=c.exports.memo(({editor:e})=>e?t.exports.jsxDEV(Qt,{children:[t.exports.jsxDEV(Zt,{children:t.exports.jsxDEV(ee,{label:"Table",icon:t.exports.jsxDEV(sr,{},void 0,!1,{fileName:Q,lineNumber:173,columnNumber:43},globalThis)},void 0,!1,{fileName:Q,lineNumber:173,columnNumber:9},globalThis)},void 0,!1,{fileName:Q,lineNumber:172,columnNumber:7},globalThis),t.exports.jsxDEV(Yt,{zIndex:99,p:0,children:t.exports.jsxDEV(Xs,{p:0,children:t.exports.jsxDEV(Cc,{editor:e},void 0,!1,{fileName:Q,lineNumber:177,columnNumber:11},globalThis)},void 0,!1,{fileName:Q,lineNumber:176,columnNumber:9},globalThis)},void 0,!1,{fileName:Q,lineNumber:175,columnNumber:7},globalThis)]},void 0,!0,{fileName:Q,lineNumber:171,columnNumber:5},globalThis):null);var Vs="/Users/zimdev/work/zim-app/src/components/ImageCheckbox/index.tsx";const Sl=e=>{const u=e,{isMultiple:s,children:o}=u,l=Ue(u,["isMultiple","children"]),{getInputProps:a,getCheckboxProps:i}=s?yi(l):Di(l),n=a(),r=i();return t.exports.jsxDEV(S,{as:"label",rounded:"base",overflow:"hidden",d:"block",children:[t.exports.jsxDEV("input",y(x({},n),{style:{display:"none"}}),void 0,!1,{fileName:Vs,lineNumber:20,columnNumber:7},globalThis),t.exports.jsxDEV(S,y(x({},r),{cursor:"pointer",opacity:.7,_checked:{borderColor:"red.500",opacity:1},_focus:{boxShadow:"none"},pos:"relative",children:[n.checked&&t.exports.jsxDEV(ji,{color:"green.500",fontSize:"2xl",pos:"absolute",top:2,left:2,zIndex:2},void 0,!1,{fileName:Vs,lineNumber:35,columnNumber:11},globalThis),o]}),void 0,!0,{fileName:Vs,lineNumber:21,columnNumber:7},globalThis)]},void 0,!0,{fileName:Vs,lineNumber:19,columnNumber:5},globalThis)},$c=async()=>{const e=yt(),s=bi(e);(!yt()||yt()&&Date.now()>=s*1e3)&&await al()};async function Sc(e){const o="https://stg-graph-api.zim.vn/upload";try{if(e)return await $c(),await(await fetch(o,{method:"POST",credentials:"include",headers:{Authorization:`Bearer ${yt()}`,Accept:"*/*","x-no-compression":"true"},body:e})).json()}catch(l){console.log("uploadFiles : "+l)}}const Vc=f`
  fragment Media on Media {
    id
    type
    path
    variants {
      id
      width
      height
      path
      type
    }
    filename
    title
    visibility
    width
    height
  }
`,kc=f`
  query medias(
    $first: Int
    $after: String
    $type: [MediaType]
    $width: Int
    $height: Int
    $search: String
    $userId: Int
  ) @api(name: "zim") {
    medias(
      first: $first
      after: $after
      type: $type
      width: $width
      height: $height
      search: $search
      userId: $userId
    ) {
      edges {
        node {
          ...Media
        }
      }
      pageInfo {
        startCursor
        endCursor
        hasNextPage
      }
    }
  }
  ${Vc}
`,wc=e=>e.scrollHeight-e.scrollTop<=e.clientHeight+10,Ic={control:e=>y(x({},e),{minHeight:42,backgroundColor:"white",borderColor:"var(--chakra-colors-gray-200)",borderRadius:"var(--chakra-radii-md)",":hover":{borderColor:"var(--chakra-colors-gray-300)"}}),menuPortal:e=>y(x({},e),{zIndex:2e7}),container:e=>y(x({},e),{fontSize:"16px",width:"100%",":focus":{borderWidth:2,borderColor:"var(--chakra-colors-blue-500)"},zIndex:1e5}),IndicatorsContainer:e=>y(x({},e),{minHeight:40}),option:(e,{isDisabled:s,isSelected:o})=>y(x({},e),{color:s?"#ccc":o?"white":"black",cursor:s?"not-allowed":"default",fontSize:"15px",":active":x({},e[":active"])}),multiValue:e=>y(x({},e),{backgroundColor:"#ccc"}),multiValueLabel:e=>y(x({},e),{color:"var(--chakra-colors-gray-700)",backgroundColor:"var(--chakra-colors-gray-200)"}),multiValueRemove:e=>y(x({},e),{color:"var(--chakra-colors-red-200)",backgroundColor:"var(--chakra-colors-gray-200)",borderRadius:0,":hover":{color:"var(--chakra-colors-red-500)",backgroundColor:"var(--chakra-colors-red-100)"}})},Pc=(e,s)=>{const[o,l]=c.exports.useState(e);return c.exports.useEffect(()=>{const a=setTimeout(()=>l(e),s);return()=>clearTimeout(a)},[e,s]),o};var U="/Users/zimdev/work/zim-app/src/components/UploadModal/index.tsx";const Vl=[{label:"T\u1EC7p tin",value:"file"},{label:"H\xECnh \u1EA3nh",value:"image"}],_c=({selectedCallback:e,onClose:s,isOpen:o,isMultiple:l=!0,defaultTypes:a=["image"],enableTypeFilter:i,enableSizeFilter:n,width:r,height:u})=>{var $e,Ie,oe,qe,Oe,J,Ee,it;const[g,b]=c.exports.useState(!1),[m,v]=c.exports.useState([]),[d,h]=c.exports.useState(!1),[A,E]=or(!0),[T,p]=c.exports.useState(""),D=Pc(T,500),[N,$]=c.exports.useState([...a]),[k]=ne(),C={first:20,type:zi(N),search:D,height:0,width:0,userId:parseInt(k.id,10)};A&&r&&u&&(C.width=r,C.height=u);const{data:F,fetchMore:Z,refetch:z,loading:G}=De(kc,{variables:C,notifyOnNetworkStatusChange:!0,skip:!o}),P=async()=>{b(!0);try{await z()}catch(R){console.log({e:R})}b(!1)},H=async()=>{const R=document.createElement("input");R.setAttribute("type","file"),N.includes("file")||R.setAttribute("accept","image/*"),R.click(),R.onchange=async()=>{const fe=R.files[0],lt=new FormData;lt.append("file",fe),lt.append("visibility","public"),await Sc(lt),await P()}},{getCheckboxProps:I,setValue:B}=ki({onChange:v}),{getRootProps:W,getRadioProps:re}=ir({name:"images",onChange:(...R)=>v(R)}),w=W(),O=()=>{var R;e((R=F==null?void 0:F.medias)==null?void 0:R.edges.filter(fe=>m.includes(fe.node.id))),B([]),s()},se=()=>{s(),B([])},ze=R=>{h(wc(R.target))};return c.exports.useEffect(()=>{var R;d&&typeof Z=="function"&&((R=F.medias)==null?void 0:R.pageInfo.hasNextPage)&&!G&&(async()=>{var fe;await Z({variables:{after:(fe=F.medias)==null?void 0:fe.pageInfo.endCursor,first:20,type:zi(N)}})})()},[d]),t.exports.jsxDEV(t.exports.Fragment,{children:t.exports.jsxDEV(Vt,{closeOnOverlayClick:!1,isCentered:!0,isOpen:o,onClose:se,size:"6xl",children:[t.exports.jsxDEV(Nt,{},void 0,!1,{fileName:U,lineNumber:182,columnNumber:9},globalThis),t.exports.jsxDEV(kt,{children:[t.exports.jsxDEV(Et,{children:"Th\u01B0 vi\u1EC7n media"},void 0,!1,{fileName:U,lineNumber:184,columnNumber:11},globalThis),t.exports.jsxDEV(wt,{},void 0,!1,{fileName:U,lineNumber:185,columnNumber:11},globalThis),t.exports.jsxDEV(It,{children:[t.exports.jsxDEV(q,{mb:4,justifyContent:"space-between",children:[t.exports.jsxDEV(Y,{as:Pt,variant:"outline",colorScheme:"black",onClick:H,leftIcon:t.exports.jsxDEV(lr,{},void 0,!1,{fileName:U,lineNumber:193,columnNumber:27},globalThis),isLoading:g,children:"Select from computer"},void 0,!1,{fileName:U,lineNumber:188,columnNumber:15},globalThis),i&&t.exports.jsxDEV(S,{width:300,children:t.exports.jsxDEV(Oi,{placeholder:"Ch\u1ECDn lo\u1EA1i media",styles:Ic,isMulti:!0,value:N.map(R=>Vl.find(fe=>fe.value===R)).filter(R=>R),onChange:R=>{$(R.map(fe=>fe.value))},options:Vl},void 0,!1,{fileName:U,lineNumber:201,columnNumber:19},globalThis)},void 0,!1,{fileName:U,lineNumber:200,columnNumber:17},globalThis),n&&r&&u&&t.exports.jsxDEV(q,{children:[t.exports.jsxDEV(M,{children:["Ch\u1EC9 filter nh\u1EEFng \u1EA3nh size ",r," x ",u]},void 0,!0,{fileName:U,lineNumber:221,columnNumber:19},globalThis),t.exports.jsxDEV(ar,{isChecked:A,onChange:E},void 0,!1,{fileName:U,lineNumber:224,columnNumber:19},globalThis)]},void 0,!0,{fileName:U,lineNumber:220,columnNumber:17},globalThis)]},void 0,!0,{fileName:U,lineNumber:187,columnNumber:13},globalThis),t.exports.jsxDEV(S,{width:350,pos:"relative",role:"group",children:[t.exports.jsxDEV(ms,{_focus:{color:"gray.900",borderColor:"gray.900"},_groupHover:{color:"gray.900",borderColor:"gray.900"},type:"text",placeholder:"T\xECm ki\u1EBFm media",size:"lg",pr:12,mb:4,value:T,onChange:R=>p(R.target.value)},void 0,!1,{fileName:U,lineNumber:229,columnNumber:15},globalThis),t.exports.jsxDEV(rr,{_groupHover:{color:"gray.900"},pos:"absolute",right:4,top:"50%",color:"gray.400",transform:"translateY(-50%)"},void 0,!1,{fileName:U,lineNumber:240,columnNumber:15},globalThis)]},void 0,!0,{fileName:U,lineNumber:228,columnNumber:13},globalThis),t.exports.jsxDEV(S,{position:"relative",h:"calc(65vh - 50px)",overflow:g?"hidden":"auto",w:"full",flexGrow:1,p:3,onScroll:ze,children:[l?t.exports.jsxDEV(q,{alignItems:"stretch",flexWrap:"wrap",mx:"-0.75rem",spacing:0,children:((Ie=($e=F==null?void 0:F.medias)==null?void 0:$e.edges)==null?void 0:Ie.length)>0&&((qe=(oe=F==null?void 0:F.medias)==null?void 0:oe.edges)==null?void 0:qe.map(({node:R})=>{const fe=I({value:R.id});return t.exports.jsxDEV(S,{w:{lg:"25%",base:"50%"},p:3,children:t.exports.jsxDEV(Sl,y(x({isMultiple:l},fe),{children:t.exports.jsxDEV(kl,x({},R),void 0,!1,{fileName:U,lineNumber:275,columnNumber:29},globalThis)}),void 0,!1,{fileName:U,lineNumber:274,columnNumber:27},globalThis)},`${R.id}`,!1,{fileName:U,lineNumber:269,columnNumber:25},globalThis)}))},void 0,!1,{fileName:U,lineNumber:259,columnNumber:17},globalThis):t.exports.jsxDEV(q,y(x({alignItems:"stretch",flexWrap:"wrap",mx:"-0.75rem",spacing:0},w),{children:((J=(Oe=F==null?void 0:F.medias)==null?void 0:Oe.edges)==null?void 0:J.length)>0&&((it=(Ee=F==null?void 0:F.medias)==null?void 0:Ee.edges)==null?void 0:it.map(({node:R})=>{const fe=re({value:R.id});return t.exports.jsxDEV(S,{w:{lg:"25%",base:"50%"},p:3,children:t.exports.jsxDEV(Sl,y(x({isMultiple:!1},fe),{children:t.exports.jsxDEV(kl,x({},R),void 0,!1,{fileName:U,lineNumber:299,columnNumber:29},globalThis)}),void 0,!1,{fileName:U,lineNumber:298,columnNumber:27},globalThis)},`${R.id}`,!1,{fileName:U,lineNumber:293,columnNumber:25},globalThis)}))}),void 0,!1,{fileName:U,lineNumber:282,columnNumber:17},globalThis),g&&t.exports.jsxDEV(q,{bgColor:"whiteAlpha.800",pos:"absolute",top:0,bottom:0,right:0,left:0,spacing:2,justifyContent:"center",children:[t.exports.jsxDEV(Gt,{color:"black",size:"xl"},void 0,!1,{fileName:U,lineNumber:318,columnNumber:19},globalThis),t.exports.jsxDEV(M,{fontSize:"2xl",color:"black",children:"\u0110ang ti\u1EBFn h\xE0nh upload"},void 0,!1,{fileName:U,lineNumber:319,columnNumber:19},globalThis)]},void 0,!0,{fileName:U,lineNumber:308,columnNumber:17},globalThis)]},void 0,!0,{fileName:U,lineNumber:249,columnNumber:13},globalThis)]},void 0,!0,{fileName:U,lineNumber:186,columnNumber:11},globalThis),t.exports.jsxDEV(Ht,{children:[t.exports.jsxDEV(Y,{colorScheme:"blue",mr:3,disabled:!m.length,onClick:O,children:"X\xE1c nh\u1EADn"},void 0,!1,{fileName:U,lineNumber:327,columnNumber:13},globalThis),t.exports.jsxDEV(Y,{colorScheme:"gray",mr:0,onClick:se,children:"\u0110\xF3ng l\u1EA1i"},void 0,!1,{fileName:U,lineNumber:335,columnNumber:13},globalThis)]},void 0,!0,{fileName:U,lineNumber:326,columnNumber:11},globalThis)]},void 0,!0,{fileName:U,lineNumber:183,columnNumber:9},globalThis)]},void 0,!0,{fileName:U,lineNumber:175,columnNumber:7},globalThis)},void 0,!1)};function kl({type:e,path:s,filename:o}){if(e==="file")return t.exports.jsxDEV(S,{display:"flex",flexDirection:"column",justifyContent:"center",borderWidth:2,borderColor:"#ccc",borderRadius:10,paddingX:"12px",w:"full",h:"150",children:t.exports.jsxDEV(ie,{spacing:6,width:"full",overflow:"hidden",children:[t.exports.jsxDEV(nr,{fontSize:"40"},void 0,!1,{fileName:U,lineNumber:360,columnNumber:11},this),t.exports.jsxDEV(S,{w:"full",display:"table",style:{tableLayout:"fixed"},children:t.exports.jsxDEV(M,{display:"table-cell",textAlign:"center",whiteSpace:"nowrap",textOverflow:"ellipsis",overflow:"hidden",children:o},void 0,!1,{fileName:U,lineNumber:362,columnNumber:13},this)},void 0,!1,{fileName:U,lineNumber:361,columnNumber:11},this)]},void 0,!0,{fileName:U,lineNumber:359,columnNumber:9},this)},void 0,!1,{fileName:U,lineNumber:348,columnNumber:7},this);const l="https://stg-graph-api.zim.vn";return t.exports.jsxDEV(S,{borderColor:"#ccc",borderRadius:10,borderWidth:2,w:"full",h:"150",backgroundSize:"cover",backgroundRepeat:"no-repeat",backgroundImage:`url('${ur(l,s)}')`},void 0,!1,{fileName:U,lineNumber:378,columnNumber:5},this)}var fo="/Users/zimdev/work/zim-app/src/components/TipTap/menu/buttons/image.tsx";const wl=c.exports.memo(({editor:e})=>{const s=r=>{r&&e.chain().focus().setImage({src:r}).run()},[o,l]=c.exports.useState(!1),a=()=>{l(!1)},i=()=>{l(!0)},n=r=>{r.map(u=>{s(`https://stg-graph-api.zim.vn/${u.node.path}`)})};return t.exports.jsxDEV(t.exports.Fragment,{children:[t.exports.jsxDEV(ee,{activeKey:"image",onClick:i,icon:t.exports.jsxDEV(cr,{},void 0,!1,{fileName:fo,lineNumber:32,columnNumber:15},globalThis),label:"H\xECnh \u1EA3nh"},void 0,!1,{fileName:fo,lineNumber:29,columnNumber:7},globalThis),t.exports.jsxDEV(_c,{isOpen:o,onClose:a,selectedCallback:n,isMultiple:!0},void 0,!1,{fileName:fo,lineNumber:35,columnNumber:7},globalThis)]},void 0,!0)});var K="/Users/zimdev/work/zim-app/src/components/TipTap/menu/index.tsx";const Fc=c.exports.memo(({editor:e,stickyMenuBar:s})=>{const o=j("white","slate.700"),l=j("gray.200","slate.600");return t.exports.jsxDEV(t.exports.Fragment,{children:t.exports.jsxDEV(xt,{minChildWidth:"38px",gap:2,w:"full",bg:o,zIndex:10,px:2,py:2,pos:s?"sticky":"static",top:0,borderBottom:"1px solid",borderBottomColor:l,children:[t.exports.jsxDEV(Cl,{},void 0,!1,{fileName:K,lineNumber:38,columnNumber:9},globalThis),t.exports.jsxDEV(ee,{onClick:()=>e.chain().focus().toggleBold().run(),activeKey:"bold",label:"In \u0111\u1EADm",icon:t.exports.jsxDEV(mr,{},void 0,!1,{fileName:K,lineNumber:43,columnNumber:17},globalThis)},void 0,!1,{fileName:K,lineNumber:39,columnNumber:9},globalThis),t.exports.jsxDEV(ee,{onClick:()=>e.chain().focus().toggleItalic().run(),activeKey:"italic",label:"In nghi\xEAng",icon:t.exports.jsxDEV(dr,{},void 0,!1,{fileName:K,lineNumber:49,columnNumber:17},globalThis)},void 0,!1,{fileName:K,lineNumber:45,columnNumber:9},globalThis),t.exports.jsxDEV(ee,{onClick:()=>e.chain().focus().toggleUnderline().run(),activeKey:"underline",label:"G\u1EA1ch ch\xE2n",icon:t.exports.jsxDEV(pr,{},void 0,!1,{fileName:K,lineNumber:55,columnNumber:17},globalThis)},void 0,!1,{fileName:K,lineNumber:51,columnNumber:9},globalThis),t.exports.jsxDEV(ee,{onClick:()=>e.chain().focus().toggleStrike().run(),activeKey:"strike",label:"G\u1EA1ch ngang",icon:t.exports.jsxDEV(br,{},void 0,!1,{fileName:K,lineNumber:61,columnNumber:17},globalThis)},void 0,!1,{fileName:K,lineNumber:57,columnNumber:9},globalThis),t.exports.jsxDEV(ee,{onClick:()=>e.chain().focus().setTextAlign("left").run(),label:"C\u0103n l\u1EC1 tr\xE1i",icon:t.exports.jsxDEV(gr,{},void 0,!1,{fileName:K,lineNumber:66,columnNumber:17},globalThis)},void 0,!1,{fileName:K,lineNumber:63,columnNumber:9},globalThis),t.exports.jsxDEV(ee,{onClick:()=>e.chain().focus().setTextAlign("center").run(),label:"C\u0103n gi\u1EEFa",icon:t.exports.jsxDEV(hr,{},void 0,!1,{fileName:K,lineNumber:71,columnNumber:17},globalThis)},void 0,!1,{fileName:K,lineNumber:68,columnNumber:9},globalThis),t.exports.jsxDEV(ee,{onClick:()=>e.chain().focus().setTextAlign("right").run(),label:"C\u0103n l\u1EC1 ph\u1EA3i",icon:t.exports.jsxDEV(fr,{},void 0,!1,{fileName:K,lineNumber:76,columnNumber:17},globalThis)},void 0,!1,{fileName:K,lineNumber:73,columnNumber:9},globalThis),t.exports.jsxDEV(ee,{onClick:()=>e.chain().focus().toggleBulletList().run(),label:"Danh s\xE1ch bullet",icon:t.exports.jsxDEV(Li,{},void 0,!1,{fileName:K,lineNumber:81,columnNumber:17},globalThis)},void 0,!1,{fileName:K,lineNumber:78,columnNumber:9},globalThis),t.exports.jsxDEV(ee,{onClick:()=>e.chain().focus().toggleOrderedList().run(),label:"Danh s\xE1ch s\u1ED1 th\u1EE9 t\u1EF1",icon:t.exports.jsxDEV(Bi,{},void 0,!1,{fileName:K,lineNumber:86,columnNumber:17},globalThis)},void 0,!1,{fileName:K,lineNumber:83,columnNumber:9},globalThis),t.exports.jsxDEV($l,{editor:e},void 0,!1,{fileName:K,lineNumber:106,columnNumber:9},globalThis),t.exports.jsxDEV(wl,{editor:e},void 0,!1,{fileName:K,lineNumber:107,columnNumber:9},globalThis)]},void 0,!0,{fileName:K,lineNumber:25,columnNumber:7},globalThis)},void 0,!1)});c.exports.memo(({editor:e})=>t.exports.jsxDEV(xt,{minChildWidth:38,gap:2,p:2,bg:"gray.50",w:{base:300,sm:300,md:450},children:[t.exports.jsxDEV(Cl,{},void 0,!1,{fileName:K,lineNumber:122,columnNumber:7},globalThis),t.exports.jsxDEV(ee,{onClick:()=>e.chain().focus().toggleBulletList().run(),label:"Danh s\xE1ch bullet",icon:t.exports.jsxDEV(Li,{},void 0,!1,{fileName:K,lineNumber:126,columnNumber:15},globalThis),isActive:e.isActive("bulletList")},void 0,!1,{fileName:K,lineNumber:123,columnNumber:7},globalThis),t.exports.jsxDEV(ee,{onClick:()=>e.chain().focus().toggleOrderedList().run(),label:"Danh s\xE1ch s\u1ED1 th\u1EE9 t\u1EF1",icon:t.exports.jsxDEV(Bi,{},void 0,!1,{fileName:K,lineNumber:132,columnNumber:15},globalThis),isActive:e.isActive("orderedList")},void 0,!1,{fileName:K,lineNumber:129,columnNumber:7},globalThis),t.exports.jsxDEV($l,{editor:e},void 0,!1,{fileName:K,lineNumber:136,columnNumber:7},globalThis),t.exports.jsxDEV(wl,{editor:e},void 0,!1,{fileName:K,lineNumber:137,columnNumber:7},globalThis)]},void 0,!0,{fileName:K,lineNumber:115,columnNumber:5},globalThis));var ke="/Users/zimdev/work/zim-app/src/components/TipTap/menu/BubbleMenu/image-menu.tsx";const Mc=c.exports.memo(({editor:e})=>{const{onOpen:s,onClose:o,isOpen:l}=Xe(),a=c.exports.useRef(null),[i,n]=c.exports.useState(""),[r,u]=c.exports.useState(""),g=async()=>{const b=e.getAttributes("image").src;e.chain().focus().setImage({src:b,alt:r,title:i}).run(),o()};return c.exports.useEffect(()=>{e.isActive("image")?(u(e.getAttributes("image").alt),n(e.getAttributes("image").title)):(u(""),n(""))},[l]),t.exports.jsxDEV(q,{bg:"white",shadow:"base",p:2,spacing:4,rounded:4,children:[t.exports.jsxDEV(S,{fontSize:"sm",children:[t.exports.jsxDEV(M,{as:"div",children:[t.exports.jsxDEV("strong",{children:"Title:"},void 0,!1,{fileName:ke,lineNumber:42,columnNumber:11},globalThis)," ",e.getAttributes("image").title]},void 0,!0,{fileName:ke,lineNumber:41,columnNumber:9},globalThis),t.exports.jsxDEV(M,{as:"div",children:[t.exports.jsxDEV("strong",{children:"Alt:"},void 0,!1,{fileName:ke,lineNumber:45,columnNumber:11},globalThis)," ",e.getAttributes("image").alt]},void 0,!0,{fileName:ke,lineNumber:44,columnNumber:9},globalThis)]},void 0,!0,{fileName:ke,lineNumber:40,columnNumber:7},globalThis),t.exports.jsxDEV(Qt,{isOpen:l,initialFocusRef:a,onOpen:s,onClose:o,placement:"top",closeOnBlur:!0,children:[t.exports.jsxDEV(Zt,{children:t.exports.jsxDEV(ee,{activeKey:"image",label:"Ch\u1EC9nh s\u1EEDa",icon:t.exports.jsxDEV(xr,{},void 0,!1,{fileName:ke,lineNumber:60,columnNumber:19},globalThis)},void 0,!1,{fileName:ke,lineNumber:57,columnNumber:11},globalThis)},void 0,!1,{fileName:ke,lineNumber:56,columnNumber:9},globalThis),t.exports.jsxDEV(Yt,{children:t.exports.jsxDEV(S,{p:4,bg:"white",shadow:"base",children:t.exports.jsxDEV(ie,{spacing:4,alignItems:"flex-start",children:[t.exports.jsxDEV(Ge,{ref:a,label:"Title",id:"title-url",value:i,onChange:b=>n(b.target.value),autoComplete:"off"},void 0,!1,{fileName:ke,lineNumber:66,columnNumber:15},globalThis),t.exports.jsxDEV(Ge,{label:"Alt",id:"alt-url",value:r,onChange:b=>u(b.target.value),autoComplete:"off"},void 0,!1,{fileName:ke,lineNumber:76,columnNumber:15},globalThis),t.exports.jsxDEV(Y,{colorScheme:"teal",onClick:g,children:"C\u1EADp nh\u1EADt"},void 0,!1,{fileName:ke,lineNumber:86,columnNumber:15},globalThis)]},void 0,!0,{fileName:ke,lineNumber:65,columnNumber:13},globalThis)},void 0,!1,{fileName:ke,lineNumber:64,columnNumber:11},globalThis)},void 0,!1,{fileName:ke,lineNumber:63,columnNumber:9},globalThis)]},void 0,!0,{fileName:ke,lineNumber:48,columnNumber:7},globalThis)]},void 0,!0,{fileName:ke,lineNumber:39,columnNumber:5},globalThis)});var qc=eo.create({name:"iframe",group:"block",atom:!0,defaultOptions:{allowFullscreen:!0,HTMLAttributes:{class:"iframe-wrapper"}},addAttributes(){return{class:{default:null},src:{default:null},frameborder:{default:0},allowfullscreen:{default:this.options.allowFullscreen,parseHTML:()=>this.options.allowFullscreen},width:{default:this.options.width},height:{default:this.options.height}}},parseHTML(){return[{tag:"iframe"}]},renderHTML({HTMLAttributes:e}){return["div",this.options.HTMLAttributes,["iframe",e]]},addCommands(){return{setIframe:e=>({tr:s,dispatch:o})=>{const{selection:l}=s,a=this.type.create(e);return o&&s.replaceRangeWith(l.from,l.to,a),!0}}}});eo.create({name:"video",group:"block",atom:!0,defaultOptions:{HTMLAttributes:{class:"iframe-wrapper"}},addAttributes(){return{class:{default:this.options.class},src:{default:null},autoplay:{default:this.options.width},loop:{default:this.options.loop},muted:{default:this.options.muted},preload:{default:this.options.preload},width:{default:this.options.width},height:{default:this.options.height},controls:{default:this.options.controls}}},parseHTML(){return[{tag:"video"}]},renderHTML({HTMLAttributes:e}){return["div",this.options.HTMLAttributes,["video",e]]},addCommands(){return{Video:e=>({tr:s,dispatch:o})=>{const{selection:l}=s,a=this.type.create(e);return o&&s.replaceRangeWith(l.from,l.to,a),!0}}}});var zc=eo.create({name:"spacing",content:"inline*",atom:!0,draggable:!0,defaultOptions:{HTMLAttributes:{class:"space-wrapper","data-question":"","data-question-group":"","data-label":"","data-type":""}},addOptions(){return{inline:!0,HTMLAttributes:{}}},inline(){return this.options.inline},group(){return this.options.inline?"inline":"block"},addAttributes(){var e,s,o,l,a,i,n;return{class:{default:((e=this==null?void 0:this.options)==null?void 0:e.class)||void 0},"data-question":{default:(o=(s=this==null?void 0:this.options)==null?void 0:s["data-question"])!=null?o:"",parseHTML:r=>r.getAttribute("data-question")},"data-question-group":{default:(a=(l=this==null?void 0:this.options)==null?void 0:l["data-question-group"])!=null?a:"",parseHTML:r=>r.getAttribute("data-question-group")},"data-type":{default:(n=(i=this==null?void 0:this.options)==null?void 0:i["data-type"])!=null?n:"",parseHTML:r=>r.getAttribute("data-type")},"data-label":{default:"",parseHTML:r=>r.getAttribute("data-label"),renderHTML:r=>r["data-label"]?{"data-label":r["data-label"]}:{"data-label":""}}}},parseHTML(){return[{tag:"span.space-wrapper"}]},renderHTML({HTMLAttributes:e}){return["span",Nr(this.options.HTMLAttributes,e)]},renderText({HTMLAttributes:e}){var s;return(s=e==null?void 0:e["data-label"])!=null?s:"r\u1ED7ng"},addCommands(){return{addSpace:e=>({commands:s})=>s.insertContent({type:this.name,attrs:e})}}}),st="/Users/zimdev/work/zim-app/src/components/TipTap/index.tsx";const Il=c.exports.createContext({editor:null}),Oc=c.exports.forwardRef(({onChange:e,placeholder:s="Nh\u1EADp n\u1ED9i dung",defaultValue:o="",extensions:l=[],enableSpaceMenu:a=!1,showWordCount:i=!0,isDisabled:n=!1,stickyMenuBar:r=!1},u)=>{const[g,b]=c.exports.useState(0),m=Er({extensions:[vr,vc.configure({resizable:!0}),Ar,Tr,Ec,Ac,yr,Dr,yc.configure({inline:!0,HTMLAttributes:{class:"custom-image"}}),Tc.configure({openOnClick:!1}),jr.configure({placeholder:s||"Nh\u1EADp n\u1ED9i dung",emptyEditorClass:"is-editor-empty"}),qc.configure({inline:!0}),Cr.configure({types:["heading","paragraph"],alignments:["left","right","center"]}),zc.configure({inline:!0}),...l],content:o,onCreate:async({editor:A})=>{const E=A.state.doc.textContent.split(" ").length;b(E)},onUpdate:({editor:A})=>{const E=A.state.doc.textContent.split(" ").length;b(E);const T=A.getHTML();typeof e=="function"&&e(T)},editable:!n}),v=j("white","slate.700"),d=j("gray.200","slate.700"),h=is(m,1e3);return t.exports.jsxDEV(Il.Provider,{value:{editor:h},children:[i&&t.exports.jsxDEV(S,{mb:4,children:["Word count: ",t.exports.jsxDEV("strong",{children:g},void 0,!1,{fileName:st,lineNumber:124,columnNumber:25},globalThis)]},void 0,!0,{fileName:st,lineNumber:123,columnNumber:11},globalThis),t.exports.jsxDEV(S,{p:2,pt:0,borderColor:d,borderRadius:4,borderWidth:1,h:"full",bg:v,children:m&&t.exports.jsxDEV(t.exports.Fragment,{children:[!n&&t.exports.jsxDEV(t.exports.Fragment,{children:[t.exports.jsxDEV(Fc,{editor:m,enableSpaceMenu:a,stickyMenuBar:r},void 0,!1,{fileName:st,lineNumber:141,columnNumber:19},globalThis),t.exports.jsxDEV(Ri,{editor:m,pluginKey:"bubbleImageMenu",shouldShow:({editor:A})=>A.isActive("image"),children:t.exports.jsxDEV(Mc,{editor:m},void 0,!1,{fileName:st,lineNumber:162,columnNumber:21},globalThis)},void 0,!1,{fileName:st,lineNumber:157,columnNumber:19},globalThis),t.exports.jsxDEV(Ri,{editor:m,tippyOptions:{placement:"bottom"},pluginKey:"bubbleTextSelection",shouldShow:({editor:A,view:E,state:T})=>A.isActive("link",{"data-contextual":!0})},void 0,!1,{fileName:st,lineNumber:181,columnNumber:19},globalThis)]},void 0,!0),t.exports.jsxDEV($r,{editor:m,ref:u},void 0,!1,{fileName:st,lineNumber:196,columnNumber:15},globalThis)]},void 0,!0)},void 0,!1,{fileName:st,lineNumber:128,columnNumber:9},globalThis)]},void 0,!0,{fileName:st,lineNumber:121,columnNumber:7},globalThis)});var Pl=c.exports.memo(Oc);function _l(){const e=Vr(),s=kr(),o=wr();return c.exports.useMemo(()=>({navigate:o,pathname:s.pathname,query:x(x({},Sr.parse(s.search)),e),location:s}),[e,s,o])}var ks="/Users/zimdev/work/zim-app/src/components/Select/index.tsx";const Lc=c.exports.forwardRef((n,i)=>{var r=n,{onChange:e,paramKey:s,options:o,allOption:l}=r,a=Ue(r,["onChange","paramKey","options","allOption"]);var v;Ir();const u=_l();vt();const g=c.exports.useMemo(()=>({menuPortal:d=>y(x({},d),{zIndex:1400}),container:(d,h)=>y(x({},d),{color:j("var(--chakra-colors-gray-900)","var(--chakra-colors-slate-400)")}),placeholder:(d,h)=>y(x({},d),{color:j("var(--chakra-colors-gray-400)","var(--chakra-colors-slate-500)")}),input:(d,h)=>y(x({},d),{minHeight:30,color:j("var(--chakra-colors-gray-900)","var(--chakra-colors-slate-400)")}),control:(d,h)=>y(x({},d),{backgroundColor:h.isDisabled?"var(--chakra-colors-gray.500)":j("var(--chakra-colors-white)","var(--chakra-colors-slate-700)"),borderRadius:"var(--chakra-radii-md)",borderColor:j("var(--chakra-colors-gray.300)","var(--chakra-colors-slate-600)")}),menu:(d,h)=>y(x({},d),{zIndex:3}),menuList:(d,h)=>y(x({},d),{backgroundColor:j("var(--chakra-colors-white)","var(--chakra-colors-slate-700)")}),option:(d,h)=>y(x({},d),{color:h.isFocused?j("var(--chakra-colors-gray.900)","var(--chakra-colors-slate-900)"):j("var(--chakra-colors-gray.900)","currentColor"),"&:hover":{color:h.isSelected?j("var(--chakra-colors-white)","var(--chakra-colors-white)"):j("var(--chakra-colors-gray.900)","var(--chakra-colors-slate-900)")}}),singleValue:(d,h)=>y(x({},d),{color:j("var(--chakra-colors-gray.900)","var(--chakra-colors-slate-300)")})}),[]),b=c.exports.useCallback((d,h)=>{e(d,h)},[a,u.query,s]),m=c.exports.useMemo(()=>l?[l,...o]:o,[a,o,l]);return t.exports.jsxDEV(cs,{isInvalid:!!(a==null?void 0:a.error),width:"100%",children:[(a==null?void 0:a.label)&&t.exports.jsxDEV(Bs,{htmlFor:a.id,children:a==null?void 0:a.label},void 0,!1,{fileName:ks,lineNumber:159,columnNumber:11},globalThis),t.exports.jsxDEV(Oi,y(x({ref:i,id:a.id},a),{styles:g,onChange:b,options:m,menuPortalTarget:document.body}),void 0,!1,{fileName:ks,lineNumber:161,columnNumber:9},globalThis),(a==null?void 0:a.error)&&t.exports.jsxDEV(ds,{children:(v=a==null?void 0:a.error)==null?void 0:v.message},void 0,!1,{fileName:ks,lineNumber:171,columnNumber:11},globalThis)]},void 0,!0,{fileName:ks,lineNumber:157,columnNumber:7},globalThis)});var ws=c.exports.memo(Lc),le="/Users/zimdev/work/zim-app/src/components/Forms/CreateQAToForum/index.tsx";const Bc=({questionText:e})=>{var i;const{data:s}=De(Mn),{register:o,formState:{errors:l}}=Ui(),a=(i=s==null?void 0:s.QAPostCategoriesTree)!=null?i:[];return t.exports.jsxDEV(At,{direction:"column",w:"full",justifyContent:"stretch",alignItems:"stretch",className:"reply-editor",gap:4,children:[t.exports.jsxDEV(Ge,y(x({label:"Ti\xEAu \u0111\u1EC1",placeholder:"Nh\u1EADp ti\xEAu \u0111\u1EC1..."},o("title",{required:!0})),{error:l==null?void 0:l.name}),void 0,!1,{fileName:le,lineNumber:53,columnNumber:7},globalThis),t.exports.jsxDEV(ve,{spacing:1,children:[t.exports.jsxDEV(M,{fontSize:14,children:"C\xE2u h\u1ECFi"},void 0,!1,{fileName:le,lineNumber:63,columnNumber:9},globalThis),t.exports.jsxDEV(S,{fontWeight:"400",bg:"gray.100",borderWidth:1,w:"100%",p:4,color:"black",rounded:"md",children:e},void 0,!1,{fileName:le,lineNumber:64,columnNumber:9},globalThis)]},void 0,!0,{fileName:le,lineNumber:62,columnNumber:7},globalThis),t.exports.jsxDEV(Kt,{rules:{required:"Vui l\xF2ng ch\u1ECDn ch\u1EE7 \u0111\u1EC1"},name:"categories",render:({field:n,fieldState:{error:r}})=>t.exports.jsxDEV(ws,x({error:r,label:"Ch\u1EE7 \u0111\u1EC1",options:a,getOptionLabel:u=>u.title,getOptionValue:u=>u.id+"",formatOptionLabel:u=>t.exports.jsxDEV(M,{children:u.title},void 0,!1,{fileName:le,lineNumber:89,columnNumber:56},globalThis)},n),void 0,!1,{fileName:le,lineNumber:83,columnNumber:11},globalThis)},void 0,!1,{fileName:le,lineNumber:77,columnNumber:7},globalThis),t.exports.jsxDEV(ve,{spacing:1,children:[t.exports.jsxDEV(M,{fontWeight:"400",fontSize:14,children:"C\xE2u tr\u1EA3 l\u1EDDi"},void 0,!1,{fileName:le,lineNumber:95,columnNumber:9},globalThis),t.exports.jsxDEV(Kt,{name:"answer",render:({field:n})=>t.exports.jsxDEV(Pl,x({showWordCount:!1,placeholder:"Nh\u1EADp c\xE2u tr\u1EA3 l\u1EDDi..."},n),void 0,!1,{fileName:le,lineNumber:101,columnNumber:13},globalThis)},void 0,!1,{fileName:le,lineNumber:98,columnNumber:9},globalThis)]},void 0,!0,{fileName:le,lineNumber:94,columnNumber:7},globalThis)]},void 0,!0,{fileName:le,lineNumber:45,columnNumber:5},globalThis)},Rc=({onClose:e,isOpen:s,questionText:o,messageQAOwnerId:l,setIsHoverMessage:a})=>{const i=c.exports.useRef(),n=bs({mode:"onBlur",reValidateMode:"onChange"}),r=Je(),[u]=Pe(Tu),[g,{loading:b}]=Pe(yu),m=c.exports.useCallback(async({title:v,categories:d,answer:h})=>{var A;try{const{data:E}=await u({variables:{input:be({title:v,content:o,type:"QUESTION",categoryIds:(d==null?void 0:d.id)?[d==null?void 0:d.id]:""},T=>T),userId:l}});await g({variables:{ref:(A=E==null?void 0:E.createQAPost)==null?void 0:A.id,content:h,type:"QAPost"}}),e(),a(!1),await r({title:"Success.",description:"\u0110\u1EA9y c\xE2u h\u1ECFi l\xEAn forum th\xE0nh c\xF4ng",status:"success",duration:3e3,isClosable:!0})}catch(E){console.log(E)}},[o,l,e,a]);return t.exports.jsxDEV(Hs,y(x({},n),{children:t.exports.jsxDEV(Vt,{finalFocusRef:i,isOpen:s,onClose:e,size:"3xl",children:[t.exports.jsxDEV(Nt,{},void 0,!1,{fileName:le,lineNumber:181,columnNumber:9},globalThis),t.exports.jsxDEV(kt,{children:[t.exports.jsxDEV(Et,{children:"T\u1EA1o QA "},void 0,!1,{fileName:le,lineNumber:183,columnNumber:11},globalThis),t.exports.jsxDEV(wt,{},void 0,!1,{fileName:le,lineNumber:184,columnNumber:11},globalThis),t.exports.jsxDEV("form",{onSubmit:n.handleSubmit(m),children:[t.exports.jsxDEV(It,{children:t.exports.jsxDEV(Bc,{questionText:o},void 0,!1,{fileName:le,lineNumber:187,columnNumber:15},globalThis)},void 0,!1,{fileName:le,lineNumber:186,columnNumber:13},globalThis),t.exports.jsxDEV(Ht,{children:[t.exports.jsxDEV(Y,{leftIcon:t.exports.jsxDEV(Wt,{as:Ws},void 0,!1,{fileName:le,lineNumber:192,columnNumber:27},globalThis),colorScheme:"green",mr:4,type:"submit",isLoading:b,children:"X\xE1c nh\u1EADn"},void 0,!1,{fileName:le,lineNumber:191,columnNumber:15},globalThis),t.exports.jsxDEV(Y,{variant:"ghost",onClick:e,children:"H\u1EE7y"},void 0,!1,{fileName:le,lineNumber:200,columnNumber:15},globalThis)]},void 0,!0,{fileName:le,lineNumber:190,columnNumber:13},globalThis)]},void 0,!0,{fileName:le,lineNumber:185,columnNumber:11},globalThis)]},void 0,!0,{fileName:le,lineNumber:182,columnNumber:9},globalThis)]},void 0,!0,{fileName:le,lineNumber:175,columnNumber:7},globalThis)}),void 0,!1,{fileName:le,lineNumber:174,columnNumber:5},globalThis)};var ce="/Users/zimdev/work/zim-app/src/pages/chat/boxMessage/components/message.tsx";function Uc({isMyMessage:e,text:s,loading:o,notSeen:l,error:a,createdAt:i,lastSeenMessageId:n,id:r,userData:u,attachments:g,deletedAt:b,from:m,isGroup:v,conversation:d,emojis:h}){var H,I,B;const[A]=ne(),{isOpen:E,onOpen:T,onClose:p}=Xe(),[D,N]=c.exports.useState(!1),$=is(D,300),[k,C]=c.exports.useState(null),[F,Z]=c.exports.useState(!1),z=j("white","slate.300"),G=(H=d==null?void 0:d.participants)==null?void 0:H.find(W=>(W==null?void 0:W.userId)===m),P=c.exports.useMemo(()=>{var W;return t.exports.jsxDEV(S,{as:"div",w:6,h:6,rounded:"full",alignItems:"center",justifyContent:"center",display:"flex",flexDirection:"column",position:"absolute",overflow:"hidden",bg:z,className:`${e?"-right-8 top-3":"-left-[30px] bottom-6"}`,children:t.exports.jsxDEV(He,{size:"xs",src:G==null?void 0:G.avatar,name:(W=G==null?void 0:G.fullName)!=null?W:"\u1EA8n danh"},void 0,!1,{fileName:ce,lineNumber:92,columnNumber:9},this)},void 0,!1,{fileName:ce,lineNumber:76,columnNumber:7},this)},[e,z,G==null?void 0:G.avatar,G==null?void 0:G.fullName]);return t.exports.jsxDEV(t.exports.Fragment,{children:[t.exports.jsxDEV(q,{onMouseEnter:()=>!b&&N(!0),onMouseLeave:()=>!b&&N(!1),w:"full",position:"relative",alignItems:"center",justifyContent:e?"flex-end":"flex-start",children:t.exports.jsxDEV(q,{w:"full",alignItems:"center",children:t.exports.jsxDEV(S,{w:"full",position:"relative",display:"flex",justifyContent:e?"flex-end":"flex-start ",children:t.exports.jsxDEV(S,{w:"full",display:"flex",flexDirection:"column",alignItems:e?"flex-end":"flex-start",children:[P,!e&&t.exports.jsxDEV(M,{color:"gray.400",fontSize:12,children:((B=(I=d==null?void 0:d.participants)==null?void 0:I.find(W=>(W==null?void 0:W.userId)===m))==null?void 0:B.fullName)||"\u1EA8n danh"},void 0,!1,{fileName:ce,lineNumber:126,columnNumber:17},this),t.exports.jsxDEV(Qt,{isOpen:$,placement:e?"left":"right",children:[t.exports.jsxDEV(Zt,{children:t.exports.jsxDEV(q,{align:"center",children:t.exports.jsxDEV(cc,{isMyMessage:e,text:s,deletedAt:b,createdAt:i,attachments:g,setAttachmentIndex:C,setShowImageViewer:Z},void 0,!1,{fileName:ce,lineNumber:138,columnNumber:21},this)},void 0,!1,{fileName:ce,lineNumber:137,columnNumber:19},this)},void 0,!1,{fileName:ce,lineNumber:136,columnNumber:17},this),t.exports.jsxDEV(Yt,{w:"65px",bg:"transparent",border:0,boxShadow:"none",_focus:{outline:"none"},children:$&&t.exports.jsxDEV(q,{alignItems:"center",flexGrow:1,justifyContent:e?"flex-end":"flex-start",children:[t.exports.jsxDEV(dc,{isMyMessage:e,isHoverMessage:!0,setIsHoverMessage:N,id:r},void 0,!1,{fileName:ce,lineNumber:165,columnNumber:23},this),t.exports.jsxDEV(xc,{id:r,setIsHoverMessage:N},void 0,!1,{fileName:ce,lineNumber:171,columnNumber:23},this),To.map(W=>W.id).includes(A.roleId)&&s&&t.exports.jsxDEV(bc,{isParticipant:!e,onOpen:T},void 0,!1,{fileName:ce,lineNumber:177,columnNumber:27},this)]},void 0,!0,{fileName:ce,lineNumber:160,columnNumber:21},this)},void 0,!1,{fileName:ce,lineNumber:150,columnNumber:17},this)]},void 0,!0,{fileName:ce,lineNumber:132,columnNumber:15},this),t.exports.jsxDEV(q,{w:"full",justify:e?"flex-end":"flex-start",mt:1,alignItems:"center",spacing:2,children:[t.exports.jsxDEV(M,{letterSpacing:.1,fontSize:12,color:"gray.500",children:Ke(i).format("HH:mm")},void 0,!1,{fileName:ce,lineNumber:193,columnNumber:17},this),t.exports.jsxDEV(pc,{isGroup:v,isMyMessage:e,lastSeenMessageId:n,notSeen:l,id:r,error:a,loading:o,userData:u},void 0,!1,{fileName:ce,lineNumber:196,columnNumber:17},this),!b&&(h==null?void 0:h.length)>0&&t.exports.jsxDEV(Nc,{emojis:h},void 0,!1,{fileName:ce,lineNumber:207,columnNumber:19},this)]},void 0,!0,{fileName:ce,lineNumber:186,columnNumber:15},this),t.exports.jsxDEV(jl,{setAttachmentIndex:C,attachments:g,attachmentIndex:k,showImageViewer:F,setShowImageViewer:Z},void 0,!1,{fileName:ce,lineNumber:211,columnNumber:15},this)]},void 0,!0,{fileName:ce,lineNumber:118,columnNumber:13},this)},void 0,!1,{fileName:ce,lineNumber:112,columnNumber:11},this)},void 0,!1,{fileName:ce,lineNumber:111,columnNumber:9},this)},void 0,!1,{fileName:ce,lineNumber:103,columnNumber:7},this),t.exports.jsxDEV(Rc,{isOpen:E,onClose:p,questionText:s,messageQAOwnerId:m,setIsHoverMessage:N},void 0,!1,{fileName:ce,lineNumber:223,columnNumber:7},this)]},void 0,!0)}var Gc=c.exports.memo(Uc),Qe="/Users/zimdev/work/zim-app/src/pages/chat/boxMessage/components/listMessage.tsx";const Hc=({conversationId:e,setConversationId:s,ownerId:o,messages:l,participants:a,userId:i,isGroup:n,loading:r,channel:u,isLoadMore:g,onLoadBeforeMessage:b,hasBeforeLoadMoreButton:m,searchLoading:v,onLoadAfterMessage:d,hasAfterLoadMoreButton:h,chatBoxRef:A,totalDocs:E})=>{var H;const[T,p]=c.exports.useState(null),D=V(I=>I.searchStringMessage),[N,$]=c.exports.useState(""),{onMarkSeen:k}=go({setContent:()=>{},userId:i,group:u,setConversationId:s}),C=c.exports.useMemo(()=>at.exports.uniqBy(at.exports.reverse([].concat(l)),I=>I.id),[l]),F=(H=l.find(I=>{var B;if(I.from===o&&((B=I.seenBy)==null?void 0:B.length)>=2)return!0}))==null?void 0:H.id,Z=a,z=Z==null?void 0:Z.filter(I=>I.userId!==o)[0],G=C[C.length-1],P=Pr();return Gs(()=>{var I;e&&(a==null?void 0:a.find(B=>B.userId===o))&&!r&&!((I=G==null?void 0:G.seenBy)==null?void 0:I.includes(o==null?void 0:o.toString()))&&async function(){await P(k(e))}()},[P,o,G,a,r,e]),c.exports.useLayoutEffect(()=>{p(null)},[e]),c.exports.useEffect(()=>{let I;return(A==null?void 0:A.current)&&!D&&T!==e&&(I=setTimeout(()=>{var B;A.current.scrollTop=(B=A.current.scrollHeight)!=null?B:5e3,p(e)},50)),()=>{clearTimeout(I)}},[e,T,D]),t.exports.jsxDEV(ie,{alignItems:"stretch",w:"full",px:10,py:4,spacing:4,children:[m&&E>=(C==null?void 0:C.length)&&t.exports.jsxDEV(ie,{children:t.exports.jsxDEV(Y,{w:"100px",h:"30px",onClick:b,isLoading:v,loadingText:"Loading",colorScheme:"teal",variant:"outline",spinnerPlacement:"end",children:"Load more"},void 0,!1,{fileName:Qe,lineNumber:108,columnNumber:11},globalThis)},void 0,!1,{fileName:Qe,lineNumber:107,columnNumber:9},globalThis),g&&t.exports.jsxDEV(_r,{in:g,animateOpacity:!0,children:t.exports.jsxDEV(S,{pt:2,children:t.exports.jsxDEV(ls,{text:"\u0110ang t\u1EA3i tin nh\u1EAFn..."},void 0,!1,{fileName:Qe,lineNumber:125,columnNumber:13},globalThis)},void 0,!1,{fileName:Qe,lineNumber:124,columnNumber:11},globalThis)},void 0,!1,{fileName:Qe,lineNumber:123,columnNumber:9},globalThis),C==null?void 0:C.map((I,B)=>{var $e,Ie,oe,qe,Oe;const W=I.from===o,re=gs(($e=C[B])==null?void 0:$e.createdAt),w=gs((Ie=C[B-1])==null?void 0:Ie.createdAt),O=gs.duration(re.diff(w)),se=O.asMinutes(),ze=se>15&&t.exports.jsxDEV(lc,{time:I==null?void 0:I.createdAt},void 0,!1,{fileName:Qe,lineNumber:136,columnNumber:13},globalThis);return t.exports.jsxDEV(St.Fragment,{children:[ze,t.exports.jsxDEV(Gc,y(x({},I),{QAId:N,setQAId:$,isGroup:n,attachments:(oe=I==null?void 0:I.attachments)!=null?oe:[],loading:I==null?void 0:I.loading,error:I==null?void 0:I.error,isMyMessage:W,text:I==null?void 0:I.text,id:I==null?void 0:I.id,userData:z,lastSeenMessageId:F,createdAt:I==null?void 0:I.createdAt,notSeen:((qe=I==null?void 0:I.seenBy)==null?void 0:qe.length)===1&&((Oe=I==null?void 0:I.seenBy)==null?void 0:Oe.includes(o))}),void 0,!1,{fileName:Qe,lineNumber:141,columnNumber:13},globalThis)]},B,!0,{fileName:Qe,lineNumber:139,columnNumber:11},globalThis)}),h&&(C==null?void 0:C.length)<E&&t.exports.jsxDEV(ie,{pb:2,children:t.exports.jsxDEV(Y,{w:"100px",h:"30px",onClick:d,isLoading:v,loadingText:"Loading",colorScheme:"teal",variant:"outline",spinnerPlacement:"end",children:"Load more"},void 0,!1,{fileName:Qe,lineNumber:164,columnNumber:11},globalThis)},void 0,!1,{fileName:Qe,lineNumber:163,columnNumber:9},globalThis)]},void 0,!0,{fileName:Qe,lineNumber:105,columnNumber:5},globalThis)},Wc=({userId:e})=>{var E,T,p;const[s]=ne(),o=mt(),[l,a]=c.exports.useState(!1),i=V(D=>D.conversationId),n=V(D=>D.setConversationId),r=y(x({},be({conversationId:i,userId:i?"":e==null?void 0:e.toString(),limit:30},D=>D)),{offset:0}),{data:u,loading:g,fetchMore:b,error:m}=De(Ze,{variables:x({},r),notifyOnNetworkStatusChange:!0,skip:!i&&!e});Rs(cl,{onSubscriptionData:({client:D,subscriptionData:N})=>{var k,C;const $=D.cache.readQuery({query:Ze,variables:r});if($==null?void 0:$.messages){let Z=[...(C=(k=$==null?void 0:$.messages)==null?void 0:k.docs)!=null?C:[]];const{conversationUpdated:z}=N.data,{lastMessage:G,isDeleteMessage:P,id:H}=z,{id:I,from:B}=G,W=Z==null?void 0:Z.find(re=>(re==null?void 0:re.id)===I);P&&!l&&i===H&&a(!0),!W&&s.id!==B&&i===H&&(D.writeQuery({query:Ze,variables:r,data:y(x({},$),{messages:y(x({},$.messages),{docs:[G,...Z]})})}),setTimeout(()=>{const re=document.getElementById("chat-box");re&&(re.scrollTop=re.scrollHeight)},50))}},shouldResubscribe:!!i});const v=((E=u==null?void 0:u.messages)==null?void 0:E.docs)||[],d=(T=u==null?void 0:u.messages)==null?void 0:T.hasNextPage,{onLoadMore:h,isLoadingMore:A}=Dt({hasNextPage:d,variables:y(x({},r),{offset:v.length+1,limit:10}),fetchMore:b});return c.exports.useEffect(()=>{l&&(o(["messages","getAttachmentsInConversation"]),a(!1))},[l,o,a]),c.exports.useEffect(()=>{var D,N;(v==null?void 0:v.length)>0&&n((N=(D=v==null?void 0:v[0])==null?void 0:D.conversation)==null?void 0:N.id)},[v]),{data:u,loading:g,isLoadMore:A,loadMore:h,hasNextPage:d,messages:v,error:m,totalDocs:(p=u==null?void 0:u.messages)==null?void 0:p.totalDocs}},Qc=({userId:e,options:s})=>{var v,d,h;const o=V(A=>A.conversationId),l=V(A=>A.beforeId),a=V(A=>A.afterId),i=x({},be({conversationId:o,userId:o?"":e==null?void 0:e.toString(),limit:10,before:l,after:a},A=>A)),{data:n,loading:r,error:u}=De(Ze,y(x({variables:x({},i)},s),{skip:!l&&!a})),g=((v=n==null?void 0:n.messages)==null?void 0:v.docs)||[],b=(d=n==null?void 0:n.messages)==null?void 0:d.hasNextPage,m=(h=n==null?void 0:n.messages)==null?void 0:h.hasPrevPage;return{data:n,loading:r,hasNextPage:b,hasPrevPage:m,messages:g,error:u}},Fl=({conversationId:e="",search:s})=>{var A,E;const o=V(T=>T.setSearchResult),l=V(T=>T.messageId),a=V(T=>T.beforeId),[i,n]=c.exports.useState(!1),{refetch:r,called:u,loading:g}=De(Fn,{variables:{conversationId:e,search:s},onCompleted:T=>{var p;o((p=T==null?void 0:T.searchMessage)==null?void 0:p.docs),n(!0)},onError:()=>{o([])},skip:e.length===0||!!l||!!a||!s}),{data:b,refetch:m,loading:v}=De(Ns,{variables:{messageId:l,limit:10},skip:!l}),d=(A=b==null?void 0:b.nearbyMessages)==null?void 0:A.hasPrevPage,h=(E=b==null?void 0:b.nearbyMessages)==null?void 0:E.hasNextPage;return c.exports.useEffect(()=>{(u&&s.length||u&&i&&(s==null?void 0:s.length)===0)&&r()},[s,i,u]),c.exports.useEffect(()=>{u&&l&&m()},[l,u]),{loading:v,hasPrevPageNearbyMessages:d,hasNextPageNearbyMessages:h,getSearchResultLoading:g}};var Ye="/Users/zimdev/work/zim-app/src/pages/chat/boxMessage/components/chatContent.tsx";const Zc=({conversationId:e,ownerId:s,userId:o,onSendMessage:l,participants:a,isGroup:i,setConversationId:n,channel:r,sendMessageLoading:u})=>{var oe,qe,Oe;const g=Vi(),b=c.exports.useRef(0),m=c.exports.useRef(0),v=c.exports.useRef(null),d=c.exports.useCallback(async()=>{u||await l({nativeEvent:{text:"Hi !!"},conversationId:e})},[l,e,u]),[h,A]=c.exports.useState(!1),E=V(J=>J.searchStringMessage),T=V(J=>J.setBeforeId),p=V(J=>J.setAfterId),D=V(J=>J.messageId),N=V(J=>J.beforeId),$=V(J=>J.afterId),{messages:k,loading:C,error:F,loadMore:Z,isLoadMore:z,hasNextPage:G,totalDocs:P}=Wc({userId:o}),{hasPrevPageNearbyMessages:H,hasNextPageNearbyMessages:I,getSearchResultLoading:B}=Fl({conversationId:e,search:E}),{hasNextPage:W,hasPrevPage:re}=Qc({userId:o,options:{onCompleted:J=>{var Ot,Lt,Bt,Rt,Ut;A(!0);const Ee=(Ot=g.cache.readQuery({query:Ns,variables:{messageId:D,limit:10}}))!=null?Ot:{},it=(Bt=(Lt=Ee==null?void 0:Ee.nearbyMessages)==null?void 0:Lt.docs)!=null?Bt:[],R=(Ut=(Rt=J==null?void 0:J.messages)==null?void 0:Rt.docs)!=null?Ut:[],fe=N?[...it,...R]:[...R,...it],lt=ps(fe,"id");m.current=v.current.scrollHeight,g.cache.writeQuery({query:Ns,variables:{messageId:D,limit:10},data:y(x({},Ee),{nearbyMessages:y(x({},Ee==null?void 0:Ee.nearbyMessages),{docs:lt})})}),A(!1),N&&(v.current.scrollTop=v.current.scrollHeight-m.current)}}}),w=(oe=g.cache.readQuery({query:Ns,variables:{messageId:D,limit:10}}))!=null?oe:{},O=(Oe=(qe=w==null?void 0:w.nearbyMessages)==null?void 0:qe.docs)!=null?Oe:[],se=(E==null?void 0:E.length)>0&&(D==null?void 0:D.length)>0?O:k,ze=c.exports.useCallback(()=>{var J;p(""),T((J=O==null?void 0:O[O.length-1])==null?void 0:J.id)},[O]),$e=c.exports.useCallback(()=>{var J;T(""),p((J=O==null?void 0:O[0])==null?void 0:J.id)},[O]),Ie=c.exports.useCallback(async J=>{const{scrollTop:Ee}=J.nativeEvent.target;Ee<=20&&Ee<b.current&&G&&!z&&!E&&(await Z(),z||(v.current.scrollTop=Ee+60)),b.current=Ee},[G,z,E]);return F?t.exports.jsxDEV(q,{bg:"blackAlpha.400",h:"100%",justifyContent:"center",children:t.exports.jsxDEV(ls,{color:"white"},void 0,!1,{fileName:Ye,lineNumber:172,columnNumber:9},globalThis)},void 0,!1,{fileName:Ye,lineNumber:171,columnNumber:7},globalThis):t.exports.jsxDEV(S,{ref:v,bg:j("white","#10172a"),h:"full",w:"full",id:"chat-box",overflow:"auto",position:"relative",onScroll:Ie,children:(se==null?void 0:se.length)===0&&!B?t.exports.jsxDEV(ie,{w:"100%",h:"100%",display:"flex",flexDirection:"column",alignItems:"center",justifyContent:"center",spacing:8,children:[t.exports.jsxDEV(ie,{w:"40%",spacing:4,maxW:"360px",maxH:"400px",children:t.exports.jsxDEV(rt,{src:Al,objectFit:"contain"},void 0,!1,{fileName:Ye,lineNumber:199,columnNumber:13},globalThis)},void 0,!1,{fileName:Ye,lineNumber:198,columnNumber:11},globalThis),(e||o)&&(se==null?void 0:se.length)===0&&!C&&!u&&t.exports.jsxDEV(S,{h:10,px:2,bg:"red.400",rounded:"lg",as:"button",onClick:d,fontSize:14,color:"white",fontWeight:500,children:"G\u1EEDi l\u1EDDi ch\xE0o"},void 0,!1,{fileName:Ye,lineNumber:206,columnNumber:15},globalThis)]},void 0,!0,{fileName:Ye,lineNumber:189,columnNumber:9},globalThis):B?t.exports.jsxDEV(ls,{},void 0,!1,{fileName:Ye,lineNumber:222,columnNumber:9},globalThis):t.exports.jsxDEV(S,{h:"full",w:"full",children:t.exports.jsxDEV(Hc,{totalDocs:P,chatBoxRef:v,hasBeforeLoadMoreButton:(!N&&H||re)&&(E==null?void 0:E.length)>0,hasAfterLoadMoreButton:(!$&&I||W)&&(E==null?void 0:E.length)>0,onLoadBeforeMessage:ze,onLoadAfterMessage:$e,isLoadMore:z,searchLoading:h,loading:C,isGroup:i,ownerId:s,messages:se,conversationId:e,participants:a,userId:o,setConversationId:n,channel:r},void 0,!1,{fileName:Ye,lineNumber:225,columnNumber:11},globalThis)},void 0,!1,{fileName:Ye,lineNumber:224,columnNumber:9},globalThis)},void 0,!1,{fileName:Ye,lineNumber:178,columnNumber:5},globalThis)};f`
  mutation updateMedia($id: ObjectID!, $input: UpdateMediaInput!)
  @api(name: "zim") {
    updateMedia(id: $id, input: $input) {
      id
      title
    }
  }
`;f`
  mutation deleteMedia($id: ObjectID!) @api(name: "zim") {
    deleteMedia(id: $id)
  }
`;f`
  mutation uploadImages($file: [Upload]) @api(name: "appZim") {
    uploadImages(file: $file)
  }
`;const Yc=f`
  mutation singleUpload($file: Upload!) @api(name: "chat") {
    singleUpload(file: $file) {
      id
      path
      fullUrl
      type
    }
  }
`,dp=f`
  mutation uploadExamDocument($file: Upload!, $folderName: String)
  @api(name: "chat") {
    uploadExamDocument(file: $file, folderName: $folderName) {
      id
      path
      fullUrl
      type
    }
  }
`,Kc=({setMediaUrls:e})=>{const[s,{loading:o}]=Pe(Yc),l=c.exports.useCallback(async i=>{var r,u,g;const n=Pi();try{await e.push({fileId:n,url:URL.createObjectURL(i),name:i.name,loading:!0,progress:0,type:((r=i==null?void 0:i.type)==null?void 0:r.includes("video"))?"video":((u=i==null?void 0:i.type)==null?void 0:u.includes("image"))?"image":"file"});const{data:b}=await s({variables:{file:i},context:{fetchOptions:{onUploadProgress:m=>{var v,d;e.update(h=>h.fileId===n,{fileId:n,url:URL.createObjectURL(i),name:i.name,loading:!0,progress:m.loaded/m.total,type:((v=i==null?void 0:i.type)==null?void 0:v.includes("video"))?"video":((d=i==null?void 0:i.type)==null?void 0:d.includes("image"))?"image":"file"})}}}});await e.update(m=>m.fileId===n,{fileId:b.singleUpload.id,name:b.singleUpload.path,url:URL.createObjectURL(i),type:i.type.includes("video")?"video":((g=i==null?void 0:i.type)==null?void 0:g.includes("image"))?"image":"file"})}catch(b){console.log(b)}},[s]);return{doUploadImage:c.exports.useCallback(async i=>{await Promise.all(i.map(l))},[l]),loading:o}};var Te="/Users/zimdev/work/zim-app/src/pages/chat/boxMessage/components/enterMessage.tsx";const Jc=xe(()=>Ne(()=>import("./index29.js").then(function(e){return e.i}),["assets/index29.js","assets/vendor.js"])),Xc=1048576*2,em=({conversationId:e,setConversationId:s,userId:o,group:l,textAreaRef:a,file:i,setFile:n,conversationDetail:r,isMyTyping:u})=>{var g,b,m,v;{const d=Je(),[h]=ne(),A=c.exports.useRef(null),E=j("gray.900","slate.300"),[T,p]=c.exports.useState(!1),[D,N]=c.exports.useState(!1),[$,k]=c.exports.useState(""),[C,F]=c.exports.useState(!1),{onSendMessage:Z,onTyping:z}=go({setContent:k,userId:o,group:l,setConversationId:s}),G=((b=(g=r==null?void 0:r.usersTyping)==null?void 0:g.filter(w=>w!==(h==null?void 0:h.id)))==null?void 0:b.length)>0,P=(v=(m=r==null?void 0:r.usersTyping)==null?void 0:m.filter(w=>w!==h.id))==null?void 0:v.map(w=>{var O;return(O=r==null?void 0:r.participants.find(se=>se.userId===w))==null?void 0:O.fullName}),{doUploadImage:H}=Kc({setMediaUrls:n}),{getRootProps:I,getInputProps:B}=Fr({accept:["image/*","application/vnd.ms-excel","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet","application/vnd.openxmlformats-officedocument.presentationml.presentation","application/pdf","application/xhtml+xml","application/xml","application/msword","application/vnd.openxmlformats-officedocument.wordprocessingml.document","application/zip"],multiple:!1,onDrop:(w,O)=>{var $e;const se=(O==null?void 0:O.length)>0,ze=O.length>0&&(($e=O[0].errors)==null?void 0:$e.find(Ie=>Ie.code==="file-too-large"));se?d({title:"Th\u1EA5t b\u1EA1i!",description:ze?Wm:Qm,status:"error",duration:3e3,isClosable:!0}):H(w).then()},minSize:0,maxSize:Xc}),W=c.exports.useCallback(async w=>{const O=w.target.value.trim();if(w.key==="Enter"&&!w.shiftKey)(O.length!==0||(i==null?void 0:i.length)>0)&&(C&&F(!1),await Z({nativeEvent:{text:O},conversationId:e}),F(!1),k(""));else return!1},[e,$,C,i,a]),re=()=>D&&t.exports.jsxDEV(to,{_focus:{outline:"none"},onFocus:()=>{p(!0)},onBlur:()=>{p(!1)},as:so,style:{resize:"none"},ref:a,minRows:1,maxRows:5,rows:1,value:$,onChange:async w=>{w.target.value!==`
`&&await k(w.target.value)},border:"none",placeholder:"Nh\u1EADp n\u1ED9i dung ...",onKeyPress:W,color:E,bg:"transparent",mr:4},void 0,!1,{fileName:Te,lineNumber:136,columnNumber:11},globalThis);return Mr(()=>{D&&async function(){T&&await z("typingOn",e),T||await z("typingOff",e)}()},[T,e,z,D,a]),c.exports.useEffect(()=>{let w;return u&&(w=setTimeout(()=>{p(!1)},15e3)),()=>{clearTimeout(w)}},[u]),c.exports.useEffect(()=>{N(!0)},[]),c.exports.useEffect(()=>{const w=O=>{A.current&&!A.current.contains(O.target)&&F(!1)};return document.addEventListener("click",w,!0),()=>{document.removeEventListener("click",w,!0)}},[F]),t.exports.jsxDEV(S,{bg:j("white",""),position:"sticky",bottom:0,w:"100%",display:"flex",flexDirection:"column",borderTopWidth:1,borderColor:"var(--chakra-colors-whiteAlpha-300)",p:2,children:[G&&t.exports.jsxDEV(q,{w:"50%",px:2,rounded:4,mb:2,children:[t.exports.jsxDEV(M,{fontSize:13,color:j("gray.700","white"),children:P==null?void 0:P.map(w=>w||"Someone is Typing").join(",")},void 0,!1,{fileName:Te,lineNumber:222,columnNumber:13},globalThis),t.exports.jsxDEV(S,{display:"flex",alignItems:"center",justifyContent:"center",w:10,h:5,children:t.exports.jsxDEV(Nl,{},void 0,!1,{fileName:Te,lineNumber:234,columnNumber:15},globalThis)},void 0,!1,{fileName:Te,lineNumber:227,columnNumber:13},globalThis)]},void 0,!0,{fileName:Te,lineNumber:221,columnNumber:11},globalThis),t.exports.jsxDEV(q,{w:"full",children:[t.exports.jsxDEV(S,y(x({as:"div"},I({className:"dropzone"})),{children:[t.exports.jsxDEV("input",y(x({},B()),{style:{display:"none"},type:"file"}),void 0,!1,{fileName:Te,lineNumber:240,columnNumber:13},globalThis),t.exports.jsxDEV(qr,{color:j("#DA0037","white"),className:"w-5 h-5"},void 0,!1,{fileName:Te,lineNumber:245,columnNumber:13},globalThis)]}),void 0,!0,{fileName:Te,lineNumber:239,columnNumber:11},globalThis),t.exports.jsxDEV(S,{w:"100%",display:"flex",flexDirection:"row",borderWidth:1,rounded:"lg",borderColor:j("gray.300","var(--chakra-colors-whiteAlpha-300)"),p:2,children:[re(),t.exports.jsxDEV(q,{display:"flex",alignItems:"center",children:[t.exports.jsxDEV(S,{as:"button",onClick:()=>F(!C),children:t.exports.jsxDEV(qi,{color:j("#DA0037","white"),className:"w-5 h-5 "},void 0,!1,{fileName:Te,lineNumber:268,columnNumber:17},globalThis)},void 0,!1,{fileName:Te,lineNumber:264,columnNumber:15},globalThis),t.exports.jsxDEV(S,{cursor:"pointer",pointerEvents:!$&&(i==null?void 0:i.length)===0?"none":"auto",onClick:async()=>{$.trim().length!==0||(i==null?void 0:i.length)>0?(C&&F(!1),await Z({nativeEvent:{text:$},conversationId:e})):k("")},children:t.exports.jsxDEV(Gi,{fill:j(($==null?void 0:$.length)>0||(i==null?void 0:i.length)>0?"#DA0037":"#686868","white"),className:"w-5 h-5"},void 0,!1,{fileName:Te,lineNumber:291,columnNumber:17},globalThis)},void 0,!1,{fileName:Te,lineNumber:274,columnNumber:15},globalThis)]},void 0,!0,{fileName:Te,lineNumber:263,columnNumber:13},globalThis)]},void 0,!0,{fileName:Te,lineNumber:250,columnNumber:11},globalThis),C&&t.exports.jsxDEV(S,{ref:A,color:"slate.900",children:t.exports.jsxDEV(Jc,{disableSkinTonePicker:!0,pickerStyle:{position:"absolute",bottom:"70px",right:"20px",overflow:"hidden",backgroundColor:"white",zIndex:"1000"},onEmojiClick:(w,O)=>{k(`${$} ${O.emoji}`)}},void 0,!1,{fileName:Te,lineNumber:306,columnNumber:15},globalThis)},void 0,!1,{fileName:Te,lineNumber:305,columnNumber:13},globalThis)]},void 0,!0,{fileName:Te,lineNumber:238,columnNumber:9},globalThis)]},void 0,!0,{fileName:Te,lineNumber:209,columnNumber:7},globalThis)}},tm=e=>{const s=Je(),[o,{loading:l}]=Pe(vu);return{onMarkDone:c.exports.useCallback(async()=>{try{await o({variables:{conversationId:e}}),await s({title:"Th\xE0nh c\xF4ng.",description:"\u0110\xE3 chuy\u1EC3n user sang tr\u1EA1ng th\xE1i done",status:"success",duration:2e3,isClosable:!0})}catch(i){s({title:"L\u1ED7i !!!",description:i.message,status:"success",duration:2e3,isClosable:!0})}},[e,o,s]),loading:l}};var we="/Users/zimdev/work/zim-app/src/pages/chat/boxMessage/components/boxMessageHeader.tsx";const sm=o=>{var l=o,{onChange:e}=l,s=Ue(l,["onChange"]);const[a,i]=c.exports.useState(""),n=is(a,1e3);return c.exports.useEffect(()=>{e(n)},[n]),t.exports.jsxDEV(ms,y(x({},s),{value:a,onChange:r=>i(r.target.value)}),void 0,!1,{fileName:we,lineNumber:53,columnNumber:5},globalThis)},Ml=e=>{const i=e,{Icon:s,onClick:o,disabled:l}=i,a=Ue(i,["Icon","onClick","disabled"]);return t.exports.jsxDEV(S,y(x({_hover:{backgroundColor:"#0085E4"},as:"button",display:"flex",alignItems:"center",justifyContent:"center",width:10,rounded:"4px",h:"100%",onClick:o,disabled:l},a),{children:t.exports.jsxDEV(s,{},void 0,!1,{fileName:we,lineNumber:77,columnNumber:7},globalThis)}),void 0,!1,{fileName:we,lineNumber:64,columnNumber:5},globalThis)},om=({userInfo:e,groupInfo:s,loading:o,participants:l})=>{var k;const[a]=ne(),[i,n]=c.exports.useState(!1),r=V(C=>C.searchStringMessage),u=V(C=>C.setSearchStringMessage),g=V(C=>C.visibleUserProfile),b=V(C=>C.conversationId),m=V(C=>C.setSearchResult),v=V(C=>C.setNearSearchResult),d=V(C=>C.setVisibleUserProfile),h=V(C=>C.setBeforeId),A=V(C=>C.setAfterId),E=V(C=>C.setMessageId),T=Jt({base:"",xl:"visible"}),p=Jt({base:"",md:"visible"}),{onMarkDone:D,loading:N}=tm(b);Fl({conversationId:b,search:r});const $=c.exports.useMemo(()=>o?"\u0110ang t\u1EA3i ...":(s==null?void 0:s.name)?s==null?void 0:s.name:(e==null?void 0:e.full_name)||(e==null?void 0:e.fullName)||"\u1EA8n danh",[o,s==null?void 0:s.name,e==null?void 0:e.full_name,e==null?void 0:e.fullName]);return t.exports.jsxDEV(S,{w:"100%",minH:"35px",maxH:"50px",display:"flex",flexDirection:"row",alignItems:"center",borderBottomWidth:1,pl:2,py:3,zIndex:1,children:[!i&&t.exports.jsxDEV(q,{id:"js-toggle-profile",flex:1,alignItems:"center",cursor:"pointer",onClick:()=>!T&&d(!g),children:[e&&at.exports.isEmpty(s)?t.exports.jsxDEV(He,{boxSize:"2em",src:e==null?void 0:e.avatar,name:$},void 0,!1,{fileName:we,lineNumber:141,columnNumber:13},globalThis):t.exports.jsxDEV(El,{groupInfo:s==null?void 0:s.participants},void 0,!1,{fileName:we,lineNumber:147,columnNumber:13},globalThis),t.exports.jsxDEV(S,{flex:1,maxW:"300px",children:t.exports.jsxDEV(M,{noOfLines:1,fontSize:14,lineHeight:"24px",fontWeight:"medium",color:j("gray.800","white"),textTransform:"capitalize",children:$},void 0,!1,{fileName:we,lineNumber:150,columnNumber:13},globalThis)},void 0,!1,{fileName:we,lineNumber:149,columnNumber:11},globalThis)]},void 0,!0,{fileName:we,lineNumber:131,columnNumber:9},globalThis),t.exports.jsxDEV(q,{w:i?"full":"",h:"100%",pr:i?2:4,spacing:0,minH:10,children:[[6].includes(parseInt(a==null?void 0:a.roleId,10))&&(l==null?void 0:l.find(C=>C.userId===a.id.toString()))&&!i&&((e==null?void 0:e.isGuest)||((k=s==null?void 0:s.participants)==null?void 0:k.find(C=>C.isGuest)))&&t.exports.jsxDEV(Ml,{onClick:D,disabled:N,Icon:zr},void 0,!1,{fileName:we,lineNumber:175,columnNumber:13},globalThis),!i&&p&&t.exports.jsxDEV(Ml,{onClick:()=>{n(!0)},Icon:Or},void 0,!1,{fileName:we,lineNumber:182,columnNumber:11},globalThis),i&&t.exports.jsxDEV(gi,{children:[t.exports.jsxDEV(sm,{type:"text",placeholder:"Nh\u1EADp g\xEC \u0111\xF3 ...",onChange:u,pr:"4.5rem"},void 0,!1,{fileName:we,lineNumber:191,columnNumber:13},globalThis),t.exports.jsxDEV(hi,{width:"4.5rem",children:t.exports.jsxDEV(Y,{h:"1.75rem",size:"sm",onClick:()=>{n(!1),u(""),m([]),v([]),E(""),h(""),A("")},children:i?"Close":"Open"},void 0,!1,{fileName:we,lineNumber:198,columnNumber:15},globalThis)},void 0,!1,{fileName:we,lineNumber:197,columnNumber:13},globalThis)]},void 0,!0,{fileName:we,lineNumber:190,columnNumber:11},globalThis)]},void 0,!0,{fileName:we,lineNumber:163,columnNumber:7},globalThis)]},void 0,!0,{fileName:we,lineNumber:118,columnNumber:5},globalThis)},ql=(e,s)=>{var n;const{data:o,loading:l,error:a}=De(wn,{variables:be({id:e,userId:e?"":s?s==null?void 0:s.toString():""},r=>r),skip:!e&&!s});return{conversationDetail:(n=o==null?void 0:o.conversationDetail)!=null?n:{},loading:l,error:a}},zl=(e="",s)=>{var i;const{data:o,loading:l}=De(tl,{variables:{userId:Number(e)},skip:s||!e||isNaN(Number(e))});return{userData:(i=o==null?void 0:o.getUserById)!=null?i:{},loading:l}},Ol=({conversationDetail:e})=>{var o,l,a,i;const[s]=ne();return!((o=e==null?void 0:e.participants)==null?void 0:o.find(n=>n.userId===(s==null?void 0:s.id)))&&((l=e==null?void 0:e.participants)==null?void 0:l.length)>=2||((a=e==null?void 0:e.participants)==null?void 0:a.find(n=>n.userId===(s==null?void 0:s.id)))&&((i=e==null?void 0:e.participants)==null?void 0:i.length)>=3},im="_sm_183st_1",lm="_base_183st_6",am="_md_183st_10";var xo={sm:im,base:lm,md:am},Is="/Users/zimdev/work/zim-app/src/components/Button/index.tsx";const Ll={sm:xo.sm,base:xo.base,md:xo.md},Bl={primary:"bg-red-700 border-red-700 dark:bg-red-700 hover:border-red-900 hover:bg-red-900 dark:hover:bg-red-900 dark:border-red-500  hover:text-white ring-red-400 text-white ",info:"bg-blue-500 border-blue-500 hover:border-blue-700 hover:bg-blue-700 hover:text-white ring-blue-400 text-white",white:"bg-white border-transparent hover:border-red-700 hover:bg-red-700 hover:text-white ring-red-400 text-black",warning:"bg-yellow-500 hover:bg-yellow-700 border-yellow-500 hover:border-yellow-700 ring-yellow-400 text-black",success:"bg-green-500 hover:bg-green-700 ring-green-400 text-white border-green-500 hover:border-green-700","primary-outline":"border border-red-700 hover:bg-red-700 hover:text-white ring-red-400 text-red-700 dark:text-red-300 dark:border-red-300 dark:hover:bg-red-300 dark:hover:text-slate-300","info-outline":"border border-blue-700 hover:bg-blue-700 hover:text-white ring-blue-400 text-blue-700","success-outline":"border border-green-700 hover:bg-green-700 hover:text-white ring-green-400 text-green-700","warning-outline":"border border-yellow-700 text-yellow-700 hover:bg-yellow-700 hover:text-black ring-yellow-400","black-outline":"border border-black hover:bg-gray-50 text-black hover:text-black ring-gray-700 dark:border-slate-500 dark:text-slate-300 dark:hover:text-slate-900",gray:"bg-gray-200 hover:bg-gray-600 ring-gray-400 text-gray-900 hover:text-white dark:bg-slate-600 dark:text-slate-300 dark:hover:bg-slate-700 dark:border-slate-700",ghost:"hover:bg-gray-100 ring-gray-400 border-transparent hover:border-transparent dark:bg-slate-600 dark:text-slate-300 dark:hover:bg-slate-700"},Rl=(e,s)=>e.match(s),rm=c.exports.forwardRef((u,r)=>{var g=u,{className:e="",buttonType:s=Bl.primary,buttonSize:o=Ll.base,isLoading:l=!1,isDisabled:a=!1,children:i}=g,n=Ue(g,["className","buttonType","buttonSize","isLoading","isDisabled","children"]);const b=c.exports.useMemo(()=>Bl[s],[s]),m=c.exports.useMemo(()=>Ll[o],[o]),v=c.exports.useMemo(()=>l||a,[l,a]);return t.exports.jsxDEV("button",y(x({disabled:a||l,ref:r,className:`whitespace-nowrap border inline-flex items-center justify-center transition-colors transition-background duration-300 ${Rl(e,"p-")?"":"py-2 px-4"} rounded focus:outline-none ring-opacity-75  focus:ring ${Rl(e,"text-")?"":"text-base"} font-medium ${v&&"opacity-60 cursor-not-allowed"} ${b} ${e} ${m}`},n),{children:[l&&t.exports.jsxDEV("svg",{className:"animate-spin -ml-1 mr-3 h-5 w-5 text-current",xmlns:"http://www.w3.org/2000/svg",fill:"none",viewBox:"0 0 24 24",children:[t.exports.jsxDEV("circle",{className:"opacity-25",cx:"12",cy:"12",r:"10",stroke:"currentColor",strokeWidth:"4"},void 0,!1,{fileName:Is,lineNumber:97,columnNumber:13},globalThis),t.exports.jsxDEV("path",{className:"opacity-100",fill:"currentColor",d:"M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"},void 0,!1,{fileName:Is,lineNumber:105,columnNumber:13},globalThis)]},void 0,!0,{fileName:Is,lineNumber:91,columnNumber:11},globalThis),i]}),void 0,!0,{fileName:Is,lineNumber:78,columnNumber:7},globalThis)});var je="/Users/zimdev/work/zim-app/src/pages/chat/boxMessage/components/uploadFileContent.tsx";const nm=c.exports.memo(({showUploadFileDialog:e,file:s,setFile:o,setShowUploadFileDialog:l,onSendMessage:a})=>{const i=mt(),n=V(u=>u.conversationId),r=c.exports.useCallback(async()=>{await a({nativeEvent:{text:""},conversationId:n,file:s}),await i(["getAttachmentsInConversation"]),await o.clear()},[n,s]);return t.exports.jsxDEV(Lr,{appear:!0,show:e,as:"div",enter:"transition-opacity duration-75",enterFrom:"opacity-0",enterTo:"opacity-100",leave:"transition-opacity duration-150",leaveFrom:"opacity-100",leaveTo:"opacity-0",children:t.exports.jsxDEV(S,{p:2,zIndex:1e3,position:"absolute",bg:j("white","slate.600"),rounded:"lg",display:"flex",flexDirection:"column",justifyContent:"space-between",w:"50%",h:"40%",maxH:"300px",shadow:"md",className:"translate-y-[90%] translate-x-[50%] inset-0 ",children:s==null?void 0:s.map((u,g)=>t.exports.jsxDEV(At,{flexDirection:"column",display:"flex",justifyContent:"space-between",w:"full",h:"100%",overflow:"hidden",rounded:"lg",children:[(u==null?void 0:u.type)!=="file"?t.exports.jsxDEV(S,{maxH:"75%",children:t.exports.jsxDEV(rt,{w:"full",h:"full",src:u==null?void 0:u.url,alt:"file",objectFit:"contain"},void 0,!1,{fileName:je,lineNumber:81,columnNumber:21},globalThis)},void 0,!1,{fileName:je,lineNumber:80,columnNumber:19},globalThis):t.exports.jsxDEV(ve,{display:"flex",maxH:"75%",h:"full",alignItems:"center",justifyContent:"center",children:t.exports.jsxDEV(ie,{children:[t.exports.jsxDEV(S,{w:14,h:14,rounded:"full",display:"flex",alignItems:"center",children:t.exports.jsxDEV(_i,{className:"w-[40px] h-[40px]"},void 0,!1,{fileName:je,lineNumber:105,columnNumber:25},globalThis)},void 0,!1,{fileName:je,lineNumber:98,columnNumber:23},globalThis),t.exports.jsxDEV(M,{textAlign:"center",flex:1,color:j("gray.800","white"),fontSize:14,children:ho(u==null?void 0:u.name)},void 0,!1,{fileName:je,lineNumber:107,columnNumber:23},globalThis)]},void 0,!0,{fileName:je,lineNumber:97,columnNumber:21},globalThis)},void 0,!1,{fileName:je,lineNumber:90,columnNumber:19},globalThis),t.exports.jsxDEV(S,{onClick:()=>{o.removeAt(g),l(!1)},top:-2,right:-1,rounded:"full",cursor:"pointer",position:"absolute",zIndex:20,bg:j("white","#10172a"),children:t.exports.jsxDEV(Br,{onClick:()=>{o.removeAt(g),l(!1)},width:32,className:"text-gray-400 hover:text-red-400"},void 0,!1,{fileName:je,lineNumber:131,columnNumber:19},globalThis)},void 0,!1,{fileName:je,lineNumber:118,columnNumber:17},globalThis),t.exports.jsxDEV(ve,{display:"flex",flexDirection:"column",height:"25%",justifyContent:"flex-end",children:[t.exports.jsxDEV(S,{h:"18%",w:"100%",children:t.exports.jsxDEV(Rr,{colorScheme:(u==null?void 0:u.progress)<1?"":"green",hasStripe:(u==null?void 0:u.progress)<1,mt:2,value:(u==null?void 0:u.progress)*100},void 0,!1,{fileName:je,lineNumber:147,columnNumber:21},globalThis)},void 0,!1,{fileName:je,lineNumber:146,columnNumber:19},globalThis),t.exports.jsxDEV(Ur,{},void 0,!1,{fileName:je,lineNumber:154,columnNumber:19},globalThis),t.exports.jsxDEV(rm,{disabled:(u==null?void 0:u.progress)<1,onClick:r,buttonType:"info",className:"w-full text-center py-2 rounded-lg text-white mt-2 max-h-[40px]",children:"G\u1EEDi"},void 0,!1,{fileName:je,lineNumber:155,columnNumber:19},globalThis)]},void 0,!0,{fileName:je,lineNumber:140,columnNumber:17},globalThis)]},g,!0,{fileName:je,lineNumber:69,columnNumber:15},globalThis))},void 0,!1,{fileName:je,lineNumber:52,columnNumber:9},globalThis)},void 0,!1,{fileName:je,lineNumber:41,columnNumber:7},globalThis)});var qt="/Users/zimdev/work/zim-app/src/pages/chat/boxMessage/index.tsx";const um=(e=[],s)=>{if((e==null?void 0:e.length)<2)return!0;if((e==null?void 0:e.length)>=2)return!!(e==null?void 0:e.find(o=>o.userId===s))},cm=()=>{var D,N,$,k;const e=V(C=>C.channel),s=V(C=>C.userId),o=V(C=>C.conversationId),l=V(C=>C.setConversationId),a=V(C=>C.searchStringMessage),i=c.exports.useRef(null),[n]=ne(),[r,u]=Gr([]),[g,b]=c.exports.useState(!1),{userData:m,loading:v}=zl(s,!s),{conversationDetail:d,loading:h}=ql(o,s),A=((D=d==null?void 0:d.participants)==null?void 0:D.filter(C=>(C==null?void 0:C.userId)!==(n==null?void 0:n.id.toString()))[0])||m,E=Ol({conversationDetail:d}),{onSendMessage:T,loading:p}=go({setContent:()=>{},userId:s,group:e,setConversationId:l});return c.exports.useEffect(()=>{(r==null?void 0:r.length)>0?b(!0):b(!1)},[r]),t.exports.jsxDEV(S,{bg:j("white","#10172a"),display:"flex",flexDirection:"column",flex:1,w:"full",position:"relative",h:"full",children:[(o||s)&&t.exports.jsxDEV(om,{participants:d==null?void 0:d.participants,userInfo:A,groupInfo:E&&o?{name:(d==null?void 0:d.name)?d==null?void 0:d.name:E&&o?($=(N=d==null?void 0:d.participants)==null?void 0:N.map(C=>C.fullName))==null?void 0:$.join(","):"",participants:d==null?void 0:d.participants}:null,loading:v||h},void 0,!1,{fileName:qt,lineNumber:73,columnNumber:9},globalThis),t.exports.jsxDEV(Zc,{isGroup:d==null?void 0:d.name,ownerId:n==null?void 0:n.id,conversationId:o,userId:s,onSendMessage:T,sendMessageLoading:p,participants:d==null?void 0:d.participants,setConversationId:l,channel:e},void 0,!1,{fileName:qt,lineNumber:93,columnNumber:7},globalThis),t.exports.jsxDEV(nm,{onSendMessage:T,file:r,setFile:u,showUploadFileDialog:g,setShowUploadFileDialog:b},void 0,!1,{fileName:qt,lineNumber:104,columnNumber:7},globalThis),(um(d==null?void 0:d.participants,n==null?void 0:n.id)&&(o||s)||E&&Jl.includes(n==null?void 0:n.roleId))&&!a&&!h&&t.exports.jsxDEV(em,{conversationDetail:d,file:r,setFile:u,textAreaRef:i,conversationId:o,setConversationId:l,userId:s,group:e,isMyTyping:(k=d==null?void 0:d.usersTyping)==null?void 0:k.includes(n==null?void 0:n.id)},void 0,!1,{fileName:qt,lineNumber:116,columnNumber:11},globalThis),(r==null?void 0:r.length)>0&&t.exports.jsxDEV(S,{opacity:.4,zIndex:10,position:"absolute",bg:j("black","slate.300"),className:"inset-0"},void 0,!1,{fileName:qt,lineNumber:129,columnNumber:9},globalThis)]},void 0,!0,{fileName:qt,lineNumber:63,columnNumber:5},globalThis)};var No="/Users/zimdev/work/zim-app/src/pages/chat/components/basicInfo.tsx";const mm=({participantAvatarUrl:e,fullName:s="",onClose:o,loading:l})=>t.exports.jsxDEV(S,{flex:1,display:"flex",alignItems:"center",cursor:"pointer",onClick:()=>o?o():()=>{},children:[t.exports.jsxDEV(He,{boxSize:"2em",src:e,name:s},void 0,!1,{fileName:No,lineNumber:26,columnNumber:7},globalThis),t.exports.jsxDEV(M,{ml:2,flex:1,noOfLines:1,fontSize:14,lineHeight:"24px",fontWeight:"medium",color:j("gray.800","slate.300"),children:l?"\u0110ang t\u1EA3i ...":s||"\u1EA8n danh"},void 0,!1,{fileName:No,lineNumber:27,columnNumber:7},globalThis)]},void 0,!0,{fileName:No,lineNumber:19,columnNumber:5},globalThis),dm=/^(?=.*\d)(?=.*[a-zA-Z])[\da-zA-Z_.\-@]{8,}$/,pm=/^\d+$/,Ul=/(84|03|05|07|08|09|01[2|6|8|9])+([0-9]{8})\b/,pp=/^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w.-]+)+[\w\-._~:/?#[\]@!$&'()*+,;=.]+$/gm,bm=e=>{if(typeof e!="string")return"";const s=e.split(".");return s.length>0?s[s.length-1]:""};_t(oo,"password",function(e){return this.matches(dm,{message:e,excludeEmptyString:!0})});_t(oo,"onlyNumber",function(e){return this.matches(pm,{message:e,excludeEmptyString:!0})});_t(oo,"phone",function(e){return this.matches(Ul,{message:e,excludeEmptyString:!0})});_t(io,"fileSize",function(e,s){return this.test("fileSize",s,o=>Array.isArray(o)&&o.length>0&&o[0]?!o.map(a=>a.size>e):!0)});_t(io,"fileType",function(e=[],s){return this.test("fileType",s,o=>e.length===0?!0:Array.isArray(o)&&o.length>0&&o[0]?!o.map(a=>e.includes(a.type)):!0)});_t(io,"file",function({size:e,type:s},o){return this.test({name:"file",test:function(l){var n,r,u,g,b,m;const a=(n=s==null?void 0:s.value)!=null?n:[],i=(r=e==null?void 0:e.value)!=null?r:5*1024*1e3;if(Array.isArray(l)&&l.length>0&&l[0]){const v=l.find(h=>!a.includes(h.type)),d=l.find(h=>h.size>i);return v?this.createError({message:`${(u=s==null?void 0:s.message)!=null?u:"Kh\xF4ng h\u1ED7 tr\u1EE3 \u0111\u1ECBnh d\u1EA1ng"} ${bm((g=v==null?void 0:v.name)!=null?g:"")}`,path:this.path}):d?this.createError({message:(m=(b=e==null?void 0:e.message)!=null?b:`Dung l\u01B0\u1EE3ng v\u01B0\u1EE3t qu\xE1 ${i/1024e3}`+(d==null?void 0:d.name))!=null?m:"",path:this.path}):!0}else return!0}})});const Ps=f`
  fragment Ticket on TicketType {
    id
    refId
    title
    description
    createDate
    status
    createBy {
      id
      user_name
      full_name
      status
      avatar
      supporter
      role_id
      phone
      address
      supporter
    }
    receiver {
      id
      user_name
      full_name
      avatar
      role_id
      phone
      address
      supporter
    }
    reactions {
      id
      ticketId
      reaction
      count
    }
  }
`,gm=f`
  mutation createTicket($input: TicketInputType) @api(name: "appZim") {
    createTicket(input: $input) {
      ...Ticket
    }
  }
  ${Ps}
`,bp=f`
  mutation updateTicket($input: TicketInputType) @api(name: "appZim") {
    updateTicket(input: $input) {
      ...Ticket
    }
  }
  ${Ps}
`,gp=f`
  mutation updateTicketStatus($input: TicketUpdateStatusType)
  @api(name: "appZim") {
    updateTicketStatus(input: $input) {
      ...Ticket
    }
  }
  ${Ps}
`,hp=f`
  mutation createTicketReaction($input: TicketReactionInputType)
  @api(name: "appZim") {
    createTicketReaction(input: $input) {
      id
      ticketId
      reaction
      count
    }
  }
  ${Ps}
`;var ae="/Users/zimdev/work/zim-app/src/components/Forms/TicketForm/index.tsx";const hm=Le.object().shape({title:Le.string().required("Vui l\xF2ng nh\u1EADp ti\xEAu \u0111\u1EC1"),description:Le.string().min(10,"T\u1ED1i thi\u1EC3u 10 k\xFD t\u1EF1").required("Vui l\xF2ng nh\u1EADp m\xF4 t\u1EA3"),receiver:Le.mixed()}),fm=({enableReceiver:e,users:s,onInputChange:o})=>{var n;const{register:l,formState:{errors:a},resetField:i}=Ui();return t.exports.jsxDEV(At,{direction:"column",w:"full",justifyContent:"stretch",alignItems:"stretch",className:"reply-editor",gap:4,children:[t.exports.jsxDEV(Ge,y(x({label:"Ti\xEAu \u0111\u1EC1",placeholder:"Nh\u1EADp..."},l("title")),{error:a==null?void 0:a.title}),void 0,!1,{fileName:ae,lineNumber:85,columnNumber:7},globalThis),e&&t.exports.jsxDEV(Kt,{name:"receiver",render:({field:r})=>t.exports.jsxDEV(ws,x({label:"Ng\u01B0\u1EDDi nh\u1EADn",options:s||[],getOptionLabel:u=>`${u.full_name} - ${u.phone}`,getOptionValue:u=>u.id+"",formatOptionLabel:u=>t.exports.jsxDEV(q,{children:[t.exports.jsxDEV(fi,{src:u.avatar,objectFit:"cover",boxSize:6,flexShrink:0},void 0,!1,{fileName:ae,lineNumber:104,columnNumber:19},globalThis),t.exports.jsxDEV(M,{children:u.full_name},void 0,!1,{fileName:ae,lineNumber:110,columnNumber:19},globalThis)]},void 0,!0,{fileName:ae,lineNumber:103,columnNumber:17},globalThis),error:a==null?void 0:a.receiver,onInputChange:o},r),void 0,!1,{fileName:ae,lineNumber:95,columnNumber:13},globalThis)},void 0,!1,{fileName:ae,lineNumber:92,columnNumber:9},globalThis),t.exports.jsxDEV(S,{children:t.exports.jsxDEV(cs,{isInvalid:a==null?void 0:a.description,children:[t.exports.jsxDEV(Kt,{name:"description",render:({field:r})=>t.exports.jsxDEV(Pl,x({showWordCount:!1,placeholder:"N\u1ED9i dung ticket..."},r),void 0,!1,{fileName:ae,lineNumber:126,columnNumber:15},globalThis)},void 0,!1,{fileName:ae,lineNumber:123,columnNumber:11},globalThis),t.exports.jsxDEV(ds,{children:(n=a==null?void 0:a.description)==null?void 0:n.message},void 0,!1,{fileName:ae,lineNumber:133,columnNumber:11},globalThis)]},void 0,!0,{fileName:ae,lineNumber:122,columnNumber:9},globalThis)},void 0,!1,{fileName:ae,lineNumber:121,columnNumber:7},globalThis)]},void 0,!0,{fileName:ae,lineNumber:77,columnNumber:5},globalThis)},xm=({onClose:e,isOpen:s,submitCallback:o=null})=>{const l=c.exports.useRef(),a=bs({resolver:Hi(hm),mode:"onBlur",reValidateMode:"onChange"}),i=Je();_l();const[n,r]=vt(),u=mt(),[g,b]=c.exports.useState(""),[m,v]=c.exports.useState(g);Hr(()=>{b(m)},500,[m]);const[d]=ne(),h=d.roleId===6,[A,{loading:E}]=Pe(gm,{onCompleted:N=>{const{createTicket:$}=N;$&&i({title:"Th\xE0nh c\xF4ng!",description:"Ticket \u0111\xE3 \u0111\u01B0\u1EE3c g\u1EEDi.",status:"success",duration:3e3,isClosable:!0}),e(),o&&typeof o=="function"&&o(),u(["getTicketsPagination"])},onError:N=>{var $;i({title:"G\u1EEDi kh\xF4ng th\xE0nh c\xF4ng!",description:`${($=N==null?void 0:N.message)!=null?$:"L\u1ED7i"}`,status:"error",duration:3e3,isClosable:!0})},notifyOnNetworkStatusChange:!0}),{data:T}=De(Vn(["avatar","full_name","phone"]),{variables:{input:{limit:100,q:g}},skip:!h}),p=T==null?void 0:T.getListStudentByEc.docs,D=c.exports.useCallback(async N=>{var k;if(h&&!(N==null?void 0:N.receiver)){a.setError("receiver",{type:"manual",message:"Vui l\xF2ng ch\u1ECDn ng\u01B0\u1EDDi nh\u1EADn"});return}const $={refId:nu,title:N==null?void 0:N.title,description:N==null?void 0:N.description,receiverId:h?parseInt((k=N==null?void 0:N.receiver)==null?void 0:k.id):null};h||delete $.receiverId,await A({variables:{input:$}})},[h]);return c.exports.useEffect(()=>{if(s){const N=n.get("receiverId");if(!N||!(p==null?void 0:p.length))return;const $=p.find(k=>k.id===Number(N));a.reset({title:"",description:"",receiver:$||null})}else n.delete("receiverId"),r(n),a.reset({title:"",description:"",receiver:null})},[s,n,p]),t.exports.jsxDEV(Hs,y(x({},a),{children:t.exports.jsxDEV(Vt,{finalFocusRef:l,isOpen:s,onClose:e,size:"3xl",children:[t.exports.jsxDEV(Nt,{},void 0,!1,{fileName:ae,lineNumber:271,columnNumber:9},globalThis),t.exports.jsxDEV(kt,{children:[t.exports.jsxDEV(Et,{children:"T\u1EA1o ticket"},void 0,!1,{fileName:ae,lineNumber:273,columnNumber:11},globalThis),t.exports.jsxDEV(wt,{},void 0,!1,{fileName:ae,lineNumber:274,columnNumber:11},globalThis),t.exports.jsxDEV("form",{onSubmit:a.handleSubmit(D),children:[t.exports.jsxDEV(It,{children:t.exports.jsxDEV(fm,{enableReceiver:h,users:p,onInputChange:v},void 0,!1,{fileName:ae,lineNumber:277,columnNumber:15},globalThis)},void 0,!1,{fileName:ae,lineNumber:276,columnNumber:13},globalThis),t.exports.jsxDEV(Ht,{children:[t.exports.jsxDEV(Y,{leftIcon:t.exports.jsxDEV(Wt,{as:Ws},void 0,!1,{fileName:ae,lineNumber:286,columnNumber:27},globalThis),colorScheme:"green",mr:4,type:"submit",isLoading:E,children:"G\u1EEDi ticket"},void 0,!1,{fileName:ae,lineNumber:285,columnNumber:15},globalThis),t.exports.jsxDEV(Y,{variant:"ghost",onClick:e,children:"H\u1EE7y"},void 0,!1,{fileName:ae,lineNumber:294,columnNumber:15},globalThis)]},void 0,!0,{fileName:ae,lineNumber:284,columnNumber:13},globalThis)]},void 0,!0,{fileName:ae,lineNumber:275,columnNumber:11},globalThis)]},void 0,!0,{fileName:ae,lineNumber:272,columnNumber:9},globalThis)]},void 0,!0,{fileName:ae,lineNumber:265,columnNumber:7},globalThis)}),void 0,!1,{fileName:ae,lineNumber:264,columnNumber:5},globalThis)};var ye="/Users/zimdev/work/zim-app/src/pages/chat/manageParticipantDetail/components/tagForm.tsx";function Nm({color:e,onClick:s,isActive:o}){return t.exports.jsxDEV(S,{borderWidth:o?3:1,borderColor:o?"white":"",as:"button",onClick:s,rounded:"sm",w:4,h:4,bg:`${e}`},void 0,!1,{fileName:ye,lineNumber:22,columnNumber:5},this)}function Em({firstFieldRef:e,onClose:s}){var T,p;const o=Je(),[l,a]=c.exports.useState("1"),[i,n]=c.exports.useState(null),r=V(D=>D.conversationId),[u,g]=c.exports.useState("#00DAA3"),[b,m]=c.exports.useState(""),{data:v}=De(_n,{skip:l==="2"}),[d,{loading:h}]=Pe(Eu),A=c.exports.useCallback(()=>{g("#00DAA3"),m(""),n(null)},[]),E=c.exports.useCallback(async()=>{var $;const D={conversationId:r,tagId:i==null?void 0:i.id},N={conversationId:r,name:b,color:u};try{await d({variables:(i==null?void 0:i.id)?D:N}),s(),A(),o({title:"Th\xE0nh c\xF4ng!",description:"G\u1EAFn tag th\xE0nh c\xF4ng",status:"success",duration:3e3,isClosable:!0})}catch(k){o({title:"G\u1EAFn tag kh\xF4ng th\xE0nh c\xF4ng!",description:`${($=k==null?void 0:k.message)!=null?$:"L\u1ED7i"}`,status:"error",duration:3e3,isClosable:!0})}},[b,u,r,i,A]);return t.exports.jsxDEV(ve,{spacing:4,children:[t.exports.jsxDEV(Wi,{defaultValue:"1",children:t.exports.jsxDEV(q,{spacing:5,align:"center",children:[t.exports.jsxDEV(Qi,{isChecked:l==="1",colorScheme:"red",value:"1",onChange:()=>{a("1"),m(""),g("#00DAA3")},children:"Tag c\xF3 s\u1EB5n"},void 0,!1,{fileName:ye,lineNumber:90,columnNumber:11},this),t.exports.jsxDEV(Qi,{isChecked:l==="2",colorScheme:"green",value:"2",onChange:()=>{a("2"),n(null)},children:"Th\xEAm tag m\u1EDBi"},void 0,!1,{fileName:ye,lineNumber:102,columnNumber:11},this)]},void 0,!0,{fileName:ye,lineNumber:89,columnNumber:9},this)},void 0,!1,{fileName:ye,lineNumber:88,columnNumber:7},this),l==="1"&&t.exports.jsxDEV(ws,{value:i,label:"Tags",onChange:n,options:(p=(T=v==null?void 0:v.getListTag)==null?void 0:T.docs)!=null?p:[],getOptionLabel:D=>D.name,getOptionValue:D=>D.id+"",formatOptionLabel:D=>t.exports.jsxDEV(q,{children:[t.exports.jsxDEV(S,{bg:D.color,w:"5px",h:"5px",rounded:"full",mr:1},void 0,!1,{fileName:ye,lineNumber:125,columnNumber:15},this),t.exports.jsxDEV(M,{children:D.name},void 0,!1,{fileName:ye,lineNumber:126,columnNumber:15},this)]},void 0,!0,{fileName:ye,lineNumber:124,columnNumber:13},this)},void 0,!1,{fileName:ye,lineNumber:116,columnNumber:9},this),l==="2"&&t.exports.jsxDEV(ve,{spacing:4,children:[t.exports.jsxDEV(Wi,{onChange:g,value:u,children:t.exports.jsxDEV(q,{spacing:3,children:Zm.map((D,N)=>{const $=D.color===u;return t.exports.jsxDEV(Nm,{color:D.color,onClick:()=>g(D.color),isActive:$},D.color,!1,{fileName:ye,lineNumber:138,columnNumber:19},this)})},void 0,!1,{fileName:ye,lineNumber:134,columnNumber:13},this)},void 0,!1,{fileName:ye,lineNumber:133,columnNumber:11},this),t.exports.jsxDEV(Ge,{label:"Tag name",id:"tag-name",ref:e,placeholder:"Nh\u1EADp t\xEAn tag",value:b,onChange:D=>m(D.target.value)},void 0,!1,{fileName:ye,lineNumber:148,columnNumber:11},this)]},void 0,!0,{fileName:ye,lineNumber:132,columnNumber:9},this),t.exports.jsxDEV(Wr,{d:"flex",justifyContent:"flex-end",children:[t.exports.jsxDEV(Y,{onClick:()=>{s(),A()},variant:"outline",children:"Cancel"},void 0,!1,{fileName:ye,lineNumber:159,columnNumber:9},this),t.exports.jsxDEV(Y,{onClick:E,disabled:!b&&!i||h,colorScheme:"teal",children:"Save"},void 0,!1,{fileName:ye,lineNumber:168,columnNumber:9},this)]},void 0,!0,{fileName:ye,lineNumber:158,columnNumber:7},this)]},void 0,!0,{fileName:ye,lineNumber:87,columnNumber:5},this)}var te="/Users/zimdev/work/zim-app/src/pages/chat/manageParticipantDetail/components/participantProfile.tsx";const vm=({item:e})=>t.exports.jsxDEV(q,{children:[t.exports.jsxDEV(S,{w:6,h:6,p:1,rounded:"full",bg:j("gray.100","#1e293b"),overflow:"hidden",display:"flex",alignItems:"center",justifyContent:"center",children:e.icon},void 0,!1,{fileName:te,lineNumber:36,columnNumber:7},globalThis),typeof e.value=="function"?e==null?void 0:e.value():t.exports.jsxDEV(M,{flex:1,fontSize:14,color:j("gray.800","slate.300"),noOfLines:2,children:e.value||"Kh\xF4ng c\xF3 th\xF4ng tin"},void 0,!1,{fileName:te,lineNumber:52,columnNumber:9},globalThis)]},void 0,!0,{fileName:te,lineNumber:35,columnNumber:5},globalThis),Am=c.exports.memo(({participantInfo:e,participantAvatarUrl:s,fullName:o,loading:l,isStudent:a,tags:i,participants:n})=>{const{isOpen:r,onClose:u,onOpen:g}=Xe(),b=St.useRef(null),[m]=ne(),v=To.map(p=>p.id),d=V(p=>p.setVisibleUserProfile),h=V(p=>p.conversationId),A=Hm(e,i),E=Jt({sm:"visible",xl:""}),T=c.exports.useCallback(()=>{d(!1)},[]);return!a&&t.exports.jsxDEV(ie,{alignItems:"stretch",spacing:2.5,px:4,py:2,borderBottomWidth:1,children:[t.exports.jsxDEV(ve,{spacing:2.5,children:[t.exports.jsxDEV(q,{justifyContent:"space-between",children:[t.exports.jsxDEV(mm,{onClose:T,participantAvatarUrl:s,fullName:o,loading:l},void 0,!1,{fileName:te,lineNumber:115,columnNumber:15},globalThis),E&&t.exports.jsxDEV(S,{onClick:T,display:"flex",cursor:"pointer",w:6,h:6,alignItems:"center",justifyContent:"center",children:t.exports.jsxDEV(Qr,{className:"w-full h-full"},void 0,!1,{fileName:te,lineNumber:131,columnNumber:19},globalThis)},void 0,!1,{fileName:te,lineNumber:122,columnNumber:17},globalThis)]},void 0,!0,{fileName:te,lineNumber:114,columnNumber:13},globalThis),A==null?void 0:A.filter(p=>p.visible).map((p,D)=>t.exports.jsxDEV(vm,{item:p},`${p.value}-${D}`,!1,{fileName:te,lineNumber:139,columnNumber:24},globalThis))]},void 0,!0,{fileName:te,lineNumber:113,columnNumber:11},globalThis),!qs.includes(m.roleId)&&h&&(n==null?void 0:n.find(p=>p.userId===(m==null?void 0:m.id.toString())))&&t.exports.jsxDEV(q,{spacing:10,children:[t.exports.jsxDEV(Qt,{children:({onClose:p})=>t.exports.jsxDEV(t.exports.Fragment,{children:[t.exports.jsxDEV(Zt,{children:t.exports.jsxDEV(Y,{bg:"transparent",_hover:{backgroundColor:"transparent"},_focus:{backgroundColor:"transparent"},_active:{backgroundColor:"transparent"},children:v.includes(m==null?void 0:m.roleId)&&t.exports.jsxDEV(Gl,{iconContainerClassName:"bg-blue-500",label:"G\u1EAFn tag"},void 0,!1,{fileName:te,lineNumber:158,columnNumber:29},globalThis)},void 0,!1,{fileName:te,lineNumber:151,columnNumber:25},globalThis)},void 0,!1,{fileName:te,lineNumber:150,columnNumber:23},globalThis),t.exports.jsxDEV(hs,{container:document.getElementById("profile-portal"),children:t.exports.jsxDEV(Yt,{children:[t.exports.jsxDEV(Zi,{},void 0,!1,{fileName:te,lineNumber:169,columnNumber:27},globalThis),t.exports.jsxDEV(Zr,{children:"G\u1EAFn tag"},void 0,!1,{fileName:te,lineNumber:170,columnNumber:27},globalThis),t.exports.jsxDEV(Xs,{children:[t.exports.jsxDEV(Zi,{},void 0,!1,{fileName:te,lineNumber:172,columnNumber:29},globalThis),t.exports.jsxDEV(Em,{firstFieldRef:b,onClose:p},void 0,!1,{fileName:te,lineNumber:173,columnNumber:29},globalThis)]},void 0,!0,{fileName:te,lineNumber:171,columnNumber:27},globalThis)]},void 0,!0,{fileName:te,lineNumber:168,columnNumber:25},globalThis)},void 0,!1,{fileName:te,lineNumber:165,columnNumber:23},globalThis)]},void 0,!0)},void 0,!1,{fileName:te,lineNumber:147,columnNumber:17},globalThis),(e==null?void 0:e.role_name)==="H\u1ECDc vi\xEAn"&&v.includes(m==null?void 0:m.roleId)&&h&&t.exports.jsxDEV(Gl,{onClick:g,label:"T\u1EA1o ticket",iconContainerClassName:"bg-red-500"},void 0,!1,{fileName:te,lineNumber:187,columnNumber:21},globalThis)]},void 0,!0,{fileName:te,lineNumber:146,columnNumber:15},globalThis),t.exports.jsxDEV(xm,{onClose:u,isOpen:r},void 0,!1,{fileName:te,lineNumber:195,columnNumber:11},globalThis)]},void 0,!0,{fileName:te,lineNumber:106,columnNumber:9},globalThis)}),Gl=({label:e="T\u1EA1o ticket",onClick:s,iconContainerClassName:o})=>t.exports.jsxDEV(q,{onClick:s,display:"flex",alignItems:"center",cursor:"pointer",children:[t.exports.jsxDEV(S,{className:o,w:5,h:5,rounded:"full",display:"flex",alignItems:"center",justifyContent:"center",children:t.exports.jsxDEV(Yr,{color:"white"},void 0,!1,{fileName:te,lineNumber:228,columnNumber:9},globalThis)},void 0,!1,{fileName:te,lineNumber:219,columnNumber:7},globalThis),t.exports.jsxDEV(M,{fontWeight:"400",color:j("gray.800","slate.300"),children:e},void 0,!1,{fileName:te,lineNumber:230,columnNumber:7},globalThis)]},void 0,!0,{fileName:te,lineNumber:213,columnNumber:5},globalThis);var jt="/Users/zimdev/work/zim-app/src/pages/chat/components/noteItem.tsx";const Tm=({createdAt:e,content:s,owner:o})=>{var l;return t.exports.jsxDEV(q,{display:"flex",alignItems:"start",children:[t.exports.jsxDEV(He,{size:"sm",src:o==null?void 0:o.avatar,name:o==null?void 0:o.fullName},void 0,!1,{fileName:jt,lineNumber:16,columnNumber:7},globalThis),t.exports.jsxDEV(ie,{flex:1,alignItems:"stretch",spacing:1,children:[t.exports.jsxDEV(q,{spacing:3,justifyContent:{base:"start","2xl":"space-between"},children:[t.exports.jsxDEV(M,{noOfLines:1,fontSize:14,lineHeight:"24px",fontWeight:"medium",color:j("gray.800","slate.400"),maxW:{base:"90px","2xl":"120px"},children:(l=o==null?void 0:o.fullName)!=null?l:"Kh\xF4ng c\xF3"},void 0,!1,{fileName:jt,lineNumber:25,columnNumber:11},globalThis),t.exports.jsxDEV(M,{fontSize:12,lineHeight:"24px",className:"text-gray-400",children:Ke(e).format("DD-MM-YYYY, HH:mm")},void 0,!1,{fileName:jt,lineNumber:38,columnNumber:11},globalThis)]},void 0,!0,{fileName:jt,lineNumber:18,columnNumber:9},globalThis),t.exports.jsxDEV(M,{fontSize:12,lineHeight:"16px",color:j("gray.800","slate.200"),children:s},void 0,!1,{fileName:jt,lineNumber:42,columnNumber:9},globalThis)]},void 0,!0,{fileName:jt,lineNumber:17,columnNumber:7},globalThis)]},void 0,!0,{fileName:jt,lineNumber:15,columnNumber:5},globalThis)};var _s="/Users/zimdev/work/zim-app/src/pages/chat/boxMessage/components/noteInput.tsx";const ym=c.exports.memo(()=>{const e=c.exports.useRef(null),s=V(m=>m.conversationId),o=mt(),[l,a]=c.exports.useState(!1),[i,n]=c.exports.useState(""),[r]=Pe(xu),u=c.exports.useCallback(async()=>{try{await r({variables:{conversationId:s,content:i}}),await o(["notes"])}catch(m){console.log(m)}},[s,i]),g=c.exports.useCallback(async()=>{i.trim().length!==0&&(await u(),n(""))},[i]),b=c.exports.useCallback(async m=>{const v=m.target.value.trim();if(m.key==="Enter"&&!m.shiftKey)v.length!==0&&(u().then(),n(""));else return!1},[u]);return c.exports.useEffect(()=>{a(!0)},[]),t.exports.jsxDEV(q,{w:"100%",borderWidth:1,rounded:"lg",borderColor:j("gray.300","whiteAlpha.300"),p:1,children:[l&&t.exports.jsxDEV(to,{_focus:{outline:"none"},as:so,style:{resize:"none"},ref:e,minRows:1,maxRows:5,rows:1,value:i,onChange:m=>{m.target.value!==`
`&&n(m.target.value)},border:"none",placeholder:"Nh\u1EADp n\u1ED9i dung ghi ch\xFA ...",onKeyPress:b,color:j("gray.900","slate.300"),bg:"transparent"},void 0,!1,{fileName:_s,lineNumber:64,columnNumber:9},globalThis),t.exports.jsxDEV(S,{cursor:"pointer",w:5,h:5,pointerEvents:i?"auto":"none",onClick:g,children:t.exports.jsxDEV(Gi,{fill:j((i==null?void 0:i.length)>0?"#DA0037":"#686868","white")},void 0,!1,{fileName:_s,lineNumber:92,columnNumber:9},globalThis)},void 0,!1,{fileName:_s,lineNumber:85,columnNumber:7},globalThis)]},void 0,!0,{fileName:_s,lineNumber:56,columnNumber:5},globalThis)}),Dm=({conversationId:e,userId:s})=>{var b,m,v;const{data:o,loading:l,fetchMore:a}=De(In,{variables:be({conversationId:e||"",userId:e?"":s?s==null?void 0:s.toString():"",limit:5},d=>d),skip:!e}),i=(b=o==null?void 0:o.notes)==null?void 0:b.docs,n=(m=o==null?void 0:o.notes)==null?void 0:m.page,r=(v=o==null?void 0:o.notes)==null?void 0:v.hasNextPage,{onLoadMore:u,isLoadingMore:g}=Dt({variables:{conversationId:e,limit:5,page:n+1},fetchMore:a,hasNextPage:r});return c.exports.useMemo(()=>({notes:i,loading:l,onLoadMore:u,isLoadingMore:g,hasNextPage:r}),[i,l,u,g,r])};var zt="/Users/zimdev/work/zim-app/src/pages/chat/manageParticipantDetail/components/notes.tsx";const jm=c.exports.memo(({isSuperAdmin:e,allowForSupperAdmin:s})=>{const o=V(m=>m.conversationId),l=V(m=>m.userId),a=c.exports.useRef(null),{notes:i,onLoadMore:n,hasNextPage:r,isLoadingMore:u}=Dm({conversationId:o,userId:l}),g=c.exports.useCallback(async()=>{await n()},[n]),b=c.exports.useMemo(()=>i==null?void 0:i.map(m=>t.exports.jsxDEV(Tm,x({},m),m.id,!1,{fileName:zt,lineNumber:22,columnNumber:14},globalThis)),[i]);return c.exports.useEffect(()=>{let m;return(a==null?void 0:a.current)&&(m=new fs(a==null?void 0:a.current,{wheelSpeed:1,minScrollbarLength:10})),()=>{m&&m.destroy()}},[]),t.exports.jsxDEV(ie,{alignItems:"stretch",spacing:2.5,px:4,py:2,children:[t.exports.jsxDEV(M,{fontSize:14,color:j("gray.800","slate.300"),children:["Notes ",i==null?void 0:i.length]},void 0,!0,{fileName:zt,lineNumber:39,columnNumber:7},globalThis),t.exports.jsxDEV(ie,{alignItems:"stretch",spacing:2.5,maxH:"200px",ref:a,className:"scroll-container",overflow:"hidden",position:"relative",minH:0,children:b},void 0,!1,{fileName:zt,lineNumber:42,columnNumber:7},globalThis),r&&t.exports.jsxDEV(Y,{h:"32px",isLoading:u,loadingText:"\u0110ang t\u1EA3i th\xEAm",onClick:g,w:"100%",rounded:"none",borderWidth:"1px 0px 0px 0px",bg:j("white","slate.600"),fontSize:14,fontWeight:400,justifyContent:"center",children:"Xem th\xEAm"},void 0,!1,{fileName:zt,lineNumber:55,columnNumber:9},globalThis),(!e||s)&&t.exports.jsxDEV(ym,{},void 0,!1,{fileName:zt,lineNumber:71,columnNumber:50},globalThis)]},void 0,!0,{fileName:zt,lineNumber:38,columnNumber:5},globalThis)}),Cm=({conversationId:e,userId:s})=>{var v,d,h;const[o,l]=c.exports.useState(!1),a=y(x({},be({conversationId:e||"",userId:e?"":s?s==null?void 0:s.toString():""},A=>A)),{offset:0}),{data:i,loading:n,fetchMore:r}=De(Pn,{variables:a}),u=(d=(v=i==null?void 0:i.getAttachmentsInConversation)==null?void 0:v.docs)==null?void 0:d.map(A=>A.attachments),g=[].concat.apply([],u),b=(h=i==null?void 0:i.getAttachmentsInConversation)==null?void 0:h.hasNextPage,m=c.exports.useCallback(async()=>{var A;!b||o||(await l(!0),await r({variables:y(x({},a),{offset:((A=i==null?void 0:i.getAttachmentsInConversation)==null?void 0:A.docs.length)+1}),updateQuery:(E,{fetchMoreResult:T})=>T?co(E,T):E}),await l(!1))},[i,r,o,e]);return{attachments:g,loading:n,onLoadMore:m,isLoadingMore:o,hasNextPage:b}};var me="/Users/zimdev/work/zim-app/src/pages/chat/manageParticipantDetail/components/attachments.tsx";function $m(e){let s=/^.+\.([^.]+)$/.exec(e);return s==null?"":s[1]}const Sm=c.exports.memo(({item:e,setAttachmentIndex:s,setShowImageViewer:o,index:l})=>{var r,u,g,b;const a=c.exports.useCallback(()=>{s(l),o(!0)},[l]),i=$m((r=e==null?void 0:e.attachment)==null?void 0:r.fullUrl),n=c.exports.useMemo(()=>{switch(i){case"xls":return Xr;case"doc":return Jr;default:return Kr}},[i]);return t.exports.jsxDEV(S,{minH:"80px",children:[(e==null?void 0:e.type)==="image"&&t.exports.jsxDEV(rt,{rounded:"md",onClick:a,cursor:"pointer",flex:1,objectFit:"contain",src:(u=e==null?void 0:e.attachment)==null?void 0:u.fullUrl,alt:"",h:"full"},void 0,!1,{fileName:me,lineNumber:50,columnNumber:11},globalThis),(e==null?void 0:e.type)==="file"&&t.exports.jsxDEV(Pt,{target:"_blank",href:(g=e==null?void 0:e.attachment)==null?void 0:g.fullUrl,children:t.exports.jsxDEV(S,{"data-tip":"file-tooltip",flex:1,display:"flex",alignItems:"center",justifyContent:"center",bg:j("blue.50","slate.400"),rounded:"md",h:"full",children:[t.exports.jsxDEV(n,{className:"w-5 h-5 "},void 0,!1,{fileName:me,lineNumber:73,columnNumber:15},globalThis),t.exports.jsxDEV(wi,{children:t.exports.jsxDEV("p",{children:ho((b=e==null?void 0:e.attachment)==null?void 0:b.path)},void 0,!1,{fileName:me,lineNumber:75,columnNumber:27},globalThis),place:"top",type:j("info","success"),effect:"float"},void 0,!1,{fileName:me,lineNumber:74,columnNumber:15},globalThis)]},void 0,!0,{fileName:me,lineNumber:63,columnNumber:13},globalThis)},void 0,!1,{fileName:me,lineNumber:62,columnNumber:11},globalThis)]},void 0,!0,{fileName:me,lineNumber:48,columnNumber:7},globalThis)}),Vm=c.exports.memo(()=>{const e=V(m=>m.conversationId),s=V(m=>m.userId),[o,l]=c.exports.useState(null),[a,i]=c.exports.useState(!1),{attachments:n,isLoadingMore:r,onLoadMore:u,hasNextPage:g}=Cm({conversationId:e,userId:s}),b=c.exports.useCallback(async()=>{await u()},[u]);return t.exports.jsxDEV(ie,{alignItems:"stretch",spacing:2.5,py:2,children:[t.exports.jsxDEV(Ti,{allowMultiple:!0,children:t.exports.jsxDEV(Ni,{children:[t.exports.jsxDEV(q,{position:"relative",spacing:0,pl:4,_hover:{backgroundColor:j("#f0f0f0","slate.600")},children:[t.exports.jsxDEV(S,{display:"flex",justifyContent:"start",flex:1,children:t.exports.jsxDEV(M,{color:j("#444","white"),fontSize:16,fontWeight:"medium",children:"T\xE0i li\u1EC7u"},void 0,!1,{fileName:me,lineNumber:114,columnNumber:15},globalThis)},void 0,!1,{fileName:me,lineNumber:113,columnNumber:13},globalThis),t.exports.jsxDEV(Ei,{display:"flex",alignItems:"center",justifyContent:"center",w:10,children:t.exports.jsxDEV(vi,{},void 0,!1,{fileName:me,lineNumber:128,columnNumber:15},globalThis)},void 0,!1,{fileName:me,lineNumber:122,columnNumber:13},globalThis)]},void 0,!0,{fileName:me,lineNumber:105,columnNumber:11},globalThis),t.exports.jsxDEV(Ai,{pb:4,children:[(n==null?void 0:n.length)===0&&t.exports.jsxDEV(M,{textAlign:"center",children:"Cu\u1ED9c tr\xF2 chuy\u1EC7n ch\u01B0a c\xF3 t\xE0i li\u1EC7u !!!"},void 0,!1,{fileName:me,lineNumber:133,columnNumber:15},globalThis),t.exports.jsxDEV(xt,{columns:3,spacing:2,children:n==null?void 0:n.map((m,v)=>t.exports.jsxDEV(Sm,{item:m,setAttachmentIndex:l,setShowImageViewer:i,index:v},void 0,!1,{fileName:me,lineNumber:140,columnNumber:19},globalThis))},void 0,!1,{fileName:me,lineNumber:137,columnNumber:13},globalThis),g&&t.exports.jsxDEV(Y,{h:"32px",isLoading:r,loadingText:"\u0110ang t\u1EA3i th\xEAm",onClick:b,w:"100%",rounded:"none",borderWidth:"1px 0px 0px 0px",bg:j("white","slate.600"),fontSize:14,fontWeight:400,justifyContent:"center",mt:2,children:"Xem th\xEAm"},void 0,!1,{fileName:me,lineNumber:150,columnNumber:15},globalThis)]},void 0,!0,{fileName:me,lineNumber:131,columnNumber:11},globalThis)]},void 0,!0,{fileName:me,lineNumber:104,columnNumber:9},globalThis)},void 0,!1,{fileName:me,lineNumber:103,columnNumber:7},globalThis),t.exports.jsxDEV(jl,{attachments:n,attachmentIndex:o,setAttachmentIndex:l,showImageViewer:a,setShowImageViewer:i},void 0,!1,{fileName:me,lineNumber:170,columnNumber:7},globalThis)]},void 0,!0,{fileName:me,lineNumber:102,columnNumber:5},globalThis)});var he="/Users/zimdev/work/zim-app/src/pages/chat/manageParticipantDetail/components/participantInfoModal.tsx";const km=({isOpen:e,onClose:s,userInfo:o,isOwner:l,groupId:a,isSupperAdmin:i,visibleRemoveMemberButton:n})=>{var A;const r=mt(),{isOpen:u,onOpen:g,onClose:b}=Xe(),m=St.useRef(),[v,{loading:d}]=Pe(Nu),h=c.exports.useCallback(async()=>{var E;try{await v({variables:{userId:(E=o==null?void 0:o.userId)==null?void 0:E.toString(),id:a}}),r(["conversationDetail"]),b(),s()}catch(T){console.log(T)}},[o,a]);return t.exports.jsxDEV(t.exports.Fragment,{children:[t.exports.jsxDEV(Vt,{isOpen:e,onClose:s,children:[t.exports.jsxDEV(Nt,{},void 0,!1,{fileName:he,lineNumber:62,columnNumber:9},globalThis),t.exports.jsxDEV(kt,{children:[t.exports.jsxDEV(Et,{textAlign:"center",children:"Th\xF4ng tin th\xE0nh vi\xEAn"},void 0,!1,{fileName:he,lineNumber:64,columnNumber:11},globalThis),t.exports.jsxDEV(wt,{},void 0,!1,{fileName:he,lineNumber:65,columnNumber:11},globalThis),t.exports.jsxDEV(It,{pb:4,children:[t.exports.jsxDEV(ie,{p:4,pt:0,children:t.exports.jsxDEV(He,{size:"2xl",src:o.avatar,name:o.fullName},void 0,!1,{fileName:he,lineNumber:68,columnNumber:15},globalThis)},void 0,!1,{fileName:he,lineNumber:67,columnNumber:13},globalThis),t.exports.jsxDEV(ie,{alignItems:"stretch",children:t.exports.jsxDEV(q,{justifyContent:l||i?"space-between":"center",alignItems:"center",children:[t.exports.jsxDEV(M,{textAlign:l||i?"left":"center",fontSize:"xl",children:[o.fullName," ",`(${(A=ao.find(E=>E.id===Number(o==null?void 0:o.roleId)))==null?void 0:A.name})`]},void 0,!0,{fileName:he,lineNumber:81,columnNumber:17},globalThis),n&&t.exports.jsxDEV(Y,{onClick:g,size:"sm",colorScheme:"red",variant:"solid",fontSize:13,textColor:"white",children:"M\u1EDDi r\u1EDDi nh\xF3m"},void 0,!1,{fileName:he,lineNumber:91,columnNumber:19},globalThis)]},void 0,!0,{fileName:he,lineNumber:75,columnNumber:15},globalThis)},void 0,!1,{fileName:he,lineNumber:74,columnNumber:13},globalThis)]},void 0,!0,{fileName:he,lineNumber:66,columnNumber:11},globalThis)]},void 0,!0,{fileName:he,lineNumber:63,columnNumber:9},globalThis)]},void 0,!0,{fileName:he,lineNumber:61,columnNumber:7},globalThis),t.exports.jsxDEV(en,{isOpen:u,leastDestructiveRef:m,onClose:b,children:t.exports.jsxDEV(Nt,{children:t.exports.jsxDEV(tn,{children:[t.exports.jsxDEV(Et,{fontSize:"lg",fontWeight:"bold",children:"C\u1EA3nh b\xE1o !!!"},void 0,!1,{fileName:he,lineNumber:114,columnNumber:13},globalThis),t.exports.jsxDEV(It,{children:"B\u1EA1n c\xF3 ch\u1EAFc kh\xF4ng? B\u1EA1n kh\xF4ng th\u1EC3 ho\xE0n t\xE1c h\xE0nh \u0111\u1ED9ng n\xE0y sau khi x\xF3a."},void 0,!1,{fileName:he,lineNumber:118,columnNumber:13},globalThis),t.exports.jsxDEV(Ht,{children:[t.exports.jsxDEV(Y,{disabled:d,ref:m,onClick:b,children:"Tr\u1EDF l\u1EA1i"},void 0,!1,{fileName:he,lineNumber:124,columnNumber:15},globalThis),t.exports.jsxDEV(Y,{disabled:d,colorScheme:"red",onClick:h,ml:3,color:"white",loadingText:"\u0110ang x\u1EED l\xFD",children:"X\xE1c nh\u1EADn"},void 0,!1,{fileName:he,lineNumber:127,columnNumber:15},globalThis)]},void 0,!0,{fileName:he,lineNumber:123,columnNumber:13},globalThis)]},void 0,!0,{fileName:he,lineNumber:113,columnNumber:11},globalThis)},void 0,!1,{fileName:he,lineNumber:112,columnNumber:9},globalThis)},void 0,!1,{fileName:he,lineNumber:107,columnNumber:7},globalThis)]},void 0,!0)};var ot="/Users/zimdev/work/zim-app/src/pages/chat/manageParticipantDetail/components/groupInfo.tsx";const wm=c.exports.memo(({conversationDetail:e})=>{var d,h,A,E;const s=c.exports.useRef(null),o=V(T=>T.conversationId),[l]=ne(),{isOpen:a,onClose:i,onOpen:n}=Xe(),{isOpen:r,onClose:u,onOpen:g}=Xe(),[b,m]=c.exports.useState(null),v=c.exports.useMemo(()=>{var T;return(T=e==null?void 0:e.participants)==null?void 0:T.map(p=>{var N,$;const D=((N=e==null?void 0:e.owner)==null?void 0:N.userId)===(p==null?void 0:p.userId);return t.exports.jsxDEV(ss,y(x({},p),{isOwner:D,lastOnline:p==null?void 0:p.online,avatarUrl:p==null?void 0:p.avatar,username:p==null?void 0:p.fullName,positionName:($=ao.find(k=>k.id===Number(p==null?void 0:p.roleId)))==null?void 0:$.name,onClick:async()=>{await m(p),await g()},px:0}),p.userId,!1,{fileName:ot,lineNumber:38,columnNumber:9},globalThis)})},[e]);return c.exports.useEffect(()=>{let T;return(s==null?void 0:s.current)&&(T=new fs(s==null?void 0:s.current,{wheelSpeed:1,minScrollbarLength:10})),()=>{T&&T.destroy()}},[]),t.exports.jsxDEV(ie,{alignItems:"stretch",spacing:2.5,px:4,py:2,borderBottomWidth:1,children:[t.exports.jsxDEV(q,{justifyContent:"space-between",alignItems:"center",children:[t.exports.jsxDEV(M,{fontSize:14,color:j("gray.800","slate.300"),children:"Th\xE0nh vi\xEAn"},void 0,!1,{fileName:ot,lineNumber:78,columnNumber:9},globalThis),((d=e==null?void 0:e.owner)==null?void 0:d.userId)===(l==null?void 0:l.id)&&t.exports.jsxDEV(S,{onClick:n,as:"button",w:6,h:6,display:"flex",alignItems:"center",justifyContent:"center",children:t.exports.jsxDEV(sn,{className:"w-full h-full"},void 0,!1,{fileName:ot,lineNumber:91,columnNumber:13},globalThis)},void 0,!1,{fileName:ot,lineNumber:82,columnNumber:11},globalThis)]},void 0,!0,{fileName:ot,lineNumber:77,columnNumber:7},globalThis),t.exports.jsxDEV(ie,{alignItems:"stretch",spacing:0,maxH:"400px",ref:s,className:"scroll-container",overflow:"hidden",position:"relative",minH:0,children:v},void 0,!1,{fileName:ot,lineNumber:95,columnNumber:7},globalThis),t.exports.jsxDEV(mo,{isChannel:!1,isAddSupporter:!1,isEdit:!0,isOpen:a,onClose:i,listParticipants:(h=e==null?void 0:e.participants)==null?void 0:h.map(T=>T==null?void 0:T.userId),name:e==null?void 0:e.name,groupId:o},void 0,!1,{fileName:ot,lineNumber:107,columnNumber:7},globalThis),t.exports.jsxDEV(km,{visibleRemoveMemberButton:l.id===((A=e==null?void 0:e.owner)==null?void 0:A.userId),isSupperAdmin:(l==null?void 0:l.roleId)===1,groupId:e==null?void 0:e.id,isOwner:((E=e==null?void 0:e.owner)==null?void 0:E.userId)===(b==null?void 0:b.userId),userInfo:b!=null?b:{},isOpen:r&&b,onClose:u},void 0,!1,{fileName:ot,lineNumber:119,columnNumber:7},globalThis)]},void 0,!0,{fileName:ot,lineNumber:70,columnNumber:5},globalThis)});var Eo="/Users/zimdev/work/zim-app/src/pages/chat/components/courseItem.tsx";const vo={ongoing:{backgroundColor:"green.500"},incoming:{backgroundColor:"orange.400"},finished:{backgroundColor:"gray.300"}},Im=({status:e,title:s})=>t.exports.jsxDEV(q,{children:[t.exports.jsxDEV(S,{w:2.5,h:2.5,rounded:"full",bg:vo==null?void 0:vo[e].backgroundColor},void 0,!1,{fileName:Eo,lineNumber:22,columnNumber:7},globalThis),t.exports.jsxDEV(M,{flex:1,lineHeight:"20px",fontSize:14,color:j("#444","slate.300"),children:s},void 0,!1,{fileName:Eo,lineNumber:28,columnNumber:7},globalThis)]},void 0,!0,{fileName:Eo,lineNumber:21,columnNumber:5},globalThis);var Ao="/Users/zimdev/work/zim-app/src/pages/chat/manageParticipantDetail/components/courses.tsx";const Pm=c.exports.memo(({data:e})=>{const s=c.exports.useRef(null);c.exports.useEffect(()=>{let l;return(s==null?void 0:s.current)&&(l=new fs(s==null?void 0:s.current,{wheelSpeed:1,minScrollbarLength:10})),()=>{l&&l.destroy()}},[]);const o=c.exports.useMemo(()=>e.map(l=>{var n;const{status:a,name:i}=(n=l==null?void 0:l.course)!=null?n:{};return c.exports.createElement(Im,y(x({},l),{key:l==null?void 0:l.id,status:a,title:i,__self:globalThis,__source:{fileName:Ao,lineNumber:25,columnNumber:9}}))}),[e]);return t.exports.jsxDEV(ie,{alignItems:"stretch",spacing:2.5,px:4,py:2,borderBottomWidth:1,maxH:"200px",ref:s,className:"scroll-container",overflow:"hidden",position:"relative",minH:0,children:[t.exports.jsxDEV(M,{fontSize:14,color:j("gray.800","slate.300"),children:["Kh\xF3a h\u1ECDc ",e.length]},void 0,!0,{fileName:Ao,lineNumber:43,columnNumber:7},globalThis),o]},void 0,!0,{fileName:Ao,lineNumber:30,columnNumber:5},globalThis)}),Hl={create:[6],changeEC:[12,1,2],makeApointment:[6]},fp={additionalTeacherOrCancelForStudent:[1,2],viewTableMockTestDetail:[1,2,6],canCreateOrEditMockTest:[1,2],assignTeacherAndEC:[1,2]};var Fs="/Users/zimdev/work/zim-app/src/components/AutoResizeTextarea/index.tsx";const _m=St.forwardRef((e,s)=>{var o;return t.exports.jsxDEV(cs,{isInvalid:!!(e==null?void 0:e.error),width:"100%",children:[(e==null?void 0:e.label)&&t.exports.jsxDEV(Bs,{htmlFor:e.id,children:e==null?void 0:e.label},void 0,!1,{fileName:Fs,lineNumber:14,columnNumber:24},globalThis),t.exports.jsxDEV(to,x({minH:"unset",overflow:"hidden",w:"100%",resize:"none",ref:s,minRows:1,as:so,transition:"height none",bg:j("white","slate.700")},e),void 0,!1,{fileName:Fs,lineNumber:15,columnNumber:7},globalThis),(e==null?void 0:e.error)&&t.exports.jsxDEV(ds,{children:(o=e==null?void 0:e.error)==null?void 0:o.message},void 0,!1,{fileName:Fs,lineNumber:28,columnNumber:9},globalThis)]},void 0,!0,{fileName:Fs,lineNumber:13,columnNumber:5},globalThis)}),Fm=(e={},s={})=>{const o=Je();return De(qn,x({variables:e,onError:l=>{o({title:"L\u1EA5y d\u1EEF li\u1EC7u th\u1EA5t b\u1EA1i.",description:`GET list source status error: ${l==null?void 0:l.message}`,status:"error",duration:3e3,isClosable:!0})},fetchPolicy:"cache-first",errorPolicy:"all"},s))},xp=f`
  mutation saveNote($rawId: Int, $note: String) @api(name: "appZim") {
    saveNote(rawId: $rawId, note: $note) {
      note
      createBy
      createDate
    }
  }
`,Np=f`
  mutation updateStatusLeads($input: updateStatusLeadsInputType)
  @api(name: "appZim") {
    updateStatusLeads(input: $input) {
      success
      error
      message
      dataString
    }
  }
`,Ep=f`
  mutation makeApointment($input: MakeApointmentInputType)
  @api(name: "appZim") {
    makeApointment(input: $input) {
      success
      error
      message
      dataString
    }
  }
`,vp=f`
  mutation changeEcLeads($input: ChangeEcLeadsInputType) @api(name: "appZim") {
    changeEcLeads(input: $input) {
      success
      error
      message
      dataString
    }
  }
`,Mm=f`
  mutation addCustomerLeads($input: AddCustomerLeadInputType)
  @api(name: "appZim") {
    addCustomerLeads(input: $input) {
      success
      error
      message
      dataString
    }
  }
`,Ap=f`
  query getReportLeads($startDate: String, $endDate: String)
  @api(name: "appZim") {
    getReportLeads(startDate: $startDate, endDate: $endDate) {
      date
      totalView
      dataLeads {
        date
        sourseName
        data
      }
    }
  }
`,Wl=f`
  query getRawDataPagination(
    $q: String
    $status: RawDataStatusEnum
    $fromDate: DateTime
    $toDate: DateTime
    $orderBy: String
    $order: String
    $page: Int
    $limit: Int
    $ecId: Int
    $schoolId: Int
    $tags: [String]
    $date: DateTime
    $sourceName: String
    $isOnlyDataFacebook: Boolean
  ) @api(name: "appZim") {
    getRawDataPagination(
      q: $q
      status: $status
      fromDate: $fromDate
      toDate: $toDate
      orderBy: $orderBy
      order: $order
      page: $page
      limit: $limit
      ecId: $ecId
      schoolId: $schoolId
      tags: $tags
      date: $date
      sourceName: $sourceName
      isOnlyDataFacebook: $isOnlyDataFacebook
    ) {
      hasNextPage
      hasPrevPage
      limit
      page
      pageNext
      pagePrev
      pagingCounter
      totalPages
      totalDocs
      docs {
        id
        student {
          id
          user_name
          full_name
          phone
          address
          email
          role_id
          role_name
          supporter
          student_more_info {
            identity_card_city_id
            note_home
            birthday
            city_id
            city_name
            district_id
            district_name
            ward_id
            ward_name
            street_id
            street_name
            home_number
            source_id
            source_name
            job_id
            job_name
            identity_card
            identity_card_city_name
            identity_card_date
            learning_status_id
            work_place
            learning_status_name
          }
        }
        fb_user_id
        source_id
        ec_name
        ec_id
        source_name
        form_name
        link
        note
        created_date
        appoinment_test_id
        status
        tags {
          level_name
          name
        }
      }
    }
  }
`,Tp=f`
  query getRawDataNote($rawId: Int) @api(name: "appZim") {
    getRawDataNote(rawId: $rawId) {
      note
      createBy
      createDate
    }
  }
`,yp=f`
  query facebookCommentsPagination(
    $page: Int
    $limit: Int
    $fbProfileId: String
    $postId: ObjectID
    $search: String
  ) @api(name: "crawler") {
    facebookCommentsPagination(
      page: $page
      limit: $limit
      search: $search
      postId: $postId
      fbProfileId: $fbProfileId
    ) {
      totalDocs
      totalPages
      docs {
        id
        post {
          id
          link
        }
        content
        facebookId
        createdAt
        updatedAt
        owner
        linkProfile
        inDemand
        fbProfileId
      }
    }
  }
`,Dp=f`
  query facebookPostsPagination(
    $page: Int
    $limit: Int
    $fbProfileId: String
    $search: String
  ) @api(name: "crawler") {
    facebookPostsPagination(
      page: $page
      limit: $limit
      search: $search
      fbProfileId: $fbProfileId
    ) {
      totalDocs
      totalPages
      docs {
        id
        content
        facebookId
        numOfComments
        numOfLikes
        numOfShares
        numOfCmtInDemand
        link
        featured
        createdAt
        updatedAt
        inDemand
        postOwnerId
        postOwnerName
      }
    }
  }
`;var ue="/Users/zimdev/work/zim-app/src/components/Forms/CreateLead/index.tsx";const qm=async(e,s)=>{var o,l,a,i,n;if(e==="")return!1;try{const r=await ts.query({query:Wl,variables:{limit:1,q:e,page:1}});return!(!((l=(o=r.data)==null?void 0:o.getRawDataPagination)==null?void 0:l.docs)||((n=(i=(a=r.data)==null?void 0:a.getRawDataPagination)==null?void 0:i.docs)==null?void 0:n.length)>0)}catch{return!1}},zm=Le.object().shape({phone:Le.string().matches(Ul,"S\u1ED1 \u0111i\u1EC7n tho\u1EA1i kh\xF4ng h\u1EE3p l\u1EC7").required("S\u1ED1 \u0111i\u1EC7n tho\u1EA1i l\xE0 b\u1EAFt bu\u1ED9c").test("phone","\u0110\xE3 c\xF3 EC ch\u0103m s\xF3c S\u0110T n\xE0y. Vui l\xF2ng ki\u1EC3m tra l\u1EA1i trong lead",qm),name:Le.string().required("Vui l\xF2ng nh\u1EADp h\u1ECD t\xEAn"),email:Le.string().email("\u0110\u1ECBa ch\u1EC9 email kh\xF4ng h\u1EE3p l\u1EC7").required("Vui l\xF2ng nh\u1EADp email"),source:Le.mixed().nullable(!0).required("M\u1EE5c n\xE0y l\xE0 b\u1EAFt bu\u1ED9c"),note:Le.string().required("Vui l\xF2ng nh\u1EADp ghi ch\xFA ")}),Ms=e=>t.exports.jsxDEV(Yi,x({w:{base:"100%",md:"50%"},p:2},e),void 0,!1,{fileName:ue,lineNumber:68,columnNumber:3},globalThis),Om=({onClose:e,isOpen:s})=>{const{handleSubmit:o,register:l,control:a,formState:{errors:i},reset:n}=bs({mode:"onSubmit",reValidateMode:"onBlur",resolver:Hi(zm)}),{data:r,loading:u}=Fm(),g=Je(),[b,{loading:m}]=Pe(Mm,{refetchQueries:[Wl],onCompleted:({addCustomerLeads:d})=>{var h;d.success?(g({title:"Th\xE0nh c\xF4ng !",description:"T\u1EA1o lead th\xE0nh c\xF4ng !",status:"success",duration:3e3,isClosable:!0}),n(),e()):g({title:"T\u1EA1o lead th\u1EA5t b\u1EA1i !",description:(h=d==null?void 0:d.message)!=null?h:"Vui l\xF2ng ki\u1EC3m tra l\u1EA1i th\xF4ng tin \u0111\xE3 nh\u1EADp",status:"error",duration:3e3,isClosable:!0})},onError:d=>{var h;g({title:"T\u1EA1o lead th\u1EA5t b\u1EA1i !",description:(h=d==null?void 0:d.message)!=null?h:"Vui l\xF2ng ki\u1EC3m tra l\u1EA1i th\xF4ng tin",status:"error",duration:3e3,isClosable:!0})}}),v=async d=>{try{await b({variables:{input:{phoneNumber:d.phone,fullName:d.name,email:d.email,sourceId:d.source.id,note:d.note}}})}catch(h){console.log({e:h})}};return t.exports.jsxDEV(Vt,{isOpen:s,onClose:()=>{},size:"3xl",children:[t.exports.jsxDEV(Nt,{},void 0,!1,{fileName:ue,lineNumber:147,columnNumber:7},globalThis),t.exports.jsxDEV(kt,{children:t.exports.jsxDEV("form",{onSubmit:o(v),children:[t.exports.jsxDEV(Et,{children:"T\u1EA1o Lead m\u1EDBi"},void 0,!1,{fileName:ue,lineNumber:150,columnNumber:11},globalThis),t.exports.jsxDEV(wt,{onClick:e},void 0,!1,{fileName:ue,lineNumber:151,columnNumber:11},globalThis),t.exports.jsxDEV(S,{px:4,pb:8,children:[t.exports.jsxDEV(Us,{spacing:0,children:[t.exports.jsxDEV(Ms,{children:t.exports.jsxDEV(Ge,y(x({label:"S\u1ED1 \u0111i\u1EC7n tho\u1EA1i (*)",placeholder:"Nh\u1EADp...",variant:"outline",pr:10},l("phone")),{error:i==null?void 0:i.phone}),void 0,!1,{fileName:ue,lineNumber:155,columnNumber:17},globalThis)},void 0,!1,{fileName:ue,lineNumber:154,columnNumber:15},globalThis),t.exports.jsxDEV(Ms,{children:t.exports.jsxDEV(Ge,y(x({},l("name")),{label:"H\u1ECD t\xEAn (*)",placeholder:"Nh\u1EADp...",error:i==null?void 0:i.name}),void 0,!1,{fileName:ue,lineNumber:166,columnNumber:17},globalThis)},void 0,!1,{fileName:ue,lineNumber:165,columnNumber:15},globalThis),t.exports.jsxDEV(Ms,{children:t.exports.jsxDEV(Ge,y(x({label:"Email (*)",placeholder:"Nh\u1EADp..."},l("email")),{error:i==null?void 0:i.email}),void 0,!1,{fileName:ue,lineNumber:174,columnNumber:17},globalThis)},void 0,!1,{fileName:ue,lineNumber:173,columnNumber:15},globalThis),t.exports.jsxDEV(Ms,{children:t.exports.jsxDEV(Kt,{name:"source",control:a,render:({field:d})=>t.exports.jsxDEV(ws,x({label:"Ngu\u1ED3n (*)",error:i==null?void 0:i.source,getOptionLabel:h=>h.sourceOfCustomer,getOptionValue:h=>h.id,options:r?r.getSourceOfCustomer:[],isLoading:u},d),void 0,!1,{fileName:ue,lineNumber:186,columnNumber:21},globalThis)},void 0,!1,{fileName:ue,lineNumber:182,columnNumber:17},globalThis)},void 0,!1,{fileName:ue,lineNumber:181,columnNumber:15},globalThis),t.exports.jsxDEV(Yi,{w:"full",p:2,children:t.exports.jsxDEV(_m,y(x({label:"Ghi ch\xFA (*)",bg:"white",minRows:2,placeholder:"N\u1ED9i dung..."},l("note")),{error:i==null?void 0:i.note}),void 0,!1,{fileName:ue,lineNumber:199,columnNumber:17},globalThis)},void 0,!1,{fileName:ue,lineNumber:198,columnNumber:15},globalThis)]},void 0,!0,{fileName:ue,lineNumber:153,columnNumber:13},globalThis),t.exports.jsxDEV(q,{px:2,mt:4,spacing:4,children:[t.exports.jsxDEV(Y,{type:"submit",colorScheme:"green",isLoading:m,children:"T\u1EA1o m\u1EDBi"},void 0,!1,{fileName:ue,lineNumber:210,columnNumber:15},globalThis),t.exports.jsxDEV(Y,{variant:"outline",onClick:e,children:"H\u1EE7y b\u1ECF"},void 0,!1,{fileName:ue,lineNumber:213,columnNumber:15},globalThis)]},void 0,!0,{fileName:ue,lineNumber:209,columnNumber:13},globalThis)]},void 0,!0,{fileName:ue,lineNumber:152,columnNumber:11},globalThis)]},void 0,!0,{fileName:ue,lineNumber:149,columnNumber:9},globalThis)},void 0,!1,{fileName:ue,lineNumber:148,columnNumber:7},globalThis)]},void 0,!0,{fileName:ue,lineNumber:146,columnNumber:5},globalThis)};var Ql="/Users/zimdev/work/zim-app/src/pages/chat/manageParticipantDetail/components/listUserInfoForAdmin.tsx";const Lm=c.exports.memo(({conversationDetail:e})=>{const s=c.exports.useMemo(()=>{var o;return(o=e==null?void 0:e.participants)==null?void 0:o.map(l=>{var a;return t.exports.jsxDEV(ss,{avatarUrl:l==null?void 0:l.avatar,username:l==null?void 0:l.fullName,positionName:(a=ao.find(i=>{var n;return i.id===Number((n=l.roleId)!=null?n:14)}))==null?void 0:a.name},l==null?void 0:l.userId,!1,{fileName:Ql,lineNumber:10,columnNumber:9},globalThis)})},[e]);return t.exports.jsxDEV(S,{borderBottomWidth:"1px",borderColor:"whiteAlpha.300",children:s},void 0,!1,{fileName:Ql,lineNumber:22,columnNumber:5},globalThis)});var Ce="/Users/zimdev/work/zim-app/src/pages/chat/manageParticipantDetail/index.tsx";const Bm=()=>{var N,$,k,C,F,Z,z,G,P,H,I,B,W,re;const{onOpen:e,isOpen:s,onClose:o}=Xe(),{isOpen:l,onOpen:a,onClose:i}=Xe(),n=V(w=>w.userId),[r]=ne(),u=V(w=>w.visibleUserProfile),g=V(w=>w.setVisibleUserProfile),b=V(w=>w.conversationId),{conversationDetail:m}=ql(b,n),v=Ol({conversationDetail:m}),d=(N=m==null?void 0:m.participants)==null?void 0:N.find(w=>(w==null?void 0:w.roleId)==="5"||(w==null?void 0:w.roleId)===null),h=v?d:($=m==null?void 0:m.participants)==null?void 0:$.filter(w=>(w==null?void 0:w.userId)!==(r==null?void 0:r.id.toString()))[0],{userData:A,loading:E}=zl(n||(h==null?void 0:h.userId),m.name),T=v?(h==null?void 0:h.fullName)||((k=m==null?void 0:m.owner)==null?void 0:k.fullName):(A==null?void 0:A.full_name)?A==null?void 0:A.full_name:(h==null?void 0:h.isGuest)?`${h==null?void 0:h.fullName}`:h==null?void 0:h.fullName;ul("click",w=>{const O=w.target;!O.closest(".profile")&&!O.closest("#js-toggle-profile")&&g(!1)});const D=Jt({base:"",xl:"visible"});return!n&&!b?null:t.exports.jsxDEV(t.exports.Fragment,{children:[t.exports.jsxDEV(ve,{pos:D?void 0:"fixed",top:D?0:"56px",right:0,bottom:0,zIndex:51,h:"full",overflowY:"auto",bg:j("white","slate.900"),w:{base:"320px","2xl":"380px"},transform:D||u?"none":"translateX(100%)",transition:"all .3s ease",className:"profile",id:"profile-portal",spacing:0,children:[!(m==null?void 0:m.name)&&(((C=m==null?void 0:m.participants)==null?void 0:C.length)===2&&((F=m==null?void 0:m.participants)==null?void 0:F.find(w=>w.userId===r.id.toString()))||n||v&&Jl.includes(r==null?void 0:r.roleId))&&t.exports.jsxDEV(S,{children:[t.exports.jsxDEV(Am,{tags:(Z=m==null?void 0:m.tags)!=null?Z:[],isStudent:qs.includes(r==null?void 0:r.roleId),participantAvatarUrl:h==null?void 0:h.avatar,fullName:T,loading:E,participants:m==null?void 0:m.participants,participantInfo:A},void 0,!1,{fileName:Ce,lineNumber:116,columnNumber:15},globalThis),Hl.create.includes(r==null?void 0:r.roleId)&&(h==null?void 0:h.isGuest)&&t.exports.jsxDEV(S,{p:2,borderBottomWidth:1,borderColor:"whiteAlpha.300",children:[t.exports.jsxDEV(Y,{w:"full",colorScheme:"green",onClick:a,children:"Th\xEAm lead m\u1EDBi"},void 0,!1,{fileName:Ce,lineNumber:128,columnNumber:21},globalThis),Hl.create.includes(r==null?void 0:r.roleId)&&t.exports.jsxDEV(Om,{isOpen:l,onClose:i},void 0,!1,{fileName:Ce,lineNumber:136,columnNumber:23},globalThis)]},void 0,!0,{fileName:Ce,lineNumber:127,columnNumber:19},globalThis),(A==null?void 0:A.supporter)&&!qs.includes(r==null?void 0:r.roleId)&&((G=(z=m==null?void 0:m.participants)==null?void 0:z.filter(w=>w.roleId!=="5"))==null?void 0:G.map(w=>{var se;const O=(w==null?void 0:w.userId)===((se=A==null?void 0:A.supporter)==null?void 0:se.id.toString());return t.exports.jsxDEV(ss,{username:w==null?void 0:w.fullName,positionName:`${O?"EC ch\xEDnh":"EC h\u1ED7 tr\u1EE3"}`},w==null?void 0:w.id,!1,{fileName:Ce,lineNumber:151,columnNumber:23},globalThis)})),((P=A==null?void 0:A.supporter)==null?void 0:P.id)===Number(r==null?void 0:r.id)&&((H=m==null?void 0:m.participants)==null?void 0:H.length)&&t.exports.jsxDEV(S,{w:"full",px:2,children:t.exports.jsxDEV(Y,{onClick:e,w:"full",rightIcon:t.exports.jsxDEV(on,{},void 0,!1,{fileName:Ce,lineNumber:167,columnNumber:34},globalThis),colorScheme:"blue",variant:"outline",children:"Th\xEAm EC h\u1ED7 tr\u1EE3"},void 0,!1,{fileName:Ce,lineNumber:164,columnNumber:21},globalThis)},void 0,!1,{fileName:Ce,lineNumber:163,columnNumber:19},globalThis)]},void 0,!0,{fileName:Ce,lineNumber:115,columnNumber:13},globalThis),v&&!(m==null?void 0:m.name)&&t.exports.jsxDEV(Lm,{conversationDetail:m},void 0,!1,{fileName:Ce,lineNumber:178,columnNumber:11},globalThis),(m==null?void 0:m.name)&&t.exports.jsxDEV(wm,{conversationDetail:m},void 0,!1,{fileName:Ce,lineNumber:181,columnNumber:11},globalThis),b&&t.exports.jsxDEV(jm,{isSuperAdmin:Kl.includes(r==null?void 0:r.roleId),allowForSupperAdmin:Kl.includes(r==null?void 0:r.roleId)&&((I=m==null?void 0:m.participants)==null?void 0:I.some(w=>(w==null?void 0:w.userId)===(r==null?void 0:r.id.toString())))},void 0,!1,{fileName:Ce,lineNumber:185,columnNumber:11},globalThis),((B=A==null?void 0:A.courseStudents)==null?void 0:B.length)>0&&!qs.includes(r==null?void 0:r.roleId)&&t.exports.jsxDEV(Pm,{data:(W=A==null?void 0:A.courseStudents)!=null?W:[]},void 0,!1,{fileName:Ce,lineNumber:197,columnNumber:13},globalThis),t.exports.jsxDEV(Vm,{},void 0,!1,{fileName:Ce,lineNumber:199,columnNumber:9},globalThis)]},void 0,!0,{fileName:Ce,lineNumber:83,columnNumber:7},globalThis),u&&t.exports.jsxDEV(t.exports.Fragment,{children:t.exports.jsxDEV(S,{bg:"blackAlpha.800",shadow:"md",pos:"fixed",top:14,bottom:0,zIndex:50,left:0,right:0,h:"screen"},void 0,!1,{fileName:Ce,lineNumber:203,columnNumber:11},globalThis)},void 0,!1),t.exports.jsxDEV(mo,{isChannel:!1,isEdit:!0,isAddSupporter:!0,isOpen:s,onClose:o,listParticipants:(re=m==null?void 0:m.participants)==null?void 0:re.map(w=>w==null?void 0:w.userId),name:m==null?void 0:m.name,groupId:b},void 0,!1,{fileName:Ce,lineNumber:216,columnNumber:7},globalThis)]},void 0,!0)};var Ct="/Users/zimdev/work/zim-app/src/pages/chat/index.tsx";const V=ln(e=>({conversationId:"",setConversationId:s=>e({conversationId:s}),userId:"",setUserId:s=>e({userId:s}),tab:"conversation",setTab:s=>e({tab:s}),channel:"",setChannel:s=>e({channel:s}),participants:[],setParticipants:s=>e({participants:s}),visibleUserProfile:!1,setVisibleUserProfile:s=>e({visibleUserProfile:s}),searchStringMessage:"",setSearchStringMessage:s=>e({searchStringMessage:s}),searchResult:[],setSearchResult:s=>e({searchResult:s}),nearSearchResult:[],setNearSearchResult:s=>e({nearSearchResult:s}),messageId:"",setMessageId:s=>e({messageId:s}),beforeId:"",setBeforeId:s=>e({beforeId:s}),afterId:"",setAfterId:s=>e({afterId:s})})),Zl=()=>{const e=V(h=>h.setChannel),s=V(h=>h.userId),o=V(h=>h.setUserId),l=V(h=>h.tab),a=V(h=>h.conversationId),i=V(h=>h.setConversationId),[n]=ne(),[r,u]=vt({}),{height:g}=ju(),b=Jt({base:"",md:"visible"});oc();const m=r.get("qsConversationId"),v=r.get("qsUserId"),d=r.get("qsGroup");return c.exports.useEffect(()=>{(n==null?void 0:n.roleId)===6?e("guest"):e("my chats")},[n]),c.exports.useEffect(()=>{m||v?(m&&i(m),v&&o(v),d&&e(d)):(r.delete("qsConversationId"),r.delete("qsUserId"),r.delete("qsGroup"))},[m,v,d]),t.exports.jsxDEV(ie,{h:g-113,spacing:0,w:"full",children:[!b&&t.exports.jsxDEV($u,{},void 0,!1,{fileName:Ct,lineNumber:85,columnNumber:27},globalThis),t.exports.jsxDEV(S,{w:"100%",h:"100%",display:"flex",className:"divide-x",overflow:"hidden",children:[(b||!b&&l==="channel")&&t.exports.jsxDEV(ed,{},void 0,!1,{fileName:Ct,lineNumber:94,columnNumber:11},globalThis),(b||!b&&l==="conversation")&&t.exports.jsxDEV(tc,{},void 0,!1,{fileName:Ct,lineNumber:97,columnNumber:11},globalThis),(b||!b&&l==="message")&&t.exports.jsxDEV(cm,{},void 0,!1,{fileName:Ct,lineNumber:100,columnNumber:11},globalThis),(s||a)&&t.exports.jsxDEV(Bm,{},void 0,!1,{fileName:Ct,lineNumber:102,columnNumber:40},globalThis)]},void 0,!0,{fileName:Ct,lineNumber:86,columnNumber:7},globalThis)]},void 0,!0,{fileName:Ct,lineNumber:84,columnNumber:5},globalThis)};var Rm=Object.freeze({__proto__:null,[Symbol.toStringTag]:"Module",useStore:V,default:Zl});const Um=()=>{const e=Je(),s=mt(),o=V(n=>n.conversationId),[l,{loading:a}]=Pe(Au);return{onDeleteTag:c.exports.useCallback(async n=>{try{await l({variables:{tagId:n,conversationId:o}}),await s(["conversationDetail"])}catch(r){e({title:"L\u1ED7i !!!",description:r.message,status:"success",duration:2e3,isClosable:!0})}},[o,l,e,s]),loading:a}};var Re="/Users/zimdev/work/zim-app/src/pages/chat/configs.tsx";function Gm({id:e,name:s,color:o}){const{onDeleteTag:l,loading:a}=Um(),i=V(n=>n.conversationId);return t.exports.jsxDEV(Ci,{size:"md",rounded:"full",children:[t.exports.jsxDEV(S,{bg:o,w:"5px",h:"5px",rounded:"full",mr:1},void 0,!1,{fileName:Re,lineNumber:22,columnNumber:7},this),t.exports.jsxDEV($i,{children:s},void 0,!1,{fileName:Re,lineNumber:23,columnNumber:7},this),a?t.exports.jsxDEV(Gt,{color:"red.500",size:"xs",ml:2.5},void 0,!1,{fileName:Re,lineNumber:25,columnNumber:9},this):i&&t.exports.jsxDEV(Si,{onClick:()=>l(e)},void 0,!1,{fileName:Re,lineNumber:27,columnNumber:27},this)]},void 0,!0,{fileName:Re,lineNumber:21,columnNumber:5},this)}const Hm=(e,s)=>{const[o]=ne();return c.exports.useMemo(()=>{var l,a;return[{icon:t.exports.jsxDEV(an,{},void 0,!1,{fileName:Re,lineNumber:38,columnNumber:15},globalThis),value:(l=e==null?void 0:e.address)!=null?l:"Kh\xF4ng c\xF3 th\xF4ng tin",visible:zs.includes(o.roleId)},{icon:t.exports.jsxDEV(rn,{},void 0,!1,{fileName:Re,lineNumber:43,columnNumber:15},globalThis),value:(e==null?void 0:e.phone)||"Kh\xF4ng c\xF3 th\xF4ng tin",visible:zs.includes(o.roleId)},{icon:t.exports.jsxDEV(nn,{},void 0,!1,{fileName:Re,lineNumber:48,columnNumber:15},globalThis),value:((a=e==null?void 0:e.more_info)==null?void 0:a.facebook)||"Kh\xF4ng c\xF3 th\xF4ng tin",visible:zs.includes(o.roleId)},{icon:t.exports.jsxDEV(un,{},void 0,!1,{fileName:Re,lineNumber:53,columnNumber:15},globalThis),value:(e==null?void 0:e.email)||"Kh\xF4ng c\xF3 th\xF4ng tin",visible:zs.includes(o.roleId)},{icon:t.exports.jsxDEV(cn,{},void 0,!1,{fileName:Re,lineNumber:58,columnNumber:15},globalThis),value:(s==null?void 0:s.length)?()=>t.exports.jsxDEV(Us,{flex:1,children:s==null?void 0:s.map((i,n)=>t.exports.jsxDEV(Gm,x({},i),n,!1,{fileName:Re,lineNumber:64,columnNumber:28},globalThis))},void 0,!1,{fileName:Re,lineNumber:62,columnNumber:17},globalThis):"Kh\xF4ng c\xF3 tag",visible:!0}]},[e,s,o])},Yl=["Admin","CC","CS","GV","EC","AM","KT","KTH","MK","BM","BP"],To=[{id:1,name:"Super Admin"},{id:2,name:"CC"},{id:4,name:"Gi\xE1o vi\xEAn"},{id:6,name:"EC"},{id:8,name:"AM"},{id:11,name:"Marketer"},{id:12,name:"BM"}],Kl=[1],qs=[5],Jl=[5],zs=[1,2,6],Wm="File c\u1EE7a b\u1EA1n v\u01B0\u1EE3t qu\xE1 10MB.",Qm="File c\u1EE7a b\u1EA1n kh\xF4ng \u0111\u01B0\u1EE3c h\u1ED7 tr\u1EE3.",Zm=[{color:"#00DAA3"},{color:"#21A3F3"},{color:"#F51C50"},{color:"#006E7F"},{color:"#F8CB2E"}];var Xl="/Users/zimdev/work/zim-app/src/pages/chat/manageChannels/channels/zimiansChannel.tsx";const Ym=c.exports.memo(({searchString:e,overallMessageUnRead:s})=>{const[o]=ne(),l=V(N=>N.setChannel),a=V(N=>N.setParticipants),i=V(N=>N.setConversationId),n=V(N=>N.setTab),r=V(N=>N.setUserId),[u,g]=vt({}),{listAccount:b,onLoadMore:m,hasNextPage:v,isLoadingMore:d,loading:h}=fl({roles:Yl,searchString:e}),A=c.exports.useCallback(async()=>{await m()},[m]),E=c.exports.useMemo(()=>h?[1,2,3,4,5,6,7,8]:(b==null?void 0:b.filter(N=>{var $;return(($=N.user)==null?void 0:$.id)!==parseInt(o==null?void 0:o.id,10)}))||[],[o,h,b]),T=c.exports.useCallback(()=>{l("zimians"),n("conversation")},[]),p=c.exports.useCallback((N,$)=>{r(N),g({qsUserId:N,qsGroup:$}),u.delete("qsConversationId"),i(""),n("message"),l($),a([])},[]),D=c.exports.useMemo(()=>t.exports.jsxDEV(pl,{canLoadMore:v,isLoading:d,onLoadMore:A,onClick:p,loading:h,conversations:E},void 0,!1,{fileName:Xl,lineNumber:57,columnNumber:9},globalThis),[E,h,v,d,p,A]);return t.exports.jsxDEV(gl,{onClick:T,title:"Zimians",overallMessageUnRead:s,children:D},void 0,!1,{fileName:Xl,lineNumber:75,columnNumber:7},globalThis)}),Km=e=>{var l;const{data:s}=De(Sn,{variables:{userId:e}}),o=s==null?void 0:s.getUserById;return{supporterInfo:o,id:(l=o==null?void 0:o.supporter)==null?void 0:l.id}},Jm=({search:e})=>{var g,b;const s=y(x({},be({isChannel:!0,search:e},m=>m)),{limit:100,offset:0}),[o,{data:l,loading:a,fetchMore:i,called:n}]=Qs(Ae,{variables:s,fetchPolicy:"network-only",nextFetchPolicy:"cache-first"});c.exports.useEffect(()=>{n||(async()=>await o())()},[n,o]);const r=((g=l==null?void 0:l.conversations)==null?void 0:g.docs)||[],u=(b=l==null?void 0:l.conversations)==null?void 0:b.hasNextPage;return Dt({variables:y(x({},s),{offset:(r==null?void 0:r.length)+1}),fetchMore:i,hasNextPage:u}),{listChannel:r,loading:a}};var ea="/Users/zimdev/work/zim-app/src/pages/chat/manageChannels/channels/studentChannel.tsx";const Xm=c.exports.memo(({searchString:e,overallMessageUnRead:s})=>{const o=V(p=>p.setChannel),l=V(p=>p.setParticipants),a=V(p=>p.setConversationId),i=V(p=>p.setTab),n=V(p=>p.setUserId),[r,u]=vt({}),{listAccount:g,hasNextPage:b,isLoadingMore:m,onLoadMore:v,loading:d}=fl({roles:["HV"],searchString:e}),h=c.exports.useMemo(()=>d?[1,2,3,4,5,6,7,8]:g,[d,g]),A=c.exports.useCallback(async()=>{await v()},[v]),E=c.exports.useCallback(()=>{o("student"),i("conversation")},[]),T=c.exports.useCallback((p,D)=>{n(p),u({qsUserId:p,qsGroup:D}),r.delete("qsConversationId"),a(""),i("message"),o(D),l([])},[]);return t.exports.jsxDEV(gl,{overallMessageUnRead:s,onClick:E,title:"Student",children:t.exports.jsxDEV(pl,{isLoading:m,canLoadMore:b,onLoadMore:A,onClick:T,loading:d,conversations:h},void 0,!1,{fileName:ea,lineNumber:51,columnNumber:11},globalThis)},void 0,!1,{fileName:ea,lineNumber:46,columnNumber:7},globalThis)});var Me="/Users/zimdev/work/zim-app/src/pages/chat/manageChannels/index.tsx";const yo=c.exports.memo(({count:e})=>t.exports.jsxDEV(mn,{w:6,h:6,variant:"solid",rounded:"full",bg:"blue.500",position:"absolute",right:8,alignItems:"center",justifyContent:"center",display:"flex",fontSize:11,children:t.exports.jsxDEV(S,{alignItems:"center",justifyContent:"center",display:"flex",children:e>99?"99+":e},void 0,!1,{fileName:Me,lineNumber:38,columnNumber:7},globalThis)},void 0,!1,{fileName:Me,lineNumber:25,columnNumber:5},globalThis)),ed=c.exports.memo(()=>{var D,N;const[e]=ne(),[s,o]=vt({}),l=V($=>$.setParticipants),a=V($=>$.setChannel),i=V($=>$.setUserId),n=V($=>$.setConversationId),r=V($=>$.setTab),[u,g]=c.exports.useState(""),b=is(u,500),{overall:m,overallMyChats:v}=au(),{id:d}=Km(Number(e==null?void 0:e.id)),{listChannel:h}=Jm(u);h==null||h.filter($=>$.name&&!["zimians","student"].includes($.group));const A=c.exports.useMemo(()=>s,[]),E=c.exports.useCallback(async()=>{o({qsUserId:d}),A.delete("qsConversationId"),A.delete("qsGroup"),a("student"),i(d),r("message"),n(""),l([])},[d]),T=c.exports.useRef(null),p=c.exports.useCallback(()=>{a("guest"),r("conversation")},[]);return c.exports.useEffect(()=>{let $;return(T==null?void 0:T.current)&&($=new fs(T==null?void 0:T.current,{wheelSpeed:1,minScrollbarLength:10})),()=>{$&&$.destroy()}},[]),t.exports.jsxDEV(At,{direction:"column",w:{base:"full",md:"200px",lg:"250px"},bg:j("white","#10172a"),h:"100%",children:[t.exports.jsxDEV(mu,{searchString:u,setSearchString:g},void 0,!1,{fileName:Me,lineNumber:105,columnNumber:7},globalThis),t.exports.jsxDEV(S,{flexGrow:1,ref:T,className:"scroll-container",overflow:"hidden",position:"relative",minH:0,children:[t.exports.jsxDEV(ve,{zIndex:50,spacing:0,position:"sticky",top:0,children:[(e==null?void 0:e.roleId)===5&&d&&t.exports.jsxDEV(S,{display:"flex",alignItems:"center",px:4,h:10,cursor:"pointer",_hover:{backgroundColor:j("white","slate.500")},onClick:E,w:"100%",rounded:"none",borderWidth:"1px 0px 0px 0px",bg:j("white","#10172a"),fontSize:16,fontWeight:500,justifyContent:"start",children:t.exports.jsxDEV(M,{children:"Chat v\u1EDBi supporter"},void 0,!1,{fileName:Me,lineNumber:137,columnNumber:15},globalThis)},void 0,!1,{fileName:Me,lineNumber:119,columnNumber:13},globalThis),t.exports.jsxDEV(S,{display:"flex",alignItems:"center",px:4,h:10,cursor:"pointer",_hover:{backgroundColor:j("white","slate.500")},onClick:()=>{a("my chats"),r("conversation")},w:"100%",rounded:"none",borderWidth:"1px 0px 0px 0px",bg:j("white","#10172a"),fontSize:16,fontWeight:500,justifyContent:"start",children:["My Chats",v>0&&t.exports.jsxDEV(yo,{count:v},void 0,!1,{fileName:Me,lineNumber:163,columnNumber:36},globalThis)]},void 0,!0,{fileName:Me,lineNumber:141,columnNumber:11},globalThis),(e==null?void 0:e.roleId)!==4&&(e==null?void 0:e.roleId)!==5&&t.exports.jsxDEV(Y,{display:"flex",alignItems:"center",px:4,h:10,cursor:"pointer",_hover:{backgroundColor:j("white","slate.500")},onClick:p,w:"100%",rounded:"none",borderWidth:"1px 0px 0px 0px",bg:j("white","#10172a"),fontSize:16,fontWeight:500,justifyContent:"start",children:["Guest",(m==null?void 0:m[3])+(m==null?void 0:m[4])>0&&t.exports.jsxDEV(yo,{count:(m==null?void 0:m[3])+(m==null?void 0:m[4])},void 0,!1,{fileName:Me,lineNumber:186,columnNumber:17},globalThis)]},void 0,!0,{fileName:Me,lineNumber:166,columnNumber:13},globalThis)]},void 0,!0,{fileName:Me,lineNumber:117,columnNumber:9},globalThis),t.exports.jsxDEV(ve,{display:{base:"block"},spacing:0,children:[(e==null?void 0:e.roleId)!==5&&t.exports.jsxDEV(Ym,{overallMessageUnRead:(D=m==null?void 0:m[0])!=null?D:null,searchString:b},void 0,!1,{fileName:Me,lineNumber:198,columnNumber:13},globalThis),t.exports.jsxDEV(Xm,{overallMessageUnRead:(N=m==null?void 0:m[1])!=null?N:null,searchString:b},void 0,!1,{fileName:Me,lineNumber:203,columnNumber:11},globalThis)]},void 0,!0,{fileName:Me,lineNumber:191,columnNumber:9},globalThis)]},void 0,!0,{fileName:Me,lineNumber:109,columnNumber:7},globalThis)]},void 0,!0,{fileName:Me,lineNumber:95,columnNumber:5},globalThis)}),td=xe(()=>Ne(()=>import("./index5.js"),["assets/index5.js","assets/index3.js","assets/vendor.js","assets/useBuildModeValue.js"])),sd=xe(()=>Ne(()=>import("./index6.js"),["assets/index6.js","assets/vendor.js","assets/index7.js","assets/index8.js","assets/dateToStringFormat.js","assets/index9.js","assets/CourseFilter.js","assets/index11.js","assets/index10.js","assets/index3.js","assets/useBuildModeValue.js"])),od=xe(()=>Ne(()=>import("./index12.js"),["assets/index12.js","assets/vendor.js","assets/dateToStringFormat.js","assets/arrayToSelectOption.js","assets/customParseFormat.js","assets/instance.js","assets/index11.js","assets/main.js"])),id=xe(()=>Ne(()=>import("./index13.js"),["assets/index13.js","assets/index10.js","assets/vendor.js","assets/index3.js","assets/useBuildModeValue.js","assets/index14.js","assets/tickets.js"])),ld=xe(()=>Ne(()=>import("./index15.js"),["assets/index15.js","assets/tickets.js","assets/vendor.js","assets/index8.js","assets/index9.js","assets/useBuildModeValue.js","assets/index3.js","assets/index7.js"])),ad=xe(()=>Ne(()=>Promise.resolve().then(function(){return Rm}),void 0)),rd=xe(()=>Ne(()=>import("./index16.js"),["assets/index16.js","assets/index14.js","assets/vendor.js","assets/index8.js","assets/index3.js","assets/useBuildModeValue.js","assets/customParseFormat.js","assets/index11.js","assets/dateToStringFormat.js","assets/index10.js"])),nd=xe(()=>Ne(()=>import("./index17.js"),["assets/index17.js","assets/vendor.js","assets/index9.js","assets/index8.js","assets/index7.js","assets/useBuildModeValue.js","assets/index3.js"])),ud=xe(()=>Ne(()=>import("./index18.js"),["assets/index18.js","assets/index14.js","assets/vendor.js","assets/main.js","assets/index19.js","assets/utils.js","assets/isSameOrAfter.js","assets/dateToStringFormat.js","assets/index20.js","assets/index10.js","assets/index3.js","assets/useBuildModeValue.js","assets/index7.js","assets/useGetCenterByUser.js","assets/instance.js","assets/arrayToSelectOption.js","assets/customParseFormat.js"])),cd=xe(()=>Ne(()=>import("./index21.js"),["assets/index21.js","assets/index22.js","assets/vendor.js","assets/useBuildModeValue.js","assets/index3.js","assets/index8.js","assets/index7.js","assets/index10.js","assets/CourseFilter.js","assets/index11.js","assets/groupByDate.js","assets/useListSubjectsOfMockTest.js","assets/arrayToSelectOption.js","assets/customParseFormat.js","assets/useGetCenterByUser.js","assets/instance.js","assets/index9.js"])),md=xe(()=>Ne(()=>import("./index23.js"),["assets/index23.js","assets/index22.js","assets/vendor.js","assets/useBuildModeValue.js","assets/index3.js","assets/index8.js","assets/index7.js","assets/index10.js","assets/CourseFilter.js","assets/index11.js","assets/groupByDate.js","assets/useListSubjectsOfMockTest.js","assets/arrayToSelectOption.js","assets/customParseFormat.js","assets/useGetCenterByUser.js","assets/instance.js","assets/index9.js"])),dd=xe(()=>Ne(()=>import("./index24.js").then(function(e){return e.i}),["assets/index24.js","assets/index7.js","assets/vendor.js","assets/index8.js","assets/groupByDate.js","assets/index20.js","assets/useBuildModeValue.js"])),pd=xe(()=>Ne(()=>import("./index25.js").then(function(e){return e.i}),["assets/index25.js","assets/main.js","assets/vendor.js","assets/dateToStringFormat.js","assets/arrayToSelectOption.js","assets/customParseFormat.js","assets/index19.js","assets/index11.js","assets/useGetCenterByUser.js","assets/instance.js"])),bd=xe(()=>Ne(()=>import("./index26.js"),["assets/index26.js","assets/index14.js","assets/vendor.js"])),gd=xe(()=>Ne(()=>import("./index27.js"),["assets/index27.js","assets/vendor.js","assets/index7.js","assets/index8.js","assets/index9.js","assets/index3.js","assets/useBuildModeValue.js","assets/index10.js","assets/index11.js"])),hd=xe(()=>Ne(()=>import("./index28.js"),["assets/index28.js","assets/index7.js","assets/vendor.js","assets/index8.js","assets/arrayToSelectOption.js","assets/customParseFormat.js","assets/useListSubjectsOfMockTest.js","assets/index9.js"])),jp=[{path:Se.home,exact:!0,component:td,allowRoles:[],breadcrumbText:"Trang ch\u1EE7"},{path:Se.courseDetail,exact:!0,component:od,allowRoles:[],breadcrumbText:"Kh\xF3a h\u1ECDc"},{path:Se.listCourses,exact:!0,component:sd,allowRoles:[],breadcrumbText:"Danh s\xE1ch kh\xF3a h\u1ECDc"},{path:`${Se.ticket}`,exact:!0,component:ld,allowRoles:[],breadcrumbText:"Danh s\xE1ch ticket"},{path:`${Se.ticket}/:id`,exact:!0,component:id,allowRoles:[],breadcrumbText:"Chi ti\u1EBFt ticket"},{path:`${Se.leadReport}`,exact:!1,component:rd,allowRoles:[],breadcrumbText:"Lead Report"},{path:`${Se.registerTestSchedule}`,exact:!1,component:bd,allowRoles:[],breadcrumbText:"\u0110\u0103ng k\xFD l\u1ECBch tr\u1EF1c test"},{path:`${Se.lead}`,exact:!1,component:gd,allowRoles:[],breadcrumbText:"Ngu\u1ED3n kh\xE1ch h\xE0ng"},{path:`${Se.studentSupportByEc}`,exact:!1,component:nd,allowRoles:[],breadcrumbText:"Danh s\xE1ch h\u1ECDc vi\xEAn"},{path:Se.chat,exact:!0,component:ad,allowRoles:[]},{path:Se.scheduleRegistration,exact:!0,component:ud,allowRoles:[],breadcrumbText:"\u0110\u0103ng k\xFD l\u1ECBch l\xE0m vi\u1EC7c"},{path:Se.listSchedules,exact:!0,component:pd,allowRoles:[],breadcrumbText:"Danh s\xE1ch l\u1ECBch l\xE0m vi\u1EC7c"},{path:Se.paperBasedTestBank,exact:!0,component:hd,allowRoles:[],breadcrumbText:"Danh s\xE1ch \u0111\u1EC1 thi"},{path:Se.mockTestOnline,exact:!0,component:cd,allowRoles:[],breadcrumbText:"Mock test online"},{path:Se.mockTestOffline,exact:!0,component:md,allowRoles:[],breadcrumbText:"Mock test offline"},{path:`${Se.mockTestDetail}/:id`,exact:!0,component:dd,allowRoles:[],breadcrumbText:"Mock test detail"}];Le.object().shape({username:Le.string().required("Vui l\xF2ng nh\u1EADp t\xE0i kho\u1EA3n"),password:Le.string().required("Vui l\xF2ng nh\u1EADp m\u1EADt kh\u1EA9u")});var rs="/Users/zimdev/work/zim-app/src/components/ErrorFallback/index.tsx";function fd({error:e,componentStack:s,resetErrorBoundary:o}){return t.exports.jsxDEV(dn,{role:"alert",status:"error",as:ie,children:[t.exports.jsxDEV(pn,{p:4,d:"block",children:[t.exports.jsxDEV("pre",{children:e.message},void 0,!1,{fileName:rs,lineNumber:8,columnNumber:9},this),t.exports.jsxDEV("pre",{children:s},void 0,!1,{fileName:rs,lineNumber:9,columnNumber:9},this)]},void 0,!0,{fileName:rs,lineNumber:7,columnNumber:7},this),t.exports.jsxDEV(Y,{onClick:o,children:"Try again"},void 0,!1,{fileName:rs,lineNumber:11,columnNumber:7},this)]},void 0,!0,{fileName:rs,lineNumber:6,columnNumber:5},this)}var $t="/Users/zimdev/work/zim-app/src/App.tsx";xe(()=>Ne(()=>import("./index2.js"),["assets/index2.js","assets/vendor.js","assets/index3.js","assets/useBuildModeValue.js"]));xe(()=>Ne(()=>import("./index4.js"),["assets/index4.js","assets/vendor.js","assets/index3.js","assets/useBuildModeValue.js"]));function xd(){const[e,s]=c.exports.useState(!0),{appSetLogin:o,appSetLogout:l,appState:a}=Kn(),i=Jn(),{loaded:n}=a,r=()=>{};return c.exports.useEffect(()=>{if(!!i()){o(il("token")),Object.values(global.wsClients).forEach(u=>{u.connect()}),s(!1);return}},[i()]),t.exports.jsxDEV(bn,{FallbackComponent:fd,onError:r,children:t.exports.jsxDEV(gn,{children:e||!n?t.exports.jsxDEV(q,{w:"100%",h:"100vh",justifyContent:"center",alignItems:"center",children:t.exports.jsxDEV(ls,{text:"\u0110ang l\u1EA5y d\u1EEF li\u1EC7u..."},void 0,!1,{fileName:$t,lineNumber:82,columnNumber:13},this)},void 0,!1,{fileName:$t,lineNumber:76,columnNumber:11},this):t.exports.jsxDEV(hn,{children:t.exports.jsxDEV(fn,{path:"*",element:t.exports.jsxDEV(Zl,{},void 0,!1,{fileName:$t,lineNumber:86,columnNumber:40},this)},void 0,!1,{fileName:$t,lineNumber:86,columnNumber:13},this)},void 0,!1,{fileName:$t,lineNumber:85,columnNumber:11},this)},void 0,!1,{fileName:$t,lineNumber:74,columnNumber:7},this)},void 0,!1,{fileName:$t,lineNumber:73,columnNumber:5},this)}const Nd={secondary:"#c89934",primary:"#3463c8",gray:{50:"#F8F8F8",100:"#F0F0F0",200:"#E6E6E6",300:"#D5D5D5",400:"#B0B0B0",500:"#909090",600:"#686868",700:"#555555",800:"#373737",900:"#171717"},secondBrand:{50:"#f8fae8",100:"#eff2c5",200:"#e5e9a0",300:"#dce07d",400:"#d6da66",500:"#d1d551",600:"#cfc449",700:"#ccae3f",800:"#c89934",900:"#c07722"},brand:{50:"#e4f2ff",100:"#bedeff",200:"#94caff",300:"#6bb5ff",400:"#4fa4ff",500:"#3463c8",600:"#2e44a8",700:"#243b9c",800:"#1b3190",900:"#081f7c"},pink:{50:"#f9e4ec",100:"#f2bcd0",200:"#eb92b1",300:"#e36993",400:"#dd4d7c",500:"#d93767",600:"#c83463",700:"#b2305d",800:"#9d2c58",900:"#77264c"},blue:{50:"#E1F2FC",100:"#B7DDFA",200:"#88C8F7",300:"#55B3F4",400:"#21A3F3",500:"#0093F1",600:"#0085E4",700:"#0074D1",800:"#0063BF",900:"#0045A1"},green:{50:"#E8F5E9",100:"#C8E6C9",200:"#A5D6A7",300:"#81C784",400:"#66BB6A",500:"#4CAF50",600:"#43A047",700:"#388E3C",800:"#2E7D32",900:"#1B5E20"},red:{50:"#FFEAED",100:"#FFCBD5",200:"#F1959E",300:"#E9686B",400:"#F54258",500:"#FC243E",600:"#EC163D",700:"#DA0037",800:"#CD002F",900:"#BF0022"},slate:{50:"#f8fafc",100:"#f1f5f9",200:"#e2e8f0",300:"#cbd5e1",400:"#94a3b8",500:"#64748b",600:"#475569",700:"#334155",800:"#1e293b",900:"#0f172a"},orange:{50:"#FFF4E2",100:"#FFE1B6",200:"#FFCE88",300:"#FFBA5A",400:"#FFAB3A",500:"#FF9C27",600:"#FF9125",700:"#FA8123",800:"#F37121",900:"#E9581E"}},Ed={baseStyle:({colorMode:e})=>{const s=e==="light";return{display:"block",background:s?"white":"slate.800",gap:6,border:"1px solid",borderColor:s?"gray.200":"slate.700"}},variants:{rounded:{padding:8,borderRadius:"xl"},smooth:{padding:4,borderRadius:"base"}},defaultProps:{variant:"smooth"}},vd={baseStyle:({colorMode:e})=>{const s=e==="dark";return{table:{th:{background:s?"slate.700":"gray.50",lineHeight:1.5},td:{background:s?"slate.800":"white",lineHeight:1.5}}}},variants:{simple:({colorMode:e})=>{const o=e==="dark"?"slate.700":"gray.300";return{td:{borderBottomColor:o,paddingTop:2,paddingBottom:2,paddingInlineStart:4,paddingInlineEnd:4},th:{borderBottomColor:o,paddingInlineStart:4,paddingInlineEnd:4,paddingTop:2,paddingBottom:2}}}},defaultProps:{variant:"simple",size:"sm"}},Ad={baseStyle:({colorMode:e})=>({textTransform:"capitalize",padding:"0.25rem 0.5rem ",borderRadius:4}),variants:{},defaultProps:{variant:"subtle",size:"sm"}},Td={baseStyle:{_invalid:{boxShadow:0}},variants:{outline:{display:"none"}},defaultProps:{variant:"outline"}},yd={baseStyle:{_hover:{textDecoration:"none",color:"brand.500"}}},Dd={baseStyle:({colorMode:e})=>({dialog:{background:e==="light"?"white":"slate.800"}})},jd={baseStyle:({colorMode:e})=>({dialog:{background:e==="light"?"white":"slate.800"}})};var Cd={Card:Ed,Table:vd,Badge:Ad,Input:Td,Link:yd,Modal:Dd,Drawer:jd};const $d={initialColorMode:"light",useSystemColorMode:!1},Sd={sm:"320px",md:"768px",lg:"960px",xl:"1280px","2xl":"1536px"},ta=xn({config:$d,colors:Nd,breakpoints:Sd,styles:{global:e=>({"html, body":{color:e.colorMode==="dark"?"gray.100":"gray.900",lineHeight:1.5},a:{color:e.colorMode==="dark"?"blue.300":"blue.500"},ul:{paddingRight:1}})},semanticTokens:{text:{default:"gray.900",_dark:"slate.400"},colors:{error:{default:"red.500",_dark:"red.200"},"gray.500":{default:"gray.500",_dark:"slate.400"}}},fonts:{body:"'Roboto', sans-serif",heading:"'Arial', sans-serif",mono:"monospace"},components:y(x({},Cd),{FormLabel:{baseStyle:{fontSize:"sm",marginBottom:1}},Avatar:{defaultProps:{colorScheme:"gray"}}})},Nn({colorScheme:"gray",components:["Avatar"]}));var ns="/Users/zimdev/work/zim-app/src/main.tsx";En.render(t.exports.jsxDEV(St.StrictMode,{children:t.exports.jsxDEV(Yn,{children:t.exports.jsxDEV(vn,{theme:ta,children:[t.exports.jsxDEV(An,{initialColorMode:ta.config.initialColorMode},void 0,!1,{fileName:ns,lineNumber:13,columnNumber:9},globalThis),t.exports.jsxDEV(xd,{},void 0,!1,{fileName:ns,lineNumber:14,columnNumber:9},globalThis)]},void 0,!0,{fileName:ns,lineNumber:12,columnNumber:7},globalThis)},void 0,!1,{fileName:ns,lineNumber:11,columnNumber:5},globalThis)},void 0,!1,{fileName:ns,lineNumber:10,columnNumber:3},globalThis),document.getElementById("root"));export{op as $,Qd as A,lp as B,hp as C,Yd as D,nu as E,xm as F,wd as G,Ap as H,As as I,Id as J,ap as K,ls as L,np as M,up as N,Jd as O,mp as P,dp as Q,Nd as R,Kd as S,Pl as T,bp as U,fp as V,ml as W,yp as X,Dp as Y,Hd as Z,Ne as _,_l as a,Tp as a0,Wl as a1,Np as a2,_m as a3,xp as a4,Ud as a5,Bd as a6,Md as a7,Rd as a8,Ld as a9,Od as aa,Ul as ab,Fm as ac,Pd as ad,Ep as ae,vp as af,Hl as ag,Om as ah,cp as ai,Vn as aj,pp as ak,_d as al,Gd as b,Se as c,Wd as d,ws as e,Xn as f,zd as g,Xd as h,Fd as i,qd as j,ne as k,gp as l,mt as m,gm as n,tp as o,Ge as p,Zd as q,jp as r,ep as s,rp as t,Jn as u,il as v,lu as w,Ps as x,sp as y,ip as z};
