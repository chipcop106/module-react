var E=Object.defineProperty,P=Object.defineProperties;var g=Object.getOwnPropertyDescriptors;var T=Object.getOwnPropertySymbols;var _=Object.prototype.hasOwnProperty,L=Object.prototype.propertyIsEnumerable;var l=(s,i,e)=>i in s?E(s,i,{enumerable:!0,configurable:!0,writable:!0,value:e}):s[i]=e,r=(s,i)=>{for(var e in i||(i={}))_.call(i,e)&&l(s,e,i[e]);if(T)for(var e of T(i))L.call(i,e)&&l(s,e,i[e]);return s},d=(s,i)=>P(s,g(i));import{Q as p,c as I,P as u}from"./mock-test.js";import{g as t,ad as $}from"./vendor.js";const j={province:null,center:{label:"T\u1EA5t c\u1EA3",value:0},status:{label:"T\u1EA5t c\u1EA3",value:0},startDate:null,endDate:null,subject:{name:"T\u1EA5t c\u1EA3",id:0}},k=2,G=1,M=1,h=6;t`
  mutation createIELTSPracticeTest(
    $type: IeltsPracticeTestType!
    $level: IeltsPracticeTestLevel!
    $title: String
    $audio: Upload
    $parts: [IeltsPracticeTestPartInput]
    $format: IeltsPracticeTestFormat
  ) @api(name: "zim") {
    createIELTSPracticeTest(
      type: $type
      level: $level
      title: $title
      audio: $audio
      parts: $parts
      format: $format
    ) {
      id
      title
      type
      level
      status
      parts {
        question
        questionGroupId
      }
    }
  }
`;t`
  mutation updateIELTSPracticeTest(
    $id: ObjectID!
    $type: IeltsPracticeTestType!
    $level: IeltsPracticeTestLevel!
    $title: String
    $audio: Upload
    $parts: [IeltsPracticeTestPartInput]
    $format: IeltsPracticeTestFormat
  ) @api(name: "zim") {
    updateIELTSPracticeTest(
      id: $id
      type: $type
      level: $level
      title: $title
      audio: $audio
      parts: $parts
      format: $format
    ) {
      id
      type
      level
      status
      parts {
        question
        questionGroupId
      }
    }
  }
`;t`
  mutation publishIELTSPracticeTest($id: ObjectID!) @api(name: "zim") {
    publishIELTSPracticeTest(id: $id) {
      id
      type
      level
      status
      questionGroups {
        ...QuestionGroupField
      }
    }
  }
  ${p}
`;t`
  mutation createIELTSPracticeTestQuestionGroup(
    $testId: ObjectID!
    $question: String!
    $groupIndex: Int
    $title: String
  ) @api(name: "zim") {
    createIELTSPracticeTestQuestionGroup(
      testId: $testId
      question: $question
      groupIndex: $groupIndex
      title: $title
    ) {
      ...QuestionGroupField
    }
  }
  ${p}
`;t`
  mutation updateIELTSPracticeTestQuestionGroup(
    $id: ObjectID!
    $question: String!
    $title: String
    $groupIndex: Int
  ) @api(name: "zim") {
    updateIELTSPracticeTestQuestionGroup(
      id: $id
      question: $question
      title: $title
      groupIndex: $groupIndex
    ) {
      ...QuestionGroupField
    }
  }
  ${p}
`;t`
  mutation removeIELTSPracticeTestQuestionGroup($id: ObjectID!)
  @api(name: "zim") {
    removeIELTSPracticeTestQuestionGroup(id: $id)
  }
`;t`
  mutation removeIELTSPracticeTestQuestion($id: ObjectID!) @api(name: "zim") {
    removeIELTSPracticeTestQuestion(id: $id)
  }
`;t`
  mutation createIELTSPracticeTestQuestion(
    $questionGroupId: ObjectID!
    $input: IELTSPracticeTestQuestionInput!
    $questionIndex: Int
  ) @api(name: "zim") {
    createIELTSPracticeTestQuestion(
      questionGroupId: $questionGroupId
      input: $input
      questionIndex: $questionIndex
    ) {
      ...QuestionField
    }
  }
  ${I}
`;t`
  mutation updateIELTSPracticeTestQuestion(
    $id: ObjectID!
    $input: IELTSPracticeTestQuestionInput!
  ) @api(name: "zim") {
    updateIELTSPracticeTestQuestion(id: $id, input: $input) {
      ...QuestionField
    }
  }
  ${I}
`;t`
  mutation swapIELTSPracticeTestQuestionIndex(
    $firstId: ObjectID!
    $secondId: ObjectID!
  ) @api(name: "zim") {
    swapIELTSPracticeTestQuestionIndex(firstId: $firstId, secondId: $secondId) {
      id
      testId
    }
  }
`;t`
  mutation swapIELTSPracticeTestQuestionGroupIndex(
    $firstId: ObjectID!
    $secondId: ObjectID!
  ) @api(name: "zim") {
    swapIELTSPracticeTestQuestionGroupIndex(
      firstId: $firstId
      secondId: $secondId
    ) {
      id
      testId
    }
  }
`;t`
  mutation createIELTSPracticeTestGroup(
    $title: String!
    $slug: String!
    $level: IeltsPracticeTestLevel!
    $practiceTests: [ObjectID]!
    $featureMediaIds: [ObjectID]!
    $description: String
  ) @api(name: "zim") {
    createIELTSPracticeTestGroup(
      title: $title
      slug: $slug
      level: $level
      practiceTests: $practiceTests
      featureMediaIds: $featureMediaIds
      description: $description
    ) {
      ...PracticeTestField
    }
  }
  ${u}
`;t`
  mutation updateIELTSPracticeTestGroup(
    $id: ObjectID!
    $title: String!
    $slug: String!
    $level: IeltsPracticeTestLevel!
    $practiceTests: [ObjectID]!
    $featureMediaIds: [ObjectID]!
    $description: String
    $categoryIds: [ObjectID]
  ) @api(name: "zim") {
    updateIELTSPracticeTestGroup(
      id: $id
      title: $title
      slug: $slug
      level: $level
      practiceTests: $practiceTests
      featureMediaIds: $featureMediaIds
      description: $description
      categoryIds: $categoryIds
    ) {
      ...PracticeTestField
    }
  }
  ${u}
`;t`
  mutation publishIELTSPracticeTestGroup($id: ObjectID!) @api(name: "zim") {
    publishIELTSPracticeTestGroup(id: $id) {
      ...PracticeTestField
    }
  }
  ${u}
`;t`
  mutation unpublishIELTSPracticeTestGroup($id: ObjectID!) @api(name: "zim") {
    unpublishIELTSPracticeTestGroup(id: $id) {
      ...PracticeTestField
    }
  }
  ${u}
`;t`
  mutation setActiveIELTSPracticeTestGroup($id: ObjectID!) @api(name: "zim") {
    setActiveIELTSPracticeTestGroup(id: $id) {
      ...PracticeTestField
    }
  }
  ${u}
`;t`
  mutation updateIELTSPracticeTestGroupSeoConfig(
    $IELTSPracticeTestGroupId: ObjectID!
    $seo: CommonSeoInput!
  ) @api(name: "zim") {
    updateIELTSPracticeTestGroupSeoConfig(
      IELTSPracticeTestGroupId: $IELTSPracticeTestGroupId
      seo: $seo
    ) {
      seo {
        title
        description
        ogDescription
        ogImage {
          id
          type
          path
          filename
          title
          visibility
          width
          height
          usage
          createdAt
          updatedAt
        }
        ogTitle
        publisher
        noIndex
        noFollow
        canonicalUrl
        customHeadHtml
      }
    }
  }
`;const v=t`
  mutation createMockTest(
    $dates: [String]
    $school_id: Int!
    $type: EnumMockTestType
    $subjectId: Int!
  ) @api(name: "appZim") {
    createMockTest(
      dates: $dates
      school_id: $school_id
      type: $type
      subjectId: $subjectId
    ) {
      id
      slug
      type
      status
      fee
      total_slot
    }
  }
`,y=t`
  mutation updateMockTest(
    $id: Int
    $status: EnumBaseStatus
    $dates: [String]
    $school_id: Int
  ) @api(name: "appZim") {
    updateMockTest(
      id: $id
      status: $status
      dates: $dates
      school_id: $school_id
    ) {
      id
    }
  }
`,Q=t`
  mutation updateMockTestDetailV2(
    $mockTestId: Int!
    $mockTestDetail: [MockTestDetailInputV2Type]!
  ) @api(name: "appZim") {
    updateMockTestDetailV2(
      mockTestDetail: $mockTestDetail
      mockTestId: $mockTestId
    )
  }
`,A=t`
  mutation updateMockTestAttendanceV2(
    $mock_test_detail_id: Int
    $student_id: Int
    $is_missing: Boolean
  ) @api(name: "appZim") {
    updateMockTestAttendanceV2(
      mock_test_detail_id: $mock_test_detail_id
      student_id: $student_id
      is_missing: $is_missing
    ) {
      id
    }
  }
`,C=t`
  mutation uploadResultStudent($input: UploadResultMockTestOrderInputType)
  @api(name: "appZim") {
    uploadResultStudent(input: $input) {
      success
    }
  }
`,F=t`
  mutation updateResultMockTest(
    $orderId: Int
    $input: UpdateOrderDetailInputType
  ) @api(name: "appZim") {
    updateResultMockTest(orderId: $orderId, input: $input) {
      message
    }
  }
`,q=t`
  mutation sentEmailResultForStudent($orderId: Int) @api(name: "appZim") {
    sentEmailResultForStudent(orderId: $orderId) {
      message
    }
  }
`,U=t`
  mutation createMockTetOfStudentSchedule(
    $input: CreateMockTestOfStudentScheduleInputType
  ) @api(name: "appZim") {
    createMockTestOfStudentSchedule(input: $input) {
      message
    }
  }
`,R=t`
  mutation updateMockTestOrderCorrectingSession(
    $input: MockTestOrderCorrectingSessionInputType
  ) @api(name: "appZim") {
    updateMockTestOrderCorrectingSession(input: $input) {
      sessionIdListening
      sessionIdReading
      sessionIdWriting
    }
  }
`,D=(s=[])=>$(s.map(e=>e.shift),"id").map(e=>{const c=s.filter(n=>n.shift.id===e.id)||[],o=c.reduce((n,a)=>(n.push(d(r({},a.subject||{}),{mockTestDetailId:a.id,ecs:a.ecs,total_slot:a.total_slot,available_slot:a.available_slot,currentSlot:a.currentSlot})),n),[]).sort((n,a)=>n.id-a.id),m=c.reduce((n,a)=>n.concat(a.ecs),[]),S=c.reduce((n,a)=>n.concat(a.teachers),[]);return d(r({},e),{subjects:$(o,"id"),ecs:m,teachers:S})});function z(s=[]){var e;const i=s.reduce(function(c,o){return c[o.date]=c[o.date]||[],c[o.date].push(o),c},Object.create(null));return(e=Object.entries(i))==null?void 0:e.map(([c,o])=>({date:c,shifts:D(o)}))}export{v as C,q as S,y as U,A as a,j as b,C as c,F as d,U as e,R as f,z as g,G as h,h as i,Q as j,M as m,k as s};
