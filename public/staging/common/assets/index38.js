var K=Object.defineProperty,ee=Object.defineProperties;var se=Object.getOwnPropertyDescriptors;var y=Object.getOwnPropertySymbols;var le=Object.prototype.hasOwnProperty,oe=Object.prototype.propertyIsEnumerable;var L=(a,s,l)=>s in a?K(a,s,{enumerable:!0,configurable:!0,writable:!0,value:l}):a[s]=l,u=(a,s)=>{for(var l in s||(s={}))le.call(s,l)&&L(a,l,s[l]);if(y)for(var l of y(s))oe.call(s,l)&&L(a,l,s[l]);return a},x=(a,s)=>ee(a,se(s));import{g as w,aA as _,ax as ie,ay as ae,h as m,j as e,V as A,aC as z,B as v,_ as T,eY as te,a0 as ne,ak as re,u as ue,H as B,dT as me,ar as ce,dg as $,p as de,v as be,w as pe,x as R,y as Ne,z as M}from"./vendor.js";import{c as p,L as U}from"./index36.js";import{a6 as fe,C as xe,o as he,p as ge,a7 as ve}from"./index.js";import{u as Te}from"./index37.js";import{P as je}from"./index7.js";import{T as Ee}from"./index9.js";import"./useListLevels.js";import"./options.js";import"./arrayToSelectOption.js";import"./useListSubCatByCat.js";const De=w`
  mutation createSetOfHandouts($input: SetOfHandoutsInputType)
  @api(name: "appZim") {
    createSetOfHandouts(input: $input) {
      id
    }
  }
`;var d="/Users/zim/Work/Zim/zim-app/src/pages/configs/set-of-handouts/components/CreateSetOfHandouts.tsx";const Ie=()=>{const a=_({defaultValues:u({},p)}),{handleSubmit:s,reset:l}=a,N=ie(),r=fe(),[f,{loading:b}]=ae(De,{onCompleted:()=>{N({title:"Th\xE0nh c\xF4ng",description:"T\u1EA1o set of handouts th\xE0nh c\xF4ng",status:"success",duration:3e3,isClosable:!0}),l(u({},p)),r(["getSetOfHandoutsPagination"])},onError:t=>{N({title:"Error",description:`Create set of handouts error: ${t.message}`,status:"error",duration:3e3,isClosable:!0})}}),h=m.exports.useCallback(async t=>{await f({variables:{input:{title:t.title,subjectId:t.subject.id,catId:t.module.id,subCatId:t.skill.id,levelIn:t.levelIn.id,levelOut:t.levelOut.id}}})},[]);return e.exports.jsxDEV(A,{align:"stretch",spacing:4,children:[e.exports.jsxDEV(z,x(u({},a),{children:e.exports.jsxDEV(U,{},void 0,!1,{fileName:d,lineNumber:70,columnNumber:9},globalThis)}),void 0,!1,{fileName:d,lineNumber:69,columnNumber:7},globalThis),e.exports.jsxDEV(v,{px:2,children:e.exports.jsxDEV(T,{colorScheme:"green",leftIcon:e.exports.jsxDEV(te,{},void 0,!1,{fileName:d,lineNumber:75,columnNumber:21},globalThis),isLoading:b,onClick:s(h),children:"T\u1EA1o"},void 0,!1,{fileName:d,lineNumber:73,columnNumber:9},globalThis)},void 0,!1,{fileName:d,lineNumber:72,columnNumber:7},globalThis)]},void 0,!0,{fileName:d,lineNumber:68,columnNumber:5},globalThis)},Ve=w`
  query getSetOfHandoutsPagination($input: PaginationSetOfHandoutsInputType)
  @api(name: "appZim") {
    getSetOfHandoutsPagination(input: $input) {
      page
      totalDocs
      docs {
        id
        title
        status
        subject {
          id
          name
        }
        cat {
          id
          name
        }
        subcat {
          id
          name
        }
        levelIn {
          id
          name
          graduation
        }
        levelOut {
          id
          name
          graduation
        }
      }
    }
  }
`;var i="/Users/zim/Work/Zim/zim-app/src/pages/configs/set-of-handouts/components/ListSetOfHandouts.tsx";const Se=()=>{var j,E;const[a,s]=m.exports.useState(0),[l,N]=m.exports.useState({label:"10 rows",value:10}),[r,f]=m.exports.useState({subjectId:0,moduleId:0,skillId:0,levelIn:0,levelOut:0}),b=_({defaultValues:u({},p)}),{handleSubmit:h,reset:t}=b,W=m.exports.useMemo(()=>({input:{page:a+1,limit:l.value,subjectId:r.subjectId,catId:r.moduleId,subcatId:r.skillId,levelIn:r.levelIn,levelOut:r.levelOut}}),[a,l,r]),{data:c,loading:Z}=ne(Ve,{variables:W}),q=Te(),G=(E=(j=c==null?void 0:c.getSetOfHandoutsPagination)==null?void 0:j.docs.map(o=>x(u({},o),{subject:o.subject.name,module:o.cat.name,skill:o.subcat.name,level:{in:o.levelIn.graduation,out:o.levelOut.graduation}})))!=null?E:[],g=c==null?void 0:c.getSetOfHandoutsPagination.totalDocs,Q=o=>{s(0),N(o)},X=!re.exports.isEqual(p,b.watch()),Y=m.exports.useCallback(()=>{t(u({},p)),f({subjectId:0,moduleId:0,skillId:0,levelIn:0,levelOut:0}),s(0)},[]),J=m.exports.useCallback(o=>{var D,I,V,S,C,O,k,H,P,F;f({subjectId:(I=(D=o.subject)==null?void 0:D.id)!=null?I:0,moduleId:(S=(V=o.module)==null?void 0:V.id)!=null?S:0,skillId:(O=(C=o.skill)==null?void 0:C.id)!=null?O:0,levelIn:(H=(k=o.levelIn)==null?void 0:k.id)!=null?H:0,levelOut:(F=(P=o==null?void 0:o.levelOut)==null?void 0:P.id)!=null?F:0}),s(0)},[]);return e.exports.jsxDEV(A,{align:"stretch",spacing:4,children:[e.exports.jsxDEV(Ee,{children:"Set of handouts"},void 0,!1,{fileName:i,lineNumber:120,columnNumber:7},globalThis),e.exports.jsxDEV(xe,{bg:ue("gray.50","slate.800"),mt:4,children:[e.exports.jsxDEV(z,x(u({},b),{children:e.exports.jsxDEV(U,{showTitleInput:!1,isRequiredFields:!1},void 0,!1,{fileName:i,lineNumber:123,columnNumber:11},globalThis)}),void 0,!1,{fileName:i,lineNumber:122,columnNumber:9},globalThis),e.exports.jsxDEV(B,{px:2,mt:2,children:[e.exports.jsxDEV(T,{colorScheme:"green",onClick:h(J),children:"L\u1ECDc d\u1EEF li\u1EC7u"},void 0,!1,{fileName:i,lineNumber:126,columnNumber:11},globalThis),X&&e.exports.jsxDEV(T,{leftIcon:e.exports.jsxDEV(me,{},void 0,!1,{fileName:i,lineNumber:131,columnNumber:25},globalThis),variant:"ghost",colorScheme:"red",onClick:Y,flexShrink:0,children:"X\xF3a b\u1ED9 l\u1ECDc"},void 0,!1,{fileName:i,lineNumber:130,columnNumber:13},globalThis)]},void 0,!0,{fileName:i,lineNumber:125,columnNumber:9},globalThis)]},void 0,!0,{fileName:i,lineNumber:121,columnNumber:7},globalThis),e.exports.jsxDEV(he,{columns:q,data:G,loading:Z},void 0,!1,{fileName:i,lineNumber:142,columnNumber:7},globalThis),g>0&&e.exports.jsxDEV(ce,{children:[e.exports.jsxDEV($,{children:g>l.value&&e.exports.jsxDEV(je,{pageCount:Math.ceil(g/l.value),forcePage:a,onPageChange:({selected:o})=>s(o)},void 0,!1,{fileName:i,lineNumber:147,columnNumber:15},globalThis)},void 0,!1,{fileName:i,lineNumber:145,columnNumber:11},globalThis),e.exports.jsxDEV($,{flexGrow:1,justifyContent:{sm:"flex-start",md:"flex-end"},children:e.exports.jsxDEV(B,{children:[e.exports.jsxDEV(de,{whiteSpace:"nowrap",children:"Hi\u1EC3n th\u1ECB"},void 0,!1,{fileName:i,lineNumber:162,columnNumber:15},globalThis),e.exports.jsxDEV(v,{width:200,children:e.exports.jsxDEV(ge,{menuPlacement:"top",value:l,options:ve,onChange:Q},void 0,!1,{fileName:i,lineNumber:164,columnNumber:17},globalThis)},void 0,!1,{fileName:i,lineNumber:163,columnNumber:15},globalThis)]},void 0,!0,{fileName:i,lineNumber:161,columnNumber:13},globalThis)},void 0,!1,{fileName:i,lineNumber:154,columnNumber:11},globalThis)]},void 0,!0,{fileName:i,lineNumber:144,columnNumber:9},globalThis)]},void 0,!0,{fileName:i,lineNumber:119,columnNumber:5},globalThis)};var n="/Users/zim/Work/Zim/zim-app/src/pages/configs/set-of-handouts/index.tsx";const ze=()=>e.exports.jsxDEV(v,{p:4,children:e.exports.jsxDEV(be,{size:"md",variant:"enclosed",children:[e.exports.jsxDEV(pe,{children:[e.exports.jsxDEV(R,{children:"Danh s\xE1ch"},void 0,!1,{fileName:n,lineNumber:10,columnNumber:11},globalThis),e.exports.jsxDEV(R,{children:"T\u1EA1o set of handouts"},void 0,!1,{fileName:n,lineNumber:11,columnNumber:11},globalThis)]},void 0,!0,{fileName:n,lineNumber:9,columnNumber:9},globalThis),e.exports.jsxDEV(Ne,{children:[e.exports.jsxDEV(M,{children:e.exports.jsxDEV(Se,{},void 0,!1,{fileName:n,lineNumber:15,columnNumber:13},globalThis)},void 0,!1,{fileName:n,lineNumber:14,columnNumber:11},globalThis),e.exports.jsxDEV(M,{children:e.exports.jsxDEV(Ie,{},void 0,!1,{fileName:n,lineNumber:18,columnNumber:13},globalThis)},void 0,!1,{fileName:n,lineNumber:17,columnNumber:11},globalThis)]},void 0,!0,{fileName:n,lineNumber:13,columnNumber:9},globalThis)]},void 0,!0,{fileName:n,lineNumber:8,columnNumber:7},globalThis)},void 0,!1,{fileName:n,lineNumber:7,columnNumber:5},globalThis);export{ze as default};
