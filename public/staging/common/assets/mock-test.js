import{g as e}from"./vendor.js";const s=e`
  fragment QuestionField on IeltsPracticeQuestion {
    id
    testId
    questionGroupId
    question
    questionIndex
    questionCount
    sampleAnswers
    allowToBreakPoint

    answers {
      answer
      correct
      alternatives
    }
    explanation
    type
  }
`,a=e`
  fragment QuestionGroupField on IeltsPracticeQuestionGroup {
    id
    title
    testId
    question
    groupIndex
    audioFiles {
      id
      url
    }
    questions {
      ...QuestionField
    }
  }
  ${s}
`,t=e`
  fragment ModuleField on IeltsPracticeTest {
    id
    title
    type
    level
    status
    format
    parts {
      question
      questionGroupId
    }
    audio {
      id
      url
    }
    questionGroups {
      ...QuestionGroupField
    }
  }
  ${a}
`,l=e`
  fragment PracticeTestField on IeltsPracticeTestGroup {
    id
    title
    slug
    level
    status
    practiceTests {
      id
      title
      type
      level
      status
    }
    active
  }
`,n=e`
  fragment SEOField on CommonSeoConfig {
    title
    description
    ogDescription
    ogImage {
      id
      type
      path
      variants {
        id
        width
        height
        path
        type
      }
      filename
      title
      visibility
      width
      height
      usage
      createdAt
      updatedAt
    }
    ogTitle
    publisher
    noIndex
    noFollow
    canonicalUrl
    customHeadHtml
  }
`,o=e`
  fragment MockTestField on MockTestV2Type {
    id
    slug
    status
    fee
    total_slot
    available_slot
    school {
      id
      name
    }
    mockTestDetails {
      id
      date
      shift {
        id
        name
        start_time
        end_time
      }
      subject {
        id
        name
      }
      mock_test_shift_id
      mock_test_subject_id
      ecs {
        id
        full_name
      }
      teachers {
        id
        full_name
      }
    }
    subject {
      id
      name
    }
  }
`,r=e`
  fragment MockTestFieldForDetail on MockTestV2Type {
    id
    slug
    status
    fee
    type
    total_slot
    available_slot
    sent_email_status
    school {
      id
      name
    }
    mockTestDetails {
      id
      date
      shift {
        id
        name
        start_time
        end_time
      }
      subject {
        id
        name
      }
      mock_test_shift_id
      mock_test_subject_id
      available_slot
      total_slot
      currentSlot
      ecs {
        id
        full_name
      }
      teachers {
        id
        full_name
      }
    }
    examQuestionMockTest {
      id
      cat {
        id
        name
      }
      configMockTestBank {
        id
        questionLink
        answerLink
        referenceLink
        mockTestSubCat {
          id
          name
        }
      }
    }
    practiceTests {
      id
      module
      listeningId
      readingId
      writingId
    }
  }
`;e`
  query IELTSPracticeTestGroupPagination(
    $page: Int
    $limit: Int
    $status: [IeltsPracticeTestGroupStatus]
    $level: [IeltsPracticeTestLevel]
  ) @api(name: "zim") {
    IELTSPracticeTestGroupPagination(
      page: $page
      limit: $limit
      status: $status
      level: $level
    ) {
      hasNextPage
      hasPrevPage
      limit
      nextPage
      page
      pagingCounter
      prevPage
      totalDocs
      totalPages
      docs {
        id
        slug
        title
        level
        status
        active
        categories {
          id
          title
          description
        }
        practiceTests {
          id
          title
        }
      }
    }
  }
`;e`
  query IELTSPracticeTestPagination(
    $page: Int
    $limit: Int
    $type: [IeltsPracticeTestType]
    $status: [IeltsPracticeTestStatus]
    $level: [IeltsPracticeTestLevel]
    $format: [IeltsPracticeTestFormat]
  ) @api(name: "zim") {
    IELTSPracticeTestPagination(
      page: $page
      limit: $limit
      type: $type
      status: $status
      level: $level
      format: $format
    ) {
      hasNextPage
      hasPrevPage
      limit
      nextPage
      page
      pagingCounter
      prevPage
      totalDocs
      totalPages
      docs {
        ...ModuleField
      }
    }
  }
  ${t}
`;e`
  query IELTSPracticeTest($id: ObjectID!) @api(name: "zim") {
    IELTSPracticeTest(id: $id) {
      ...ModuleField
    }
  }
  ${t}
`;e`
  query IELTSPracticeTestGroup($id: ObjectID!) @api(name: "zim") {
    IELTSPracticeTestGroup(id: $id) {
      id
      title
      slug
      description
      level
      status
      active
      practiceTests {
        id
        title
        type
        status
      }
      categories {
        id
        title
        description
        slug
      }
      featureMedias {
        id
        type
        path
        variants {
          id
          width
          height
          path
          type
        }
        filename
        title
        visibility
        width
        height
        usage
        createdAt
        updatedAt
      }
      seo {
        ...SEOField
      }
    }
  }
  ${n}
`;const c=e`
  query getMockTestDetailV2($idOrSlug: String) @api(name: "appZim") {
    getMockTestDetailV2(idOrSlug: $idOrSlug) {
      id
      status
      details {
        date
      }
      fee
      school {
        id
        name
      }
      subject {
        id
        name
      }
    }
  }
`,u=e`
  query getMockTestDetailV2($idOrSlug: String) @api(name: "appZim") {
    getMockTestDetailV2(idOrSlug: $idOrSlug) {
      ...MockTestFieldForDetail
    }
  }
  ${r}
`,g=(i=[])=>e`
  query getMockTestDetailByField($idOrSlug: String) @api(name: "appZim") {
    getMockTestDetailV2(idOrSlug: $idOrSlug) {
      id
      ${i.join()}
    }
  }
`,T=e`
  query getListMockTestByRoleV2(
    $q: String
    $type: EnumMockTestType
    $status: EnumBaseStatus
    $schoolId: Int
    $fromDate: String
    $toDate: String
    $offset: Int
    $limit: Int
    $subjectId: Int
  ) @api(name: "appZim") {
    getListMockTestByRoleV2(
      q: $q
      type: $type
      status: $status
      schoolId: $schoolId
      fromDate: $fromDate
      toDate: $toDate
      offset: $offset
      limit: $limit
      subjectId: $subjectId
    ) {
      edges {
        node {
          ...MockTestField
        }
      }
      totalCount
      pageInfo {
        hasNextPage
        hasPreviousPage
      }
    }
  }
  ${o}
`,m=e`
  query getListTeacherRegisterForFunction(
    $type: Int!
    $date: String!
    $schoolId: Int
    $shift: Int!
  ) @api(name: "appZim") {
    getListTeacherRegisterForFunction(
      type: $type
      date: $date
      schoolId: $schoolId
      shift: $shift
    ) {
      id
      full_name
    }
  }
`,p=e`
  query getListEcForCenter($schoolId: Int) @api(name: "appZim") {
    getListEcForCenter(schoolId: $schoolId) {
      id
      full_name
    }
  }
`,_=e`
  query getListMockTestOrderByRoles(
    $mocktestId: String
    $status: MockTestStatusEnum
  ) @api(name: "appZim") {
    getListMockTestOrderByRoles(mocktestId: $mocktestId, status: $status) {
      id
      id_number
      status
      order_id
      student {
        id
        full_name
        phone
        email
      }
      owner {
        name
        email
        phone
      }
      moduleV2 {
        id
        name
        create_date
        status
      }
      mockTestDetailV2 {
        date
        id
        shift {
          id
          start_time
          end_time
        }
        subject {
          id
          name
        }
      }
      schedulesV2 {
        is_missing
        subject {
          id
        }
        timePreview
        platform_id
      }
      feedback_writing1
      feedback_writing2
      teacherV2 {
        id
        full_name
      }
      catV2 {
        id
        name
      }
      mockTestResultv2 {
        id
        reading
        writing1
        writing2
        writing_note
        overall
        listening
        speaking
        send_email_status
        speakingNote
        sessions {
          sessionIdReading
          sessionIdWriting
          sessionIdSpeaking
          sessionIdListening
          sessionIdWritingTask1
          sessionIdWritingTask2
        }
        mock_test_writings {
          id
          essay_correction {
            id
            title
          }
          score
          status
        }
      }
      imageMockTestOrder {
        id
        mockTestOrderId
        mockTestSubCatId
        mockTestSubCat {
          id
          name
        }
        url
      }
    }
  }
`,I=e`
  query ($catId: Int!) @api(name: "appZim") {
    getSubCatMockTestForStudent(catId: $catId) {
      id
      name
    }
  }
`,$=e`
  query getImageMockTestOrderBySubCat($orderId: Int!, $mockTestSubCatId: Int)
  @api(name: "appZim") {
    getImageMockTestOrderBySubCat(
      mockTestSubCatId: $mockTestSubCatId
      orderId: $orderId
    ) {
      id
      mockTestOrderId
      mockTestSubCatId
      mockTestSubCat {
        id
        name
      }
      url
    }
  }
`;e`
  query calcWritingAverage($writingTask1: Float, $writingTask2: Float)
  @api(name: "appZim") {
    calcWritingAverage(writingTask1: $writingTask1, writingTask2: $writingTask2)
  }
`;const k=e`
  query calcOverall(
    $listening: Float!
    $reading: Float!
    $writing: Float
    $writingTask1: Float
    $writingTask2: Float
    $speaking: Float!
  ) @api(name: "appZim") {
    calcOverall(
      listening: $listening
      reading: $reading
      writing: $writing
      speaking: $speaking
      writingTask1: $writingTask1
      writingTask2: $writingTask2
    )
  }
`,S=e`
  query getResultMockTestStudent($mocktestId: String) @api(name: "appZim") {
    getResultMockTestStudent(mocktestId: $mocktestId) {
      id
      listening
      reading
      writing1
      writing2
      writing_note
      send_email_status
      speaking
      speakingNote
    }
  }
`,E=e`
  query getListTeacherAssignedMockTest($mockTestId: Int!) @api(name: "appZim") {
    getListTeacherAssignedMockTest(mockTestId: $mockTestId) {
      id
      full_name
    }
  }
`,h=e`
  query getListTeacherOfMockTestWithSchedule($mockTestId: Int, $orderId: Int)
  @api(name: "appZim") {
    getListTeacherOfMockTestWithSchedule(
      mockTestId: $mockTestId
      orderId: $orderId
    ) {
      teacher_id
      teacher_name
      schedule {
        mock_test_detail_id
        isChange
        id_number
        lRW
        speaking
      }
    }
  }
`;export{k as C,p as G,_ as M,l as P,a as Q,c as a,T as b,s as c,I as d,$ as e,S as f,h as g,g as h,u as i,m as j,E as k};
