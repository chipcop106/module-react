import{g as e}from"./vendor.js";const i=e`
  query getMockTestReportEc($input: MockTestReportInputType)
  @api(name: "appZim") {
    getMockTestReportEc(input: $input) {
      ec {
        id
        full_name
        email
        phone
      }
      countLRW
      countSpeaking
      salary
    }
  }
`,a=e`
  query getMockTestReportTeacher($input: MockTestReportInputType)
  @api(name: "appZim") {
    getMockTestReportTeacher(input: $input) {
      teacher {
        id
        full_name
        email
        phone
      }
      countSpeaking
      countFeedbackW1
      countFeedbackW2
      countLRW
      count
      salary
    }
  }
`,p=e`
  query mockTestReportTeacherDetail($input: MockTestECDetailReportInputType)
  @api(name: "appZim") {
    mockTestReportTeacherDetail(input: $input) {
      feedback_writing1
      feedback_writing2
      start_time_lrw
      start_time_speak
      student_id
      isMissingSpeaking
      isMissingLRW
      teacher {
        id
        full_name
      }
      student {
        id
        full_name
      }
      mockTest {
        id
        slug
        school {
          id
          name
        }
      }
    }
  }
`,n=e`
  query mockTestReportECDetail($input: MockTestECDetailReportInputType)
  @api(name: "appZim") {
    mockTestReportEcDetail(input: $input) {
      ec_id
      mock_test_id
      mock_test_shift_id
      mock_test_subject_id
      date
      mockTest {
        id
        slug
        school {
          id
          name
        }
      }
      ecV2 {
        id
        full_name
      }
    }
  }
`;export{a as G,p as M,n as a,i as b};
