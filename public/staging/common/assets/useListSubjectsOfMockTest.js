var o=Object.defineProperty;var i=Object.getOwnPropertySymbols;var u=Object.prototype.hasOwnProperty,m=Object.prototype.propertyIsEnumerable;var n=(s,t,e)=>t in s?o(s,t,{enumerable:!0,configurable:!0,writable:!0,value:e}):s[t]=e,c=(s,t)=>{for(var e in t||(t={}))u.call(t,e)&&n(s,e,t[e]);if(i)for(var e of i(t))m.call(t,e)&&n(s,e,t[e]);return s};import{g as a,a0 as T,ax as d}from"./vendor.js";const r=a`
  query @api(name: "appZim") {
    getSubjectForMockTest {
      id
      name
      status
      catMockTest {
        id
        name
        subject {
          id
          name
        }
        mockTestSubCat {
          id
          name
          cat {
            id
            name
          }
        }
      }
    }
  }
`,_=a`
  query @api(name: "appZim") {
    getSubjectForMockTest {
      id
      name
    }
  }
`,b=a`
  query getListMockTestBank($subjectId: Int, $offset: Int, $limit: Int)
  @api(name: "appZim") {
    getListMockTestBank(limit: $limit, offset: $offset, subjectId: $subjectId) {
      edges {
        node {
          id
          subject {
            id
            name
          }
          mockTestSubCat {
            id
            name
          }
          cat {
            id
            name
          }
          answerLink
          questionLink
          referenceLink
          isPublish
          usedNumber
        }
      }
      totalCount
      pageInfo {
        hasNextPage
        hasPreviousPage
      }
    }
  }
`,p=a`
  query getMockTestBank($id: Int) @api(name: "appZim") {
    getMockTestBank(id: $id) {
      id
      subject {
        id
        name
      }
      mockTestSubCat {
        id
        name
      }
      cat {
        id
        name
      }
      answerLink
      questionLink
      referenceLink
      isPublish
    }
  }
`,S=a`
  query ($catIds: [Int]!) @api(name: "appZim") {
    getSubCatMockTest(catIds: $catIds) {
      id
      name
    }
  }
`,l=s=>{const t=d();return T(r,c({onError:e=>{t({title:"L\u1EA5y d\u1EEF li\u1EC7u th\u1EA5t b\u1EA1i.",description:`GET  subject of mock test error: ${e==null?void 0:e.message}`,status:"error",duration:3e3,isClosable:!0})},fetchPolicy:"cache-first",errorPolicy:"all"},s))};export{S as G,p as a,_ as b,b as c,l as u};
