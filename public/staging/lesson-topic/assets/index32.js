var Y=Object.defineProperty,ee=Object.defineProperties;var le=Object.getOwnPropertyDescriptors;var w=Object.getOwnPropertySymbols;var se=Object.prototype.hasOwnProperty,oe=Object.prototype.propertyIsEnumerable;var y=(i,l,s)=>l in i?Y(i,l,{enumerable:!0,configurable:!0,writable:!0,value:s}):i[l]=s,r=(i,l)=>{for(var s in l||(l={}))se.call(l,s)&&y(i,s,l[s]);if(w)for(var s of w(l))oe.call(l,s)&&y(i,s,l[s]);return i},x=(i,l)=>ee(i,le(l));import{g as _,aw as A,at as ae,au as ie,h as m,j as e,V as L,ay as z,B as v,_ as T,dE as te,a0 as ne,ag as ue,u as re,H as $,dI as me,an as ce,df as B,p as de,v as be,w as Ne,x as R,y as fe,z as M}from"./vendor.js";import{be as N,a2 as pe,bf as U,bg as xe,T as he,C as ge,f as ve,P as Te,g as Ee,ag as je}from"./index.js";const De=_`
  mutation createSetOfHandouts($input: SetOfHandoutsInputType)
  @api(name: "appZim") {
    createSetOfHandouts(input: $input) {
      id
    }
  }
`;var d="/Users/zim/Work/Zim/zim-app/src/pages/configs/set-of-handouts/components/CreateSetOfHandouts.tsx";const Ie=()=>{const i=A({defaultValues:r({},N)}),{handleSubmit:l,reset:s}=i,f=ae(),u=pe(),[p,{loading:b}]=ie(De,{onCompleted:()=>{f({title:"Th\xE0nh c\xF4ng",description:"T\u1EA1o set of handouts th\xE0nh c\xF4ng",status:"success",duration:3e3,isClosable:!0}),s(r({},N)),u(["getSetOfHandoutsPagination"])},onError:t=>{f({title:"Error",description:`Create set of handouts error: ${t.message}`,status:"error",duration:3e3,isClosable:!0})}}),h=m.exports.useCallback(async t=>{await p({variables:{input:{title:t.title,subjectId:t.subject.id,catId:t.module.id,subCatId:t.skill.id,levelIn:t.levelIn.id,levelOut:t.levelOut.id}}})},[]);return e.exports.jsxDEV(L,{align:"stretch",spacing:4,children:[e.exports.jsxDEV(z,x(r({},i),{children:e.exports.jsxDEV(U,{},void 0,!1,{fileName:d,lineNumber:70,columnNumber:9},globalThis)}),void 0,!1,{fileName:d,lineNumber:69,columnNumber:7},globalThis),e.exports.jsxDEV(v,{px:2,children:e.exports.jsxDEV(T,{colorScheme:"green",leftIcon:e.exports.jsxDEV(te,{},void 0,!1,{fileName:d,lineNumber:75,columnNumber:21},globalThis),isLoading:b,onClick:l(h),children:"T\u1EA1o"},void 0,!1,{fileName:d,lineNumber:73,columnNumber:9},globalThis)},void 0,!1,{fileName:d,lineNumber:72,columnNumber:7},globalThis)]},void 0,!0,{fileName:d,lineNumber:68,columnNumber:5},globalThis)},Ve=_`
  query getSetOfHandoutsPagination($input: PaginationSetOfHandoutsInputType)
  @api(name: "appZim") {
    getSetOfHandoutsPagination(input: $input) {
      page
      totalDocs
      docs {
        id
        title
        status
        subject {
          id
          name
        }
        cat {
          id
          name
        }
        subcat {
          id
          name
        }
        levelIn {
          id
          name
          graduation
        }
        levelOut {
          id
          name
          graduation
        }
      }
    }
  }
`;var a="/Users/zim/Work/Zim/zim-app/src/pages/configs/set-of-handouts/components/ListSetOfHandouts.tsx";const Se=()=>{var E,j;const[i,l]=m.exports.useState(0),[s,f]=m.exports.useState({label:"10 rows",value:10}),[u,p]=m.exports.useState({subjectId:0,moduleId:0,skillId:0,levelIn:0,levelOut:0}),b=A({defaultValues:r({},N)}),{handleSubmit:h,reset:t}=b,W=m.exports.useMemo(()=>({input:{page:i+1,limit:s.value,subjectId:u.subjectId,catId:u.moduleId,subcatId:u.skillId,levelIn:u.levelIn,levelOut:u.levelOut}}),[i,s,u]),{data:c,loading:Z}=ne(Ve,{variables:W}),q=xe(),G=(j=(E=c==null?void 0:c.getSetOfHandoutsPagination)==null?void 0:E.docs.map(o=>x(r({},o),{subject:o.subject.name,module:o.cat.name,skill:o.subcat.name,level:{in:o.levelIn.graduation,out:o.levelOut.graduation}})))!=null?j:[],g=c==null?void 0:c.getSetOfHandoutsPagination.totalDocs,Q=o=>{l(0),f(o)},X=!ue.exports.isEqual(N,b.watch()),J=m.exports.useCallback(()=>{t(r({},N)),p({subjectId:0,moduleId:0,skillId:0,levelIn:0,levelOut:0}),l(0)},[]),K=m.exports.useCallback(o=>{var D,I,V,S,O,C,H,k,P,F;p({subjectId:(I=(D=o.subject)==null?void 0:D.id)!=null?I:0,moduleId:(S=(V=o.module)==null?void 0:V.id)!=null?S:0,skillId:(C=(O=o.skill)==null?void 0:O.id)!=null?C:0,levelIn:(k=(H=o.levelIn)==null?void 0:H.id)!=null?k:0,levelOut:(F=(P=o==null?void 0:o.levelOut)==null?void 0:P.id)!=null?F:0}),l(0)},[]);return e.exports.jsxDEV(L,{align:"stretch",spacing:4,children:[e.exports.jsxDEV(he,{children:"Set of handouts"},void 0,!1,{fileName:a,lineNumber:120,columnNumber:7},globalThis),e.exports.jsxDEV(ge,{bg:re("gray.50","slate.800"),mt:4,children:[e.exports.jsxDEV(z,x(r({},b),{children:e.exports.jsxDEV(U,{showTitleInput:!1,isRequiredFields:!1},void 0,!1,{fileName:a,lineNumber:123,columnNumber:11},globalThis)}),void 0,!1,{fileName:a,lineNumber:122,columnNumber:9},globalThis),e.exports.jsxDEV($,{px:2,mt:2,children:[e.exports.jsxDEV(T,{colorScheme:"green",onClick:h(K),children:"L\u1ECDc d\u1EEF li\u1EC7u"},void 0,!1,{fileName:a,lineNumber:126,columnNumber:11},globalThis),X&&e.exports.jsxDEV(T,{leftIcon:e.exports.jsxDEV(me,{},void 0,!1,{fileName:a,lineNumber:131,columnNumber:25},globalThis),variant:"ghost",colorScheme:"red",onClick:J,flexShrink:0,children:"X\xF3a b\u1ED9 l\u1ECDc"},void 0,!1,{fileName:a,lineNumber:130,columnNumber:13},globalThis)]},void 0,!0,{fileName:a,lineNumber:125,columnNumber:9},globalThis)]},void 0,!0,{fileName:a,lineNumber:121,columnNumber:7},globalThis),e.exports.jsxDEV(ve,{columns:q,data:G,loading:Z},void 0,!1,{fileName:a,lineNumber:142,columnNumber:7},globalThis),g>0&&e.exports.jsxDEV(ce,{children:[e.exports.jsxDEV(B,{children:g>s.value&&e.exports.jsxDEV(Te,{pageCount:Math.ceil(g/s.value),forcePage:i,onPageChange:({selected:o})=>l(o)},void 0,!1,{fileName:a,lineNumber:147,columnNumber:15},globalThis)},void 0,!1,{fileName:a,lineNumber:145,columnNumber:11},globalThis),e.exports.jsxDEV(B,{flexGrow:1,justifyContent:{sm:"flex-start",md:"flex-end"},children:e.exports.jsxDEV($,{children:[e.exports.jsxDEV(de,{whiteSpace:"nowrap",children:"Hi\u1EC3n th\u1ECB"},void 0,!1,{fileName:a,lineNumber:162,columnNumber:15},globalThis),e.exports.jsxDEV(v,{width:200,children:e.exports.jsxDEV(Ee,{menuPlacement:"top",value:s,options:je,onChange:Q},void 0,!1,{fileName:a,lineNumber:164,columnNumber:17},globalThis)},void 0,!1,{fileName:a,lineNumber:163,columnNumber:15},globalThis)]},void 0,!0,{fileName:a,lineNumber:161,columnNumber:13},globalThis)},void 0,!1,{fileName:a,lineNumber:154,columnNumber:11},globalThis)]},void 0,!0,{fileName:a,lineNumber:144,columnNumber:9},globalThis)]},void 0,!0,{fileName:a,lineNumber:119,columnNumber:5},globalThis)};var n="/Users/zim/Work/Zim/zim-app/src/pages/configs/set-of-handouts/index.tsx";const ke=()=>e.exports.jsxDEV(v,{p:4,children:e.exports.jsxDEV(be,{size:"md",variant:"enclosed",children:[e.exports.jsxDEV(Ne,{children:[e.exports.jsxDEV(R,{children:"Danh s\xE1ch"},void 0,!1,{fileName:n,lineNumber:10,columnNumber:11},globalThis),e.exports.jsxDEV(R,{children:"T\u1EA1o set of handouts"},void 0,!1,{fileName:n,lineNumber:11,columnNumber:11},globalThis)]},void 0,!0,{fileName:n,lineNumber:9,columnNumber:9},globalThis),e.exports.jsxDEV(fe,{children:[e.exports.jsxDEV(M,{children:e.exports.jsxDEV(Se,{},void 0,!1,{fileName:n,lineNumber:15,columnNumber:13},globalThis)},void 0,!1,{fileName:n,lineNumber:14,columnNumber:11},globalThis),e.exports.jsxDEV(M,{children:e.exports.jsxDEV(Ie,{},void 0,!1,{fileName:n,lineNumber:18,columnNumber:13},globalThis)},void 0,!1,{fileName:n,lineNumber:17,columnNumber:11},globalThis)]},void 0,!0,{fileName:n,lineNumber:13,columnNumber:9},globalThis)]},void 0,!0,{fileName:n,lineNumber:8,columnNumber:7},globalThis)},void 0,!1,{fileName:n,lineNumber:7,columnNumber:5},globalThis);export{ke as default};
