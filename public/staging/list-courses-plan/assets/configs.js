import{g as e,j as a,af as u,H as c,eq as p,p as o}from"./vendor.js";const d=e`
  query ($type: EnumShiftsType) @api(name: "appZim") {
    getShiftRegisterSchedule(type: $type) {
      id
      start_time
      end_time
      name
    }
  }
`;e`
  query shifts($q: String, $status: String, $shift_minute: Int)
  @api(name: "appZim") {
    shifts(q: $q, status: $status, shift_minute: $shift_minute) {
      id
      start_time
      end_time
    }
  }
`;const m=e`
  query getScheduleDetail(
    $registerScheduleId: Int!
    $teacherId: CustomUserInputType
    $schoolId: Int
    $status: EnumStatusScheduleType
  ) @api(name: "appZim") {
    getScheduleDetail(
      registerScheduleId: $registerScheduleId
      teacherId: $teacherId
      schoolId: $schoolId
      status: $status
    ) {
      id
      type
      date
      schoolId
      startTime
      endTime
      students {
        id
        full_name
      }
      teacher {
        id
        full_name
      }
      eC {
        id
        full_name
      }
      linkMeeting
      description
    }
  }
`,h=e`
  mutation createRegisterSchedule($input: RegisterScheduleInputCreateType)
  @api(name: "appZim") {
    createRegisterSchedule(input: $input) {
      success
      error
      message
      dataString
    }
  }
`,S=e`
  mutation cancelRegisterSchedule($input: CancelRegisterInputType)
  @api(name: "appZim") {
    cancelRegisterSchedule(input: $input) {
      success
      error
      message
    }
  }
`,E=e`
  mutation createOrUpdateRegisterScheduleDetail(
    $input: CreateRegisterScheduleDetailInputType
  ) @api(name: "appZim") {
    createOrUpdateRegisterScheduleDetail(input: $input) {
      success
      error
      message
    }
  }
`,_=e`
  mutation approveOrRejectRequestCancel(
    $input: RejectRequestCancelScheduleInputType
  ) @api(name: "appZim") {
    approveOrRejectRequestCancel(input: $input) {
      success
      message
      dataString
    }
  }
`,T=e`
  mutation changeScheduleForTeacher($input: ChangeScheduleForTeacherInputType)
  @api(name: "appZim") {
    changeScheduleForTeacher(input: $input) {
      success
      error
      message
    }
  }
`,$=e`
  mutation cancelScheduleDetail($input: ScheduleDetailInputType)
  @api(name: "appZim") {
    cancelScheduleDetail(input: $input) {
      message
      success
      dataString
      error
    }
  }
`,y=e`
  mutation copyRegisterSchedule($input: CopyRegisterScheduleInputType)
  @api(name: "appZim") {
    copyRegisterSchedule(input: $input) {
      success
      message
    }
  }
`,I=e`
  mutation reopenScheduleDetail($input: ReopenScheduleDetailInputType)
  @api(name: "appZim") {
    reopenScheduleDetail(input: $input) {
      success
      error
    }
  }
`,r=e`
  fragment AllScheduleCopyType on ScheduleCopyType {
    type
    id
    configFunction {
      isAutoSchedule
    }
    date
    shift {
      id
      start_time
      end_time
    }
    teachers {
      id
      full_name
    }
    school {
      id
      name
    }
  }
`;e`
  query getTodoScheduleByEc($idEc: Int, $date: String) @api(name: "appZim") {
    getTodoScheduleByEc(idEc: $idEc, date: $date) {
      id
      type
      content
      customer {
        id
        role_id
        user_name
        full_name
        avatar
      }
      ec {
        id
        user_name
        full_name
        avatar
      }
      instructor {
        id
        role_id
        user_name
        full_name
        avatar
      }
      file_attached
      link
      start_time
      end_time
      date
      is_delete
      create_by
      create_date
      update_by
      update_date
    }
  }
`;const g=e`
  query getConfigFunctionByType($type: EnumConfigFunctionType!)
  @api(name: "appZim") {
    getConfigFunctionByType(type: $type) {
      id
      name
      type
      description
      timePerShift
      color
      isAutoSchedule
      shiftType
    }
  }
`,C=e`
  query getListRegisterSchedule(
    $fromDate: CustomDateInputType
    $toDate: CustomDateInputType
  ) @api(name: "appZim") {
    getListRegisterSchedule(fromDate: $fromDate, toDate: $toDate) {
      id
      type
      date
      shift {
        id
        start_time
        end_time
      }
      isFull
      requestsCancel {
        teacherId
        id
        reason
      }
      school {
        id
        name
      }
      configFunction {
        autoCancel
      }
    }
  }
`,D=e`
  query ($teacherId: CustomUserInputType!) @api(name: "appZim") {
    getFunctionAssignedTeacher(teacherId: $teacherId) {
      id
      name
      color
      isAutoSchedule
      shiftType
      needToCenter
    }
  }
`,f=e`
  query getListRegisterSchedule(
    $fromDate: CustomDateInputType
    $toDate: CustomDateInputType
    $type: Int
    $schoolId: Int
    $teacherId: CustomUserInputType
    $status: EnumStatusScheduleType
  ) @api(name: "appZim") {
    getListRegisterSchedule(
      fromDate: $fromDate
      toDate: $toDate
      type: $type
      schoolId: $schoolId
      teacherId: $teacherId
      status: $status
    ) {
      id
      type
      date
      shift {
        id
        start_time
        end_time
        name
      }
      school {
        id
        name
      }
      teachers {
        id
        full_name
      }
      requestsCancel {
        teacherId
        id
        reason
      }
      configFunction {
        isAutoSchedule
        timePerShift
        id
        color
        name
      }
      isFull
    }
  }
`,R=e`
  query checkListCopySchedule($input: CopyRegisterScheduleInputType)
  @api(name: "appZim") {
    checkListCopySchedule(input: $input) {
      scheduleConflict {
        ...AllScheduleCopyType
      }
      scheduleApprove {
        ...AllScheduleCopyType
      }
    }
  }
  ${r}
`,L=e`
  query getListScheduleByStatus(
    $fromDate: String
    $toDate: String
    $type: Int
    $teacherId: CustomUserInputType
    $schoolId: Int
    $status: EnumStatusScheduleType
  ) @api(name: "appZim") {
    getListScheduleByStatus(
      fromDate: $fromDate
      toDate: $toDate
      type: $type
      teacherId: $teacherId
      schoolId: $schoolId
      status: $status
    ) {
      id
      type
      date
      configFunction {
        id
        isAutoSchedule
      }
      teacher {
        id
        full_name
      }
      students {
        id
        full_name
      }
      startTime
      endTime
      description
      roomId
      linkMeeting
    }
  }
`;e`
  query getRequestCancelByStatus(
    $fromDate: String
    $toDate: String
    $type: Int
    $teacherId: CustomUserInputType
    $schoolId: Int
    $status: EnumStatusScheduleType
  ) @api(name: "appZim") {
    getRequestCancelByStatus(
      fromDate: $fromDate
      toDate: $toDate
      type: $type
      teacherId: $teacherId
      schoolId: $schoolId
      status: $status
    ) {
      id
      type
      date
    }
  }
`;var s="/Users/zim/Work/Zim/zim-app/src/pages/schedule/components/ScheduleTypeNotes/index.tsx";const A=({typeRegister:t})=>(t==null?void 0:t.length)?a.exports.jsxDEV(u,{columns:{xl:4,base:1},maxW:"xl",rowGap:2,columnGap:{xl:6,base:0},children:t.map(i=>a.exports.jsxDEV(c,{spacing:2,children:[a.exports.jsxDEV(p,{size:3,bg:`${i.color}.500`},void 0,!1,{fileName:s,lineNumber:21,columnNumber:13},globalThis),a.exports.jsxDEV(o,{whiteSpace:"nowrap",children:i.name},void 0,!1,{fileName:s,lineNumber:22,columnNumber:13},globalThis)]},i.id,!0,{fileName:s,lineNumber:20,columnNumber:11},globalThis))},void 0,!1,{fileName:s,lineNumber:12,columnNumber:5},globalThis):null,U=3,q=4;var n;(function(t){t.register="register",t.function="function"})(n||(n={}));export{_ as A,h as C,n as E,d as G,q as O,I as R,A as S,S as a,m as b,R as c,g as d,D as e,C as f,y as g,T as h,$ as i,f as j,L as k,U as l,E as m};
