import{P as e}from"./index.js";import{g as t}from"./vendor.js";const n=t`
  query getTicketsPagination($input: TicketPaginationInputType)
  @api(name: "appZim") {
    getTicketsPagination(input: $input) {
      hasNextPage
      hasPrevPage
      limit
      page
      pageNext
      pagePrev
      pagingCounter
      totalPages
      totalDocs
      docs {
        ...Ticket
        comments {
          ...Ticket
        }
      }
    }
  }
  ${e}
`;t`
  query getTicketsPagination($input: TicketPaginationInputType)
  @api(name: "appZim") {
    getTicketsPagination(input: $input) {
      hasNextPage
      hasPrevPage
      limit
      page
      pageNext
      pagePrev
      pagingCounter
      totalPages
      totalDocs
      docs {
        status
      }
    }
  }
  ${e}
`;const s=t`
  query getTicketDetail($idTicket: Guid!) @api(name: "appZim") {
    getTicketDetail(idTicket: $idTicket) {
      userId
      fullName
      ticketId
      createDateTicket
      schoolAddress
      course {
        id
        name
        ec {
          id
          full_name
        }
      }
      ticket {
        ...Ticket
        comments {
          ...Ticket
        }
      }
      user {
        id
        full_name
        phone
        email
        address
        avatar
      }
    }
  }
  ${e}
`;export{s as G,n as a};
