var Y=Object.defineProperty,ee=Object.defineProperties;var se=Object.getOwnPropertyDescriptors;var w=Object.getOwnPropertySymbols;var le=Object.prototype.hasOwnProperty,oe=Object.prototype.propertyIsEnumerable;var F=(a,s,l)=>s in a?Y(a,s,{enumerable:!0,configurable:!0,writable:!0,value:l}):a[s]=l,u=(a,s)=>{for(var l in s||(s={}))le.call(s,l)&&F(a,l,s[l]);if(w)for(var l of w(s))oe.call(s,l)&&F(a,l,s[l]);return a},h=(a,s)=>ee(a,se(s));import{g as _,aw as z,at as ie,au as ae,h as m,j as e,V as A,ay as B,B as T,_ as v,eV as ne,a0 as te,ag as re,u as ue,H as $,dW as me,an as ce,df as R,p as be,v as de,w as pe,x as W,y as Ne,z as Z}from"./vendor.js";import{c as p,L as M}from"./index34.js";import{Z as xe,C as he,f as fe,g as ge,a9 as Te}from"./index.js";import{P as ve}from"./index7.js";import{T as je}from"./index6.js";import{u as Ee}from"./index35.js";import"./useListLevels.js";import"./options.js";import"./arrayToSelectOption.js";import"./useListSubCatByCat.js";const De=_`
  mutation createLessonTopic($input: LessonTopicInsertInputType)
  @api(name: "appZim") {
    createLessonTopic(input: $input) {
      id
      title
    }
  }
`;var b="/Users/zim/Work/Zim/zim-app/src/pages/configs/lesson-topic/components/CreateLessonTopic.tsx";const Ie=()=>{const a=z({defaultValues:u({},p)}),{handleSubmit:s,reset:l}=a,N=ie(),r=xe(),[x,{loading:d}]=ae(De,{onCompleted:()=>{N({title:"Th\xE0nh c\xF4ng",description:"T\u1EA1o lesson topic th\xE0nh c\xF4ng",status:"success",duration:3e3,isClosable:!0}),l(u({},p)),r(["getLessonTopicPagination"])},onError:n=>{N({title:"Error",description:`Create lesson topic error: ${n.message}`,status:"error",duration:3e3,isClosable:!0})}}),f=m.exports.useCallback(async n=>{await x({variables:{input:{title:n.title,subjectId:n.subject.id,catId:n.module.id,subCatId:n.skill.id,levelIn:n.levelIn.id,levelOut:n.levelOut.id}}})},[]);return e.exports.jsxDEV(A,{align:"stretch",spacing:4,children:[e.exports.jsxDEV(B,h(u({},a),{children:e.exports.jsxDEV(M,{},void 0,!1,{fileName:b,lineNumber:71,columnNumber:9},globalThis)}),void 0,!1,{fileName:b,lineNumber:70,columnNumber:7},globalThis),e.exports.jsxDEV(T,{px:2,children:e.exports.jsxDEV(v,{colorScheme:"green",leftIcon:e.exports.jsxDEV(ne,{},void 0,!1,{fileName:b,lineNumber:76,columnNumber:21},globalThis),isLoading:d,onClick:s(f),children:"T\u1EA1o"},void 0,!1,{fileName:b,lineNumber:74,columnNumber:9},globalThis)},void 0,!1,{fileName:b,lineNumber:73,columnNumber:7},globalThis)]},void 0,!0,{fileName:b,lineNumber:69,columnNumber:5},globalThis)},Ve=_`
  query getLessonTopicPagination($input: PaginationLessonTopicInputType)
  @api(name: "appZim") {
    getLessonTopicPagination(input: $input) {
      page
      totalDocs
      docs {
        id
        title
        status
        subject {
          id
          name
        }
        cat {
          id
          name
        }
        subcat {
          id
          name
        }
        levelIn {
          id
          name
          graduation
        }
        levelOut {
          id
          name
          graduation
        }
      }
    }
  }
`;var i="/Users/zim/Work/Zim/zim-app/src/pages/configs/lesson-topic/components/ListLessonTopics.tsx";const Ce=()=>{var j,E;const[a,s]=m.exports.useState(0),[l,N]=m.exports.useState({label:"10 rows",value:10}),[r,x]=m.exports.useState({subjectId:0,moduleId:0,skillId:0,levelIn:0,levelOut:0}),d=z({defaultValues:u({},p)}),{handleSubmit:f,reset:n}=d,q=Ee(),G=m.exports.useMemo(()=>({input:{page:a+1,limit:l.value,subjectId:r.subjectId,catId:r.moduleId,subcatId:r.skillId,levelIn:r.levelIn,levelOut:r.levelOut}}),[a,l,r]),{data:c,loading:H}=te(Ve,{variables:G}),Q=(E=(j=c==null?void 0:c.getLessonTopicPagination)==null?void 0:j.docs.map(o=>h(u({},o),{subject:o.subject.name,module:o.cat.name,skill:o.subcat.name,level:{in:o.levelIn.graduation,out:o.levelOut.graduation}})))!=null?E:[],g=c==null?void 0:c.getLessonTopicPagination.totalDocs,U=o=>{s(0),N(o)},X=!re.exports.isEqual(p,d.watch()),J=m.exports.useCallback(()=>{n(u({},p)),x({subjectId:0,moduleId:0,skillId:0,levelIn:0,levelOut:0}),s(0)},[]),K=m.exports.useCallback(o=>{var D,I,V,C,L,S,k,P,O,y;x({subjectId:(I=(D=o.subject)==null?void 0:D.id)!=null?I:0,moduleId:(C=(V=o.module)==null?void 0:V.id)!=null?C:0,skillId:(S=(L=o.skill)==null?void 0:L.id)!=null?S:0,levelIn:(P=(k=o.levelIn)==null?void 0:k.id)!=null?P:0,levelOut:(y=(O=o==null?void 0:o.levelOut)==null?void 0:O.id)!=null?y:0}),s(0)},[]);return e.exports.jsxDEV(A,{align:"stretch",spacing:4,children:[e.exports.jsxDEV(je,{children:"Lesson topic"},void 0,!1,{fileName:i,lineNumber:117,columnNumber:7},globalThis),e.exports.jsxDEV(he,{bg:ue("gray.50","slate.800"),mt:4,children:[e.exports.jsxDEV(B,h(u({},d),{children:e.exports.jsxDEV(M,{showTitleInput:!1,isRequiredFields:!1},void 0,!1,{fileName:i,lineNumber:120,columnNumber:11},globalThis)}),void 0,!1,{fileName:i,lineNumber:119,columnNumber:9},globalThis),e.exports.jsxDEV($,{px:2,mt:2,children:[e.exports.jsxDEV(v,{colorScheme:"green",onClick:f(K),children:"L\u1ECDc d\u1EEF li\u1EC7u"},void 0,!1,{fileName:i,lineNumber:123,columnNumber:11},globalThis),X&&e.exports.jsxDEV(v,{leftIcon:e.exports.jsxDEV(me,{},void 0,!1,{fileName:i,lineNumber:128,columnNumber:25},globalThis),variant:"ghost",colorScheme:"red",onClick:J,flexShrink:0,children:"X\xF3a b\u1ED9 l\u1ECDc"},void 0,!1,{fileName:i,lineNumber:127,columnNumber:13},globalThis)]},void 0,!0,{fileName:i,lineNumber:122,columnNumber:9},globalThis)]},void 0,!0,{fileName:i,lineNumber:118,columnNumber:7},globalThis),e.exports.jsxDEV(fe,{columns:q,data:Q,loading:H},void 0,!1,{fileName:i,lineNumber:139,columnNumber:7},globalThis),g>0&&e.exports.jsxDEV(ce,{children:[e.exports.jsxDEV(R,{children:g>l.value&&e.exports.jsxDEV(ve,{pageCount:Math.ceil(g/l.value),forcePage:a,onPageChange:({selected:o})=>s(o)},void 0,!1,{fileName:i,lineNumber:144,columnNumber:15},globalThis)},void 0,!1,{fileName:i,lineNumber:142,columnNumber:11},globalThis),e.exports.jsxDEV(R,{flexGrow:1,justifyContent:{sm:"flex-start",md:"flex-end"},children:e.exports.jsxDEV($,{children:[e.exports.jsxDEV(be,{whiteSpace:"nowrap",children:"Hi\u1EC3n th\u1ECB"},void 0,!1,{fileName:i,lineNumber:159,columnNumber:15},globalThis),e.exports.jsxDEV(T,{width:200,children:e.exports.jsxDEV(ge,{menuPlacement:"top",value:l,options:Te,onChange:U},void 0,!1,{fileName:i,lineNumber:161,columnNumber:17},globalThis)},void 0,!1,{fileName:i,lineNumber:160,columnNumber:15},globalThis)]},void 0,!0,{fileName:i,lineNumber:158,columnNumber:13},globalThis)},void 0,!1,{fileName:i,lineNumber:151,columnNumber:11},globalThis)]},void 0,!0,{fileName:i,lineNumber:141,columnNumber:9},globalThis)]},void 0,!0,{fileName:i,lineNumber:116,columnNumber:5},globalThis)};var t="/Users/zim/Work/Zim/zim-app/src/pages/configs/lesson-topic/index.tsx";const Be=()=>e.exports.jsxDEV(T,{p:4,children:e.exports.jsxDEV(de,{size:"md",variant:"enclosed",children:[e.exports.jsxDEV(pe,{children:[e.exports.jsxDEV(W,{children:"Danh s\xE1ch"},void 0,!1,{fileName:t,lineNumber:10,columnNumber:11},globalThis),e.exports.jsxDEV(W,{children:"T\u1EA1o lesson topic"},void 0,!1,{fileName:t,lineNumber:11,columnNumber:11},globalThis)]},void 0,!0,{fileName:t,lineNumber:9,columnNumber:9},globalThis),e.exports.jsxDEV(Ne,{children:[e.exports.jsxDEV(Z,{children:e.exports.jsxDEV(Ce,{},void 0,!1,{fileName:t,lineNumber:15,columnNumber:13},globalThis)},void 0,!1,{fileName:t,lineNumber:14,columnNumber:11},globalThis),e.exports.jsxDEV(Z,{children:e.exports.jsxDEV(Ie,{},void 0,!1,{fileName:t,lineNumber:18,columnNumber:13},globalThis)},void 0,!1,{fileName:t,lineNumber:17,columnNumber:11},globalThis)]},void 0,!0,{fileName:t,lineNumber:13,columnNumber:9},globalThis)]},void 0,!0,{fileName:t,lineNumber:8,columnNumber:7},globalThis)},void 0,!1,{fileName:t,lineNumber:7,columnNumber:5},globalThis);export{Be as default};
