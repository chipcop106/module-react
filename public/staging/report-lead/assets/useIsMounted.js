import{e as r}from"./vendor.js";const s=()=>{const e=r.exports.useRef(!1);return r.exports.useEffect(()=>(e.current=!0,()=>{e.current=!1}),[]),r.exports.useCallback(()=>e.current,[])};export{s as u};
