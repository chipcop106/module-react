var S=Object.defineProperty,E=Object.defineProperties;var k=Object.getOwnPropertyDescriptors;var p=Object.getOwnPropertySymbols;var P=Object.prototype.hasOwnProperty,f=Object.prototype.propertyIsEnumerable;var m=(i,s,e)=>s in i?S(i,s,{enumerable:!0,configurable:!0,writable:!0,value:e}):i[s]=e,u=(i,s)=>{for(var e in s||(s={}))P.call(s,e)&&m(i,e,s[e]);if(p)for(var e of p(s))f.call(s,e)&&m(i,e,s[e]);return i},l=(i,s)=>E(i,k(s));import{g as t,a9 as I}from"./vendor.js";const M={province:null,center:{label:"T\u1EA5t c\u1EA3",value:0},status:{label:"T\u1EA5t c\u1EA3",value:0},startDate:null,endDate:null,subject:{name:"T\u1EA5t c\u1EA3",id:0}},v=2,F=1,G=1,C=6,T=t`
  fragment QuestionField on IeltsPracticeQuestion {
    id
    testId
    questionGroupId
    question
    questionIndex
    questionCount
    sampleAnswers
    allowToBreakPoint

    answers {
      answer
      correct
      alternatives
    }
    explanation
    type
  }
`,d=t`
  fragment QuestionGroupField on IeltsPracticeQuestionGroup {
    id
    title
    testId
    question
    groupIndex
    audioFiles {
      id
      url
    }
    questions {
      ...QuestionField
    }
  }
  ${T}
`,$=t`
  fragment ModuleField on IeltsPracticeTest {
    id
    title
    type
    level
    status
    format
    parts {
      question
      questionGroupId
    }
    audio {
      id
      url
    }
    questionGroups {
      ...QuestionGroupField
    }
  }
  ${d}
`,r=t`
  fragment PracticeTestField on IeltsPracticeTestGroup {
    id
    title
    slug
    level
    status
    practiceTests {
      id
      title
      type
      level
      status
    }
    active
  }
`;t`
  mutation createIELTSPracticeTest(
    $type: IeltsPracticeTestType!
    $level: IeltsPracticeTestLevel!
    $title: String
    $audio: Upload
    $parts: [IeltsPracticeTestPartInput]
    $format: IeltsPracticeTestFormat
  ) @api(name: "zim") {
    createIELTSPracticeTest(
      type: $type
      level: $level
      title: $title
      audio: $audio
      parts: $parts
      format: $format
    ) {
      id
      title
      type
      level
      status
      parts {
        question
        questionGroupId
      }
    }
  }
`;t`
  mutation updateIELTSPracticeTest(
    $id: ObjectID!
    $type: IeltsPracticeTestType!
    $level: IeltsPracticeTestLevel!
    $title: String
    $audio: Upload
    $parts: [IeltsPracticeTestPartInput]
    $format: IeltsPracticeTestFormat
  ) @api(name: "zim") {
    updateIELTSPracticeTest(
      id: $id
      type: $type
      level: $level
      title: $title
      audio: $audio
      parts: $parts
      format: $format
    ) {
      id
      type
      level
      status
      parts {
        question
        questionGroupId
      }
    }
  }
`;t`
  mutation publishIELTSPracticeTest($id: ObjectID!) @api(name: "zim") {
    publishIELTSPracticeTest(id: $id) {
      id
      type
      level
      status
      questionGroups {
        ...QuestionGroupField
      }
    }
  }
  ${d}
`;t`
  mutation createIELTSPracticeTestQuestionGroup(
    $testId: ObjectID!
    $question: String!
    $groupIndex: Int
    $title: String
  ) @api(name: "zim") {
    createIELTSPracticeTestQuestionGroup(
      testId: $testId
      question: $question
      groupIndex: $groupIndex
      title: $title
    ) {
      ...QuestionGroupField
    }
  }
  ${d}
`;t`
  mutation updateIELTSPracticeTestQuestionGroup(
    $id: ObjectID!
    $question: String!
    $title: String
    $groupIndex: Int
  ) @api(name: "zim") {
    updateIELTSPracticeTestQuestionGroup(
      id: $id
      question: $question
      title: $title
      groupIndex: $groupIndex
    ) {
      ...QuestionGroupField
    }
  }
  ${d}
`;t`
  mutation removeIELTSPracticeTestQuestionGroup($id: ObjectID!)
  @api(name: "zim") {
    removeIELTSPracticeTestQuestionGroup(id: $id)
  }
`;t`
  mutation removeIELTSPracticeTestQuestion($id: ObjectID!) @api(name: "zim") {
    removeIELTSPracticeTestQuestion(id: $id)
  }
`;t`
  mutation createIELTSPracticeTestQuestion(
    $questionGroupId: ObjectID!
    $input: IELTSPracticeTestQuestionInput!
    $questionIndex: Int
  ) @api(name: "zim") {
    createIELTSPracticeTestQuestion(
      questionGroupId: $questionGroupId
      input: $input
      questionIndex: $questionIndex
    ) {
      ...QuestionField
    }
  }
  ${T}
`;t`
  mutation updateIELTSPracticeTestQuestion(
    $id: ObjectID!
    $input: IELTSPracticeTestQuestionInput!
  ) @api(name: "zim") {
    updateIELTSPracticeTestQuestion(id: $id, input: $input) {
      ...QuestionField
    }
  }
  ${T}
`;t`
  mutation swapIELTSPracticeTestQuestionIndex(
    $firstId: ObjectID!
    $secondId: ObjectID!
  ) @api(name: "zim") {
    swapIELTSPracticeTestQuestionIndex(firstId: $firstId, secondId: $secondId) {
      id
      testId
    }
  }
`;t`
  mutation swapIELTSPracticeTestQuestionGroupIndex(
    $firstId: ObjectID!
    $secondId: ObjectID!
  ) @api(name: "zim") {
    swapIELTSPracticeTestQuestionGroupIndex(
      firstId: $firstId
      secondId: $secondId
    ) {
      id
      testId
    }
  }
`;t`
  mutation createIELTSPracticeTestGroup(
    $title: String!
    $slug: String!
    $level: IeltsPracticeTestLevel!
    $practiceTests: [ObjectID]!
    $featureMediaIds: [ObjectID]!
    $description: String
  ) @api(name: "zim") {
    createIELTSPracticeTestGroup(
      title: $title
      slug: $slug
      level: $level
      practiceTests: $practiceTests
      featureMediaIds: $featureMediaIds
      description: $description
    ) {
      ...PracticeTestField
    }
  }
  ${r}
`;t`
  mutation updateIELTSPracticeTestGroup(
    $id: ObjectID!
    $title: String!
    $slug: String!
    $level: IeltsPracticeTestLevel!
    $practiceTests: [ObjectID]!
    $featureMediaIds: [ObjectID]!
    $description: String
    $categoryIds: [ObjectID]
  ) @api(name: "zim") {
    updateIELTSPracticeTestGroup(
      id: $id
      title: $title
      slug: $slug
      level: $level
      practiceTests: $practiceTests
      featureMediaIds: $featureMediaIds
      description: $description
      categoryIds: $categoryIds
    ) {
      ...PracticeTestField
    }
  }
  ${r}
`;t`
  mutation publishIELTSPracticeTestGroup($id: ObjectID!) @api(name: "zim") {
    publishIELTSPracticeTestGroup(id: $id) {
      ...PracticeTestField
    }
  }
  ${r}
`;t`
  mutation unpublishIELTSPracticeTestGroup($id: ObjectID!) @api(name: "zim") {
    unpublishIELTSPracticeTestGroup(id: $id) {
      ...PracticeTestField
    }
  }
  ${r}
`;t`
  mutation setActiveIELTSPracticeTestGroup($id: ObjectID!) @api(name: "zim") {
    setActiveIELTSPracticeTestGroup(id: $id) {
      ...PracticeTestField
    }
  }
  ${r}
`;t`
  mutation updateIELTSPracticeTestGroupSeoConfig(
    $IELTSPracticeTestGroupId: ObjectID!
    $seo: CommonSeoInput!
  ) @api(name: "zim") {
    updateIELTSPracticeTestGroupSeoConfig(
      IELTSPracticeTestGroupId: $IELTSPracticeTestGroupId
      seo: $seo
    ) {
      seo {
        title
        description
        ogDescription
        ogImage {
          id
          type
          path
          filename
          title
          visibility
          width
          height
          usage
          createdAt
          updatedAt
        }
        ogTitle
        publisher
        noIndex
        noFollow
        canonicalUrl
        customHeadHtml
      }
    }
  }
`;const q=t`
  mutation createMockTest(
    $dates: [String]
    $school_id: Int!
    $type: EnumMockTestType
    $subjectId: Int!
  ) @api(name: "appZim") {
    createMockTest(
      dates: $dates
      school_id: $school_id
      type: $type
      subjectId: $subjectId
    ) {
      id
      slug
      type
      status
      fee
      total_slot
    }
  }
`,j=t`
  mutation updateMockTest(
    $id: Int
    $status: EnumBaseStatus
    $dates: [String]
    $school_id: Int
  ) @api(name: "appZim") {
    updateMockTest(
      id: $id
      status: $status
      dates: $dates
      school_id: $school_id
    ) {
      id
    }
  }
`,R=t`
  mutation updateMockTestDetailV2(
    $mockTestId: Int!
    $mockTestDetail: [MockTestDetailInputV2Type]!
  ) @api(name: "appZim") {
    updateMockTestDetailV2(
      mockTestDetail: $mockTestDetail
      mockTestId: $mockTestId
    )
  }
`,A=t`
  mutation updateMockTestAttendanceV2(
    $mock_test_detail_id: Int
    $student_id: Int
    $is_missing: Boolean
  ) @api(name: "appZim") {
    updateMockTestAttendanceV2(
      mock_test_detail_id: $mock_test_detail_id
      student_id: $student_id
      is_missing: $is_missing
    ) {
      id
    }
  }
`,w=t`
  mutation uploadResultStudent($input: UploadResultMockTestOrderInputType)
  @api(name: "appZim") {
    uploadResultStudent(input: $input) {
      success
    }
  }
`,Q=t`
  mutation updateResultMockTest(
    $orderId: Int
    $input: UpdateOrderDetailInputType
  ) @api(name: "appZim") {
    updateResultMockTest(orderId: $orderId, input: $input) {
      message
    }
  }
`,U=t`
  mutation sentEmailResultForStudent($orderId: Int) @api(name: "appZim") {
    sentEmailResultForStudent(orderId: $orderId) {
      message
    }
  }
`,x=t`
  mutation createMockTetOfStudentSchedule(
    $input: CreateMockTestOfStudentScheduleInputType
  ) @api(name: "appZim") {
    createMockTestOfStudentSchedule(input: $input) {
      message
    }
  }
`;t`
  mutation confirmOrCancelOrder(
    $orderId: ObjectID!
    $action: OrderActionInput!
    $cancelReason: String
    $confirmPayment: Boolean
  ) @api(name: "zim") {
    confirmOrCancelOrder(
      orderId: $orderId
      action: $action
      cancelReason: $cancelReason
      confirmPayment: $confirmPayment
    ) {
      id
    }
  }
`;const h=t`
  fragment SEOField on CommonSeoConfig {
    title
    description
    ogDescription
    ogImage {
      id
      type
      path
      variants {
        id
        width
        height
        path
        type
      }
      filename
      title
      visibility
      width
      height
      usage
      createdAt
      updatedAt
    }
    ogTitle
    publisher
    noIndex
    noFollow
    canonicalUrl
    customHeadHtml
  }
`,L=t`
  fragment MockTestField on MockTestV2Type {
    id
    slug
    status
    fee
    total_slot
    available_slot
    school {
      id
      name
    }
    mockTestDetails {
      id
      date
      shift {
        id
        name
        start_time
        end_time
      }
      subject {
        id
        name
      }
      mock_test_shift_id
      mock_test_subject_id
      ecs {
        id
        full_name
      }
      teachers {
        id
        full_name
      }
    }
    subject {
      id
      name
    }
  }
`,O=t`
  fragment MockTestFieldForDetail on MockTestV2Type {
    id
    slug
    status
    fee
    type
    total_slot
    available_slot
    sent_email_status
    school {
      id
      name
    }
    mockTestDetails {
      id
      date
      shift {
        id
        name
        start_time
        end_time
      }
      subject {
        id
        name
      }
      mock_test_shift_id
      mock_test_subject_id
      available_slot
      total_slot
      currentSlot
      ecs {
        id
        full_name
      }
      teachers {
        id
        full_name
      }
    }
    examQuestionMockTest {
      id
      cat {
        id
        name
      }
      configMockTestBank {
        id
        questionLink
        answerLink
        referenceLink
        mockTestSubCat {
          id
          name
        }
      }
    }
  }
`;t`
  query IELTSPracticeTestGroupPagination(
    $page: Int
    $limit: Int
    $status: [IeltsPracticeTestGroupStatus]
    $level: [IeltsPracticeTestLevel]
  ) @api(name: "zim") {
    IELTSPracticeTestGroupPagination(
      page: $page
      limit: $limit
      status: $status
      level: $level
    ) {
      hasNextPage
      hasPrevPage
      limit
      nextPage
      page
      pagingCounter
      prevPage
      totalDocs
      totalPages
      docs {
        id
        slug
        title
        level
        status
        active
        categories {
          id
          title
          description
        }
        practiceTests {
          id
          title
        }
      }
    }
  }
`;t`
  query IELTSPracticeTestPagination(
    $page: Int
    $limit: Int
    $type: [IeltsPracticeTestType]
    $status: [IeltsPracticeTestStatus]
    $level: [IeltsPracticeTestLevel]
    $format: [IeltsPracticeTestFormat]
  ) @api(name: "zim") {
    IELTSPracticeTestPagination(
      page: $page
      limit: $limit
      type: $type
      status: $status
      level: $level
      format: $format
    ) {
      hasNextPage
      hasPrevPage
      limit
      nextPage
      page
      pagingCounter
      prevPage
      totalDocs
      totalPages
      docs {
        ...ModuleField
      }
    }
  }
  ${$}
`;t`
  query IELTSPracticeTest($id: ObjectID!) @api(name: "zim") {
    IELTSPracticeTest(id: $id) {
      ...ModuleField
    }
  }
  ${$}
`;t`
  query IELTSPracticeTestGroup($id: ObjectID!) @api(name: "zim") {
    IELTSPracticeTestGroup(id: $id) {
      id
      title
      slug
      description
      level
      status
      active
      practiceTests {
        id
        title
        type
        status
      }
      categories {
        id
        title
        description
        slug
      }
      featureMedias {
        id
        type
        path
        variants {
          id
          width
          height
          path
          type
        }
        filename
        title
        visibility
        width
        height
        usage
        createdAt
        updatedAt
      }
      seo {
        ...SEOField
      }
    }
  }
  ${h}
`;const V=t`
  query getMockTestDetailV2($idOrSlug: String) @api(name: "appZim") {
    getMockTestDetailV2(idOrSlug: $idOrSlug) {
      id
      status
      details {
        date
      }
      fee
      school {
        id
        name
      }
      subject {
        id
        name
      }
    }
  }
`,z=t`
  query getMockTestDetailV2($idOrSlug: String) @api(name: "appZim") {
    getMockTestDetailV2(idOrSlug: $idOrSlug) {
      ...MockTestFieldForDetail
    }
  }
  ${O}
`,B=(i=[])=>t`
  query getMockTestDetailByField($idOrSlug: String) @api(name: "appZim") {
    getMockTestDetailV2(idOrSlug: $idOrSlug) {
      id
      ${i.join()}
    }
  }
`,Z=t`
  query getListMockTestByRoleV2(
    $q: String
    $type: EnumMockTestType
    $status: EnumBaseStatus
    $schoolId: Int
    $fromDate: String
    $toDate: String
    $offset: Int
    $limit: Int
    $subjectId: Int
  ) @api(name: "appZim") {
    getListMockTestByRoleV2(
      q: $q
      type: $type
      status: $status
      schoolId: $schoolId
      fromDate: $fromDate
      toDate: $toDate
      offset: $offset
      limit: $limit
      subjectId: $subjectId
    ) {
      edges {
        node {
          ...MockTestField
        }
      }
      totalCount
      pageInfo {
        hasNextPage
        hasPreviousPage
      }
    }
  }
  ${L}
`,K=t`
  query getListTeacherRegisterForFunction(
    $type: Int!
    $date: String!
    $schoolId: Int
    $shift: Int!
  ) @api(name: "appZim") {
    getListTeacherRegisterForFunction(
      type: $type
      date: $date
      schoolId: $schoolId
      shift: $shift
    ) {
      id
      full_name
    }
  }
`,N=t`
  query getListEcForCenter($schoolId: Int) @api(name: "appZim") {
    getListEcForCenter(schoolId: $schoolId) {
      id
      full_name
    }
  }
`,H=t`
  query getListMockTestOrderByRoles(
    $mocktestId: String
    $status: MockTestStatusEnum
  ) @api(name: "appZim") {
    getListMockTestOrderByRoles(mocktestId: $mocktestId, status: $status) {
      id
      id_number
      status
      order_id
      student {
        id
        full_name
        phone
        email
      }
      owner {
        name
        email
        phone
      }
      moduleV2 {
        id
        name
        create_date
        status
      }
      mockTestDetailV2 {
        date
        id
        shift {
          id
          start_time
          end_time
        }
        subject {
          id
          name
        }
      }
      schedulesV2 {
        is_missing
        subject {
          id
        }
        timePreview
        platform_id
      }
      feedback_writing1
      feedback_writing2
      teacherV2 {
        id
        full_name
      }
      catV2 {
        id
        name
      }
      mockTestResultv2 {
        id
        reading
        writing1
        writing2
        writing_note
        overall
        listening
        speaking
        send_email_status
        speakingNote
        sessions {
          sessionIdReading
          sessionIdWriting
          sessionIdSpeaking
          sessionIdListening
          sessionIdWritingTask1
          sessionIdWritingTask2
        }
        mock_test_writings {
          id
          essay_correction {
            id
            title
          }
          score
          status
        }
      }
      imageMockTestOrder {
        id
        mockTestOrderId
        mockTestSubCatId
        mockTestSubCat {
          id
          name
        }
        url
      }
    }
  }
`,W=t`
  query ($catId: Int!) @api(name: "appZim") {
    getSubCatMockTestForStudent(catId: $catId) {
      id
      name
    }
  }
`,Y=t`
  query getImageMockTestOrderBySubCat($orderId: Int!, $mockTestSubCatId: Int)
  @api(name: "appZim") {
    getImageMockTestOrderBySubCat(
      mockTestSubCatId: $mockTestSubCatId
      orderId: $orderId
    ) {
      id
      mockTestOrderId
      mockTestSubCatId
      mockTestSubCat {
        id
        name
      }
      url
    }
  }
`;t`
  query calcWritingAverage($writingTask1: Float, $writingTask2: Float)
  @api(name: "appZim") {
    calcWritingAverage(writingTask1: $writingTask1, writingTask2: $writingTask2)
  }
`;const J=t`
  query calcOverall(
    $listening: Float!
    $reading: Float!
    $writing: Float
    $writingTask1: Float
    $writingTask2: Float
    $speaking: Float!
  ) @api(name: "appZim") {
    calcOverall(
      listening: $listening
      reading: $reading
      writing: $writing
      speaking: $speaking
      writingTask1: $writingTask1
      writingTask2: $writingTask2
    )
  }
`,X=t`
  query getResultMockTestStudent($mocktestId: String) @api(name: "appZim") {
    getResultMockTestStudent(mocktestId: $mocktestId) {
      id
      listening
      reading
      writing1
      writing2
      writing_note
      send_email_status
      speaking
      speakingNote
    }
  }
`,tt=t`
  query getListTeacherAssignedMockTest($mockTestId: Int!) @api(name: "appZim") {
    getListTeacherAssignedMockTest(mockTestId: $mockTestId) {
      id
      full_name
    }
  }
`,et=t`
  query getListTeacherOfMockTestWithSchedule($mockTestId: Int, $orderId: Int)
  @api(name: "appZim") {
    getListTeacherOfMockTestWithSchedule(
      mockTestId: $mockTestId
      orderId: $orderId
    ) {
      teacher_id
      teacher_name
      schedule {
        mock_test_detail_id
        isChange
        id_number
        lRW
        speaking
      }
    }
  }
`,D=(i=[])=>I(i.map(e=>e.shift),"id").map(e=>{const n=i.filter(o=>o.shift.id===e.id)||[],c=n.reduce((o,a)=>(o.push(l(u({},a.subject||{}),{mockTestDetailId:a.id,ecs:a.ecs,total_slot:a.total_slot,available_slot:a.available_slot,currentSlot:a.currentSlot})),o),[]).sort((o,a)=>o.id-a.id),g=n.reduce((o,a)=>o.concat(a.ecs),[]),_=n.reduce((o,a)=>o.concat(a.teachers),[]);return l(u({},e),{subjects:I(c,"id"),ecs:g,teachers:_})});function it(i=[]){var e;const s=i.reduce(function(n,c){return n[c.date]=n[c.date]||[],n[c.date].push(c),n},Object.create(null));return(e=Object.entries(s))==null?void 0:e.map(([n,c])=>({date:n,shifts:D(c)}))}export{q as C,V as G,H as M,U as S,j as U,Z as a,M as b,A as c,W as d,Y as e,w as f,it as g,X as h,J as i,Q as j,et as k,x as l,F as m,B as n,z as o,K as p,N as q,G as r,v as s,C as t,tt as u,R as v};
